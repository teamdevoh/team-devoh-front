// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.5.0/firebase-messaging.js');
// Initialize Firebase
const config = {
  apiKey: 'AIzaSyCOEVJeb2uRSqz04fMRtMQLkQwFQtKzYwk',
  authDomain: 'smartrnd-235602.firebaseapp.com',
  databaseURL: 'https://smartrnd-235602.firebaseio.com',
  projectId: 'smartrnd-235602',
  storageBucket: 'smartrnd-235602.appspot.com',
  messagingSenderId: '448548435657',
  appId: '1:448548435657:web:09aa1c5023e227acc17f18',
  measurementId: 'G-G05F0JEP5L'
};

firebase.initializeApp(config);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.

if (firebase.messaging.isSupported()) {
  const messaging = firebase.messaging();

  messaging.setBackgroundMessageHandler(({ payload } = {}) => {
    const title = payload.notification.title || 'Title2';
    const options = {
      body: payload.notification.body || 'Body'
    };

    return window.self.registration.showNotification(title, options);
  });
}
