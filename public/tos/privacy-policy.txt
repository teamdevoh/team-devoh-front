개인정보의 수집목적 및 이용목적<br />
Purpose of personal information collection and use<br />
<br />
개인 정보의 수집 목적 및 이용 목적은 다음과 같습니다.<br />
<br />
1.회원제 서비스 이용에 따른 본인 확인 절차에 사용<br />
2.고지사항 전달, 불만 처리 등을 위한 의사소통 경로, 새로운 서비스 및 행사정보 안내<br />
3.각종 서비스의 원활한 제공을 위한 온라인 및 오프라인에서의 공지<br />
4.통계분석자료로 활용을 위한 개인 식별할 수 없는 최소한의 정보<br />
5.교육 신청자의 교육진행을 위한 자료<br />
(위 항의 원활한 업무처리를 위하여 개인정보 처리(취급) 업무(DM등)를 외부 전문업체에 위탁할 수 있으며 해당내용은 홈페이지에 공개합니다.)<br />
<br />
회원님이 제공하신 모든 정보는 상기 목적에 필요한 용도 이외로는 사용되지 않으며 수집 정보의 범위나 사용 목적, 용도가 변경될 시에는 반드시 회원님께 사전 동의를 구할 것입니다. ('홈페이지' 이용 약관 제8조)<br />
<br />
<br />
개인정보의 항목 및 방법<br />
Items and methods of personal information<br />
개인 정보의 수집 항목<br />
'홈페이지'가 서비스 제공을 위해 제공 받는 개인 정보는 아이디, 비밀번호, 성명, 생년월일, 성별, 사무실 전화번호, 핸드폰 번호, 이메일, 소속기관, 부서, 직위, 접속국가입니다. 서비스 이용 과정에서 접속 기록, 접속 아이피의 정보가 수집될 수 있습니다. '홈페이지'에서는 이용자의 출신지 및 본적지 등과 같은 기본적 인권을 침해할 우려가 있는 민감한 개인정보는 수집하고 있지 않습니다.<br />
개인 정보의 수집 방법<br />
센터는 홈페이지 회원가입, 신청 양식, 전화, 팩스, 이메일, 이벤트 응모등을 통해 개인정보를 수집합니다.<br />
<br />
개인정보 수집 시에는 별도로 동의 받아야할 사항(고유식별정보, 민감정보, 목적 외 이용 및 제 3자 제공, 마케팅목적 활용)은 가입신청서등을 통하여 각각 동의를 받은 후 수집합니다.<br />
<br />
<br />
개인정보의 보유 및 이용기간<br />
Retention and use period of personal information<br />
회원님이 '홈페이지'에서 제공하는 서비스를 받는 동안 회원님의 개인 정보는 '홈페이지'에서 계속 보유하며 서비스 제공을 위해 이용하게 됩니다. 회원 탈퇴 시에는 즉시 파기합니다.<br />
1. 이용기간 : 회원 가입기간 (탈퇴 후 즉시 파기)<br />
2. 설문조사, 행사등의 목적으로 수집된 경우 : 설문조사, 행사등의 목적 달성이 종료된 후 파기<br />
3. 진료 및 임상연구 서비스의 제공을 위하여 수집된 경우 : 의료법 기준에 준함<br />
수집목적 또는 제공받은 목적이 달성된 경우에도 상법 등 법령의 규정, 의약품 등의 안전에 관한 규칙에 의하여 보존할 필요성이 있는 경우에는 귀하의 개인정보를 보유할 수 있습니다.