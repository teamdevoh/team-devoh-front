const path = require('path');

module.exports = {
  title: 'Smart R&D Workstation',
  webpackConfig: require('./config/webpack.config.dev.js'),
  ignore: ['**/components/**/ProjectAdd.js'],
  styles: {
    Playground: {
      preview: {
        paddingLeft: 0,
        paddingRight: 0,
        borderWidth: [[0, 0, 1, 0]],
        borderRadius: 0
      }
    },
    Markdown: {
      pre: {
        border: 0,
        background: 'none'
      },
      code: {
        fontSize: 14,
        class: 'hljs javascript'
      }
    }
  },
  context: {
    project_manange: path.join(__dirname, 'src/styleguide/project_manange_data')
  },
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/styleguide/Wrapper')
  },
  sections: [
    {
      name: 'Introduction',
      content: 'docs/introduction.md'
    },
    {
      name: 'How to',
      content: 'docs/howto.md'
    },
    {
      name: 'Components',
      content: 'docs/components.md',
      sections: [
        {
          name: 'Date Picker',
          content: 'docs/picker.md',
          components: 'src/components/datepicker/[A-Z]*.js'
        },
        {
          name: 'Button',
          components: 'src/components/button/[A-Z]*.js'
        },
        {
          name: 'Input',
          components: 'src/components/input/[A-Z]*.js'
        },
        // {
        //   name: 'Project Manage',
        //   components: 'src/components/project-manage/[A-Z]*.js'
        // },
        {
          name: 'Item',
          components: 'src/components/item/[A-Z]*.js'
        }
      ]
    }
  ]
};
