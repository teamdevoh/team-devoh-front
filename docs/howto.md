

Component init code :


```jsx
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


const propTypes = {
  /** props is described here. */
  prop1: PropTypes.string.isRequired
};


const defaultProps = {
};


const mapStateToProps = (state) => {
  return {
    
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    
  };
};


/**
 * Component is described here.
 */
class Example extends Component {
  render () {
    return (
      <div>
        
      </div>
    );
  }
}


Example.propTypes = propTypes;
Example.defaultProps = defaultProps;


export default connect(mapStateToProps, mapDispatchToProps)(Example);
```