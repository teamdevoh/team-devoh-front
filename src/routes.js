import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Authorized } from './components/auth';
import Loadable from 'react-loadable';
import { Loading } from './components/loading'
import { asyncComponent } from './components/modules';
import { role } from './constants';

const cohortStatus = asyncComponent(() =>
  import('./components/cohort/Status')
);
const Network = asyncComponent(() =>
  import('./components/error/Network')
);
const Console = asyncComponent(() =>
  import('./components/console/Console')
);
const TaskProgressContainer = asyncComponent(() =>
  import('./components/console/TaskProgressContainer')
);
const TaskSchedulerContainer = asyncComponent(() =>
  import('./components/console/TaskSchedulerContainer')
);
const WordCloud = asyncComponent(() =>
  import('./components/word-pubmed-analysis/WordCloud')
);
const WordNetwork = asyncComponent(() =>
  import('./components/word-pubmed-analysis/WordNetwork')
);
const SignIn = asyncComponent(() =>
  import('./components/account-manage/SignIn')
);
const MemberSecurityPledge = asyncComponent(() =>
  import('./components/account-manage/MemberSecurityPledge')
);
const TermsOfService = asyncComponent(() =>
  import('./components/account-manage/TermsOfService')
);
const FindId = asyncComponent(() =>
  import('./components/account-manage/FindId')
);
const FoundId = asyncComponent(() =>
  import('./components/account-manage/FoundId')
);
const GuidelineContainer = asyncComponent(() =>
  import('./components/guidline/GuidelineContainer')
);
const ResetPasswordVerify = asyncComponent(() =>
  import('./components/account-manage/ResetPasswordVerify')
);
const ResetPassword = asyncComponent(() =>
  import('./components/account-manage/ResetPassword')
);
const Verification = asyncComponent(() =>
  import('./components/auth/Verification')
);
const SignUp = asyncComponent(() =>
  import('./components/account-manage/SignUp')
);
const CompletedMembership = asyncComponent(() =>
  import('./components/account-manage/CompletedMembership')
);
const Profile = asyncComponent(() =>
  import('./components/account-manage/Profile')
);
const Settings = asyncComponent(() =>
  import('./components/account-manage/Settings')
);
const Leave = asyncComponent(() =>
  import('./components/account-manage/Leave')
);
const ProjectDashBoard = asyncComponent(() =>
  import('./components/project-manage/ProjectDashBoard')
);
const ProjectAdd = asyncComponent(() =>
  import('./components/project-manage/ProjectAdd')
);
const ProjectEdit = asyncComponent(() =>
  import('./components/project-manage/ProjectEdit')
);
const ProjectSiteListAll = asyncComponent(() =>
  import('./components/project-manage/ProjectSiteListAll')
);
const ProjectSiteAdd = asyncComponent(() =>
  import('./components/project-manage/ProjectSiteAdd')
);
const ProjectSiteEdit = asyncComponent(() =>
  import('./components/project-manage/ProjectSiteEdit')
);
const ProjectMemberListContainer = asyncComponent(() =>
  import('./components/project-manage/ProjectMemberListContainer')
);
const ProjectMemberAdd = asyncComponent(() =>
  import('./components/project-manage/ProjectMemberAdd')
);
const ProjectMemberEdit = asyncComponent(() =>
  import('./components/project-manage/ProjectMemberEdit')
);
const TaskDashBoard = asyncComponent(() =>
  import('./components/task-manage/TaskDashBoard')
);
const TaskAdd = asyncComponent(() =>
  import('./components/task-manage/TaskAdd')
);
const TaskNotify = asyncComponent(() =>
  import('./components/task-manage/TaskNotifyContainer')
);
const Conversation = asyncComponent(() =>
  import('./components/conversation/ConversationDashBoard')
);
const ProjectSearchDashBoard = asyncComponent(() =>
  import('./components/project-manage/ProjectSearchDashBoard')
);
const ProjectArchiveList = asyncComponent(() =>
  import('./components/project-archive/ProjectArchiveList')
);
const ProjectArchiveAdd = asyncComponent(() =>
  import('./components/project-archive/ProjectArchiveAdd')
);
const ProjectArchiveEdit = asyncComponent(() =>
  import('./components/project-archive/ProjectArchiveEdit')
);
const ProjectAttachHistoryList = asyncComponent(() =>
  import('./components/project-archive/ProjectAttachHistoryList')
);
const ProjectArchiveListByTask = asyncComponent(() =>
  import('./components/project-archive/ProjectArchiveListByTask')
);
const ArchiveReqDashBoard = asyncComponent(() =>
  import('./components/project-archive/ArchiveReqDashBoard')
);
const ArchiveSearch = asyncComponent(() =>
  import('./components/project-archive/ArchiveSearch')
);
const CollectionChart = asyncComponent(() =>
  import('./components/collection/CollectionChartTab')
);
const CollectionCalendar = asyncComponent(() =>
  import('./components/collection/CollectionCalendar')
);
const CollectionDataDashBoard = asyncComponent(() =>
  import('./components/collection/DashBoard')
);
const DistContainer = asyncComponent(() =>
  import('./components/distribution/DistContainer')
);
const TDMScatterPlot = asyncComponent(() =>
  import('./components/tdm-scatter-plot/TDMScatterPlot')
);
const MICDisContainer = asyncComponent(() =>
  import('./components/distributionMIC/MICDisContainer')
);
const QueryContainer = asyncComponent(() =>
  import('./components/collection/QueryContainer')
);
const CRContainer = asyncComponent(() =>
  import('./components/case-review/CRContainer')
);
const CohortExportContainer = asyncComponent(() =>
  import('./components/collection-export/ExportContainer')
);
const CohortDashboardContainer = asyncComponent(() =>
  import('./components/dashboard/cohort/CohortTab')
);
const TdmExportContainer = asyncComponent(() =>
  import('./components/export-tdm/DashBoardContainer')
);
const TdmReportContainer = asyncComponent(() =>
  import('./components/tdm-report/DashBoardContainer')
);
const ListWarpper = asyncComponent(() =>
  import('./components/export-data-manage/ListWarpper')
);
const ReqFormContainer = asyncComponent(() =>
  import('./components/export-data-manage/ReqFormContainer')
);
const EditReqCohort = asyncComponent(() =>
  import('./components/export-data-manage/EditReqCohort')
);
const EditReqTDM = asyncComponent(() =>
  import('./components/export-data-manage/EditReqTDM')
);
const CollectionTbdrugContainer = asyncComponent(() =>
  import('./components/collection-tbdrug/DashBoardContainer')
);
const SubjectContainer = asyncComponent(() =>
  import('./components/collection/SubjectContainer')
);
const ActivityContainer = asyncComponent(() =>
  import('./components/collection/ActivityContainer')
)
const TDMReport = asyncComponent(() =>
  import('./components/report/TDMReport')
);
const CollectionDiliCohortContainer = asyncComponent(() =>
  import('./components/collection-diliCohort/DashBoardContainer')
);
const CollectionPopPKContainer = asyncComponent(() =>
  import('./components/collection-poppk/DashBoardContainer')
);
const ModelFormAdd = asyncComponent(() =>
  import('./components/collection-poppk/ModelFormAdd')
);
const ModelFormEdit = asyncComponent(() =>
  import('./components/collection-poppk/ModelFormEdit')
);
const CollectionSampleCollectionContainer = asyncComponent(() =>
  import('./components/sample-status/DashBoardContainer')
);
const SampleBarcodeContainer = asyncComponent(() =>
  import('./components/sample-barcode')
);
const SampleAliquotBarcodeContainer = asyncComponent(() =>
  import('./components/sampleAliquot-barcode')
);
const SampleSettingsContainer = asyncComponent(() =>
  import('./components/sample-setting')
);
const SampleAliquot = asyncComponent(() =>
  import('./components/sample-aliquot')
);
const SampleBoxesContainer = asyncComponent(() =>
  import('./components/sample-boxes')
);
const SubjectLogContainer = asyncComponent(() =>
  import('./components/subjectlog-manager')
);
const SampleBoxing = asyncComponent(() =>
  import('./components/sample-boxing')
);
const FreezerIn = asyncComponent(() =>
  import('./components/sample-freezer-in')
);
const FreezerOut = asyncComponent(() =>
  import('./components/sample-freezer-out')
);
const SampleOut = asyncComponent(() =>
  import('./components/sample-out')
);
const SampleFreezing = asyncComponent(() =>
  import('./components/sample-freezing')
);
const SampleDashboard = asyncComponent(() =>
  import('./components/dashboard/sample/Dashboard')
);
const TdmStatus = asyncComponent(() =>
  import('./components/tdm-status')
);
const MicStatus = asyncComponent(() =>
  import('./components/mic-status')
);
const TbdrugDashboard = asyncComponent(() =>
  import('./components/dashboard/tbdrug/Dashboard')
);
const PkDataExportContainer = asyncComponent(() =>
  import('./components/collection-tbdrug-export/pk-data/DashBoardContainer')
);
const PbPkModelExportContainer = asyncComponent(() =>
  import('./components/collection-tbdrug-export/pbpk-model/DashBoardContainer')
);

function Routes() {
  return (
    <Switch>
      <Route exact path="/cohort/status" component={cohortStatus} />
      <Route exact path="/error/network" component={Network} />
      <Route exact path="/" component={Authorized(Console)} />
      <Route exact path="/console/task" component={Authorized(TaskProgressContainer, role.ConsoleTask_Read)} />
      <Route exact path="/console/task/scheduler" component={Authorized(TaskSchedulerContainer, role.ConsoleTask_Read)} />
      <Route exact path="/console/wordcloud" component={Authorized(WordCloud)} />
      <Route exact path="/console/wordnetwork" component={Authorized(WordNetwork)} />
      <Route exact path="/signin" component={SignIn} />
      <Route exact path="/securitysign" component={MemberSecurityPledge} />
      <Route exact path="/tos" component={TermsOfService} />

      <Route exact path="/findId" component={FindId} />
      <Route exact path="/verify/resetPassword" component={ResetPasswordVerify} />
      <Route path="/foundId" component={FoundId} />
      <Route path="/resetPassword" component={ResetPassword} />
      <Route exact path="/guideline" component={GuidelineContainer} />

      <Route exact path="/verification" component={Verification} />

      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/completed" component={CompletedMembership} />
      <Route exact path="/user/profile" component={Authorized(Profile)} />
      <Route exact path="/user/settings" component={Authorized(Settings)} />
      <Route exact path="/user/leave" component={Authorized(Leave)} />
      <Route path="/project/new" component={Authorized(ProjectAdd, role.Project_Edit)} />
      <Route exact path="/project/:projectId" render={(props) => {
        const Component = Authorized(ProjectDashBoard, role.Project_Read);
        return <Component {...props} />
      }} />
      <Route exact path="/project/:projectId/edit" component={Authorized(ProjectEdit, role.Project_Edit)} />
      <Route exact path="/project/:projectId/site" component={Authorized(ProjectSiteListAll, role.ProjectSite_Read)} />
      <Route exact path="/project/:projectId/site/new" component={Authorized(ProjectSiteAdd, role.ProjectSite_Edit)} />
      <Route exact path="/project/:projectId/site/edit/:siteId" component={Authorized(ProjectSiteEdit, role.ProjectSite_Edit)} />
      <Route exact path="/project/:projectId/member" component={Authorized(ProjectMemberListContainer, role.ProjectMember_Read)} />
      <Route exact path="/project/:projectId/member/new" component={Authorized(ProjectMemberAdd, role.ProjectMember_Edit)} />
      <Route exact path="/project/:projectId/member/edit/:memberId" component={Authorized(ProjectMemberEdit, role.ProjectMember_Edit)} />
      <Route exact path="/project/:projectId/task/portal/:active" component={Authorized(TaskDashBoard, `${role.Task_Read},${role.ConsoleTask_Read}`)} />
      <Route exact path="/project/:projectId/task/new" component={Authorized(TaskAdd, `${role.Task_Edit},${role.ConsoleTask_Edit}`)} />
      <Route exact path="/project/:projectId/notify" component={Authorized(TaskNotify, role.Task_Edit)} />
      <Route exact path="/project/:projectId/conversation" render={(props) => {
        const Component = Authorized(Conversation, role.Post_Read);
        return <Component {...props} />
      }} />
      <Route exact path="/project/:projectId/search" render={(props) => {
        const Component = Authorized(ProjectSearchDashBoard, role.Project_Read);
        return <Component {...props} />
      }} />
      <Route exact path="/project/:projectId/archive/req" component={Authorized(ArchiveReqDashBoard, role.ProjectArchive_ReqAllow)} />
      <Route exact path="/project/:projectId/archive/:parentId" render={(props) => {
        const Component = Authorized(ProjectArchiveList, role.ProjectArchive_Read);
        return <Component {...props} />
      }} />
      <Route exact path="/project/:projectId/archive-search" render={(props) => {
        const Component = Authorized(ArchiveSearch, role.ProjectArchive_Read);
        return <Component {...props} />
      }} />
      <Route exact path="/project/:projectId/archive/:parentId/new" component={Authorized(ProjectArchiveAdd, role.ProjectArchive_Edit)} />
      <Route exact path="/project/:projectId/archive/:parentId/edit/:projectArchiveId" component={Authorized(ProjectArchiveEdit, role.ProjectArchive_Edit)} />
      <Route exact path="/project/:projectId/archive/:parentId/attach/:projectAttachmentId/histories" component={Authorized(ProjectAttachHistoryList, role.ProjectArchive_Read)} />
      <Route exact path="/project/:projectId/archivetask/:parentId" render={(props) => {
        const Component = Authorized(ProjectArchiveListByTask, role.Task_Read);
        return <Component {...props} />
      }} />
      <Route path="/report/tdm" component={Authorized(TDMReport, role.Project_Read)} />
      <Route path="/project/:projectId/sample/barcode" component={Authorized(SampleBarcodeContainer, role.Sample_Barcode_Allow)} />
      <Route path="/project/:projectId/sample/aliquot/barcode" component={Authorized(SampleAliquotBarcodeContainer, role.Sample_Aliquot_Barcode_Allow)} />
      <Route path="/project/:projectId/sample/settings" component={Authorized(SampleSettingsContainer, role.Sample_Setting_Allow)} />
      <Route exact path="/project/:projectId/collection/cohort" component={Authorized(CollectionDataDashBoard, role.Activity_Allow)} />
      <Route exact path="/project/:projectId/collection/cohort/dist" component={Authorized(DistContainer, role.Distribution_Read)} />
      <Route exact path="/project/:projectId/collection/cohort/tdm" component={Authorized(TDMScatterPlot, role.TDMScatterPlot_Allow)} />
      <Route exact path="/project/:projectId/collection/cohort/dist-mic" component={Authorized(MICDisContainer, role.MICDistribution_Read)} />
      <Route path="/project/:projectId/collection/cohort/query" component={Authorized(QueryContainer, role.Activity_Read)} />
      <Route path="/project/:projectId/subject/:subjectId/casereview" component={Authorized(CRContainer, role.CaseReview_Read)} />
      <Route exact path="/project/:projectId/collection/cohort/export" component={Authorized(CohortExportContainer, role.Activity_Export_Allow)} />
      <Route exact path="/project/:projectId/collection/cohort/dashboard" component={Authorized(CohortDashboardContainer, role.Activity_Dashboard_Allow)} />
      <Route path="/project/:projectId/tdm/export" component={Authorized(TdmExportContainer, role.TDM_Export_Allow)} />
      <Route path="/project/:projectId/tdm/report" component={Authorized(TdmReportContainer, role.TDMReport_Build)} />
      <Route path="/project/:projectId/export/req/list" component={Authorized(ListWarpper, role.Project_Read)} />
      <Route path="/project/:projectId/export/req/new" component={Authorized(ReqFormContainer, role.Project_Read)} />
      <Route path="/project/:projectId/export/req/edit/:subjectDataRequestId/cohort" component={Authorized(EditReqCohort, role.Project_Read)} />
      <Route path="/project/:projectId/export/req/edit/:subjectDataRequestId/tdm" component={Authorized(EditReqTDM, role.Project_Read)} />
      <Route exact path="/project/:projectId/collection/tbdrug/dashboard" component={Authorized(TbdrugDashboard, role.Activity_Dashboard_Allow)} />
      <Route exact path="/project/:projectId/collection/tbdrug/pkdata/export" component={Authorized(PkDataExportContainer, role.PKData_Export_Allow)} />
      <Route exact path="/project/:projectId/collection/tbdrug/pbpkmodel/export" component={Authorized(PbPkModelExportContainer, role.PBPKModel_Export_Allow)} />
      <Route path="/project/:projectId/collection/tbdrug" component={Authorized(CollectionTbdrugContainer, role.Activity_Allow)} />
      <Route path="/project/:projectId/collection/chart" component={Authorized(CollectionChart, role.Activity_Read)} />
      <Route path="/project/:projectId/collection/calendar" component={Authorized(CollectionCalendar, role.Activity_Read)} />
      <Route path="/project/:projectId/subject/:subjectId/step/:index" component={Authorized(SubjectContainer, role.Subject_Read)} />
      <Route path="/project/:projectId/subject/:subjectId/activitygroup/:activityGroupId" component={ActivityContainer} />
      <Route path="/project/:projectId/collection/dilicohort" component={Authorized(CollectionDiliCohortContainer, role.Activity_Allow)} />
      <Route exact path="/project/:projectId/collection/poppk" component={Authorized(CollectionPopPKContainer, role.PopPK_Read)} />
      <Route path="/project/:projectId/collection/poppk/:tbdrugId/new" component={Authorized(ModelFormAdd, role.PopPK_Edit)} />
      <Route path="/project/:projectId/collection/poppk/:tbdrugId/edit/:tbdrugPopPkmodelId" component={Authorized(ModelFormEdit, role.PopPK_Edit)} />
      <Route path="/project/:projectId/sample/status" component={Authorized(CollectionSampleCollectionContainer, role.Sample_Allow)} />
      <Route path="/project/:projectId/sample/aliquot" component={Authorized(SampleAliquot, role.Sample_Aliquot_Allow)} />
      <Route path="/project/:projectId/sample/boxes" component={Authorized(SampleBoxesContainer, role.Sample_Box_Allow)} />
      <Route path="/project/:projectId/subject/log/:index" component={Authorized(SubjectLogContainer, role.SubjectLog_Read)} />
      <Route path="/project/:projectId/sample/boxing" component={Authorized(SampleBoxing, role.Sample_Boxing_Allow)} />
      <Route path="/project/:projectId/sample/freezer-in" component={Authorized(FreezerIn, role.Sample_Freezer_In_Allow)} />
      <Route path="/project/:projectId/sample/freezer-out" component={Authorized(FreezerOut, role.Sample_Freezer_Out_Allow)} />
      <Route path="/project/:projectId/sample/out" component={Authorized(SampleOut, role.Sample_Out_Allow)} />
      <Route path="/project/:projectId/sample/freezing" component={Authorized(SampleFreezing, role.Sample_Freezing_Read)} />
      <Route path="/project/:projectId/sample/dashboard" component={Authorized(SampleDashboard, role.Sample_Dashboard_Read)} />
      <Route path="/project/:projectId/tdm/status" component={Authorized(TdmStatus, role.TDMStatus_Read)} />
      <Route path="/project/:projectId/mic/status/:index" component={Authorized(MicStatus, role.MICStatus_Read)} />
    </Switch>
  );
}


export default Routes;