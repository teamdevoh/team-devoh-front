import 'babel-polyfill';
import React from 'react';
import { createStore, compose, applyMiddleware } from 'redux';
import ReactDom from 'react-dom';
import { IntlProvider, FormattedMessage } from 'react-intl';
import 'semantic-ui-css/semantic.min.css';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';
import { Provider } from 'react-redux';
import { logger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './root-reducers';
import reduxCatch from 'redux-catch';
import { App } from './components/app';
import Routes from './routes';
// import registerServiceWorker from './registerServiceWorker';1

import * as serviceWorker from './registerServiceWorker';
import moment from 'moment';
import helpers from './helpers';

import { addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import ko from 'react-intl/locale-data/ko';
import {
  ProvideUser
} from './contexts';
import 'moment/locale/ko';
import { createHashHistory } from 'history';
addLocaleData([...en, ...ko]);
export const history = createHashHistory();

// app messages
import local_en from './locales/en.json';
import local_ko from './locales/ko.json';

const appMessages = {
  en: local_en,
  ko: local_ko
};

const userLanguage = window.localStorage.getItem(helpers.storagekeys.Locale);
const language = userLanguage || 'en'; //navigator.language.split(/[-_]/)[0];
if (process.env.NODE_ENV !== 'production') {
  const whyDidYouRender = require('@welldone-software/why-did-you-render');

  whyDidYouRender(React, {
    onlyLogs: true,
    titleColor: "green",
    diffNameColor: "darkturquoise"
  });
  // System.import('react-addons-perf').then(Perf => (window.Perf = Perf));
}
moment.locale(language);

function configureStore(initialState) {
  const middlewares = [
    reduxCatch(errorHandler),
    // thunk middleware can also accept an extra argument to be passed to each thunk action
    // https://github.com/gaearon/redux-thunk#injecting-a-custom-argument
    thunkMiddleware
  ];

  if (process.env.NODE_ENV === 'development') {
    middlewares.push(logger);
  }

  function errorHandler(error, getState, lastAction, dispatch) {
    console.log(error);
    //dispatch(action.errors(error));
  }

  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middlewares),
      window.devToolsExtension
        ? window.devToolsExtension()
        : f => {
          // add support for Redux dev tools
          return f;
        }
    )
  );

  // if (module.hot) {
  //   // Enable Webpack hot module replacement for reducers
  //   module.hot.accept(rootReducer, () => {
  //     const nextReducer = require(rootReducer).default; // eslint-disable-line global-require
  //     store.replaceReducer(nextReducer);
  //   });
  // }

  return store;
}

export const store = configureStore();

const AppProvider = ({ contexts, children }) =>
  contexts.reduce(
    (prev, context) =>
      React.createElement(context, {
        children: prev
      }),
    children
  );

ReactDom.render(
  <Provider store={store}>
    <IntlProvider locale={language} messages={appMessages[language]}>
      <AppProvider
        contexts={[]}
      >
        <App>
          <Routes />
        </App>
      </AppProvider>
    </IntlProvider>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
