import { useState, useEffect } from 'react';
import ListModel from './ListModel';
import helpers from '../helpers';

const useProjectMemberKeyPair = (
  { projectId, offset, limit, search, holder, projectSiteId },
  deps
) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const model = new ListModel();

  let isSubscribed = true;

  const params = helpers.qs.stringify({
    projectSiteId,
    memberName: search === void 0 ? '' : search,
    offset,
    limit
  });

  model.loading = loading;
  async function fetchUrl() {
    const res = await helpers.Service.get(
      `api/projects/${projectId}/members?${params}`
    );
    const list = [];
    if (holder) {
      list.push({
        key: 0,
        text: holder,
        value: 0,
        image: {}
      });
    }

    for (let i = 0; i < res.data.items.length; i++) {
      const item = res.data.items[i];
      const imageUrl = await helpers.util.getFace(item.myFaceFileStorageId);

      list.push({
        key: item.projectMemberId,
        text: item.memberName,
        value: item.projectMemberId,
        image: { avatar: true, src: imageUrl },
        data: { ...item }
      });
    }

    if (isSubscribed) {
      setData(list);
      setLoading(false);
    }
  }

  useEffect(
    () => {
      fetchUrl();
      return () => (isSubscribed = false);
    },
    [deps]
  );

  if (!loading) {
    model.loading = loading;
    model.items = data;
  }

  return model;
};

export default useProjectMemberKeyPair;
