import { useEffect, useState } from 'react';

const useToKeyPairs = ({ key, text, source = [] }) => {
  const [items, setItems] = useState([]);

  const generate = () => {
    let keyPairs = [];
    source.forEach(item => {
      if (!_.find(keyPairs, { key: item[key] })) {
        keyPairs.push({ key: item[key], value: item[key], text: item[text] });
      }
    });
    setItems(keyPairs);
  };

  useEffect(
    () => {
      if (source && source.length > 0) {
        generate();
      }
    },
    [source]
  );

  return { items };
};

export default useToKeyPairs;
