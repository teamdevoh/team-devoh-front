import { useValidator } from './';

const useSubmit = validCallback => {
  const { validator, showValidationMessage } = useValidator();
  validator.purgeFields();

  const handleValidate = (event, data) => {
    if (validator.allValid()) {
      validCallback();
    } else {
      showValidationMessage(true);
    }
  };

  return [{ validator, onValidate: handleValidate }];
};

export default useSubmit;
