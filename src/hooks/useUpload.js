import { useState } from 'react';
import helpers from '../helpers';
import map from 'lodash/map';

const useUpload = () => {
  const [fakes, setFakes] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const upload = (acceptedFiles, completed) => {
    uploadFiles(acceptedFiles, completed);
  };

  async function uploadFiles(acceptedFiles, completed) {
    try {
      setLoading(true);
      setFakes(acceptedFiles);
      const storageFiles = await helpers.util.upload(
        { files: acceptedFiles },
        percent => {}
      );
      let ids = [];
      map(storageFiles, storageFile => {
        ids.push(storageFile.fileStorageId);
      });
      setFakes([]);
      completed(ids, acceptedFiles);
    } catch (error) {
      setIsError(true);
      setFakes([]);
    } finally {
      setLoading(false);
    }
  }

  return { upload, fakes, loading, isError };
};

export default useUpload;
