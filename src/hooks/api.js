import helpers from '../helpers';

const fetchSites = ({ search, offset, limit }) => {
  const params = helpers.qs.stringify({
    name: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(`api/sites?${params}`);
};

const fetchProjectSite = ({ projectId, name = '' }) => {
  const params = helpers.qs.stringify({
    name
  });
  return helpers.Service.get(`api/projects/${projectId}/sites?${params}`);
};

export default {
  fetchSites,
  fetchProjectSite
};
