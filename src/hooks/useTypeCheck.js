const useTypeCheck = () => {
  const isNumeric = value => {
    return !isNaN(value) && value !== '' && value !== null;
  };

  return { isNumeric };
};

export default useTypeCheck;
