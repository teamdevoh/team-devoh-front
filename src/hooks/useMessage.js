import { useFormatMessage } from '@comparaonline/react-intl-hooks';

const useMessage = () => {
  const t = useFormatMessage();
  return t;
};

export default useMessage;
