import { useEffect, useState } from 'react';
import helpers from '../helpers';
import api from './api';

const useSiteList = () => {
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchSites({ search: '' });
    const sites = helpers.util.withOption('siteId', 'name', res.data.items);
    setItems(sites);
  };

  useEffect(() => {
    fetchItems();
  }, []);

  return [items, setItems];
};

export default useSiteList;
