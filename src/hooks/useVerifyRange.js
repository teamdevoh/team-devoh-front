import { useCallback, useEffect, useState } from 'react';
import { activity } from '../constants';
import { useTypeCheck } from './';

const useVerifyRange = ({ value, min, max }) => {
  const [level, setLevel] = useState('');
  const { isNumeric } = useTypeCheck();

  const DoVerifyRange = () => {
    return min !== null && max !== null;
  };

  const verify = useCallback(
    value => {
      if (isNumeric(value) && DoVerifyRange()) {
        const isLow = min > value;
        const isHigh = max < value;
        const isNormal = !isLow && !isHigh;

        if (isLow) {
          return activity.LabRanges.LOW.name;
        } else if (isHigh) {
          return activity.LabRanges.HIGH.name;
        } else if (isNormal) {
          return activity.LabRanges.NORMAL.name;
        }
      } else {
        return activity.LabRanges.NORMAL.name;
      }
    },
    [value, min, max]
  );

  useEffect(
    () => {
      const result = verify(value);
      setLevel(result);
    },
    [value, min, max]
  );

  return [level];
};

export default useVerifyRange;
