import helpers from '../helpers';
import { useState, useEffect } from 'react';

const useFace = storageId => {
  const [faceUrl, setFaceUrl] = useState();

  useEffect(
    () => {
      fetchFace();
    },
    [storageId]
  );

  const fetchFace = async () => {
    const imageUrl = await helpers.util.getFace(storageId);
    setFaceUrl(imageUrl);
  };

  return [faceUrl, setFaceUrl];
};

export default useFace;
