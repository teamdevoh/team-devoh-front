import { useState, useEffect, useCallback } from 'react';
import { api } from '.';

const useProjectSite = ({ projectId }) => {
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(async () => {
    let options = [];

    const res = await api.fetchProjectSite({ projectId, name: '' });

    for (let i = 0; i < res.data.items.length; i++) {
      const item = res.data.items[i];
      options.push({
        text: item.siteName,
        value: item.projectSiteId,
        prefix: item.subjectPrefix
      });
    }
    setItems(options);
  });

  useEffect(
    () => {
      if (!!projectId) {
        fetchItems();
        return () => {};
      }
    },
    [projectId]
  );

  return [items];
};

export default useProjectSite;
