class ListModel {
  constructor() {
    this._loading = false;
    this._items = [];
    this._paging = new Paging({
      limit: 0,
      offset: 0,
      returned: 0,
      total: 0
    });
  }

  get loading() {
    return this._loading;
  }

  set loading(loading) {
    this._loading = loading;
  }

  get items() {
    return this._items;
  }

  set items(items) {
    this._items = items;
  }

  get paging() {
    return this._paging;
  }

  set paging(paging) {
    this._paging.total = paging.total;
    this._paging.limit = paging.limit;
    this._paging.offset = paging.offset;
    this._paging.returned = paging.returned;
  }
}

class Paging {
  constructor(total = 0, limit, offset, returned) {
    this._total = total;
    this._limit = limit;
    this._offset = offset;
    this._returned = returned;
  }

  get total() {
    return this._total;
  }

  set total(total) {
    this._total = total;
  }

  get limit() {
    return this._limit;
  }

  set limit(limit) {
    this._limit = limit;
  }

  get offset() {
    return this._offset;
  }

  set offset(offset) {
    this._offset = offset;
  }

  get returned() {
    return this._returned;
  }

  set returned(returned) {
    this._returned = returned;
  }
}

export default ListModel;
