import { useState, useEffect } from 'react';
import helpers from '../helpers';

const useFetch = (initialUrl, initialData) => {
  const [url, setUrl] = useState(initialUrl);
  const [data, setData] = useState(initialData);
  const [loading, setLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  let isSubscribed = true;

  async function fetchUrl() {
    try {
      const res = await helpers.Service.get(url);
      if (isSubscribed) {
        setData(res.data);
      }
    } catch (error) {
      if (isSubscribed) {
        setIsError(true);
      }
    } finally {
      if (isSubscribed) {
        setLoading(false);
      }
    }
  }
  useEffect(
    () => {
      fetchUrl();
      return () => (isSubscribed = false);
    },
    [url]
  );
  return [{ data, loading, isError }, setUrl];
};

export default useFetch;
