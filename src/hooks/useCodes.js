import { useState, useEffect, useCallback } from 'react';
import helpers from '../helpers';

const useCodes = ({ codeGroupId }) => {
  if (!codeGroupId) {
    throw new Error('codeGroupId is required');
  }

  const [items, setItems] = useState([]);

  useEffect(() => {
    fetchCodes();
    return () => {};
  }, []);

  const fetchCodes = useCallback(async () => {
    let options = [];
    const codes = helpers.Storage.find(
      `${helpers.storagekeys.CodeGroup}/${codeGroupId}`
    );
    if (codes) {
      setItems(codes);
    } else {
      try {
        const res = await helpers.Service.get(
          `api/codegroups/${codeGroupId}/codes`
        );
        for (let i = 0; i < res.data.length; i++) {
          const item = res.data[i];

          options.push({
            key: item.codeId,
            text: item.name,
            value: item.codeId,
            tag1: item.tag1,
            tag2: item.tag2,
            note: item.note
          });
        }

        helpers.Storage.add(
          `${helpers.storagekeys.CodeGroup}/${codeGroupId}`,
          options
        );
      } catch (error) {
        console.log(error);
      }

      setItems(options);
    }
  }, []);

  return [items];
};

export default useCodes;
