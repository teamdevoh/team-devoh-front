import { useCallback, useState } from 'react';
import omit from 'lodash/omit';
import isEqual from 'lodash/isEqual';
import useDisabledAutoSubmit from './useDisabledAutoSubmit';

const useFormFields = initialState => {
  useDisabledAutoSubmit();
  const [fields, setValues] = useState(initialState);
  const [prevModel, setPrevModel] = useState(null);

  const handleChange = useCallback(
    (event, { name, value }) => {
      if (prevModel === null) {
        setPrevModel(fields);
      }
      const prev = prevModel || fields;
      const next = { ...fields, [name]: value };

      const a = skipProperties(prev);
      const b = skipProperties(next);

      const isChanged = !isEqual(a, b);

      if (fields.hasOwnProperty(name)) {
        setValues({ ...fields, [name]: value, isChanged: isChanged });
      }
    },
    [fields]
  );

  const skipProperties = model => {
    const source = ['isDone', 'reason', 'isChanged'];
    return omit(model, source);
  };

  const resetPrevModel = () => {
    setPrevModel(null);
    setValues({ ...fields, isChanged: false, reason: '' });
  };

  const changeFields = newFields => {
    setPrevModel(fields);

    const isChanged = !isEqual(fields, newFields);

    setValues({ ...newFields, isChanged });
  };

  return [
    { model: fields, onChange: handleChange, resetPrevModel, changeFields },
    setValues
  ];
};

export default useFormFields;
