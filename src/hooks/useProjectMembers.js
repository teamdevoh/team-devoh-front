import { useState, useEffect } from 'react';
import helpers from '../helpers';

const useProjectMembers = (initialUrl, initialData) => {
  const [url, setUrl] = useState(initialUrl);
  const [data, setData] = useState(initialData);
  const [loading, setLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  let isSubscribed = true;

  const fetchUrl = async () => {
    try {
      setLoading(true);
      setIsError(false);
      const res = await helpers.Service.get(url);
      if (isSubscribed) {
        setData(res.data);
      }
    } catch (error) {
      if (isSubscribed) {
        setIsError(true);
      }
    } finally {
      if (isSubscribed) {
        setLoading(false);
      }
    }
  };

  useEffect(
    () => {
      if (url !== '') {
        fetchUrl();
      }
      return () => (isSubscribed = false);
    },
    [url]
  );

  return [{ data, loading, isError }, setUrl];
};

export default useProjectMembers;
