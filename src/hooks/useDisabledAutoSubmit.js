import { useEffect } from 'react';

const useDisabledAutoSubmit = () => {
  const handleKeyDown = (event, data) => {
    const shouldPrevention =
      event.target.type === 'text' &&
      (event.key === 'Enter' || event.keyCode === 13);

    if (shouldPrevention) {
      event.preventDefault();
    }
  };

  useEffect(() => {
    window.addEventListener('keydown', handleKeyDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, []);
};

export default useDisabledAutoSubmit;
