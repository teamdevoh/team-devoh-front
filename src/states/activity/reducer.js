import types from './action-types';
import update from 'immutability-helper';
import findIndex from 'lodash/findIndex';

const initialState = {
  keyValuePairs: [],
  activities: [],
  filter: { name: '-' },
  actives: {
    history: false
  },
  notes: [],
  queries: [],
  hasFile: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.NOTE_LIST:
      return {
        ...state,
        notes: action.payloads
      };
    case types.NOTE_ADD:
      return {
        ...state,
        notes: [action.payload].concat(state.notes)
      };
    case types.NOTE_EDIT:
      return {
        ...state,
        notes: state.notes.map(item => {
          if (item.activityNoteId === action.payload.activityNoteId) {
            return { ...item, ...action.payload };
          } else {
            return { ...item };
          }
        })
      };
    case types.NOTE_REMOVE:
      return {
        ...state,
        notes: state.notes.filter(note => {
          return note.activityNoteId !== action.id;
        })
      };
    case types.QUERY_LIST:
      return {
        ...state,
        queries: action.payloads
      };
    case types.QUERY_ADD:
      return {
        ...state,
        queries: state.queries.concat(action.payload)
      };
    case types.QUERY_EDIT:
      return {
        ...state,
        queries: state.queries.map(item => {
          if (
            item.activityQueryHistoryId ===
            action.payload.activityQueryHistoryId
          ) {
            return { ...item, ...action.payload };
          } else {
            return { ...item };
          }
        })
      };
    case types.QUERY_REMOVE:
      return {
        ...state,
        queries: state.queries.filter(query => {
          return query.activityQueryHistoryId !== action.id;
        })
      };
    case types.SEARCH:
      return {
        ...state,
        filter: { ...state.filter, ...action.filter }
      };
    case types.KEYVALUEPAIRS:
      return {
        ...state,
        keyValuePairs: action.payloads
      };
    case types.ACTIVITIES_OF_GROUP_LiST:
      return {
        ...state,
        activities: action.activities
      };
    case types.UPDATE_STATUS_ACTIVITIES_OF_GROUP:
      const index = findIndex(state.activities, {
        projectActivityId: action.projectActivityId
      });

      const updated = update(state.activities, {
        [index]: {
          activityStatusCodeId: { $set: action.activityStatusCodeId }
        }
      });

      return {
        ...state,
        activities: updated
      };

    case types.UPDATE_STATUS_NOTE_ACTIVITIES_OF_GROUP:
      return {
        ...state,
        activities: state.activities.map(item => {
          if (item.projectActivityId === action.payload.projectActivityId) {
            return { ...item, hasNote: action.payload.hasNote };
          } else {
            return { ...item };
          }
        })
      };
    case types.UPDATE_STATUS_QUERY_ACTIVITIES_OF_GROUP:
      return {
        ...state,
        activities: state.activities.map(item => {
          if (item.projectActivityId === action.payload.projectActivityId) {
            return {
              ...item,
              queryStatusCodeId: action.payload.queryStatusCodeId
            };
          } else {
            return { ...item };
          }
        })
      };
    case types.FILE_STATUS:
      return {
        ...state,
        hasFile: action.payload.hasFile
      };
    default:
      return state;
  }
};

export default reducer;
