import constants from './constants';
import { createSelector } from 'reselect';
import filter from 'lodash/filter';

const notes = createSelector(
  state => state[constants.NAME],
  activity => activity.notes
);

const queries = createSelector(
  state => state[constants.NAME],
  activity => activity.queries
);

const hasFile = createSelector(
  state => state[constants.NAME],
  activity => activity.hasFile
);

const keyValuePairs = createSelector(
  state => state[constants.NAME],
  activity => activity.keyValuePairs
);

const keyValuePairsWithFilter = createSelector(
  keyValuePairs,
  state => state[constants.NAME].filter,
  (keyValuePairs, filters) => {
    const filteredPairs = filter(keyValuePairs, keyValuePair => {
      return filters.name === '-' || filters.name === keyValuePair.value;
    });
    return filteredPairs;
  }
);

const activitiesOfGroup = createSelector(
  state => state[constants.NAME],
  activity => activity.activities
);

export default {
  notes,
  queries,
  hasFile,
  keyValuePairs,
  keyValuePairsWithFilter,
  activitiesOfGroup
};
