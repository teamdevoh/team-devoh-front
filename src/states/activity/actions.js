import types from './action-types';

const filter = filter => {
  return { type: types.SEARCH, filter: filter };
};

const fetchNotes = payloads => {
  return { type: types.NOTE_LIST, payloads: payloads };
};

const addNote = model => {
  return { type: types.NOTE_ADD, payload: model };
};

const editNote = model => {
  return { type: types.NOTE_EDIT, payload: model };
};

const removeNote = id => {
  return { type: types.NOTE_REMOVE, id };
};

const fetchQueries = payloads => {
  return { type: types.QUERY_LIST, payloads: payloads };
};

const addQuery = model => {
  return { type: types.QUERY_ADD, payload: model };
};

const editQuery = model => {
  return { type: types.QUERY_EDIT, payload: model };
};

const removeQuery = id => {
  return { type: types.QUERY_REMOVE, id };
};

const fetchActivityKeyValuePairs = payloads => {
  return { type: types.KEYVALUEPAIRS, payloads: payloads };
};

const fetchActivitiesOfGroup = payloads => {
  return { type: types.ACTIVITIES_OF_GROUP_LiST, activities: payloads };
};

const updateStatusActivitiesOfGroup = (
  projectActivityId,
  activityStatusCodeId
) => {
  return {
    type: types.UPDATE_STATUS_ACTIVITIES_OF_GROUP,
    projectActivityId,
    activityStatusCodeId
  };
};

const updateNoteStatusActivitiesOfGroup = (projectActivityId, hasNote) => {
  return {
    type: types.UPDATE_STATUS_NOTE_ACTIVITIES_OF_GROUP,
    payload: { projectActivityId, hasNote }
  };
};

const updateQueryStatusActivitiesOfGroup = (
  projectActivityId,
  queryStatusCodeId
) => {
  return {
    type: types.UPDATE_STATUS_QUERY_ACTIVITIES_OF_GROUP,
    payload: { projectActivityId, queryStatusCodeId }
  };
};

const updateFileStatus = hasFile => {
  return {
    type: types.FILE_STATUS,
    payload: { hasFile }
  };
};

export default {
  fetchNotes,
  addNote,
  editNote,
  removeNote,
  fetchQueries,
  addQuery,
  editQuery,
  removeQuery,
  filter,
  fetchActivityKeyValuePairs,
  fetchActivitiesOfGroup,
  updateStatusActivitiesOfGroup,
  updateNoteStatusActivitiesOfGroup,
  updateQueryStatusActivitiesOfGroup,
  updateFileStatus
};
