import constants from './constants';
import { createSelector } from 'reselect';

const getSelectedItem = createSelector(
  state => state[constants.NAME],
  poppk => poppk.selected
);

export default {
  getSelectedItem
};
