import types from './action-types';

const selectedItem = model => {
  return { type: types.SELECTED, selected: model };
};

export default {
  selectedItem
};
