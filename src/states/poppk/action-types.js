import constants from './constants';

const SELECTED = `${constants.NAME}/SELECTED`;

export default {
  SELECTED
};
