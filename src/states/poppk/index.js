import types from './action-types';
import actions from './actions';
import constants from './constants';
import reducer from './reducer';
import selectors from './selectors';

export default {
  actions,
  constants,
  reducer,
  types,
  selectors
};
