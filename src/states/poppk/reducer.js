import types from './action-types';

const initialState = {
  selected: { tbdrugId: 0, genericName: '', modelType: 0 }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.SELECTED:
      return {
        ...state,
        selected: action.selected
      };
    default:
      return state;
  }
}
