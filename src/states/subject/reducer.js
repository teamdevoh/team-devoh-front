import types from './action-types';

const initialState = {
  payloads: [],
  crpayloads: [],
  isCRLoading: false,
  filter: {
    projectSiteId: 0,
    subjectStatusCode: 0,
    subjectNo: '',
    queryStatusQuery: '',
    ongoingMonths: [],
    caseConMonths: []
  },
  visible: true,
  current: {
    subjectId: 0,
    projectSiteId: 0,
    subjectNo: '',
    name: '',
    initial: '',
    hospitalNo: '',
    contactNo: '',
    subjectStatusCode: 0,
    dropoutReasonCode: 0,
    remark: ''
  }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CLEAR_LIST:
      return {
        ...state,
        payloads: []
      };
    case types.TOGGLE_LIST:
      return {
        ...state,
        visible: !state.visible
      };
    case types.LIST:
      return {
        ...state,
        payloads: state.payloads.concat(action.payloads)
      };
    case types.SEARCH:
      return {
        ...state,
        filter: { ...state.filter, ...action.filter }
      };
    case types.CLEAR_SEARCH:
      return {
        ...state,
        filter: { ...initialState.filter }
      };
    case types.CURRENT:
      return {
        ...state,
        current: action.payload
      };
    case types.CR_LIST:
      return {
        ...state,
        crpayloads: action.payloads
      };
    case types.IS_CR_LOADING:
      return {
        ...state,
        isCRLoading: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
