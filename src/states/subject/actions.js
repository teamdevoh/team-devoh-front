import types from './action-types';

const clearItems = () => {
  return { type: types.CLEAR_LIST };
};

const fetchItems = payloads => {
  return { type: types.LIST, payloads: payloads };
};

const filter = filter => {
  return { type: types.SEARCH, filter: filter };
};

const clearFilter = () => {
  return { type: types.CLEAR_SEARCH };
};

const toggleSubjectList = () => {
  return { type: types.TOGGLE_LIST };
};

const setCurrentSubject = payload => {
  return { type: types.CURRENT, payload };
};

const fetchCRList = payloads => {
  return { type: types.CR_LIST, payloads: payloads };
};

const isCRLoading = loading => {
  return { type: types.IS_CR_LOADING, payload: loading };
};

export default {
  clearItems,
  fetchItems,
  filter,
  clearFilter,
  toggleSubjectList,
  setCurrentSubject,
  fetchCRList,
  isCRLoading
};
