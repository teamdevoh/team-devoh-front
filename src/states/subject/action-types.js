import constants from './constants';

const SEARCH = `${constants.NAME}/SEARCH`;
const CLEAR_SEARCH = `${constants.NAME}/CLEAR_SEARCH`;
const TOGGLE_LIST = `${constants.NAME}/TOGGLE_LIST`;
const LIST = `${constants.NAME}/LIST`;
const CLEAR_LIST = `${constants.NAME}/CLEAR_LIST`;
const EDIT = `${constants.NAME}/EDIT`;
const REMOVE = `${constants.NAME}/REMOVE`;
const CURRENT = `${constants.NAME}/CURRENT`;
const CR_LIST = `${constants.NAME}/CR_LIST`;
const IS_CR_LOADING = `${constants.NAME}/IS_CR_LOADING`;

export default {
  SEARCH,
  CLEAR_SEARCH,
  TOGGLE_LIST,
  LIST,
  CLEAR_LIST,
  EDIT,
  REMOVE,
  CURRENT,
  CR_LIST,
  IS_CR_LOADING
};
