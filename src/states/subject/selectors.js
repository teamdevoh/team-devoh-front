import constants from './constants';
import { createSelector } from 'reselect';
import filter from 'lodash/filter';

const getCurrent = createSelector(
  state => state[constants.NAME],
  subject => subject.current
);

const getItemsWithFilter = createSelector(
  state => state[constants.NAME],
  subject => {
    return filter(subject.payloads, payload => {
      return payload.subjectNo.indexOf(subject.filter.subjectNo) > -1;
    });
  }
);

const getVisible = createSelector(
  state => state[constants.NAME].visible,
  visible => visible
);

const getCRItemsWithFilter = createSelector(
  state => state[constants.NAME],
  subject => {
    return {
      items: subject.crpayloads,
      loading: subject.isCRLoading
    };
  }
);

export default {
  getCurrent,
  getItemsWithFilter,
  getVisible,
  getCRItemsWithFilter
};
