import types from './action-types';
import helpers from '../../helpers';

const errors = error => {
  return {
    type: types.ERROR,
    error
  };
};

const clearErrors = () => {
  return {
    type: types.ERROR_CLEAR,
    error: {}
  };
};

const isLoading = isLoading => {
  return {
    type: types.LOADING,
    isLoading
  };
};

const fetchFCMToken = (id, token) => {
  return dispatch => {
    const model = {
      id,
      token
    };
    return helpers.Service.put('api/fcm', model).then(res => {});
  };
};

const pushNotify = (tokens, title, body) => {
  return dispatch => {
    const click_action = 'https://spmed-genotype.firebaseapp.com/#/teams';
    const message = {
      registration_ids: tokens,
      notification: {
        title,
        body,
        click_action
      }
    };
    return helpers.Service.post('api/fcm', message).then(res => {});
  };
};

const activeMenu = (key, name) => {
  return { type: types.ACTIVE_MENU, active: { key, name } };
};

const clearActiveMenu = () => {
  return { type: types.ACTIVE_MENU_CLEAR };
};

const visibleSideBar = visible => {
  return { type: types.ACTIVE_SIDEBAR, sidebar: { visible: visible } };
};

const toggleSideBar = () => {
  return { type: types.TOGGLE_SIDEBAR };
};

export default {
  errors,
  clearErrors,
  isLoading,
  fetchFCMToken,
  pushNotify,
  activeMenu,
  clearActiveMenu,
  visibleSideBar,
  toggleSideBar
};
