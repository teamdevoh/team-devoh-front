import constants from './constants';
import projectConstants from '../project/constants';
import projectSelectors from '../project/selectors';
import { createSelector } from 'reselect';
import filter from 'lodash/filter';
import { navigation } from '../../constants';

const getError = state => {
  return state[constants.NAME].error;
};

const getIsLoading = state => {
  return state[constants.NAME].isLoading;
};

const getProjectSelectedItem = createSelector(
  state => projectSelectors.getSelectedItem(state),
  projectSelectedItem => projectSelectedItem
);

const getMenuItems = state => {
  const projectSelectedItem = getProjectSelectedItem(state);
  if (projectSelectedItem.key > 0) {
    return navigation.WorkItem.Project.filter(item => {
      return item.groups;
    });
  } else {
    return navigation.WorkItem.Console.filter(item => {
      return item.groups;
    });
  }
};

const getConsoleItems = () => {
  return navigation.WorkItem.Console;
};

const isMyMenu = (item, variable) => {
  if (
    item.groups.indexOf(navigation.WorkItemGroup.Common) > -1 ||
    item.groups.indexOf(navigation.WorkItemGroup.Console) > -1 ||
    item.groups.indexOf(variable) > -1
  ) {
    return true;
  }
  return false;
};

const setActive = (item, activeId) => {
  if (item.id === activeId) {
    item.active = true;
  } else {
    item.active = false;
  }
};

const routeToOriginal = ({ route, projectId, variable }) => {
  return route
    .replace('{projectId}', projectId)
    .replace('/{variable}', variable ? `/${variable}`.toLowerCase() : '');
};

const getMenuItemsWithFilter = createSelector(
  state => getMenuItems(state),
  state => state[projectConstants.NAME].selected,
  state => state[constants.NAME].activeMenu,
  (items, selectedItem, activeMenu) =>
    filter(items, item => {
      if (isMyMenu(item, selectedItem.variable)) {
        setActive(item, activeMenu.key);

        item.originalRoute = item.route;

        if (item.dependencyProject && selectedItem.key > 0) {
          item.originalRoute = routeToOriginal({
            route: item.route,
            projectId: selectedItem.key,
            variable: selectedItem.variable
          });
        }

        return item;
      }
    })
);

const getSideBar = state => {
  return state[constants.NAME].sidebar;
};

export default {
  getError,
  getIsLoading,
  getMenuItemsWithFilter,
  getConsoleItems,
  getSideBar
};
