import constants from './constants';

const ERROR = `${constants.NAME}/ERROR`;
const ERROR_CLEAR = `${constants.NAME}/ERROR_CLEAR`;
const LOADING = `${constants.NAME}/LOADING`;
const ACTIVE_MENU = `${constants.NAME}/ACTIVE_MENU`;
const ACTIVE_MENU_CLEAR = `${constants.NAME}/ACTIVE_MENU_CLEAR`;
const ACTIVE_SIDEBAR = `${constants.NAME}/ACTIVE_SIDEBAR`;
const TOGGLE_SIDEBAR = `${constants.NAME}/TOGGLE_SIDEBAR`;

export default {
  ERROR,
  ERROR_CLEAR,
  LOADING,
  ACTIVE_MENU,
  ACTIVE_MENU_CLEAR,
  ACTIVE_SIDEBAR,
  TOGGLE_SIDEBAR
};
