import types from './action-types';
import helpers from '../../helpers';

const storageKey = helpers.storagekeys.WorkItem;

const data = helpers.Storage.find(storageKey);
const parseData = JSON.parse(data);

const initActiveMenu = {
  key: '',
  name: ''
};

const initialState = {
  activeMenu: parseData || { ...initActiveMenu },
  sidebar: {
    visible: false
  }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ERROR:
      return {
        ...state,
        error: action.error
      };
    case types.ERROR_CLEAR:
      return {
        ...state,
        error: ''
      };
    case types.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case types.ACTIVE_MENU:
      const data = JSON.stringify({
        key: action.active.key,
        name: action.active.name
      });
      helpers.Storage.add(storageKey, data);
      return {
        ...state,
        activeMenu: action.active
      };
    case types.ACTIVE_MENU_CLEAR:
      helpers.Storage.add(storageKey, JSON.stringify(initActiveMenu));
      return {
        ...state,
        activeMenu: initActiveMenu
      };
    case types.ACTIVE_SIDEBAR:
      return {
        ...state,
        sidebar: { ...state.sidebar, visible: action.sidebar.visible }
      };
    case types.TOGGLE_SIDEBAR:
      return {
        ...state,
        sidebar: { ...state.sidebar, visible: !state.sidebar.visible }
      };
    default:
      return state;
  }
};

export default reducer;
