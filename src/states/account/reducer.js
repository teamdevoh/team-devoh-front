import types from './action-types';
import helpers from '../../helpers';

const initialState = {
  isAuth: helpers.Storage.find(helpers.storagekeys.AccessToken) ? true : false
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case types.SIGN_IN:
      return {
        ...state,
        isAuth: true
      };
    case types.SIGN_OUT:
      return {
        ...state,
        isAuth: false
      };
    default:
      return state;
  }
}
