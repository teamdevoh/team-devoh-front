import constants from './constants';

const getIsAuth = state => {
  return state[constants.NAME].isAuth;
};

const getToken = state => {
  return state[constants.NAME].token;
};

export default {
  getIsAuth,
  getToken
};
