import types from './action-types';
import helpers from '../../helpers';

const signIn = ({ token, refreshToken }) => {
  helpers.Auth.setCredential({
    token: token,
    refreshToken: refreshToken
  });

  return { type: types.SIGN_IN };
};

const signOut = () => {
  helpers.Auth.clearCredential();
  return { type: types.SIGN_OUT };
};

export default {
  signIn,
  signOut
};
