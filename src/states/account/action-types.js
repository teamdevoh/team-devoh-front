import constants from './constants';

const SIGN_IN = `${constants.NAME}/SIGN_IN`;
const PWD_CHANGE = `${constants.NAME}/PWD_CHANGE`;
const SIGN_UP = `${constants.NAME}/SIGN_UP`;
const SIGN_OUT = `${constants.NAME}/SIGN_OUT`;

export default {
  SIGN_IN,

  PWD_CHANGE,
  SIGN_UP,
  SIGN_OUT
};
