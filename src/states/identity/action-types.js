import constants from './constants';

const SET_USER = `${constants.NAME}/SET_USER`;
const UPDATE_PROFILE = `${constants.NAME}/UPDATE_PROFILE`;
const CLEAR = `${constants.NAME}/CLEAR`;

export default {
  SET_USER,
  UPDATE_PROFILE,
  CLEAR
};
