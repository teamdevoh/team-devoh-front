import types from './action-types';
import helpers from '../../helpers';

const setUser = profile => {
  helpers.Identity.setProfile({
    id: profile.memberId,
    name: profile.name,
    loginName: profile.loginName,
    faceId: profile.myFaceFileStorageId,
    roles: profile.roles,
    securityPledge: profile.securityPledge
  });

  return {
    type: types.SET_USER,
    profile: {
      id: profile.memberId,
      name: profile.name,
      loginName: profile.loginName,
      faceId: profile.myFaceFileStorageId,
      securityPledge: profile.securityPledge
    }
  };
};

const updateProfile = payload => {
  helpers.Identity.setProfile({ ...helpers.Identity.profile, ...payload });
  return {
    type: types.UPDATE_PROFILE,
    profile: { ...helpers.Identity.profile, ...payload }
  };
};

const clear = () => {
  return { type: types.CLEAR };
};

export default {
  setUser,
  updateProfile,
  clear
};
