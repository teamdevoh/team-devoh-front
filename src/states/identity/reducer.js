import types from './action-types';
import helpers from '../../helpers';

const initialState = {
  profile: {
    id: helpers.Identity.profile ? helpers.Identity.profile.id : '',
    name: helpers.Identity.profile ? helpers.Identity.profile.name : '',
    loginName: '',
    faceId: helpers.Identity.profile ? helpers.Identity.profile.faceId : 0,
    securityPledge: helpers.Identity.profile
      ? helpers.Identity.profile.securityPledge
      : false
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.SET_USER:
      return {
        ...state,
        profile: action.profile
      };
    case types.UPDATE_PROFILE:
      return {
        ...state,
        profile: action.profile
      };
    case types.CLEAR:
      return {
        ...state,
        profile: {
          id: '',
          name: '',
          loginName: '',
          faceId: 0,
          securityPledge: false
        }
      };
    default:
      return state;
  }
}
