import constants from './constants';

const getProfile = state => state[constants.NAME].profile;

export default {
  getProfile
};
