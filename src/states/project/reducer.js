import types from './action-types';
import update from 'react-addons-update';
import helpers from '../../helpers';
import map from 'lodash/map';
import findIndex from 'lodash/findIndex';
import filter from 'lodash/filter';

const storageKey = helpers.storagekeys.Project;

const data = helpers.Storage.find(storageKey);
const parseData = JSON.parse(data);

const initMyProject = {
  key: 0,
  name: '',
  engName: '',
  role: '',
  periodFrom: '',
  periodTo: '',
  protocolNo: ''
};

const initialState = {
  payloads: [],
  payload: {},
  selected: parseData ? parseData : initMyProject,
  filter: {
    name: ''
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.SELECTED:
      const data = JSON.stringify(action.selected);
      helpers.Storage.add(storageKey, data);
      return {
        ...state,
        selected: action.selected
      };
    case types.SELECTED_CLEAR:
      helpers.Storage.remove(storageKey);
      return {
        ...state,
        selected: initMyProject
      };
    case types.LIST:
      return {
        ...state,
        payloads: action.payloads
      };
    case types.SEARCH:
      return {
        ...state,
        filter: { ...state.filter, ...action.filter }
      };
    case types.EDIT:
      return {
        ...state,
        payloads: map(state.payloads, (group, groupIndex) => {
          let index = findIndex(group.projects, {
            projectId: action.payload.projectId
          });
          if (index > -1) {
            const updatedProjects = update(group.projects, {
              [index]: { $set: action.payload }
            });
            return { ...group, projects: updatedProjects };
          } else {
            return group;
          }
        })
      };
    case types.REMOVE:
      helpers.Storage.remove(storageKey);
      return {
        ...state,
        selected: {
          key: 0,
          name: '',
          engName: '',
          role: '',
          periodFrom: '',
          periodTo: '',
          protocolNo: ''
        },
        payloads: map(state.payloads, (group, groupIndex) => {
          let projects = filter(group.projects, project => {
            return Number(project.projectId) !== Number(action.id);
          });
          return { ...group, projects };
        })
      };
    default:
      return state;
  }
}
