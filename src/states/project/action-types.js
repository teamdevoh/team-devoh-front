import constants from './constants';

const SELECTED = `${constants.NAME}/SELECTED`;
const SELECTED_CLEAR = `${constants.NAME}/SELECTED_CLEAR`;
const LIST = `${constants.NAME}/LIST`;
const SEARCH = `${constants.NAME}/SEARCH`;
const EDIT = `${constants.NAME}/EDIT`;
const REMOVE = `${constants.NAME}/REMOVE`;

export default {
  SELECTED,
  SELECTED_CLEAR,
  LIST,
  SEARCH,
  EDIT,
  REMOVE
};
