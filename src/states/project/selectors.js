import constants from './constants';
import { createSelector } from 'reselect';
import map from 'lodash/map';
import filter from 'lodash/filter';

const getSelectedItem = createSelector(
  state => state[constants.NAME],
  project => {
    if (project.selected.key > 0) {
      return project.selected;
    } else {
      return {
        key: 0,
        name: '',
        engName: '',
        role: '',
        periodFrom: '',
        periodTo: '',
        protocolNo: ''
      };
    }
  }
);

const getFilter = createSelector(
  state => state[constants.NAME],
  project => project.filter
);

const getItems = createSelector(
  state => state[constants.NAME],
  project => project.payloads
);

const getItemsWithFilter = createSelector(
  getItems,
  state => state[constants.NAME].filter,
  (payloads, filters) =>
    map(payloads, (group, groupIndex) => {
      const projects = filter(group.projects, project => {
        return (
          project.title.toLowerCase().indexOf(filters.name.toLowerCase()) > -1
        );
      });
      return { ...group, projects };
    })
);

export default {
  getSelectedItem,
  getFilter,
  getItems,
  getItemsWithFilter
};
