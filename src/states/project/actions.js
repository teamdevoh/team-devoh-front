import types from './action-types';

const selectedItem = model => {
  return { type: types.SELECTED, selected: model };
};

const clearSelectedItem = () => {
  return { type: types.SELECTED_CLEAR };
};

const fetchItems = payloads => {
  return { type: types.LIST, payloads: payloads };
};

const filter = filter => {
  return { type: types.SEARCH, filter: filter };
};

const edit = model => {
  return { type: types.EDIT, payload: model };
};

const remove = projectId => {
  return { type: types.REMOVE, id: projectId };
};

export default {
  selectedItem,
  clearSelectedItem,
  fetchItems,
  filter,
  edit,
  remove
};
