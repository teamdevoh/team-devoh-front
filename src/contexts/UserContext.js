// import React, { useState, useContext, createContext } from 'react';
// import helpers from '../helpers';

// const defaultValue = {
//   id: 0,
//   name: '',
//   loginName: '',
//   faceId: 0,
//   add: () => {},
//   clear: () => {},
//   setFace: (faceId = 0) => {}
// };

// const userContext = createContext(defaultValue);

// export const ProvideUser = ({ children }) => {
//   const user = useProvideUser();
//   return <userContext.Provider value={user}>{children}</userContext.Provider>;
// };

// export const useUser = () => {
//   return useContext(userContext);
// };

// const useProvideUser = () => {
//   const [user, setUser] = useState({
//     id: helpers.Identity.profile ? helpers.Identity.profile.id : '',
//     name: helpers.Identity.profile ? helpers.Identity.profile.name : '',
//     faceId: helpers.Identity.profile ? helpers.Identity.profile.faceId : 0
//   });

//   const add = profile => {
//     helpers.Identity.setProfile({
//       id: profile.memberId,
//       name: profile.name,
//       loginName: profile.loginName,
//       faceId: profile.myFaceFileStorageId,
//       roles: profile.roles
//     });

//     setUser({
//       id: profile.memberId,
//       name: profile.name,
//       loginName: profile.loginName,
//       faceId: profile.myFaceFileStorageId
//     });
//   };

//   const clear = () => {
//     setUser({
//       id: '',
//       name: '',
//       loginName: '',
//       faceId: 0
//     });
//   };

//   const setFace = faceId => {
//     setUser({ ...user, faceId: faceId });
//   };

//   return {
//     ...user,
//     add,
//     clear,
//     setFace
//   };
// };
