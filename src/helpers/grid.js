const rowSelectedAll = (source, keyname, checked) => {
  let selections = [];
  if (checked) {
    source.forEach(payload => {
      selections.push(payload[keyname]);
    });
  }

  return selections;
};

const rowSelected = (dest, key, checked) => {
  const selections = dest;
  if (checked) {
    selections.push(key);
  } else {
    selections.splice(dest.indexOf(key), 1);
  }

  return selections;
};

export default {
  rowSelected,
  rowSelectedAll
};
