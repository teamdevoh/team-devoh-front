import helpers from '.';
import moment from 'moment';
import { format } from '../constants';

const setFileInfo = async ({
  extension,
  id,
  originalName,
  size,
  created,
  creator,
  thumbnailApiUrl,
  tag
}) => {
  return {
    key: id,
    name: originalName,
    type: extension,
    size: helpers.util.formatFileSize(size),
    created: moment(created, format.YYYYMMDDHHMM).fromNow(),
    creator: creator,
    imageUrl: thumbnailApiUrl,
    tags: tag
  };
};

const syncSetFileInfo = ({
  extension,
  id,
  originalName,
  size,
  created,
  creator,
  thumbnailApiUrl,
  tag
}) => {
  return {
    key: id,
    name: originalName,
    type: extension,
    size: helpers.util.formatFileSize(size),
    created: moment(created, format.YYYYMMDDHHMM).fromNow(),
    creator: creator,
    imageUrl: thumbnailApiUrl,
    tags: tag
  };
};

export default { setFileInfo, syncSetFileInfo };
