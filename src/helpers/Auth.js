import Storage from './Storage';
import Identity from './Identity';

class Auth {
  set jwt(value) {
    Storage.add('access_token', `bearer ${value}`);
  }

  get jwt() {
    return Storage.find('access_token');
  }

  get is() {
    return Storage.find('access_token') ? true : false;
  }

  clearJwt() {
    Storage.remove('access_token');
  }

  set refreshToken(value) {
    Storage.add('refresh_token', value);
  }

  get refreshToken() {
    return Storage.find('refresh_token');
  }

  clearRefreshToken() {
    Storage.remove('refresh_token');
  }

  clearUserAction() {
    Storage.remove('project');
    Storage.remove('workItem');
  }

  setCredential = ({ token, refreshToken }) => {
    this.refreshToken = refreshToken;
    this.jwt = token;
  };

  clearCredential() {
    this.clearJwt();
    this.clearRefreshToken();
    this.clearUserAction();

    Identity.clear();
  }
}

export default new Auth();
