const AccessToken = 'access_token';
const AccessTokenExternal = 'access_token_external';
const Avatar = 'avatar';
const CodeGroup = 'codegroup';
const Locale = 'locale';
const Project = 'project';
const WorkItem = 'workItem';
const MyLastLocation = 'MyLastLocation';
const HomePG = 'HomePG';

export default {
  AccessToken,
  AccessTokenExternal,
  Avatar,
  CodeGroup,
  Locale,
  Project,
  WorkItem,
  MyLastLocation,
  HomePG
};
