import qs from 'qs';

/**
 * usage)
 *
 * const obj = helpers.qs.parse('a=c');
 *
 * result : { a: 'c' }
 *
 * https://github.com/ljharb/qs
 * @param {String} query
 * @param {object} options
 */
const parse = (query, options) => {
  return qs.parse(query, options ? options : { ignoreQueryPrefix: true });
};

/**
 * usage)
 *
 * const str = helpers.qs.stringify({ a: 'c' });
 *
 * result : 'a=c'
 *
 * https://github.com/ljharb/qs
 * @param {object} obj
 * @param {object} options
 */
const stringify = (obj, options) => {
  return qs.stringify(obj, options ? options : { indices: false });
};

export default {
  parse,
  stringify
};
