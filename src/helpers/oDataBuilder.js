/**
 * field: [value]
 * @param {String} field
 * @param {String} value
 */
const any = (field, value) => {
  return `${field}/any(a: a eq '${value}')`;
  // soc/any(a: a eq '10029205')
};

/**
 * field: [ { property : value } ]
 * @param {String} field
 * @param {String} value
 * @param {String} property
 */
const anyDeep = (field, value, property) => {
  // gt: Greater than
  return `${field}/any(a: a/${property} eq '${value}')`;
  // soc/any(a: a eq '10029205')
};

const between = (field, value1, value2) => {
  // ge : Greater than or equal
  // le : Less than or equal
  return `${field} ge ${value1} and ${field} le ${value2}`;
};

/**
 * contains
 * @param {String} field
 * @param {String} value
 */
const contains = (field, value) => {
  return `contains(${field}, '${value}')`;
};

/**
 * Equals
 * @param {string} field
 * @param {string|number} value
 */
const equal = (field, value) => {
  return `${field} eq ${value}`;
};

/**
 * Not Equals
 * @param {string} field
 * @param {string|number} value
 */
const notEqual = (field, value) => {
  return `${field} ne ${value}`;
};

const equalOrequal = (field, values) => {
  let query = '';

  if (Array.isArray(values)) {
    query = `${field} eq ${values[0]}`;

    for (let i = 1; i < values.length; i++) {
      query += ` or ${field} eq ${values[i]}`;
    }
  } else {
    query = `${field} eq ${values}`;
  }

  return query;
};

const greaterThan = (field, value) => {
  // gt: Greater than
  return `${field} gt ${value}`;
};

export default {
  any,
  anyDeep,
  between,
  contains,
  equal,
  equalOrequal,
  greaterThan,
  notEqual
};
