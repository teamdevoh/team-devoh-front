import axios from 'axios';
import alert from '../components/modal/notification';
import helpers from '../helpers';

const apiUrl = __API_URL__;

class Service {
  constructor() {
    const service = axios.create({
      baseURL: apiUrl
    });

    service.interceptors.request.use(
      function(config) {
        config.headers.common['Authorization'] = null;
        delete config.headers.common['Authorization'];

        const AUTH_TOKEN = helpers.Auth.jwt;
        if (AUTH_TOKEN) {
          // Alter defaults after instance has been created
          config.headers.common['Authorization'] = AUTH_TOKEN;
        }

        const lang =
          window.localStorage.getItem(helpers.storagekeys.Locale) ||
          navigator.language.split(/[-_]/)[0];
        switch (lang) {
          case 'ko':
            config.headers.common['Accept-language'] = 'ko-KR';
            break;
          default:
            config.headers.common['Accept-language'] = 'en-US';
            break;
        }

        return config;
      },
      function(error) {
        return Promise.reject(error);
      }
    );

    service.interceptors.response.use(
      function(response) {
        return response;
      },
      function(error) {
        if (!error.response) {
          window.location = '#/error/network';
        } else {
          switch (error.response.status) {
            case 400:
              const { modelState } = error.response.data;

              const contents = [];
              for (let modelKey in modelState) {
                if (modelState.hasOwnProperty(modelKey)) {
                  contents.push(modelState[modelKey]);
                }
              }

              alert.error({
                title: error.response.data.message,
                text: contents.join('<br />')
              });
              break;
            case 401:
              if (error.response.data && window.location.pathname === '/') {
                // alert.warning({ title: error.response.data.message });
              } else {
                helpers.Auth.clearCredential();
                alert.error({ title: 'expired token' });
                window.location = '/';
              }
              break;
            case 500:
              console.error(error.response.data.message);
              alert.error({ text: error.response.data.message });
              break;
            default:
              break;
          }
        }

        return Promise.reject(error.response);
      }
    );

    this.service = service;
  }

  get(path, config, callback, failed, reject = f => f) {
    return this.service
      .get(path, { responseType: 'json', ...config })
      .catch(err => {
        console.error(err.data.message);
        alert.error({ title: err.data.message });
        reject(err);
      });
  }

  patch(path, payload) {
    return this.service
      .patch(path, payload, { responseType: 'json' })
      .catch(err => {
        console.error(err.data.message);
        alert.error({ title: err.data.message });
      });
  }

  put(path, payload) {
    return this.service
      .put(path, payload, { responseType: 'json' })
      .catch(err => {
        console.error(err.data.message);
      });
  }

  post(path, payload) {
    return this.service
      .post(path, payload, { responseType: 'json' })
      .catch(err => {
        console.error(err.data.message);
        return Promise.reject(err);
      });
  }

  delete(path, payload) {
    return this.service
      .delete(path, payload, { responseType: 'json' })
      .catch(err => {
        console.error(err.data.message);
        alert.error({ title: err.data.message });
      });
  }

  upload(path, payload, onUploadProgress) {
    const formData = new FormData();

    const key = payload.key;
    const files = payload.files;

    for (let i = 0; i < files.length; i++) {
      formData.append(key, files[i]);
    }

    return this.service
      .request({
        method: 'POST',
        url: path,
        responseType: 'json',
        data: formData,
        contentType: 'multipart/form-data',
        onUploadProgress: event => {
          onUploadProgress(~~((event.loaded / event.total) * 100));
        }
      })
      .catch(err => {
        console.error(err.data.message);
        throw new Error(err.data.message);
      });
  }

  downloadBlob(path) {
    return this.service
      .get(path, {
        responseType: 'blob'
      })
      .catch(err => {
        console.error(err.data.message);
      });
  }

  download(path, onDownloadProgress) {
    return this.service
      .get(path, {
        responseType: 'arraybuffer',
        onDownloadProgress(event) {
          onDownloadProgress(~~((event.loaded / event.total) * 100));
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  }

  refreshToken(projectId) {
    return this.service
      .request({
        method: 'POST',
        url: 'api/token/refresh',
        responseType: 'json',
        data: {
          token: helpers.Auth.jwt,
          refreshToken: helpers.Auth.refreshToken,
          projectId: projectId
        }
      })
      .then(res => {
        helpers.Auth.clearJwt();
        helpers.Auth.setCredential({
          token: res.data.newTokens.token,
          refreshToken: res.data.newTokens.refreshToken
        });
        helpers.Identity.setRole(res.data.profile.roles);

        return true;
      })
      .catch(err => {
        console.error(err.data.message);
      });
  }
}

export default new Service();
