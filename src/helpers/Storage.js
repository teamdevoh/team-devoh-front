class Storage {
  add(key, data) {
    sessionStorage.setItem(key, JSON.stringify(data));
  }

  find(key) {
    return JSON.parse(sessionStorage.getItem(key));
  }

  remove(key) {
    return sessionStorage.removeItem(key);
  }
}

export default new Storage();
