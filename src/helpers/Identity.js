import Storage from './Storage';
import some from 'lodash/some';

class Identity {
  setProfile = ({ id, name, loginName, faceId, roles, securityPledge }) => {
    Storage.add(
      'profile',
      JSON.stringify({
        id: id,
        name: name,
        loginName: loginName,
        faceId: faceId,
        roles: roles,
        securityPledge
      })
    );
  };

  get profile() {
    const profile = JSON.parse(Storage.find('profile'));
    if (!profile) {
      return null;
    }

    return {
      id: profile.id,
      name: profile.name,
      loginName: profile.loginName,
      faceId: profile.faceId,
      roles: profile.roles,
      securityPledge: profile.securityPledge
    };
  }

  setRole(roles) {
    let currentProfile = this.profile;
    this.clear();
    this.setProfile({
      ...currentProfile,
      roles
    });
  }

  clear() {
    Storage.remove('profile');
  }

  hasRole(allowedRole) {
    const profile = JSON.parse(Storage.find('profile'));
    if (!profile) {
      return null;
    }

    const roles = profile.roles;
    const allowedRoles = allowedRole.split(',');

    return some(allowedRoles, allowedRole => {
      return roles.indexOf(allowedRole) > -1;
    });
  }
}

export default new Identity();
