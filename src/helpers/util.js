import { orderBy } from 'lodash';
import moment from 'moment';
import Service from './Service';
import storagekeys from './storagekeys';
import 'moment-timezone';
import Showdown from 'showdown';
import file from './file';
import json2csv from 'json2csv';
import watermark from 'watermarkjs';
import { format } from '../constants';
import { saveAs } from 'file-saver';
import TableToExcel from '@linways/table-to-excel';

let urls = new Map();

const isMobile = () => {
  if (
    navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i)
  ) {
    return true;
  }
  return false;
};

/**
 * usage)
 *
 * helpers.util.sort(items, 'id', 'asc')
 *
 * Sort by items in ascending order
 *
 * asc, desc
 *
 * https://lodash.com/docs#orderBy
 * @param {Array|Object} source
 * @param {string} column
 * @param {string} direction
 */
const sort = (source, column, direction) => {
  return direction && direction !== 'NONE'
    ? orderBy(
        source,
        [
          data =>
            data[column] === null ? '' : String(data[column]).toLowerCase()
        ],
        [direction.toLowerCase()]
      )
    : source;
};

/**
 * usage)
 *
 * helpers.util.dateformat(new Date() , 'YYYY-MM-DD HH:mm')
 *
 * Parse, validate, manipulate, and display dates and times in JavaScript.
 *
 * https://momentjs.com/
 * @param {String|Date} date
 * @param {String} f
 */
const dateformat = (date, f) => {
  if (verifyDate(date)) {
    return moment(date).format(f || format.YYYY_MM_DD_HHMM);
  } else {
    return '';
  }
};

const dateRange = (from, to, format) => {
  const fromDate = dateformat(from, format);
  const toDate = dateformat(to, format);

  if (fromDate) {
    return `${fromDate} ~ ${toDate}`;
  } else {
    return '';
  }
};

const verifyDate = date => {
  return moment(date).isValid();
};

/**
 * usage)
 *
 * helpers.util.addMonth('2019-01-01', 1)
 * return : 2019-02-01
 * https://momentjs.com/
 * @param {String|Date} from
 * @param {String|Date} to
 */
const addMonth = (date, amount) => {
  if (verifyDate(date)) {
    return moment(date).add(amount, 'M');
  }
  return null;
};

/**
 * usage)
 *
 * helpers.util.firstDayOfMonth('2019-01-10')
 * return : 2019-01-01
 * https://momentjs.com/
 * @param {String|Date} from
 * @param {String|Date} to
 */
const firstDayOfMonth = date => {
  if (verifyDate(date)) {
    return moment(date).startOf('month');
  }
  return null;
};

/**
 * usage)
 *
 * helpers.util.firstDayOfMonth('2019-01-10')
 * return : 2019-01-31
 * https://momentjs.com/
 * @param {String|Date} from
 * @param {String|Date} to
 */
const lastDayOfMonth = date => {
  if (verifyDate(date)) {
    return moment(date).endOf('month');
  }
  return null;
};

/**
 * usage)
 *
 * helpers.util.diffMinutes('2019-01-01', '2019-01-03')
 * return : 4320
 * https://momentjs.com/
 * @param {String|Date} from
 * @param {String|Date} to
 */
const diffMinutes = (from, to) => {
  if (verifyDate(from) && verifyDate(to)) {
    return moment(to).diff(moment(from), 'minutes');
  }
  return null;
};

/**
 * usage)
 *
 * helpers.util.diffDays('2019-01-01', '2019-01-03')
 * return : 3
 * https://momentjs.com/
 * @param {String|Date} from
 * @param {String|Date} to
 */
const diffDays = (from, to) => {
  if (verifyDate(from) && verifyDate(to)) {
    return moment(to).diff(moment(from), 'days') + 1;
  }
  return null;
};

/**
 * usage)
 *
 * helpers.util.diffMonths('2019-01-01', '2019-02-03')
 * return : 2
 * https://momentjs.com/
 * @param {String|Date} from
 * @param {String|Date} to
 */
const diffMonths = (from, to) => {
  if (verifyDate(from) && verifyDate(to)) {
    return moment(to).diff(moment(from), 'months') + 1;
  }
  return null;
};

/**
 * usage)
 *
 * helpers.util.diffYears('2000-01-01', '2001-02-01')
 * return : 1
 * https://momentjs.com/
 * @param {String|Date} from
 * @param {String|Date} to
 */
const diffYears = (from, to) => {
  if (verifyDate(from) && verifyDate(to)) {
    return moment(to).diff(moment(from), 'years');
  }
  return null;
};

/**
 * usage)
 *
 * utcToLocal("2019-11-14 07:15:37", "Asia/Kolkata")
 * @param {*} utcdateTime
 * @param {*} tz
 */
const utcToLocal = (utcdateTime, tz) => {
  const localTimeZone = moment.tz.guess();
  var zone = moment.tz(tz ? tz : localTimeZone).format('Z'); // Actual zone value e:g +5:30
  var zoneValue = zone.replace(/[^0-9: ]/g, ''); // Zone value without + - chars
  var operator =
    zone && zone.split('') && zone.split('')[0] === '-' ? '-' : '+'; // operator for addition subtraction
  var localDateTime;
  var hours = zoneValue.split(':')[0];
  var minutes = zoneValue.split(':')[1];
  if (operator === '-') {
    localDateTime = moment(utcdateTime)
      .subtract(hours, 'hours')
      .subtract(minutes, 'minutes')
      .format(format.YYYY_MM_DD_HHMMSS);
  } else if (operator) {
    localDateTime = moment(utcdateTime)
      .add(hours, 'hours')
      .add(minutes, 'minutes')
      .format(format.YYYY_MM_DD_HHMMSS);
  } else {
    localDateTime = 'Invalid Timezone Operator';
  }
  return localDateTime;
};

const getLocale = () => {
  const lang =
    window.localStorage.getItem(storagekeys.Locale) ||
    navigator.language.split(/[-_]/)[0];
  return lang;
};

const upload = (payload, onUploadProgress = f => f) => {
  payload.key = 'files';
  return Service.upload('api/files', payload, percent => {
    onUploadProgress(percent);
  }).then(res => {
    return res.data;
  });
};

const download = (
  path,
  onDownloadProgress = f => f,
  resolve = f => f,
  reject = f => f
) => {
  let windowReference = null;
  if (isMobile()) {
    windowReference = window.open();
  }

  return Service.download(path, percent => {
    onDownloadProgress(percent);
  })
    .then(res => {
      const octetStreamMime = 'application/octet-stream';
      const fileName = res.headers['x-filename'] || 'download.bin';
      const contentType = res.headers['content-type'] || octetStreamMime;

      const blob = new Blob([res.data], { type: contentType });
      if (navigator.msSaveBlob) {
        // IE 10+
        navigator.msSaveBlob(blob, fileName);
      } else {
        const url = URL.createObjectURL(blob);

        if (isMobile()) {
          if (windowReference !== null) {
            windowReference.location = url;
          }
        } else {
          const link = document.createElement('a');
          if (link.download !== undefined) {
            // feature detection
            // Browsers that support HTML5 download attribute
            link.setAttribute('href', url);
            link.setAttribute('target', '_blank');
            link.setAttribute(
              'download',
              decodeURIComponent(fileName).replace(/\+/g, ' ')
            );
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            window.URL.revokeObjectURL(url);
          }
        }
      }

      resolve(res);

      return res.data;
    })
    .catch(err => {
      reject(err);
      return Promise.reject(err);
    });
};

const getSignedUrlByName = fileStorageName => {
  return Service.get(`api/files/${fileStorageName}/signedUrl`).then(
    response => {
      return response.data.signedUrl;
    }
  );
};

const previewImage = (path, fileStorageId) => {
  return Service.downloadBlob(path).then(res => {
    if (res.data.size > 0) {
      const url = URL.createObjectURL(res.data);
      return url;
    } else {
      return 'images/empty.png';
    }
  });
};

const getBlobWithSetMap = (path, key) => {
  if (urls.has(key)) {
    return new Promise(resolve => {
      resolve(urls.get(key));
    });
  } else {
    return Service.downloadBlob(path).then(res => {
      if (res.data.size > 0) {
        const url = URL.createObjectURL(res.data);
        urls.set(key, url);
        return url;
      } else {
        return '';
      }
    });
  }
};

const imageTypes =
  'image/png,image/jpg,image/jpeg,image/bmp,image/gif,image/tiff';

const isImage = extension => {
  if (
    extension.indexOf('image/png') > -1 ||
    extension.indexOf('image/jpg') > -1 ||
    extension.indexOf('image/jpeg') > -1 ||
    extension.indexOf('image/bmp') > -1 ||
    extension.indexOf('image/gif') > -1 ||
    extension.indexOf('image/tiff') > -1
  ) {
    return true;
  }
  return false;
};

/**
 * usage)
 *
 * helpers.util.getFace(1)
 *
 * Get blob url by thumbnail image
 * @param {number} fileStorageId
 */
const getFace = async (fileStorageId, size) => {
  if (fileStorageId > 0) {
    const faceUrl = await getBlobWithSetMap(
      `api/members/face.thumbnail/${fileStorageId}?size=${size || 25}`,
      `${fileStorageId}_${size || 25}`
    );
    if (faceUrl) {
      return new Promise(resolve => {
        resolve(faceUrl);
      });
    } else {
      return new Promise(resolve => {
        resolve('images/user.png');
      });
    }
  }
  return new Promise(resolve => {
    resolve('images/user.png');
  });
};

/**
 * usage)
 *
 * formatBytes(2000);       // 2 KB
 *
 * formatBytes(2234);       // 2.23 KB
 *
 * formatBytes(2234, 3);    // 2.234 KB
 *
 * Convert File Size in Bytes to KB, MB, GB ...
 * @param {number} bytes
 * @param {number} decimalPoint
 */
const formatFileSize = (bytes, decimalPoint) => {
  if (bytes === 0) return '0 Bytes';
  var k = 1000,
    dm = decimalPoint || 2,
    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
    i = Math.floor(Math.log(bytes) / Math.log(k));
  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))}${sizes[i]}`;
};

const jsonToString = data => {
  return JSON.stringify(data);
};

const stringToJson = data => {
  return JSON.parse(data);
};

const withOption = (keyField, textField, source) => {
  let options = [];
  if (source && source.length > 0) {
    source.forEach(item => {
      options.push({ text: item[textField], value: item[keyField] });
    });
    return options;
  }
  return [];
};

const markdownToHtml = (option = {}) => {
  return new Showdown.Converter({
    tables: true,
    simplifiedAutoLink: true,
    strikethrough: true,
    tasklists: true,
    ...option
  });
};

const generatePostFileInfo = files => {
  const result = [];

  for (let i = 0; i < files.length; i++) {
    const curFile = files[i];
    const { postAttachmentId } = curFile;

    result.push(file.syncSetFileInfo({ id: postAttachmentId, ...curFile }));
  }
  return result;
};

/**
 * usage)
 *
 * await sleep(1000); // timer 1 second
 *
 * @param {number} milliseconds
 */
const sleep = milliseconds => {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
};

/**
 * usage)
 *
 * calcPercentage(256, 5000);
 * return : 5.1
 *
 * @param {number} dividee
 * @param {number} divider
 */
const calcPercentage = (dividee, divider) => {
  return ((dividee / divider) * 100).toFixed(1);
};

const exportCSV = ({ fileName, fields, fieldNames, data }) => {
  const fieldKeys =
    fields &&
    fields.map((column, index) => {
      fields[index]['label'] = fieldNames[index];
      return column.key;
    });

  if (fieldNames && fieldKeys.length !== fieldNames.length) {
    throw new Error(
      'fieldNames and fields should be of the same length, if fieldNames is provided.'
    );
  }

  if (typeof fields[0].value !== 'function') {
    fields = fieldKeys;
  }

  const opts = {
    data,
    fields: fields,
    fieldNames: fieldNames,
    quotes: '',
    withBOM: true
  };

  const csv = json2csv(opts);

  const blob = new Blob([csv], {
    type: 'text/csv;charset=utf-8;'
  });
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, fileName);
  } else {
    const link = document.createElement('a');
    if (link.download !== undefined) {
      // feature detection
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
};

const getOrdinal = n => {
  var s = ['th', 'st', 'nd', 'rd'],
    v = n % 100;
  return n + (s[(v - 20) % 10] || s[v] || s[0]);
};

const watermarkLogo = (imgUrl, cb, posX, posY, logoWidth, logoHeight) => {
  watermark([imgUrl, './images/brandCI.png'])
    .image((uploadImage, logo) => {
      const context = uploadImage.getContext('2d');
      context.save();

      context.globalAlpha = 0.4;
      context.drawImage(
        logo,
        posX || 10,
        posY || 10,
        logoWidth || 350,
        logoHeight || 100
      );

      context.restore();
      return uploadImage;
    })
    .then(cb);
};

const exportExcel = async ({
  sheetName,
  tableId,
  fileName,
  wsStyleFunction = f => f
}) => {
  try {
    const tableEl = document.getElementById(tableId);
    const wb = TableToExcel.tableToBook(tableEl, {
      sheet: {
        name: sheetName
      }
    });

    wb.worksheets[0].model.rows.forEach(row => {
      row.cells.forEach(cell => {
        cell.style.border = {
          top: { style: 'thin', color: { argb: '000000' } },
          left: { style: 'thin', color: { argb: '000000' } },
          bottom: { style: 'thin', color: { argb: '000000' } },
          right: { style: 'thin', color: { argb: '000000' } }
        };
      });
    });

    for (let i = 0; i < tableEl.tHead.rows.length; i++) {
      wb.worksheets[0].getRow(i + 1).height = 25;
      wb.worksheets[0].model.rows[i].cells.forEach(cell => {
        cell.style.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'D8D8D8' }
        };
        cell.style.alignment = { vertical: 'middle', horizontal: 'center' };
      });
    }

    wsStyleFunction(wb.worksheets[0]);

    const buf = await wb.xlsx.writeBuffer();

    saveAs(
      new Blob([buf], {
        type:
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      }),
      `${fileName}.xlsx`
    );
  } catch (err) {
    console.log(err);
  }
};

const compareString = (a, b) => {
  a = a || '';
  b = b || '';
  if (a < b) {
    return -1;
  }
  if (a > b) {
    return 1;
  }

  return 0;
};

export default {
  isMobile,
  sort,
  verifyDate,
  dateformat,
  dateRange,
  addMonth,
  firstDayOfMonth,
  lastDayOfMonth,
  diffMinutes,
  diffDays,
  diffMonths,
  diffYears,
  utcToLocal,
  upload,
  download,
  getSignedUrlByName,
  previewImage,
  getLocale,
  imageTypes,
  isImage,
  getFace,
  formatFileSize,
  jsonToString,
  stringToJson,
  withOption,
  markdownToHtml,
  generatePostFileInfo,
  sleep,
  calcPercentage,
  exportCSV,
  getOrdinal,
  watermarkLogo,
  exportExcel,
  compareString
};
