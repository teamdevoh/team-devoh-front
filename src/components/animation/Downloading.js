import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './download.json';

const Downloading = onlyUpdateForKeys([])(() => {
  const options = {
    loop: false,
    animationData
  };
  return (
    <div style={{ cursor: 'pointer' }}>
      <Animation options={options} height={50} width={50} />
    </div>
  );
});

export default Downloading;
