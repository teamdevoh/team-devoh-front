import React, { PureComponent } from 'react';
import Animation from './Animation';
import animationData from './close.json';
import { fromJS } from 'immutable';
import styled from 'styled-components';

const Container = styled.div`
  cursor: pointer;
  position: absolute;
  right: 0;
  top: 0;
  z-index: 1;
`;

export default class Close extends PureComponent {
  constructor(props) {
    super(props);

    this.data = fromJS(animationData);
    this.options = {
      loop: false,
      animationData
    };
  }

  componentWillUnmount() {
    this.refs.animation.defaultOptions.animationData = null;
  }

  render() {
    const { onClick } = this.props;
    return (
      <Container onClick={onClick}>
        <Animation
          ref="animation"
          options={this.options}
          height={80}
          width={80}
        />
      </Container>
    );
  }
}
