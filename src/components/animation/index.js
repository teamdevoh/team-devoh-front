import Loadable from 'react-loadable';
import { Loading } from '../loading';

const Close = Loadable({
  loader: () => import('./Close'),
  loading: Loading
});
const Copy = Loadable({
  loader: () => import('./Copy'),
  loading: Loading
});
const Done = Loadable({
  loader: () => import('./Done'),
  loading: Loading
});
const Downloading = Loadable({
  loader: () => import('./Downloading'),
  loading: Loading
});
const LoadingBall = Loadable({
  loader: () => import('./LoadingBall'),
  loading: Loading
});
const Logo = Loadable({
  loader: () => import('./Logo'),
  loading: Loading
});
const NetworkError = Loadable({
  loader: () => import('./NetworkError'),
  loading: Loading
});
const Uploading = Loadable({
  loader: () => import('./Uploading'),
  loading: Loading
});
const Pencil = Loadable({
  loader: () => import('./Pencil'),
  loading: Loading
});
const TaskComplete = Loadable({
  loader: () => import('./TaskComplete'),
  loading: Loading
});
const Run = Loadable({
  loader: () => import('./Run'),
  loading: Loading
});
const DeletedSuccessfully = Loadable({
  loader: () => import('./DeletedSuccessfully'),
  loading: Loading
});
const Star = Loadable({
  loader: () => import('./Star'),
  loading: Loading
});
const DoneCircle = Loadable({
  loader: () => import('./DoneCircle'),
  loading: Loading
});
const Error = Loadable({
  loader: () => import('./Error'),
  loading: Loading
});
const Warning = Loadable({
  loader: () => import('./Warning'),
  loading: Loading
});
const Information = Loadable({
  loader: () => import('./Information'),
  loading: Loading
});
const SendingSuccess = Loadable({
  loader: () => import('./SendingSuccess'),
  loading: Loading
});

export {
  Close,
  Copy,
  Done,
  Downloading,
  LoadingBall,
  Logo,
  NetworkError,
  Uploading,
  Pencil,
  TaskComplete,
  Run,
  DeletedSuccessfully,
  Star,
  DoneCircle,
  Error,
  Warning,
  Information
};
