import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './copy.json';

const Copy = onlyUpdateForKeys([])(() => {
  const options = {
    animationData
  };
  return (
    <div
      style={{
        position: 'absolute',
        top: 0,
        width: '100%',
        zIndex: 1
      }}
    >
      <Animation options={options} height={200} width={200} />
    </div>
  );
});

export default Copy;
