import React, { useState } from 'react';
import { useEffect } from 'react';
import Lottie from 'react-lottie';
import animationData from './sending-success.json';

const SendingSuccess = ({ play }) => {
  const [isStopped, setIsStopped] = useState(true);
  const [isPaused, setIsPaused] = useState(true);

  const defaultOptions = {
    loop: false,
    autoplay: true,
    animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  useEffect(
    () => {
      setIsStopped(!play);
      setIsPaused(!play);
    },
    [play]
  );

  return (
    <Lottie
      options={defaultOptions}
      height="1.2em"
      width="1.2em"
      isStopped={isStopped}
      isPaused={isPaused}
    />
  );
};

export default SendingSuccess;
