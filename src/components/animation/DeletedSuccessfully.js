import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './deletedsuccessfully.json';

const DeletedSuccessfully = onlyUpdateForKeys(['style'])(({ style }) => {
  const options = {
    animationData
  };
  return (
    <div style={style}>
      <Animation options={options} height={150} width={150} />
    </div>
  );
});

export default DeletedSuccessfully;
