import React, { Component } from 'react';
import Lottie from 'react-lottie';
import animationData from './taskcomplete.json';

class TaskComplete extends Component {
  constructor(props) {
    super(props);
    this.state = { isStopped: false, isPaused: false };
  }

  render() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };

    return (
      <Lottie
        options={defaultOptions}
        height="2em"
        width="5em"
        isStopped={this.state.isStopped}
        isPaused={this.state.isPaused}
      />
    );
  }
}

export default TaskComplete;
