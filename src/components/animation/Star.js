import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './star.json';

const Star = onlyUpdateForKeys(['style'])(({ style }) => {
  const options = {
    animationData
  };
  return (
    <div style={style}>
      <Animation options={options} height={100} width={100} />
    </div>
  );
});

export default Star;
