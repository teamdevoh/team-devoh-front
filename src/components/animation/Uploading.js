import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './uploading.json';
import { Responsive } from 'semantic-ui-react';

const Uploading = onlyUpdateForKeys([])(() => {
  const options = {
    animationData
  };
  return (
    <div>
      <Responsive minWidth={Responsive.onlyLargeScreen.minWidth}>
        <Animation options={options} height={200} width={200} />
      </Responsive>
      <Responsive maxWidth={Responsive.onlyLargeScreen.minWidth}>
        <Animation options={options} height={200} width={200} />
      </Responsive>
    </div>
  );
});

export default Uploading;
