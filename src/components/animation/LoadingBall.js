import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './loadingball.json';
import { Responsive } from 'semantic-ui-react';

const LoadingBall = onlyUpdateForKeys(['style'])(({ style }) => {
  const options = {
    animationData
  };
  return (
    <div style={style}>
      <Responsive minWidth={Responsive.onlyLargeScreen.minWidth}>
        <Animation options={options} height={50} width={50} />
      </Responsive>
      <Responsive maxWidth={Responsive.onlyLargeScreen.minWidth}>
        <Animation options={options} height={50} width={50} />
      </Responsive>
    </div>
  );
});

export default LoadingBall;
