import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './donecircle.json';

const DoneCircle = onlyUpdateForKeys(['style'])(({ style }) => {
  const options = {
    animationData
  };
  return (
    <div style={style}>
      <Animation options={options} height={200} width={200} />
    </div>
  );
});

export default DoneCircle;
