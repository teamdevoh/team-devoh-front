import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './networkerror.json';
import { Responsive } from 'semantic-ui-react';

const NetworkError = onlyUpdateForKeys([])(() => {
  const options = {
    loop: false,
    animationData
  };
  return (
    <div>
      <Responsive minWidth={Responsive.onlyLargeScreen.minWidth}>
        <Animation options={options} height={600} width={600} />
      </Responsive>
      <Responsive maxWidth={Responsive.onlyLargeScreen.minWidth}>
        <Animation options={options} height={300} width={300} />
      </Responsive>
    </div>
  );
});

export default NetworkError;
