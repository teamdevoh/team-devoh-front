import React, { Component } from 'react';
import Lottie from 'react-lottie';
import animationData from './pencil.json';

class Pencil extends Component {
  constructor(props) {
    super(props);
    this.state = { isStopped: false, isPaused: false };
  }

  render() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };

    return (
      <Lottie
        options={defaultOptions}
        height="2em"
        width="2em"
        isStopped={this.state.isStopped}
        isPaused={this.state.isPaused}
      />
    );
  }
}

export default Pencil;
