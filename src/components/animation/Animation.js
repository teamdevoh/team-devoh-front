import React, { PureComponent } from 'react';
import Lottie from 'react-lottie';

class Animation extends PureComponent {
  constructor(props) {
    super(props);
    this.defaultOptions = {
      loop: true,
      autoplay: true,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid meet'
      }
    };
  }

  componentDidMount() {
    this.refs.uploading.options.container.style.outline = 'none';
  }

  componentWillUnmount() {
    const uploading = this.refs.uploading;
    uploading.anim.destroy();
  }

  render() {
    const { height, width, options, isClickToPauseDisabled } = this.props;
    this.defaultOptions = { ...options };

    return (
      <div>
        <Lottie
          ref="uploading"
          height={height}
          width={width}
          options={this.defaultOptions}
          isClickToPauseDisabled={
            isClickToPauseDisabled === void 0 ? true : isClickToPauseDisabled
          }
        />
      </div>
    );
  }
}
export default Animation;
