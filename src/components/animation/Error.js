import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './error.json';

const Error = onlyUpdateForKeys(['style'])(({ style }) => {
  const options = {
    animationData,
    loop: false
  };
  return (
    <div style={style}>
      <Animation options={options} height={200} width={200} />
    </div>
  );
});

export default Error;
