import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import Animation from './Animation';
import animationData from './logo.json';
import { Responsive } from 'semantic-ui-react';

const Logo = onlyUpdateForKeys([])(() => {
  const options = {
    loop: false,
    animationData
  };
  return (
    <div
      style={{ display: 'inline-block', marginRight: '-30px' }}
      className="brandLogo"
    >
      <Responsive minWidth={Responsive.onlyLargeScreen.minWidth}>
        <Animation options={options} height={40} width={80} />
      </Responsive>
      <Responsive maxWidth={Responsive.onlyLargeScreen.minWidth}>
        <Animation options={options} height={30} width={70} />
      </Responsive>
    </div>
  );
});

export default Logo;
