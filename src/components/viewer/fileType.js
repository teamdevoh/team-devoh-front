export const type = {
  pdf: 'pdf',
  image: 'image',
  msOffice: 'msOffice',
  text: 'text'
};

export const fileType = {
  //'text/plain',
  'application/pdf': type.pdf,
  'image/png': type.image,
  'image/jpeg': type.image,
  'image/gif': type.image,
  'image/tiff': type.image,
  'application/vnd.ms-excel': type.msOffice,
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
    type.msOffice,
  'application/msword': type.msOffice,
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
    type.msOffice,
  'application/vnd.openxmlformats-officedocument.presentationml.presentation':
    type.msOffice,
  'text/plain': type.text
};
