import React, { Fragment, useEffect, useState } from 'react';
import styled from 'styled-components';
import { Container, Button, Modal, Popup, Image } from 'semantic-ui-react';

import helpers from '../../helpers';

import PDFViewer from './PDFViewer';
import OfficeViewer from './OfficeViewer';
import { fileType, type as viewerType } from './fileType';
import Service from '../../helpers/Service';
import CsvViewer from './CsvViewer';

const StyledModal = styled(Modal)`
  &&& {
    width: 97% !important;
    position: relative;
    height: 98%;

    & .modal-content.scrolling.content {
      padding: 0;
    }
  }
`;

const PDFWrap = styled.div`
  //width: calc(100% - 10%);
  //max-width: 1110px;
  margin-top: calc(var(--component-height) / -2);
  margin-left: auto;
  margin-right: auto;
  display: flex;
  justify-content: center;
  font-size: 19px;

  & {
    .react-pdf__Document {
      box-shadow: 0 30px 40px 0 rgb(16 36 94 / 20%);
      border-radius: 8px;
      max-width: 100%;
      position: relative;
    }

    .react-pdf__Page__canvas {
      width: auto !important;
      height: auto !important;
      border-radius: 8px;
      margin: auto;
    }

    .react-pdf__Page__textContent {
      & > * {
        margin-top: 2px;
        font-family: 'NanumSquare', '맑은 고딕', 'Malgun Gothic', 'Noto Sans',
          sans-serif !important;
      }
    }
  }
`;

const TopButtonWrap = styled.div`
  position: fixed;
  display: flex;
  transition: opacity ease-in-out 0.2s;
  border-radius: 4px;
  color: #000;
  padding: 10px;
  z-index: 1000;

  right: 0;
  background: black;
  left: 0;
  top: 0;
  height: 56px;
`;

const DocWrap = styled.div`
  overflow: scroll;
  height: 100vh;
  //padding: 10px;
  position: relative;
  width: 100%;
  top: 56px;

  &:hover .page-controls {
    opacity: 1;
  }
`;

const CloseButton = styled(Button)`
  &&& {
    margin-left: auto;
  }
`;

const FileName = styled.h2`
  color: #fff;
  width: calc(100% - 41px);
  padding: 0 5px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  margin: 0;
`;

const Wrap = styled.div`
  display: flex;
  justify-content: center;
  height: 100%;
`;

const Viewer = ({
  open,
  onOK,
  src,
  type,
  name,
  pageNumber = 0,
  fileKey,
  useModalType = true,
  getDownloadUrl = f => f
}) => {
  const [viewer, setViewer] = useState(null);
  const handleCloseClick = () => {
    onOK();
  };

  useEffect(
    () => {
      if (!open) {
        setViewer(null);
      } else {
        const ft = fileType[type];

        if (typeof ft !== 'undefined') {
          if (ft === viewerType.pdf && pageNumber > 0) {
            setViewer(
              <PDFViewer open={open} file={src} pageNumber={pageNumber} />
            );
          } else if (ft === viewerType.msOffice || ft === viewerType.pdf) {
            const isCsv = name.lastIndexOf('.csv') === name.length - 1 - 3;

            if (isCsv) {
              const url = getDownloadUrl(fileKey);
              Service.get(url, {
                responseType: 'text'
              }).then(res => {
                setViewer(<CsvViewer data={res.data} />);
              });
            } else {
              setViewer(<OfficeViewer open={open} src={src} type={type} />);
            }
          } else if (ft === viewerType.image) {
            setViewer(
              <Wrap>
                <div style={{ display: 'flex' }}>
                  <Image src={src} size="large" style={{ margin: 'auto' }} />
                </div>
              </Wrap>
            );
          } else if (ft === viewerType.text) {
            setViewer(
              <Wrap>
                <div style={{ display: 'flex', flex: 1 }}>
                  <iframe
                    src={src}
                    style={{
                      margin: 'auto',
                      backgroundColor: '#fff',
                      width: '100%',
                      height: '100%'
                    }}
                    frameBorder="0"
                  />
                </div>
              </Wrap>
            );
          } else {
            setViewer(null);
          }
        }
      }
    },
    [type, src, open, pageNumber]
  );

  if (useModalType === false) {
    return (
      <Container fluid>
        <PDFWrap>
          <DocWrap>{viewer}</DocWrap>
        </PDFWrap>
      </Container>
    );
  } else {
    return (
      <StyledModal
        basic
        onClose={onOK}
        open={open}
        size="fullscreen"
        title={name}
        content={
          <Container fluid>
            <PDFWrap>
              <DocWrap>
                <TopButtonWrap isMobile={helpers.util.isMobile()}>
                  <Popup
                    trigger={<FileName>{name}</FileName>}
                    style={{
                      borderRadius: 0,
                      opacity: 0.7
                    }}
                    position="bottom left"
                    content={name}
                    inverted
                  />
                  <CloseButton
                    circular
                    icon="close"
                    onClick={handleCloseClick}
                  />
                </TopButtonWrap>
                {viewer}
              </DocWrap>
            </PDFWrap>
          </Container>
        }
      />
    );
  }
};

export default Viewer;
