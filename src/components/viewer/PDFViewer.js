import React, { useState, useEffect, Fragment, useRef } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import styled from 'styled-components';
import { Button, Input } from 'semantic-ui-react';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${
  pdfjs.version
}/pdf.worker.js`;

const PageButton = styled(Button)`
  &&& {
    box-shadow: none;
    margin: 0;
    display: inline-flex;
    justify-content: center;
    align-items: center;
  }
`;

const PageControls = styled.div`
  position: fixed;
  display: flex;
  flex-wrap: wrap;

  opacity: 0;

  bottom: 5%;
  left: 50%;
  background: white;
  transform: translateX(-50%);
  transition: opacity ease-in-out 0.2s;
  box-shadow: 0 30px 40px 0 rgb(16 36 94 / 20%);
  border-radius: 4px;
  color: #000;
  padding: 10px;
  z-index: 1000;

  & button {
    width: 44px;
    height: 44px;
    background: white;
    border: 0;
    font: inherit;
    font-size: 0.8em;
    border-radius: 4px;
  }

  & button:first-child {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }

  & button:last-child {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }

  & div {
    font: inherit;
    padding: 0 0.5em;
  }
`;

const Wrap = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 12px 0 12px 0;
`;

const SmallInput = styled(Input)`
  &&& {
    display: flex;
    justify-content: center;
    align-items: center;

    & > input {
      width: 90px;
    }
  }
`;

const FlexWrap = styled.div`
  display: flex;
`;

const Margin = styled.span`
  margin: auto 8px;
`;

const SCALE_MIN = 20;
const SCALE_MAX = 500;

const PDFViewer = ({ open, file, pageNumber: pgNum }) => {
  const [pageNumber, setPageNumber] = useState(pgNum);
  const [numPages, setNumPages] = useState(null);
  const [scale, setScale] = useState(1);
  const iscaleInputRef = useRef();

  const onDocumentLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };

  const handleMovePage = newPageNumber => () => {
    setPageNumber(newPageNumber);
  };

  // 임시
  const handleDownloadClick = () => {
    const link = document.createElement('a');
    link.href = file;
    link.download = '결핵진료지침(4판)-(Web용)_최종.pdf';
    link.dispatchEvent(new MouseEvent('click'));
  };

  useEffect(
    () => {
      if (open) {
        setPageNumber(56);
      }
    },
    [open]
  );

  const handleScaleButtonClick = value => () => {
    const newScale = scale + value;
    setScale(newScale);
    iscaleInputRef.current.value = Math.round(newScale * 100);
  };

  const handleScaleInputChange = e => {
    let value = e.target.value;
    value = value.trim();
    let newScale = Math.floor(value);

    if (newScale < SCALE_MIN) {
      newScale = SCALE_MIN;
    } else if (newScale > SCALE_MAX) {
      newScale = SCALE_MAX;
    }

    iscaleInputRef.current.value = newScale;
    setScale(newScale / 100);
  };

  const handleKeyDown = e => {
    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      handleScaleInputChange(e);
    }
  };

  return (
    <Fragment>
      <PageControls className="page-controls" style={{ opacity: 1 }}>
        <div>
          <FlexWrap>
            <PageButton
              basic
              type="button"
              disabled={pageNumber <= 1}
              icon="angle left"
              onClick={handleMovePage(pageNumber - 1)}
            />
            <Wrap>
              <div>
                {pageNumber} of {numPages}
              </div>
            </Wrap>
            <PageButton
              basic
              type="button"
              disabled={pageNumber === numPages}
              icon="angle right"
              onClick={handleMovePage(pageNumber + 1)}
            />
            <Button circular icon="download" onClick={handleDownloadClick} />
          </FlexWrap>
          <SmallInput
            type="number"
            onBlur={handleScaleInputChange}
            onKeyDown={handleKeyDown}
            defaultValue={scale * 100}
          >
            <Button
              type="button"
              icon="minus"
              onClick={handleScaleButtonClick(-0.1)}
              disabled={Math.round(scale * 100) <= SCALE_MIN}
            />
            <input
              //  type="number"
              ref={iscaleInputRef}
            />
            <Margin>%</Margin>
            <Button
              type="button"
              icon="plus"
              onClick={handleScaleButtonClick(+0.1)}
              disabled={Math.round(scale * 100) >= SCALE_MAX}
            />
          </SmallInput>
        </div>
      </PageControls>
      <Document file={file} onLoadSuccess={onDocumentLoadSuccess}>
        <Page pageNumber={pageNumber} scale={scale} />
      </Document>
    </Fragment>
  );
};

export default PDFViewer;
