import React, { useEffect, useState, Fragment } from 'react';
import styled from 'styled-components';
import { fileType, type as viewerType } from './fileType';

const OfficeViewerRoot = styled.iframe`
  width: 100%;
  height: calc(100vh - 56px);
`;

const OfficeViewer = ({ src, type }) => {
  const [viewer, setViewer] = useState(null);

  useEffect(
    () => {
      const ft = fileType[type];

      if (typeof ft !== 'undefined') {
        switch (ft) {
          case viewerType.pdf:
            setViewer(
              <div id="container">
                <OfficeViewerRoot
                  id="obj"
                  type="application/pdf"
                  // src={`${src}#toolbar=0 `}
                  src={`${src}#zoom=100`}
                  width="500"
                  height="500"
                  frameBorder="0"
                >
                  Object can't be rendered
                </OfficeViewerRoot>
              </div>
            );
            break;
          case viewerType.msOffice:
            setViewer(
              <OfficeViewerRoot
                title="MS office viewer"
                frameBorder="0"
                src={`https://view.officeapps.live.com/op/embed.aspx?src=${encodeURIComponent(
                  src
                )}`}
              >
                This is an embedded
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="http://office.com"
                >
                  Microsoft Office
                </a>
                document, powered by
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="http://office.com/webapps"
                >
                  Office Online
                </a>
                .
              </OfficeViewerRoot>
            );
            break;
          default:
            break;
        }
      }
    },
    [type, src]
  );

  return <Fragment>{viewer}</Fragment>;
};

export default OfficeViewer;
