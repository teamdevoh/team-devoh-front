import React, { useEffect, useState } from 'react';
import FileViewer from './index';

const viewerInit = {
  type: null,
  name: null,
  key: null
};

const FileViewerWrapper = ({
  fileInfo = {},
  fetchSignedUrl,
  open,
  onClose,
  getDownloadUrl,
  useModalType
}) => {
  const [src, setSrc] = useState();
  const [viewerOpen, setViewerOpen] = useState(false);
  const [viewer, setViewer] = useState({
    ...viewerInit
  });

  const handleViewerClose = () => {
    setViewer({
      ...viewerInit
    });
    setViewerOpen(false);
    onClose();
  };

  useEffect(
    () => {
      const { type, name, key } = fileInfo;

      setViewer({
        type,
        name,
        key
      });
    },
    [open]
  );

  useEffect(
    () => {
      if (open) {
        const fetchUrl = async () => {
          const res = await fetchSignedUrl(fileInfo);
          setViewerOpen(true);
          if (res && res.data) {
            setSrc(res.data.signedUrl);
          }
        };

        fetchUrl();
      }
    },
    [open, fetchSignedUrl, fileInfo]
  );

  return (
    <FileViewer
      open={viewerOpen}
      src={src}
      type={viewer.type}
      name={viewer.name}
      fileKey={viewer.key}
      onOK={handleViewerClose}
      getDownloadUrl={getDownloadUrl}
      useModalType={useModalType}
    />
  );
};

export default FileViewerWrapper;
