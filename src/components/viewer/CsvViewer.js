import React, { useState, useEffect, Fragment } from 'react';
import { Table, Loader } from 'semantic-ui-react';
import { StyledTh, TableWrap } from '../case-review/styles';
import { useInView } from 'react-intersection-observer';
import { SortableHeader } from '../case-review/CROtherTest';
import helpers from '../../helpers';
import { EmptyRowsView } from '../modules';
import styled from 'styled-components';

const Root = styled.div`
  background-color: #fff;
`;

const ROW_LIMIT = 200;

const CsvViewer = ({ data = '' }) => {
  const [ref, inView, entry] = useInView({
    root: null,
    rootMargin: '200px',
    threshold: 0
  });

  const [header, setHeader] = useState([]);
  const [items, setItems] = useState([]);
  const [visibleItems, setVisibleItems] = useState([]);
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('');
  const [selectedKey, setSelectedKey] = useState();

  const handleSort = index => () => {
    const isAsc = orderBy === index && order === 'asc';
    const newOrder = isAsc ? 'desc' : 'asc';
    setOrder(newOrder);
    setOrderBy(index);

    const newItems = [...items];

    newItems.sort((a, b) => {
      let [x, y] = newOrder === 'asc' ? [a, b] : [b, a];

      x = x[index];
      y = y[index];

      if (!isNaN(x) && !isNaN(y)) {
        return (function(a, b) {
          a = a === null || a === '' ? -999 : Number(a);
          b = b === null || b === '' ? -999 : Number(b);

          if (a > b) {
            return 1;
          }
          if (a < b) {
            return -1;
          }

          return 0;
        })(x, y);
      } else {
        return helpers.util.compareString(x, y);
      }
    });

    setItems(newItems);
  };

  const handleRowClick = index => () => {
    setSelectedKey(index);
  };

  useEffect(
    () => {
      const arr = data.split('\n');
      setHeader(arr[0].split(','));
      setItems(arr.slice(1).map(item => item.split(',')));
    },
    [data]
  );

  useEffect(
    () => {
      setVisibleItems(items.slice(0, ROW_LIMIT));
      setSelectedKey(null);
    },
    [items]
  );

  useEffect(
    () => {
      if (
        inView &&
        entry.isIntersecting &&
        visibleItems.length < items.length
      ) {
        let newVisibleItems = [...visibleItems];

        newVisibleItems = newVisibleItems.concat(
          items.slice(
            newVisibleItems.length,
            newVisibleItems.length + ROW_LIMIT
          )
        );

        setVisibleItems(newVisibleItems);
      }
    },
    [inView]
  );

  return (
    <Fragment>
      {data === '' ? (
        <div
          style={{
            color: '#000'
          }}
        >
          <EmptyRowsView />
        </div>
      ) : (
        <Root>
          <TableWrap maxHeight="92vh">
            <Table
              compact="very"
              style={{
                fontSize: '14px'
              }}
            >
              <Table.Header>
                <Table.Row>
                  <StyledTh>No</StyledTh>
                  {header.map((column, index) => {
                    return (
                      <SortableHeader
                        key={index}
                        onClick={handleSort(index)}
                        orderByKey={index}
                        order={order}
                        orderBy={orderBy}
                      >
                        {column}
                      </SortableHeader>
                    );
                  })}
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {visibleItems.map((column, index) => {
                  return (
                    <Table.Row
                      key={index}
                      onClick={handleRowClick(index)}
                      style={
                        selectedKey === index
                          ? {
                              backgroundColor: 'aliceblue'
                            }
                          : null
                      }
                    >
                      <Table.Cell>{index + 1}</Table.Cell>
                      {column.map((column, index) => (
                        <Table.Cell key={index}>{column}</Table.Cell>
                      ))}
                    </Table.Row>
                  );
                })}
              </Table.Body>
            </Table>
            <div ref={ref}>
              {visibleItems.length < items.length && (
                <Loader active={true} inline="centered" size="mini" />
              )}
            </div>
          </TableWrap>
        </Root>
      )}
    </Fragment>
  );
};

export default CsvViewer;
