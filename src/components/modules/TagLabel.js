import React from 'react';
import { Label, Icon } from 'semantic-ui-react';

const TagLabel = ({ name, onDeleteClick }) => {
  const handleClick = () => {
    onDeleteClick(name);
  };

  return (
    <Label size="mini">
      {name}
      <Icon name="delete" onClick={handleClick} />
    </Label>
  );
};

export default TagLabel;
