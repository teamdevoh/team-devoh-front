import React from 'react';
import { Upload } from './';
import { Modal } from '../modal';

const UploadLayer = ({ open, onClose, title, onDrop, ...rest }) => {
  return (
    <Modal
      open={open}
      onOK={onClose}
      title={title}
      content={<Upload onDrop={onDrop} {...rest} />}
    />
  );
};

export default UploadLayer;
