import React, { memo } from 'react';
import { List } from 'semantic-ui-react';
import { useBoolean } from '../../hooks';

const FileTitle = memo(({ name }) => {
  const more = useBoolean(false);

  return (
    <List.Header
      className={more.value ? 'fileheader-fulltext' : 'fileheader-ellipsis'}
      onClick={more.toggle}
    >
      {name}
    </List.Header>
  );
});

export default FileTitle;
