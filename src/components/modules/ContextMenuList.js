import React, { memo } from 'react';
import { Dropdown } from 'semantic-ui-react';
import styled from 'styled-components';
import { ContextMenuListItem } from './';

const StyledMenu = styled(Dropdown.Menu)`
  &&&&& {
    right: 22px !important;
    bottom: 0px;
  }
`;

const ContextMenuList = ({ items }) => {
  if (items.length > 0) {
    return (
      <Dropdown
        text=""
        icon="ellipsis horizontal"
        direction="left"
        upward
        className="icon"
      >
        <StyledMenu>
          {items.map(item => {
            return (
              <ContextMenuListItem
                key={`${item.id}_${item.name}`}
                id={item.id}
                name={item.name}
                icon={item.icon}
                iconColor={item.iconColor}
                onClick={item.onClick}
              />
            );
          })}
        </StyledMenu>
      </Dropdown>
    );
  } else {
    return null;
  }
};

export default memo(ContextMenuList);
