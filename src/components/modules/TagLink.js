import React, { useCallback } from 'react';

const TagLink = ({ value, onClick }) => {
  const handleClick = useCallback(() => {
    onClick(value);
  }, []);

  return <a key={value} onClick={handleClick}>{`#${value}  `}</a>;
};

export default TagLink;
