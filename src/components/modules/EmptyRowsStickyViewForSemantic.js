import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Icon, Loader, Table } from 'semantic-ui-react';
import { StyledDimmer as Dimmer } from './styles';
import { RootWrap as Root, StyledCell } from './EmptyRowsViewForSemantic';

export const RootWrap = styled(Root)`
  @media screen and (min-width: 768px) {
    width: calc(100vw - 40vw);
  }
`;

const StyledDimmer = styled(Dimmer)`
  & .content {
    position: fixed;
  }
`;

const EmptyRowsStickyViewForSemantic = ({
  message,
  loading = false,
  columnLength,
  size = 'big'
}) => {
  return (
    <Table.Row>
      <StyledCell collapsing colSpan={columnLength} style={{ padding: 0 }}>
        <RootWrap size={size}>
          {loading ? (
            <StyledDimmer active inverted>
              <Loader size={size}>Loading</Loader>
            </StyledDimmer>
          ) : (
            <Fragment>
              <Icon name="window close outline" size="big" />
              <h3>{message ? message : 'No Data'}</h3>
            </Fragment>
          )}
        </RootWrap>
      </StyledCell>
    </Table.Row>
  );
};

export default EmptyRowsStickyViewForSemantic;
