import styled from 'styled-components';
import AnimatedNumber from 'react-animated-number/build/AnimatedNumber';
import { Button, Image, Segment, Dimmer } from 'semantic-ui-react';

export const UploadWrapper = styled.div`
  background-color: #eff4f3;
`;

export const TransitNumberBackground = styled(AnimatedNumber).attrs({
  duration: 1000
})`
  transition: 0.8s ease-out;
  font-size: 50px;
  transition-property: background-color, color, opacity;
`;

export const DropPanel = styled.div`
  height: 100px;
  width: 100px;
  outline: none;
  cursor: pointer;
`;

export const ResetButton = styled(Button).attrs({ color: 'yellow' })`
  text-align: center;
`;

export const Face = styled(Image)`
  height: 100px;
  width: 100px;
`;

export const Root = styled.div`
  display: flex;
  text-align: center;
  background-color: #ddd;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  position: absolute;
  width: 100%;
`;

export const StyledSegment = styled(Segment)`
  &&& {
    flex: 1;
    width: 100%;
  }
`;

export const StyledDimmer = styled(Dimmer)`
  &&& {
    background-color: transparent;
  }
`;
