import React, { useCallback } from 'react';
import { Dropdown, Icon } from 'semantic-ui-react';

const ContextMenuListItem = ({ name, icon, iconColor, id, onClick }) => {
  const handleClick = useCallback(() => {
    onClick(id);
  }, []);

  return (
    <Dropdown.Item onClick={handleClick}>
      <Icon name={icon} color={iconColor} className="right floated" />
      {name}
    </Dropdown.Item>
  );
};

export default ContextMenuListItem;
