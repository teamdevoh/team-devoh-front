import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Icon, Loader, Table } from 'semantic-ui-react';
import { Root, StyledDimmer } from './styles';

export const StyledCell = styled(Table.Cell)`
  &&& {
    padding: 0;
  }
`;

export const RootWrap = styled(Root)`
  padding: 64px;
  position: relative;
  min-height: 205px;
  background-color: #fff;

  ${props => {
    if (props.size !== 'big') {
      return `min-height: 100px
      padding: 24px;`;
    }
  }};
`;

const EmptyRowsViewForSemantic = ({
  message,
  loading = false,
  columnLength,
  size = 'big'
}) => {
  return (
    <Table.Row>
      <StyledCell collapsing colSpan={columnLength} style={{ padding: 0 }}>
        <RootWrap size={size}>
          {loading ? (
            <StyledDimmer active inverted>
              <Loader size={size}>Loading</Loader>
            </StyledDimmer>
          ) : (
            <Fragment>
              <Icon name="window close outline" size="big" />
              <h3>{message ? message : 'No Data'}</h3>
            </Fragment>
          )}
        </RootWrap>
      </StyledCell>
    </Table.Row>
  );
};

export default EmptyRowsViewForSemantic;
