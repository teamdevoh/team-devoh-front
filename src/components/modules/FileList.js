import React, { Fragment, useEffect, useState } from 'react';
import { List, Checkbox } from 'semantic-ui-react';
import { Modal, notification } from '../modal';
import find from 'lodash/find';
import map from 'lodash/map';
import styled from 'styled-components';
import {
  FileMeta,
  FileTitle,
  FileContextMenu,
  FileThumbnail,
  TagLink,
  ContextMenuList
} from './';
import { useBoolean, useFormatMessage } from '../../hooks';
import TagListContainer from './TagListContainer';
import { fileType as viewerFileType } from '../viewer/fileType';

FileContextMenu.preload();
FileThumbnail.preload();
FileMeta.preload();
FileTitle.preload();
TagLink.preload();

const Content = styled(List.Content)`
  .fileList &&&&& {
    display: inline-block;
    vertical-align: middle;
    margin-left: 0.5em;
    width: calc(100% - 99px);
  }
`;

const StyledFileThumbnail = styled(FileThumbnail)`
  .fileList &&&&& {
    vertical-align: top;
  }
`;

const FileListItem = styled(List.Item)`
  &&& {
    justify-content: center;
    align-items: flex-start;
    display: flex;
  }
`;

export const StyledCheckbox = styled(Checkbox)`
  &&& {
    margin-right: 8px;
  }
`;

const FileCheckBox = ({ onChange, checked }) => {
  const handleCheckChange = (e, data) => {
    onChange(e, data);
  };

  return <StyledCheckbox onChange={handleCheckChange} checked={checked} />;
};

const FileList = ({
  files,
  onDownloadClick,
  onDeleteClick,
  onUpdateTag,
  onTagClick,
  userContextMenus,
  lazyThumbnail,
  deleteButton,
  downloadButton,
  hasContextMenu = true,
  MultiDownloadButton,
  onViewerClick
}) => {
  const [choose, setChoose] = useState({ id: 0, name: '' });
  const [tags, setTags] = useState([]);
  const t = useFormatMessage();
  const showTag = useBoolean(false);
  const showDelete = useBoolean(false);
  const [selectedList, setSelectedList] = useState({});

  useEffect(() => {
    return () => {
      for (let i = 0; i < files.length; i++) {
        const file = files[i];
        window.URL.revokeObjectURL(file.imageUrl);
      }
    };
  }, []);

  const handleTagMenuClick = id => {
    const found = find(files, { key: id });

    let tags = [];
    if (found.tags) {
      const items = found.tags.split('#');
      for (let i = 0; i < items.length; i++) {
        const tag = items[i];
        if (tag.length > 0) {
          tags.push(`#${tag}`);
        }
      }
    }

    setTags(tags);
    showTag.setTrue();
    setChoose({ id, name: '' });
  };

  const handleUpdateTagClick = (id, tags) => {
    showTag.setFalse();
    onUpdateTag(id, tags);
  };

  const handleDeleteMenuClick = (id, name) => {
    setChoose({ id, name });
    showDelete.setTrue();
  };

  const handleUpdateTags = source => {
    setTags(source);
  };

  const handleOutsideDelBtnClick = (id, name) => () => {
    handleDeleteMenuClick(id, name);
  };

  const handleOutsideDownloadBtnClick = (id, name) => () => {
    onDownloadClick(id, name);
  };

  const handleCheckChange = fileInfo => (e, { checked }) => {
    const newItems = { ...selectedList };

    if (checked) {
      newItems[fileInfo.key] = {
        ...fileInfo
      };
    } else {
      delete newItems[fileInfo.key];
    }

    setSelectedList({ ...newItems });
  };

  const handleCheckAllClick = (e, { checked }) => {
    if (checked) {
      const newItems = {};

      for (let i = 0; i < files.length; i++) {
        const file = files[i];
        newItems[file.key] = {
          ...file
        };
      }

      setSelectedList({ ...newItems });
    } else {
      setSelectedList({});
    }
  };

  const handleViewerClick = fileInfo => (id, name) => {
    onViewerClick({ fileInfo, id, name });
  };

  useEffect(
    () => {
      if (showDelete.value) {
        notification.confirm({
          title: t('common.attach.delete.title'),
          content: t('common.attach.delete', {
            name: choose.name
          }),
          onCancel: showDelete.setFalse,
          onOK: () => {
            onDeleteClick(choose.id);
            showDelete.setFalse();
          }
        });
      }
    },
    [showDelete.value]
  );

  useEffect(
    () => {
      setSelectedList({});
    },
    [files]
  );

  return (
    <Fragment>
      {MultiDownloadButton && (
        <MultiDownloadButton
          totalCnt={files.length}
          selectedList={selectedList}
          onCheckAllClick={handleCheckAllClick}
        />
      )}

      <List relaxed className="fileList">
        {map(files, file => {
          const { type } = file;
          const hasViewer =
            typeof viewerFileType[type] === 'undefined' ? false : true;

          return (
            <FileListItem key={file.key}>
              {MultiDownloadButton && (
                <FileCheckBox
                  onChange={handleCheckChange(file)}
                  checked={typeof selectedList[file.key] !== 'undefined'}
                />
              )}
              <StyledFileThumbnail
                imageUrl={file.imageUrl}
                type={file.type}
                lazyThumbnail={lazyThumbnail}
              />
              <Content>
                <FileTitle name={file.name} />
                <FileMeta
                  size={file.size}
                  created={file.created}
                  creator={file.creator}
                />
                <List.Description className="filedescription">
                  {file.tags &&
                    file.tags.split('#').map(tag => {
                      if (tag) {
                        return (
                          <TagLink key={tag} value={tag} onClick={onTagClick} />
                        );
                      } else {
                        return null;
                      }
                    })}
                </List.Description>
              </Content>
              <List.Content
                //floated="right"
                style={{
                  display: 'inline-block'
                }}
              >
                {hasContextMenu ? (
                  <FileContextMenu
                    id={file.key}
                    name={file.name}
                    userContextMenus={userContextMenus}
                    onDownloadClick={onDownloadClick}
                    showDownloadMenu={onDownloadClick ? true : false}
                    showDeleteMenu={onDeleteClick ? true : false}
                    showTagMenu={onUpdateTag ? true : false}
                    showViewerClick={
                      !!onViewerClick && hasViewer ? true : false
                    }
                    onClickTagMenu={handleTagMenuClick}
                    onDeleteMenuClick={handleDeleteMenuClick}
                    onViewerClick={handleViewerClick(file)}
                  >
                    {contextItems => {
                      return <ContextMenuList items={contextItems} />;
                    }}
                  </FileContextMenu>
                ) : !!deleteButton ? (
                  deleteButton({
                    onDeleteClick: handleOutsideDelBtnClick(file.key, file.name)
                  })
                ) : !!downloadButton ? (
                  downloadButton({
                    onDownloadClick: handleOutsideDownloadBtnClick(
                      file.key,
                      file.name
                    )
                  })
                ) : null}
              </List.Content>
            </FileListItem>
          );
        })}
      </List>

      <Modal
        open={showTag.value}
        onOK={showTag.setFalse}
        title={t('task.tag')}
        size="mini"
        content={
          <TagListContainer
            id={choose.id}
            saveButton
            items={tags}
            onClick={handleUpdateTagClick}
            onUpdate={handleUpdateTags}
          />
        }
      />
    </Fragment>
  );
};

export default FileList;
