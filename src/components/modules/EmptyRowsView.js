import React, { Fragment } from 'react';
import { Dimmer, Icon, Loader } from 'semantic-ui-react';
import { Root, StyledSegment } from './styles';

const EmptyRowsView = ({ message, loading = false, style }) => {
  return (
    <Root style={style}>
      {loading ? (
        <StyledSegment>
          <Dimmer active inverted>
            <Loader size="big">Loading</Loader>
          </Dimmer>
        </StyledSegment>
      ) : (
        <Fragment>
          <Icon name="window close outline" size="big" />
          <h3>{message ? message : 'No Data'}</h3>
        </Fragment>
      )}
    </Root>
  );
};

export default EmptyRowsView;
