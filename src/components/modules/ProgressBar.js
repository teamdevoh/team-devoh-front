import React, { memo } from 'react';
import { Progress } from 'react-sweet-progress';
import 'react-sweet-progress/lib/style.css';

const ProgressBar = ({ percent }) => {
  return <Progress percent={percent} />;
};

export default memo(ProgressBar);
