import React from 'react';
import SimpleReactValidator from 'simple-react-validator';
import { Label } from 'semantic-ui-react';
import map from 'lodash/map';

const validator = () => {
  return new SimpleReactValidator({
    element: message => {
      return (
        <Label color="red" pointing basic>
          {message}
        </Label>
      );
    },
    validators: {
      password: {
        message:
          'The :attribute must contain at least one number and one uppercase and lowercase letter, and at least :min or more characters.',
        rule: (val, params, validator) => {
          return (
            validator.helpers.testRegex(
              val,
              /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
            ) && validator.helpers.size(val, 'string') >= parseFloat(params[0])
          );
        },
        messageReplace: (message, params) => message.replace(':min', params[0])
      },
      passwordMatch: {
        message: 'The :attribute not match password',
        rule: (val, params, validator) => {
          return val === params[0];
        }
      },
      name: {
        message: 'The :attribute may only contain letters',
        rule: (val, params, validator) => {
          return validator.helpers.testRegex(val, /^[가-힣]|[a-zA-Z]$/);
        }
      }
    }
  });
};

const validationTrans = intl => {
  map(validator.rules, (rule, key) => {
    rule.message = intl.formatMessage({ id: key });
  });
};

export default {
  validator,
  validationTrans
};
