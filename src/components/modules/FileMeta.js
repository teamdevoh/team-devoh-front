import React, { memo } from 'react';
import { List } from 'semantic-ui-react';
import styled from 'styled-components';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

const StyledDescription = styled(List.Description)`
  font-size: 0.8em;
`;

const FileMeta = memo(({ size, created, creator }) => {
  const t = useFormatMessage();

  return (
    <StyledDescription>
      {`${size}/${t('file.upload.info', {
        created: created,
        creator: creator
      })}`}
    </StyledDescription>
  );
});

export default FileMeta;
