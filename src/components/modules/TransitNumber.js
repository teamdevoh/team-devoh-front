import React, { useCallback } from 'react';
import { TransitNumberBackground } from './styles';

const TransitNumber = ({ value }) => {
  const handleRender = useCallback(n => {
    return Math.floor(n).toString();
  }, []);

  return (
    <TransitNumberBackground
      value={value}
      frameStyle={perc => (perc === 100 ? {} : { opacity: 0.5 })}
      formatValue={handleRender}
    />
  );
};

export default TransitNumber;
