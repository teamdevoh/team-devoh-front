import React, { Fragment } from 'react';
import { Header } from 'semantic-ui-react';
import Dropzone from 'react-dropzone';
import { Uploading } from '../animation';
import map from 'lodash/map';
import { useFormatMessage } from '../../hooks';
import { UploadWrapper } from './styles';

const InvalidPanel = ({ rejectedFiles, maxSize }) => {
  const t = useFormatMessage();
  return (
    <Fragment>
      <Header color="red">{t('file.is.too.large')}</Header>
      {map(rejectedFiles, rejected => {
        if (rejected.size > maxSize) {
          return <div key={rejected.name}>{rejected.name}</div>;
        }
      })}
    </Fragment>
  );
};

const Upload = ({ accept, onDrop, intl, multiple }) => {
  const t = useFormatMessage();
  const maxSize = 104857600; // 100MB

  const handleDrop = acceptedFiles => {
    if (acceptedFiles.length > 0) {
      onDrop(acceptedFiles);
    }
  };

  return (
    <UploadWrapper>
      <Dropzone
        onDrop={handleDrop}
        accept={accept}
        multiple={multiple === void 0 ? true : multiple}
        minSize={0}
        maxSize={maxSize}
      >
        {({ getRootProps, getInputProps, rejectedFiles }) => {
          const isFileTooLarge =
            rejectedFiles.length > 0 && rejectedFiles[0].size > maxSize;

          return (
            <section>
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                <Header>{t('common.attachment.title')}</Header>
                <Uploading />
                {isFileTooLarge && (
                  <InvalidPanel
                    rejectedFiles={rejectedFiles}
                    maxSize={maxSize}
                  />
                )}
              </div>
            </section>
          );
        }}
      </Dropzone>
    </UploadWrapper>
  );
};

export default Upload;
