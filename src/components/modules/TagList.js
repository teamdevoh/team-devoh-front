import React, { Fragment } from 'react';
import TagLabel from './TagLabel';

const TagList = ({ items, onDeleteClick }) => {
  return (
    <Fragment>
      {items.map(item => {
        return (
          <TagLabel key={item} name={item} onDeleteClick={onDeleteClick} />
        );
      })}
    </Fragment>
  );
};

export default TagList;
