import React, { memo, Fragment } from 'react';
import FadeIn from 'react-lazyload-fadein';
import { Image } from 'semantic-ui-react';
import uniqueId from 'lodash/uniqueId';
import styled from 'styled-components';

const FixedImage = styled(Image)`
  &&& {
    height: 25px;
    width: 25px;
  }
`;
const PreImageWrapper = styled.div`
  display: inline-block;
`;
const ImageWrapper = styled.div`
  visibility: hidden;
  width: 0;
  height: 0;
  display: inline-block;
`;

const LazyImageWithPre = ({ src, avatar, rounded, size, onClick, options }) => {
  if (src && src.length > 0) {
    const uid = uniqueId('lzImg-');
    const imgId = uid;
    const preImgId = `${uid}pre`;
    return (
      <Fragment>
        <ImageWrapper id={imgId}>
          <FadeIn height={20} once>
            {onload => (
              <FixedImage
                src={src}
                rounded={rounded}
                size={size}
                avatar={avatar ? avatar : false}
                onClick={onClick}
                onError={() => {
                  const preImg = document.getElementById(preImgId);
                  preImg.style.display = 'none';
                }}
                onLoad={event => {
                  const img = document.getElementById(imgId);
                  const preImg = document.getElementById(preImgId);
                  img.style.visibility = 'visible';
                  img.style.width = '25px';
                  img.style.height = '25px';
                  preImg.style.display = 'none';
                  onload();
                }}
                {...options}
              />
            )}
          </FadeIn>
        </ImageWrapper>
        <PreImageWrapper id={preImgId}>
          <FixedImage src={'images/giphy.gif'} rounded={rounded} size={size} />
        </PreImageWrapper>
      </Fragment>
    );
  } else {
    return <img alt="" src={'images/giphy.gif'} width={25} height={25} />;
  }
};

export default memo(LazyImageWithPre);
