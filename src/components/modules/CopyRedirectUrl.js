import React, { useEffect, useState } from 'react';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import CopyToClipboard from './CopyToClipboard';

const CopyRedirectUrl = () => {
  const [text, setText] = useState('');
  const t = useFormatMessage();

  useEffect(
    () => {
      const url =
        window.location.protocol +
        '//' +
        window.location.host +
        '/#/signin?redirect=' +
        helpers.history.location.pathname;
      setText(url);
    },
    [helpers.history.location]
  );

  return <CopyToClipboard text={text} title={t('common.copy.url.clipboard')} />;
};

export default CopyRedirectUrl;
