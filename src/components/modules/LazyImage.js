import React, { memo } from 'react';
import FadeIn from 'react-lazyload-fadein';
import { Image } from 'semantic-ui-react';

const LazyImage = ({ src, avatar, rounded, size, onClick, options }) => {
  return (
    <FadeIn height={20} once>
      {onload => (
        <Image
          src={src}
          rounded={rounded}
          size={size}
          avatar={avatar ? avatar : false}
          onClick={onClick}
          onLoad={onload}
          {...options}
        />
      )}
    </FadeIn>
  );
};

export default memo(LazyImage);
