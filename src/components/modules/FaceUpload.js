import React from 'react';
import { Icon } from 'semantic-ui-react';
import Dropzone from 'react-dropzone';
import { useFormatMessage } from '../../hooks';
import { DropPanel, Face, ResetButton } from './styles';

const FaceUpload = ({ imageSrc, onDrop, onReset }) => {
  const t = useFormatMessage();
  const src =
    imageSrc === void 0 || imageSrc === '' ? '/images/user.png' : imageSrc;

  return (
    <div>
      <Dropzone
        onDrop={onDrop}
        accept={'image/jpeg,image/jpg,image/png'}
        multiple={false}
      >
        {({ getRootProps, getInputProps }) => (
          <DropPanel {...getRootProps()}>
            <Icon.Group size="huge">
              <input {...getInputProps()} />
              <Face src={src} circular />
              <Icon corner="bottom right" name="camera retro" />
            </Icon.Group>
          </DropPanel>
        )}
      </Dropzone>
      <ResetButton onClick={onReset}>{t('common.reset.myface')}</ResetButton>
    </div>
  );
};

export default FaceUpload;
