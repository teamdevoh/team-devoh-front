import React from 'react';
import { Grid, Input, Button } from 'semantic-ui-react';
import TagList from './TagList';
import { useFormatMessage } from '../../hooks';

const TagListContainer = ({
  id,
  onClick,
  onUpdate,
  items,
  saveButton = false,
  onKeyDown = f => f
}) => {
  const t = useFormatMessage();

  const handleOnClick = event => {
    event.preventDefault();
  };

  const handleKeyDown = event => {
    onKeyDown(event);
  };

  const handleKeyUp = (event, data) => {
    const value = event.target.value;

    if (
      (event.keyCode === 13 || event.key.toLowerCase() === 'enter') &&
      value.length > 0 &&
      value.indexOf('#') < 0
    ) {
      event.target.value = '';
      handleAdd(`#${value}`);
    }
  };

  const handleAdd = tag => {
    if (tag && items.indexOf(tag) < 0) {
      const merged = items.concat(tag);
      onUpdate(merged);
    }
  };

  const handleDeleteClick = tag => {
    const filtered = items.filter(value => {
      return value !== tag;
    });
    onUpdate(filtered);
  };

  const handleClick = () => {
    onClick(id, items);
  };

  return (
    <Grid>
      <Grid.Column width={16}>
        <Input
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'tags',
            content: t('task.add.tag'),
            onClick: handleOnClick
          }}
          placeholder={t('task.enter.tag')}
          defaultValue=""
          onKeyUp={handleKeyUp}
          onKeyDown={handleKeyDown}
        />
      </Grid.Column>
      <Grid.Column width={16}>
        <TagList items={items} onDeleteClick={handleDeleteClick} />
      </Grid.Column>
      {saveButton && (
        <Grid.Column width={16}>
          <Button
            color="teal"
            content={t('common.save')}
            onClick={handleClick}
          />
        </Grid.Column>
      )}
    </Grid>
  );
};

export default TagListContainer;
