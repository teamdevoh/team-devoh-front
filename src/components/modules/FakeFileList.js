import React, { memo } from 'react';
import {
  Segment,
  Dimmer,
  Item,
  Loader,
  Placeholder,
  List
} from 'semantic-ui-react';

const FakeFileList = memo(({ items }) => {
  return (
    <List>
      {items.map((item, index) => {
        return <FakeItem key={index} name={item.name} />;
      })}
    </List>
  );
});

const FakeItem = ({ name }) => {
  return (
    <List.Item>
      <List.Content>
        <Segment>
          <Placeholder>
            <Placeholder.Image />
          </Placeholder>
          <Item.Meta />
          <Dimmer active inverted>
            <Loader size="mini">{`${name}`}</Loader>
          </Dimmer>
        </Segment>
      </List.Content>
    </List.Item>
  );
};

export default FakeFileList;
