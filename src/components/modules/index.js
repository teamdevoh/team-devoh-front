import Loadable from 'react-loadable';
import asyncComponent from './asyncComponent';

const FaceUpload = asyncComponent(() => import('./FaceUpload'));
const TransitNumber = asyncComponent(() => import('./TransitNumber'));
const Upload = asyncComponent(() => import('./Upload'));
const UploadLayer = asyncComponent(() => import('./UploadLayer'));
const FileList = asyncComponent(() => import('./FileList'));
const ProgressBar = asyncComponent(() => import('./ProgressBar'));
const ContextMenuList = asyncComponent(() => import('./ContextMenuList'));
const ContextMenuListItem = asyncComponent(() =>
  import('./ContextMenuListItem')
);
const FakeFileList = asyncComponent(() => import('./FakeFileList'));
const FileThumbnail = asyncComponent(() => import('./FileThumbnail'));
const FileContextMenu = asyncComponent(() => import('./FileContextMenu'));
const FileMeta = asyncComponent(() => import('./FileMeta'));
const FileTitle = asyncComponent(() => import('./FileTitle'));
const TagLink = asyncComponent(() => import('./TagLink'));
const EmptyRowsView = asyncComponent(() => import('./EmptyRowsView'));

export {
  FaceUpload,
  TransitNumber,
  Upload,
  UploadLayer,
  FileList,
  ProgressBar,
  ContextMenuList,
  ContextMenuListItem,
  FakeFileList,
  FileContextMenu,
  FileThumbnail,
  FileMeta,
  FileTitle,
  TagLink,
  EmptyRowsView
};

export { default as asyncComponent } from './asyncComponent';
export { default as SimpleValidator } from './Validator';
export { default as LazyImageWithPre } from './LazyImageWithPre';
export { default as LazyImage } from './LazyImage';
export {
  default as EmptyRowsViewForSemantic
} from './EmptyRowsViewForSemantic';
export {
  default as EmptyRowsStickyViewForSemantic
} from './EmptyRowsStickyViewForSemantic';
