import React, { useState } from 'react';
import { CopyToClipboard as ReactCopyToClipboard } from 'react-copy-to-clipboard';
import { Icon } from 'semantic-ui-react';

const CopyToClipboard = ({ title, text }) => {
  const [copied, setCopied] = useState(false);

  const handleCopied = () => {
    setTimeout(() => {
      setCopied(false);
    }, 400);
    setCopied(true);
  };

  return (
    <ReactCopyToClipboard text={text} onCopy={handleCopied}>
      <span style={{ cursor: 'pointer', fontSize: '0.7em' }}>
        <Icon name="copy outline" color={copied ? 'red' : 'black'} />
        {title}
      </span>
    </ReactCopyToClipboard>
  );
};

export default CopyToClipboard;
