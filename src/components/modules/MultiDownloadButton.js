import React, { useState, useEffect } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { Modal } from '../modal';
import { StyledCheckbox } from './FileList';
import helpers from '../../helpers';
import { FileDownloadResult } from '../project-archive/MultiDownloadButton';

const MultiDownloadButton = ({ getDownloadApi }) => ({
  selectedList,
  totalCnt,
  onCheckAllClick
}) => {
  const t = useFormatMessage();
  const [loading, setLoading] = useState(false);
  const [downloadIds, setDownloadIds] = useState([]);
  const [result, setResult] = useState({
    succeedIds: [],
    failedIds: []
  });
  const [showResult, setShowResult] = useState(false);

  const handleDownloadByReq = ({ downloadIds }) => {
    setLoading(true);

    for (let i = 0; i < downloadIds.length; i++) {
      const id = downloadIds[i];

      const resolve = res => {
        if (res) {
          const newResult = {
            ...result
          };

          newResult.succeedIds.push(id);

          setResult(newResult);
        }
      };

      const reject = err => {
        const newResult = {
          ...result
        };

        newResult.failedIds.push(id);

        setResult(newResult);
      };

      const downloadApi = getDownloadApi(id);

      helpers.util.download(downloadApi, f => f, resolve, reject);
    }
  };

  const handleDownloadClick = async () => {
    if (Object.keys(selectedList).length > 0) {
      const downloadIds = [];

      setLoading(true);

      for (let itemKey in selectedList) {
        if (selectedList.hasOwnProperty(itemKey)) {
          const { key: id } = selectedList[itemKey];

          downloadIds.push(id);
        }
      }

      setDownloadIds([...downloadIds]);

      handleDownloadByReq({ downloadIds });
      setLoading(false);
    }
  };

  const handleResultOkClick = () => {
    setShowResult(false);
  };

  useEffect(
    () => {
      const { failedIds, succeedIds } = result;

      if (
        downloadIds.length > 0 &&
        downloadIds.length === failedIds.length + succeedIds.length
      ) {
        setShowResult(true);
      }
    },
    [result, downloadIds]
  );

  useEffect(
    () => {
      if (showResult === false) {
        setResult({
          succeedIds: [],
          failedIds: []
        });
      }
    },
    [showResult]
  );

  useEffect(
    () => {
      if (showResult) {
        setLoading(false);
      }
    },
    [showResult]
  );

  return (
    totalCnt > 0 && (
      <div>
        <StyledCheckbox
          indeterminate={
            Object.keys(selectedList).length > 0 &&
            totalCnt > Object.keys(selectedList).length
          }
          checked={
            Object.keys(selectedList).length > 0 &&
            totalCnt === Object.keys(selectedList).length
          }
          onChange={onCheckAllClick}
        />
        <Button
          disabled={Object.keys(selectedList).length === 0}
          onClick={handleDownloadClick}
          loading={loading}
          type="button"
        >
          <Icon name="attach" />
          {t('common.download')}
        </Button>
        <Modal
          open={showResult}
          onOK={handleResultOkClick}
          c
          title={t('common.result')}
          content={
            <FileDownloadResult
              fileInfo={selectedList}
              succeedIds={result.succeedIds}
              failedIds={result.failedIds}
            />
          }
        />
      </div>
    )
  );
};

export default MultiDownloadButton;
