import React, { memo, useCallback, useState, useEffect, Fragment } from 'react';
import find from 'lodash/find';
import { LazyImageWithPre } from './';
import styled from 'styled-components';
import helpers from '../../helpers';
import { Image } from 'semantic-ui-react';

const StyledImage = styled(LazyImageWithPre)`
  &&& {
    margin-right: 1em;
    display: inline-block;
  }
`;

const mimeTypes = [
  { name: 'images/pdf.png', type: 'application/pdf' },
  { name: 'images/word.png', type: 'application/vnd.ms-word' },
  { name: 'images/word.png', type: 'application/msword' },
  { name: 'images/excel.png', type: 'application/vnd.ms-excel' },
  { name: 'images/notepad.png', type: 'text/plain' },
  {
    name: 'images/excel.png',
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  },
  {
    name: 'images/powerpoint.png',
    type:
      'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  }
];

const FileThumbnail = memo(
  ({ imageUrl, type, lazyThumbnail = true, ...rest }) => {
    const [url, setUrl] = useState('');

    const getThumbnail = () => {
      helpers.util.previewImage(imageUrl).then(blob => {
        setUrl(blob);
      });
    };

    const getImageSource = useCallback(type => {
      const found = find(mimeTypes, { type: type });
      if (found) {
        return found.name;
      } else {
        return 'images/file.png';
      }
    }, []);

    useEffect(
      () => {
        if (!!imageUrl) {
          if (helpers.util.isImage(type)) {
            getThumbnail();
          }
        } else {
          setUrl('images/empty.png');
        }
      },
      [imageUrl]
    );

    let imgSrc = url;
    if (type.indexOf('image') <= -1) {
      imgSrc = getImageSource(type);
    }

    return (
      <Fragment>
        {lazyThumbnail ? (
          <StyledImage src={imgSrc} {...rest} />
        ) : (
          <Image src={imgSrc} {...rest} />
        )}
      </Fragment>
    );
  }
);

export default FileThumbnail;
