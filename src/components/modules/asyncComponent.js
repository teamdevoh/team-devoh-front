import Loadable from 'react-loadable';
import { Loading } from '../loading';

const asyncComponent = (loader, loading) => {
  return Loadable({
    loader,
    loading: loading !== void 0 ? () => loading : Loading
  });
};

export default asyncComponent;
