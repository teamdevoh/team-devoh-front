import React, { Fragment } from 'react';
import { useFormatMessage } from '../../hooks';

const FileContextMenu = ({
  children,
  id,
  name,
  userContextMenus,
  showTagMenu,
  showDownloadMenu,
  showDeleteMenu,
  showViewerClick,
  onDownloadClick,
  onDeleteMenuClick,
  onClickTagMenu,
  onViewerClick
}) => {
  const t = useFormatMessage();

  const handleDownloadClick = () => {
    onDownloadClick(id, name);
  };

  const handleOpenDelete = () => {
    onDeleteMenuClick(id, name);
  };

  const handleOpenTag = () => {
    onClickTagMenu(id, name);
  };

  let contextItems = [];
  if (userContextMenus) {
    userContextMenus.forEach(contextMenu => {
      contextItems.push({
        id: id,
        name: contextMenu.name,
        icon: contextMenu.icon.name,
        iconColor: contextMenu.icon.color,
        onClick: () => {
          contextMenu.onClick({ id, name });
        }
      });
    });
  }

  if (showTagMenu) {
    contextItems.push({
      id: id,
      name: t('task.tag'),
      icon: 'tag',
      iconColor: 'yellow',
      onClick: handleOpenTag
    });
  }

  if (showViewerClick) {
    contextItems.push({
      id: id,
      icon: 'eye',
      name: 'View',
      onClick: onViewerClick
    });
  }

  if (showDownloadMenu) {
    contextItems.push({
      id: id,
      name: t('common.download'),
      icon: 'cloud download',
      onClick: handleDownloadClick
    });
  }

  if (showDeleteMenu) {
    contextItems.push({
      id: id,
      name: t('common.delete'),
      icon: 'delete',
      iconColor: 'red',
      onClick: handleOpenDelete
    });
  }

  return <Fragment>{children(contextItems)}</Fragment>;
};

export default FileContextMenu;
