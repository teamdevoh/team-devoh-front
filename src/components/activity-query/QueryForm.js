import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';

const QueryForm = ({ loading, model, onChange, onSubmit, label }) => {
  const t = useFormatMessage();

  return (
    <Form onSubmit={onSubmit}>
      <Form.Field width="sixteen">
        <Input
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'comments',
            content: t('common.save')
          }}
          placeholder="Enter query"
          name="query"
          label={label}
          value={model.query}
          onChange={onChange}
        />
      </Form.Field>
    </Form>
  );
};

export default QueryForm;
