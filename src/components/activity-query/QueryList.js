import React, { Fragment } from 'react';
import { useQueryList, useQueryGroup } from './hooks';
import { Comment } from 'semantic-ui-react';
import QueryWrapper from './QueryWrapper';
import { code } from '../../constants';

const QueryList = props => {
  const { subjectId, activityKeyId, projectActivityId } = props;

  const [query] = useQueryList({
    subjectId,
    activityKeyId,
    projectActivityId
  });
  const queryGroup = useQueryGroup(query.items);

  return (
    <Comment.Group size="small">
      {/* <Header as="h3" dividing>
        User 최다영 CRC
      </Header> */}
      {queryGroup.groups.map((queries, index) => {
        if (queries.length > 0) {
          const topQuery = queries[0];

          const subQueries = queries.filter(query => {
            return (
              query.activityQueryHistoryId !== topQuery.activityQueryHistoryId
            );
          });

          const openQueries = queries.filter(query => {
            return query.queryStatusCodeId === code.QueryOpen;
          });

          const lastOpenMemberId =
            openQueries.length > 0
              ? openQueries[openQueries.length - 1].memberId
              : topQuery.memberId;

          return (
            <QueryWrapper
              topQuery={topQuery}
              subQueries={subQueries}
              lastOpenMemberId={lastOpenMemberId}
              {...props}
            />
          );
        } else {
          return <Fragment />;
        }
      })}
    </Comment.Group>
  );
};
export default QueryList;
