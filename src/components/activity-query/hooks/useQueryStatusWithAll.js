import api from '../api';

const useQueryStatusWithAll = ({ subjectId }) => {
  const getQueryStatus = items => {
    return Promise.all(
      items.map(async item => {
        const res = await api.fetchActivityQueryStatus({
          subjectId,
          activityKeyId: item.activityKeyId,
          projectActivityId: item.projectActivityId
        });

        item['queryStatusCodeId'] = 0;

        if (res && res.data) {
          item['queryStatusCodeId'] = res.data.queryStatusCodeId;
        }

        return item;
      })
    );
  };

  return { getQueryStatus };
};

export default useQueryStatusWithAll;
