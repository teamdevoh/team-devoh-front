import { useCallback, useEffect } from 'react';
import api from '../api';
import { useBoolean } from '../../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import { activity } from '../../../states';

const useQueryList = ({ subjectId, activityKeyId, projectActivityId }) => {
  const isLoading = useBoolean(false);
  const dispatch = useDispatch();
  const queries = useSelector(activity.selectors.queries);

  const fetchItems = useCallback(
    async () => {
      // isLoading.setTrue();
      const res = await api.fetchQueries({
        subjectId,
        activityKeyId,
        projectActivityId
      });
      if (res && res.data) {
        dispatch(activity.actions.fetchQueries(res.data));
      }
      // isLoading.setFalse();
    },
    [subjectId, activityKeyId, projectActivityId]
  );

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, activityKeyId, projectActivityId]
  );

  return [{ items: queries, loading: isLoading.value }];
};

export default useQueryList;
