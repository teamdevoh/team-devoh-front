import { useCallback, useEffect, useState } from 'react';
import groupBy from 'lodash/groupBy';
import values from 'lodash/values';
import orderBy from 'lodash/orderBy';
import min from 'lodash/min';

const useQueryGroup = queries => {
  const [groups, setGroups] = useState([]);

  useEffect(
    () => {
      const dicGroups = groupBy(queries, 'activityQueryId');
      const grouped = values(dicGroups);

      let orderedGroups = [];
      grouped.forEach(group => {
        const orderedItems = orderBy(group, 'created', 'asc');
        orderedGroups.push(orderedItems);
      });

      setGroups(orderedGroups);
    },
    [queries]
  );

  const getPublishCode = useCallback(
    () => {
      const lastQueryStatusesByGroup = groups.map((items, index) => {
        const lastQueryOfGroup = items[items.length - 1];
        return lastQueryOfGroup.queryStatusCodeId;
      });
      return lastQueryStatusesByGroup.length > 0
        ? min(lastQueryStatusesByGroup)
        : 0;
    },
    [groups]
  );

  return { groups, getPublishCode };
};

export default useQueryGroup;
