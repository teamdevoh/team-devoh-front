import { useCallback } from 'react';
import api from '../api';
import { useDispatch, useSelector } from 'react-redux';
import { activity, project } from '../../../states';
import { useBoolean } from '../../../hooks';
import helpers from '../../../helpers';

const useQueryEdit = ({ subjectId, activityKeyId, activityQueryHistoryId }) => {
  const loading = useBoolean(false);
  const dispatch = useDispatch();
  const selectedProject = useSelector(project.selectors.getSelectedItem);

  const add = useCallback(
    async model => {
      loading.setTrue();
      const res = await api.addQuery({ subjectId, activityKeyId, model });
      loading.setFalse();
      if (res && res.data) {
        dispatch(
          activity.actions.addQuery({
            activityQueryHistoryId: res.data.value,
            activityQueryId: res.data.key,
            query: model.query,
            queryStatusCodeId: model.queryStatusCodeId,
            memberId: helpers.Identity.profile.id,
            myFaceFileStorageId: helpers.Identity.profile.faceId,
            author: helpers.Identity.profile.name,
            team: selectedProject.role,
            created: Date.now()
          })
        );

        return res.data.value > 0 ? true : false;
      }

      return false;
    },
    [subjectId, activityKeyId]
  );

  const edit = useCallback(
    async model => {
      loading.setTrue();
      const res = await api.updateQuery({
        subjectId,
        activityKeyId,
        activityQueryHistoryId,
        model
      });
      loading.setFalse();
      if (res && res.data) {
        dispatch(activity.actions.editQuery({ ...model, created: Date.now() }));

        return res.data ? true : false;
      }
      return false;
    },
    [subjectId, activityKeyId, activityQueryHistoryId]
  );

  const remove = useCallback(
    async () => {
      loading.setTrue();
      const res = await api.removeQuery({
        subjectId,
        activityKeyId,
        activityQueryHistoryId
      });
      loading.setFalse();
      if (res && res.data) {
        dispatch(activity.actions.removeQuery(activityQueryHistoryId));
        return res.data ? true : false;
      }
      return false;
    },
    [subjectId, activityKeyId, activityQueryHistoryId]
  );

  return { add, edit: edit, remove, loading: loading.value };
};

export default useQueryEdit;
