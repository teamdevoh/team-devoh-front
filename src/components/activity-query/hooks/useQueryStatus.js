import { useEffect, useState } from 'react';
import api from '../api';
import { useBoolean } from 'react-hanger';

const useQueryStatus = ({ subjectId, activityKeyId, projectActivityId }) => {
  const initModel = {
    author: '',
    queryStatusCodeId: 0
  };

  const [item, setItem] = useState(initModel);
  const isLoading = useBoolean(true);

  const fetchItem = async () => {
    const res = await api.fetchActivityQueryStatus({
      subjectId,
      activityKeyId,
      projectActivityId
    });
    if (res) {
      setItem({ ...initModel, ...res.data });
    }
    isLoading.setFalse();
  };

  useEffect(
    () => {
      if (subjectId > 0 && projectActivityId > 0 && activityKeyId !== void 0) {
        fetchItem();
      }
    },
    [subjectId, activityKeyId, projectActivityId]
  );

  return [{ query: item, loading: isLoading.value }, setItem];
};

export default useQueryStatus;
