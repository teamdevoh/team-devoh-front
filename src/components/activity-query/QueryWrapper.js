import React, { Fragment, useCallback, useState } from 'react';
import { Comment, Icon } from 'semantic-ui-react';
import { QueryFormContainer, QueryMeta, QueryListItem } from '.';
import { useBoolean } from '../../hooks';
import { code } from '../../constants';

const QueryWrapper = props => {
  const {
    topQuery,
    subQueries,
    projectId,
    subjectId,
    activityKeyId,
    projectActivityId,
    lastOpenMemberId
  } = props;
  const [queryStatus, setQueryStatus] = useState(0);
  const visibleAddForm = useBoolean(false);
  const isNew = useBoolean(false);
  const [inputLabel, setInputLabel] = useState();

  const queryOwnerId = topQuery.memberId;

  const lastQuery =
    subQueries.length > 0 ? subQueries[subQueries.length - 1] : topQuery;

  const handleUndoClick = useCallback(
    () => {
      visibleAddForm.setFalse();
    },
    [visibleAddForm.value]
  );

  const handleModifyClick = useCallback(
    () => {
      setInputLabel('Modify');
      isNew.setFalse();
      setQueryStatus(props.topQuery.queryStatusCodeId);
      visibleAddForm.setTrue();
    },
    [visibleAddForm.value, queryStatus, isNew.value]
  );

  const handleCloseClick = useCallback(
    () => {
      setInputLabel('Close');
      isNew.setTrue();
      visibleAddForm.setTrue();
      setQueryStatus(code.QueryClose);
    },
    [visibleAddForm.value, queryStatus, isNew.value]
  );

  const handleAnswerClick = useCallback(
    () => {
      setInputLabel('Answer');
      isNew.setTrue();
      visibleAddForm.setTrue();
      setQueryStatus(code.QueryAnswer);
    },
    [visibleAddForm.value, queryStatus, isNew.value]
  );

  return (
    <QueryMeta
      {...topQuery}
      subjectId={subjectId}
      activityKeyId={activityKeyId}
      projectActivityId={projectActivityId}
      queryOwnerId={queryOwnerId}
      lastOpenMemberId={lastOpenMemberId}
      lastQueryStatusCodeId={lastQuery.queryStatusCodeId}
      lastActivityQueryHistoryId={lastQuery.activityQueryHistoryId}
      lastMemberId={lastQuery.memberId}
      onModifyClick={handleModifyClick}
      onCloseClick={handleCloseClick}
      onAnswerClick={handleAnswerClick}
    >
      <Comment.Group size="small">
        {visibleAddForm.value && (
          <Fragment>
            <Icon
              name="undo"
              style={{ cursor: 'pointer' }}
              onClick={handleUndoClick}
            />
            <QueryFormContainer
              activityQueryId={topQuery.activityQueryId}
              activityQueryHistoryId={
                isNew.value ? 0 : topQuery.activityQueryHistoryId
              }
              queryStatusCodeId={queryStatus}
              projectId={projectId}
              subjectId={subjectId}
              activityKeyId={activityKeyId}
              projectActivityId={projectActivityId}
              value={isNew.value ? '' : topQuery.query}
              cbSubmit={handleUndoClick}
              label={inputLabel}
            />
          </Fragment>
        )}
        {subQueries.length > 0 && (
          <Fragment>
            {subQueries.map((item, index) => {
              return (
                <QueryListItem
                  key={index}
                  index={index}
                  {...item}
                  {...props}
                  activityQueryId={topQuery.activityQueryId}
                  queryOwnerId={queryOwnerId}
                  lastOpenMemberId={lastOpenMemberId}
                  lastQueryStatusCodeId={lastQuery.queryStatusCodeId}
                  lastActivityQueryHistoryId={lastQuery.activityQueryHistoryId}
                  lastMemberId={lastQuery.memberId}
                  onDeleteItemClick={() => {}}
                />
              );
            })}
          </Fragment>
        )}
      </Comment.Group>
    </QueryMeta>
  );
};

export default QueryWrapper;
