import React, { useCallback } from 'react';
import { QueryForm } from './';
import { useQueryEdit } from './hooks';
import { useFormFields, useFormatMessage } from '../../hooks';
import { notification } from '../modal';

const QueryFormContainer = ({
  activityQueryId,
  activityQueryHistoryId,
  queryStatusCodeId,
  projectId,
  subjectId,
  activityKeyId,
  projectActivityId,
  cbSubmit,
  value,
  label
}) => {
  const t = useFormatMessage();
  const { add, edit, loading } = useQueryEdit({
    subjectId,
    activityKeyId,
    activityQueryHistoryId
  });
  const [{ model, onChange }, setModel] = useFormFields({
    activityQueryId,
    queryStatusCodeId,
    subjectId: subjectId,
    activityKeyId: activityKeyId,
    projectActivityId: projectActivityId,
    activityQueryHistoryId: activityQueryHistoryId,
    query: value
  });

  const handleSubmit = useCallback(
    async () => {
      if (activityQueryHistoryId > 0) {
        if (await edit(model)) {
          notification.success({
            title: t('common.alert.changed'),
            onClose: () => {
              if (cbSubmit) {
                cbSubmit();
              }
            }
          });
        }
      } else {
        if (await add(model)) {
          setModel({ ...model, query: value });
          notification.success({
            title: t('common.alert.added'),
            onClose: () => {
              if (cbSubmit) {
                cbSubmit();
              }
            }
          });
        }
      }
    },
    [model]
  );

  return (
    <QueryForm
      loading={loading}
      model={model}
      onChange={onChange}
      onSubmit={handleSubmit}
      label={label}
    />
  );
};

export default QueryFormContainer;
