import React, { useCallback } from 'react';
import { Label } from 'semantic-ui-react';
import { code } from '../../constants';

const QueryTag = ({ queryStatusCodeId }) => {
  const color =
    queryStatusCodeId === code.QueryClose
      ? 'green'
      : queryStatusCodeId === code.QueryAnswer
        ? 'red'
        : 'blue';

  const getQueryStatusName = useCallback(
    () => {
      return queryStatusCodeId === code.QueryOpen
        ? 'Open'
        : queryStatusCodeId === code.QueryAnswer
          ? 'Answer'
          : 'Close';
    },
    [queryStatusCodeId]
  );

  return (
    <Label size="mini" color={color}>
      {getQueryStatusName(queryStatusCodeId)}
    </Label>
  );
};

export default QueryTag;
