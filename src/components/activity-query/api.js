import helpers from '../../helpers';

const fetchActivityQueryStatus = ({
  subjectId,
  activityKeyId,
  projectActivityId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/query.status?${params}`
  );
};

const fetchQueries = ({ subjectId, activityKeyId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });
  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/queries?${params}`
  );
};

const addQuery = ({ subjectId, activityKeyId, model }) => {
  return helpers.Service.post(
    `api/subjects/${subjectId}/activities/${activityKeyId}/queries`,
    model
  );
};

const updateQuery = ({
  subjectId,
  activityKeyId,
  activityQueryHistoryId,
  model
}) => {
  return helpers.Service.put(
    `api/subjects/${subjectId}/activities/${activityKeyId}/queries/${activityQueryHistoryId}`,
    model
  );
};

const removeQuery = ({ subjectId, activityKeyId, activityQueryHistoryId }) => {
  return helpers.Service.delete(
    `api/subjects/${subjectId}/activities/${activityKeyId}/queries/${activityQueryHistoryId}`
  );
};

export default {
  fetchActivityQueryStatus,
  fetchQueries,
  addQuery,
  updateQuery,
  removeQuery
};
