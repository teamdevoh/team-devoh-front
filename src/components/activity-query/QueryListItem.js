import React, { useCallback, useState, Fragment } from 'react';
import { Icon } from 'semantic-ui-react';
import { useBoolean } from '../../hooks';
import { QueryFormContainer } from './';
import QueryMeta from './QueryMeta';
import { code } from '../../constants';

const QueryListItem = props => {
  const {
    activityQueryId,
    activityQueryHistoryId,
    queryStatusCodeId,
    query,
    projectId,
    subjectId,
    activityKeyId,
    projectActivityId,
    queryOwnerId
  } = props;

  const [queryStatus, setQueryStatus] = useState(0);
  const visibleAddForm = useBoolean(false);
  const isNew = useBoolean(false);
  const [inputLabel, setInputLabel] = useState();

  const handleUndoClick = useCallback(
    () => {
      visibleAddForm.setFalse();
    },
    [visibleAddForm.value]
  );

  const handleModifyClick = useCallback(
    () => {
      setInputLabel('Modify');
      isNew.setFalse();
      setQueryStatus(queryStatusCodeId);
      visibleAddForm.setTrue();
    },
    [visibleAddForm.value, queryStatus, isNew.value]
  );

  const handleAnswerClick = useCallback(
    () => {
      setInputLabel('Answer');
      isNew.setTrue();
      visibleAddForm.setTrue();
      setQueryStatus(code.QueryAnswer);
    },
    [visibleAddForm.value, queryStatus, isNew.value]
  );

  const handleCloseClick = useCallback(
    () => {
      setInputLabel('Close');
      isNew.setTrue();
      visibleAddForm.setTrue();
      setQueryStatus(code.QueryClose);
    },
    [visibleAddForm.value, queryStatus, isNew.value]
  );

  const handleReopenClick = useCallback(
    () => {
      isNew.setTrue();
      visibleAddForm.setTrue();
      setQueryStatus(code.QueryOpen);
    },
    [visibleAddForm.value, queryStatus, isNew.value]
  );

  return (
    <QueryMeta
      {...props}
      queryOwnerId={queryOwnerId}
      onModifyClick={handleModifyClick}
      onAnswerClick={handleAnswerClick}
      onCloseClick={handleCloseClick}
      onReopenClick={handleReopenClick}
    >
      {visibleAddForm.value && (
        <Fragment>
          <Icon
            name="undo"
            style={{ cursor: 'pointer' }}
            onClick={handleUndoClick}
          />
          <QueryFormContainer
            activityQueryId={activityQueryId}
            activityQueryHistoryId={isNew.value ? 0 : activityQueryHistoryId}
            queryStatusCodeId={queryStatus}
            projectId={projectId}
            subjectId={subjectId}
            activityKeyId={activityKeyId}
            projectActivityId={projectActivityId}
            value={isNew.value ? '' : query}
            cbSubmit={handleUndoClick}
            label={inputLabel}
          />
        </Fragment>
      )}
    </QueryMeta>
  );
};

export default QueryListItem;
