import React, { useCallback, Fragment } from 'react';
import { Comment, Icon } from 'semantic-ui-react';
import { useFace, useBoolean, useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import { Confirm, notification } from '../modal';
import { useQueryEdit } from './hooks';
import { QueryTag } from './';
import { code, format } from '../../constants';

const AuthorFace = ({ myFaceFileStorageId }) => {
  const [faceUrl] = useFace(myFaceFileStorageId);
  return <Comment.Avatar src={faceUrl} />;
};

const ActionModify = ({ isOwner, isLastQuery, isClose, onClick }) => {
  return (
    <Fragment>
      {isOwner &&
        isLastQuery && (
          <Fragment>
            <Icon name="edit" />
            <Comment.Action onClick={onClick}>Modidfy</Comment.Action>
          </Fragment>
        )}
    </Fragment>
  );
};

const ActionDelete = ({
  isOwner,
  isLastQuery,
  isClose,
  subjectId,
  activityKeyId,
  activityQueryHistoryId
}) => {
  const t = useFormatMessage();
  const showRemoveConfirm = useBoolean(false);

  const { remove, loading } = useQueryEdit({
    subjectId,
    activityKeyId,
    activityQueryHistoryId
  });

  const handleDeleteClick = useCallback(() => {
    showRemoveConfirm.setTrue();
  });

  const handleCancelClick = useCallback(
    () => {
      showRemoveConfirm.setFalse();
    },
    [showRemoveConfirm.value]
  );

  const handleOKClick = useCallback(async () => {
    if (await remove()) {
      notification.success({
        title: t('common.alert.deleted'),
        onClose: () => {
          showRemoveConfirm.setTrue();
        }
      });
    }
  });

  return (
    <Fragment>
      {isOwner &&
        isLastQuery && (
          <Fragment>
            <Icon name="trash" />
            <Comment.Action onClick={handleDeleteClick}>Delete</Comment.Action>
            <Confirm
              title={t('activity.query.remove.title')}
              content={t('activity.query.remove.description')}
              open={showRemoveConfirm.value}
              onOK={handleOKClick}
              loading={loading}
              onCancel={handleCancelClick}
            />
          </Fragment>
        )}
    </Fragment>
  );
};

const ActionAnswer = ({ isLastQuery, isOpen, isAuthor, onClick }) => {
  return (
    <Fragment>
      {isLastQuery &&
        (isOpen && !isAuthor) && (
          <Fragment>
            <Icon name="reply" />
            <Comment.Action onClick={onClick}>Answer</Comment.Action>
          </Fragment>
        )}
    </Fragment>
  );
};

const ActionClose = ({
  isReopenUser,
  isLastQuery,
  isOpen,
  isAnswer,
  onClick
}) => {
  return (
    <Fragment>
      {isLastQuery &&
        (isOpen || isAnswer) &&
        // isReopenUser && (
        true && (
          <Fragment>
            <Icon name="close" />
            <Comment.Action onClick={onClick}>Close</Comment.Action>
          </Fragment>
        )}
    </Fragment>
  );
};

const ActionReopen = ({ isLastQuery, isAnswer, onClick }) => {
  return (
    <Fragment>
      {isAnswer &&
        isLastQuery && (
          <Fragment>
            <Icon name="redo" />
            <Comment.Action onClick={onClick}>Reopen</Comment.Action>
          </Fragment>
        )}
    </Fragment>
  );
};

const QueryMeta = ({
  children,
  queryOwnerId,
  subjectId,
  activityKeyId,
  activityQueryHistoryId,
  author,
  myFaceFileStorageId,
  queryStatusCodeId,
  memberId,
  query,
  created,
  team,
  onClick,
  lastOpenMemberId,
  lastQueryStatusCodeId,
  lastActivityQueryHistoryId,
  lastMemberId,
  onModifyClick,
  onCloseClick,
  onAnswerClick,
  onReopenClick
}) => {
  const isReopenUser = helpers.Identity.profile.id === lastOpenMemberId;
  const isOwner = helpers.Identity.profile.id === memberId;
  const isAuthor = helpers.Identity.profile.id === lastMemberId;
  const isOpen = lastQueryStatusCodeId === code.QueryOpen;
  const isAnswer = lastQueryStatusCodeId === code.QueryAnswer;
  const isClose = lastQueryStatusCodeId === code.QueryClose;
  const isLastQuery = lastActivityQueryHistoryId === activityQueryHistoryId;

  return (
    <Comment>
      <AuthorFace myFaceFileStorageId={myFaceFileStorageId} />
      <Comment.Content>
        <Comment.Author as="a" onClick={onClick}>
          {author}
        </Comment.Author>{' '}
        {team}
        <Comment.Metadata>
          <div>{helpers.util.dateformat(created, format.YYYY_MM_DD_HHMM)}</div>
        </Comment.Metadata>
        <Comment.Text>
          <QueryTag queryStatusCodeId={queryStatusCodeId} /> {query}
        </Comment.Text>
        <Comment.Actions>
          <ActionModify
            isOwner={isOwner}
            isLastQuery={isLastQuery}
            isClose={isClose}
            onClick={onModifyClick}
          />
          <ActionDelete
            isOwner={isOwner}
            isLastQuery={isLastQuery}
            isClose={isClose}
            subjectId={subjectId}
            activityKeyId={activityKeyId}
            activityQueryHistoryId={activityQueryHistoryId}
          />
          <ActionAnswer
            isAuthor={isAuthor}
            isOpen={isOpen}
            isLastQuery={isLastQuery}
            onClick={onAnswerClick}
          />
          <ActionClose
            isReopenUser={isReopenUser}
            isLastQuery={isLastQuery}
            isOpen={isOpen}
            isAnswer={isAnswer}
            onClick={onCloseClick}
          />
          <ActionReopen
            isLastQuery={isLastQuery}
            isAnswer={isAnswer}
            onClick={onReopenClick}
          />
        </Comment.Actions>
      </Comment.Content>
      {children}
    </Comment>
  );
};

export default QueryMeta;
