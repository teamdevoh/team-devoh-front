import React from 'react';
import { QueryFormContainer, QueryList } from './';
import { useSelector } from 'react-redux';
import { project } from '../../states';
import helpers from '../../helpers';
import { MyFace } from '../account-manage';
import { Grid, Segment } from 'semantic-ui-react';
import { RoleAware } from '../auth';
import { code, role } from '../../constants';

const QueryContainer = ({ subjectId, activityKeyId, projectActivityId }) => {
  const selectedProject = useSelector(project.selectors.getSelectedItem);

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column
          width="sixteen"
          style={{ height: '300px', overflowY: 'auto' }}
        >
          <QueryList
            projectId={selectedProject.key}
            subjectId={subjectId}
            activityKeyId={activityKeyId}
            projectActivityId={projectActivityId}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="sixteen">
          <RoleAware allowedRole={role.Activity_Read}>
            <Segment style={{ backgroundColor: '#FBFAFA' }}>
              <div style={{ marginBottom: '0.5em' }}>
                <MyFace />
                {helpers.Identity.profile.name} {selectedProject.role}
              </div>
              <div>
                <QueryFormContainer
                  activityQueryId={0}
                  activityQueryHistoryId={0}
                  queryStatusCodeId={code.QueryOpen}
                  projectId={selectedProject.key}
                  subjectId={subjectId}
                  activityKeyId={activityKeyId}
                  projectActivityId={projectActivityId}
                  value={''}
                  label={'New Open'}
                />
              </div>
            </Segment>
          </RoleAware>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default QueryContainer;
