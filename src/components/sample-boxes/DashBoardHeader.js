import React, { Fragment } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { ProjectsOfSamples, ProjectSite } from '../sample-status/Selector';
import { StyledForm } from '../sample-status/styles';
import { TdmVisitOrder } from '../sample-barcode/Selector';
import { BoxType, Material, Analyte } from './Selector';

const DashBoardHeader = ({ projectId, filter, onFilterChange }) => {
  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <StyledForm>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Project</label>
                <ProjectsOfSamples
                  name="projectId"
                  value={filter.projectId}
                  onChange={onFilterChange}
                  defaultFirstValue
                />
              </Form.Field>
              <Form.Field>
                <label>Site</label>
                <ProjectSite
                  projectId={filter.projectId}
                  name="projectSiteId"
                  value={filter.projectSiteId}
                  onChange={onFilterChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Visit</label>
                <TdmVisitOrder
                  name="visitId"
                  value={filter.visitId}
                  onChange={onFilterChange}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Type</label>
                <BoxType
                  name="boxTypeCodeId"
                  value={filter.boxTypeCodeId}
                  onChange={onFilterChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Material</label>
                <Material
                  name="sampleMaterialCodeId"
                  value={filter.sampleMaterialCodeId}
                  onChange={onFilterChange}
                  projectId={filter.projectId}
                />
              </Form.Field>
              <Form.Field>
                <label>Analyte</label>
                <Analyte
                  name="projectSampleAliquotId"
                  value={filter.projectSampleAliquotId}
                  onChange={onFilterChange}
                  projectId={filter.projectId}
                  projectSampleId={0}
                />
              </Form.Field>
            </Form.Group>
          </StyledForm>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
