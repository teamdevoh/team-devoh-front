import helpers from '../../helpers';

const fetchProjectSampleNameList = ({ projectId }) => {
  return helpers.Service.get(
    `api/project/${projectId}/samples/name?$select=sampleName, projectSampleId, sampleMaterial, sampleMaterialCodeId`
  );
};

const fetchProjectSampleMaterialList = ({ projectId }) => {
  return helpers.Service.get(
    `api/project/${projectId}/samples/material?$select=sampleMaterial, sampleMaterialCodeId`
  );
};

const fetchProjectSampleAnalyteList = ({ projectId, projectSampleId }) => {
  return helpers.Service.get(
    `api/project/${projectId}/samples/${projectSampleId}/aliquot/analyte?$select=aliquotAnalyte, projectSampleAliquotId`
  );
};

const fetchSampleBoxList = ({
  projectId,
  projectSiteId,
  visitId,
  boxTypeCodeId,
  sampleMaterialCodeId,
  projectSampleAliquotId
}) => {
  const params = helpers.qs.stringify({
    projectSiteId,
    visitId,
    boxTypeCodeId,
    sampleMaterialCodeId,
    projectSampleAliquotId
  });
  return helpers.Service.get(
    `api/project/${projectId}/samples/boxes?${params}`
  );
};

const fetchSampleBoxAliquotList = ({ sampleBoxId }) => {
  return helpers.Service.get(`api/samples/boxes/${sampleBoxId}/aliquot`);
};

const addSampleBoxes = ({ projectId, model }) => {
  return helpers.Service.post(`api/project/${projectId}/samples/boxes`, model);
};

const addSampleBoxBarcodesPrint = ({ projectId, model }) => {
  return helpers.Service.post(
    `api/project/${projectId}/samples/boxes/barcodes/print`,
    model
  );
};

export default {
  fetchProjectSampleNameList,
  fetchProjectSampleMaterialList,
  fetchProjectSampleAnalyteList,
  fetchSampleBoxList,
  fetchSampleBoxAliquotList,
  addSampleBoxes,
  addSampleBoxBarcodesPrint
};
