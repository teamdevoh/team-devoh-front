import React, { Fragment } from 'react';
import { Select, Form } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import { useCodes, useFormatMessage, useProjectSite } from '../../hooks';
import {
  useProjectSampleNameList,
  useProjectSampleMaterialList,
  useProjectSampleAnalyteList
} from './hooks';

export const BoxType = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.BoxTypeCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[{ key: 0, text: t('all'), value: 0 }, ...items]}
    />
  );
};

export const RadioBoxType = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.BoxTypeCode });

  return (
    <Fragment>
      {items.map(item => {
        const { value: itemValue, text } = item;
        return (
          <Form.Radio
            key={itemValue}
            name={name}
            value={itemValue}
            checked={value === itemValue}
            label={text}
            onChange={onChange}
          />
        );
      })}
    </Fragment>
  );
};

export const ProjectSampleName = ({ name, value, onChange, projectId }) => {
  const [{ items, loading }] = useProjectSampleNameList({ projectId });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={items}
      loading={loading}
    />
  );
};

export const Material = ({ name, value, onChange, projectId }) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useProjectSampleMaterialList({ projectId });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[{ key: 0, text: t('all'), value: 0 }, ...items]}
      loading={loading}
    />
  );
};

export const Analyte = ({
  name,
  value,
  onChange,
  projectId,
  projectSampleId
}) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useProjectSampleAnalyteList({
    projectId,
    projectSampleId
  });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[{ key: 0, text: t('all'), value: 0 }, ...items]}
      loading={loading}
    />
  );
};

export const AnalyteHasNotSelected = ({
  name,
  value,
  onChange,
  projectId,
  projectSampleId
}) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useProjectSampleAnalyteList({
    projectId,
    projectSampleId
  });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[
        { key: 0, text: t('common.notSelected'), value: null },
        ...items
      ]}
      loading={loading}
    />
  );
};

export const ProjectSite = ({ name, value, onChange, projectId, ...rest }) => {
  const [items] = useProjectSite({ projectId });

  return (
    <Select
      item
      name={name}
      value={value}
      onChange={onChange}
      options={items}
      {...rest}
    />
  );
};
