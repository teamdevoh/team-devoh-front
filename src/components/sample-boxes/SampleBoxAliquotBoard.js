import React from 'react';
import { Container } from 'semantic-ui-react';
import { DataGrid } from '../data-grid';

const SampleBoxAliquotBoard = ({ items, loading, columns }) => {
  return (
    <Container>
      <DataGrid
        columns={columns}
        rows={items}
        rowKey="aliquotBarcode"
        minHeight={580}
        loading={loading}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
      />
    </Container>
  );
};

export default SampleBoxAliquotBoard;
