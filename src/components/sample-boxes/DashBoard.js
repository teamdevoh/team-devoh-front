import React, { useState, useEffect } from 'react';
import BoxBoard from './BoxBoard';
import SampleBoxAliquotBoard from './SampleBoxAliquotBoard';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import { Grid, Segment, Responsive } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useSampleBoxList, useSampleBoxAliquotList } from './hooks';
import SampleBoxButtonTool from './SampleBoxButtonTool';
import SampleBoxAliquotButtonTool from './SampleBoxAliquotButtonTool';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const DashBoard = ({ filter, onFilterChange }) => {
  const { projectId } = useParams();
  const [selected, setSelected] = useState(null);
  const [selectedIndexes, setSelectedIndexes] = useState([]);
  const [
    { items: boxItems, loading: boxItemsLoading, isDo, columns, exportCSV },
    { setIsDo, setItems }
  ] = useSampleBoxList({
    ...filter
  });

  const [
    {
      items: aliquotItems,
      loading: aliquotLoading,
      columns: aliquotColumns,
      exportCSV: aliquotExportCSV
    }
  ] = useSampleBoxAliquotList({
    sampleBoxId: selected === null ? 0 : selected.sampleBoxId
  });

  /**
   * grid row 클릭 이벤트
   * @param {int} index
   * @param {json} row
   */
  const handleSampleRowClick = (index, row) => {
    if (selected !== null && row.sampleBoxId === selected.sampleBoxId) {
      return;
    } else {
      setSelected(row);
    }
  };

  const handleRowsSelected = rows => {
    setSelectedIndexes(selectedIndexes.concat(rows.map(r => r.rowIdx)));
  };

  const handleRowsDeselected = rows => {
    let rowIndexes = rows.map(r => r.rowIdx);
    setSelectedIndexes(
      selectedIndexes.filter(i => rowIndexes.indexOf(i) === -1)
    );
  };

  useEffect(
    () => {
      setSelected(null);
    },
    [filter]
  );

  useEffect(
    () => {
      if (isDo) {
        setSelectedIndexes([]);
      }
    },
    [isDo]
  );

  return (
    <Segment basic>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onFilterChange={onFilterChange}
            filter={filter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid divided="vertically">
        <Grid.Row style={{ marginTop: '20px' }}>
          <Grid.Column width={isMobile ? 16 : 8}>
            <Grid.Row>
              <Grid.Column>
                <DashBoardTool title="Box" items={boxItems}>
                  <SampleBoxButtonTool
                    setIsDo={setIsDo}
                    projectId={projectId}
                    boxItems={boxItems}
                    selectedIndexes={selectedIndexes}
                    exportCSV={exportCSV}
                  />
                </DashBoardTool>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <BoxBoard
                  columns={columns}
                  items={boxItems}
                  setItems={setItems}
                  loading={boxItemsLoading}
                  onRowClick={handleSampleRowClick}
                  selected={selected}
                  onRowsSelected={handleRowsSelected}
                  onRowsDeselected={handleRowsDeselected}
                  selectedIndexes={selectedIndexes}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid.Column>
          <Grid.Column width={isMobile ? 16 : 8}>
            <Grid.Row>
              <Grid.Column>
                <DashBoardTool title="Aliquot" items={aliquotItems}>
                  <SampleBoxAliquotButtonTool
                    items={aliquotItems}
                    exportCSV={aliquotExportCSV}
                  />
                </DashBoardTool>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <SampleBoxAliquotBoard
                  columns={aliquotColumns}
                  items={aliquotItems}
                  loading={aliquotLoading}
                  curProjectId={projectId}
                  selected={selected}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default DashBoard;
