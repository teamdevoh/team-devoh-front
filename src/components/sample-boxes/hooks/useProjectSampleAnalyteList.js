import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useProjectSampleAnalyteList = ({ projectId, projectSampleId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    try {
      const res = await api.fetchProjectSampleAnalyteList({
        projectId,
        projectSampleId
      });

      if (res && res.data) {
        const newItem = res.data.map(item => {
          const { aliquotAnalyte, projectSampleAliquotId } = item;
          return {
            key: projectSampleAliquotId,
            text: aliquotAnalyte,
            value: projectSampleAliquotId
          };
        });
        setItems(newItem);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      if (projectId) {
        fetchItems();
      }
    },
    [projectId, projectSampleId]
  );

  return [{ items, loading }, { setItems }];
};

export default useProjectSampleAnalyteList;
