import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';

const useSampleBoxList = ({
  projectId,
  projectSiteId,
  visitId,
  boxTypeCodeId,
  sampleMaterialCodeId,
  projectSampleAliquotId
}) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isDo, setIsDo] = useState();

  const fetchItems = useCallback(async () => {
    let data = [];
    setLoading(true);
    try {
      const res = await api.fetchSampleBoxList({
        projectId,
        projectSiteId,
        visitId,
        boxTypeCodeId,
        sampleMaterialCodeId,
        projectSampleAliquotId
      });

      if (res && res.data) {
        data = res.data;
      }
    } catch (err) {}

    setLoading(false);
    setIsDo(false);

    return data;
  });

  useEffect(
    () => {
      if (projectId) {
        const fetch = async () => {
          const item = await fetchItems();

          setItems(item);
        };
        fetch();
      }
    },
    [
      projectId,
      projectSiteId,
      visitId,
      boxTypeCodeId,
      sampleMaterialCodeId,
      projectSampleAliquotId
    ]
  );

  useEffect(
    () => {
      if (isDo) {
        const fetch = async () => {
          const item = await fetchItems();

          setItems(item);
        };
        fetch();
      }
    },
    [isDo]
  );

  const columns = [
    {
      key: 'site',
      name: 'Site'
    },
    {
      key: 'boxBarcode',
      name: 'Box ID'
    },
    { key: 'visit', name: 'Visit' },
    {
      key: 'boxLocation',
      name: 'Location'
    },
    {
      key: 'boxNote',
      name: 'Note'
    },
    {
      key: 'aliquotCnt',
      name: 'Count',
      width: 70
    },
    {
      key: 'boxStatus',
      name: 'Status',
      width: 70
    }
  ];

  const exportCSV = useCallback(
    async fileName => {
      const items = await fetchItems();
      if (items) {
        items.forEach(item => {
          const { boxLocation, boxNote } = item;
          item.boxLocation =
            !!boxLocation && boxLocation !== ', '
              ? boxLocation.replaceAll(', ', ' | ')
              : '';
          item.boxNote = !!boxNote ? boxNote.replaceAll(',', ' ') : '';
        });

        const fields = columns.map(item => ({ key: item.key }));
        const fieldNames = columns.map(item => item.name);

        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          data: items,
          fields,
          fieldNames
        });

        return items;
      }
      return [];
    },
    [
      projectId,
      projectSiteId,
      visitId,
      boxTypeCodeId,
      sampleMaterialCodeId,
      projectSampleAliquotId
    ]
  );

  return [{ items, loading, isDo, columns, exportCSV }, { setIsDo, setItems }];
};

export default useSampleBoxList;
