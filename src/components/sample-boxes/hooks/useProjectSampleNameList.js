import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useProjectSampleNameList = ({ projectId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    try {
      const res = await api.fetchProjectSampleNameList({
        projectId
      });

      if (res && res.data) {
        const newItem = res.data.map(item => {
          const { sampleName, projectSampleId } = item;
          return {
            key: projectSampleId,
            text: sampleName,
            value: projectSampleId,
            data: {
              ...item
            }
          };
        });
        setItems(newItem);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      if (projectId) {
        fetchItems();
      }
    },
    [projectId]
  );

  return [{ items, loading }, { setItems }];
};

export default useProjectSampleNameList;
