import { useState } from 'react';
import api from '../api';

const useSampleBoxBarcodePrint = ({ projectId }) => {
  const [loading, setLoading] = useState(false);

  const printBarcode = async model => {
    setLoading(true);
    try {
      const res = await api.addSampleBoxBarcodesPrint({
        projectId,
        model
      });
      return res.data;
    } finally {
      setLoading(false);
    }
  };

  return [{ loading, onPrintBarcode: printBarcode }];
};

export default useSampleBoxBarcodePrint;
