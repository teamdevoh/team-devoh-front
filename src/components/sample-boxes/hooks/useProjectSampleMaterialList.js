import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useProjectSampleMaterialList = ({ projectId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    try {
      const res = await api.fetchProjectSampleMaterialList({
        projectId
      });

      if (res && res.data) {
        const newItem = res.data.map(item => {
          const { sampleMaterial, sampleMaterialCodeId } = item;
          return {
            key: sampleMaterialCodeId,
            text: sampleMaterial,
            value: sampleMaterialCodeId
          };
        });
        setItems(newItem);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      if (projectId) {
        fetchItems();
      }
    },
    [projectId]
  );

  return [{ items, loading }, { setItems }];
};

export default useProjectSampleMaterialList;
