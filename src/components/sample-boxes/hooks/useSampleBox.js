import { useState, useEffect } from 'react';
import { useFormFields } from '../../../hooks';
import api from '../api';

const initial = {
  projectId: null,
  projectSiteId: null,
  visitOrder: null,
  projectSampleId: null,
  sampleMaterial: '',
  projectSampleAliquotId: null,
  boxTypeCodeId: null,
  boxCount: 0
};

const useSampleBox = () => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields(initial);

  const add = async () => {
    setLoading(true);
    try {
      const res = await api.addSampleBoxes({
        projectId: model.projectId,
        model
      });
      return res.data;
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    setModel(initial);
  }, []);

  return [{ model, loading, onChange, onAdd: add }, setModel];
};

export default useSampleBox;
