import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';
import { format } from '../../../constants';

const useSampleBoxAliquotList = ({ sampleBoxId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    let data = [];
    try {
      if (!!!sampleBoxId) {
        data = [];
      } else {
        const res = await api.fetchSampleBoxAliquotList({
          sampleBoxId
        });

        if (res && res.data) {
          data = res.data;
        }
      }
    } catch (err) {}
    setLoading(false);

    return data;
  });

  useEffect(
    () => {
      const fetch = async () => {
        const item = await fetchItems();

        setItems(item);
      };
      fetch();
    },
    [sampleBoxId]
  );

  const columns = [
    {
      key: 'subject',
      name: 'Subject',
      width: 120,
      formatter: ({ value, row }) => {
        const { initial } = row;
        return value + (!!initial ? `/${initial}` : '');
      }
    },
    {
      key: 'aliquotBarcode',
      name: 'Aliquot ID',
      width: 100
    },
    {
      key: 'sample',
      name: 'Sample'
    },
    {
      key: 'material',
      name: 'Material'
    },
    {
      key: 'aliquotAnalyte',
      name: 'Analyte'
    },
    {
      key: 'aliquotVolume',
      name: 'aliquotVolume',
      formatter: ({ value, row }) => {
        const { unit } = row;
        return value === null ? '' : `${value} ${unit}`;
      }
    },
    {
      key: 'boxingDateTime',
      name: 'Boxing',
      width: 140,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'memberName',
      name: 'User'
    }
  ];

  const exportCSV = useCallback(
    async fileName => {
      const items = await fetchItems();
      if (items) {
        items.forEach(item => {
          const {
            subject,
            initial,
            aliquotVolume,
            unit,
            boxingDateTime,
            memberName
          } = item;
          item.subject = subject + (!!initial ? `/${initial}` : '');
          item.aliquotVolume =
            aliquotVolume === null ? '' : `${aliquotVolume} ${unit}`;
          item.boxingDateTime = helpers.util.dateformat(
            boxingDateTime,
            format.YYYY_MM_DD_HHMM
          );
          item.memberName = !!memberName ? memberName.replaceAll(',', ' ') : '';
        });

        const fields = columns.map(item => ({ key: item.key }));
        const fieldNames = columns.map(item => item.name);

        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          data: items,
          fields,
          fieldNames
        });

        return items;
      }
      return [];
    },
    [sampleBoxId]
  );

  return [{ items, loading, columns, exportCSV }, { setItems }];
};

export default useSampleBoxAliquotList;
