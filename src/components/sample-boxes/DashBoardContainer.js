import React, { useState } from 'react';
import DashBoard from './DashBoard';

const DashBoardContainer = () => {
  const [filter, setFilter] = useState({
    projectId: null,
    projectSiteId: 0,
    visitId: 0,
    boxTypeCodeId: 0,
    sampleMaterialCodeId: 0,
    projectSampleAliquotId: 0
  });

  const handleFilterChange = (event, { name, value }) => {
    setFilter({ ...filter, [name]: value });
  };

  return <DashBoard filter={filter} onFilterChange={handleFilterChange} />;
};

export default DashBoardContainer;
