import React, { Fragment, useState } from 'react';
import { RightButtonWrap } from '../sample-barcode/styles';
import { StyledCSVExport } from '../export-tdm/styles';

const SampleBoxAliquotButtonTool = ({ items, exportCSV }) => {
  const [csvLoading, setCsvLoading] = useState(false);

  const handleExport = async () => {
    setCsvLoading(true);
    try {
      await exportCSV('Sample Aliquot List');
    } catch (err) {}
    setCsvLoading(false);
  };

  return (
    <Fragment>
      <RightButtonWrap>
        <StyledCSVExport
          positive
          icon="download"
          loading={csvLoading}
          onClick={handleExport}
          content="CSV Export"
          disabled={items.length === 0}
        />
      </RightButtonWrap>
    </Fragment>
  );
};

export default SampleBoxAliquotButtonTool;
