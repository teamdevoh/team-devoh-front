import React from 'react';
import { Form, Input, Divider } from 'semantic-ui-react';
import { StyledForm } from '../sample-status/styles';
import { ProjectsOfSamples } from '../sample-status/Selector';
import { TdmVisitOrderHasNotSelected } from '../sample-barcode/Selector';
import {
  ProjectSampleName,
  ProjectSite,
  AnalyteHasNotSelected,
  RadioBoxType
} from './Selector';
import { useSubmit, useFormatMessage } from '../../hooks';
import { useSampleBox } from './hooks';
import { SaveButton } from '../button';
import { role } from '../../constants';

const SampleBoxForm = ({ onSubmitCb = f => f }) => {
  const t = useFormatMessage();
  const [{ model, loading, onChange, onAdd }, setModel] = useSampleBox();

  const handleSubmit = async () => {
    const res = await onAdd();
    if (res) {
      onSubmitCb();
    }
  };

  const [{ validator, onValidate }] = useSubmit(handleSubmit);

  const handleSampleChange = (e, data) => {
    const { name, value, options } = data;
    const selectedSample = options.filter(item => item.value === value)[0];

    onChange(e, data);

    setModel({
      ...model,
      [name]: value,
      sampleMaterial: selectedSample.data.sampleMaterial,
      sampleMaterialCodeId: selectedSample.data.sampleMaterialCodeId
    });
  };

  return (
    <StyledForm onSubmit={onValidate}>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>Project</label>
          <ProjectsOfSamples
            name="projectId"
            value={model.projectId}
            onChange={onChange}
            defaultFirstValue
          />
          {validator.message('Project', model.projectId, 'required')}
        </Form.Field>
        <Form.Field required>
          <label>Site</label>
          <ProjectSite
            projectId={model.projectId}
            name="projectSiteId"
            value={model.projectSiteId}
            onChange={onChange}
          />
          {validator.message('Site', model.projectSiteId, 'required')}
        </Form.Field>
        <Form.Field>
          <label>Visit</label>
          <TdmVisitOrderHasNotSelected
            name="visitOrder"
            value={model.visitOrder}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>Sample</label>
          <ProjectSampleName
            projectId={model.projectId}
            name="projectSampleId"
            value={model.projectSampleId}
            onChange={handleSampleChange}
          />
          {validator.message('Sample', model.projectSampleId, 'required')}
        </Form.Field>
        <Form.Field>
          <label>Material</label>
          <Input name="sampleMaterial" value={model.sampleMaterial} readOnly />
        </Form.Field>
        <Form.Field>
          <label>Analyte</label>
          <AnalyteHasNotSelected
            name="projectSampleAliquotId"
            value={model.projectSampleAliquotId}
            onChange={onChange}
            projectId={model.projectSampleId && model.projectId}
            projectSampleId={model.projectSampleId}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>Type</label>
          <RadioBoxType
            name="boxTypeCodeId"
            value={model.boxTypeCodeId}
            onChange={onChange}
          />
          {validator.message('Type', model.boxTypeCodeId, 'required')}
        </Form.Field>
        <Form.Field required>
          <label>Box Count</label>
          <Input
            name="boxCount"
            type="number"
            value={model.boxCount}
            onChange={onChange}
          />
          {validator.message('Project', model.boxCount, 'required')}
        </Form.Field>
        <Form.Field />
      </Form.Group>
      <Divider />
      <SaveButton
        allowedRole={role.Sample_Box_Edit}
        name={t('common.save')}
        loading={loading}
      />
    </StyledForm>
  );
};

export default SampleBoxForm;
