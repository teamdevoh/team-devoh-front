import React, { Fragment, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { RightButtonWrap } from '../sample-barcode/styles';
import { useSampleBoxBarcodePrint } from './hooks';
import { useBoolean } from '../../hooks';
import SampleBoxForm from './SampleBoxForm';
import BarcodePrintInfoModal from '../sample-barcode/BarcodePrintInfoModal';
import { Modal } from '../modal';
import { StyledCSVExport } from '../export-tdm/styles';

const SampleBoxButtonTool = ({
  setIsDo,
  projectId,
  boxItems,
  selectedIndexes,
  exportCSV
}) => {
  const showModal = useBoolean(false);
  const showBarcodePrintinfo = useBoolean(false);
  const [printableCount, setPrintableCount] = useState(0);
  const [csvLoading, setCsvLoading] = useState(false);

  const [{ loading: printLoading, onPrintBarcode }] = useSampleBoxBarcodePrint({
    projectId
  });

  const handleCreateBoxIdClick = () => {
    showModal.setTrue();
  };

  const handleCreateBoxIdSubmitCb = () => {
    setIsDo(true);
    showModal.setFalse();
  };

  const handlePrintBarcodeClick = async () => {
    const printBarcodes = [];

    for (let i = 0; i < selectedIndexes.length; i++) {
      const index = selectedIndexes[i];
      printBarcodes.push(boxItems[index]);
    }

    const model = printBarcodes.map(item => {
      const { site, visit, aliquotAnalyte, sampleMaterial, boxBarcode } = item;

      return {
        irbNo: site,
        visitName: visit,
        analyte: aliquotAnalyte,
        material: sampleMaterial,
        barcode: boxBarcode
      };
    });

    const data = await onPrintBarcode(model);

    if (data > 0) {
      showBarcodePrintinfo.setTrue();
      setPrintableCount(data);
    }
  };

  const handleExport = async () => {
    setCsvLoading(true);
    try {
      await exportCSV('Sample Box List');
    } catch (err) {
      console.log(err);
    }
    setCsvLoading(false);
  };

  return (
    <Fragment>
      <RightButtonWrap>
        <Button loading={printLoading} onClick={handleCreateBoxIdClick}>
          Create Box ID
        </Button>
        <Button
          loading={printLoading}
          onClick={handlePrintBarcodeClick}
          disabled={selectedIndexes.length === 0}
        >
          Print Barcode
        </Button>
        <StyledCSVExport
          positive
          icon="download"
          loading={csvLoading}
          onClick={handleExport}
          content="CSV Export"
        />
      </RightButtonWrap>
      <Modal
        size="large"
        open={showModal.value}
        onOK={showModal.setFalse}
        title={'Create Box ID'}
        content={<SampleBoxForm onSubmitCb={handleCreateBoxIdSubmitCb} />}
      />
      <BarcodePrintInfoModal
        open={showBarcodePrintinfo.value}
        onOK={showBarcodePrintinfo.setFalse}
        title={'Print Box Barcode'}
        printableCount={printableCount}
        targetUrl={`http://oz.econsert.com/ozrviewer/ozreport_c.html?api=SampleManager&action=GetStudyLabelPrintByLocalIP&doc=SampleManagement/BoxLabel&param=studyID:0`}
      />
    </Fragment>
  );
};

export default SampleBoxButtonTool;
