import React from 'react';
import { Container } from 'semantic-ui-react';
import { DataGrid } from '../data-grid';

const BoxBoard = ({
  columns,
  items,
  loading,
  onRowClick,
  selectedIndexes,
  onRowsSelected,
  onRowsDeselected
}) => {
  return (
    <Container>
      <DataGrid
        columns={columns}
        rows={items}
        rowKey="sampleBoxId"
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        minHeight={580}
        loading={loading}
        onRowClick={onRowClick}
        rowSelection={{
          showCheckbox: true,
          enableShiftSelect: true,
          onRowsSelected,
          onRowsDeselected,
          selectBy: {
            indexes: selectedIndexes
          }
        }}
      />
    </Container>
  );
};

export default BoxBoard;
