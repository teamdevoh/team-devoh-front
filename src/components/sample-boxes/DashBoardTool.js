import React, { Fragment } from 'react';
import { Label } from 'semantic-ui-react';
import { StyledDetail, FlexDiv } from '../sample-barcode/styles';

const DashBoardTool = ({ title, items, children }) => {
  return (
    <Fragment>
      <FlexDiv>
        <Label size="big">
          {title}
          <StyledDetail>{items.length}</StyledDetail>
        </Label>
        {children}
      </FlexDiv>
    </Fragment>
  );
};

export default DashBoardTool;
