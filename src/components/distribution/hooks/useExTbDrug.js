import { useEffect, useState } from 'react';
import api from '../../collection-tbdrug/api';

const useExTbDrug = () => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchTbdrugs();
      if (res && res.data) {
        let exdrugs = [];
        res.data.items.forEach(item => {
          if (item.isExdrug === true) {
            exdrugs.push({
              key: item.tbdrugId,
              text: item.genericName,
              value: item.tbdrugId
            });
          }
        });

        setItems(exdrugs);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchItems();
  }, []);

  return { items, loading };
};

export default useExTbDrug;
