import { useEffect, useState } from 'react';
import api from '../api';

const useAdrNameByPT = (projectSiteId, selectedItem) => {
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchAdrNamesAdr(projectSiteId, selectedItem.value);
    if (res && res.data) {
      setItems(res.data);
    }
  };

  useEffect(
    () => {
      if (selectedItem.field === 'pt') {
        fetchItems();
      }
    },
    [projectSiteId, selectedItem]
  );

  return { items };
};

export default useAdrNameByPT;
