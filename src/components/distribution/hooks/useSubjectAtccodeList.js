import { useState, useEffect } from 'react';
import api from '../api';

const useSubjectAtccodeList = () => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchItems = async () => {
      setLoading(true);
      try {
        const res = await api.fetchSubjectAtccodeList();

        if (res && res.data) {
          setItems(res.data);
        }
      } catch (err) {}
      setLoading(false);
    };

    fetchItems();
  }, []);

  return [{ items, loading }];
};

export default useSubjectAtccodeList;
