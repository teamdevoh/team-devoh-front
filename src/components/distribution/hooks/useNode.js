import { useCallback } from 'react';

const useNode = () => {
  const getId = (major, minor) => {
    return `${major}-${minor}`;
  };

  const GenerateNodeIds = (data, parentNodeId, nodeCount) => {
    const splited = getSplittingId(parentNodeId);

    const ordered = _.orderBy(data, 'nodeId', 'asc');

    const nextMajorId = splited.majorId + 1;

    let lastId = 0;
    let childCount = 0;
    _.forEach(ordered, item => {
      const ids = getSplittingId(item.nodeId);
      if (ids.majorId === nextMajorId) {
        childCount++;
        if (lastId < ids.minorId) {
          lastId = ids.minorId;
        }
      }
    });

    let nodeIds = [];

    for (let i = 0; i < nodeCount; i++) {
      const newMajorId = splited.majorId + 1;
      const newMinorId = Number(childCount) + i;
      nodeIds.push(getId(newMajorId, newMinorId));
    }

    return nodeIds;
  };

  const getChildren = (data, nodeId) => {
    const children = _.filter(data, item => {
      return item.parentNodeId === nodeId;
    });
    return children;
  };

  const getNodeById = (data, nodeId) => {
    const foundParentItem = _.find(data, { nodeId });
    return foundParentItem;
  };

  const clearChildrenNode = useCallback((id, chart) => {
    _.forEach(chart.data(), item => {
      if (item.parentNodeId === id) {
        chart.removeNode(item.nodeId);
        clearChildrenNode(item.nodeId, chart);
      }
    });
  }, []);

  const searchDeeplyNested = (nodeId, parents, source) => {
    const parent = getNodeById(source, nodeId);
    if (parent && parent.nodeId !== getRootId()) {
      parents.push(parent);
      searchDeeplyNested(parent.parentNodeId, parents);
    }
  };

  const findAllDeeplyParents = (nodeId, source) => {
    let parents = [];
    searchDeeplyNested(nodeId, parents, source);

    return parents;
  };

  const getSplittingId = nodeId => {
    const ids = nodeId.split('-');

    return {
      majorId: Number(ids.length > 1 ? ids[0] : 0),
      minorId: Number(ids.length > 1 ? ids[1] : 0)
    };
  };

  const hasAlreadyQuery = (data, query) => {
    if (data.length === 0) {
      return false;
    }

    let alreadyQuery = false;
    for (let i = 0; i < data.length; i++) {
      if (data[i].query.indexOf(query) > -1) {
        alreadyQuery = true;
        break;
      }
    }

    return alreadyQuery;
  };

  const getRootId = () => {
    return '0-1';
  };

  const setAppearanceSelectedNode = (data, nodeId) => {
    if (data) {
      data.forEach(item => {
        if (item.nodeId !== getRootId()) {
          let appearance = {};
          if (item.nodeId === nodeId) {
            appearance = getSelectingAppearance();
            item.backgroundColor = appearance.backgroundColor;
          } else {
            appearance = getUnselectingAppearance();
            item.backgroundColor = appearance.backgroundColor;
          }
        }
      });
    }
  };

  const getUnselectingAppearance = () => {
    return {
      backgroundColor: {
        red: 0,
        green: 0,
        blue: 0,
        alpha: 1
      }
    };
  };

  const getSelectingAppearance = () => {
    return {
      backgroundColor: {
        red: 0,
        green: 181,
        blue: 1,
        alpha: 173
      }
    };
  };

  const getNodeImage = url => {
    return {
      nodeImage: {
        url: url || './images/user.png',
        width: 100,
        height: 100,
        cornerShape: 'CIRCLE',
        shadow: true,
        zIndex: 99999999
      }
    };
  };

  const getNodeImageByField = name => {
    let imgSrc = '';
    switch (name) {
      case 'digFinCodeId':
        imgSrc = './images/f.png';
        break;
      default:
        break;
    }
    return imgSrc;
  };

  const getRootNode = () => {
    return {
      nodeId: getRootId(),
      parentNodeId: null,
      query: '',
      width: 300,
      height: 80,
      borderWidth: 0,
      borderRadius: 10,
      borderColor: {
        red: 250,
        green: 250,
        blue: 250,
        alpha: 1
      },
      backgroundColor: {
        red: 250,
        green: 250,
        blue: 250,
        alpha: 1
      },
      template: `<div><h2 class="ui header"><i aria-hidden="true" class="cubes icon"></i><div class="content">Subject Distribution</div></h2></div>`,
      connectorLineColor: {
        red: 255,
        green: 255,
        blue: 255,
        alpha: 0
      },
      connectorLineWidth: 5,
      dashArray: '',
      expanded: false,
      directSubordinates: 1,
      totalSubordinates: 413
    };
  };

  const build = ({
    id,
    parentId,
    label,
    count,
    query,
    item,
    map,
    backgroundColor,
    nodeImage,
    rate
  }) => {
    return {
      nodeId: id,
      parentNodeId: parentId,
      query: query,
      item,
      map: map,
      count,
      rate,
      width: 230,
      height: 100,
      borderWidth: 1,
      borderRadius: 5,
      borderColor: {
        red: 15,
        green: 140,
        blue: 121,
        alpha: 1
      },
      backgroundColor: {
        red: 0,
        green: 0,
        blue: 0,
        alpha: 1,
        ...backgroundColor
      },
      template: `<div style="text-align:center;height:100%">
        <div style="margin-top:10px;font-size:30px;font-weight:bold;">${count}
          <span style="font-size:20px;font-weight:bold;color:red">${
            rate === void 0 ? '' : rate.toFixed(1) + '%'
          }</span>
        </div>
        <div style="font-size:20px;bottom:20px;">
          <div>${label}</div>
        </div>
      </div>`,
      connectorLineColor: {
        red: 220,
        green: 189,
        blue: 207,
        alpha: 1
      },
      connectorLineWidth: 5,
      dashArray: '',
      expanded: true,
      directSubordinates: 13,
      totalSubordinates: 413,
      nodeImage: nodeImage
    };
  };

  return {
    getId,
    getRootId,
    getRootNode,
    getChildren,
    getNodeById,
    clearChildrenNode,
    findAllDeeplyParents,
    hasAlreadyQuery,
    setAppearanceSelectedNode,
    getNodeImage,
    getNodeImageByField,
    build,
    GenerateNodeIds,
    getSelectingAppearance
  };
};

export default useNode;
