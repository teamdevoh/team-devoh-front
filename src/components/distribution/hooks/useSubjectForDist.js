import api from '../api';

const useSubjectForDist = () => {
  const fetch = (projectSiteId, query) => {
    return api.fetchSubjectsForDist(projectSiteId, query);
  };

  return { fetch };
};

export default useSubjectForDist;
