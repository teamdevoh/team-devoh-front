import { useEffect, useState } from 'react';
import api from '../api';

const useSocByAdr = projectSiteId => {
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchSocsAdr(projectSiteId);
    if (res && res.data) {
      setItems(res.data);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectSiteId]
  );

  return { items };
};

export default useSocByAdr;
