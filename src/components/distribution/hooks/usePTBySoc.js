import { useEffect, useState } from 'react';
import api from '../api';

const usePTBySoc = (projectSiteId, selectedItem) => {
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchPTsAdr(projectSiteId, selectedItem.value);
    if (res && res.data) {
      setItems(res.data);
    }
  };

  useEffect(
    () => {
      if (selectedItem.field === 'soc') {
        fetchItems();
      }
    },
    [projectSiteId, selectedItem]
  );

  return { items };
};

export default usePTBySoc;
