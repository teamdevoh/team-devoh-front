import { useEffect, useState } from 'react';
import api from '../api';

const useRegimen = (projectSiteId, selectedItem) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchRegimenNames(
        projectSiteId,
        selectedItem.value
      );
      if (res && res.data) {
        let regimens = [];

        res.data.forEach(value =>
          regimens.push({
            key: `'${value}'`,
            value: `'${value}'`,
            text: value
          })
        );
        setItems(regimens);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      if (selectedItem.field === 'tbdrugId') {
        fetchItems();
      }
    },
    [projectSiteId, selectedItem]
  );

  return { items, loading };
};

export default useRegimen;
