import React, { Fragment } from 'react';
import { Header, Image } from 'semantic-ui-react';
import { Modal } from '../modal';

const DistHelp = ({ show, onClose }) => {
  return (
    <Modal
      open={show}
      onOK={onClose}
      title="Help"
      content={
        <Fragment>
          <Header>How to add child nodes</Header>
          <Image src={'/images/node_add.gif'} />
          <Header>How to remove all child nodes</Header>
          <Image src={'/images/node_remove.gif'} />
        </Fragment>
      }
    />
  );
};

export default DistHelp;
