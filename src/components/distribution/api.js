import helpers from '../../helpers';

const fetchSubjectsForDist = (projectSiteId, query) => {
  const queryFilter = query ? `$filter=${query}` : '';

  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/subjects/dist?${queryFilter}`
  );
};

const fetchSocsAdr = projectSiteId => {
  return helpers.Service.get(`api/projectsites/${projectSiteId}/adrs/soc`);
};

const fetchPTsAdr = (projectSiteId, aesoccd) => {
  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/adrs/pt?aesoccd=${aesoccd}`
  );
};

const fetchAdrNamesAdr = (projectSiteId, aeptcd) => {
  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/adrs/adrname?aeptcd=${aeptcd}`
  );
};

const fetchRegimenNames = (projectSiteId, tbdrugId) => {
  const params = helpers.qs.stringify({
    tbdrugId
  });
  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/subjects/dist/regimens?${params}`
  );
};

const fetchSubjectAtccodeList = () => {
  return helpers.Service.get(`api/subjects/subcms/cmdecods`);
};

const fetchATCCodes = () => {
  return helpers.Service.get(`api/subjects/subcms/atccode`);
};

export default {
  fetchSubjectsForDist,
  fetchSocsAdr,
  fetchPTsAdr,
  fetchAdrNamesAdr,
  fetchRegimenNames,
  fetchSubjectAtccodeList,
  fetchATCCodes
};
