import React, { Fragment } from 'react';
import { Breadcrumb } from 'semantic-ui-react';

const DistBreadcrumbs = ({ items, id }) => {
  return (
    <Breadcrumb>
      {items.map((item, index) => {
        return (
          <Fragment key={item.value}>
            <Breadcrumb.Section link active={item.value === id}>
              {item.name}
            </Breadcrumb.Section>
            <Breadcrumb.Divider icon="right chevron" />
          </Fragment>
        );
      })}
    </Breadcrumb>
  );
};

export default DistBreadcrumbs;
