import React, { useLayoutEffect, useRef } from 'react';
import TreeChart from 'd3-org-chart';
import { useNode } from './hooks';

const DistChart = ({ data, onNodeClick, load }, ref) => {
  const d3Container = useRef(null);
  const node = useNode();
  let chart = null;

  const handleNodeItemClick = nodeId => {
    if (nodeId !== node.getRootId()) {
      onNodeClick(nodeId, chart);
    }
  };

  useLayoutEffect(
    () => {
      if (data && d3Container.current) {
        if (!chart) {
          chart = new TreeChart();
        }
        chart
          .container(d3Container.current)
          .data(data)
          .svgHeight(700)
          .svgWidth(500)
          .initialZoom(0.4)
          .onNodeClick(handleNodeItemClick)
          .render();
        load(chart);
      }
    },
    [data, d3Container.current]
  );

  return (
    <div>
      <div ref={d3Container} />
    </div>
  );
};

export default DistChart;
