import React, { useCallback, useEffect, useState } from 'react';
import DistChart from './DistChart';
import { useSubjectForDist, useNode } from './hooks';
import ItemForm from './ItemForm';
import helpers from '../../helpers';
import {
  Grid,
  Button,
  Header,
  Icon,
  Segment,
  Dimmer,
  Loader
} from 'semantic-ui-react';
import { ProjectSite } from '../collection/Selector';
import { useParams } from 'react-router-dom';
import * as d3 from 'd3';
import { useBoolean, useFormatMessage } from '../../hooks';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';
import DistBreadcrumbs from './DistBreadcrumbs';
import DistHelp from './DistHelp';
import { toast } from 'react-toastify';
import saveSvgAsPng from 'save-svg-as-png';
import './DistContainer.css';

const DistContainer = () => {
  const t = useFormatMessage();
  const { projectId } = useParams();
  const [projectSiteId, setProjectSiteId] = useState(0);
  const [chartData, setChartData] = useState(null);
  const loadingChart = useBoolean(false);
  const dist = useSubjectForDist();
  const node = useNode();
  const [selectedNodeId, setSelectedNodeId] = useState('');
  const [selectedItem, setSelectedItem] = useState({});
  const [chart, setChart] = useState(null);
  const [map, setMap] = useState([]);
  const help = useBoolean(false);
  const exportSvgLoading = useBoolean(false);
  const onlyGreaterThanZero = useBoolean(false);

  const handleSelectedNodeClick = useCallback((nodeId, chart) => {
    const selectedItem = node.getNodeById(chart.data(), nodeId);
    setMap(selectedItem.map);
    setSelectedNodeId(nodeId);
    setSelectedItem(selectedItem.item);
  }, []);

  const removeAllChildNodes = useCallback(
    () => {
      node.clearChildrenNode(selectedNodeId, chart);
    },
    [selectedNodeId, chartData, chart]
  );

  const getOdataQuery = ({ type, field, value }) => {
    if (type === 'equal') {
      return helpers.oDataBuilder.equal(field, value);
    }

    if (type === 'between') {
      const values = value.split('-');
      const from = values[0];
      const to = values[1];
      return helpers.oDataBuilder.between(field, from, to);
    }

    if (type === 'any') {
      return helpers.oDataBuilder.any(field, value);
    }

    if (type === 'contains') {
      return helpers.oDataBuilder.contains(field, value);
    }
  };

  const transformSelectingNode = item => {
    const rect = item.parentElement.getElementsByClassName('node-rect')[0];

    rect.classList.add('selectedNode');
  };

  const transformUnselectingNode = item => {
    const rect = item.parentElement.getElementsByClassName('node-rect')[0];

    rect.classList.remove('selectedNode');
  };

  const appendNodeClickEvent = () => {
    const obj = d3.selectAll('.node-foreign-object');
    obj.on('click', (target, i, nodes) => {
      nodes.forEach((item, index) => {
        if (item.__data__.id !== node.getRootId()) {
          if (index === i) {
            transformSelectingNode(item);
          } else {
            if (index > 0) {
              transformUnselectingNode(item);
            }
          }
        }
      });
    });
  };

  const handleItemChange = useCallback(
    (event, data) => {
      const parent = node.getNodeById(chart.data(), selectedNodeId);

      if (parent) {
        const children = node.getChildren(chart.data(), selectedNodeId);

        const nodeIds = node.GenerateNodeIds(
          chart.data(),
          selectedNodeId,
          data.options.length
        );

        const parentQuery = parent.query
          ? `${parent.query} and `
          : parent.query;

        const nestedParents = node.findAllDeeplyParents(
          selectedNodeId,
          chart.data()
        );

        const isAlreadyInParent = node.hasAlreadyQuery(
          nestedParents,
          data.field
        );

        if (isAlreadyInParent === false) {
          let nodes = [];
          let reqCount = 0;
          let zeroCount = 0;
          let already = false;

          loadingChart.setTrue();
          data.options.forEach(async (option, index) => {
            let odataQuery = '';
            if (data.generateOData) {
              odataQuery = data.generateOData(option.value);
            } else {
              odataQuery = getOdataQuery({
                type: data.type,
                field: data.field,
                value: option.value
              });
            }

            const query = `${parentQuery}${odataQuery}`;
            const ok = !node.hasAlreadyQuery(children, odataQuery);

            if (ok) {
              reqCount++;

              try {
                const res = await dist.fetch(projectSiteId, query);

                if (res && res.data) {
                  if (onlyGreaterThanZero.value && res.data.length === 0) {
                    zeroCount++;
                  }

                  if (
                    onlyGreaterThanZero.value
                      ? res.data.length > 0
                      : res.data.length > -1
                  ) {
                    const imgSrc = node.getNodeImageByField(data.field);

                    let itemCount = res.data.length;
                    if (data.itemCount) {
                      itemCount = data.itemCount({
                        items: res.data,
                        option
                      });
                    }

                    nodes.push(
                      node.build({
                        id: nodeIds[index],
                        parentId: selectedNodeId,
                        count: itemCount,
                        label: option.text,
                        query: query,
                        rate:
                          res.data.length > 0
                            ? (itemCount / parent.count) * 100
                            : 0,
                        item: { field: data.field, value: option.value },
                        map: [
                          ...parent.map,
                          { name: option.text, value: nodeIds[index] }
                        ],
                        nodeImage: imgSrc
                          ? node.getNodeImage(imgSrc).nodeImage
                          : null
                      })
                    );
                  }

                  if (reqCount === nodes.length + zeroCount) {
                    loadingChart.setFalse();
                    if (nodes.length === 0) {
                      return toast.warn(t('no.data'));
                    }
                    if (zeroCount > 0) {
                      toast.info(
                        t('excluding.item.count', { count: zeroCount })
                      );
                    }
                    _.orderBy(nodes, 'count', 'desc').forEach(node => {
                      chart.addNode(node);
                    });
                    appendNodeClickEvent();
                  }
                }
              } catch (error) {
                loadingChart.setFalse();
              }
            } else {
              loadingChart.setFalse();
              already = true;
            }
          });

          if (already) {
            toast.warn(t('already.exists'));
          }
        } else {
          toast.warn(t('already.exists'));
        }
      }
    },
    [chart, chartData, selectedNodeId, onlyGreaterThanZero.value]
  );

  const initLoad = useCallback(
    async () => {
      try {
        loadingChart.setTrue();

        const res = await dist.fetch(projectSiteId, '');
        if (res && res.data) {
          let nodes = [node.getRootNode()];
          const topId = node.getId(1, 1);
          const nodeImage = node.getNodeImage();
          const label = 'Total';

          nodes.push(
            node.build({
              id: topId,
              parentId: node.getRootId(),
              count: res.data.length,
              label: label,
              query: '',
              map: [{ name: label, value: topId }],
              ...nodeImage
            })
          );

          setSelectedNodeId(topId);
          setChartData(nodes);
          setMap([{ name: label, value: topId }]);

          const obj = d3.selectAll('.node-foreign-object');
          transformSelectingNode(obj._groups[0][1]);
        }
      } catch (error) {
      } finally {
        loadingChart.setFalse();
      }
    },
    [chartData, projectSiteId, chart]
  );

  useEffect(
    () => {
      initLoad();
    },
    [projectSiteId]
  );

  const handleLoadedChart = useCallback(chartObj => {
    setChart(chartObj);
  }, []);

  appendNodeClickEvent();

  const handleTestClick = async () => {
    try {
      exportSvgLoading.setTrue();

      const canvas = document.getElementsByClassName('svg-chart-container')[0];
      const url = await saveSvgAsPng.svgAsPngUri(canvas, {
        scale: 5,
        encoderOptions: 1,
        backgroundColor: 'white'
      });
      helpers.util.watermarkLogo(
        url,
        img => {
          exportSvgLoading.setFalse();
          saveSvgAsPng.download('Distribution.png', img.src);
        },
        100,
        100,
        1200,
        180
      );
    } catch (error) {
      exportSvgLoading.setFalse();
    }
  };

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column>
          <Header as="h3" dividing>
            Distribution
            <Header.Subheader>Distribution</Header.Subheader>
            <CopyRedirectUrl />
          </Header>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="five">
          <Grid>
            <Grid.Row>
              <Grid.Column width="sixteen">
                <ProjectSite
                  name="projectSiteId"
                  value={0}
                  projectId={projectId}
                  onChange={(event, data) => {
                    setSelectedNodeId('');
                    setChartData(null);
                    setChart(null);
                    setProjectSiteId(data.value);
                    d3.select('g').remove();
                  }}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <ItemForm
                  projectId={projectId}
                  projectSiteId={projectSiteId}
                  selectedItem={selectedItem}
                  onChange={handleItemChange}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
        <Grid.Column width="eleven">
          <Grid.Row>
            <Grid.Column>
              <DistHelp show={help.value} onClose={help.setFalse} />
              <Button
                icon
                labelPosition="right"
                color="twitter"
                onClick={onlyGreaterThanZero.toggle}
              >
                {t('excluding.count.zero')}
                <Icon
                  name="check"
                  color={onlyGreaterThanZero.value ? 'yellow' : 'grey'}
                />
              </Button>
              <Button.Group>
                <Button animated="vertical" secondary onClick={help.toggle}>
                  <Button.Content hidden>{t('help')}</Button.Content>
                  <Button.Content visible>
                    <Icon name="help" />
                  </Button.Content>
                </Button>
                <Button
                  animated="vertical"
                  disabled={exportSvgLoading.value}
                  loading={exportSvgLoading.value}
                  onClick={handleTestClick}
                  primary
                >
                  <Button.Content hidden>{t('common.export')}</Button.Content>
                  <Button.Content visible>
                    <Icon name="download" />
                  </Button.Content>
                </Button>
                <Button
                  fluid
                  animated="vertical"
                  onClick={removeAllChildNodes}
                  color="teal"
                >
                  <Button.Content hidden>
                    {t('remove.all.child.nodes')}
                  </Button.Content>
                  <Button.Content visible>
                    <Icon name="undo" />
                  </Button.Content>
                </Button>
              </Button.Group>
            </Grid.Column>
            <Grid.Column>
              <DistBreadcrumbs items={map} id={selectedNodeId} />
            </Grid.Column>
            <Grid.Column>
              <Segment basic style={{ height: '700px' }}>
                <Dimmer active={loadingChart.value} inverted>
                  <Loader inverted content="Loading" />
                </Dimmer>
                <DistChart
                  load={handleLoadedChart}
                  onNodeClick={handleSelectedNodeClick}
                  data={chartData}
                />
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default DistContainer;
