import React, { useCallback, useEffect, useState } from 'react';
import { Table, Dropdown } from 'semantic-ui-react';
import { useCodes, useFormatMessage, useProjectSite } from '../../hooks';
import {
  useAdrNameByPT,
  useExTbDrug,
  usePTBySoc,
  useRegimen,
  useSocByAdr,
  useSubjectAtccodeList
} from './hooks';
import { codeGroup } from '../../constants';
import util from '../../helpers/util';
import api from './api';

const ItemForm = ({
  projectId,
  projectSiteId,
  selectedItem = { field: '', value: '' },
  onChange
}) => {
  const [treatmentCodes] = useCodes({
    codeGroupId: codeGroup.TBTreatmentCode
  });
  const [diagnosisCodes] = useCodes({
    codeGroupId: codeGroup.TBDiagnosisCode
  });
  const [subjectStatusCodes] = useCodes({
    codeGroupId: codeGroup.SubjectStatus
  });
  const [sites] = useProjectSite({ projectId });

  const [caseConCodes] = useCodes({ codeGroupId: codeGroup.CaseconCode });
  const [sexCodes] = useCodes({ codeGroupId: codeGroup.SexCode });
  const [smokeCodes] = useCodes({ codeGroupId: codeGroup.SmokeCode });
  const [ethnicCodes] = useCodes({ codeGroupId: codeGroup.EthnicCode });
  const soc = useSocByAdr(projectSiteId);
  const pt = usePTBySoc(projectSiteId, selectedItem);
  const adrname = useAdrNameByPT(projectSiteId, selectedItem);
  const [susceptCodes] = useCodes({ codeGroupId: codeGroup.TBSusceptCode });
  const [digFinCodes] = useCodes({ codeGroupId: codeGroup.DigFinCode });
  const [natgenCodes] = useCodes({ codeGroupId: codeGroup.NatGenCode });
  const [nat2PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Nat2PhenResCode
  });
  const [slcob1GenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1GenResCode
  });
  const [slcob1PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1PhenResCode
  });
  const [comorbidCodes] = useCodes({ codeGroupId: codeGroup.ComorbidCode });
  const exTbDrug = useExTbDrug();
  const regimen = useRegimen(projectSiteId, selectedItem);
  const [{ items: subjectAtccodeList }] = useSubjectAtccodeList();

  const [atcCodes, setAtcCodes] = useState({});
  const [cmdecodOptions, setCmdecodOptions] = useState([]);
  const [cmdecodSubOptions, setCmdecodSubOptions] = useState([]);
  const [cmdecodSub2Options, setCmdecodSub2Options] = useState([]);

  const t = useFormatMessage();

  const getAtcCodeName = useCallback(
    code => {
      return atcCodes[code.toLowerCase()] || '';
    },
    [atcCodes]
  );

  useEffect(() => {
    const ffetch = async () => {
      const res = await api.fetchATCCodes();

      if (res && res.data) {
        setAtcCodes(res.data);
      }
    };

    ffetch();
  }, []);

  useEffect(
    () => {
      const Charactes = new Set();

      for (let i = 0; i < subjectAtccodeList.length; i++) {
        const cur = subjectAtccodeList[i];
        const firstCharacter = cur[0];

        Charactes.add(firstCharacter.toUpperCase());
      }

      const newItems = [];
      Charactes.forEach(c => {
        let atcCode = getAtcCodeName(c);
        atcCode = !!atcCode ? `(${atcCode})` : '';

        newItems.push({
          key: c,
          text: `${c} ${atcCode}`,
          value: c
        });
      });

      newItems.sort((a, b) => {
        return util.compareString(a.text, b.text);
      });

      setCmdecodOptions(newItems);
    },
    [subjectAtccodeList, atcCodes]
  );

  useEffect(
    () => {
      if (selectedItem.field === 'cmdecod') {
        const selectedChar = selectedItem.value.toLowerCase();
        const temp = new Set();

        for (let i = 0; i < subjectAtccodeList.length; i++) {
          const cur = subjectAtccodeList[i];
          const value = cur.substr(0, 3);

          if (value[0].toLowerCase() === selectedChar) {
            temp.add(value);
          }
        }

        const newItems = [];
        temp.forEach(c => {
          let atcCode = getAtcCodeName(c);
          atcCode = !!atcCode ? `(${atcCode})` : '';

          newItems.push({
            key: c,
            text: `${c} ${atcCode}`,
            value: c
          });
        });

        newItems.sort((a, b) => {
          return util.compareString(a.text, b.text);
        });

        setCmdecodSubOptions(newItems);
      } else if (selectedItem.field === 'cmdecodSub') {
        const selectedChar = selectedItem.value.toLowerCase();
        const temp = new Set();

        for (let i = 0; i < subjectAtccodeList.length; i++) {
          const cur = subjectAtccodeList[i];
          const value = cur.substr(0, 5);
          const compareValue = value.substr(0, 3);

          if (compareValue.toLowerCase() === selectedChar) {
            temp.add(value);
          }
        }

        const newItems = [];
        temp.forEach(c => {
          let atcCode = getAtcCodeName(c);
          atcCode = !!atcCode ? `(${atcCode})` : '';

          newItems.push({
            key: c,
            text: `${c} ${atcCode}`,
            value: c
          });
        });

        newItems.sort((a, b) => {
          return util.compareString(a.text, b.text);
        });

        setCmdecodSub2Options(newItems);
      }
    },
    [selectedItem.field, selectedItem.value, atcCodes]
  );

  const generateOData = useCallback(value => {
    return `Cmdecod/any(a: indexof(tolower(a), '${value.toLowerCase()}') eq 0)`;
  }, []);

  const itemCount = useCallback(({ items, option }) => {
    let { value } = option;
    value = value.toLowerCase();
    let count = 0;
    for (let i = 0; i < items.length; i++) {
      const { cmdecod } = items[i];

      for (let n = 0; n < cmdecod.length; n++) {
        const curValue = cmdecod[n].toLowerCase();
        if (curValue.indexOf(value) === 0) {
          count++;
        }
      }
    }

    return count;
  }, []);

  const handleChange = useCallback(
    (event, data) => {
      const filteredOptions =
        data.value === 0
          ? _.filter(
              data.options,
              option => option.value > 0 || option.value > ''
            )
          : _.filter(data.options, option => option.value === data.value);

      let options = [];

      filteredOptions.forEach(option => {
        options.push({
          value: option.value,
          text: option.text
        });
      });

      onChange(event, {
        field: data.name,
        type: data.type === void 0 ? 'equal' : data.type,
        options: options,
        generateOData:
          data.name === 'cmdecod' ||
          data.name === 'cmdecodSub' ||
          data.name === 'cmdecodSub2'
            ? generateOData
            : null,
        itemCount:
          data.name === 'cmdecod' ||
          data.name === 'cmdecodSub' ||
          data.name === 'cmdecodSub2'
            ? itemCount
            : null
      });
    },
    [onChange]
  );

  const BuildSelection = ({ name, type, options, loading }) => {
    return (
      <Dropdown
        name={name}
        type={type}
        fluid
        selection
        search
        value=""
        options={options}
        onChange={handleChange}
        loading={loading}
        disabled={loading}
      />
    );
  };

  return (
    <div className="distItemScroll">
      <Table color="teal">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="seven">Item</Table.HeaderCell>
            <Table.HeaderCell width="nine">Value</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell>{t('project.member.site')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'projectSiteId',
                options: [{ key: 0, text: 'All', value: 0 }, ...sites]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('status')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'subjectStatusCode',
                options: [
                  { key: 0, text: 'All', value: 0 },
                  ...subjectStatusCodes
                ]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>
              {t('activity.caseConclusion.caseconCodeId')}
            </Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'caseconCodeId',
                options: [{ key: 0, text: 'All', value: 0 }, ...caseConCodes]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('activity.interview.sexcodeId')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'sexCodeId',
                options: [{ key: 0, text: 'All', value: 0 }, ...sexCodes]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('activity.interview.smokecodeId')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'smokecodeId',
                options: [{ key: 0, text: 'All', value: 0 }, ...smokeCodes]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('activity.interview.age')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'age',
                type: 'between',
                options: [
                  { key: 0, text: 'All', value: 0 },
                  {
                    key: '0-13',
                    text: '0 ~ 13세',
                    value: '0-13'
                  },
                  {
                    key: '14-18',
                    text: '14세 ~ 18세',
                    value: '14-18'
                  },
                  {
                    key: '19-63',
                    text: '19세 ~ 63세',
                    value: '19-63'
                  },
                  {
                    key: '64-999',
                    text: '64세 이상',
                    value: '64-999'
                  }
                ]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('activity.body.bmi')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'bmi',
                type: 'between',
                options: [
                  { key: 0, text: 'All', value: 0 },
                  {
                    key: '0-18.5',
                    text: '저체중( ~ 18.5kg/m2)',
                    value: '0-18.5'
                  },
                  {
                    key: '18.6-23',
                    text: '정상(18.6~23KG/m2)',
                    value: '18.6-23'
                  },
                  {
                    key: '23.1-25',
                    text: '과제충(23.1~25KG/m2)',
                    value: '23.1-25'
                  },
                  {
                    key: '25.1-30',
                    text: '비만(25.1~30KG/m2)',
                    value: '25.1-30'
                  },
                  {
                    key: '30.1-100',
                    text: '고도비만(30.1~)',
                    value: '30.1-100'
                  }
                ]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('activity.interview.ethniccodeId')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'ethniccodeId',
                options: [{ key: 0, text: 'All', value: 0 }, ...ethnicCodes]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>ADR</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'hasAdr',
                options: [
                  { key: 0, text: 'All', value: 0 },
                  {
                    key: 'true',
                    text: 'ADR Yes',
                    value: 'true'
                  },
                  {
                    key: 'false',
                    text: 'ADR No',
                    value: 'false'
                  }
                ]
              })}
            </Table.Cell>
          </Table.Row>
          {selectedItem.field !== 'soc' &&
            selectedItem.field !== 'pt' &&
            selectedItem.field !== 'adrName' && (
              <Table.Row>
                <Table.Cell>ADR Term SOC</Table.Cell>
                <Table.Cell>
                  {BuildSelection({
                    name: 'soc',
                    type: 'any',
                    options: [{ key: 0, text: 'All', value: 0 }, ...soc.items]
                  })}
                </Table.Cell>
              </Table.Row>
            )}
          {selectedItem.field === 'soc' && (
            <Table.Row>
              <Table.Cell>ADR Term PT</Table.Cell>
              <Table.Cell>
                {BuildSelection({
                  name: 'pt',
                  type: 'any',
                  options: [{ key: 0, text: 'All', value: 0 }, ...pt.items]
                })}
              </Table.Cell>
            </Table.Row>
          )}
          {selectedItem.field === 'pt' && (
            <Table.Row>
              <Table.Cell>ADR Term Name</Table.Cell>
              <Table.Cell>
                {BuildSelection({
                  name: 'adrName',
                  type: 'any',
                  options: [{ key: 0, text: 'All', value: 0 }, ...adrname.items]
                })}
              </Table.Cell>
            </Table.Row>
          )}
          <Table.Row>
            <Table.Cell>{t('activity.diagnosis.tbTreatment')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'tbTreatmentcodeId',
                options: [{ key: 0, text: 'All', value: 0 }, ...treatmentCodes]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('activity.diagnosis.tbDiagnosiscodeId')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'tbDiagnosiscodeId',
                options: [{ key: 0, text: 'All', value: 0 }, ...diagnosisCodes]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('activity.diagnosis.name')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'tbSusceptcodeId',
                options: [{ key: 0, text: 'All', value: 0 }, ...susceptCodes]
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>{t('activity.caseConclusion.digFinCodeId')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'digFinCodeId',
                options: [{ key: 0, text: 'All', value: 0 }, ...digFinCodes]
              })}
            </Table.Cell>
          </Table.Row>

          {selectedItem.field === 'nat2PhenResCodeId' && (
            <Table.Row>
              <Table.Cell>
                {t('activity.genotyping.nat2GenResCodeId')}
              </Table.Cell>
              <Table.Cell>
                {BuildSelection({
                  name: 'nat2GenResCodeId',
                  options: [{ key: 0, text: 'All', value: 0 }, ...natgenCodes]
                })}
              </Table.Cell>
            </Table.Row>
          )}
          {selectedItem.field !== 'nat2GenResCodeId' &&
            selectedItem.field !== 'nat2PhenResCodeId' && (
              <Table.Row>
                <Table.Cell>
                  {t('activity.genotyping.nat2PhenResCodeId')}
                </Table.Cell>
                <Table.Cell>
                  {BuildSelection({
                    name: 'nat2PhenResCodeId',
                    options: [
                      { key: 0, text: 'All', value: 0 },
                      ...nat2PhenResCodes
                    ]
                  })}
                </Table.Cell>
              </Table.Row>
            )}
          {selectedItem.field !== 'slco1b1GenResCodeId' &&
            selectedItem.field !== 'slco1b1PhenResCodeId' && (
              <Table.Row>
                <Table.Cell>
                  {t('activity.genotyping.slco1b1PhenResCodeId')}
                </Table.Cell>
                <Table.Cell>
                  {BuildSelection({
                    name: 'slco1b1PhenResCodeId',
                    options: [
                      { key: 0, text: 'All', value: 0 },
                      ...slcob1PhenResCodes
                    ]
                  })}
                </Table.Cell>
              </Table.Row>
            )}
          {selectedItem.field === 'slco1b1PhenResCodeId' && (
            <Table.Row>
              <Table.Cell>
                {t('activity.genotyping.slco1b1GenResCodeId')}
              </Table.Cell>
              <Table.Cell>
                {BuildSelection({
                  name: 'slco1b1GenResCodeId',
                  options: [
                    { key: 0, text: 'All', value: 0 },
                    ...slcob1GenResCodes
                  ]
                })}
              </Table.Cell>
            </Table.Row>
          )}
          <Table.Row>
            <Table.Cell>{t('activity.comorbid.comorbidcodeId')}</Table.Cell>
            <Table.Cell>
              {BuildSelection({
                name: 'comorbidcodeId',
                type: 'any',
                options: [{ key: 0, text: 'All', value: 0 }, ...comorbidCodes]
              })}
            </Table.Cell>
          </Table.Row>
          {selectedItem.field !== 'tbdrugId' &&
            selectedItem.field !== 'regimenName' && (
              <Table.Row>
                <Table.Cell>TDM Drug</Table.Cell>
                <Table.Cell>
                  {BuildSelection({
                    name: 'tbdrugId',
                    type: 'any',
                    options: [
                      { key: 0, text: 'All', value: 0 },
                      ...exTbDrug.items
                    ],
                    loading: exTbDrug.loading
                  })}
                </Table.Cell>
              </Table.Row>
            )}
          {selectedItem.field === 'tbdrugId' && (
            <Table.Row>
              <Table.Cell>TDM Regimen</Table.Cell>
              <Table.Cell>
                {BuildSelection({
                  name: 'regimenName',
                  options: [
                    { key: 0, text: 'All', value: 0 },
                    ...regimen.items
                  ],
                  loading: regimen.loading
                })}
              </Table.Cell>
            </Table.Row>
          )}
          {selectedItem.field !== 'cmdecodSub2' && (
            <Table.Row>
              <Table.Cell>{t('activity.cm.title')}</Table.Cell>
              <Table.Cell>
                {selectedItem.field === 'cmdecod' ||
                selectedItem.field === 'cmdecodSub'
                  ? selectedItem.field === 'cmdecodSub'
                    ? BuildSelection({
                        name: 'cmdecodSub2',
                        options: [
                          { key: 0, text: 'All', value: 0 },
                          ...cmdecodSub2Options
                        ]
                      })
                    : BuildSelection({
                        name: 'cmdecodSub',
                        options: [
                          { key: 0, text: 'All', value: 0 },
                          ...cmdecodSubOptions
                        ]
                      })
                  : BuildSelection({
                      name: 'cmdecod',
                      options: [
                        { key: 0, text: 'All', value: 0 },
                        ...cmdecodOptions
                      ]
                    })}
              </Table.Cell>
            </Table.Row>
          )}
        </Table.Body>
      </Table>
    </div>
  );
};

export default ItemForm;
