import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { shared } from '../../states';
import { LogoWrapper } from './styles';

const Logo = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(shared.actions.activeMenu('console', 'Console'));
    history.push('/');
  };

  return (
    <LogoWrapper onClick={handleClick}>Smart R&amp;D Workstation</LogoWrapper>
  );
};

export default Logo;
