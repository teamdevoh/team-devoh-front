import { useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';

const useAttachList = ({ subjectId, activityKeyId, projectActivityId }) => {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchAttachments({
      subjectId,
      activityKeyId,
      projectActivityId
    });
    let items = [];
    await res.data.forEach(async item => {
      const fileObj = await helpers.file.setFileInfo({
        ...item,
        id: item.activityAttachmentId,
        thumbnailApiUrl: `api/subjects/{subjectId}/activities/{activityKeyId}/attachs.thumbnail/${
          item.activityAttachmentId
        }`
      });
      items.push(fileObj);
    });

    setItems(items);
    setLoading(false);
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, activityKeyId, projectActivityId]
  );

  return [{ items, loading, fetch: fetchItems }, setItems];
};

export default useAttachList;
