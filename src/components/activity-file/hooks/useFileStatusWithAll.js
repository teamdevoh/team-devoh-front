import api from '../api';

const useFileStatusWithAll = ({ subjectId }) => {
  const getFileStatus = items => {
    return Promise.all(
      items.map(async item => {
        const res = await api.fetchActivityFileStatus({
          subjectId,
          activityKeyId: item.activityKeyId,
          projectActivityId: item.projectActivityId
        });

        item['hasFile'] = false;
        if (res && res.data) {
          item['hasFile'] = res.data;
        }

        return item;
      })
    );
  };

  return { getFileStatus };
};

export default useFileStatusWithAll;
