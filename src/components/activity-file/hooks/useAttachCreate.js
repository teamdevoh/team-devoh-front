import { useState } from 'react';
import api from '../api';

const useAttachCreate = ({ subjectId, activityKeyId, projectActivityId }) => {
  const [loading, setLoading] = useState(false);

  const add = async model => {
    setLoading(true);
    const res = await api.addAttachment({
      subjectId,
      activityKeyId,
      projectActivityId,
      model
    });
    setLoading(false);
    if (res && res.data) {
      return res.data;
    }
    return {};
  };

  return { add, loading };
};

export default useAttachCreate;
