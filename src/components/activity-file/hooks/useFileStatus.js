import { useEffect } from 'react';
import api from '../api';
import { useBoolean } from '../../../hooks';

const useFileStatus = ({ subjectId, activityKeyId, projectActivityId }) => {
  const hasFile = useBoolean(false);
  const isLoading = useBoolean(true);

  const fetchItem = async () => {
    const res = await api.fetchActivityFileStatus({
      subjectId,
      activityKeyId,
      projectActivityId
    });
    if (res) {
      hasFile.setValue(res.data);
    }
    isLoading.setFalse();
  };

  useEffect(
    () => {
      if (subjectId > 0 && projectActivityId > 0 && activityKeyId !== void 0) {
        fetchItem();
      }
    },
    [subjectId, activityKeyId, projectActivityId]
  );

  return [
    { hasFile: hasFile.value, loading: isLoading.value },
    hasFile.setValue
  ];
};

export default useFileStatus;
