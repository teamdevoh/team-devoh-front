import { useState } from 'react';
import api from '../api';

const useTagUpdate = ({ subjectId, activityKeyId }) => {
  const [loading, setLoading] = useState(false);

  const update = async (activityAttachmentId, model) => {
    setLoading(true);
    const res = await api.updateTag({
      subjectId,
      activityKeyId,
      activityAttachmentId,
      model
    });
    setLoading(true);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  return { update, loading };
};

export default useTagUpdate;
