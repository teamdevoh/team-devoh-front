import { useState, useCallback } from 'react';
import helpers from '../../../helpers';

const useAttachDownload = ({ subjectId, activityKeyId }) => {
  const [loading, setLoading] = useState(false);

  const download = useCallback(async activityAttachmentId => {
    setLoading(true);
    helpers.util
      .download(
        `api/subjects/${subjectId}/activities/${activityKeyId}/attachs/${activityAttachmentId}`,
        percent => {
          if (percent === 100) {
            setLoading(false);
          }
        }
      )
      .catch(err => {
        setLoading(false);
      });
  }, []);

  return { download, loading };
};

export default useAttachDownload;
