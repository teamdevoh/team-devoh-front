import { useState } from 'react';
import api from '../api';

const useAttachRemove = ({ subjectId, activityKeyId }) => {
  const [loading, setLoading] = useState(false);

  const remove = async activityAttachmentId => {
    setLoading(true);
    const res = await api.removeAttachment({
      subjectId,
      activityKeyId,
      activityAttachmentId
    });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  return { remove, loading };
};

export default useAttachRemove;
