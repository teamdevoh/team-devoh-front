import React, { useCallback, useState, Fragment } from 'react';
import history from '../../helpers/history';
import { Confirm } from '../modal';
import helpers from '../../helpers';
import { UploadLayer, FileList, FakeFileList } from '../modules';
import { Dimmer, Loader } from 'semantic-ui-react';
import { useUpload, useFormatMessage, useBoolean } from '../../hooks';
import map from 'lodash/map';
import toArray from 'lodash/toArray';
import { useUser } from '../account-manage/hooks';
import {
  useAttachCreate,
  useAttachList,
  useAttachRemove,
  useAttachDownload,
  useTagUpdate
} from './hooks';
import LinkButton from '../button/LinkButton';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { activity } from '../../states';
import { useEffect } from 'react';
import MultiDownloadButton from '../modules/MultiDownloadButton';
import FileViewerWrapper from '../viewer/FileViewerWrapper';
import api from './api';

const FileContainer = ({
  subjectId,
  activityKeyId,
  projectActivityId,
  setHasFile = f => f,
  fetchFileList = false,
  setFetchFileList = f => f
}) => {
  const t = useFormatMessage();
  const isUpload = useBoolean(false);
  const file = useUpload();
  const { projectId } = useParams();
  const dispatch = useDispatch();
  const [search, setSearch] = useState({
    open: false,
    tag: '',
    location: ''
  });
  const [my] = useUser();
  const { add } = useAttachCreate({
    subjectId,
    activityKeyId,
    projectActivityId
  });
  const [attach, setFiles] = useAttachList({
    subjectId,
    activityKeyId,
    projectActivityId
  });
  const attachDown = useAttachDownload({ subjectId, activityKeyId });
  const attachRemove = useAttachRemove({ subjectId, activityKeyId });
  const tag = useTagUpdate({ subjectId, activityKeyId });
  const [fileInfo, setFileInfo] = useState();
  const [viewerOpen, setViewerOpen] = useState(false);

  useEffect(
    () => {
      dispatch(activity.actions.updateFileStatus(attach.items.length > 0));
    },
    [attach.items]
  );

  useEffect(
    () => {
      if (!attach.loading) {
        setHasFile(attach.items.length > 0);
      }
    },
    [attach.items, attach.loading]
  );

  useEffect(
    () => {
      if (fetchFileList) {
        attach.fetch();
        setFetchFileList(false);
      }
    },
    [fetchFileList]
  );

  const handleUpdateTag = useCallback(
    async (id, tags) => {
      const isOK = await tag.update(id, { tag: tags.join('') });
      if (isOK) {
        const files = attach.items;
        map(files, file => {
          if (file.key === id) {
            file.tags = tags.join('');
          }
        });

        setFiles({ ...files });
      }
    },
    [attach.items]
  );

  const handleTagClick = useCallback(tag => {
    setSearch({
      open: true,
      tag: tag,
      location: `/project/${projectId}/search?keyword=${tag}`
    });
  }, []);

  const handleRemoveAttach = useCallback(
    async id => {
      const is = attachRemove.remove(id);
      if (is) {
        const updatedItems = toArray(attach.items).filter(file => {
          return file.key !== id;
        });
        setFiles(updatedItems);
      }
    },
    [attach.items]
  );

  const handleDrop = async acceptedFiles => {
    isUpload.setFalse();

    file.upload(acceptedFiles, async ids => {
      const data = await add({ fileStorageIds: ids });

      let newItems = [];
      await ids.forEach(async (id, index) => {
        const attached = _.find(data, { key: id });
        const meta = attached.value.split('|');
        const newAttachmentId = meta[0];
        const created = meta[1];
        const acceptedFile = acceptedFiles[index];

        const fileObj = await helpers.file.setFileInfo({
          extension: acceptedFile.type,
          id: newAttachmentId,
          originalName: acceptedFile.name,
          size: acceptedFile.size,
          created: created,
          creator: my.profile.name,
          thumbnailApiUrl: `api/subjects/${subjectId}/activities/${activityKeyId}/attachs.thumbnail/${newAttachmentId}`,
          tag: ''
        });

        newItems.push(fileObj);
      });

      const updatedItems = toArray(attach.items).concat(newItems);
      setFiles(updatedItems);
    });
  };

  const getDownloadApi = useCallback(
    id => {
      return `api/subjects/${subjectId}/activities/${activityKeyId}/attachs/${id}`;
    },
    [subjectId, activityKeyId]
  );

  const handleViewerClick = useCallback(
    async ({ fileInfo = viewer, id }) => {
      setFileInfo(fileInfo);
      setViewerOpen(true);
    },
    [attach.items]
  );

  const fetchSignedUrl = fileInfo => {
    return api.fetchArchivePresignedurl({
      subjectId,
      activityKeyId,
      activityAttachmentId: fileInfo.key
    });
  };

  const handleViewerClose = () => {
    setViewerOpen(false);
  };

  const getDownloadUrl = useCallback(
    key => {
      return `api/subjects/${subjectId}/activities/${activityKeyId}/attachs/${key}`;
    },
    [subjectId, activityKeyId]
  );

  return (
    <Fragment>
      <FileViewerWrapper
        fileInfo={fileInfo}
        fetchSignedUrl={fetchSignedUrl}
        open={viewerOpen}
        onClose={handleViewerClose}
        getDownloadUrl={getDownloadUrl}
      />
      <LinkButton
        color="twitter"
        icon="cloud upload"
        labelPosition="right"
        name={t('common.add.attachment')}
        onClick={isUpload.setTrue}
        type="button"
      />
      <UploadLayer
        open={isUpload.value}
        onClose={isUpload.setFalse}
        title={t('common.attachment.upload')}
        onDrop={handleDrop}
      />
      <Dimmer active={attachDown.loading} inverted>
        <Loader size="small">Loading</Loader>
      </Dimmer>
      <div
        style={{
          marginTop: '8px'
        }}
      >
        <FileList
          files={attach.items}
          onDownloadClick={attachDown.download}
          onDeleteClick={handleRemoveAttach}
          onUpdateTag={handleUpdateTag}
          onTagClick={handleTagClick}
          MultiDownloadButton={MultiDownloadButton({ getDownloadApi })}
          onViewerClick={handleViewerClick}
        />
      </div>
      <FakeFileList items={file.fakes} />
      <Confirm
        title={t('common.search')}
        content={t('project.search.is', { name: search.tag })}
        open={search.open}
        onOK={() => {
          history.push(search.location);
        }}
        onCancel={() => setSearch({ ...search, open: false })}
      />
    </Fragment>
  );
};

export default FileContainer;
