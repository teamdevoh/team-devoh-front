import helpers from '../../helpers';

const fetchActivityFileStatus = ({
  subjectId,
  activityKeyId,
  projectActivityId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/file.status?${params}`
  );
};

const fetchAttachments = ({ subjectId, activityKeyId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });
  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/attachs?${params}`
  );
};

const fetchAttachment = ({
  subjectId,
  activityKeyId,
  activityAttachmentId
}) => {
  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/attachs/${activityAttachmentId}`
  );
};

const fetchArchivePresignedurl = ({
  subjectId,
  activityKeyId,
  activityAttachmentId
}) => {
  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/attachs/${activityAttachmentId}/req.presignedurl`
  );
};

const addAttachment = ({
  subjectId,
  activityKeyId,
  projectActivityId,
  model
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });
  return helpers.Service.post(
    `api/subjects/${subjectId}/activities/${activityKeyId}/attachs?${params}`,
    model
  );
};

const removeAttachment = ({
  subjectId,
  activityKeyId,
  activityAttachmentId
}) => {
  return helpers.Service.delete(
    `api/subjects/${subjectId}/activities/${activityKeyId}/attachs/${activityAttachmentId}`
  );
};

const updateTag = ({
  subjectId,
  activityKeyId,
  activityAttachmentId,
  model
}) => {
  return helpers.Service.put(
    `api/subjects/${subjectId}/activities/${activityKeyId}/attachs.tag/${activityAttachmentId}`,
    model
  );
};

export default {
  fetchActivityFileStatus,
  fetchAttachments,
  fetchAttachment,
  fetchArchivePresignedurl,
  addAttachment,
  removeAttachment,
  updateTag
};
