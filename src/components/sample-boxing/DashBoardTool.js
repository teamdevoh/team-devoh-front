import React, { Fragment } from 'react';
import { Label, Button } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import {
  FlexDiv,
  RightButtonWrap,
  StyledDetail
} from '../sample-barcode/styles';

const DashBoardTool = ({ items, onSave, loading }) => {
  const t = useFormatMessage();

  const handleSaveClick = () => {
    onSave();
  };

  return (
    <Fragment>
      <FlexDiv>
        <Label size="big">
          Aliquots
          <StyledDetail>{items.length}</StyledDetail>
        </Label>
        <RightButtonWrap>
          <Button primary onClick={handleSaveClick} loading={loading}>
            {t('common.save')}
          </Button>
        </RightButtonWrap>
      </FlexDiv>
    </Fragment>
  );
};

export default DashBoardTool;
