import { useState } from 'react';
import api from '../api';

const useAliquotInfoToBoxingByBarcode = () => {
  const [loading, setLoading] = useState(false);

  const fetchItem = async ({ aliquotBarcode }) => {
    setLoading(true);
    try {
      const res = await api.fetchAliquotInfoToBoxingByBarcode({
        aliquotBarcode
      });
      if (res && res.data) {
        return res.data;
      } else {
        return res;
      }
    } finally {
      setLoading(false);
    }
  };

  return [{ loading, fetch: fetchItem }];
};

export default useAliquotInfoToBoxingByBarcode;
