import { useState } from 'react';
import api from '../api';

const useSampleBoxInfoToBoxingByBarcode = () => {
  const [loading, setLoading] = useState(false);

  const fetchItem = async boxBarcode => {
    setLoading(true);
    try {
      const res = await api.fetchSampleBoxInfoToBoxingByBarcode({
        boxBarcode: boxBarcode.trim()
      });
      if (res && res.data) {
        return res.data;
      } else {
        return res;
      }
    } finally {
      setLoading(false);
    }
  };

  return [{ loading, fetch: fetchItem }];
};

export default useSampleBoxInfoToBoxingByBarcode;
