import React, { useState, useEffect, useRef } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import AliquotBoard from './AliquotBoard';
import DashBoardTool from './DashBoardTool';
import DashBoardHeader from './DashBoardHeader';
import notification from '../modal/notification';
import { useFormatMessage } from '../../hooks';
import api from './api';
import useUser from '../account-manage/hooks/useUser';

const DashBoard = () => {
  const t = useFormatMessage();
  const boxIdEl = useRef(null);
  const aliquotIdEl = useRef(null);
  const [sampleBoxInfo, setSampleBoxInfo] = useState({});
  const [aliquotInfo, setAliquotInfo] = useState({});
  const [aliquotItems, setAliquotItems] = useState([]);
  const [removeAliquotId, setRemoveAliquotId] = useState();
  const [loading, setLoading] = useState(false);
  const [{ profile }] = useUser();

  useEffect(
    () => {
      if (sampleBoxInfo.boxBarcode) {
        setAliquotItems([]);
      } else {
        setAliquotInfo({});
        setAliquotItems([]);
      }
    },
    [sampleBoxInfo.boxBarcode]
  );

  useEffect(
    () => {
      if (aliquotInfo.aliquotBarcode) {
        const addedAliquot = aliquotItems.filter(
          item => item.aliquotBarcode === aliquotInfo.aliquotBarcode
        );

        if (addedAliquot.length === 0) {
          setAliquotItems(aliquotItems.concat([{ ...aliquotInfo }]));
        }
      }
    },
    [aliquotInfo.aliquotBarcode]
  );

  useEffect(
    () => {
      if (removeAliquotId) {
        const newItems = aliquotItems.filter(
          item => removeAliquotId !== item.sampleAliquotId
        );

        setAliquotItems(newItems);
        setAliquotInfo({});
        setRemoveAliquotId();
        aliquotIdEl.current.focus();
      }
    },
    [removeAliquotId]
  );

  const handleAliquotRemove = ({ sampleAliquotId }) => () => {
    setRemoveAliquotId(sampleAliquotId);
  };

  const handleSaveAliquotClick = async () => {
    setLoading(true);
    boxIdEl.current.focus();
    const sampleBoxAliquots = aliquotItems.map(item => {
      const { sampleAliquotId, currentDateTime } = item;
      return {
        sampleAliquotId,
        boxingDateTime: currentDateTime,
        boxingMemberId: profile.id
      };
    });
    try {
      const res = await api.saveSampleBoxAliquot({
        sampleBoxId: sampleBoxInfo.sampleBoxId,
        model: {
          boxNote: sampleBoxInfo.boxNote,
          createSampleBoxAliquots: sampleBoxAliquots
        }
      });

      if (res && res.data) {
        notification.success({
          title: t('common.ok'),
          onClose: () => {}
        });
        setSampleBoxInfo({});
      } else {
        notification.error({});
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const handleSampleBoxInfoChange = (e, { name, value }) => {
    setSampleBoxInfo({
      ...sampleBoxInfo,
      [name]: value
    });
  };

  return (
    <Segment basic>
      <Grid.Row>
        <Grid.Column width={16}>
          <DashBoardHeader
            sampleBoxInfo={sampleBoxInfo}
            setSampleBoxInfo={setSampleBoxInfo}
            setAliquotInfo={setAliquotInfo}
            aliquotItems={aliquotItems}
            boxIdEl={boxIdEl}
            aliquotIdEl={aliquotIdEl}
            onSampleBoxInfoChange={handleSampleBoxInfoChange}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool
            items={aliquotItems}
            onSave={handleSaveAliquotClick}
            loading={loading}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <AliquotBoard items={aliquotItems} onRemove={handleAliquotRemove} />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
