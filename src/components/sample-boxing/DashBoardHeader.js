import React, { useState, useEffect } from 'react';
import { Form, Input, Responsive } from 'semantic-ui-react';
import { StyledForm } from '../sample-status/styles';
import styled from 'styled-components';
import notification from '../modal/notification';
import { useFormatMessage } from '../../hooks';
import {
  useSampleBoxInfoToBoxingByBarcode,
  useAliquotInfoToBoxingByBarcode
} from './hooks';
import helpers from '../../helpers';
import { StyledInput } from '../sample-aliquot/styles';
import { code, format } from '../../constants';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const StyledDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const BoxinfoWrap = styled.div`
  flex: 1;

  ${prop => {
    if (prop.isMobile) {
      return `
        margin-top: 20px;
      `;
    } else {
      return `
        margin-left: 20px;
      `;
    }
  }};
`;

const DashBoardHeader = ({
  sampleBoxInfo,
  setSampleBoxInfo,
  setAliquotInfo,
  aliquotItems,
  boxIdEl,
  aliquotIdEl,
  onSampleBoxInfoChange
}) => {
  const t = useFormatMessage();
  const [sampleBoxBarcode, setSampleBoxBarcode] = useState('');
  const [aliquotBarcode, setAliquotBarcode] = useState('');

  const [
    { loading: sampleInfoLoading, fetch: fetchSampleBoxInfo }
  ] = useSampleBoxInfoToBoxingByBarcode();

  const [
    { loading: aliquotInfoLoading, fetch: fetchAliquotInfo }
  ] = useAliquotInfoToBoxingByBarcode();

  useEffect(() => {
    boxIdEl.current.focus();
  }, []);

  useEffect(
    () => {
      if (!sampleBoxInfo.sampleBoxId) {
        setSampleBoxBarcode('');
        boxIdEl.current.focus();
      }
      setAliquotInfo({});
      setAliquotBarcode('');
    },
    [sampleBoxInfo.sampleBoxId]
  );

  const handleBoxIdKeyUp = async e => {
    const value = e.target.value.trim();

    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      if (value !== '') {
        const boxInfo = await fetchSampleBoxInfo(value);
        if (boxInfo) {
          if (boxInfo.sampleBoxId > 0) {
            if (boxInfo.boxStatusCodeId === code.Warehousing) {
              notification.warning({
                title: t('disabled.boxing')
              });
              setSampleBoxBarcode('');
              boxIdEl.current.focus();
            } else if (
              boxInfo.boxStatusCodeId === code.OutOfStock ||
              boxInfo.boxStatusCodeId === null
            ) {
              setSampleBoxInfo(boxInfo);
              setSampleBoxBarcode(value);
              aliquotIdEl.current.focus();
            }
          } else {
            // 존재하지 않는 box id
            setSampleBoxInfo({});
            boxIdEl.current.focus();
            setSampleBoxBarcode('');
            notification.warning({
              title: t('does.not.exist', { name: 'Box ID' })
            });
          }
        }
      } else {
        notification.warning({
          title: t('empty.info', { name: 'Box ID' })
        });
      }
    }
  };

  const handleBarcodeChange = (event, { value, name }) => {
    if (name === 'boxId') {
      setSampleBoxBarcode(value);
    } else if (name === 'aliquotId') {
      setAliquotBarcode(value);
    }
  };

  const confirmConcord = aliquot => {
    const {
      projectId,
      projectSiteId,
      sampleMaterialCodeId,
      projectSampleAliquotId,
      visitTypeCodeId,
      visitOrder
    } = sampleBoxInfo;

    const notMatchMessage = [];

    if (aliquot.projectId !== projectId) {
      notMatchMessage.push(t('project'));
    }

    if (projectSiteId !== null && aliquot.projectSiteId !== projectSiteId) {
      notMatchMessage.push(t('project.site'));
    }

    if (
      sampleMaterialCodeId !== null &&
      aliquot.sampleMaterialCodeId !== sampleMaterialCodeId
    ) {
      notMatchMessage.push('Material');
    }

    if (
      projectSampleAliquotId !== null &&
      aliquot.projectSampleAliquotId !== projectSampleAliquotId
    ) {
      notMatchMessage.push('Analyte');
    }

    if (visitTypeCodeId !== null && aliquot.visitTypeCode !== visitTypeCodeId) {
      notMatchMessage.push(t('visit.type'));
    }

    if (visitOrder !== null && aliquot.visitOrder !== visitOrder) {
      notMatchMessage.push(t('visit.order'));
    }

    return notMatchMessage;
  };

  const handleAliquotIdKeyUp = async e => {
    const value = e.target.value.trim();

    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      if (!!!sampleBoxInfo.sampleBoxId) {
        boxIdEl.current.focus();
        notification.warning({
          title: t('empty.info', { name: 'Box ID' })
        });
      } else if (value !== '') {
        //
        const aliquotInfo = await fetchAliquotInfo({
          aliquotBarcode: value
        });
        if (aliquotInfo) {
          if (aliquotInfo.sampleAliquotId > 0) {
            const addedAliquot = aliquotItems.filter(
              item => item.aliquotBarcode === aliquotInfo.aliquotBarcode
            );

            if (addedAliquot.length > 0) {
              notification.warning({
                title: t('added.item', { name: 'Aliquot' })
              });
            } else {
              if (!aliquotInfo.aliquotDateTime) {
                // 분주 정보 없는 경우
                notification.warning({
                  title: t('no.aliquot')
                });
              } else if (aliquotInfo.sampleBoxAliquotId !== null) {
                // 이미 boxing 된 aliquot
                notification.warning({
                  title: t('already.boxed', {
                    boxId: aliquotInfo.boxBarcode,
                    date: helpers.util.dateformat(
                      aliquotInfo.boxingDateTime,
                      format.YYYY_MM_DD_HHMM
                    )
                  })
                });
              } else {
                const notMatchMessage = confirmConcord(aliquotInfo);
                if (notMatchMessage.length === 0) {
                  setAliquotInfo(aliquotInfo);
                } else {
                  notification.warning({
                    title: t('sample.n.aliquot.not.match', {
                      items: notMatchMessage.join(', ')
                    })
                  });
                }
              }
            }
          } else {
            // 존재하지 않는 aliquot id || box id와 일치 하지 않음
            setAliquotInfo({});
            notification.warning({
              title: t('fetch.aliquot.error.info', { name: 'Box ID' })
            });
          }
          setAliquotBarcode('');
          aliquotIdEl.current.focus();
        }
      } else {
        notification.warning({
          title: t('empty.info', { name: 'Aliquot ID' })
        });
      }
    }
  };

  return (
    <StyledDiv>
      <StyledForm>
        <Form.Field required>
          <label>Box ID</label>
          <StyledInput
            size="massive"
            name="boxId"
            ref={boxIdEl}
            onKeyUp={handleBoxIdKeyUp}
            onChange={handleBarcodeChange}
            value={sampleBoxBarcode}
            loading={sampleInfoLoading}
          />
        </Form.Field>
        <Form.Field required>
          <label>Aliquot ID</label>
          <StyledInput
            size="massive"
            name="aliquotId"
            ref={aliquotIdEl}
            onKeyUp={handleAliquotIdKeyUp}
            onChange={handleBarcodeChange}
            value={aliquotBarcode}
            loading={aliquotInfoLoading}
          />
        </Form.Field>
      </StyledForm>
      <BoxinfoWrap isMobile={isMobile}>
        <StyledForm>
          <Form.Field>
            <label>{t('project')}</label>
            {sampleBoxInfo.project}
          </Form.Field>
          <Form.Group widths="equal">
            <Form.Field>
              <label>{t('project.site')}</label>
              {sampleBoxInfo.site}
            </Form.Field>
            <Form.Field>
              <label>{t('activity.visit')}</label>
              {sampleBoxInfo.visit}
            </Form.Field>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field>
              <label>Material</label>
              {sampleBoxInfo.sampleMaterial}
            </Form.Field>
            <Form.Field>
              <label>Analyte</label>
              {sampleBoxInfo.aliquotAnalyte}
            </Form.Field>
          </Form.Group>
          <Form.Field inline>
            <label>{t('remark')}</label>
            <Input
              name="boxNote"
              fluid
              value={sampleBoxInfo.boxNote || ''}
              onChange={onSampleBoxInfoChange}
              disabled={!sampleBoxInfo.sampleBoxId}
            />
          </Form.Field>
        </StyledForm>
      </BoxinfoWrap>
    </StyledDiv>
  );
};

export default DashBoardHeader;
