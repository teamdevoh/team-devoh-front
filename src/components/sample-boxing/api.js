import helpers from '../../helpers';

const fetchSampleBoxInfoToBoxingByBarcode = ({ boxBarcode }) => {
  return helpers.Service.get(`api/samples/boxes/barcodes/${boxBarcode}`);
};

const fetchAliquotInfoToBoxingByBarcode = ({ aliquotBarcode }) => {
  return helpers.Service.get(
    `api/samples/boxes/aliquot/barcodes/${aliquotBarcode}`
  );
};

const saveSampleBoxAliquot = ({ sampleBoxId, model }) => {
  return helpers.Service.post(`api/samples/boxes/${sampleBoxId}`, model);
};

export default {
  fetchSampleBoxInfoToBoxingByBarcode,
  fetchAliquotInfoToBoxingByBarcode,
  saveSampleBoxAliquot
};
