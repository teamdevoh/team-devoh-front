import { asyncComponent } from '../modules';

const ResponsiveLarge = asyncComponent(() =>
  import('./ResponsiveLarge')
);
const ResponsiveSmall = asyncComponent(() =>
  import('./ResponsiveSmall')
);
const StepGroup = asyncComponent(() =>
  import('./StepGroup')
);
const StepItem = asyncComponent(() =>
  import('./StepItem')
);

export { ResponsiveLarge, ResponsiveSmall, StepGroup, StepItem };
