import React from 'react';
import { Fragment } from 'react';
import { Responsive } from 'semantic-ui-react';

const ResponsiveSmall = ({ children }) => {
  return (
    <Responsive maxWidth={Responsive.onlyLargeScreen.minWidth}>
      <Fragment>{children}</Fragment>
    </Responsive>
  );
};

export default ResponsiveSmall;
