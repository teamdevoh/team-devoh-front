import React from 'react';
import { Fragment } from 'react';
import { Responsive } from 'semantic-ui-react';

const ResponsiveLarge = ({ children }) => {
  return (
    <Responsive minWidth={Responsive.onlyLargeScreen.minWidth}>
      <Fragment>{children}</Fragment>
    </Responsive>
  );
};

export default ResponsiveLarge;
