import React from 'react';
import { Step } from 'semantic-ui-react';

const StepItem = ({ name, value, length, index, active, onClick }) => {
  return (
    <Step
      style={
        !!!onClick
          ? {
              cursor: 'default'
            }
          : { padding: '1em' }
      }
      index={index}
      value={value}
      link
      active={active}
      onClick={onClick}
    >
      <Step.Content>
        <Step.Title>
          <span>{name} </span>
          <span style={{ color: 'red' }}>{length}</span>
        </Step.Title>
      </Step.Content>
    </Step>
  );
};

export default StepItem;
