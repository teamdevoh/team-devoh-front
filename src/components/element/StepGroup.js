import React from 'react';
import { Step } from 'semantic-ui-react';
import StepItem from './StepItem';

const StepGroup = ({ items, onClick }) => {
  return (
    <Step.Group size="mini" fluid stackable="tablet">
      {items.map((item, index) => {
        return (
          <StepItem
            key={item.value}
            value={item.value}
            index={index}
            name={item.name}
            length={item.length}
            active={item.active}
            onClick={onClick}
          />
        );
      })}
    </Step.Group>
  );
};

export default StepGroup;
