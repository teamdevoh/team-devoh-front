import React, { Fragment, useEffect, useState } from 'react';
import DashBoardTool from './DashBoardTool';
import { Grid } from 'semantic-ui-react';
import PkDataBoard from './PkDataBoard';
import { codeGroup } from '../../../constants';
import { useCodes } from '../../../hooks';
import { usePkDataList } from './hooks';
import { activity, activityGroup } from '../../../constants';
import { useGetProjectActivityId } from '../../collection/hooks';
import { useBuild } from '../../activity/hooks';
import { pkCommonDisplayName } from '../../activity/pkData/RepeatList';
import { code } from '../../../constants';

const PkDataDashBoard = ({ drugs, refInfos, tbdrugId }) => {
  const build = useBuild();
  const [doseRouteCodes] = useCodes({
    codeGroupId: codeGroup.DoseRouteCode
  });
  const [dosfrqCodes] = useCodes({
    codeGroupId: codeGroup.DosfrqCode
  });

  const pkdata = useGetProjectActivityId({
    activityGroupId: activityGroup.PKData,
    subjectId: 0,
    activityId: activity.PkData
  });

  const [{ items, loading }] = usePkDataList({
    tbdrugId: tbdrugId,
    projectActivityId: pkdata.id
  });
  const [pkdataList, setPkDataList] = useState([]);

  useEffect(
    () => {
      const newPkDataList = [];
      for (let i = 0; i < items.length; i++) {
        const item = items[i];

        let result = '';
        const doseValue = [];
        const regimen = [];
        let route = '';
        const fed = [];
        let subjects = '';
        const female = [];
        const ethinic = [];
        let ageValue = [];
        let weightValue = [];
        let statusValue = [];
        let cmaxValue = [];
        let tmaxValue = [];
        let auc0InfiniteValue = [];
        let auc024Value = [];
        let clfValue = [];
        let clrValue = [];
        let vdValue = [];
        let vdssValue = [];
        let t12Value = [];
        let ae024hValue = [];
        let ae048hValue = [];
        let ka = [];
        let tlag = [];
        const referenceTitle = !!refInfos[item.referenceId]
          ? refInfos[item.referenceId].title
          : '';

        // Dose (mg)
        if (!!item.dose) {
          if (doseValue.length > 0) {
            doseValue[doseValue.length - 1] += `: ${item.dose}`;
          } else {
            result = `${item.dose}`;
            doseValue.push(result);
          }
        }

        // Regimen
        if (!!item.regimenCodeId) {
          result = build.displayName({
            source: dosfrqCodes,
            value: item.regimenCodeId,
            other: item.regimenOther,
            otherValue: code.DosfrqOther
          });

          regimen.push(result);
        }
        if (!!item.doseOther) {
          result = `Other: ${item.doseOther}`;
          regimen.push(result);
        }

        // Route
        if (!!item.doseRouteCodeId) {
          route = build.displayName({
            source: doseRouteCodes,
            value: item.doseRouteCodeId,
            other: item.doseRouteOther,
            otherValue: code.DoseRouteOther
          });
        }

        // Fast/Fed
        if (!!item.fedStatus) {
          result = item.fedStatus;
          fed.push(result);
        }
        if (!!item.fedStatusOther) {
          if (fed.length > 0) {
            fed[fed.length - 1] += `: ${item.fedStatusOther}`;
          } else {
            result = item.fedStatusOther;
            fed.push(result);
          }
        }

        // Sample size (subjects)
        if (!!item.genderTotal) {
          subjects = item.genderTotal;
        }

        // Population
        if (item.ethinicIsAfrican) {
          result = 'African';
          if (!!item.ethinicAfricanCountry) {
            result += `: ${item.ethinicAfricanCountry}`;
          }
          ethinic.push(result);
        }
        if (item.ethinicIsAmerican) {
          result = 'American';
          if (!!item.ethinicAmericanCountry) {
            result += `: ${item.ethinicAmericanCountry}`;
          }
          ethinic.push(result);
        }
        if (item.ethinicIsEastAsian) {
          result = 'East Asian';
          if (!!item.ethinicEastAsianCountry) {
            result += `: ${item.ethinicEastAsianCountry}`;
          }
          ethinic.push(result);
        }
        if (item.ethinicIsEuropean) {
          result = 'European';
          if (!!item.ethinicEuropeanCountry) {
            result += `: ${item.ethinicEuropeanCountry}`;
          }
          ethinic.push(result);
        }
        if (item.ethinicIsSouthAsian) {
          result = 'South Asian';
          if (!!item.ethinicSouthAsianCountry) {
            result += `: ${item.ethinicSouthAsianCountry}`;
          }
          ethinic.push(result);
        }
        if (!!item.ethinicOther) {
          result = `Other detail: ${item.ethinicOther}`;
          ethinic.push(result);
        }

        // Female (%)
        if (!!item.genderFemale) {
          if (!!item.genderUnit === '%') {
            female.push(item.genderFemale);
          } else {
            female.push(
              ((item.genderFemale / item.genderTotal) * 100).toFixed(2)
            );
          }
        }
        if (!!item.genderOther) {
          result = `Gender Other: ${item.genderOther}`;
          female.push(result);
        }

        // Age
        ageValue = pkCommonDisplayName({
          mean: item.ageMean,
          sd: item.ageSd,
          median: item.ageMedian,
          min: item.ageMin,
          max: item.ageMax,
          other: item.ageOther
        });

        // weight
        weightValue = pkCommonDisplayName({
          mean: item.weightMean,
          sd: item.weightSd,
          median: item.weightMedian,
          min: item.weightMin,
          max: item.weightMax,
          other: item.weightOther
        });

        // statusIs
        if (item.statusIsTb) {
          result = 'TB';
          if (!!item.statusTbDisease) {
            result += `: ${item.statusTbDisease}`;
          }
          statusValue.push(result);
        }
        if (item.statusIsHealthy) {
          result = 'Healthy';
          if (!!item.statusHealthyDisease) {
            result += `: ${item.statusHealthyDisease}`;
          }
          statusValue.push(result);
        }
        if (!!item.statusOther) {
          result = `Other: ${item.statusOther}`;
          statusValue.push(result);
        }

        // cmax
        cmaxValue = pkCommonDisplayName({
          mean: item.cmaxMean,
          sd: item.cmaxSd,
          median: item.cmaxMedian,
          min: item.cmaxMin,
          max: item.cmaxMax,
          other: item.cmaxOther
        });

        // tmax
        tmaxValue = pkCommonDisplayName({
          mean: item.tmaxMean,
          sd: item.tmaxSd,
          median: item.tmaxMedian,
          min: item.tmaxMin,
          max: item.tmaxMax,
          other: item.tmaxOther
        });

        // auc0Infinite
        auc0InfiniteValue = pkCommonDisplayName({
          mean: item.auc0InfiniteMean,
          sd: item.auc0InfiniteSd,
          median: item.auc0InfiniteMedian,
          min: item.auc0InfiniteMin,
          max: item.auc0InfiniteMax,
          other: item.auc0InfiniteOther
        });

        // auc024
        auc024Value = pkCommonDisplayName({
          mean: item.auc024Mean,
          sd: item.auc024Sd,
          median: item.auc024Median,
          min: item.auc024Min,
          max: item.auc024Max,
          other: item.auc024Other
        });

        // clf
        clfValue = pkCommonDisplayName({
          mean: item.clfMean,
          sd: item.clfSd,
          median: item.clfMedian,
          min: item.clfMin,
          max: item.clfMax,
          other: item.clfOther
        });

        // clr
        clrValue = pkCommonDisplayName({
          mean: item.clRMean,
          sd: item.clRSd,
          median: item.clRMedian,
          min: item.clRMin,
          max: item.clRMax,
          other: item.clROther
        });

        // vd
        vdValue = pkCommonDisplayName({
          mean: item.vdMean,
          sd: item.vdSd,
          median: item.vdMedian,
          min: item.vdMin,
          max: item.vdMax,
          other: item.vdOther
        });

        // vdss
        vdssValue = pkCommonDisplayName({
          mean: item.vdssMean,
          sd: item.vdssSd,
          median: item.vdssMedian,
          min: item.vdssMin,
          max: item.vdssMax,
          other: item.vdssOther
        });

        // t12
        t12Value = pkCommonDisplayName({
          mean: item.t12Mean,
          sd: item.t12Sd,
          median: item.t12Median,
          min: item.t12Min,
          max: item.t12Max,
          other: item.t12Other
        });

        // ae024h
        ae024hValue = pkCommonDisplayName({
          mean: item.ae024hMean,
          sd: item.ae024hSd,
          median: item.ae024hMedian,
          min: item.ae024hMin,
          max: item.ae024hMax,
          other: item.ae024hOther
        });

        // ae048h
        ae048hValue = pkCommonDisplayName({
          mean: item.ae048hMean,
          sd: item.ae048hSd,
          median: item.ae048hMedian,
          min: item.ae048hMin,
          max: item.ae048hMax,
          other: item.ae048hOther
        });

        // ka
        ka = pkCommonDisplayName({
          mean: item.kaMean,
          sd: item.kaSd,
          median: item.kaMedian,
          min: item.kaMin,
          max: item.kaMax,
          unit: item.kaUnit,
          other: item.kaOther
        });

        // tlag
        tlag = pkCommonDisplayName({
          mean: item.tlagMean,
          sd: item.tlagSd,
          median: item.tlagMedian,
          min: item.tlagMin,
          max: item.tlagMax,
          unit: item.tlagUnit,
          other: item.tlagOther
        });

        const curData = {
          tbdrugPkdataId: item.tbdrugPkdataId,
          drug: build.displayName({
            source: drugs,
            value: item.tbdrugId
          }),
          drugInforBrandName: item.drugInforBrandName,
          drugInforDosageForm: item.drugInforDosageForm,
          dose: {
            value: doseValue.join(', '),
            unit: item.doseUnit || 'mg'
          },
          regimen: regimen.join(', '),
          route: route,
          fastFed: fed.join(', '),
          subjects: subjects,
          population: ethinic.join(', '),
          female: female.join(', '),
          age: {
            value: ageValue.join(', '),
            unit: item.ageUnit || 'Year'
          },
          weight: {
            value: weightValue.join(', '),
            unit: item.weightUnit || ''
          },
          status: statusValue.join(', '),
          cmax: {
            value: cmaxValue.join(', '),
            unit: item.cmaxUnit || ''
          },
          tmax: {
            value: tmaxValue.join(', '),
            unit: item.tmaxUnit || ''
          },
          auc0Infinite: {
            value: auc0InfiniteValue.join(', '),
            unit: item.auc0InfiniteUnit || ''
          },
          auc024: {
            value: auc024Value.join(', '),
            unit: item.auc024Unit || ''
          },
          clf: {
            value: clfValue.join(', '),
            unit: item.clfUnit || ''
          },
          clr: {
            value: clrValue.join(', '),
            unit: item.clRUnit || ''
          },
          vd: {
            value: vdValue.join(', '),
            unit: item.vdUnit || ''
          },
          vdss: {
            value: vdssValue.join(', '),
            unit: item.vdssUnit || ''
          },
          t12: {
            value: t12Value.join(', '),
            unit: item.t12Unit || ''
          },
          ae024h: {
            value: ae024hValue.join(', '),
            unit: item.ae024hUnit || ''
          },
          ae048h: {
            value: ae048hValue.join(', '),
            unit: item.ae048hUnit || ''
          },
          ka: ka.join(', '),
          tlag: tlag.join(', '),
          referenceTitle
        };
        newPkDataList.push(curData);
      }

      newPkDataList.sort((a, b) => {
        if (a.drug > b.drug) {
          return 1;
        }
        if (a.drug < b.drug) {
          return -1;
        }

        return 0;
      });

      setPkDataList(newPkDataList);
    },
    [items, refInfos, drugs]
  );

  return (
    <Fragment>
      <Grid.Row>
        <Grid.Column width="16">
          <DashBoardTool total={items.length} loading={loading} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <PkDataBoard items={pkdataList} loading={loading} />
        </Grid.Column>
      </Grid.Row>
    </Fragment>
  );
};

export default PkDataDashBoard;
