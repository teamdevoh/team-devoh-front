import React, { Fragment } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { StyledForm } from '../../sample-status/styles';
import { TbdrugList } from '../../collection-tbdrug/Selector';
import { useFormatMessage } from '../../../hooks';

const DashBoardHeader = ({ filter, onFilterChange }) => {
  const t = useFormatMessage();
  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <StyledForm>
            <Form.Group widths="equal">
              <Form.Field>
                <label>TB Drug</label>
                <TbdrugList
                  defaultOption={[
                    {
                      text: t('all'),
                      value: 0
                    }
                  ]}
                  fluid
                  name="tbdrugId"
                  value={filter.tbdrugId}
                  onChange={onFilterChange}
                  style={{
                    zIndex: '1000'
                  }}
                />
              </Form.Field>
            </Form.Group>
          </StyledForm>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
