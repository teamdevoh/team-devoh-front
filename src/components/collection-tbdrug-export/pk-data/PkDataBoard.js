import React from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { TableWrap, StyledTh } from '../../case-review/styles';
import { EmptyRowsViewForSemantic } from '../../modules';

const PkDataBoard = ({ items, loading }) => {
  const t = useFormatMessage();

  return (
    <TableWrap>
      <Table celled selectable id="tbdrug_pkdata_table">
        <Table.Header>
          <Table.Row>
            <StyledTh rowSpan="2">Drug</StyledTh>
            <StyledTh rowSpan="2">
              {t('activity.tbdrugPkdata.drugInforBrandName')}
            </StyledTh>
            <StyledTh rowSpan="2">
              {t('activity.tbdrugPkdata.drugInforDosageForm')}
            </StyledTh>
            <StyledTh rowSpan="2">Drug</StyledTh>
            <StyledTh colSpan="2">Dose</StyledTh>
            <StyledTh rowSpan="2">Regimen</StyledTh>
            <StyledTh rowSpan="2">Route</StyledTh>
            <StyledTh rowSpan="2">Fast/Fed</StyledTh>
            <StyledTh rowSpan="2">Sample size (subjects)</StyledTh>
            <StyledTh rowSpan="2">Population</StyledTh>
            <StyledTh rowSpan="2">Disease/healthy status</StyledTh>
            <StyledTh rowSpan="2">Female (%)</StyledTh>
            <StyledTh colSpan="2">Age</StyledTh>
            <StyledTh colSpan="2">Weight</StyledTh>
            <StyledTh colSpan="2">Cmax</StyledTh>
            <StyledTh colSpan="2">Tmax</StyledTh>
            <StyledTh colSpan="2">AUC0-∞</StyledTh>
            <StyledTh colSpan="2">AUC0-24h</StyledTh>
            <StyledTh colSpan="2">CL/F</StyledTh>
            <StyledTh colSpan="2">CLr</StyledTh>
            <StyledTh colSpan="2">Vd</StyledTh>
            <StyledTh colSpan="2">Vdss</StyledTh>
            <StyledTh colSpan="2">T1/2</StyledTh>
            <StyledTh colSpan="2">Ae24h</StyledTh>
            <StyledTh colSpan="2">Ae48h</StyledTh>
            <StyledTh rowSpan="2">Ka</StyledTh>
            <StyledTh rowSpan="2">Tlag</StyledTh>
            <StyledTh rowSpan="2">Ref</StyledTh>
          </Table.Row>
          <Table.Row>
            <StyledTh
              top="46px"
              style={{
                borderLeft: '1px solid rgba(34,36,38,.1)'
              }}
            >
              Value
            </StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
            <StyledTh top="46px">Value</StyledTh>
            <StyledTh top="46px">Unit</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {items.map(item => {
            return (
              <Table.Row key={item.tbdrugPkdataId}>
                <Table.Cell>{item.drug}</Table.Cell>
                <Table.Cell>{item.drugInforBrandName}</Table.Cell>
                <Table.Cell>{item.drugInforDosageForm}</Table.Cell>
                <Table.Cell>{item.dose.value}</Table.Cell>
                <Table.Cell>{item.dose.unit}</Table.Cell>
                <Table.Cell>{item.regimen}</Table.Cell>
                <Table.Cell>{item.route}</Table.Cell>
                <Table.Cell>{item.fastFed}</Table.Cell>
                <Table.Cell>{item.subjects}</Table.Cell>
                <Table.Cell>{item.population}</Table.Cell>
                <Table.Cell>{item.status}</Table.Cell>
                <Table.Cell>{item.female}</Table.Cell>
                <Table.Cell>{item.age.value}</Table.Cell>
                <Table.Cell>{item.age.unit}</Table.Cell>
                <Table.Cell>{item.weight.value}</Table.Cell>
                <Table.Cell>{item.weight.unit}</Table.Cell>
                <Table.Cell>{item.cmax.value}</Table.Cell>
                <Table.Cell>{item.cmax.unit}</Table.Cell>
                <Table.Cell>{item.tmax.value}</Table.Cell>
                <Table.Cell>{item.tmax.unit}</Table.Cell>
                <Table.Cell>{item.auc0Infinite.value}</Table.Cell>
                <Table.Cell>{item.auc0Infinite.unit}</Table.Cell>
                <Table.Cell>{item.auc024.value}</Table.Cell>
                <Table.Cell>{item.auc024.unit}</Table.Cell>
                <Table.Cell>{item.clf.value}</Table.Cell>
                <Table.Cell>{item.clf.unit}</Table.Cell>
                <Table.Cell>{item.clr.value}</Table.Cell>
                <Table.Cell>{item.clr.unit}</Table.Cell>
                <Table.Cell>{item.vd.value}</Table.Cell>
                <Table.Cell>{item.vd.unit}</Table.Cell>
                <Table.Cell>{item.vdss.value}</Table.Cell>
                <Table.Cell>{item.vdss.unit}</Table.Cell>
                <Table.Cell>{item.t12.value}</Table.Cell>
                <Table.Cell>{item.t12.unit}</Table.Cell>
                <Table.Cell>{item.ae024h.value}</Table.Cell>
                <Table.Cell>{item.ae024h.unit}</Table.Cell>
                <Table.Cell>{item.ae048h.value}</Table.Cell>
                <Table.Cell>{item.ae048h.unit}</Table.Cell>
                <Table.Cell>{item.ka}</Table.Cell>
                <Table.Cell>{item.tlag}</Table.Cell>
                <Table.Cell>{item.referenceTitle}</Table.Cell>
              </Table.Row>
            );
          })}
          {items.length === 0 && (
            <EmptyRowsViewForSemantic loading={loading} columnLength={41} />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default PkDataBoard;
