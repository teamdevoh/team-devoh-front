import React, { useState, useEffect } from 'react';
import DashBoardHeader from './DashBoardHeader';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useReferences, useTbdrugList } from '../../collection-tbdrug/hooks';
import PkDataDashBoard from './PkDataDashBoard';

const DashBoard = () => {
  const { projectId } = useParams();
  const [{ items }] = useReferences();
  const [drugs] = useTbdrugList();
  const [filter, setFilter] = useState({
    tbdrugId: 0
  });
  const [refInfos, setRefInfos] = useState({});

  const handleFilterChange = (event, { name, value }) => {
    let newFilter = { ...filter, [name]: value };
    setFilter({ ...newFilter });
  };

  useEffect(
    () => {
      const newRefInfos = {};
      for (let i = 0; i < items.length; i++) {
        newRefInfos[items[i].referenceId] = {
          ...items[i]
        };
      }

      setRefInfos(newRefInfos);
    },
    [items]
  );

  return (
    <Segment basic>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onFilterChange={handleFilterChange}
            filter={filter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <PkDataDashBoard
            drugs={drugs.items}
            refInfos={refInfos}
            tbdrugId={filter.tbdrugId}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
