import React from 'react';
import { Label } from 'semantic-ui-react';
import { StyledCSVExport } from '../../export-tdm/styles';
import { FlexDiv, StyledDetail } from '../../sample-barcode/styles';
import util from '../../../helpers/util';
import { role } from '../../../constants';
import { RoleAware } from '../../auth';

const DashBoardTool = ({ total, loading }) => {
  const handleCSVExportClick = async e => {
    await util.exportExcel({
      tableId: 'tbdrug_pkdata_table',
      fileName: 'TB Drug PK Data'
    });
  };

  return (
    <FlexDiv>
      <Label size="big">
        Results
        <StyledDetail>{total}</StyledDetail>
      </Label>
      <RoleAware allowedRole={role.PKData_Export_Excel_Read}>
        <StyledCSVExport
          positive
          loading={loading}
          onClick={handleCSVExportClick}
          content="Excel Export"
        />
      </RoleAware>
    </FlexDiv>
  );
};

export default DashBoardTool;
