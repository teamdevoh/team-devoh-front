import { useEffect, useState } from 'react';
import api from '../../../activity/api';
import { useBoolean } from '../../../../hooks';

const usePkDataList = ({ tbdrugId, projectActivityId }) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(true);

  const fetchItems = async () => {
    loading.setTrue();
    const res = await api.fetchPkDataList({ tbdrugId, projectActivityId });
    if (res && res.data) {
      res.data.forEach(item => {
        item.projectActivityId = projectActivityId;
        item.activityKeyId = item.tbdrugPkdataId;
        item.subjectId = item.tbdrugId;
      });

      setItems(res.data);
    }

    loading.setFalse();
  };

  useEffect(
    () => {
      if (projectActivityId > 0) {
        fetchItems();
      }
    },
    [tbdrugId, projectActivityId]
  );

  return [{ items, loading: loading.value, fetch: fetchItems }, setItems];
};

export default usePkDataList;
