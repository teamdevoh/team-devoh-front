import React, { Fragment } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { StyledForm } from '../../sample-status/styles';
import { TbdrugList } from '../../collection-tbdrug/Selector';

const DashBoardHeader = ({ filter, onFilterChange }) => {
  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <StyledForm>
            <Form.Group widths="equal">
              <Form.Field>
                <label>TB Drug</label>
                <TbdrugList
                  fluid
                  name="tbdrugId"
                  value={filter.tbdrugId}
                  onChange={onFilterChange}
                  defaultFirstValue={true}
                  style={{
                    zIndex: '1000'
                  }}
                />
              </Form.Field>
            </Form.Group>
          </StyledForm>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
