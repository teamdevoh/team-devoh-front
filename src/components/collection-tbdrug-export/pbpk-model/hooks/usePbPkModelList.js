import { useEffect, useState } from 'react';
import api from '../../../activity/api';
import { useBoolean } from '../../../../hooks';

const initItem = {
  basicPhysico: {},
  admeBasic: {},
  pkdata: [],
  meTransporter: []
};
const usePbPkModelList = ({ tbdrugId, projectActivityIds }) => {
  const [items, setItems] = useState({
    ...initItem
  });
  const loading = useBoolean(true);

  const fetchItems = async () => {
    loading.setTrue();
    try {
      const {
        basicPhysico,
        admeBasic,
        pkdata,
        meTransporter
      } = projectActivityIds;

      const basicPhysicoRes = await api.fetchPhysico({
        tbdrugId,
        projectActivityId: basicPhysico
      });

      const admeBasicRes = await api.fetchAdmeBasic({
        tbdrugId,
        projectActivityId: admeBasic
      });

      const pkdataRes = await api.fetchPkDataList({
        tbdrugId,
        projectActivityId: pkdata
      });

      const meTransporterRes = await api.fetchMeTransporters({
        tbdrugId,
        projectActivityId: meTransporter
      });

      setItems({
        basicPhysico: basicPhysicoRes.data || initItem.basicPhysico,
        admeBasic: admeBasicRes.data || initItem.admeBasic,
        pkdata: pkdataRes.data || initItem.pkdata,
        meTransporter: meTransporterRes.data || initItem.meTransporter
      });
    } catch (error) {}
    loading.setFalse();
  };

  useEffect(
    () => {
      const {
        basicPhysico,
        admeBasic,
        pkdata,
        meTransporter
      } = projectActivityIds;
      if (
        basicPhysico &&
        admeBasic &&
        pkdata &&
        meTransporter > 0 &&
        tbdrugId > 0
      ) {
        setItems({ ...initItem });
        fetchItems();
      }
    },
    [tbdrugId, projectActivityIds]
  );

  return [{ items, loading: loading.value, fetch: fetchItems }, setItems];
};

export default usePbPkModelList;
