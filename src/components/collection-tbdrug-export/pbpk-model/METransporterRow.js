import React, { Fragment } from 'react';
import { Table } from 'semantic-ui-react';
import styled from 'styled-components';
import { codeGroup } from '../../../constants';
import { useCodes } from '../../../hooks';
import { useBuild } from '../../activity/hooks';
import { getReferenceTitle } from './PbPkModelBoard';

const BorderTopNoneCell = styled(Table.Cell)`
  &&& {
    border-top: none;
  }
`;

const METransporterRow = ({ items, refInfos }) => {
  const build = useBuild();
  const enzymeResult = [];
  const transporterResult = [];

  const [kineticparameterUnitCodes] = useCodes({
    codeGroupId: codeGroup.KineticparameterUnitCode
  });
  const [experimentsystemCodes] = useCodes({
    codeGroupId: codeGroup.ExperimentsystemCode
  });

  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    const experimentsystem = build.displayName({
      source: experimentsystemCodes,
      value: item.experimentsystemCodeId
    });

    const kineticparameterVmaxUnit = build.displayName({
      source: kineticparameterUnitCodes,
      value: item.kineticparameterVmaxUnitCodeId
    });

    const kineticparameterKmUnit = build.displayName({
      source: kineticparameterUnitCodes,
      value: item.kineticparameterKmUnitCodeId
    });

    if (item.enzymeTransporterType === 'E') {
      enzymeResult.push(
        <Fragment key={item.tbdrugMeTransporterId}>
          <Table.Row>
            <Table.Cell colSpan={3} data-f-bold={true}>
              <b>Metabolic Enzyme/Transporter: {item.enzymeTransporterName}</b>
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>CLint: In vitro intrinsic clearance</Table.Cell>
            <Table.Cell>{`${!!item.clInt ? item.clInt : ''}`}</Table.Cell>
            <Table.Cell rowSpan={11}>
              {getReferenceTitle({
                refInfos,
                referenceId: item.referenceId
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none"> ㆍ Unit</BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {`${!!item.clIntUnit ? `${item.clIntUnit}` : ''}`}
            </BorderTopNoneCell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none">
              {' '}
              ㆍ In vitro system
            </BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {experimentsystem}
            </BorderTopNoneCell>
          </Table.Row>

          <Table.Row>
            <Table.Cell>Vmax : Maximum rate of metabolism</Table.Cell>
            <Table.Cell>{item.kineticparameterVmaxValue}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none"> ㆍ Unit</BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {kineticparameterVmaxUnit}
              {`${
                !!kineticparameterVmaxUnit
                  ? ` (${item.kineticparameterVmaxUnitOther})`
                  : `${item.kineticparameterVmaxUnitOther || ''}`
              }`}
            </BorderTopNoneCell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none">
              {' '}
              ㆍ In vitro system
            </BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {experimentsystem}
            </BorderTopNoneCell>
          </Table.Row>

          <Table.Row>
            <Table.Cell>Km: Michaelis-Menten constant</Table.Cell>
            <Table.Cell>{item.kineticparameterKmValue}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none"> ㆍ Unit</BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {kineticparameterKmUnit}
              {`${
                !!kineticparameterKmUnit
                  ? ` (${item.kineticparameterKmUnitOther})`
                  : `${item.kineticparameterKmUnitOther || ''}`
              }`}
            </BorderTopNoneCell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none">
              {' '}
              ㆍ In vitro system
            </BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {experimentsystem}
            </BorderTopNoneCell>
          </Table.Row>

          <Table.Row>
            <Table.Cell>
              fuinc : fraction of unbound drug in the in vitro incubation
            </Table.Cell>
            <Table.Cell>{item.fuInc}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>
              fumic : fraction of unbound drug in the in vitro microsomal
              incubation
            </Table.Cell>
            <Table.Cell>{item.fuMic}</Table.Cell>
          </Table.Row>
        </Fragment>
      );
    } else if (item.enzymeTransporterType === 'T') {
      transporterResult.push(
        <Fragment key={item.tbdrugMeTransporterId}>
          <Table.Row>
            <Table.Cell colSpan={3} data-f-bold={true}>
              <b>Metabolic Enzyme/Transporter: {item.enzymeTransporterName}</b>
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>
              Clint,T: In vitro transporter-mediated intrinsic clearance
            </Table.Cell>
            <Table.Cell>{`${!!item.clInt ? item.clInt : ''}`}</Table.Cell>
            <Table.Cell rowSpan={9}>
              {getReferenceTitle({
                refInfos,
                referenceId: item.referenceId
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none"> ㆍ Unit</BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {`${!!item.clIntUnit ? `${item.clIntUnit}` : ''}`}
            </BorderTopNoneCell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none">
              {' '}
              ㆍ In vitro system
            </BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {experimentsystem}
            </BorderTopNoneCell>
          </Table.Row>

          <Table.Row>
            <Table.Cell>
              Jmax/Vmax: In vitro Maximum rate of transporter-mediated efflux or
              uptake
            </Table.Cell>
            <Table.Cell>{item.kineticparameterVmaxValue}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none"> ㆍ Unit</BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {kineticparameterVmaxUnit}
              {`${
                !!kineticparameterVmaxUnit
                  ? ` (${item.kineticparameterVmaxUnitOther})`
                  : `${item.kineticparameterVmaxUnitOther || ''}`
              }`}
            </BorderTopNoneCell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none">
              {' '}
              ㆍ In vitro system
            </BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {experimentsystem}
            </BorderTopNoneCell>
          </Table.Row>

          <Table.Row>
            <Table.Cell>Km : Michaelis constant (µM)</Table.Cell>
            <Table.Cell>{item.kineticparameterKmValue}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none"> ㆍ Unit</BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {kineticparameterKmUnit}
              {`${
                !!kineticparameterKmUnit
                  ? ` (${item.kineticparameterKmUnitOther})`
                  : `${item.kineticparameterKmUnitOther || ''}`
              }`}
            </BorderTopNoneCell>
          </Table.Row>
          <Table.Row>
            <BorderTopNoneCell data-b-t-s="none">
              {' '}
              ㆍ In vitro system
            </BorderTopNoneCell>
            <BorderTopNoneCell data-b-t-s="none">
              {experimentsystem}
            </BorderTopNoneCell>
          </Table.Row>
        </Fragment>
      );
    }
  }

  if (enzymeResult.length > 0) {
    enzymeResult.unshift(
      <Table.Row key="enzymeTitle">
        <Table.Cell colSpan={3} data-f-bold={true}>
          <b>Metabolism (for each ezyme)</b>
        </Table.Cell>
      </Table.Row>
    );
  }

  if (transporterResult.length > 0) {
    transporterResult.unshift(
      <Table.Row key="transporterTitle">
        <Table.Cell colSpan={3} data-f-bold={true}>
          <b>Transporter (for each transporter)</b>
        </Table.Cell>
      </Table.Row>
    );
  }

  return [...enzymeResult, ...transporterResult];
};

export default METransporterRow;
