import React, { Fragment } from 'react';
import { Table } from 'semantic-ui-react';
import styled from 'styled-components';
import { TableWrap, StyledTh } from '../../case-review/styles';
import { EmptyRowsViewForSemantic } from '../../modules';
import { codeGroup } from '../../../constants';
import { useCodes } from '../../../hooks';
import { useBuild } from '../../activity/hooks';
import { pkCommonDisplayName } from '../../activity/pkData/RepeatList';
import METransporterRow from './METransporterRow';

const FirstTd = styled(Table.Cell)`
  &&& {
    border-left: 1px solid rgba(34, 36, 38, 0.1) !important;
  }
`;

export const getReferenceTitle = ({ refInfos, referenceId }) => {
  return !!referenceId ? refInfos[referenceId].title : '';
};

const generateRow = ({ title, refInfos, item, name }) => {
  if (Object.keys(item).length === 0) {
    return null;
  }

  const MAX_CNT = 3;
  const result = [];

  for (let i = MAX_CNT; i > 1; i--) {
    const key = `${name}${i}`;
    const value = item[key];
    const reference = item[`${key}ReferenceId`];
    if (value !== null && value.trim() !== '') {
      result.unshift(
        <Table.Row key={key}>
          <FirstTd>{value}</FirstTd>
          <Table.Cell>
            {getReferenceTitle({ refInfos, referenceId: reference })}
          </Table.Cell>
        </Table.Row>
      );
    }
  }

  const key = `${name}1`;
  const value = item[key];
  const reference = item[`${key}ReferenceId`];
  if (value !== null && value.trim() !== '') {
    result.unshift(
      <Table.Row key={key}>
        <Table.Cell rowSpan={result.length + 1}>{title}</Table.Cell>
        <Table.Cell>{value}</Table.Cell>
        <Table.Cell>
          {getReferenceTitle({
            refInfos,
            referenceId: reference
          })}
        </Table.Cell>
      </Table.Row>
    );
  }

  return result;
};

const generatePkDataRow = ({ pkdataList, refInfos }) => {
  const result = [];

  for (let i = 0; i < pkdataList.length; i++) {
    const item = pkdataList[i];
    const ka = pkCommonDisplayName({
      mean: item.kaMean,
      sd: item.kaSd,
      median: item.kaMedian,
      min: item.kaMin,
      max: item.kaMax,
      unit: item.kaUnit,
      other: item.kaOther
    });

    const tlag = pkCommonDisplayName({
      mean: item.tlagMean,
      sd: item.tlagSd,
      median: item.tlagMedian,
      min: item.tlagMin,
      max: item.tlagMax,
      unit: item.tlagUnit,
      other: item.tlagOther
    });

    if (ka.length > 0 || tlag.length > 0) {
      result.push(
        <Fragment key={item.tbdrugPkdataId}>
          <Table.Row>
            <Table.Cell>
              ka: first-order absorption rate constant (1/h)
            </Table.Cell>
            <Table.Cell>{ka.join(', ')}</Table.Cell>
            <Table.Cell rowSpan={2}>
              {getReferenceTitle({
                refInfos,
                referenceId: item.referenceId
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>Tlag: Lag time (h)</Table.Cell>
            <Table.Cell>{tlag.join(', ')}</Table.Cell>
          </Table.Row>
        </Fragment>
      );
    }
  }

  return result;
};

const generatePkDataVdssRow = ({ pkdataList, refInfos }) => {
  const result = [];

  for (let i = 0; i < pkdataList.length; i++) {
    const item = pkdataList[i];
    const vdss = pkCommonDisplayName({
      mean: item.vdssMean,
      sd: item.vdssSd,
      median: item.vdssMedian,
      min: item.vdssMin,
      max: item.vdssMax,
      unit: item.vdssUnit,
      other: item.vdssOther
    });

    if (vdss.length > 0) {
      result.push(
        <Fragment key={item.tbdrugPkdataId}>
          <Table.Row>
            <Table.Cell>
              Vdss: volume of distribution at steady state (L/kg)
            </Table.Cell>
            <Table.Cell>{vdss.join(', ')}</Table.Cell>
            <Table.Cell>
              {getReferenceTitle({
                refInfos,
                referenceId: item.referenceId
              })}
            </Table.Cell>
          </Table.Row>
        </Fragment>
      );
    }
  }

  if (result.length > 0) {
    result.unshift(
      <Table.Row key="distribution">
        <Table.Cell colSpan={3} data-f-bold={true}>
          <b>Distribution</b>
        </Table.Cell>
      </Table.Row>
    );
  }

  return result;
};

const PbPkModelBoard = ({ items, refInfos, drugs, loading }) => {
  const build = useBuild();
  const [proteinBindingTypeCodes] = useCodes({
    codeGroupId: codeGroup.ProteinBindingTypeCode
  });

  const isEmpty =
    Object.keys(items.basicPhysico).length === 0 &&
    Object.keys(items.admeBasic).length === 0 &&
    items.pkdata.length === 0 &&
    items.meTransporter.length === 0;

  return (
    <TableWrap>
      <Table celled id="tbdrug_pbpkmodel_table">
        <Table.Header>
          <Table.Row>
            <StyledTh>Parameters</StyledTh>
            <StyledTh>Value</StyledTh>
            <StyledTh>Reference</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {!(loading || isEmpty || Object.keys(refInfos).length === 0) && (
            <Fragment>
              <Table.Row>
                <Table.Cell colSpan={3} data-f-bold={true}>
                  <b>Psychem and blood binding</b>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Molecular Weight (g/mol)</Table.Cell>
                <Table.Cell>{items.basicPhysico.mw}</Table.Cell>
                <Table.Cell>
                  {getReferenceTitle({
                    refInfos,
                    referenceId: items.basicPhysico.mwReferenceId
                  })}
                </Table.Cell>
              </Table.Row>
              {generateRow({
                title: 'Log P',
                refInfos,
                item: items.basicPhysico,
                name: 'logP'
              })}
              <Table.Row>
                <Table.Cell>Compound Type</Table.Cell>
                <Table.Cell>{items.basicPhysico.compounudType}</Table.Cell>
                <Table.Cell>
                  {getReferenceTitle({
                    refInfos,
                    referenceId: items.basicPhysico.compounudTypeReferenceId
                  })}
                </Table.Cell>
              </Table.Row>
              {generateRow({
                title: 'pKa',
                refInfos,
                item: items.basicPhysico,
                name: 'pKa'
              })}
              {generateRow({
                title: 'B/P: Blood-to-plasma partition ratio',
                refInfos,
                item: items.admeBasic,
                name: 'bp'
              })}
              <Table.Row>
                <Table.Cell>fu: fraction undbound in plasma</Table.Cell>
                <Table.Cell>{items.admeBasic.protein}</Table.Cell>
                <Table.Cell rowSpan={2}>
                  {getReferenceTitle({
                    refInfos,
                    referenceId: items.admeBasic.proteinReferenceId
                  })}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Plasma binding components</Table.Cell>
                <Table.Cell>
                  {build.displayName({
                    source: proteinBindingTypeCodes,
                    value: items.admeBasic.proteinBindingTypeCodeId
                  })}
                  {!!items.admeBasic.proteinBindingTypeOther
                    ? ` (${items.admeBasic.proteinBindingTypeOther})`
                    : ''}
                </Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell colSpan={3} data-f-bold={true}>
                  <b>Absorption</b>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>F: Bioavailability (%)</Table.Cell>
                <Table.Cell>
                  {items.admeBasic.fBioavailability}
                  {!!items.admeBasic.fBioavailabilityUnit
                    ? ` ${items.admeBasic.fBioavailabilityUnit}`
                    : ''}
                </Table.Cell>
                <Table.Cell>
                  {getReferenceTitle({
                    refInfos,
                    referenceId: items.admeBasic.fBioavailabilityReferenceId
                  })}
                </Table.Cell>
              </Table.Row>
              {generateRow({
                title: 'fa: fraction available from dosage form',
                refInfos,
                item: items.admeBasic,
                name: 'fa'
              })}
              {generatePkDataRow({
                pkdataList: items.pkdata,
                refInfos
              })}
              {generateRow({
                title: 'fuGut : Unbound fraction of drug in enterocytes',
                refInfos,
                item: items.admeBasic,
                name: 'fu'
              })}
              <Table.Row>
                <Table.Cell>
                  MDCK : MDCK II permeability (10
                  <sup>-6</sup> cm/s)
                </Table.Cell>
                <Table.Cell>
                  {items.admeBasic.mdck}
                  {!!items.admeBasic.mdckUnit
                    ? ` ${items.admeBasic.mdckUnit}`
                    : ''}
                </Table.Cell>
                <Table.Cell>
                  {getReferenceTitle({
                    refInfos,
                    referenceId: items.admeBasic.mdckReferenceId
                  })}
                </Table.Cell>
              </Table.Row>
              {generateRow({
                title: (
                  <Fragment>
                    Caco-2: Caco-2 permeability (10
                    <sup>-6</sup> cm/s)
                  </Fragment>
                ),
                refInfos,
                item: items.admeBasic,
                name: 'caco'
              })}
              <Table.Row>
                <Table.Cell>
                  PAMPA : PAMPA permeability (10
                  <sup>-6</sup> cm/s)
                </Table.Cell>
                <Table.Cell>
                  {items.admeBasic.pampa}
                  {!!items.admeBasic.pampaUnit
                    ? ` ${items.admeBasic.pampaUnit}`
                    : ''}
                </Table.Cell>
                <Table.Cell>
                  {getReferenceTitle({
                    refInfos,
                    referenceId: items.admeBasic.pampaReferenceId
                  })}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  LLC-PK1: LLC-PK1 permeability (10
                  <sup>-6</sup> cm/s)
                </Table.Cell>
                <Table.Cell>
                  {items.admeBasic.llcPk1}
                  {!!items.admeBasic.llcPk1Unit
                    ? ` ${items.admeBasic.llcPk1Unit}`
                    : ''}
                </Table.Cell>
                <Table.Cell>
                  {getReferenceTitle({
                    refInfos,
                    referenceId: items.admeBasic.llcPk1ReferenceId
                  })}
                </Table.Cell>
              </Table.Row>
              {generateRow({
                title: 'PSA : polar surface area',
                refInfos,
                item: items.basicPhysico,
                name: 'psa'
              })}
              <Table.Row>
                <Table.Cell>HBD : number of hydrogen bond donors</Table.Cell>
                <Table.Cell>{items.basicPhysico.hbd}</Table.Cell>
                <Table.Cell>
                  {getReferenceTitle({
                    refInfos,
                    referenceId: items.basicPhysico.hbdreferenceId
                  })}
                </Table.Cell>
              </Table.Row>

              {generatePkDataVdssRow({
                pkdataList: items.pkdata,
                refInfos
              })}
              <METransporterRow
                items={items.meTransporter}
                refInfos={refInfos}
              />
            </Fragment>
          )}
          {isEmpty && (
            <EmptyRowsViewForSemantic loading={loading} columnLength={3} />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default PbPkModelBoard;
