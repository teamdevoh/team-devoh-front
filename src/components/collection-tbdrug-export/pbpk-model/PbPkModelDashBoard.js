import React, { Fragment, useEffect, useState } from 'react';
import DashBoardTool from './DashBoardTool';
import { Grid } from 'semantic-ui-react';
import PbPkModelBoard from './PbPkModelBoard';
import { usePbPkModelList } from './hooks';
import { activity, activityGroup } from '../../../constants';
import { useGetProjectActivityId } from '../../collection/hooks';

const PbPkModelDashBoard = ({ tbdrugId, drugs, refInfos }) => {
  const basicPhysico = useGetProjectActivityId({
    activityGroupId: activityGroup.ADME,
    subjectId: 0,
    activityId: activity.BasicPhysico
  });
  const admeBasic = useGetProjectActivityId({
    activityGroupId: activityGroup.ADME,
    subjectId: 0,
    activityId: activity.AdmeBasic
  });
  const pkdata = useGetProjectActivityId({
    activityGroupId: activityGroup.PKData,
    subjectId: 0,
    activityId: activity.PkData
  });
  const meTransporter = useGetProjectActivityId({
    activityGroupId: activityGroup.ADME,
    subjectId: 0,
    activityId: activity.MeTransporter
  });
  const [projectActivityIds, setProjectActivityIds] = useState({});

  useEffect(
    () => {
      setProjectActivityIds({
        basicPhysico: basicPhysico.id,
        admeBasic: admeBasic.id,
        pkdata: pkdata.id,
        meTransporter: meTransporter.id
      });
    },
    [basicPhysico.id, admeBasic.id, pkdata.id, meTransporter.id]
  );

  const [{ items, loading }] = usePbPkModelList({
    tbdrugId,
    projectActivityIds
  });

  return (
    <Fragment>
      <Grid.Row>
        <Grid.Column width="16">
          <DashBoardTool loading={loading} tbdrugId={tbdrugId} drugs={drugs} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <PbPkModelBoard
            items={items}
            refInfos={refInfos}
            drugs={drugs}
            loading={loading}
          />
        </Grid.Column>
      </Grid.Row>
    </Fragment>
  );
};

export default PbPkModelDashBoard;
