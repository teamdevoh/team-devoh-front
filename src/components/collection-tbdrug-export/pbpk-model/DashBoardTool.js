import React from 'react';
import { StyledCSVExport } from '../../export-tdm/styles';
import { FlexDiv } from '../../sample-barcode/styles';
import { useBuild } from '../../activity/hooks';
import util from '../../../helpers/util';
import { role } from '../../../constants';
import { RoleAware } from '../../auth';

const DashBoardTool = ({ loading, tbdrugId, drugs }) => {
  const build = useBuild();
  const handleCSVExportClick = async e => {
    const drug = build.displayName({
      source: drugs,
      value: tbdrugId
    });
    await util.exportExcel({
      sheetName: drug,
      tableId: 'tbdrug_pbpkmodel_table',
      fileName: 'Parameter values used to construct the PBPK model',
      wsStyleFunction: ws => {
        ws.getColumn(1).width = 72;
        ws.getColumn(2).width = 72;
        ws.getColumn(3).width = 65;

        // 제곱 표시
        ws.model.rows.forEach(row => {
          const value = row.cells[0].value;
          if (!!value && value.includes('(10-6 cm/s)')) {
            const temp = value.split('10-6 cm/s');
            row.cells[0].value = {
              richText: [
                { text: temp[0] },
                { text: '10' },
                { font: { vertAlign: 'superscript' }, text: '-6' },
                { text: ' cm/s' },
                { text: temp[1] }
              ]
            };
          }
        });
      }
    });
  };

  return (
    <FlexDiv>
      <RoleAware allowedRole={role.PBPKModel_Export_Excel_Read}>
        <StyledCSVExport
          positive
          loading={loading}
          onClick={handleCSVExportClick}
          content="Excel Export"
        />
      </RoleAware>
    </FlexDiv>
  );
};

export default DashBoardTool;
