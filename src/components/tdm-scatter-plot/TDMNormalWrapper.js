import React, { useEffect, useState } from 'react';
import { Grid, Input, Loader, Button } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import AdjustChart from '../case-review/AdjustChart';
import { useDrugChartData } from './hooks';
import OriginalChart from '../case-review/OriginalChart';
import { useDrugConcToNormalize } from '../case-review/hooks';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

export const TitleWrap = styled.div`
  height: 17px;
`;

export const SmallInput = styled(Input)`
  &&& {
    width: 100px;
    height: 19px;
    & > input {
      text-align: right;
    }
  }
`;

export const SmallApplyButton = styled(Button)`
  &&& {
    height: 24px;
    margin: 0 0 0 10px;
    padding: 0 20px;
  }
`;

const DoseInput = props => {
  const { setNormailzeExDose } = props;
  const t = useFormatMessage();
  const [exdose, setExdose] = useState(null);

  const handleExdoseChange = (e, { value }) => {
    setExdose(value);
  };

  const handleApplyButtonClick = () => {
    setNormailzeExDose(exdose);
  };

  useEffect(
    () => {
      setExdose(props.exdose);
    },
    [props.exdose]
  );

  return (
    <TitleWrap>
      <FormattedMessage
        id="cr.tdm.scatter.normalized"
        values={{
          dose: (
            <SmallInput
              value={exdose || props.exdose || ''}
              type="number"
              onChange={handleExdoseChange}
            />
          )
        }}
      />
      <SmallApplyButton
        type="button"
        onClick={handleApplyButtonClick}
        disabled={exdose === props.exdose}
      >
        {t('common.apply')}
      </SmallApplyButton>
    </TitleWrap>
  );
};

const TDMNormalWrapper = ({
  isCompare,
  filter,
  exdrugId,
  loading,
  source,
  predictItems = [],
  projectSiteItems
}) => {
  const t = useFormatMessage();
  const [normailzeExDose, setNormailzeExDose] = useState(null);

  const normalized = useDrugConcToNormalize({
    source,
    normailzeExDose
  });
  const data = useDrugChartData(
    filter,
    exdrugId,
    normalized.source,
    predictItems,
    isCompare,
    projectSiteItems
  );

  useEffect(
    () => {
      setNormailzeExDose(normalized.exdose);
    },
    [normalized.exdose]
  );

  useEffect(
    () => {
      setNormailzeExDose(null);
    },
    [source]
  );

  return (
    <Grid columns={2} stackable>
      <Grid.Row>
        <Grid.Column>
          <Loader active={loading} />
          <OriginalChart
            title={
              <DoseInput
                setNormailzeExDose={setNormailzeExDose}
                exdose={normailzeExDose || normalized.exdose}
              />
            }
            exDrugId={exdrugId}
            data={data}
            showAlways={true}
          />
        </Grid.Column>
        <Grid.Column>
          <Loader active={loading} />
          <AdjustChart
            title={t('cr.tdm.scatter.normalized', { dose: normalized.exdose })}
            exDrugId={exdrugId}
            data={data}
            show={true}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default TDMNormalWrapper;
