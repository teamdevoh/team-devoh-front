import React, { Fragment, useEffect, useState } from 'react';
import { Accordion, Icon, Header } from 'semantic-ui-react';
import TDMBasicWrapper from './TDMBasicWrapper';
import TDMNormalWrapper from './TDMNormalWrapper';
import TDMNormaByWeightlWrapper from './TDMNormaByWeightlWrapper';
import styled from 'styled-components';

export const AccordionTitle = styled(Accordion.Title)`
  &&& {
    &:hover {
      background-color: #ededed;
    }
  }
`;

const ChartWrapper = ({
  isCompare,
  loading,
  filter,
  exdrugId,
  source,
  drugs,
  predictItems,
  setLoading,
  projectSiteItems
}) => {
  const [normalPredictItems, setNormalPredictItems] = useState([]);

  const [activeIndex, setActiveIndex] = useState(-1);

  const handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const newIndex = activeIndex === index ? -1 : index;

    setActiveIndex(newIndex);
  };

  useEffect(
    () => {
      let curPredict = [];
      const { exdose, exdrugId, exdosfrqcodeId = 0 } = filter;
      if (exdose !== -1 && typeof exdose !== 'undefined') {
        curPredict = predictItems.filter(
          item =>
            item.tbdrugId === exdrugId &&
            item.dose === Number(exdose) &&
            item.exdosfrqcodeId === exdosfrqcodeId
        );
      }

      setNormalPredictItems(curPredict);
    },
    [predictItems, filter]
  );

  return (
    <Fragment>
      <TDMBasicWrapper
        isCompare={isCompare}
        filter={filter}
        exdrugId={exdrugId}
        loading={loading}
        source={source}
        predictItems={normalPredictItems}
        setLoading={setLoading}
        drugs={drugs}
        projectSiteItems={projectSiteItems}
      />
      <Accordion>
        <AccordionTitle
          active={activeIndex === 0}
          index={0}
          onClick={handleClick}
        >
          <Header as="h3">
            <Icon name="dropdown" />
            Normalized to Standard Dose
          </Header>
        </AccordionTitle>
        <Accordion.Content active={activeIndex === 0}>
          <TDMNormalWrapper
            isCompare={isCompare}
            filter={filter}
            exdrugId={exdrugId}
            loading={loading}
            source={source}
            predictItems={normalPredictItems}
            projectSiteItems={projectSiteItems}
          />
        </Accordion.Content>
        <AccordionTitle
          active={activeIndex === 1}
          index={1}
          onClick={handleClick}
        >
          <Header as="h3">
            <Icon name="dropdown" />
            Body Weight
          </Header>
        </AccordionTitle>
        <Accordion.Content active={activeIndex === 1}>
          <TDMNormaByWeightlWrapper
            isCompare={isCompare}
            filter={filter}
            exdrugId={exdrugId}
            loading={loading}
            source={source}
            predictItems={normalPredictItems}
            projectSiteItems={projectSiteItems}
          />
        </Accordion.Content>
      </Accordion>
    </Fragment>
  );
};

export default ChartWrapper;
