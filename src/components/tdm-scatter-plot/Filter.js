import React, { Fragment, useEffect, useState, useRef } from 'react';
import {
  Form,
  Grid,
  Dropdown,
  Label,
  Segment,
  Button,
  Divider,
  Checkbox
} from 'semantic-ui-react';
import { StyledForm } from '../sample-status/styles';
import { useFormatMessage } from '../../hooks';
import { DoseList, Dosfrq, Dosfrm, Route } from './Selector';
import styled from 'styled-components';
import TargetFilter from './TargetFilter';
import CompareTargetFilter from './CompareTargetFilter';

const AllSubjectLabel = styled(Label)`
  &&& {
    border-color: #1a69a4 !important;
    background-color: rgb(0, 143, 251) !important;
    color: #fff !important;
    font-size: 1em;
  }
`;

const TargetSubjectLabel = styled(Label)`
  &&& {
    border-color: #1ea63e !important;
    background-color: rgb(1 206 136) !important;
    color: #fff !important;
    font-size: 1em;
  }
`;

const StyledCheckbox = styled(Checkbox)`
  &&& {
    margin-left: 16px;

    & label {
      color #fff !important;
    }
  }
`;

const ApplyButton = props => {
  const { onClick } = props;
  const t = useFormatMessage();
  const [loading, setLoading] = useState(props.loading);
  const isClick = useRef(false);

  const setIsClick = bool => {
    isClick.current = bool;
  };

  const getIsClick = () => {
    return isClick.current;
  };

  useEffect(
    () => {
      setLoading(props.loading);
    },
    [props.loading]
  );

  useEffect(
    () => {
      if (getIsClick()) {
        onClick();
        setIsClick(false);
      }
    },
    [isClick.current]
  );

  const handleClick = () => {
    setLoading(true);
    setIsClick(true);
  };

  return (
    <Button primary onClick={handleClick} loading={loading}>
      {t('project.search')}
    </Button>
  );
};

const Filter = ({
  projectId,
  isCompare: isCompareProp,
  setIsCompare,
  initCompareFilter,
  commonFilter: commonFilterProp,
  setCommonFilter,
  filter: filterProp,
  setFilter,
  compareFilter: compareFilterProp,
  setCompareFilter,
  drugs,
  loading,
  setLoading: setLoadingProp,
  projectSiteItems
}) => {
  const t = useFormatMessage();
  const [isCompare, setTempIsCompare] = useState(isCompareProp);
  const [commonFilter, setTempFCommonilter] = useState({
    ...commonFilterProp
  });
  const [filter, setTempFilter] = useState({
    ...filterProp
  });

  const [compareFilter, setTempCompareFilter] = useState({
    ...compareFilterProp
  });

  const [prevCompareFilter, setPrevCompareFilter] = useState();

  const handleCommonFilterChange = (event, { name, value }) => {
    let newFilter = { ...commonFilter, [name]: value };

    if (name === 'exdrugId') {
      newFilter = {
        ...newFilter,
        exdose: -1
      };
    }

    setTempFCommonilter({ ...newFilter });
  };

  const handleChange = (event, { name, value }) => {
    if (isCompare) {
      let newFilter = { ...compareFilter, [name]: value };
      if (prevCompareFilter !== name) {
        newFilter = {
          ...initCompareFilter,
          [name]: value
        };

        setPrevCompareFilter(name);
      }

      setTempCompareFilter({ ...newFilter });
    } else {
      let newFilter = { ...filter, [name]: value };

      if (name === 'projectSiteId') {
        newFilter = {
          ...newFilter,
          subjectIds: null
        };
      }

      setTempFilter({ ...newFilter });
    }
  };

  const handleApplyFIlterClick = () => {
    setLoadingProp(true);
    setCommonFilter(commonFilter);
    setIsCompare(isCompare);

    if (isCompare) {
      setCompareFilter({ ...compareFilter });
    } else {
      setFilter({ ...filter });
    }
  };

  const handleCompareCheckChange = () => {
    setTempIsCompare(!isCompare);
  };

  useEffect(
    () => {
      if (drugs.items.length > 0 && !!!commonFilter.exdrugId) {
        handleCommonFilterChange(null, {
          name: 'exdrugId',
          value: drugs.items[0].value
        });
        setCommonFilter({
          ...commonFilter,
          exdrugId: drugs.items[0].value
        });
      }
    },
    [drugs.items, commonFilter.exdrugId]
  );

  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <Segment raised>
            <StyledForm>
              <AllSubjectLabel ribbon>All Subject</AllSubjectLabel>
              <Form.Group widths="equal">
                <Form.Field>
                  <label>{t('tb.drug')}</label>
                  <Dropdown
                    placeholder={t('all')}
                    fluid
                    search
                    selection
                    name="exdrugId"
                    options={drugs.items}
                    loading={drugs.loading}
                    onChange={handleCommonFilterChange}
                    value={commonFilter.exdrugId}
                  />
                </Form.Field>
                <Form.Field>
                  <label>{t('activity.common.exdose')}</label>
                  <DoseList
                    name="exdose"
                    exdrugId={commonFilter.exdrugId}
                    value={commonFilter.exdose}
                    onChange={handleCommonFilterChange}
                  />
                </Form.Field>
                <Form.Field>
                  <label>{t('activity.subtbdrug.exdosfrqcodeId')}</label>
                  <Dosfrq
                    name="exdosfrqcodeId"
                    value={commonFilter.exdosfrqcodeId}
                    onChange={handleCommonFilterChange}
                  />
                </Form.Field>
                <Form.Field>
                  <label>{t('activity.subtbdrug.exdosfrmcodeId')}</label>
                  <Dosfrm
                    name="exdosfrmcodeId"
                    value={commonFilter.exdosfrmcodeId}
                    onChange={handleCommonFilterChange}
                  />
                </Form.Field>
                <Form.Field>
                  <label>{t('activity.subtbdrug.exroutecodeId')}</label>
                  <Route
                    name="exroutecodeId"
                    value={commonFilter.exroutecodeId}
                    onChange={handleCommonFilterChange}
                  />
                </Form.Field>
              </Form.Group>
              <TargetSubjectLabel ribbon>
                Target Subject
                <StyledCheckbox
                  label={t('compare')}
                  checked={isCompare}
                  onChange={handleCompareCheckChange}
                />
              </TargetSubjectLabel>
              {isCompare ? (
                <CompareTargetFilter
                  isCompare={isCompare}
                  compareFilter={compareFilter}
                  onChange={handleChange}
                  projectSiteItems={projectSiteItems}
                />
              ) : (
                <TargetFilter
                  isCompare={isCompare}
                  filter={filter}
                  onChange={handleChange}
                  projectSiteItems={projectSiteItems}
                />
              )}
              <Divider />
              <ApplyButton onClick={handleApplyFIlterClick} loading={loading} />
            </StyledForm>
          </Segment>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default Filter;
