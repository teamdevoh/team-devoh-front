import React, { Fragment } from 'react';
import { Form, Dropdown } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../hooks';
import MultiAgeRangeFilter from './MultiAgeRangeFilter';
import { codeGroup } from '../../constants';

const CompareTargetFilter = ({
  compareFilter: filter,
  onChange,
  projectSiteItems
}) => {
  const t = useFormatMessage();

  const [nat2PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Nat2PhenResCode
  });
  const [slcob1PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1PhenResCode
  });

  const [sexCodeItems] = useCodes({ codeGroupId: codeGroup.SexCode });
  const [ethnicItems] = useCodes({ codeGroupId: codeGroup.EthnicCode });
  const [comorbidCodes] = useCodes({ codeGroupId: codeGroup.ComorbidCode });

  return (
    <Fragment>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.member.site')}</label>
          <Dropdown
            fluid
            multiple
            selection
            name="projectSiteId"
            value={filter.projectSiteId}
            onChange={onChange}
            options={projectSiteItems}
          />
        </Form.Field>
        <Form.Field>
          <label>FDC</label>
          <Dropdown
            name="fdc"
            fluid
            multiple
            selection
            search
            value={filter.fdc}
            options={[
              {
                key: 'true',
                text: 'FDC Yes',
                value: 'true'
              },
              {
                key: 'false',
                text: 'FDC No',
                value: 'false'
              }
            ]}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.sexcodeId')}</label>
          <Dropdown
            fluid
            multiple
            selection
            name="sexcodeId"
            value={filter.sexcodeId}
            onChange={onChange}
            options={sexCodeItems}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.ethniccodeId')}</label>
          <Dropdown
            fluid
            multiple
            selection
            name="ethniccodeId"
            value={filter.ethniccodeId}
            onChange={onChange}
            options={ethnicItems}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.age')}</label>
          <MultiAgeRangeFilter
            name="ageRange"
            onChange={onChange}
            value={filter.ageRange}
          />
        </Form.Field>
      </Form.Group>

      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.genotyping.nat2PhenResCodeId')}</label>
          <Dropdown
            name="nat2PhenResCodeId"
            fluid
            multiple
            selection
            search
            value={filter.nat2PhenResCodeId}
            options={nat2PhenResCodes}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.genotyping.slco1b1PhenResCodeId')}</label>
          <Dropdown
            name="slco1b1PhenResCodeId"
            fluid
            multiple
            selection
            search
            value={filter.slco1b1PhenResCodeId}
            options={slcob1PhenResCodes}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.comorbid.comorbidcodeId')}</label>
          <Dropdown
            name="comorbidcodeId"
            fluid
            multiple
            selection
            search
            value={filter.comorbidcodeId}
            options={comorbidCodes}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>ADR</label>
          <Dropdown
            name="hasAdr"
            fluid
            multiple
            selection
            search
            value={filter.hasAdr}
            options={[
              {
                key: 'true',
                text: 'ADR Yes',
                value: 'true'
              },
              {
                key: 'false',
                text: 'ADR No',
                value: 'false'
              }
            ]}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
    </Fragment>
  );
};

export default CompareTargetFilter;
