import React, { Fragment } from 'react';
import { Form, Dropdown } from 'semantic-ui-react';
import { Subjects } from '../collection/Selector';
import { useFormatMessage, useCodes } from '../../hooks';
import { Sex, Ethnic } from '../activity/Selector';
import AgeRangeFilter from './AgeRangeFilter';
import { codeGroup } from '../../constants';

const TargetFilter = ({ filter, onChange, projectSiteItems }) => {
  const t = useFormatMessage();

  const [nat2PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Nat2PhenResCode
  });
  const [slcob1PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1PhenResCode
  });
  const [comorbidCodes] = useCodes({ codeGroupId: codeGroup.ComorbidCode });

  return (
    <Fragment>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.member.site')}</label>
          <Dropdown
            fluid
            selection
            name="projectSiteId"
            defaultValue={filter.projectSiteId}
            onChange={onChange}
            options={[{ key: 0, text: '-', value: 0 }, ...projectSiteItems]}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('subject')}</label>
          <Subjects
            projectSiteId={filter.projectSiteId}
            name="subjectIds"
            value={!!!filter.subjectIds ? [] : filter.subjectIds}
            onChange={onChange}
            multiple
            reset={filter.projectSiteId === 0}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.sexcodeId')}</label>

          <Sex name="sexcodeId" value={filter.sexcodeId} onChange={onChange} />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.ethniccodeId')}</label>
          <Ethnic
            name="ethniccodeId"
            value={filter.ethniccodeId}
            onChange={onChange}
            useAll={true}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.age')}</label>
          <AgeRangeFilter name="ageRange" onChange={onChange} />
        </Form.Field>
      </Form.Group>

      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.genotyping.nat2PhenResCodeId')}</label>
          <Dropdown
            name="nat2PhenResCodeId"
            fluid
            selection
            search
            value={filter.nat2PhenResCodeId}
            options={[{ key: 0, text: '-', value: 0 }, ...nat2PhenResCodes]}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.genotyping.slco1b1PhenResCodeId')}</label>
          <Dropdown
            name="slco1b1PhenResCodeId"
            fluid
            selection
            search
            value={filter.slco1b1PhenResCodeId}
            options={[{ key: 0, text: '-', value: 0 }, ...slcob1PhenResCodes]}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.comorbid.comorbidcodeId')}</label>
          <Dropdown
            name="comorbidcodeId"
            fluid
            selection
            search
            value={filter.comorbidcodeId}
            options={[{ key: 0, text: '-', value: 0 }, ...comorbidCodes]}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>ADR</label>
          <Dropdown
            name="hasAdr"
            fluid
            selection
            search
            value={filter.hasAdr}
            options={[
              { key: 0, text: '-', value: 0 },
              {
                key: 'true',
                text: 'ADR Yes',
                value: 'true'
              },
              {
                key: 'false',
                text: 'ADR No',
                value: 'false'
              }
            ]}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
    </Fragment>
  );
};

export default TargetFilter;
