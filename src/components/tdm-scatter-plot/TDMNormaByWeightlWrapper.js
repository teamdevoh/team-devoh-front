import React, { useState } from 'react';
import { Grid, Loader } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import AdjustChart from '../case-review/AdjustChart';
import { useDrugChartData, useNormalizedToWeight } from './hooks';
import OriginalChart from '../case-review/OriginalChart';
import { SmallInput, SmallApplyButton, TitleWrap } from './TDMNormalWrapper';
import { FormattedMessage } from 'react-intl';

const WeightInput = props => {
  const { setNormailzeWeight } = props;
  const t = useFormatMessage();
  const [weight, setWeight] = useState(props.weight);

  const handleWeightChange = (e, { value }) => {
    setWeight(value);
  };

  const handleApplyButtonClick = () => {
    setNormailzeWeight(weight);
  };

  return (
    <TitleWrap>
      <FormattedMessage
        id="cr.tdm.scatter.normalized.weight"
        values={{
          weight: (
            <SmallInput
              defaultValue={weight}
              type="number"
              onChange={handleWeightChange}
            />
          )
        }}
      />
      <SmallApplyButton
        type="button"
        onClick={handleApplyButtonClick}
        disabled={weight === props.weight}
      >
        {t('common.apply')}
      </SmallApplyButton>
    </TitleWrap>
  );
};

const TDMNormaByWeightlWrapper = ({
  isCompare,
  filter,
  exdrugId,
  loading,
  source,
  predictItems = [],
  projectSiteItems
}) => {
  const [normailzeWeight, setNormailzeWeight] = useState(60);

  const [{ items: normalized }] = useNormalizedToWeight({
    source,
    normailzeWeight
  });
  const data = useDrugChartData(
    filter,
    exdrugId,
    normalized,
    predictItems,
    isCompare,
    projectSiteItems
  );
  const t = useFormatMessage();

  return (
    <Grid columns={2} stackable>
      <Grid.Row>
        <Grid.Column>
          <Loader active={loading} />
          <OriginalChart
            title={
              <WeightInput
                setNormailzeWeight={setNormailzeWeight}
                weight={normailzeWeight}
              />
            }
            exDrugId={exdrugId}
            data={data}
            showAlways={true}
          />
        </Grid.Column>
        <Grid.Column>
          <Loader active={loading} />
          <AdjustChart
            title={t('cr.tdm.scatter.normalized.weight', {
              weight: normailzeWeight
            })}
            exDrugId={exdrugId}
            data={data}
            show={true}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default TDMNormaByWeightlWrapper;
