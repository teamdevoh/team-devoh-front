import helpers from '../../helpers';

const fetchDoseListByExdrugId = ({ exdrugId }) => {
  return helpers.Service.get(`api/subjects/drugs/${exdrugId}/dose`);
};

export default {
  fetchDoseListByExdrugId
};
