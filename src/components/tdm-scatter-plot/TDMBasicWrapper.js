import React, { Fragment, useEffect, useRef, useMemo, useState } from 'react';
import {
  Grid,
  Header,
  Loader,
  Accordion,
  Icon,
  Divider
} from 'semantic-ui-react';
import AdjustChart from '../case-review/AdjustChart';
import OriginalChart from '../case-review/OriginalChart';
import styled from 'styled-components';
import { useCodes } from '../../hooks';
import { useDoseListByExDrugId, useDrugChartData } from './hooks';
import { codeGroup } from '../../constants';

const StyledHeader = styled(Header)`
  &&& {
    display: flex;
    flex-direction: column;
  }
`;

const FlexDiv = styled.div`
  display: flex;
`;

const StyledAccordionTitle = styled(Accordion.Title)`
  padding: 0 !important;
`;

const CountWrap = styled.div`
  margin-left: 16px;
  display: inline-block;
`;

const AllCount = styled.span`
  color: rgb(0, 143, 251);
`;

const TargetCount = styled.span`
  color: rgb(0, 227, 150);
`;

const AccordionContent = styled(Accordion.Content)`
  &&& {
    margin-left: 28px;
  }
`;

const TargetDetailWrap = styled.div`
  display: inline-flex;
  align-items: center;
`;

const LegendMarker = styled.span`
  height: 12px;
  width: 12px;
  left: 0px;
  top: 0px;
  border-radius: 12px;

  position: relative;
  display: inline-block;
`;

const LegendText = styled.span`
  color: rgb(55, 61, 63);
  font-size: 12px;
  margin-left: 8px;
`;

const Count = ({ data, isCompare }) => {
  const [activeIndex, setActiveIndex] = useState(-1);
  const [targetCount, setTargetCount] = useState();
  const [targetDetail, setTargetDetail] = useState([]);
  const [subjectSeries] = data;

  const handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const newIndex = activeIndex === index ? -1 : index;

    setActiveIndex(newIndex);
  };

  useEffect(
    () => {
      if (isCompare) {
        let newTargetCount = 0;
        const targetDetail = [];

        for (let i = 1; i < data.length; i++) {
          const curSeries = data[i];

          if (curSeries.name !== 'Population Prediction') {
            newTargetCount += curSeries.data.length;

            targetDetail.push(
              <TargetDetailWrap key={curSeries.name}>
                <LegendMarker
                  style={{
                    background: curSeries.color,
                    color: curSeries.color
                  }}
                />
                <LegendText>
                  {curSeries.name} : {curSeries.data.length}
                </LegendText>
              </TargetDetailWrap>
            );
          }
        }

        setTargetCount(newTargetCount);
        setTargetDetail(targetDetail);
      } else {
        const targetData = data.filter(item => item.name === 'Target Subject');
        const newTargetCount =
          targetData.length === 0 ? 0 : targetData[0].data.length;
        setTargetCount(newTargetCount);
      }
    },
    [data, isCompare]
  );

  const allCount = subjectSeries.data.length;

  useEffect(
    () => {
      if (!isCompare) {
        setActiveIndex(-1);
      }
    },
    [isCompare]
  );

  return (
    <CountWrap>
      <Accordion>
        <StyledAccordionTitle
          active={activeIndex === 0}
          index={0}
          onClick={isCompare ? handleClick : null}
        >
          {isCompare && <Icon name="dropdown" />}
          <TargetCount>{targetCount}</TargetCount>
          <AllCount> / {allCount} cases</AllCount>
        </StyledAccordionTitle>
        <AccordionContent active={activeIndex === 0}>
          {targetDetail.map((item, index) => {
            return (
              <Fragment key={item.key}>
                {index > 0 ? ' / ' : ''}
                {item}
              </Fragment>
            );
          })}
        </AccordionContent>
      </Accordion>
    </CountWrap>
  );
};

const ChartHeader = ({ title, data, isCompare }) => {
  return (
    <StyledHeader as="h3">
      <FlexDiv>
        {title}
        <Count data={data} isCompare={isCompare} />
      </FlexDiv>
      <Divider />
    </StyledHeader>
  );
};

const TDMBasicWrapper = ({
  isCompare,
  filter,
  exdrugId,
  loading,
  source,
  predictItems = [],
  setLoading,
  drugs,
  projectSiteItems
}) => {
  const data = useDrugChartData(
    filter,
    filter.exdrugId,
    source,
    predictItems,
    isCompare,
    projectSiteItems
  );

  const [{ item: dose }] = useDoseListByExDrugId({ exdrugId: filter.exdrugId });
  const [dosfrqCodes] = useCodes({ codeGroupId: codeGroup.DosfrqCode });
  const [dosfrmCodes] = useCodes({ codeGroupId: codeGroup.DosfrmCode });
  const [routeCodes] = useCodes({ codeGroupId: codeGroup.RouteCode });
  const title = useMemo(
    () => {
      const { exdose, exdosfrqcodeId, exdosfrmcodeId, exroutecodeId } = filter;
      const curDrug = drugs.items.filter(
        item => item.value === filter.exdrugId
      )[0];
      let title = '';

      if (!!curDrug) {
        title = curDrug.text;
      }

      if (exdose !== -1) {
        title += ` ${exdose} (mg)`;
      }

      if (exdosfrqcodeId !== 0) {
        const frq = dosfrqCodes.filter(
          item => item.value === exdosfrqcodeId
        )[0];
        title += `, ${frq.text}`;
      }

      if (exdosfrmcodeId !== 0) {
        const frm = dosfrmCodes.filter(
          item => item.value === exdosfrmcodeId
        )[0];
        title += `, ${frm.text}`;
      }

      if (exroutecodeId !== 0) {
        const route = routeCodes.filter(
          item => item.value === exroutecodeId
        )[0];
        title += `, ${route.text}`;
      }

      return title;
    },
    [drugs.items, dose, dosfrqCodes, dosfrmCodes, routeCodes, filter]
  );
  const prevDataRef = useRef();

  useEffect(
    () => {
      prevDataRef.current = { data, title, source };
    },
    [data, title, source]
  );

  useEffect(
    () => {
      const prev = prevDataRef.current;
      if (
        JSON.stringify(prev.data) !== JSON.stringify(data) ||
        prev.source.length !== source.length ||
        prev.title !== title
      ) {
        setLoading(true);
      } else {
        setLoading(false);
      }
    },
    [data, title, source]
  );

  return (
    <Fragment>
      <ChartHeader title={title} data={data} isCompare={isCompare} />
      <Grid columns={2} stackable>
        <Grid.Row>
          <Grid.Column>
            <Loader active={loading} />
            <OriginalChart title="" exDrugId={exdrugId} data={data} />
          </Grid.Column>
          <Grid.Column>
            <Loader active={loading} />
            <AdjustChart title="" exDrugId={exdrugId} data={data} show={true} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Fragment>
  );
};

export default TDMBasicWrapper;
