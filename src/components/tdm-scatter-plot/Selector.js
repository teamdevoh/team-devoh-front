import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import { useCodes, useFormatMessage } from '../../hooks';
import { useDoseListByExDrugId } from './hooks';

export const DoseList = ({ value, name, exdrugId, onChange }) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useDoseListByExDrugId({ exdrugId });

  return (
    <Dropdown
      placeholder=""
      fluid
      search
      selection
      name={name}
      options={[
        {
          text: t('all'),
          value: -1
        },
        ...items
      ]}
      loading={loading}
      onChange={onChange}
      value={value}
    />
  );
};

export const Dosfrq = ({ value, name, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.DosfrqCode });

  return (
    <Dropdown
      style={{ zIndex: 12 }}
      fluid
      search
      selection
      name={name}
      options={[
        {
          text: t('all'),
          value: 0
        },
        ...items
      ]}
      onChange={onChange}
      value={value}
    />
  );
};

export const Dosfrm = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.DosfrmCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('all'),
          value: 0
        },
        ...items
      ]}
    />
  );
};

export const Route = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.RouteCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('all'),
          value: 0
        },
        ...items
      ]}
    />
  );
};
