import React from 'react';
import { Header } from 'semantic-ui-react';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';

const Title = () => {
  return (
    <Header as="h3" dividing>
      TDM Scatter Plot
      <Header.Subheader>TDM Scatter Plot</Header.Subheader>
      <CopyRedirectUrl />
    </Header>
  );
};

export default Title;
