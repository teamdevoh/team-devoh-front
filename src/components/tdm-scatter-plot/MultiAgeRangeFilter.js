import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import React, { Fragment, useState } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import AgeRangeFilter from './AgeRangeFilter';

const FlexDiv = styled.div`
  display: flex;
`;

const TokenWrap = styled.div`
  display: inline-block;
  vertical-align: top;
  white-space: normal;
  font-size: 1em;
  padding: 0.35714286em 0.78571429em;
  margin: 0.14285714rem 0.28571429rem 0.14285714rem 0;
  box-shadow: inset 0 0 0 1px rgb(34 36 38 / 15%);
  line-height: 1;
  background-color: #e8e8e8;
  background-image: none;
  color: rgba(0, 0, 0, 0.6);
  text-transform: none;
  font-weight: 700;
  border: 0 solid transparent;
  border-radius: 0.28571429rem;
  transition: background 0.1s ease;
`;

const StyledIcon = styled(Icon)`
  &&& {
    cursor: pointer;
    margin-right: 0;
    margin-left: 0.5em;
    font-size: 0.92857143em;
    opacity: 0.5;
    transition: background 0.1s ease;

    &:hover {
      opacity: 1;
    }
  }
`;

const MultiAgeRangeFilter = ({ name, onChange, value }) => {
  const t = useFormatMessage();
  const [curFromTo, setCurFromTo] = useState([]);
  const [isError, setIsError] = useState(true);

  const handleChange = (e, data) => {
    const [from, to] = data.value;
    if (from >= 0 && to) {
      setIsError(false);
    }
    setCurFromTo(data.value);
  };

  const handleAddAgeRange = (e, data) => {
    const [from, to] = curFromTo;
    if (from >= 0 && to) {
      const newValue = [...value];

      const alreadyExist = newValue.find(([f, t]) => {
        return f === from && t === to;
      });

      if (!!!alreadyExist || alreadyExist.length === 0) {
        newValue.push(curFromTo);

        onChange(null, {
          name,
          value: newValue
        });
      }
    }
  };

  const handleXClick = ({ index }) => () => {
    const newValue = [...value.slice(0, index), ...value.slice(index + 1)];

    onChange(null, {
      name,
      value: newValue
    });
  };

  return (
    <Fragment>
      {value.map((item, index) => {
        const [from, to] = item;

        return (
          <TokenWrap key={index}>
            {`${from} ~ ${to}`}
            <StyledIcon name="x" onClick={handleXClick({ index })} />
          </TokenWrap>
        );
      })}
      <FlexDiv>
        <AgeRangeFilter
          name={name}
          onChange={handleChange}
          setIsError={setIsError}
          useEnterKey={false}
        />
        <Button
          onClick={handleAddAgeRange}
          disabled={isError}
          style={{ wordBreak: 'keep-all' }}
        >
          {t('activity.add')}
        </Button>
      </FlexDiv>
    </Fragment>
  );
};

export default MultiAgeRangeFilter;
