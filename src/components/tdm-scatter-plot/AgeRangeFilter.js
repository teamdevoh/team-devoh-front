import React, { useState, useRef, useEffect } from 'react';
import { Input, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useFormatMessage } from '../../hooks';

const FlexWrap = styled.div`
  display: flex;
  align-items: center;
`;

const AgeRangeFilter = props => {
  const { onChange, setIsError = f => f, useEnterKey = true } = props;
  const t = useFormatMessage();
  const [error, setError] = useState(false);
  const [ageFilter, setAgeFilter] = useState([]);
  const toRef = useRef();

  const handleAgeFilterChange = e => {
    let { name, value } = e.currentTarget;
    value = value === '' ? null : Number(value);
    setError(false);
    const [from, to] = ageFilter;
    let newFrom = from;
    let newTo = to;
    let isOk = true;

    if (value !== null) {
      if (
        (name === 'from' &&
          to !== null &&
          typeof to !== 'undefined' &&
          value > to) ||
        (name === 'to' &&
          from !== null &&
          typeof from !== 'undefined' &&
          value < from)
      ) {
        isOk = false;
      }
    }

    if (name === 'from') {
      newFrom = value;
    } else {
      newTo = value;
    }

    if (isOk) {
      if (`${newFrom} ~ ${newTo}` !== `${from} ~ ${to}`) {
        onChange(e, { name: props.name, value: [newFrom, newTo] });
      }
    } else {
      setError(true);
    }

    setAgeFilter([newFrom, newTo]);
  };

  const handleAgeFilterKeyDown = e => {
    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      handleAgeFilterChange(e);
      if (e.currentTarget.name === 'from') {
        toRef.current.focus();
      }
      e.preventDefault();
    }
  };

  const handleAgeFilterKeyUp = e => {
    handleAgeFilterChange(e);
  };

  useEffect(
    () => {
      if (error) {
        setIsError(true);
      }
    },
    [error]
  );

  return (
    <Popup
      position="top center"
      open={error}
      content={t('check.start.end.values')}
      trigger={
        <FlexWrap>
          <Input
            name="from"
            placeholder="From"
            error={error}
            type="number"
            min="0"
            onBlur={handleAgeFilterChange}
            onKeyDown={useEnterKey ? handleAgeFilterKeyDown : null}
            onKeyUp={useEnterKey ? null : handleAgeFilterKeyUp}
          />
          <span style={{ padding: '0 8px' }}>~</span>
          <Input
            name="to"
            placeholder="To"
            error={error}
            type="number"
            min="0"
            ref={toRef}
            onBlur={handleAgeFilterChange}
            onKeyDown={useEnterKey ? handleAgeFilterKeyDown : null}
            onKeyUp={useEnterKey ? null : handleAgeFilterKeyUp}
          />
        </FlexWrap>
      }
    />
  );
};

export default AgeRangeFilter;
