import React, { useState, useEffect } from 'react';
import Filter from './Filter';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import ChartWrapper from './ChartWrapper';
import { useDrugSubjects, useTbdrugPredictionList } from '../case-review/hooks';
import { useExTbdrugList } from '../collection-tbdrug/hooks';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';

const initCompareFilter = {
  projectSiteId: [],
  fdc: [],
  sexcodeId: [],
  ethniccodeId: [],
  ageRange: [],
  nat2PhenResCodeId: [],
  slco1b1PhenResCodeId: [],
  comorbidcodeId: [],
  hasAdr: []
};

const TDMScatterPlot = () => {
  const { projectId } = useParams();
  const [isCompare, setIsCompare] = useState(false);
  const [commonFilter, setCommonFilter] = useState({
    exdrugId: 0,
    exdose: -1,
    exdosfrqcodeId: 0,
    exdosfrmcodeId: 0,
    exroutecodeId: 0
  });

  const [filter, setFilter] = useState({
    projectSiteId: 0,
    subjectIds: null,
    sexcodeId: 0,
    ethniccodeId: 0,
    ageRange: [],
    nat2PhenResCodeId: 0,
    slco1b1PhenResCodeId: 0,
    comorbidcodeId: 0,
    hasAdr: 0
  });
  const [compareFilter, setCompareFilter] = useState({
    ...initCompareFilter
  });

  const [drugs] = useExTbdrugList();
  const { items, loading } = useDrugSubjects(null, commonFilter.exdrugId);
  const [{ items: predictItems }] = useTbdrugPredictionList();
  const [projectSiteItems] = useMyProjectSiteList({ projectId });

  const [rendering, setRendering] = useState(loading);

  useEffect(
    () => {
      if (loading) {
        setRendering(true);
      }
    },
    [loading]
  );

  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <Filter
            projectId={projectId}
            isCompare={isCompare}
            setIsCompare={setIsCompare}
            initCompareFilter={initCompareFilter}
            commonFilter={commonFilter}
            setCommonFilter={setCommonFilter}
            filter={filter}
            setFilter={setFilter}
            compareFilter={compareFilter}
            setCompareFilter={setCompareFilter}
            drugs={drugs}
            loading={loading || rendering}
            setLoading={setRendering}
            projectSiteItems={projectSiteItems}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <ChartWrapper
            isCompare={isCompare}
            filter={
              isCompare
                ? {
                    ...commonFilter,
                    ...compareFilter
                  }
                : {
                    ...commonFilter,
                    ...filter
                  }
            }
            exdrugId={commonFilter.exdrugId}
            loading={loading || rendering}
            source={items}
            drugs={drugs}
            predictItems={predictItems}
            setLoading={setRendering}
            projectSiteItems={projectSiteItems}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default TDMScatterPlot;
