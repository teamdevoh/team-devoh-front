import { useMemo } from 'react';
import useScatterAxis from './useScatterAxis';
import { chartDataGenerate } from '../../case-review/CRTDMChartContainer';
import { useCodes } from '../../../hooks';
import { useBuild } from '../../activity/hooks';
import { codeGroup } from '../../../constants';

const checkAge = (range, subjectAge) => {
  let isIn = false;
  let [from, to] = range;

  from = !!from ? from : 0;
  to = !!to ? to : 999;
  subjectAge = !!subjectAge ? subjectAge : -1;

  if (from === 0 && to === 999) {
    isIn = true;
  } else if (subjectAge !== -1 && subjectAge >= from && subjectAge <= to) {
    isIn = true;
  }

  return isIn;
};

const isFiltered = filter => {
  const {
    projectSiteId,
    subjectIds,
    sexcodeId,
    ethniccodeId,
    ageRange,
    nat2PhenResCodeId,
    slco1b1PhenResCodeId,
    comorbidcodeId,
    hasAdr
  } = filter;
  let [from, to] = ageRange;

  from = !!from ? from : 0;
  to = !!to ? to : 0;

  return !(
    projectSiteId === 0 &&
    (subjectIds === null || subjectIds.length === 0) &&
    sexcodeId === 0 &&
    ethniccodeId === 0 &&
    from === 0 &&
    to === 0 &&
    nat2PhenResCodeId === 0 &&
    slco1b1PhenResCodeId === 0 &&
    comorbidcodeId === 0 &&
    hasAdr === 0
  );
};

const checkDose = (exdose, exdoseFilter) => {
  const p = /[0-9]*(\.|,)?[0-9]+/;
  if (
    exdose === null ||
    typeof exdose === 'undefined' ||
    exdose.trim() === ''
  ) {
    return false;
  } else {
    const match = exdose.match(p);
    const number = match[0];
    return Number(number) === Number(exdoseFilter);
  }
};

const chartColor = [
  'rgb(0, 143, 251)',
  'rgb(0, 227, 150)',
  'rgb(254, 176, 25)',
  'rgb(255, 69, 96)',
  'rgb(119, 93, 208)'
];

const multipleChartColor = [
  '#ef476f',
  '#6b705c',
  '#06d6a0',
  '#118ab2',
  '#073b4c',
  '#3d348b',
  '#7678ed',
  '#f7b801',
  '#f18701',
  '#f35b04',
  '#9d4edd',
  '#b56576',
  '#0b525b',
  '#9ef01a',
  '#f19c79',
  '#714674'
];

const useDrugChartData = (
  filter,
  exdrugId,
  source,
  predictItems = [],
  isCompare = false,
  projectSiteItems = []
) => {
  const axis = useScatterAxis();
  const build = useBuild();
  const [sexCodeItems] = useCodes({ codeGroupId: codeGroup.SexCode });
  const [ethnicItems] = useCodes({ codeGroupId: codeGroup.EthnicCode });
  const [nat2PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Nat2PhenResCode
  });
  const [slcob1PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1PhenResCode
  });
  const [comorbidCodes] = useCodes({ codeGroupId: codeGroup.ComorbidCode });

  const generateData = () => {
    let data = source ? source : [];
    const dataSet = [];

    let multipleFilterColorIndex = 0;

    const {
      exdose,
      exdosfrqcodeId,
      exdosfrmcodeId,
      exroutecodeId,
      projectSiteId,
      subjectIds,
      sexcodeId,
      ethniccodeId,
      ageRange,
      nat2PhenResCodeId,
      slco1b1PhenResCodeId,
      comorbidcodeId,
      hasAdr,
      fdc
    } = filter;

    data = data.filter(item => {
      return (
        (exdose === -1 || checkDose(item.exdose, exdose)) &&
        (exdosfrqcodeId === 0 || item.exdosfrqcodeId === exdosfrqcodeId) &&
        (exdosfrmcodeId === 0 || item.exdosfrmcodeId === exdosfrmcodeId) &&
        (exroutecodeId === 0 || item.exroutecodeId === exroutecodeId)
      );
    });

    const others = axis.buildOther({
      name: 'Subject',
      targets: data,
      color: chartColor[0]
    });
    dataSet.push(others);

    if (isCompare) {
      if (projectSiteId.length !== 0 && projectSiteItems.length > 0) {
        for (let i = 0; i < projectSiteId.length; i++) {
          const curProjectSiteId = Number(projectSiteId[i]);
          const curSite = projectSiteItems.filter(
            item => item.value === curProjectSiteId
          );

          const siteName = curSite[0].text;

          const currentSubjects = data.filter(item => {
            return curProjectSiteId === item.projectSiteId;
          });

          const target = axis.buildOther({
            name: siteName,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }

      if (fdc.length !== 0) {
        for (let i = 0; i < fdc.length; i++) {
          const curValue = fdc[i];
          const name =
            String(curValue).toLowerCase() === 'true' ? 'FDC Yes' : 'FDC No';

          const currentSubjects = data.filter(item => {
            return String(curValue) === String(item.fdc);
          });

          const target = axis.buildOther({
            name,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }

      if (sexcodeId.length !== 0) {
        for (let i = 0; i < sexcodeId.length; i++) {
          const curCodeId = Number(sexcodeId[i]);
          const codeName = build.displayName({
            source: sexCodeItems,
            value: curCodeId
          });

          const currentSubjects = data.filter(item => {
            return curCodeId === item.sexcodeId;
          });

          const target = axis.buildOther({
            name: codeName,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }

      if (ethniccodeId.length !== 0) {
        for (let i = 0; i < ethniccodeId.length; i++) {
          const curCodeId = Number(ethniccodeId[i]);
          const codeName = build.displayName({
            source: ethnicItems,
            value: curCodeId
          });

          const currentSubjects = data.filter(item => {
            return curCodeId === item.ethniccodeId;
          });

          const target = axis.buildOther({
            name: codeName,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }

      if (ageRange.length !== 0) {
        for (let i = 0; i < ageRange.length; i++) {
          const curAgeRange = ageRange[i];
          const ageRangeTitle = `${curAgeRange[0]} ~ ${curAgeRange[1]}`;

          const currentSubjects = data.filter(item => {
            return checkAge(curAgeRange, item.age);
          });

          const target = axis.buildOther({
            name: ageRangeTitle,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }

      if (nat2PhenResCodeId.length !== 0) {
        for (let i = 0; i < nat2PhenResCodeId.length; i++) {
          const curCodeId = Number(nat2PhenResCodeId[i]);
          const codeName = build.displayName({
            source: nat2PhenResCodes,
            value: curCodeId
          });

          const currentSubjects = data.filter(item => {
            return curCodeId === item.nat2PhenResCodeId;
          });

          const target = axis.buildOther({
            name: codeName,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }

      if (slco1b1PhenResCodeId.length !== 0) {
        for (let i = 0; i < slco1b1PhenResCodeId.length; i++) {
          const curCodeId = Number(slco1b1PhenResCodeId[i]);
          const codeName = build.displayName({
            source: slcob1PhenResCodes,
            value: curCodeId
          });

          const currentSubjects = data.filter(item => {
            return curCodeId === item.slco1b1PhenResCodeId;
          });

          const target = axis.buildOther({
            name: codeName,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }

      if (comorbidcodeId.length !== 0) {
        for (let i = 0; i < comorbidcodeId.length; i++) {
          const curCodeId = Number(comorbidcodeId[i]);
          const codeName = build.displayName({
            source: comorbidCodes,
            value: curCodeId
          });

          const currentSubjects = data.filter(item => {
            return item.comorbidcodeId.includes(String(curCodeId));
          });

          const target = axis.buildOther({
            name: codeName,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }

      if (hasAdr.length !== 0) {
        for (let i = 0; i < hasAdr.length; i++) {
          const curValue = hasAdr[i];
          const name =
            String(curValue).toLowerCase() === 'true' ? 'ADR Yes' : 'ADR No';

          const currentSubjects = data.filter(item => {
            return String(curValue) === String(item.hasAdr);
          });

          const target = axis.buildOther({
            name,
            targets: currentSubjects,
            color:
              multipleChartColor[
                multipleFilterColorIndex++ % multipleChartColor.length
              ]
          });

          dataSet.push(target);
        }
      }
    } else {
      if (isFiltered(filter)) {
        const currentSubjects = data.filter(item => {
          return (
            (projectSiteId === 0 || projectSiteId === item.projectSiteId) &&
            (subjectIds === null ||
              subjectIds.length === 0 ||
              subjectIds.includes(item.subjectId)) &&
            (sexcodeId === 0 || sexcodeId === item.sexcodeId) &&
            (ethniccodeId === 0 || ethniccodeId === item.ethniccodeId) &&
            checkAge(ageRange, item.age) &&
            (nat2PhenResCodeId === 0 ||
              nat2PhenResCodeId === item.nat2PhenResCodeId) &&
            (slco1b1PhenResCodeId === 0 ||
              slco1b1PhenResCodeId === item.slco1b1PhenResCodeId) &&
            (comorbidcodeId === 0 ||
              item.comorbidcodeId.includes(String(comorbidcodeId))) &&
            (hasAdr === 0 || String(hasAdr) === String(item.hasAdr))
          );
        });

        const target = axis.buildOther({
          name: 'Target Subject',
          targets: currentSubjects,
          color: chartColor[1]
        });

        dataSet.push(target);
      }
    }

    if (predictItems.length > 0) {
      dataSet.push(chartDataGenerate(predictItems));
    }
    return dataSet;
  };

  return useMemo(() => generateData(), [
    filter,
    exdrugId,
    source,
    predictItems
  ]);
};

export default useDrugChartData;
