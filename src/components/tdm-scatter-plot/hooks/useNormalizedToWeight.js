import { useEffect, useState } from 'react';

const useNormalizedToWeight = ({ source, normailzeWeight = 60 }) => {
  const [items, setItems] = useState([]);

  useEffect(
    () => {
      const newItems = source.map(item => {
        const drugConc =
          (Number(item.drugConc) / Number(item.weight)) * normailzeWeight;

        return {
          ...item,
          drugConc
        };
      });

      setItems(newItems);
    },
    [source, normailzeWeight]
  );

  return [{ items }];
};

export default useNormalizedToWeight;
