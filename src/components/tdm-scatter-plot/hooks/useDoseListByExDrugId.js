import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useDoseListByExDrugId = ({ exdrugId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(
    async () => {
      setLoading(true);
      const res = await api.fetchDoseListByExdrugId({ exdrugId });
      if (res && res.data) {
        const options = res.data.map(item => {
          return {
            text: item,
            value: item
          };
        });

        setLoading(false);
        setItems(options);
      }
    },
    [exdrugId]
  );

  useEffect(
    () => {
      if (exdrugId > 0) {
        fetchItems();
      } else {
        setItems([]);
      }
    },
    [exdrugId]
  );

  return [{ items, loading }];
};

export default useDoseListByExDrugId;
