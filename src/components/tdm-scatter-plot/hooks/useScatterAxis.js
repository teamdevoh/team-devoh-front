import { useFakeFace } from '../../case-review/hooks';

const useScatterAxis = () => {
  const fakeFace = useFakeFace();

  const buildOther = ({ name, targets = [], color }) => {
    const other = {
      name,
      type: 'scatter',
      data: [],
      subjects: [],
      color: !!color ? color : undefined
    };

    targets.forEach(item => {
      const xy = [item.tad, Number(item.drugConc)];
      other.data.push(xy);
      other.subjects.push({
        subjectId: item.subjectId,
        siteName: item.siteName,
        subjectNo: item.subjectNo,
        img: fakeFace.get(item.sexcodeId),
        initial: item.initial,
        drugConcUnit: item.drugConcUnit,
        exdose: item.exdose
      });
    });

    return other;
  };

  return { buildOther };
};

export default useScatterAxis;
