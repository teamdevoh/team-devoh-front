import helpers from '../../helpers';

const fetchUnit = () => {
  return helpers.Service.get(`api/unit`);
};

export default {
  fetchUnit
};
