import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useUnitList = () => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    const res = await api.fetchUnit();

    if (res && res.data) {
      const newItems = res.data.map(item => {
        const { unit1, unitId } = item;
        return {
          text: unit1,
          value: unitId
        };
      });

      setItems(newItems);
    }
    setLoading(false);
  });

  useEffect(() => {
    fetchItems();
  }, []);

  return [{ items, loading }];
};

export default useUnitList;
