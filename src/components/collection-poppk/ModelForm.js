import React, { Fragment, useEffect } from 'react';
import { Container, Form, Input, Radio, TextArea } from 'semantic-ui-react';
import { code, codeGroup } from '../../constants';
import helpers from '../../helpers';
import { useCodes, useFormatMessage, useSubmit } from '../../hooks';
import DatePicker from '../datepicker/DatePicker';

const ModelForm = ({ model, onChange, onSubmit, children }) => {
  const [modelTypeCodes] = useCodes({ codeGroupId: codeGroup.ModelTypeCode });

  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  useEffect(
    () => {
      if (!!!model.modelTypeCodeId && modelTypeCodes.length > 0) {
        onChange(null, {
          name: 'modelTypeCodeId',
          value: modelTypeCodes[0].value
        });
      }
    },
    [modelTypeCodes, model.modelTypeCodeId]
  );

  return (
    <Container>
      <Form onSubmit={onValidate}>
        <Form.Group
          {...(helpers.util.isMobile() ? { grouped: true } : { inline: true })}
        >
          <label>{t('model.type')}</label>
          {modelTypeCodes.map((item, index) => {
            return (
              <Form.Field key={item.value}>
                <Radio
                  name="modelTypeCodeId"
                  label={item.text}
                  value={item.value}
                  checked={item.value === model.modelTypeCodeId}
                  onChange={onChange}
                />
              </Form.Field>
            );
          })}
          {validator.message(
            t('model.type'),
            model.modelTypeCodeId,
            'required'
          )}
        </Form.Group>
        <Form.Field required>
          <label>{t('title')}</label>
          <Input
            name="modelTitle"
            value={model.modelTitle}
            onChange={onChange}
          />
          {validator.message(t('title'), model.modelTitle, 'required')}
        </Form.Field>
        <Form.Field>
          <label>{t('common.date')}</label>
          <DatePicker
            name="modelDate"
            value={model.modelDate}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('author')}</label>
          <Input
            name="modelAuthor"
            value={model.modelAuthor}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('version')}</label>
          <Input
            name="modelVersion"
            value={model.modelVersion}
            onChange={onChange}
          />
        </Form.Field>
        {model.modelTypeCodeId === code.TDMModel && (
          <Fragment>
            <Form.Group inline>
              <Form.Field>
                <label>{t('inhouse.developed.model')}</label>
                <Radio
                  name="isInhouseModel"
                  label="Yes"
                  checked={model.isInhouseModel === true}
                  onChange={(event, data) => {
                    onChange(event, {
                      ...data,
                      value: data.checked ? true : false
                    });
                  }}
                />
              </Form.Field>
              <Form.Field>
                <Radio
                  name="isInhouseModel"
                  label="No"
                  checked={model.isInhouseModel === false}
                  onChange={(event, data) => {
                    onChange(event, {
                      ...data,
                      value: data.checked ? true : false
                    });
                  }}
                />
              </Form.Field>
              {validator.message(
                t('inhouse.developed.model'),
                model.isInhouseModel,
                'required'
              )}
            </Form.Group>
          </Fragment>
        )}
        <Form.Field>
          <label>{t('comment')}</label>
          <TextArea
            name="modelComment"
            value={model.modelComment}
            onChange={onChange}
          />
        </Form.Field>
        {children}
      </Form>
    </Container>
  );
};

export default ModelForm;
