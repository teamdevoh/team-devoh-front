import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const usePopPKModelList = ({ tbdrugId }) => {
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(
    async () => {
      const res = await api.fetchPopPKModels({ projectId: 152, tbdrugId });
      if (res && res.data) {
        setItems(res.data);
      }
    },
    [tbdrugId]
  );

  useEffect(
    () => {
      if (tbdrugId > 0) {
        fetchItems();
      }
    },
    [tbdrugId]
  );

  return { items };
};

export default usePopPKModelList;
