import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const usePopPKModelDetail = ({ projectId, tbdrugId, tbdrugPopPkmodelId }) => {
  const [item, setItem] = useState({});

  const fetchItem = useCallback(
    async () => {
      const res = await api.fetchPopPKModel({
        projectId,
        tbdrugId,
        tbdrugPopPkmodelId
      });
      if (res && res.data) {
        setItem(res.data);
      }
    },
    [tbdrugPopPkmodelId]
  );

  useEffect(
    () => {
      if (tbdrugPopPkmodelId > 0) {
        fetchItem();
      }
    },
    [tbdrugPopPkmodelId]
  );

  return { item };
};

export default usePopPKModelDetail;
