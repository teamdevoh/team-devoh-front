import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useTbDrugsForPopPK = () => {
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(async () => {
    const res = await api.fetchTbDrugsForPopPK({ projectId: 152 });
    if (res && res.data) {
      setItems(res.data);
    }
  }, []);

  useEffect(() => {
    fetchItems();
  }, []);

  return { items };
};

export default useTbDrugsForPopPK;
