import api from '../api';

const usePopPKModel = () => {
  const Add = async ({ projectId, tbdrugId, model }) => {
    model.isInHouseModel = model.isInHouseModel === '1' ? true : false;
    const res = await api.addPopPK({
      projectId,
      tbdrugId,
      model
    });
    if (res && res.data) {
      return res.data;
    }
  };

  const edit = async ({ projectId, tbdrugId, tbdrugPopPkmodelId, model }) => {
    const res = await api.updatePopPK({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId,
      model
    });
    return res.data;
  };

  const remove = async ({ projectId, tbdrugId, tbdrugPopPkmodelId }) => {
    const res = await api.removePopPK({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId
    });
    return res.data;
  };

  return { Add, edit, remove };
};

export default usePopPKModel;
