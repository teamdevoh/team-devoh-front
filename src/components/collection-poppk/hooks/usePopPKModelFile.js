import { useCallback } from 'react';
import api from '../api';

const usePopPKModelFile = () => {
  const add = useCallback(
    async ({ projectId, tbdrugId, tbdrugPopPkmodelId, model }) => {
      const res = await api.addPopPKFile({
        projectId,
        tbdrugId,
        tbdrugPopPkmodelId,
        model
      });
      return res.data;
    },
    []
  );

  const remove = useCallback(
    async ({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId,
      tbdrugPopPkmodelFileId
    }) => {
      const res = await api.removePopPKFile({
        projectId,
        tbdrugId,
        tbdrugPopPkmodelId,
        tbdrugPopPkmodelFileId
      });
      return res.data;
    },
    []
  );

  const download = useCallback(
    async ({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId,
      tbdrugPopPkmodelFileId
    }) => {
      await api.fetchPopPKModelFileDownload({
        projectId,
        tbdrugId,
        tbdrugPopPkmodelId,
        tbdrugPopPkmodelFileId,
        progress: percent => {
          if (percent === 100) {
            // setLoading(false);
          }
        }
      });
    }
  );

  const downloadHistory = useCallback(
    async ({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId,
      tbdrugPopPkmodelFileId
    }) => {
      const res = await api.fetchPopPKModelFileDownloadHistories({
        projectId,
        tbdrugId,
        tbdrugPopPkmodelId,
        tbdrugPopPkmodelFileId
      });
      if (res && res.data) {
        return res.data;
      }
      return [];
    }
  );

  return { add, remove, download, downloadHistory };
};

export default usePopPKModelFile;
