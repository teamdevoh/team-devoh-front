import { useCallback, useEffect, useState } from 'react';
import { PopPKFile } from '../../../constants/category';
import api from '../api';

const usePopPKModelFileList = ({ projectId, tbdrugId, tbdrugPopPkmodelId }) => {
  const [originalItems, setOriginalItems] = useState([]);
  const [nonmemItems, setNonmemItems] = useState([]);
  const [modelItems, setModelItems] = useState([]);

  const fetchItems = useCallback(async () => {
    const res = await api.fetchPopPKModelFiles({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId
    });
    if (res && res.data) {
      setOriginalItems(_.filter(res.data, { fileType: PopPKFile.Original }));
      setNonmemItems(_.filter(res.data, { fileType: PopPKFile.Nonmem }));
      setModelItems(_.filter(res.data, { fileType: PopPKFile.Model }));
    }
  }, []);

  const refresh = useCallback(async () => {
    await fetchItems();
  }, []);

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, tbdrugId, tbdrugPopPkmodelId]
  );

  return { originalItems, nonmemItems, modelItems, refresh };
};

export default usePopPKModelFileList;
