import helpers from '../../helpers';

const addPopPK = ({ projectId, tbdrugId, model }) => {
  return helpers.Service.post(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models`,
    model
  );
};

const updatePopPK = ({ projectId, tbdrugId, tbdrugPopPkmodelId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models/${tbdrugPopPkmodelId}`,
    model
  );
};

const removePopPK = ({ projectId, tbdrugId, tbdrugPopPkmodelId }) => {
  return helpers.Service.delete(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models/${tbdrugPopPkmodelId}`
  );
};

const fetchTbDrugsForPopPK = ({ projectId }) => {
  return helpers.Service.get(`api/projects/${projectId}/poppk.tbdrugs`);
};

const fetchPopPKModels = ({ projectId, tbdrugId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models`
  );
};

const fetchPopPKModel = ({ projectId, tbdrugId, tbdrugPopPkmodelId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models/${tbdrugPopPkmodelId}`
  );
};

const addPopPKFile = ({ projectId, tbdrugId, tbdrugPopPkmodelId, model }) => {
  return helpers.Service.post(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models/${tbdrugPopPkmodelId}/files`,
    model
  );
};

const removePopPKFile = ({
  projectId,
  tbdrugId,
  tbdrugPopPkmodelId,
  tbdrugPopPkmodelFileId
}) => {
  return helpers.Service.delete(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models/${tbdrugPopPkmodelId}/files/${tbdrugPopPkmodelFileId}`
  );
};

const fetchPopPKModelFiles = ({ projectId, tbdrugId, tbdrugPopPkmodelId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models/${tbdrugPopPkmodelId}/files`
  );
};

const fetchPopPKModelFileDownload = ({
  projectId,
  tbdrugId,
  tbdrugPopPkmodelId,
  tbdrugPopPkmodelFileId,
  progress,
  resolve,
  reject
}) => {
  return helpers.util.download(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models/${tbdrugPopPkmodelId}/files/${tbdrugPopPkmodelFileId}`,
    progress,
    resolve,
    reject
  );
};

const fetchPopPKModelFileDownloadHistories = ({
  projectId,
  tbdrugId,
  tbdrugPopPkmodelId,
  tbdrugPopPkmodelFileId
}) => {
  return helpers.Service.get(
    `api/projects/${projectId}/poppk.tbdrugs/${tbdrugId}/models/${tbdrugPopPkmodelId}/files/${tbdrugPopPkmodelFileId}/download.history`
  );
};

export default {
  addPopPK,
  updatePopPK,
  removePopPK,
  fetchTbDrugsForPopPK,
  fetchPopPKModels,
  fetchPopPKModel,
  addPopPKFile,
  removePopPKFile,
  fetchPopPKModelFiles,
  fetchPopPKModelFileDownload,
  fetchPopPKModelFileDownloadHistories
};
