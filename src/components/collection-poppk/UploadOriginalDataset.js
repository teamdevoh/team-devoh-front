import React, { useCallback } from 'react';
import { useFormatMessage, useUpload } from '../../hooks';
import UploadLayer from '../modules/UploadLayer';
import { usePopPKModelFile } from './hooks';

const UploadOriginalDataset = ({
  projectId,
  tbdrugId,
  tbdrugPopPkmodelId,
  fileType,
  open,
  onClose,
  uploadDone
}) => {
  const t = useFormatMessage();
  const popPKModelFile = usePopPKModelFile();
  const file = useUpload();

  const handleDrop = useCallback(acceptedFiles => {
    onClose();
    let uploadLength = 0;

    file.upload(acceptedFiles, ids => {
      ids.forEach(id => {
        const model = {
          fileStrorageId: id,
          fileType: fileType
        };
        popPKModelFile
          .add({ projectId, tbdrugId, tbdrugPopPkmodelId, model })
          .then(isOK => {
            uploadLength++;
            if (ids.length === uploadLength) {
              uploadDone();
            }
          });
      });
    });
  }, []);

  return (
    <UploadLayer
      open={open}
      onClose={onClose}
      title={t('common.attachment.upload')}
      onDrop={handleDrop}
    />
  );
};

export default UploadOriginalDataset;
