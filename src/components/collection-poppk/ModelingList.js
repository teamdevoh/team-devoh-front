import React, { Fragment } from 'react';
import { Message } from 'semantic-ui-react';
import { ModelingListItem } from './';
import { usePopPKModelList } from './hooks';

const ModelingList = ({ projectId, tbdrugId, filterModelTypeCode }) => {
  const { items } = usePopPKModelList({ projectId, tbdrugId });

  return (
    <Fragment>
      {items.length > 0 &&
        items
          .filter(item => {
            return (
              filterModelTypeCode === 0 ||
              item.modelTypeCodeId === filterModelTypeCode
            );
          })
          .map(item => {
            return (
              <ModelingListItem
                key={item.tbdrugPopPkmodelId}
                projectId={projectId}
                {...item}
              />
            );
          })}
      {items.length === 0 && <Message>No Data</Message>}
    </Fragment>
  );
};

export default ModelingList;
