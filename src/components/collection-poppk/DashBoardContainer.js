import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { Button, Grid, Header, Icon } from 'semantic-ui-react';
import { PopPK_Edit } from '../../constants/role';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import poppk from '../../states/poppk';
import { RoleAware } from '../auth';
import { ModelingList, DrugList } from './';

const DashBoardContainer = ({ match }) => {
  const { projectId } = match.params;
  const t = useFormatMessage();
  const selectedRow = useSelector(poppk.selectors.getSelectedItem);

  const handleAddModelClick = useCallback(
    () => {
      helpers.history.push(
        `/project/${projectId}/collection/poppk/${selectedRow.tbdrugId}/new`
      );
    },
    [selectedRow]
  );

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column width="eight">
          <Header as="h3">
            <Icon name="pills" />
            <Header.Content>TB Drugs</Header.Content>
          </Header>
          <div
            style={{
              height: helpers.util.isMobile() ? '250px' : '780px',
              padding: '20px',
              overflowY: 'auto',
              border: 'solid 1px #d2d2d2',
              color: '#666'
            }}
          >
            <DrugList />
          </div>
        </Grid.Column>
        <Grid.Column width="eight">
          {selectedRow.tbdrugId > 0 && (
            <Grid>
              <Grid.Row style={{ marginBottom: '-1.5em' }}>
                <Grid.Column width="eight">
                  <Header as="h3">
                    <Icon name="list alternate" />
                    <Header.Content>
                      {selectedRow.genericName} Modeling List
                    </Header.Content>
                  </Header>
                </Grid.Column>
                <Grid.Column width="eight">
                  <RoleAware allowedRole={PopPK_Edit}>
                    <Button
                      floated="right"
                      size="small"
                      onClick={handleAddModelClick}
                    >
                      {t('add.model')}
                    </Button>
                  </RoleAware>
                </Grid.Column>
              </Grid.Row>
              <Grid.Column width="sixteen">
                <div
                  style={{
                    height: helpers.util.isMobile() ? '250px' : '700px',
                    padding: '20px',
                    overflowY: 'auto',
                    border: 'solid 1px #d2d2d2',
                    color: '#666'
                  }}
                >
                  <ModelingList
                    projectId={projectId}
                    tbdrugId={selectedRow.tbdrugId}
                    filterModelTypeCode={selectedRow.modelType}
                  />
                </div>
              </Grid.Column>
            </Grid>
          )}
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default DashBoardContainer;
