import React from 'react';
import { Table } from 'semantic-ui-react';
import { DrugListItem } from './';
import { useTbDrugsForPopPK } from './hooks';

const DrugList = ({ onRowClick }) => {
  const TbDrugsForPopPK = useTbDrugsForPopPK();

  return (
    <Table celled selectable>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Short</Table.HeaderCell>
          <Table.HeaderCell>Generic name</Table.HeaderCell>
          {/* <Table.HeaderCell>Metabolite</Table.HeaderCell> */}
          <Table.HeaderCell style={{ backgroundColor: '#F9DCD3' }}>
            InHouse Developed Model
          </Table.HeaderCell>
          <Table.HeaderCell style={{ backgroundColor: '#EDF9D3' }}>
            TDM Model
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {TbDrugsForPopPK.items.map(item => {
          return <DrugListItem key={item.tbdrugId} {...item} />;
        })}
      </Table.Body>
    </Table>
  );
};

export default DrugList;
