import React, { Fragment, useCallback, useState } from 'react';
import { useBoolean } from 'react-hanger';
import { Grid, Header, Icon, List, Segment } from 'semantic-ui-react';
import { ModelFileList, UploadOriginalDataset } from '.';
import { PopPKFile } from '../../constants/category';
import { PopPK_Edit } from '../../constants/role';
import { usePopPKModelFile, usePopPKModelFileList } from './hooks';
import { Modal } from '../modal';
import helpers from '../../helpers';
import { YYYY_MM_DD_HHMM } from '../../constants/format';

const ModelFileListContainer = ({
  projectId,
  tbdrugId,
  tbdrugPopPkmodelId,
  isMyModel = false
}) => {
  const [downloadHistories, setDownloadHistories] = useState([]);
  const showHistory = useBoolean(false);
  const {
    originalItems,
    nonmemItems,
    modelItems,
    refresh
  } = usePopPKModelFileList({
    projectId,
    tbdrugId,
    tbdrugPopPkmodelId
  });
  const popPKModelFile = usePopPKModelFile({
    projectId,
    tbdrugId,
    tbdrugPopPkmodelId
  });

  const originalUpload = useBoolean(false);
  const nonmemUpload = useBoolean(false);
  const modelUpload = useBoolean(false);

  const handleDownloadClick = useCallback(async id => {
    await popPKModelFile.download({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId,
      tbdrugPopPkmodelFileId: id
    });
  }, []);

  const handleDeleteClick = useCallback(async id => {
    const isOK = await popPKModelFile.remove({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId,
      tbdrugPopPkmodelFileId: id
    });
    if (isOK) {
      await refresh();
    }
  }, []);

  const handleUploadDone = useCallback(async () => {
    await refresh();
  }, []);

  const handleHistoryClick = useCallback(async id => {
    const items = await popPKModelFile.downloadHistory({
      projectId,
      tbdrugId,
      tbdrugPopPkmodelId,
      tbdrugPopPkmodelFileId: id
    });
    showHistory.setTrue();
    setDownloadHistories(items);
  }, []);

  const hasEditRole = helpers.Identity.hasRole(PopPK_Edit);

  return (
    <Fragment>
      {(originalItems.length > 0 || (hasEditRole && isMyModel)) && (
        <Segment>
          <Grid>
            <Grid.Row>
              <Grid.Column floated="left" width={5}>
                <Header as="h4">Original Dataset</Header>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                {hasEditRole &&
                  isMyModel && (
                    <Icon
                      name="upload"
                      size="large"
                      className="link"
                      onClick={originalUpload.setTrue}
                      style={{ float: 'right' }}
                    />
                  )}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <ModelFileList
                  isMyModel={isMyModel}
                  items={originalItems}
                  onHistoryClick={handleHistoryClick}
                  onDownloadClick={handleDownloadClick}
                  onDeleteClick={handleDeleteClick}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <UploadOriginalDataset
            projectId={projectId}
            tbdrugId={tbdrugId}
            tbdrugPopPkmodelId={tbdrugPopPkmodelId}
            fileType={PopPKFile.Original}
            open={originalUpload.value}
            onClose={originalUpload.setFalse}
            uploadDone={handleUploadDone}
          />
        </Segment>
      )}
      {(nonmemItems.length > 0 || (hasEditRole && isMyModel)) && (
        <Segment>
          <Grid>
            <Grid.Row>
              <Grid.Column floated="left" width={5}>
                <Header as="h4">Nonmem Dataset</Header>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                {hasEditRole &&
                  isMyModel && (
                    <Icon
                      name="upload"
                      size="large"
                      className="link"
                      onClick={nonmemUpload.setTrue}
                      style={{ float: 'right' }}
                    />
                  )}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <ModelFileList
                  isMyModel={isMyModel}
                  items={nonmemItems}
                  onHistoryClick={handleHistoryClick}
                  onDownloadClick={handleDownloadClick}
                  onDeleteClick={handleDeleteClick}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <UploadOriginalDataset
            projectId={projectId}
            tbdrugId={tbdrugId}
            tbdrugPopPkmodelId={tbdrugPopPkmodelId}
            fileType={PopPKFile.Nonmem}
            open={nonmemUpload.value}
            onClose={nonmemUpload.setFalse}
            uploadDone={handleUploadDone}
          />
        </Segment>
      )}
      {(modelItems.length > 0 || (hasEditRole && isMyModel)) && (
        <Segment>
          <Grid>
            <Grid.Row>
              <Grid.Column floated="left" width={5}>
                <Header as="h4">Model</Header>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                {hasEditRole &&
                  isMyModel && (
                    <Icon
                      name="upload"
                      size="large"
                      className="link"
                      onClick={modelUpload.setTrue}
                      style={{ float: 'right' }}
                    />
                  )}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <ModelFileList
                  isMyModel={isMyModel}
                  items={modelItems}
                  onHistoryClick={handleHistoryClick}
                  onDownloadClick={handleDownloadClick}
                  onDeleteClick={handleDeleteClick}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <UploadOriginalDataset
            projectId={projectId}
            tbdrugId={tbdrugId}
            tbdrugPopPkmodelId={tbdrugPopPkmodelId}
            fileType={PopPKFile.Model}
            open={modelUpload.value}
            onClose={modelUpload.setFalse}
            uploadDone={handleUploadDone}
          />
        </Segment>
      )}
      <Modal
        open={showHistory.value}
        onOK={showHistory.setFalse}
        title="Download Logs"
        content={
          <List celled>
            {_.orderBy(downloadHistories, ['created'], 'desc').map(
              (item, index) => {
                return (
                  <List.Item key={index}>
                    <List.Content>
                      {helpers.util.dateformat(item.created, YYYY_MM_DD_HHMM)} /{' '}
                      {item.memberName} / {item.siteName}
                    </List.Content>
                  </List.Item>
                );
              }
            )}
          </List>
        }
      />
    </Fragment>
  );
};

export default ModelFileListContainer;
