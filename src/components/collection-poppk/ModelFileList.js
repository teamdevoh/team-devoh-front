import React, { useCallback, useState } from 'react';
import { Button, Icon, List } from 'semantic-ui-react';
import { YYYY_MM_DD_HHMM } from '../../constants/format';
import { PopPK_Edit, PopPK_Read } from '../../constants/role';
import helpers from '../../helpers';
import { useBoolean, useFormatMessage } from '../../hooks';
import { RoleAware } from '../auth';
import { Confirm } from '../modal';
import { FileMeta, FileTitle } from '../modules';

const ModelFileList = ({
  isMyModel,
  items,
  onHistoryClick,
  onDownloadClick,
  onDeleteClick
}) => {
  const t = useFormatMessage();

  const isRemove = useBoolean(false);
  const [selectedItem, setSelectedItem] = useState({ name: '', id: 0 });

  const handleDeleteClick = useCallback(
    () => {
      isRemove.setFalse();
      onDeleteClick(selectedItem.id);
      setSelectedItem({ name: '', id: 0 });
    },
    [selectedItem]
  );

  return (
    <div>
      <List divided relaxed>
        {items.map(item => {
          return (
            <List.Item key={item.tbdrugPopPkmodelFileId}>
              <List.Content style={{ float: 'right' }}>
                <RoleAware allowedRole={PopPK_Read}>
                  <Button
                    type="button"
                    icon="download"
                    color="blue"
                    size="mini"
                    onClick={() => {
                      onDownloadClick(item.tbdrugPopPkmodelFileId);
                    }}
                  />
                </RoleAware>
                {isMyModel && (
                  <RoleAware allowedRole={PopPK_Edit}>
                    <Button
                      type="button"
                      icon="delete"
                      color="red"
                      size="mini"
                      onClick={() => {
                        setSelectedItem({
                          name: item.originalName,
                          id: item.tbdrugPopPkmodelFileId
                        });
                        isRemove.setTrue();
                      }}
                    />
                  </RoleAware>
                )}
              </List.Content>
              <List.Content>
                <RoleAware allowedRole={PopPK_Read}>
                  <Icon.Group
                    size="large"
                    className="link"
                    style={{ float: 'left', marginRight: '1em' }}
                    onClick={() => {
                      onHistoryClick(item.tbdrugPopPkmodelFileId);
                    }}
                  >
                    <Icon name="history" />
                    <Icon corner name="download" />
                  </Icon.Group>
                </RoleAware>
                <FileTitle name={item.originalName} />
                <FileMeta
                  size={helpers.util.formatFileSize(item.size)}
                  created={helpers.util.dateformat(
                    item.created,
                    YYYY_MM_DD_HHMM
                  )}
                  creator={item.creator}
                />
              </List.Content>
            </List.Item>
          );
        })}
      </List>
      <Confirm
        title={t('common.attach.delete.title')}
        content={t('common.attach.delete', { name: selectedItem.name })}
        open={isRemove.value}
        onOK={handleDeleteClick}
        loading={false}
        onCancel={isRemove.setFalse}
      />
    </div>
  );
};

export default ModelFileList;
