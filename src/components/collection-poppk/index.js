import { asyncComponent } from '../modules';

const DashBoard = asyncComponent(() => import('./DashBoardContainer'));
const DrugList = asyncComponent(() => import('./DrugList'));
const DrugListItem = asyncComponent(() => import('./DrugListItem'));
const ModelingList = asyncComponent(() => import('./ModelingList'));
const ModelingListItem = asyncComponent(() => import('./ModelingListItem'));
const ModelForm = asyncComponent(() => import('./ModelForm'));
const ModelFormAdd = asyncComponent(() => import('./ModelFormAdd'));
const ModelFormEdit = asyncComponent(() => import('./ModelFormEdit'));
const UploadOriginalDataset = asyncComponent(() => import('./UploadOriginalDataset'));
const ModelFileListContainer = asyncComponent(() => import('./ModelFileListContainer'));
const ModelFileList = asyncComponent(() => import('./ModelFileList'));

export { DashBoardContainer, DrugList, DrugListItem, ModelingList, ModelingListItem, ModelForm, ModelFormAdd, ModelFormEdit, UploadOriginalDataset, ModelFileListContainer, ModelFileList };
