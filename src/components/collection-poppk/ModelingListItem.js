import React, { Fragment, useCallback } from 'react';
import { Divider, Header, Icon, Segment } from 'semantic-ui-react';
import { code } from '../../constants';
import { YYYY_MM_DD, YYYY_MM_DD_HHMM } from '../../constants/format';
import { PopPK_Edit } from '../../constants/role';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { RoleAware } from '../auth';
import ModelFileListContainer from './ModelFileListContainer';

const ModelingListItem = ({
  projectId,
  authorId,
  tbdrugPopPkmodelId,
  tbdrugId,
  modelTitle,
  modelTypeCodeId,
  modelTypeName,
  modelDate,
  modelAuthor,
  modelComment,
  modelVersion,
  modelType,
  isInhouseModel,
  creator,
  created
}) => {
  const t = useFormatMessage();
  const dateformat = helpers.util.dateformat;

  const handleEditClick = useCallback(
    () => {
      helpers.history.push(
        `/project/${projectId}/collection/poppk/${tbdrugId}/edit/${tbdrugPopPkmodelId}`
      );
    },
    [projectId, tbdrugId, tbdrugPopPkmodelId]
  );
  const isMyModel =
    helpers.Identity.profile.id === authorId && tbdrugPopPkmodelId > 0;

  return (
    <Fragment>
      <div style={{ marginBottom: '1em' }}>
        <Header
          attached="top"
          style={{
            backgroundColor:
              modelTypeCodeId === code.TDMModel ? '#EDF9D3' : '#F9DCD3'
          }}
        >
          {modelTypeName}
          {isMyModel && (
            <RoleAware allowedRole={PopPK_Edit}>
              <Icon
                style={{ float: 'right', cursor: 'pointer' }}
                name="edit"
                onClick={handleEditClick}
              />
            </RoleAware>
          )}
        </Header>
        <Segment attached>
          <div>
            {t('title')}: <b style={{ color: 'blue' }}>{modelTitle}</b>
          </div>
          <div>
            {t('common.date')}:{' '}
            <b style={{ color: 'blue' }}>{dateformat(modelDate, YYYY_MM_DD)}</b>
          </div>
          <div>
            {t('author')}: <b style={{ color: 'blue' }}>{modelAuthor}</b>
          </div>
          <div>
            {t('version')}: <b style={{ color: 'blue' }}>{modelVersion}</b>
          </div>
          {modelTypeCodeId === code.TDMModel && (
            <div>
              {t('inhouse.developed.model')}:{' '}
              <b style={{ color: 'blue' }}>{isInhouseModel ? 'Yes' : 'No'}</b>
            </div>
          )}
          <div>{t('comment')}:</div>
          <b style={{ color: 'blue' }}>
            {modelComment.split('\n').map((item, key) => {
              return (
                <Fragment key={key}>
                  {item}
                  <br />
                </Fragment>
              );
            })}
          </b>
          <ModelFileListContainer
            projectId={projectId}
            tbdrugId={tbdrugId}
            tbdrugPopPkmodelId={tbdrugPopPkmodelId}
            isMyModel={isMyModel}
          />
          <Divider
            style={{
              fontStyle: 'italic',
              fontWeight: 'lighter',
              fontSize: '0.8em'
            }}
          >
            {t('activity.author.date', {
              author: creator,
              date: dateformat(created, YYYY_MM_DD_HHMM)
            })}
          </Divider>
        </Segment>
      </div>
    </Fragment>
  );
};

export default ModelingListItem;
