import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button, Header } from 'semantic-ui-react';
import { role } from '../../constants';
import helpers from '../../helpers';
import { useBoolean, useFormatMessage } from '../../hooks';
import { RemoveButton, SaveButton } from '../button';
import { Confirm, notification } from '../modal';
import { usePopPKModelDetail } from './hooks';
import usePopPKModel from './hooks/usePopPKModel';
import { ModelFileListContainer, ModelForm } from './';
import { useSelector } from 'react-redux';
import poppk from '../../states/poppk';

const ModelFormEdit = ({ match }) => {
  const { projectId, tbdrugPopPkmodelId, tbdrugId } = match.params;
  const selectedTbDrugItem = useSelector(poppk.selectors.getSelectedItem);
  const showRemove = useBoolean(false);
  const loading = useBoolean(false);
  const t = useFormatMessage();
  const popPK = usePopPKModel();
  const popPKModelDetail = usePopPKModelDetail({
    projectId,
    tbdrugId,
    tbdrugPopPkmodelId
  });

  const [model, setModel] = useState({});

  useEffect(
    () => {
      setModel({ ...popPKModelDetail.item });
    },
    [popPKModelDetail.item]
  );

  const handleChange = useCallback(
    (event, data) => {
      setModel({ ...model, [data.name]: data.value });
    },
    [model]
  );

  const onSubmit = async () => {
    loading.setTrue();
    const isOK = await popPK.edit({
      projectId: projectId,
      tbdrugId,
      tbdrugPopPkmodelId,
      model
    });

    if (isOK) {
      notification.success({
        title: t('common.alert.changed'),
        onClose: async () => {}
      });
    }
    loading.setFalse();
  };

  const onRemoveClick = async () => {
    loading.setTrue();
    const isOK = await popPK.remove({
      projectId: projectId,
      tbdrugId,
      tbdrugPopPkmodelId
    });

    if (isOK) {
      notification.success({
        title: t('common.alert.deleted'),
        onClose: async () => {
          showRemove.setFalse();
          helpers.history.replace(`/project/${projectId}/collection/poppk`);
        }
      });
    }
    loading.setFalse();
  };

  const handleBackButtonClick = useCallback(
    () => {
      helpers.history.push(`/project/${projectId}/collection/poppk`);
    },
    [projectId]
  );

  const isMyModel =
    helpers.Identity.profile.id === model.authorId && tbdrugPopPkmodelId > 0;

  return (
    <Fragment>
      <Header dividing>{selectedTbDrugItem.genericName} Edit Model</Header>
      <ModelForm model={model} onChange={handleChange} onSubmit={onSubmit}>
        <Button
          type="button"
          style={{ marginTop: '0.5em' }}
          content={t('common.back')}
          onClick={handleBackButtonClick}
        />
        {isMyModel && (
          <SaveButton
            allowedRole={role.PopPK_Edit}
            name={t('common.save')}
            loading={loading.value}
          />
        )}
        {isMyModel && (
          <RemoveButton
            allowedRole={role.PopPK_Edit}
            name={t('common.remove')}
            onClick={showRemove.setTrue}
          />
        )}
        <ModelFileListContainer
          projectId={projectId}
          tbdrugId={tbdrugId}
          tbdrugPopPkmodelId={tbdrugPopPkmodelId}
          isMyModel={isMyModel}
        />
      </ModelForm>
      <Confirm
        title={t('poppkmodel.delete.title', { name: model.modelTitle })}
        content={t('poppkmodel.delete', { name: model.modelTitle })}
        open={showRemove.value}
        onOK={onRemoveClick}
        onCancel={showRemove.setFalse}
        loading={loading.value}
      />
    </Fragment>
  );
};

export default ModelFormEdit;
