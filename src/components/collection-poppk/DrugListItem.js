import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Table } from 'semantic-ui-react';
import { code } from '../../constants';
import poppk from '../../states/poppk';

const DrugListItem = ({
  tbdrugId,
  shortName,
  genericName,
  // metabolite,
  inHouseDevelopedModelCount,
  tdmModelCount,
  onClick
}) => {
  const dispatch = useDispatch();
  const selectedRow = useSelector(poppk.selectors.getSelectedItem);

  const handleRowClick = useCallback(
    () => {
      dispatch(
        poppk.actions.selectedItem({ tbdrugId, genericName, modelType: 0 })
      );
    },
    [onClick]
  );

  const handleInHouseModelClick = useCallback(
    () => {
      dispatch(
        poppk.actions.selectedItem({
          tbdrugId,
          genericName,
          modelType: code.InHouseDevelopedModel
        })
      );
    },
    [onClick]
  );

  const handleTDMModelClick = useCallback(
    () => {
      dispatch(
        poppk.actions.selectedItem({
          tbdrugId,
          genericName,
          modelType: code.TDMModel
        })
      );
    },
    [onClick]
  );

  return (
    <Table.Row active={selectedRow.tbdrugId === tbdrugId}>
      <Table.Cell>{shortName}</Table.Cell>
      <Table.Cell style={{ cursor: 'pointer' }} onClick={handleRowClick}>
        <a>{genericName}</a>
      </Table.Cell>
      {/* <Table.Cell>{metabolite}</Table.Cell> */}
      <Table.Cell
        style={{ cursor: 'pointer' }}
        onClick={handleInHouseModelClick}
      >
        <a>{inHouseDevelopedModelCount}</a>
      </Table.Cell>
      <Table.Cell style={{ cursor: 'pointer' }} onClick={handleTDMModelClick}>
        <a>{tdmModelCount}</a>
      </Table.Cell>
    </Table.Row>
  );
};

export default DrugListItem;
