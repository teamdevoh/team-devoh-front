import React, { Fragment, useCallback, useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Header, Message } from 'semantic-ui-react';
import { role } from '../../constants';
import helpers from '../../helpers';
import { useBoolean, useFormatMessage } from '../../hooks';
import poppk from '../../states/poppk';
import { SaveButton } from '../button';
import { notification } from '../modal';
import usePopPKModel from './hooks/usePopPKModel';
import ModelForm from './ModelForm';

const ModelFormAdd = ({ match, history }) => {
  const { projectId, tbdrugId } = match.params;
  const selectedTbDrugItem = useSelector(poppk.selectors.getSelectedItem);
  const loading = useBoolean();
  const t = useFormatMessage();
  const popPK = usePopPKModel();
  const [model, setModel] = useState({
    modelTypeCodeId: '',
    tbdrugId: tbdrugId,
    modelTitle: '',
    modelDate: '',
    modelAuthor: '',
    isInhouseModel: '',
    modelVersion: '',
    modelComment: ''
  });

  const handleChange = useCallback(
    (event, data) => {
      setModel({ ...model, [data.name]: data.value });
    },
    [model]
  );

  const onSubmit = async () => {
    loading.setTrue();
    const newId = await popPK.Add({
      projectId: projectId,
      tbdrugId,
      model
    });
    loading.setFalse();

    if (newId > 0) {
      notification.success({
        title: t('common.alert.added'),
        onClose: async () => {
          helpers.history.push(
            `/project/${projectId}/collection/poppk/${tbdrugId}/edit/${newId}`
          );
        }
      });
    }
  };

  const handleBackButtonClick = useCallback(
    () => {
      helpers.history.push(`/project/${projectId}/collection/poppk`);
    },
    [projectId]
  );

  return (
    <Fragment>
      <Header dividing>{selectedTbDrugItem.genericName} Add Model</Header>
      <ModelForm model={model} onChange={handleChange} onSubmit={onSubmit}>
        <Button
          type="button"
          style={{ marginTop: '0.5em' }}
          content={t('common.back')}
          onClick={handleBackButtonClick}
        />
        <SaveButton
          allowedRole={role.PopPK_Edit}
          name={t('common.save')}
          loading={false}
        />
        <Message>{t('after.saving.attach.info')}</Message>
      </ModelForm>
    </Fragment>
  );
};

export default ModelFormAdd;
