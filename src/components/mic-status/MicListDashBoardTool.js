import React, { Fragment, useState } from 'react';
import { Label } from 'semantic-ui-react';
import {
  FlexDiv,
  RightButtonWrap,
  StyledDetail
} from '../sample-barcode/styles';
import { StyledCSVExport } from '../export-tdm/styles';
import { role } from '../../constants';
import { RoleAware } from '../auth';

const MicListDashBoardTool = ({ total, exportCSV, fileName }) => {
  const [loading, setLoading] = useState(false);

  const handleExport = async () => {
    setLoading(true);
    try {
      await exportCSV(fileName);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Fragment>
      <FlexDiv>
        <Label size="big">
          Counts
          <StyledDetail>{total}</StyledDetail>
        </Label>
        <RightButtonWrap>
          <RoleAware allowedRole={role.MICStatus_Excel_Read}>
            <StyledCSVExport
              positive
              loading={loading}
              icon="download"
              onClick={handleExport}
              content="CSV Export"
            />
          </RoleAware>
        </RightButtonWrap>
      </FlexDiv>
    </Fragment>
  );
};

export default MicListDashBoardTool;
