import React, { useCallback, useEffect, useState } from 'react';
import api from '../api';
import { useFormatMessage } from '../../../hooks';
import helpers from '../../../helpers';
import { format } from '../../../constants';

const useMicResultList = ({
  projectSiteId,
  subjectId,
  tbdrugId,
  dateType,
  fromDate,
  toDate,
  search,
  projectId
}) => {
  const [total, setTotal] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const t = useFormatMessage();

  const columns = [
    {
      key: 'siteName',
      name: t('site'),
      width: 190
    },
    {
      key: 'subjectId',
      name: t('cohort.dashboard.subjects'),
      formatter: ({ value, row }) => {
        const {
          subjectPrefix,
          subjectNo,
          subjectInitial,
          age,
          sex,
          activityId,
          projectActivityId
        } = row;
        return (
          <a
            style={{ cursor: 'pointer' }}
            onClick={() => {
              helpers.history.push(
                `/project/${projectId}/subject/${value}/activitygroup/5?projectActivityId=${projectActivityId}&activityId=${activityId}`
              );
            }}
          >
            {`${subjectPrefix}${subjectNo}/${subjectInitial}/${age}/${sex.substr(
              0,
              1
            )}`}
          </a>
        );
      },
      width: 170
    },
    {
      key: 'micspecdtc',
      name: t('activity.mictransfer.micspecdtc'),
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      },
      width: 100
    },
    {
      key: 'strainreqdtc',
      name: t('activity.mictransfer.strainreqdtc'),
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      },
      width: 110
    },
    {
      key: 'straindelidtc',
      name: t('activity.mictransfer.straindelidtc'),
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      },
      width: 100
    },
    {
      key: 'strainlabel',
      name: t('activity.mictransfer.strainlabel'),
      width: 150
    },
    {
      key: 'subculstor',
      name: t('activity.mictransfer.subculstor')
    },
    {
      key: 'micstartdtc',
      name: t('activity.micresult.micstartdtc'),
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      },
      width: 110
    },
    {
      key: 'micenddtc',
      name: t('activity.micresult.micenddtc'),
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      },
      width: 110
    },
    {
      key: 'tbdrug',
      name: t('antituberculosis.drugs'),
      width: 150
    },
    {
      key: 'testmic',
      name: t('activity.micresult.testmic'),
      formatter: ({ value, row }) => {
        return `${value} μg/ml`;
      },
      width: 110
    },
    {
      key: 'typemic',
      name: t('activity.micresult.typemic'),
      formatter: ({ value, row }) => {
        return `${value} μg/ml`;
      },
      width: 110
    }
  ];

  const fetchItems = useCallback(
    async ({ offset, limit }) => {
      if (dateType > 0 && fromDate === null && toDate === null) {
        return false;
      }

      setLoading(true);
      try {
        const res = await api.fetchSubjectsMicResult({
          projectSiteId,
          subjectId,
          tbdrugId,
          dateType,
          fromDate,
          toDate,
          search,
          limit,
          offset
        });
        if (res && res.data) {
          setTotal(res.data.paging.total);
        }
        return res;
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [projectSiteId, subjectId, tbdrugId, dateType, fromDate, toDate, search]
  );

  const resetItems = useCallback(
    async () => {
      const res = await fetchItems({ offset: 0, limit: 50 });
      if (res && res.data) {
        setItems(res.data.items);
      }
    },
    [projectSiteId, subjectId, tbdrugId, dateType, fromDate, toDate, search]
  );

  const more = useCallback(
    async () => {
      if (total > items.length && !loading) {
        const res = await fetchItems({ offset: items.length, limit: 50 });
        if (res && res.data) {
          setItems(items.concat(res.data.items));
        }
      }
    },
    [items, total]
  );

  const exportCSV = useCallback(
    async fileName => {
      const res = await fetchItems({ offset: null, limit: null });
      if (res && res.data) {
        const fieldNames = columns.map(item =>
          String(item.name).replaceAll(',', '/')
        );
        const fields = columns.map(item => {
          return {
            key: item.key,
            // label: item.name,
            value: (dataElement, field, data) => {
              const { key } = item;
              let value = dataElement[item.key];

              if (key === 'subjectId') {
                value = `${dataElement.subjectPrefix}${dataElement.subjectNo}/${
                  dataElement.subjectInitial
                }/${dataElement.age}/${dataElement.sex.substr(0, 1)}`;
              } else if (
                key === 'micspecdtc' ||
                key === 'strainreqdtc' ||
                key === 'straindelidtc' ||
                key === 'micstartdtc' ||
                key === 'micenddtc'
              ) {
                value = helpers.util.dateformat(value, format.YYYY_MM_DD);
              } else if (key === 'testmic' || key === 'typemic') {
                value = `${value} μg/ml`;
              }

              return !!value ? String(value).replaceAll(',', '/') : '';
            }
          };
        });

        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          fields,
          fieldNames,
          data: res.data.items
        });

        return res.data.items;
      }
      return [];
    },
    [projectSiteId, subjectId, tbdrugId, dateType, fromDate, toDate, search]
  );

  useEffect(
    () => {
      // if (projectSiteId > 0) {
      setItems([]);
      resetItems();
      // }
    },
    [projectSiteId, subjectId, tbdrugId, dateType, fromDate, toDate, search]
  );

  return { items, loading, total, more, exportCSV, columns };
};

export default useMicResultList;
