import React, { useCallback, useEffect, useState } from 'react';
import api from '../api';
import { useFormatMessage } from '../../../hooks';
import helpers from '../../../helpers';
import { format } from '../../../constants';

const useMicTransferedList = ({
  projectId,
  projectSiteId,
  subjectId,
  requestFromDate,
  requestToDate,
  transferedFromDate,
  transferedToDate,
  search
}) => {
  const [total, setTotal] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const t = useFormatMessage();

  const columns = [
    {
      key: 'site',
      name: t('site'),
      width: 190
    },
    {
      key: 'subjectId',
      name: t('cohort.dashboard.subjects'),
      formatter: ({ value, row }) => {
        const {
          subjectPrefix,
          subjectNo,
          subjectInitial,
          age,
          sex,
          activityId,
          projectActivityId
        } = row;
        return (
          <a
            style={{ cursor: 'pointer' }}
            onClick={() => {
              helpers.history.push(
                `/project/${projectId}/subject/${value}/activitygroup/5?projectActivityId=${projectActivityId}&activityId=${activityId}`
              );
            }}
          >
            {`${subjectPrefix}${subjectNo}/${subjectInitial}/${age}/${sex.substr(
              0,
              1
            )}`}
          </a>
        );
      },
      width: 170
    },
    {
      key: 'micspecdtc',
      name: t('activity.mictransfer.micspecdtc'),
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      },
      width: 100
    },
    {
      key: 'strainreqdtc',
      name: t('request.date'),
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      },
      width: 100
    },
    {
      key: 'straindelidtc',
      name: t('activity.mictransfer.straindelidtc'),
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      },
      width: 100
    },
    {
      key: 'contam',
      name: t('activity.mictransfer.contam'),
      width: 150,
      formatter: ({ value, row }) => {
        return value ? 'Yes' : 'No';
      }
    },
    {
      key: 'strainlabel',
      name: t('activity.mictransfer.strainlabel'),
      width: 150
    },
    {
      key: 'subculstor',
      name: t('activity.mictransfer.subculstor')
    }
  ];

  const fetchItems = useCallback(
    async ({ offset, limit }) => {
      setLoading(true);
      try {
        const res = await api.fetchSubjectsMicTransfered({
          projectSiteId,
          subjectId,
          requestFromDate,
          requestToDate,
          transferedFromDate,
          transferedToDate,
          search,
          limit,
          offset
        });
        if (res && res.data) {
          setTotal(res.data.paging.total);
        }
        return res;
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [
      projectSiteId,
      subjectId,
      requestFromDate,
      requestToDate,
      transferedFromDate,
      transferedToDate,
      search
    ]
  );

  const resetItems = useCallback(
    async () => {
      const res = await fetchItems({ offset: 0, limit: 50 });
      if (res && res.data) {
        setItems(res.data.items);
      }
    },
    [
      projectSiteId,
      subjectId,
      requestFromDate,
      requestToDate,
      transferedFromDate,
      transferedToDate,
      search
    ]
  );

  const more = useCallback(
    async () => {
      if (total > items.length && !loading) {
        const res = await fetchItems({ offset: items.length, limit: 50 });
        if (res && res.data) {
          setItems(items.concat(res.data.items));
        }
      }
    },
    [items, total]
  );

  const exportCSV = useCallback(
    async fileName => {
      const res = await fetchItems({ offset: null, limit: null });
      if (res && res.data) {
        const fieldNames = columns.map(item =>
          String(item.name).replaceAll(',', '/')
        );
        const fields = columns.map(item => {
          return {
            key: item.key,
            // label: item.name,
            value: (dataElement, field, data) => {
              const { key } = item;
              let value = dataElement[item.key];

              if (key === 'subjectId') {
                value = `${dataElement.subjectPrefix}${dataElement.subjectNo}/${
                  dataElement.subjectInitial
                }/${dataElement.age}/${dataElement.sex.substr(0, 1)}`;
              } else if (
                key === 'micspecdtc' ||
                key === 'strainreqdtc' ||
                key === 'straindelidtc'
              ) {
                value = helpers.util.dateformat(value, format.YYYY_MM_DD);
              } else if (key === 'contam') {
                value = value ? 'Yes' : 'No';
              }

              return !!value ? String(value).replaceAll(',', '/') : '';
            }
          };
        });

        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          fields,
          fieldNames,
          data: res.data.items
        });

        return res.data.items;
      }
      return [];
    },
    [
      projectSiteId,
      subjectId,
      requestFromDate,
      requestToDate,
      transferedFromDate,
      transferedToDate,
      search
    ]
  );

  useEffect(
    () => {
      // if (projectSiteId > 0) {
      setItems([]);
      resetItems();
      // }
    },
    [
      projectSiteId,
      subjectId,
      requestFromDate,
      requestToDate,
      transferedFromDate,
      transferedToDate,
      search
    ]
  );

  return { items, loading, total, more, exportCSV, columns };
};

export default useMicTransferedList;
