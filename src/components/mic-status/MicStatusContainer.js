import React, { useCallback, Fragment, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Tab, Grid } from 'semantic-ui-react';
import Title from './Title';
import MicListDashBoardContainer from './MicListDashBoardContainer';
import MicResultDashBoardContainer from './MicResultDashBoardContainer';
import { useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import { useMicTransferedList, useMicResultList } from './hooks';

const MicStatusContainer = () => {
  const t = useFormatMessage();
  const { projectId, index } = useParams();

  const [micListFilter, setMicListFilter] = useState({
    projectSiteId: 0,
    subjectId: 0,
    requestFromDate: null,
    requestToDate: null,
    transferedFromDate: null,
    transferedToDate: null,
    search: null
  });

  const [micResultFilter, setMicResultFilter] = useState({
    projectSiteId: 0,
    subjectId: 0,
    tbdrugId: 0,
    dateType: 0,
    fromDate: null,
    toDate: null,
    search: null
  });

  const micList = useMicTransferedList({
    ...micListFilter,
    projectId
  });
  const micResultList = useMicResultList({
    ...micResultFilter,
    projectId
  });

  const handleTabChange = useCallback((event, data) => {
    switch (data.activeIndex) {
      case 0:
        helpers.history.push(`/project/${projectId}/mic/status/0`);
        break;
      case 1:
        helpers.history.push(`/project/${projectId}/mic/status/1`);
        break;
      default:
        break;
    }
  }, []);

  const panes = [
    {
      menuItem: `${t('mic.list')} : ${micList.total}`,
      render: () => (
        <Tab.Pane basic active={index === '0'}>
          <MicListDashBoardContainer
            filter={micListFilter}
            projectId={projectId}
            items={micList.items}
            loading={micList.loading}
            total={micList.total}
            onRowVisibleEnd={micList.more}
            setFilter={setMicListFilter}
            exportCSV={micList.exportCSV}
            columns={micList.columns}
          />
        </Tab.Pane>
      )
    },
    {
      menuItem: `${t('mic.result')} : ${micResultList.total}`,
      render: () => (
        <Tab.Pane basic active={index === '1'}>
          <MicResultDashBoardContainer
            filter={micResultFilter}
            projectId={projectId}
            items={micResultList.items}
            loading={micResultList.loading}
            total={micResultList.total}
            onRowVisibleEnd={micResultList.more}
            setFilter={setMicResultFilter}
            exportCSV={micResultList.exportCSV}
            columns={micResultList.columns}
          />
        </Tab.Pane>
      )
    }
  ];

  return (
    <Fragment>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Tab
              panes={panes}
              onTabChange={handleTabChange}
              activeIndex={index}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Fragment>
  );
};

export default MicStatusContainer;
