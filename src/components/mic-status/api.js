import helpers from '../../helpers';

const fetchSubjectsMicTransfered = ({
  projectSiteId,
  subjectId,
  requestFromDate,
  requestToDate,
  transferedFromDate,
  transferedToDate,
  search,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    requestFromDate,
    requestToDate,
    transferedFromDate,
    transferedToDate,
    search,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/subjects/${subjectId}/mictransfer?${params}`
  );
};

const fetchSubjectsMicResult = ({
  projectSiteId,
  subjectId,
  tbdrugId,
  dateType,
  fromDate,
  toDate,
  search,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    tbdrugId,
    dateType,
    fromDate,
    toDate,
    search,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/subjects/${subjectId}/mic.result?${params}`
  );
};

export default {
  fetchSubjectsMicTransfered,
  fetchSubjectsMicResult
};
