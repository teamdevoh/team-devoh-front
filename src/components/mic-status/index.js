import { asyncComponent } from '../modules';

const MicStatusContainer = asyncComponent(() => import('./MicStatusContainer'));

export default MicStatusContainer;
