import React from 'react';
import MicResultDashBoard from './MicResultDashBoard';

const MicResultDashBoardContainer = ({
  projectId,
  items,
  loading,
  onRowVisibleEnd,
  setFilter,
  total,
  exportCSV,
  columns,
  filter
}) => {
  const handleChange = (event, { name, value }) => {
    const newFilter = {
      ...filter,
      [name]: value
    };
    if (name === 'dateType' && Number(value) === 0) {
      newFilter['fromDate'] = null;
      newFilter['toDate'] = null;
    }

    if (name === 'projectSiteId' && Number(value) === 0) {
      newFilter['subjectId'] = 0;
    }

    setFilter({ ...newFilter });
  };

  const handleDateRangeChange = (e, { name, value }) => {
    setFilter({
      ...filter,
      fromDate: !value[0] ? null : value[0],
      toDate: !value[1] ? null : value[1]
    });
  };

  return (
    <MicResultDashBoard
      {...filter}
      projectId={projectId}
      onChange={handleChange}
      onDateRangeChange={handleDateRangeChange}
      items={items}
      loading={loading}
      total={total}
      exportCSV={exportCSV}
      columns={columns}
      onRowVisibleEnd={onRowVisibleEnd}
    />
  );
};

export default MicResultDashBoardContainer;
