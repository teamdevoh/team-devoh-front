import React from 'react';
import { DataGrid } from '../data-grid';

const MicResultBoard = ({ items, loading, onRowVisibleEnd, columns }) => {
  return (
    <DataGrid
      columns={columns}
      rowNumber={{ show: true, key: 'no', name: 'No' }}
      rows={items}
      minHeight={550}
      rowKey="rowNum"
      loading={loading}
      onRowVisibleEnd={onRowVisibleEnd}
    />
  );
};

export default MicResultBoard;
