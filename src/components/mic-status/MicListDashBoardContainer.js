import React from 'react';
import MicListDashBoard from './MicListDashBoard';

const MicListDashBoardContainer = ({
  projectId,
  items,
  loading,
  onRowVisibleEnd,
  setFilter,
  total,
  exportCSV,
  columns,
  filter
}) => {
  const handleChange = (event, { name, value }) => {
    setFilter({ ...filter, [name]: value });
  };

  const handleDateRangeChange = which => (e, { name, value }) => {
    const newFilter = {
      ...filter,
      [`${which}FromDate`]: !value[0] ? null : value[0],
      [`${which}ToDate`]: !value[1] ? null : value[1]
    };

    if (name === 'projectSiteId' && Number(value) === 0) {
      newFilter['subjectId'] = 0;
    }

    setFilter({ ...newFilter });
  };

  return (
    <MicListDashBoard
      {...filter}
      projectId={projectId}
      onChange={handleChange}
      onDateRangeChange={handleDateRangeChange}
      items={items}
      loading={loading}
      total={total}
      exportCSV={exportCSV}
      columns={columns}
      onRowVisibleEnd={onRowVisibleEnd}
    />
  );
};

export default MicListDashBoardContainer;
