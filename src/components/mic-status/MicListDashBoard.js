import React, { Fragment } from 'react';
import MicListBoard from './MicListBoard';
import MicListDashBoardHeader from './MicListDashBoardHeader';
import MicListDashBoardTool from './MicListDashBoardTool';
import { Grid } from 'semantic-ui-react';

const MicListDashBoard = ({
  projectId,
  projectSiteId,
  subjectId,
  requestFromDate,
  requestToDate,
  transferedFromDate,
  transferedToDate,
  search,
  onChange,
  onDateRangeChange,
  onRowVisibleEnd,
  items,
  loading,
  total,
  exportCSV,
  columns,
  setFilter
}) => {
  return (
    <Fragment>
      <Grid.Row>
        <Grid.Column>
          <MicListDashBoardHeader
            key={`${projectSiteId}${subjectId}${requestFromDate}${requestToDate}${transferedFromDate}${transferedToDate}`}
            projectId={projectId}
            onChange={onChange}
            projectSiteId={projectSiteId}
            subjectId={subjectId}
            requestFromDate={requestFromDate}
            requestToDate={requestToDate}
            transferedFromDate={transferedFromDate}
            transferedToDate={transferedToDate}
            search={search}
            onDateRangeChange={onDateRangeChange}
            setFilter={setFilter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <MicListDashBoardTool
            total={total}
            exportCSV={exportCSV}
            fileName="MIC Transfered List"
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <MicListBoard
            items={items}
            loading={loading}
            onRowVisibleEnd={onRowVisibleEnd}
            columns={columns}
          />
        </Grid.Column>
      </Grid.Row>
    </Fragment>
  );
};

export default MicListDashBoard;
