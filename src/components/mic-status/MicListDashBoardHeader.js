import React, { Fragment, useState } from 'react';
import { Grid, Form, Input, Icon } from 'semantic-ui-react';
import { ProjectSite } from '../collection/Selector';
import { useFormatMessage } from '../../hooks';
import DateRangePicker from '../datepicker/DateRangePicker';
import { Subjects } from '../collection/Selector';

const MicListDashBoardHeader = props => {
  const {
    projectId,
    onChange,
    projectSiteId,
    subjectId,
    requestFromDate,
    requestToDate,
    transferedFromDate,
    transferedToDate,
    //search,
    onDateRangeChange
  } = props;
  const t = useFormatMessage();
  const [search, setSearch] = useState(props.search || '');

  const handleSearchKeyUp = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      hdnaleSearchClick();
    }
  };

  const handleSearchChange = (e, { value }) => {
    setSearch(value);
  };

  const hdnaleSearchClick = () => {
    const value = search.trim();
    setSearch(value);

    onChange(null, { name: 'search', value });
  };

  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <Form>
            <Form.Group widths="equal">
              <Form.Field>
                <label>{t('site')}</label>
                <ProjectSite
                  name="projectSiteId"
                  value={projectSiteId}
                  onChange={onChange}
                  projectId={projectId}
                />
              </Form.Field>
              <Form.Field>
                <label>{t('cohort.dashboard.subjects')}</label>
                <Subjects
                  name="subjectId"
                  value={subjectId}
                  projectSiteId={projectSiteId}
                  onChange={onChange}
                  reset={projectSiteId === 0}
                  hasAll
                />
              </Form.Field>
              <Form.Field>
                <label>{t('request.date')}</label>
                <DateRangePicker
                  name=""
                  value={[requestFromDate, requestToDate]}
                  onChange={onDateRangeChange('request')}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label>{t('activity.mictransfer.straindelidtc')}</label>
                <DateRangePicker
                  name=""
                  value={[transferedFromDate, transferedToDate]}
                  onChange={onDateRangeChange('transfered')}
                />
              </Form.Field>
              <Form.Field>
                <label>{t('project.search')}</label>
                <Input
                  placeholder="Labeling, Location..."
                  icon={
                    <Icon
                      name="search"
                      inverted
                      circular
                      link
                      onClick={hdnaleSearchClick}
                    />
                  }
                  onChange={handleSearchChange}
                  onKeyUp={handleSearchKeyUp}
                  name="search"
                  value={search}
                />
              </Form.Field>
            </Form.Group>
          </Form>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default MicListDashBoardHeader;
