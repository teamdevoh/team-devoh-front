import React, { Fragment } from 'react';
import MicResultBoard from './MicResultBoard';
import MicResultDashBoardHeader from './MicResultDashBoardHeader';
import MicListDashBoardTool from './MicListDashBoardTool';
import { Grid } from 'semantic-ui-react';

const MicResultDashBoard = ({
  projectId,
  projectSiteId,
  subjectId,
  tbdrugId,
  dateType,
  fromDate,
  toDate,
  search,
  onChange,
  onDateRangeChange,
  onRowVisibleEnd,
  items,
  loading,
  total,
  exportCSV,
  columns,
  setFilter
}) => {
  return (
    <Fragment>
      <Grid.Row>
        <Grid.Column>
          <MicResultDashBoardHeader
            key={`${projectSiteId}${subjectId}${tbdrugId}${dateType}${fromDate}${toDate}${search}`}
            projectId={projectId}
            onChange={onChange}
            projectSiteId={projectSiteId}
            subjectId={subjectId}
            tbdrugId={tbdrugId}
            dateType={dateType}
            fromDate={fromDate}
            toDate={toDate}
            search={search}
            onDateRangeChange={onDateRangeChange}
            setFilter={setFilter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <MicListDashBoardTool
            total={total}
            exportCSV={exportCSV}
            fileName="MIC Result List"
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <MicResultBoard
            items={items}
            loading={loading}
            onRowVisibleEnd={onRowVisibleEnd}
            columns={columns}
          />
        </Grid.Column>
      </Grid.Row>
    </Fragment>
  );
};

export default MicResultDashBoard;
