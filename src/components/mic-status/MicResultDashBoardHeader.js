import React, { Fragment, useState } from 'react';
import { Grid, Form, Input, Icon, Dropdown } from 'semantic-ui-react';
import { ProjectSite } from '../collection/Selector';
import { useFormatMessage } from '../../hooks';
import DateRangePicker from '../datepicker/DateRangePicker';
import { Subjects } from '../collection/Selector';
import { TbdrugList } from '../collection-tbdrug/Selector';
import styled from 'styled-components';

const FlecDiv = styled.div`
  display: flex;
`;

const DateRangeWrap = styled.div`
  margin-left: 10px;
  display: flex;
  flex: 1;
`;

const MicResultDashBoardHeader = props => {
  const {
    projectId,
    onChange,
    projectSiteId,
    subjectId,
    tbdrugId,
    dateType,
    fromDate,
    toDate,
    //search,
    onDateRangeChange
  } = props;
  const t = useFormatMessage();
  const [search, setSearch] = useState(props.search || '');

  const handleSearchKeyUp = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      hdnaleSearchClick();
    }
  };

  const handleSearchChange = (e, { value }) => {
    setSearch(value);
  };

  const hdnaleSearchClick = () => {
    const value = search.trim();
    setSearch(value);
    onChange(null, { name: 'search', value });
  };

  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <Form>
            <Form.Group widths="equal">
              <Form.Field>
                <label>{t('site')}</label>
                <ProjectSite
                  name="projectSiteId"
                  value={projectSiteId}
                  onChange={onChange}
                  projectId={projectId}
                />
              </Form.Field>
              <Form.Field>
                <label>{t('cohort.dashboard.subjects')}</label>
                <Subjects
                  name="subjectId"
                  value={subjectId}
                  projectSiteId={projectSiteId}
                  onChange={onChange}
                  reset={projectSiteId === 0}
                  hasAll
                />
              </Form.Field>
              <Form.Field>
                <label>TB Drug</label>
                <TbdrugList
                  defaultOption={[
                    {
                      text: t('all'),
                      value: 0
                    }
                  ]}
                  name="tbdrugId"
                  value={tbdrugId}
                  onChange={onChange}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label>{t('date.filter')}</label>
                <FlecDiv>
                  <Dropdown
                    name="dateType"
                    value={dateType}
                    selection
                    onChange={onChange}
                    options={[
                      {
                        text: t('common.notSelected'),
                        value: 0
                      },
                      {
                        text: t('request.date'),
                        value: 1
                      },
                      {
                        text: t('transfered.date'),
                        value: 2
                      },
                      {
                        text: t('set.up.date'),
                        value: 3
                      },
                      {
                        text: t('reading.date'),
                        value: 4
                      }
                    ]}
                  />
                  <DateRangeWrap>
                    <DateRangePicker
                      name=""
                      value={[fromDate, toDate]}
                      onChange={onDateRangeChange}
                      disabled={dateType === 0}
                    />
                  </DateRangeWrap>
                </FlecDiv>
              </Form.Field>
              <Form.Field>
                <label>{t('project.search')}</label>
                <Input
                  placeholder="Labeling, Location..."
                  icon={
                    <Icon
                      name="search"
                      inverted
                      circular
                      link
                      onClick={hdnaleSearchClick}
                    />
                  }
                  onChange={handleSearchChange}
                  onKeyUp={handleSearchKeyUp}
                  name="search"
                  value={search}
                />
              </Form.Field>
            </Form.Group>
          </Form>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default MicResultDashBoardHeader;
