import React from 'react';
import { DataGrid } from '../data-grid';

const MicListBoard = ({ items, loading, onRowVisibleEnd, columns }) => {
  return (
    <DataGrid
      columns={columns}
      rowNumber={{ show: true, key: 'no', name: 'No' }}
      rows={items}
      minHeight={550}
      rowKey="subjectMictransferId"
      loading={loading}
      onRowVisibleEnd={onRowVisibleEnd}
    />
  );
};

export default MicListBoard;
