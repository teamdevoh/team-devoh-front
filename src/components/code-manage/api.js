import helpers from '../../helpers';

const fetchCodes = codeGroupId => {
  return helpers.Service.get(`api/codegroups/${codeGroupId}/codes`);
};

const fetchCode = ({ codeGroupId, codeId }) => {
  return helpers.Service.get(`api/codegroups/${codeGroupId}/codes/${codeId}`);
};

export default {
  fetchCodes,
  fetchCode
};
