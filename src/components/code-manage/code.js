const Open = 1;
const Ongoing = 2;
const Completed = 3;

export default {
  Open,
  Ongoing,
  Completed
};
