import React, { useMemo } from 'react';
import { Container, Form, Divider } from 'semantic-ui-react';
import { StyledForm } from './styles';
import { useSampleTrackingList } from './hooks';
import { DataGrid } from '../data-grid';

const SampleTrackingContent = ({ info }) => {
  const {
    title,
    site,
    subjectNo,
    visit,
    sampling,
    sampleBarocode,
    volume,
    projectId,
    sampleAliquotId
  } = info;
  const [{ items, loading }] = useSampleTrackingList({
    projectId,
    sampleAliquotId
  });
  const used = useMemo(
    () => {
      return items.reduce((accumulator, currentValue) => {
        return accumulator + Number(currentValue.usedAliquotVolume);
      }, 0);
    },
    [items]
  );

  const columns = [
    {
      key: 'aliquotBarcode',
      name: 'Aliquot ID'
    },
    {
      key: 'status',
      name: 'Status'
    },
    {
      key: 'boxBarcode',
      name: 'Box'
    },
    { key: 'dateTime', name: 'Datetime' },
    {
      key: 'memberName',
      name: 'User'
    },
    {
      key: 'usedAliquotVolume',
      name: 'Used Volume'
    }
  ];

  return (
    <Container>
      <StyledForm>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Project</label>
            {title}
          </Form.Field>
          <Form.Field>
            <label>Site</label>
            {site}
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Subject</label>
            {subjectNo}
          </Form.Field>
          <Form.Field>
            <label>Visit</label>
            {visit}
          </Form.Field>
          <Form.Field>
            <label>Sampling</label>
            {sampling}
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>AliquotId</label>
            {sampleBarocode}
          </Form.Field>
          <Form.Field>
            <label>Volume</label>
            {volume}
          </Form.Field>
          <Form.Field>
            <label>Used</label>
            {used}
            ml
          </Form.Field>
        </Form.Group>
      </StyledForm>
      <Divider />
      <DataGrid
        columns={columns}
        rows={items}
        minHeight={350}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        loading={loading}
        rowKey="rowIndex"
      />
    </Container>
  );
};

export default SampleTrackingContent;
