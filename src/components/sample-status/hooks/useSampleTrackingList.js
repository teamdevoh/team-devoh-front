import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useSampleTrackingList = ({ projectId, sampleAliquotId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    try {
      const res = await api.fetchSampleTrackingList({
        projectId,
        sampleAliquotId
      });

      if (res && res.data) {
        setItems(res.data);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, sampleAliquotId]
  );

  return [{ items, loading }];
};

export default useSampleTrackingList;
