import { useState, useEffect } from 'react';
import api from '../api';

const useProjectsOfSamples = () => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItem = async () => {
    setLoading(true);
    const res = await api.fetchProjectsOfSamples();
    setLoading(false);
    if (res && res.data) {
      const newItem = res.data.map(item => {
        const { title, projectId } = item;
        return {
          text: title,
          value: projectId
        };
      });
      setItems(newItem);
    }
  };

  useEffect(() => {
    fetchItem();
  }, []);

  return [{ items, loading }];
};

export default useProjectsOfSamples;
