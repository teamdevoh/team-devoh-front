import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useSampleCollectionList = ({
  curProjectId,
  projectId,
  projectSiteId,
  subjectIds,
  sampleStatusCodeId,
  freezerId,
  barcode,
  preserveEndDtc
}) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);

    const params = {
      curProjectId,
      projectId,
      projectSiteId,
      subjectIds: subjectIds && subjectIds.join(','),
      sampleStatusCodeId,
      freezerId,
      barcode
    };

    if (!!preserveEndDtc) {
      const [yyyy, mm] = preserveEndDtc.split('-');

      const endDate = new Date(yyyy, mm, 0).getDate();

      params[
        'odataQuery'
      ] = `&$filter=preserve ne '' and preserve ne null and preserve le '${preserveEndDtc}-${endDate}'`;
    }

    const res = await api.fetchSampleCollection(params);

    if (res && res.data) {
      setItems(res.data);
    }
    setLoading(false);
  });

  useEffect(
    () => {
      fetchItems();
    },
    [
      curProjectId,
      projectId,
      projectSiteId,
      subjectIds,
      sampleStatusCodeId,
      freezerId,
      barcode,
      preserveEndDtc
    ]
  );

  return [{ items, loading }, setItems];
};

export default useSampleCollectionList;
