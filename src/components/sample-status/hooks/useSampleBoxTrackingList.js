import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useSampleBoxTrackingList = ({ projectId, sampleBoxId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    try {
      const res = await api.fetchBoxTrackingList({
        projectId,
        sampleBoxId
      });

      if (res && res.data) {
        setItems(res.data);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, sampleBoxId]
  );

  return [{ items, loading }];
};

export default useSampleBoxTrackingList;
