import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { asyncComponent } from '../../components/modules';
import { Authorized } from '../../components/auth';
import { role } from '../../constants';

const SampleCollectionDataDashBoard = asyncComponent(() =>
  import('./DashBoard')
);

const SampleCollectionRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path="/project/:projectId/sample/status"
        render={props => {
          const Component = Authorized(
            SampleCollectionDataDashBoard,
            role.Sample_Read
          );
          return <Component {...props} />;
        }}
      />
    </Switch>
  );
};

export default SampleCollectionRoutes;
