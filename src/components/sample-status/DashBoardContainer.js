import React from 'react';
import SampleCollectionRoutes from './SampleCollectionRoutes';

const DashBoardContainer = () => {
  return <SampleCollectionRoutes />;
};

export default DashBoardContainer;
