import helpers from '../../helpers';

const fetchSampleCollection = ({
  curProjectId,
  projectId,
  projectSiteId,
  subjectIds,
  sampleStatusCodeId,
  freezerId,
  barcode,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    subjectIds,
    sampleStatusCodeId,
    freezerId,
    barcode
  });

  return helpers.Service.get(
    `api/projects/${curProjectId}/samples.status?${params}${odataQuery}`
  );
};

const fetchProjectsOfSamples = () => {
  return helpers.Service.get(`api/projects/samples`);
};

const fetchSampleTrackingList = ({ projectId, sampleAliquotId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/samples.status/${sampleAliquotId}`
  );
};

const updateSampleStatusEmpty = ({ projectId, sampleAliquotId, isEmpty }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/samples.status/${sampleAliquotId}/empty`,
    { isEmpty }
  );
};

const updateSampleStatusRemark = ({ projectId, sampleAliquotId, remark }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/samples.status/${sampleAliquotId}/remark`,
    { remark }
  );
};

const fetchBoxTrackingList = ({ projectId, sampleBoxId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/samples.box/${sampleBoxId}`
  );
};

export default {
  fetchSampleCollection,
  fetchProjectsOfSamples,
  fetchSampleTrackingList,
  updateSampleStatusEmpty,
  updateSampleStatusRemark,
  fetchBoxTrackingList
};
