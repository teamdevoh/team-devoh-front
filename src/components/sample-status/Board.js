import React, { Fragment, useState, useRef, useEffect } from 'react';
import { useBoolean, useFormatMessage } from '../../hooks';
import { Modal } from '../modal';
import SampleTrackingContent from './SampleTrackingContent';
import BoxTrackingContent from './BoxTrackingContent';
import helpers from '../../helpers';
import { format } from '../../constants';
import styled from 'styled-components';
import { Checkbox, Loader, Button, TextArea } from 'semantic-ui-react';
import { Table, WindowScroller, Column, AutoSizer } from 'react-virtualized';
import 'react-virtualized/styles.css';
import Draggable from 'react-draggable';
import api from './api';
import { EmptyRowsView } from '../modules';
import { RootWrap } from '../modules/EmptyRowsViewForSemantic';
import { SortableHeader } from '../case-review/CROtherTest';
import util from '../../helpers/util';

const Root = styled.div`
  height: calc(100vh - 404px);
  overflow: auto;
  width: 100%;
  position: relative;
  outline: 1px solid #e7eaec;

  & {
    .ReactVirtualized__Table__headerColumn,
    .ReactVirtualized__Table__rowColumn {
      margin: 0px;
    }

    .ReactVirtualized__Table__headerRow {
      position: sticky;
      background-color: whitesmoke;
      z-index: 5;
      top: 0;
      text-transform: none;

      .ReactVirtualized__Table__headerColumn {
        position: relative;
        display: flex;
        align-items: center;
        background: #f9f9f9;
        padding: 8px;
        font-weight: bold;
        border-right: 1px solid #dddddd;
        border-bottom: 1px solid #dddddd;

        .DragHandle {
          position: absolute;
          top: 0px;
          right: 0px;
          width: 6px;
          height: 100%;
          cursor: col-resize;
        }
      }
    }

    .ReactVirtualized__Table__rowColumn {
      height: 100%;
      border-right: 1px solid #eee;
      border-bottom: 1px solid #dddddd;
    }
  }
`;

const SampleGridRowWrapper = styled.div`
  height: 100%;
  padding-left: 8px;
  padding-right: 8px;
  display: flex;

  ${props => {
    const { color } = props;

    if (!!color) {
      return `
        &

        {
          background-color: ${color};
        }
      `;
    }

    return '';
  }};
`;

const ContentWrap = styled.div`
  display: inline-block;
  max-width: 100%;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;

  ${props => {
    const { align } = props;

    if (align === 'center') {
      return 'margin: auto';
    }

    return 'margin: auto 0;';
  }};
`;

const preserveColor = {
  last: '#db2828',
  thisMonth: '#e03997'
};

const nowYM = `${helpers.util
  .dateformat(new Date(), format.YYYY_MM_DD)
  .substr(0, 7)}`;

const IsEmpty = ({ cellData, projectId, rowData, rowIndex, onItemChange }) => {
  const [checked, setChecked] = useState(cellData);
  const [loading, setLoading] = useState(false);

  const handleChange = async (e, { checked }) => {
    setLoading(true);
    try {
      const res = await api.updateSampleStatusEmpty({
        projectId,
        sampleAliquotId: rowData.sampleAliquotId,
        isEmpty: checked
      });

      if (res && res.data) {
        onItemChange({
          index: rowIndex,
          newItem: {
            ...rowData,
            isEmpty: checked
          }
        });

        setChecked(checked);
      }
    } catch (err) {}

    setLoading(false);
  };

  return (
    <Fragment>
      {loading ? (
        <Loader active inline size="mini" />
      ) : (
        <Checkbox checked={checked} onChange={handleChange} />
      )}
    </Fragment>
  );
};

const SampleTrackingButton = ({ rowData: data }) => {
  const showSampleTrackingModal = useBoolean(false);

  const handleClick = () => {
    showSampleTrackingModal.setTrue();
  };

  return (
    <Fragment>
      <a style={{ cursor: 'pointer' }} onClick={handleClick}>
        {data.sampleBarocode}
      </a>
      <Modal
        size="large"
        open={showSampleTrackingModal.value}
        onOK={showSampleTrackingModal.setFalse}
        title={'Sample Tracking'}
        content={<SampleTrackingContent info={data} />}
      />
    </Fragment>
  );
};

const TrackingModalButton = ({ rowData: data }) => {
  const showBoxTrackingModal = useBoolean(false);

  const handleClick = () => {
    showBoxTrackingModal.setTrue();
  };

  return (
    <Fragment>
      <a style={{ cursor: 'pointer' }} onClick={handleClick}>
        {data.boxBarcode}
      </a>
      <Modal
        size="large"
        open={showBoxTrackingModal.value}
        onOK={showBoxTrackingModal.setFalse}
        title={'Box Tracking'}
        content={<BoxTrackingContent info={data} />}
      />
    </Fragment>
  );
};

const Remark = ({ rowData: data, projectId, rowIndex, onItemChange }) => {
  const t = useFormatMessage();
  const showModal = useBoolean(false);
  const [value, setValue] = useState(data.remark);
  const [loading, setLoading] = useState(false);
  const [remark, setRemark] = useState(data.remark || value);

  const handleClick = () => {
    showModal.setTrue();
  };

  const handleChange = (e, { value }) => {
    setValue(value);
  };

  const handleSaveClick = async () => {
    setLoading(true);
    try {
      const res = await api.updateSampleStatusRemark({
        projectId,
        sampleAliquotId: data.sampleAliquotId,
        remark: value
      });

      if (res && res.data) {
        setRemark(value);
        onItemChange({
          index: rowIndex,
          newItem: {
            ...data,
            remark: value
          }
        });

        showModal.setFalse();
      }
    } catch (err) {}

    setLoading(false);
  };

  const handleCancel = () => {
    showModal.setFalse();
    setValue(remark);
  };

  return (
    <Fragment>
      <a style={{ cursor: 'pointer' }} onClick={handleClick} title={remark}>
        {remark || <Button icon="plus" size="mini" circular />}
      </a>
      <Modal
        size="large"
        open={showModal.value}
        onOK={handleCancel}
        title={`${data.sampleBarocode} Remark`}
        content={
          <Fragment>
            <TextArea
              style={{
                minHeight: 200,
                width: '100%'
              }}
              value={value || ''}
              onChange={handleChange}
            />
            <Button loading={loading} onClick={handleCancel}>
              {t('common.cancel')}
            </Button>
            <Button primary loading={loading} onClick={handleSaveClick}>
              {!!remark ? t('common.modify') : t('common.save')}
            </Button>
          </Fragment>
        }
      />
    </Fragment>
  );
};

const CellRenderer = ({ children, rowIndex, align = 'left', ...props }) => {
  const { preserve } = props.rowData;
  let color = '';

  if (preserve !== null && preserve !== '' && preserve !== 'Permanent') {
    const yyyymm = preserve.substr(0, 7);

    if (nowYM === yyyymm) {
      color = preserveColor.thisMonth;
    } else if (yyyymm < nowYM) {
      color = preserveColor.last;
    }
  }

  return (
    <SampleGridRowWrapper
      color={color}
      title={typeof children === 'object' ? null : children}
    >
      <ContentWrap align={align}>{children}</ContentWrap>
    </SampleGridRowWrapper>
  );
};

const ResizableHeader = ({ columnIndex, columnsLeng, resizeRow }) => ({
  dataKey,
  label,
  sortBy,
  sortDirection,
  disableSort
}) => {
  return (
    <Fragment key={dataKey}>
      <div className="ReactVirtualized__Table__headerTruncatedText">
        {disableSort ? (
          label
        ) : (
          <SortableHeader
            as="div"
            //onClick={handleSort('subjectVisitId')}
            orderByKey={dataKey}
            order={sortDirection}
            orderBy={sortBy}
          >
            {label}
          </SortableHeader>
        )}
      </div>
      {columnIndex < columnsLeng - 1 && (
        <Draggable
          axis="x"
          defaultClassName="DragHandle"
          defaultClassNameDragging="DragHandleActive"
          onDrag={(event, { deltaX }) =>
            resizeRow({
              deltaX: deltaX,
              columnIndex
            })
          }
          position={{ x: 0 }}
          zIndex={999}
        >
          <span className="DragHandleIcon">⋮</span>
        </Draggable>
      )}
    </Fragment>
  );
};

const Board = ({ items, loading, projectId, setItems }) => {
  const containerRef = useRef();
  const [sort, setSort] = useState({
    sortBy: null,
    sortDirection: null
  });
  const [columns, setColumns] = useState([
    {
      label: 'No',
      dataKey: 'no',
      width: 50,
      cellRenderer: props => props.rowIndex + 1
    },
    {
      label: 'Project',
      dataKey: 'title',
      width: 200
    },
    { label: 'Site', dataKey: 'site', width: 160 },
    { label: 'Subject', dataKey: 'subjectNo', width: 150 },
    { label: 'Visit', dataKey: 'visit', width: 100 },
    { label: 'Sampling', dataKey: 'sampling', width: 140 },
    { label: 'Analyte', dataKey: 'analyte', width: 80 },
    {
      label: 'Aliquot ID',
      dataKey: 'sampleBarocode',
      width: 100,
      cellRenderer: props => <SampleTrackingButton {...props} />
    },
    {
      label: 'Origin ID',
      dataKey: 'originalBarcode',
      width: 103
    },
    { label: 'Volume', dataKey: 'volume', width: 90 },
    {
      label: 'Empty',
      dataKey: 'isEmpty',
      width: 60,
      align: 'center',
      cellRenderer: props => <IsEmpty {...props} projectId={projectId} />
    },
    {
      label: 'Remark',
      dataKey: 'remark',
      width: 100,
      cellRenderer: props => <Remark {...props} />
    },
    { label: 'Preserve', dataKey: 'preserve', width: 115 },
    { label: 'Status', dataKey: 'sampleStatus', width: 100 },
    { label: 'DateTime', dataKey: 'dateTime', width: 140 },
    {
      label: 'User',
      dataKey: 'sampleAliquotStatusMember',
      width: 130
    },
    {
      label: 'Box ID',
      dataKey: 'boxBarcode',
      width: 100,
      cellRenderer: props => <TrackingModalButton {...props} />
    },
    {
      label: 'Boxing',
      dataKey: 'boxingDateTime',
      width: 140,
      cellRenderer: ({ cellData }) => {
        return helpers.util.dateformat(cellData, format.YYYY_MM_DD_HHMM);
      }
    },
    { label: 'Freezer', dataKey: 'freezer', width: 100 },
    { label: 'Location', dataKey: 'location', width: 100 },
    {
      label: 'Boxing User',
      dataKey: 'boxingMember',
      width: 130
    }
  ]);

  const resizeRow = ({ deltaX, columnIndex }) => {
    const newColumns = [...columns];

    newColumns[columnIndex].width = newColumns[columnIndex].width + deltaX;

    newColumns[columnIndex + 1].width =
      newColumns[columnIndex + 1].width - deltaX;

    setColumns(newColumns);
  };

  useEffect(
    () => {
      if (items.length > 0) {
        const newItems = items.sort((a, b) => {
          let x = a[sort.sortBy];
          let y = b[sort.sortBy];
          if (sort.sortDirection.toLowerCase() !== 'asc') {
            y = a[sort.sortBy];
            x = b[sort.sortBy];
          }

          return util.compareString(x, y);
        });
        setItems([...newItems]);
      }
    },
    [sort]
  );

  const generateColumns = () => {
    let totalWidth = 0;

    const handleItemChange = ({ index, newItem }) => {
      const newItems = [...items];

      newItems[index] = {
        ...newItem
      };
      setItems([...newItems]);
    };

    const columnList = columns.map((column, columnIndex) => {
      const { label, dataKey, width, cellRenderer, align = 'left' } = column;
      totalWidth += width;

      return (
        <Column
          key={columnIndex}
          disableSort={dataKey === 'no' ? true : false}
          label={label}
          dataKey={dataKey}
          width={width}
          minWidth={width}
          cellRenderer={props => {
            props['onItemChange'] = handleItemChange;
            return (
              <CellRenderer
                key={props.rowData.sampleAliquotId}
                {...props}
                align={align}
              >
                {cellRenderer ? cellRenderer(props) : props.cellData}
              </CellRenderer>
            );
          }}
          headerRenderer={ResizableHeader({
            columnIndex,
            columnsLeng: columns.length,
            resizeRow
          })}
        />
      );
    });

    return {
      columnList,
      totalWidth
    };
  };

  return (
    <Root ref={containerRef}>
      <WindowScroller scrollElement={containerRef.current}>
        {({ height, isScrolling, onChildScroll, scrollTop }) => {
          return (
            <AutoSizer disableHeight>
              {({ width }) => {
                const { columnList, totalWidth } = generateColumns();

                return (
                  <Table
                    autoHeight
                    width={loading || items.length === 0 ? width : totalWidth}
                    height={height}
                    isScrolling={isScrolling}
                    onScroll={onChildScroll}
                    scrollTop={scrollTop}
                    rowHeight={35}
                    headerHeight={35}
                    rowCount={items.length}
                    rowGetter={({ index }) => items[index]}
                    // row 선택하면 border 보이기 위해
                    onRowClick={() => {}}
                    noRowsRenderer={() => (
                      <RootWrap>
                        <EmptyRowsView loading={loading} />
                      </RootWrap>
                    )}
                    sort={({ sortBy, sortDirection }) => {
                      if (!loading) {
                        setSort({
                          ...sort,
                          sortBy,
                          sortDirection
                        });
                      }
                    }}
                    sortBy={sort.sortBy}
                    sortDirection={sort.sortDirection}
                  >
                    {columnList}
                  </Table>
                );
              }}
            </AutoSizer>
          );
        }}
      </WindowScroller>
    </Root>
  );
};

export default Board;
