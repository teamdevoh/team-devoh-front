import React, { Fragment, useRef, useState } from 'react';
import { Form, Grid, Input, Icon, Label, Popup } from 'semantic-ui-react';
import {
  ProjectsOfSamples,
  ProjectSite,
  SampleAliquotStatus,
  FreezerMachine
} from './Selector';
import { Subjects } from '../collection/Selector';
import { StyledForm } from './styles';
import DateInput from '../input/DateInput';
import { format } from '../../constants';
import styled from 'styled-components';
import { useFormatMessage } from '../../hooks';

const LabelButton = styled(Label)`
  cursor: pointer;
`;

const StyledIcon = styled(Icon)`
  &&& {
    margin: 0;
  }
`;

const DashBoardHeader = ({ projectId, filter, onFilterChange }) => {
  const t = useFormatMessage();
  const search = useRef('');
  const [searchPreserve, setSearchPreserve] = useState(filter.preserveEndDtc);
  const [error, setError] = useState(false);

  const setSearch = value => {
    search.current = value;
  };

  const getSearch = () => {
    return search.current;
  };

  const handleSearchKeyUp = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      hdnaleSearchClick();
    }
  };

  const handlePreserveSearchKeyUp = event => {
    setError(false);
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      hdnalePreserveSearchClick();
    }
  };

  const handleSearchChange = (e, { value }) => {
    setSearch(value);
  };

  const handlePreserveSearchChange = (e, { value }) => {
    setSearchPreserve(value);
  };

  const hdnaleSearchClick = () => {
    const value = getSearch().trim();

    onFilterChange(null, { name: 'barcode', value });
  };

  const hdnalePreserveSearchClick = () => {
    if (searchPreserve !== '') {
      const mm = Number(searchPreserve.split('-')[1]);

      if (isNaN(mm) || mm < 1 || mm > 12) {
        setError(true);
        return;
      }
    }

    onFilterChange(null, { name: 'preserveEndDtc', value: searchPreserve });
  };

  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <StyledForm>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Project</label>
                <ProjectsOfSamples
                  name="projectId"
                  value={filter.projectId}
                  onChange={onFilterChange}
                  hasAll
                />
              </Form.Field>
              <Form.Field>
                <label>Site</label>
                <ProjectSite
                  projectId={filter.projectId}
                  name="projectSiteId"
                  value={filter.projectSiteId}
                  onChange={onFilterChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Subject</label>
                <Subjects
                  projectSiteId={filter.projectSiteId}
                  name="subjectIds"
                  value={!!!filter.subjectIds ? [] : filter.subjectIds}
                  onChange={onFilterChange}
                  multiple
                  reset={filter.projectSiteId === 0}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Sample Status</label>
                <SampleAliquotStatus
                  name="sampleStatusCodeId"
                  value={filter.sampleStatusCodeId}
                  onChange={onFilterChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Freezer</label>
                <FreezerMachine
                  name="freezerId"
                  value={filter.freezerId}
                  onChange={onFilterChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Barcode</label>
                <Input
                  placeholder="Sample, Box Barcode"
                  labelPosition="right"
                  onChange={handleSearchChange}
                  onKeyUp={handleSearchKeyUp}
                  name="barcode"
                  fluid
                >
                  <input />
                  <LabelButton onClick={hdnaleSearchClick}>
                    <StyledIcon name="search" />
                  </LabelButton>
                </Input>
              </Form.Field>
              <Form.Field>
                <label>Preserve</label>
                <Popup
                  trigger={
                    <DateInput
                      name="preserveEndDtc"
                      placeholder="yyyy-mm"
                      value={searchPreserve}
                      options={{ dateFormat: format.YYYY_MM, separate: '-' }}
                      onChange={handlePreserveSearchChange}
                      onKeyUp={handlePreserveSearchKeyUp}
                      rightLabel={
                        <LabelButton onClick={hdnalePreserveSearchClick}>
                          <StyledIcon name="search" />
                        </LabelButton>
                      }
                    />
                  }
                  content={t('check.the.date')}
                  position="bottom center"
                  style={{
                    color: '#9f3a38'
                  }}
                  open={error}
                />
              </Form.Field>
            </Form.Group>
          </StyledForm>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
