import React from 'react';
import { Label } from 'semantic-ui-react';
import { CSVExport } from '../export';
import styled from 'styled-components';

const FlexDiv = styled.div`
  display: flex;
`;

const StyledCSVExport = styled(CSVExport)`
  &&& {
    margin-left: auto;
    margin-right: 0;
  }
`;

const StyledDetail = styled(Label.Detail)`
  &&& {
    color: #db2828;
  }
`;

export const fields = [
  {
    key: 'title',
    name: 'Project'
  },
  {
    key: 'site',
    name: 'Site'
  },
  { key: 'subjectNo', name: 'Subject' },
  {
    key: 'visit',
    name: 'Visit'
  },
  {
    key: 'sampling',
    name: 'Sampling'
  },
  {
    key: 'analyte',
    name: 'Analyte'
  },
  {
    key: 'sampleBarocode',
    name: 'Aliquot ID'
  },
  {
    key: 'originalBarcode',
    name: 'Origin ID'
  },
  {
    key: 'volume',
    name: 'Volume'
  },
  {
    key: 'isEmpty',
    name: 'Empty'
  },
  {
    key: 'remark',
    name: 'Remark'
  },
  {
    key: 'preserve',
    name: 'Preserve'
  },
  {
    key: 'sampleStatus',
    name: 'Status'
  },
  {
    key: 'dateTime',
    name: 'DateTime'
  },
  {
    key: 'sampleAliquotStatusMember',
    name: 'User'
  },
  {
    key: 'boxBarcode',
    name: 'Box ID'
  },
  {
    key: 'boxingDateTime',
    name: 'Boxing'
  },
  {
    key: 'freezer',
    name: 'Freezer'
  },
  {
    key: 'location',
    name: 'Location'
  },
  {
    key: 'boxingMember',
    name: 'Boxing User'
  }
];

export const fieldNames = [
  'Project',
  'Site',
  'Subject',
  'Visit',
  'Sampling',
  'Analyte',
  'Aliquot ID',
  'Origin ID',
  'Volume',
  'Empty',
  'Remark',
  'Preserve',
  'Status',
  'DateTime',
  'User',
  'Box ID',
  'Boxing',
  'Freezer',
  'Location',
  'Boxing User'
];
const DashBoardTool = ({ items }) => {
  return (
    <FlexDiv>
      <Label size="big">
        Sample Count
        <StyledDetail>{items.length}</StyledDetail>
      </Label>
      <StyledCSVExport
        data={items}
        fileName="All_Sample_List.csv"
        fields={fields}
        fieldNames={fieldNames}
      />
    </FlexDiv>
  );
};

export default DashBoardTool;
