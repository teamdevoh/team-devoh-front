import React, { useState, useEffect } from 'react';
import { Container, Form, Divider } from 'semantic-ui-react';
import { StyledForm } from './styles';
import { useSampleBoxTrackingList } from './hooks';
import { useCodes } from '../../hooks';
import { useBuild } from '../activity/hooks';
import { DataGrid } from '../data-grid';
import { codeGroup } from '../../constants';

const BoxTrackingContent = ({ info }) => {
  const {
    title,
    site,
    visit,
    projectId,
    analyte,
    boxBarcode,
    sampleBoxId
  } = info;

  const [{ items, loading }] = useSampleBoxTrackingList({
    projectId,
    sampleBoxId
  });
  const [topInfo, setTopInfo] = useState({});
  const [sampleMaterialCodes] = useCodes({
    codeGroupId: codeGroup.SampleMaterialCode
  });
  const build = useBuild();

  useEffect(
    () => {
      if (items.length > 0 && sampleMaterialCodes.length > 0) {
        const { sampleMaterialCodeId, boxNote } = items[0];
        setTopInfo({
          material: build.displayName({
            source: sampleMaterialCodes,
            value: sampleMaterialCodeId
          }),
          note: boxNote
        });
      }
    },
    [items, sampleMaterialCodes]
  );

  const columns = [
    {
      key: 'boxBarcode',
      name: 'Box ID'
    },
    {
      key: 'status',
      name: 'Status'
    },
    {
      key: 'freezer',
      name: 'Freezer'
    },
    { key: 'location', name: 'Location' },
    {
      key: 'dateTime',
      name: 'Datetime'
    },
    {
      key: 'memberName',
      name: 'User'
    },
    {
      key: 'freezerOutReason',
      name: 'Out Reason'
    }
  ];

  return (
    <Container>
      <StyledForm>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Project</label>
            {title}
          </Form.Field>
          <Form.Field>
            <label>Site</label>
            {site}
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Material</label>
            {topInfo.material}
          </Form.Field>
          <Form.Field>
            <label>Aliquot</label>
            {analyte}
          </Form.Field>
          <Form.Field>
            <label>Visit</label>
            {visit}
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Box ID</label>
            {boxBarcode}
          </Form.Field>
          <Form.Field>
            <label>Note</label>
            {topInfo.note}
          </Form.Field>
          <Form.Field>
            <label />
          </Form.Field>
        </Form.Group>
      </StyledForm>
      <Divider />
      <DataGrid
        columns={columns}
        rows={items}
        minHeight={350}
        rowsCount={items.length}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        rowKey="rowIndex"
        loading={loading}
      />
    </Container>
  );
};

export default BoxTrackingContent;
