import React, { useEffect } from 'react';
import { Select } from 'semantic-ui-react';
import { useCodes, useFormatMessage } from '../../hooks';
import { useProjectsOfSamples } from './hooks';
import { useProjectSite } from '../../hooks';
import { codeGroup } from '../../constants';

export const ProjectsOfSamples = ({
  name,
  value,
  onChange,
  hasAll = false,
  defaultFirstValue = false,
  fluid = false
}) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useProjectsOfSamples();
  const options = hasAll
    ? [
        {
          text: t('all'),
          value: 0
        },
        ...items
      ]
    : items;

  useEffect(
    () => {
      if (defaultFirstValue && items.length > 0 && value === null) {
        onChange(null, { name, value: items[0].value });
      }
    },
    [items, value]
  );

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={options}
      loading={loading}
      fluid={fluid}
    />
  );
};

export const ProjectSite = ({
  value,
  onChange,
  projectId,
  select,
  ...rest
}) => {
  const t = useFormatMessage();
  const [items] = useProjectSite({ projectId });

  let options = [
    {
      text: t('all'),
      value: 0,
      subjectprefix: ''
    }
  ];

  return (
    <Select
      item
      value={value}
      onChange={onChange}
      options={options.concat(items)}
      {...rest}
    />
  );
};

export const SampleAliquotStatus = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.SampleAliquotStatusCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('all'),
          value: 0
        },
        ...items
      ]}
    />
  );
};

export const FreezerMachine = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.FreezerMachineCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('all'),
          value: 0
        },
        ...items
      ]}
    />
  );
};
