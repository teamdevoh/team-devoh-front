import React, { useState } from 'react';
import Board from './Board';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams, useLocation } from 'react-router-dom';
import { useSampleCollectionList } from './hooks';
import helpers from '../../helpers';

const DashBoard = () => {
  const { projectId } = useParams();
  const location = useLocation();
  const [filter, setFilter] = useState({
    projectId: 0,
    projectSiteId: 0,
    subjectIds: null,
    sampleStatusCodeId: 0,
    freezerId: 0,
    barcode: '',
    preserveEndDtc: helpers.qs.parse(location.search).preserve || ''
  });

  const handleFilterChange = (event, { name, value }) => {
    let newFilter = { ...filter, [name]: value };

    if (name === 'projectId') {
      newFilter = {
        ...newFilter,
        projectSiteId: 0,
        subjectIds: null
      };
    }

    if (name === 'projectSiteId') {
      newFilter = {
        ...newFilter,
        subjectIds: null
      };
    }

    setFilter({ ...newFilter });
  };

  const [{ items, loading }, setItems] = useSampleCollectionList({
    curProjectId: projectId,
    ...filter
  });

  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onFilterChange={handleFilterChange}
            filter={filter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool items={items} loading={loading} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Board
            items={items}
            loading={loading}
            projectId={projectId}
            setItems={setItems}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
