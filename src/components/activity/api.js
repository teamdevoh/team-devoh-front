import helpers from '../../helpers';

const reviewFirst = ({ subjectId, projectActivityId, activityKeyId }) => {
  return helpers.Service.post(
    `api/subjects/${subjectId}/activities/${activityKeyId}/review.first`,
    {
      projectActivityId
    }
  );
};

const reviewSecond = ({ subjectId, projectActivityId, activityKeyId }) => {
  return helpers.Service.post(
    `api/subjects/${subjectId}/activities/${activityKeyId}/review.second`,
    {
      projectActivityId
    }
  );
};

const reviewThird = ({ subjectId, projectActivityId, activityKeyId }) => {
  return helpers.Service.post(
    `api/subjects/${subjectId}/activities/${activityKeyId}/review.third`,
    {
      projectActivityId
    }
  );
};

const fetchProjectActivity = ({ projectId, activityGroupId, activityId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/activitygroups/${activityGroupId}/activities/${activityId}`
  );
};

const fetchLabRanges = ({ subjectId, lbType }) => {
  const params = helpers.qs.stringify({
    subjectId,
    lbType
  });

  return helpers.Service.get(`api/labranges?${params}`);
};

const fetchInterview = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/interview?${params}`);
};

const addInterview = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/interview`, model);
};

const updateInterview = ({ activityId, subjectInterviewId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/interview/${subjectInterviewId}`,
    model
  );
};

const fetchConsent = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/consent?${params}`);
};

const addConsent = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/consent`, model);
};

const updateConsent = ({ activityId, subjectConsentId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/consent/${subjectConsentId}`,
    model
  );
};

const fetchICRE = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/icre?${params}`);
};

const addICRE = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/icre`, model);
};

const updateICRE = ({ activityId, subjectIcreid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/icre/${subjectIcreid}`,
    model
  );
};

const fetchDiagnosisList = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/diagnosis?${params}`);
};

const fetchDiagnosis = ({
  subjectId,
  projectActivityId,
  subjectTbDiagnosisId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/diagnosis/${subjectTbDiagnosisId}?${params}`
  );
};

const addDiagnosis = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/diagnosis`, model);
};

const updateDiagnosis = ({ activityId, subjectTbDiagnosisId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/diagnosis/${subjectTbDiagnosisId}`,
    model
  );
};

const removeDiagnosis = ({ activityId, subjectTbDiagnosisId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/diagnosis/${subjectTbDiagnosisId}`,
    { data: model }
  );
};

const fetchPkParamList = ({ subjectId, projectActivityId, reject }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/pkparam?${params}`,
    null,
    null,
    null,
    reject
  );
};

const fetchPkParam = ({
  subjectId,
  projectActivityId,
  subjectPkparamId,
  subjectTdmdrugId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId,
    subjectTdmdrugId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/pkparam/${subjectPkparamId}?${params}`
  );
};

const addPkParam = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/pkparam`, model);
};

const updatePkParam = ({ activityId, subjectPkparamId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/pkparam/${subjectPkparamId}`,
    model
  );
};

const removePkParam = ({ activityId, subjectPkparamId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/pkparam/${subjectPkparamId}`,
    { data: model }
  );
};

const fetchTdmconcentrationList = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/tdmconcentration?${params}`
  );
};

const fetchTdmconcentration = ({
  subjectId,
  projectActivityId,
  subjectTdmconcentrationId,
  subjectTdmdrugId,
  subjectTdmsamplingId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId,
    subjectTdmdrugId,
    subjectTdmsamplingId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/tdmconcentration/${subjectTdmconcentrationId}?${params}`
  );
};

const addTdmconcentration = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(
    `api/activities/${activityId}/tdmconcentration`,
    model
  );
};

const updateTdmconcentration = ({
  activityId,
  subjectTdmconcentrationId,
  model
}) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/tdmconcentration/${subjectTdmconcentrationId}`,
    model
  );
};

const removeTdmconcentration = ({
  activityId,
  subjectTdmconcentrationId,
  model
}) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/tdmconcentration/${subjectTdmconcentrationId}`,
    { data: model }
  );
};

const fetchBodies = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/bodies?${params}`);
};

const fetchBody = ({ subjectId, projectActivityId, subjectBodyId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/bodies/${subjectBodyId}?${params}`
  );
};

const addBody = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/bodies`, model);
};

const updateBody = ({ activityId, subjectBodyId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/bodies/${subjectBodyId}`,
    model
  );
};

const removeBody = ({ activityId, subjectBodyId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/bodies/${subjectBodyId}`,
    { data: model }
  );
};

const fetchComorbids = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/comorbids?${params}`);
};

const fetchComorbid = ({ subjectId, projectActivityId, subjectComorbidId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/comorbids/${subjectComorbidId}?${params}`
  );
};

const addComorbid = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/comorbids`, model);
};

const updateComorbid = ({ activityId, subjectComorbidId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/comorbids/${subjectComorbidId}`,
    model
  );
};

const removeComorbid = ({ activityId, subjectComorbidId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/comorbids/${subjectComorbidId}`,
    { data: model }
  );
};

const fetchSubTBDrugs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/subtbdrugs?${params}`);
};

const fetchSubTBDrug = ({ subjectId, projectActivityId, subjectTbdrugId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/subtbdrugs/${subjectTbdrugId}?${params}`
  );
};

const addSubTBDrug = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/subtbdrugs`, model);
};

const updateSubTBDrug = ({ activityId, subjectTbdrugId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/subtbdrugs/${subjectTbdrugId}`,
    model
  );
};

const removeSubTBDrug = ({ activityId, subjectTbdrugId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/subtbdrugs/${subjectTbdrugId}`,
    { data: model }
  );
};

const fetchCMs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/subcms?${params}`);
};

const fetchCM = ({ subjectId, projectActivityId, subjectCmdrugId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/subcms/${subjectCmdrugId}?${params}`
  );
};

const addCM = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/subcms`, model);
};

const updateCM = ({ activityId, subjectCmdrugId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/subcms/${subjectCmdrugId}`,
    model
  );
};

const removeCM = ({ activityId, subjectCmdrugId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/subcms/${subjectCmdrugId}`,
    { data: model }
  );
};

const fetchPhysico = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/physico?${params}`);
};

const addPhysico = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/physico`, model);
};

const updatePhysico = ({ activityId, tbdrugPhysicoId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/physico/${tbdrugPhysicoId}`,
    model
  );
};

const fetchAdrAgep = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/adrAgep?${params}`);
};

const addAdrAgep = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/adrAgep`, model);
};

const updateAdrAgep = ({ activityId, subjectAdrAgepid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/adrAgep/${subjectAdrAgepid}`,
    model
  );
};

const fetchAdrDress = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/adrDress?${params}`);
};

const addAdrDress = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/adrDress`, model);
};

const updateAdrDress = ({ activityId, subjectAdrDressid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/adrDress/${subjectAdrDressid}`,
    model
  );
};

const fetchAdrSjsten = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/adrSjsten?${params}`);
};

const addAdrSjsten = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/adrSjsten`, model);
};

const updateAdrSjsten = ({ activityId, subjectAdrSjstenid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/adrSjsten/${subjectAdrSjstenid}`,
    model
  );
};

const fetchCaseConclusion = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/caseConclusion?${params}`
  );
};

const addCaseConclusion = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(
    `api/activities/${activityId}/caseConclusion`,
    model
  );
};

const updateCaseConclusion = ({
  activityId,
  subjectCaseConclusionId,
  model
}) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/caseConclusion/${subjectCaseConclusionId}`,
    model
  );
};

const fetchAFBs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/afbs?${params}`);
};

const fetchAFB = ({ subjectId, projectActivityId, subjectAfbid }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/afbs/${subjectAfbid}?${params}`
  );
};

const addAFB = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/afbs`, model);
};

const updateAFB = ({ activityId, subjectAfbid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/afbs/${subjectAfbid}`,
    model
  );
};

const removeAFB = ({ activityId, subjectAfbid, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/afbs/${subjectAfbid}`,
    { data: model }
  );
};

const fetchXperts = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/xperts?${params}`);
};

const fetchXpert = ({ subjectId, projectActivityId, subjectXpertId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/xperts/${subjectXpertId}?${params}`
  );
};

const addXpert = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/xperts`, model);
};

const updateXpert = ({ activityId, subjectXpertId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/xperts/${subjectXpertId}`,
    model
  );
};

const removeXpert = ({ activityId, subjectXpertId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/xperts/${subjectXpertId}`,
    { data: model }
  );
};

const fetchRapids = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/rapids?${params}`);
};

const fetchRapid = ({ subjectId, projectActivityId, subjectRapidTestId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/rapids/${subjectRapidTestId}?${params}`
  );
};

const addRapid = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/rapids`, model);
};

const updateRapid = ({ activityId, subjectRapidTestId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/rapids/${subjectRapidTestId}`,
    model
  );
};

const removeRapid = ({ activityId, subjectRapidTestId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/rapids/${subjectRapidTestId}`,
    { data: model }
  );
};

const fetchIGRAs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/igras?${params}`);
};

const fetchIGRA = ({ subjectId, projectActivityId, subjectIgraid }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/igras/${subjectIgraid}?${params}`
  );
};

const addIGRA = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/igras`, model);
};

const updateIGRA = ({ activityId, subjectIgraid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/igras/${subjectIgraid}`,
    model
  );
};

const removeIGRA = ({ activityId, subjectIgraid, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/igras/${subjectIgraid}`,
    { data: model }
  );
};

const fetchPCRs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/pcrs?${params}`);
};

const fetchPCR = ({ subjectId, projectActivityId, subjectPcrtestId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/pcrs/${subjectPcrtestId}?${params}`
  );
};

const addPCR = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/pcrs`, model);
};

const updatePCR = ({ activityId, subjectPcrtestId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/pcrs/${subjectPcrtestId}`,
    model
  );
};

const removePCR = ({ activityId, subjectPcrtestId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/pcrs/${subjectPcrtestId}`,
    { data: model }
  );
};

const fetchXRays = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/xrays?${params}`);
};

const fetchXRay = ({ subjectId, projectActivityId, subjectXrayId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/xrays/${subjectXrayId}?${params}`
  );
};

const addXRay = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/xrays`, model);
};

const updateXRay = ({ activityId, subjectXrayId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/xrays/${subjectXrayId}`,
    model
  );
};

const removeXRay = ({ activityId, subjectXrayId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/xrays/${subjectXrayId}`,
    { data: model }
  );
};

const fetchCTs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/cts?${params}`);
};

const fetchCT = ({ subjectId, projectActivityId, subjectCtid }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/cts/${subjectCtid}?${params}`
  );
};

const addCT = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/cts`, model);
};

const updateCT = ({ activityId, subjectCtid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/cts/${subjectCtid}`,
    model
  );
};

const removeCT = ({ activityId, subjectCtid, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/cts/${subjectCtid}`,
    { data: model }
  );
};

const fetchDSTs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/dsts?${params}`);
};

const fetchDST = ({ subjectId, projectActivityId, subjectDstid }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/dsts/${subjectDstid}?${params}`
  );
};

const addDST = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/dsts`, model);
};

const updateDST = ({ activityId, subjectDstid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/dsts/${subjectDstid}`,
    model
  );
};

const removeDST = ({ activityId, subjectDstid, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/dsts/${subjectDstid}`,
    { data: model }
  );
};

const fetchMicresults = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/micresults?${params}`);
};

const fetchMicresult = ({
  subjectId,
  projectActivityId,
  subjectMicresultId,
  subjectTbdrugId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId,
    subjectTbdrugId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/micresults/${subjectMicresultId}?${params}`
  );
};

const fetchMicTestOfSubjectMicResult = ({ subjectId, exdrugId }) => {
  const params = helpers.qs.stringify({
    exdrugId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/micresults/mictest?${params}`
  );
};

const addMicresult = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/micresults`, model);
};

const updateMicresult = ({ activityId, subjectMicresultId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/micresults/${subjectMicresultId}`,
    model
  );
};

const removeMicresult = ({ activityId, subjectMicresultId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/micresults/${subjectMicresultId}`,
    { data: model }
  );
};

const fetchMicreports = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/micreports?${params}`);
};

const fetchMicreport = ({
  subjectId,
  projectActivityId,
  subjectMicreportId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/micreports/${subjectMicreportId}?${params}`
  );
};

const addMicreport = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/micreports`, model);
};

const updateMicreport = ({ activityId, subjectMicreportId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/micreports/${subjectMicreportId}`,
    model
  );
};

const removeMicreport = ({ activityId, subjectMicreportId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/micreports/${subjectMicreportId}`,
    { data: model }
  );
};

const fetchResGenes = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/resgenes?${params}`);
};

const fetchResGene = ({
  subjectId,
  projectActivityId,
  subjectResGeneId,
  subjectTbdrugId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId,
    subjectTbdrugId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/resgenes/${subjectResGeneId}?${params}`
  );
};

const addResGene = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/resgenes`, model);
};

const updateResGene = ({ activityId, subjectResGeneId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/resgenes/${subjectResGeneId}`,
    model
  );
};

const removeResGene = ({ activityId, subjectResGeneId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/resgenes/${subjectResGeneId}`,
    { data: model }
  );
};

const fetchTdmsamplingList = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/tdmsampling?${params}`);
};

const fetchTdmsamplingListOfSubjectVisit = ({ subjectId, subjectVisitId }) => {
  return helpers.Service.get(
    `api/subjects/${subjectId}/visits/${subjectVisitId}/tdmsampling`
  );
};

const fetchTdmsampling = ({
  subjectId,
  projectActivityId,
  subjectTdmsamplingId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/tdmsampling/${subjectTdmsamplingId}?${params}`
  );
};

const addTdmsampling = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(
    `api/activities/${activityId}/tdmsampling`,
    model
  );
};

const updateTdmsampling = ({ activityId, subjectTdmsamplingId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/tdmsampling/${subjectTdmsamplingId}`,
    model
  );
};

const removeTdmsampling = ({ activityId, subjectTdmsamplingId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/tdmsampling/${subjectTdmsamplingId}`,
    { data: model }
  );
};

const fetchTdmreportList = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/tdmreport?${params}`);
};

const fetchTdmreport = ({
  subjectId,
  projectActivityId,
  subjectTdmreportId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/tdmreport/${subjectTdmreportId}?${params}`
  );
};

const addTdmreport = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/tdmreport`, model);
};

const updateTdmreport = ({ activityId, subjectTdmreportId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/tdmreport/${subjectTdmreportId}`,
    model
  );
};

const removeTdmreport = ({ activityId, subjectTdmreportId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/tdmreport/${subjectTdmreportId}`,
    { data: model }
  );
};

const fetchGenotyping = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/genotyping?${params}`);
};

const addGenotyping = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/genotyping`, model);
};

const updateGenotyping = ({ activityId, subjectGenotypingId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/genotyping/${subjectGenotypingId}`,
    model
  );
};

const fetchMictransferList = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/mictransfer?${params}`);
};

const fetchMictransfer = ({
  subjectId,
  projectActivityId,
  subjectMictransferId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/mictransfer/${subjectMictransferId}?${params}`
  );
};

const addMictransfer = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(
    `api/activities/${activityId}/mictransfer`,
    model
  );
};

const updateMictransfer = ({ activityId, subjectMictransferId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/mictransfer/${subjectMictransferId}`,
    model
  );
};

const removeMictransfer = ({ activityId, subjectMictransferId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/mictransfer/${subjectMictransferId}`,
    { data: model }
  );
};

const fetchLBCHs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/lbchs?${params}`);
};

const fetchLBCH = ({ subjectId, projectActivityId, subjectLbchid }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/lbchs/${subjectLbchid}?${params}`
  );
};

const addLBCH = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/lbchs`, model);
};

const updateLBCH = ({ activityId, subjectLbchid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/lbchs/${subjectLbchid}`,
    model
  );
};

const removeLBCH = ({ activityId, subjectLbchid, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/lbchs/${subjectLbchid}`,
    { data: model }
  );
};

const fetchLBHEMs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/lbhems?${params}`);
};

const fetchLBHEM = ({ subjectId, projectActivityId, subjectLbhemid }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/lbhems/${subjectLbhemid}?${params}`
  );
};

const addLBHEM = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/lbhems`, model);
};

const updateLBHEM = ({ activityId, subjectLbhemid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/lbhems/${subjectLbhemid}`,
    model
  );
};

const removeLBHEM = ({ activityId, subjectLbhemid, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/lbhems/${subjectLbhemid}`,
    { data: model }
  );
};

const fetchDialysis = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/dialysises?${params}`);
};

const addDialysis = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/dialysises`, model);
};

const updateDialysis = ({ activityId, subjectDialysisId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/dialysises/${subjectDialysisId}`,
    model
  );
};

const fetchElectrolytes = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/electrolytes?${params}`
  );
};

const fetchElectrolyte = ({
  subjectId,
  projectActivityId,
  subjectElectrolyteId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/electrolytes/${subjectElectrolyteId}?${params}`
  );
};

const addElectrolyte = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(
    `api/activities/${activityId}/electrolytes`,
    model
  );
};

const updateElectrolyte = ({ activityId, subjectElectrolyteId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/electrolytes/${subjectElectrolyteId}`,
    model
  );
};

const removeElectrolyte = ({ activityId, subjectElectrolyteId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/electrolytes/${subjectElectrolyteId}`,
    { data: model }
  );
};

const fetchOtherExams = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/otherexams?${params}`);
};

const fetchOtherExam = ({
  subjectId,
  projectActivityId,
  subjectOtherExamId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/otherexams/${subjectOtherExamId}?${params}`
  );
};

const addOtherExam = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/otherexams`, model);
};

const updateOtherExam = ({ activityId, subjectOtherExamId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/otherexams/${subjectOtherExamId}`,
    model
  );
};

const removeOtherExam = ({ activityId, subjectOtherExamId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/otherexams/${subjectOtherExamId}`,
    { data: model }
  );
};

const fetchAdmeBasic = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });
  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/admebasic?${params}`);
};

const addAdmeBasic = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/admebasic`, model);
};

const updateAdmeBasic = ({ activityId, tbdrugAdmeBasicId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/admebasic/${tbdrugAdmeBasicId}`,
    model
  );
};

const fetchInhibitions = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/inhibitions?${params}`);
};

const fetchInhibition = ({
  tbdrugId,
  projectActivityId,
  tbdrugInhibitionId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/inhibitions/${tbdrugInhibitionId}?${params}`
  );
};

const addInhibition = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/inhibition`, model);
};

const updateInhibition = ({ activityId, tbdrugInhibitionId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/inhibitions/${tbdrugInhibitionId}`,
    model
  );
};

const removeInhibition = ({ activityId, tbdrugInhibitionId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/inhibitions/${tbdrugInhibitionId}`,
    { data: model }
  );
};

const fetchMeTransporters = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/meTransporters?${params}`
  );
};

const fetchMeTransporter = ({
  tbdrugId,
  projectActivityId,
  tbdrugMeTransporterId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/meTransporters/${tbdrugMeTransporterId}?${params}`
  );
};

const addMeTransporter = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(
    `api/activities/${activityId}/meTransporter`,
    model
  );
};

const updateMeTransporter = ({ activityId, tbdrugMeTransporterId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/meTransporters/${tbdrugMeTransporterId}`,
    model
  );
};

const removeMeTransporter = ({ activityId, tbdrugMeTransporterId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/meTransporters/${tbdrugMeTransporterId}`,
    { data: model }
  );
};

const fetchPkDataList = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/pkData?${params}`);
};

const fetchPkData = ({ tbdrugId, projectActivityId, tbdrugPkdataId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/pkData/${tbdrugPkdataId}?${params}`
  );
};

const addPkData = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/pkData`, model);
};

const updatePkData = ({ activityId, tbdrugPkdataId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/pkData/${tbdrugPkdataId}`,
    model
  );
};

const removePkData = ({ activityId, tbdrugPkdataId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/pkData/${tbdrugPkdataId}`,
    { data: model }
  );
};

const fetchFactorsList = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/factors?${params}`);
};

const fetchFactors = ({ tbdrugId, projectActivityId, tbdrugFactorsId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/factors/${tbdrugFactorsId}?${params}`
  );
};

const addFactors = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/factors`, model);
};

const updateFactors = ({ activityId, tbdrugFactorsId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/factors/${tbdrugFactorsId}`,
    model
  );
};

const removeFactors = ({ activityId, tbdrugFactorsId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/factors/${tbdrugFactorsId}`,
    { data: model }
  );
};

const fetchPgxList = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/pgx?${params}`);
};

const fetchPgx = ({ tbdrugId, projectActivityId, tbdrugPgxId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/pgx/${tbdrugPgxId}?${params}`
  );
};

const addPgx = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/pgx`, model);
};

const updatePgx = ({ activityId, tbdrugPgxId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/pgx/${tbdrugPgxId}`,
    model
  );
};

const removePgx = ({ activityId, tbdrugPgxId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/pgx/${tbdrugPgxId}`,
    { data: model }
  );
};

const fetchDdiList = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/ddi?${params}`);
};

const fetchDdi = ({ tbdrugId, projectActivityId, tbdrugDdiId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/ddi/${tbdrugDdiId}?${params}`
  );
};

const addDdi = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/ddi`, model);
};

const updateDdi = ({ activityId, tbdrugDdiId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/ddi/${tbdrugDdiId}`,
    model
  );
};

const removeDdi = ({ activityId, tbdrugDdiId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/ddi/${tbdrugDdiId}`,
    { data: model }
  );
};

const fetchPdDataList = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/pdData?${params}`);
};

const fetchPdData = ({ tbdrugId, projectActivityId, tbdrugPddataId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/pdData/${tbdrugPddataId}?${params}`
  );
};

const addPdData = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/pdData`, model);
};

const updatePdData = ({ activityId, tbdrugPddataId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/pdData/${tbdrugPddataId}`,
    model
  );
};

const removePdData = ({ activityId, tbdrugPddataId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/pdData/${tbdrugPddataId}`,
    { data: model }
  );
};

const fetchToxicityList = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/toxicity?${params}`);
};

const fetchToxicity = ({ tbdrugId, projectActivityId, tbdrugToxicityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/toxicity/${tbdrugToxicityId}?${params}`
  );
};

const addToxicity = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/toxicity`, model);
};

const updateToxicity = ({ activityId, tbdrugToxicityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/toxicity/${tbdrugToxicityId}`,
    model
  );
};

const removeToxicity = ({ activityId, tbdrugToxicityId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/toxicity/${tbdrugToxicityId}`,
    { data: model }
  );
};

const fetchModelList = ({ tbdrugId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/tbdrugs/${tbdrugId}/model?${params}`);
};

const fetchModel = ({ tbdrugId, projectActivityId, tbdrugModelId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/tbdrugs/${tbdrugId}/model/${tbdrugModelId}?${params}`
  );
};

const addModel = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/model`, model);
};

const updateModel = ({ activityId, tbdrugModelId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/model/${tbdrugModelId}`,
    model
  );
};

const removeModel = ({ activityId, tbdrugModelId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/model/${tbdrugModelId}`,
    { data: model }
  );
};

const fetchTDMDrugs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/tdmdrugs?${params}`);
};

const fetchTDMDrug = ({
  subjectId,
  projectActivityId,
  subjectVisitId,
  subjectTbdrugId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId,
    subjectVisitId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/tdmdrugs/${subjectTbdrugId}?${params}`
  );
};

const addTDMDrug = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/tdmdrugs`, model);
};

const updateTDMDrug = ({ activityId, subjectTdmdrugId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/tdmdrugs/${subjectTdmdrugId}`,
    model
  );
};

const removeTDMDrug = ({ activityId, subjectTdmdrugId, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/tdmdrugs/${subjectTdmdrugId}`,
    { data: model }
  );
};

const fetchADRs = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/adrs?${params}`);
};

const fetchADR = ({ subjectId, projectActivityId, subjectAdrid }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/adrs/${subjectAdrid}?${params}`
  );
};

const addADR = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/adrs`, model);
};

const updateADR = ({ activityId, subjectAdrid, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/adrs/${subjectAdrid}`,
    model
  );
};

const removeADR = ({ activityId, subjectAdrid, model }) => {
  return helpers.Service.delete(
    `api/activities/${activityId}/adrs/${subjectAdrid}`,
    { data: model }
  );
};

const fetchRelapse = ({ subjectId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(`api/subjects/${subjectId}/relapse?${params}`);
};

const addRelapse = ({ activityId, model }) => {
  model.activityId = activityId;
  return helpers.Service.post(`api/activities/${activityId}/relapse`, model);
};

const updateRelapse = ({ activityId, subjectRelapseId, model }) => {
  model.activityId = activityId;
  return helpers.Service.put(
    `api/activities/${activityId}/relapse/${subjectRelapseId}`,
    model
  );
};

export default {
  reviewFirst,
  reviewSecond,
  reviewThird,
  fetchProjectActivity,
  fetchLabRanges,
  fetchInterview,
  addInterview,
  updateInterview,
  fetchConsent,
  addConsent,
  updateConsent,
  fetchICRE,
  addICRE,
  updateICRE,
  fetchDiagnosisList,
  fetchDiagnosis,
  addDiagnosis,
  updateDiagnosis,
  removeDiagnosis,
  fetchPkParamList,
  fetchPkParam,
  addPkParam,
  updatePkParam,
  removePkParam,
  fetchTdmconcentrationList,
  fetchTdmconcentration,
  addTdmconcentration,
  updateTdmconcentration,
  removeTdmconcentration,
  fetchBodies,
  fetchBody,
  addBody,
  updateBody,
  removeBody,
  fetchComorbids,
  fetchComorbid,
  addComorbid,
  updateComorbid,
  removeComorbid,
  fetchSubTBDrugs,
  fetchSubTBDrug,
  addSubTBDrug,
  updateSubTBDrug,
  removeSubTBDrug,
  fetchCMs,
  fetchCM,
  addCM,
  updateCM,
  removeCM,
  fetchPhysico,
  addPhysico,
  updatePhysico,
  fetchAdrAgep,
  addAdrAgep,
  updateAdrAgep,
  fetchAdrDress,
  addAdrDress,
  updateAdrDress,
  fetchAdrSjsten,
  addAdrSjsten,
  updateAdrSjsten,
  fetchCaseConclusion,
  addCaseConclusion,
  updateCaseConclusion,
  fetchAFBs,
  fetchAFB,
  addAFB,
  updateAFB,
  removeAFB,
  fetchXperts,
  fetchXpert,
  addXpert,
  updateXpert,
  removeXpert,
  fetchRapids,
  fetchRapid,
  addRapid,
  updateRapid,
  removeRapid,
  fetchIGRAs,
  fetchIGRA,
  addIGRA,
  updateIGRA,
  removeIGRA,
  fetchPCRs,
  fetchPCR,
  addPCR,
  updatePCR,
  removePCR,
  fetchXRays,
  fetchXRay,
  addXRay,
  updateXRay,
  removeXRay,
  fetchCTs,
  fetchCT,
  addCT,
  updateCT,
  removeCT,
  fetchDSTs,
  fetchDST,
  addDST,
  updateDST,
  removeDST,
  fetchMicresults,
  fetchMicresult,
  fetchMicTestOfSubjectMicResult,
  addMicresult,
  updateMicresult,
  removeMicresult,
  fetchMicreports,
  fetchMicreport,
  addMicreport,
  updateMicreport,
  removeMicreport,
  fetchResGenes,
  fetchResGene,
  addResGene,
  updateResGene,
  removeResGene,
  fetchTdmsamplingList,
  fetchTdmsamplingListOfSubjectVisit,
  fetchTdmsampling,
  addTdmsampling,
  updateTdmsampling,
  removeTdmsampling,
  fetchTdmreportList,
  fetchTdmreport,
  addTdmreport,
  updateTdmreport,
  removeTdmreport,
  fetchGenotyping,
  addGenotyping,
  updateGenotyping,
  fetchMictransferList,
  fetchMictransfer,
  addMictransfer,
  updateMictransfer,
  removeMictransfer,
  fetchLBCHs,
  fetchLBCH,
  addLBCH,
  updateLBCH,
  removeLBCH,
  fetchLBHEMs,
  fetchLBHEM,
  addLBHEM,
  updateLBHEM,
  removeLBHEM,
  fetchDialysis,
  addDialysis,
  fetchElectrolytes,
  fetchElectrolyte,
  addElectrolyte,
  updateElectrolyte,
  removeElectrolyte,
  fetchOtherExams,
  fetchOtherExam,
  addOtherExam,
  updateOtherExam,
  removeOtherExam,
  updateDialysis,
  fetchAdmeBasic,
  addAdmeBasic,
  updateAdmeBasic,
  fetchInhibitions,
  fetchInhibition,
  addInhibition,
  updateInhibition,
  removeInhibition,
  fetchMeTransporters,
  fetchMeTransporter,
  addMeTransporter,
  updateMeTransporter,
  removeMeTransporter,
  fetchPkDataList,
  fetchPkData,
  addPkData,
  updatePkData,
  removePkData,
  fetchFactorsList,
  fetchFactors,
  addFactors,
  updateFactors,
  removeFactors,
  fetchPgxList,
  fetchPgx,
  addPgx,
  updatePgx,
  removePgx,
  fetchDdiList,
  fetchDdi,
  addDdi,
  updateDdi,
  removeDdi,
  fetchPdDataList,
  fetchPdData,
  addPdData,
  updatePdData,
  removePdData,
  fetchToxicityList,
  fetchToxicity,
  addToxicity,
  updateToxicity,
  removeToxicity,
  fetchModelList,
  fetchModel,
  addModel,
  updateModel,
  removeModel,
  fetchTDMDrugs,
  fetchTDMDrug,
  addTDMDrug,
  updateTDMDrug,
  removeTDMDrug,
  fetchADRs,
  fetchADR,
  addADR,
  updateADR,
  removeADR,
  fetchRelapse,
  addRelapse,
  updateRelapse
};
