import React from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DatePicker } from '../../datepicker';
import { SelectReason } from '../Selector';

const MicreportForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.micreport.micRepDtc')}</label>
        <DatePicker
          name="micRepDtc"
          value={model.micRepDtc}
          onChange={onChange}
        />
        {validator.message(
          t('activity.micreport.micRepDtc'),
          model.micRepDtc,
          'required'
        )}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.micreport.micResDes')}</label>
        <TextArea
          name="micResDes"
          value={model.micResDes}
          onChange={onChange}
        />
      </Form.Field>
      {model.subjectMicreportId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.micreport.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default MicreportForm;
