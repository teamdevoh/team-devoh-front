import { usePublishStatus } from '../../collection/hooks';
import { useLocation } from 'react-router-dom';
import helpers from '../../../helpers';

const useRepeat = (key, projectActivityId = 0) => {
  const location = useLocation();
  const query = helpers.qs.parse(location.search);
  const publish = usePublishStatus({
    projectActivityId: query.projectActivityId
  });

  const updateByKey = (source, model) => {
    const isNewItem = !_.find(source, { [key]: model[key] });
    if (isNewItem) {
      source = source.concat(model);
    }

    const updated = source.map(item => {
      if (item[key] === model[key]) {
        return {
          ...item,
          ...model
        };
      } else {
        return item;
      }
    });

    return updated;
  };

  const updateRepeatByKey = ({
    activityStatusCodeId,
    source,
    setSource,
    target,
    setTarget,
    updatedRepeat
  }) => {
    const updatedModel = {
      ...target,
      activityStatusCodeId,
      isChanged: false,
      reason: ''
    };
    if (typeof updatedRepeat === 'undefined' || updatedRepeat === null) {
      updatedRepeat = updateByKey(source, updatedModel);
    }

    setSource(updatedRepeat);

    if (
      projectActivityId === 0 ||
      Number(updatedModel.projectActivityId) === Number(projectActivityId)
    ) {
      projectActivityId = query.projectActivityId;
      const pickedActivity = publish.pickActivityValueBy(
        updatedRepeat.filter(
          item => Number(item.projectActivityId) === Number(projectActivityId)
        )
      );
      publish.activityStatus(pickedActivity);
    }

    setTarget(updatedModel);
  };

  return { updateRepeatByKey, updateByKey };
};

export default useRepeat;
