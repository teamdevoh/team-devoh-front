import { useCallback } from 'react';
import { useBoolean } from '../../../hooks';
import api from '../api';
import useFormStatus from './useFormStatus';

const useAdmeBasic = ({ activityKey }) => {
  const isFetching = useBoolean(false);
  const isSaving = useBoolean(false);
  const { convert } = useFormStatus();

  const fetchItem = useCallback(
    async ({ tbdrugId, projectActivityId }) => {
      isFetching.setTrue();
      const res = await api.fetchAdmeBasic({
        tbdrugId,
        projectActivityId
      });
      isFetching.setFalse();

      if (res && res.data) {
        return res.data;
      }
      return {};
    },
    [isFetching.value]
  );

  const validateModel = model => {
    const newModel = { ...model };
    for (let key in newModel) {
      if (newModel.hasOwnProperty(key)) {
        if (key.indexOf('ReferenceId') >= 0) {
          const inputTextKey = key.split('ReferenceId')[0];
          if (!newModel[inputTextKey]) {
            // text 값을 입력 안했으면 referenceId 도 null 로
            newModel[key] = null;
          }
        }
      }
    }
    return newModel;
  };

  const add = useCallback(
    async ({ model }) => {
      model.activityStatusCodeId = convert(model.isDone);
      model = validateModel(model);
      isSaving.setTrue();
      try {
        const res = await api.addAdmeBasic({
          activityId: activityKey,
          model
        });
        if (res && res.data > 0) {
          return res.data;
        }
        return 0;
      } finally {
        isSaving.setFalse();
      }
    },
    [activityKey]
  );

  const update = useCallback(
    async ({ model }) => {
      model.activityStatusCodeId = convert(
        model.isDone,
        model.activityStatusCodeId
      );
      model = validateModel(model);
      isSaving.setTrue();
      const res = await api.updateAdmeBasic({
        activityId: activityKey,
        tbdrugAdmeBasicId: model.tbdrugAdmeBasicId,
        model
      });
      isSaving.setFalse();
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [activityKey]
  );

  return {
    add,
    update,
    fetch: fetchItem,
    isFetching: isFetching.value,
    isSaving: isSaving.value
  };
};

export default useAdmeBasic;
