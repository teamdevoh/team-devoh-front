import { useCallback } from 'react';
import { useBoolean } from '../../../hooks';
import api from '../api';
import { useFormStatus } from './';

const usePkParam = ({ activityKey }) => {
  const isSaving = useBoolean(false);
  const isFetching = useBoolean(false);
  const { convert } = useFormStatus();

  const fetchItem = useCallback(
    async ({
      subjectId,
      projectActivityId,
      subjectPkparamId,
      subjectTdmdrugId
    }) => {
      isFetching.setTrue();
      const res = await api.fetchPkParam({
        subjectId,
        projectActivityId,
        subjectPkparamId,
        subjectTdmdrugId
      });

      isFetching.setFalse();

      if (res && res.data) {
        return res.data;
      }

      return {};
    },
    [isFetching.value]
  );

  const add = useCallback(
    async model => {
      model.activityStatusCodeId = convert(model.isDone);
      isSaving.setTrue();
      try {
        const res = await api.addPkParam({
          activityId: activityKey,
          model
        });

        if (res && res.data > 0) {
          return res.data;
        }

        return 0;
      } finally {
        isSaving.setFalse();
      }
    },
    [activityKey]
  );

  const update = useCallback(
    async model => {
      model.activityStatusCodeId = convert(
        model.isDone,
        model.activityStatusCodeId
      );
      isSaving.setTrue();
      const res = await api.updatePkParam({
        activityId: activityKey,
        subjectPkparamId: model.subjectPkparamId,
        model
      });
      isSaving.setFalse();
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [activityKey]
  );

  const remove = useCallback(
    async model => {
      isSaving.setTrue();
      const res = await api.removePkParam({
        activityId: activityKey,
        subjectPkparamId: model.subjectPkparamId,
        model
      });
      isSaving.setFalse();
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [isSaving.value]
  );

  return {
    add,
    update,
    remove,
    fetch: fetchItem,
    isFetching: isFetching.value,
    isSaving: isSaving.value
  };
};

export default usePkParam;
