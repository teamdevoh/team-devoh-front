import { useCallback } from 'react';
import useFormStatus from './useFormStatus';
import api from '../api';
import { useBoolean } from '../../../hooks';

const useBody = ({ activityKey }) => {
  const isSaving = useBoolean(false);
  const isFetching = useBoolean(false);
  const { convert } = useFormStatus();

  const fetchItem = useCallback(
    async ({ subjectId, projectActivityId, subjectBodyId }) => {
      isFetching.setTrue();
      const res = await api.fetchBody({
        subjectId,
        projectActivityId,
        subjectBodyId
      });
      isFetching.setFalse();

      if (res && res.data) {
        return res.data;
      }

      return {};
    },
    [isFetching.value]
  );

  const add = useCallback(
    async model => {
      model.activityStatusCodeId = convert(model.isDone);
      isSaving.setTrue();
      try {
        const res = await api.addBody({
          activityId: activityKey,
          model
        });
        if (res && res.data > 0) {
          return res.data;
        }
        return 0;
      } finally {
        isSaving.setFalse();
      }
    },
    [isSaving.value]
  );

  const update = useCallback(
    async model => {
      model.activityStatusCodeId = convert(
        model.isDone,
        model.activityStatusCodeId
      );
      isSaving.setTrue();
      const res = await api.updateBody({
        activityId: activityKey,
        subjectBodyId: model.subjectBodyId,
        model
      });
      isSaving.setFalse();
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [isSaving.value]
  );

  const remove = useCallback(
    async model => {
      isSaving.setTrue();
      const res = await api.removeBody({
        activityId: activityKey,
        subjectBodyId: model.subjectBodyId,
        model
      });
      isSaving.setFalse();
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [isSaving.value]
  );

  return {
    add,
    update,
    remove,
    fetch: fetchItem,
    isFetching: isFetching.value,
    isSaving: isSaving.value
  };
};

export default useBody;
