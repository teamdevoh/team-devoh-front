import { useCallback } from 'react';
import { useBoolean } from '../../../hooks';

const useActionStatus = cb => {
  const isNext = useBoolean(false);

  const submitted = useCallback(
    (event, data) => {
      if (data.name === 'next') {
        isNext.setTrue();
      }
      if (data.action === 'review') {
        cb(data.activityStatusCodeId);
      }
    },
    [isNext.value, cb]
  );

  return { submitted, isNext: isNext.value };
};

export default useActionStatus;
