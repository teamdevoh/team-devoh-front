import { useEffect, useState } from 'react';
import api from '../api';
import { useBoolean } from '../../../hooks';
import { useNoteStatusWithAll } from '../../activity-note/hooks';
import { useQueryStatusWithAll } from '../../activity-query/hooks';
import { useFileStatusWithAll } from '../../activity-file/hooks';

const useMictransferList = ({ subjectId, projectActivityId }) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(true);
  const { getNoteStatus } = useNoteStatusWithAll({ subjectId });
  const { getQueryStatus } = useQueryStatusWithAll({ subjectId });
  const { getFileStatus } = useFileStatusWithAll({ subjectId });

  const fetchItems = async () => {
    loading.setTrue();
    const res = await api.fetchMictransferList({
      subjectId,
      projectActivityId
    });
    if (res && res.data) {
      res.data.forEach(item => {
        item.projectActivityId = projectActivityId;
        item.activityKeyId = item.subjectMictransferId;
      });

      await getNoteStatus(res.data);
      await getQueryStatus(res.data);
      await getFileStatus(res.data);

      setItems(res.data);
    }
    loading.setFalse();
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, projectActivityId]
  );

  return [{ items, loading: loading.value, fetch: fetchItems }, setItems];
};

export default useMictransferList;
