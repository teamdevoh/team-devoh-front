import find from 'lodash/find';

const useBuild = () => {
  /**
   * After item name find, It will be return with item name and other value
   * @param {Array} source
   * @param {object} value
   * @param {string} other
   * @param {object} otherValue
   */
  const displayName = ({ source, value, other, otherValue }) => {
    if (value > 0) {
      const found = find(source, { value: value });
      if (found) {
        return `${found.text}${otherValue === value ? ` - ${other}` : ''}`;
      }
    }
    return '';
  };

  return { displayName };
};

export default useBuild;
