import { useEffect, useState } from 'react';
import api from '../api';
import { useBoolean } from 'react-hanger';
import { useNoteStatusWithAll } from '../../activity-note/hooks';
import { useQueryStatusWithAll } from '../../activity-query/hooks';
import { useFileStatusWithAll } from '../../activity-file/hooks';

const useLBCHs = ({ subjectId, projectActivityId }) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(true);
  const { getNoteStatus } = useNoteStatusWithAll({ subjectId });
  const { getQueryStatus } = useQueryStatusWithAll({ subjectId });
  const { getFileStatus } = useFileStatusWithAll({ subjectId });

  const fetchItems = async () => {
    loading.setTrue();
    const res = await api.fetchLBCHs({ subjectId, projectActivityId });
    if (res && res.data) {
      res.data.forEach(item => {
        item.projectActivityId = item.projectActivityId || projectActivityId;
        item.activityKeyId = item.subjectLbchid;
      });

      await getNoteStatus(res.data);
      await getQueryStatus(res.data);
      await getFileStatus(res.data);

      setItems(res.data);
    }
    loading.setFalse();
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, projectActivityId]
  );

  return [{ items, loading: loading.value, fetch: fetchItems }, setItems];
};

export default useLBCHs;
