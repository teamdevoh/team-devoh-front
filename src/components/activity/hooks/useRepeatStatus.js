import { useCallback } from 'react';

const useRepeatStatus = () => {
  const Refresh = useCallback(
    ({
      subjectId,
      activityKeyId,
      projectActivityId,
      hasNote,
      hasFile,
      queryStatusCodeId,
      items
    }) => {
      const updated = items.map(item => {
        if (
          Number(item.subjectId) === Number(subjectId) &&
          Number(item.activityKeyId) === Number(activityKeyId) &&
          Number(item.projectActivityId) === Number(projectActivityId)
        ) {
          return {
            ...item,
            hasNote: hasNote !== void 0 ? hasNote : item.hasNote,
            hasFile: hasFile !== void 0 ? hasFile : item.hasFile,
            queryStatusCodeId:
              queryStatusCodeId !== void 0
                ? queryStatusCodeId
                : item.queryStatusCodeId
          };
        } else {
          return { ...item };
        }
      });

      return updated;
    },
    []
  );

  return { Refresh };
};

export default useRepeatStatus;
