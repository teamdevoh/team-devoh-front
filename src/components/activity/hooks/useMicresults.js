import { useEffect, useState } from 'react';
import api from '../api';
import { useBoolean } from 'react-hanger';
import { useNoteStatusWithAll } from '../../activity-note/hooks';
import { useQueryStatusWithAll } from '../../activity-query/hooks';

const useMicresults = ({ subjectId, projectActivityId }) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(true);
  const { getNoteStatus } = useNoteStatusWithAll({ subjectId });
  const { getQueryStatus } = useQueryStatusWithAll({ subjectId });

  const fetchItems = async () => {
    loading.setTrue();
    const res = await api.fetchMicresults({ subjectId, projectActivityId });
    if (res && res.data) {
      res.data.forEach(item => {
        item.projectActivityId = projectActivityId;
        item.activityKeyId = item.subjectMicresultId;
      });

      await getNoteStatus(res.data);
      await getQueryStatus(res.data);

      setItems(res.data);
    }
    loading.setFalse();
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, projectActivityId]
  );

  return [{ items, loading: loading.value, fetch: fetchItems }, setItems];
};

export default useMicresults;
