import { useCallback } from 'react';
import { useBoolean } from '../../../hooks';
import api from '../api';
import { useFormStatus } from './';

const useDiagnosis = ({ activityKey }) => {
  const isSaving = useBoolean(false);
  const isFetching = useBoolean(false);
  const { convert } = useFormStatus();

  const fetchItem = useCallback(
    async ({ subjectId, projectActivityId, subjectTbDiagnosisId }) => {
      isFetching.setTrue();
      const res = await api.fetchDiagnosis({
        subjectId,
        projectActivityId,
        subjectTbDiagnosisId
      });

      isFetching.setFalse();

      if (res && res.data) {
        return res.data;
      }

      return {};
    },
    [isFetching.value]
  );

  const add = useCallback(
    async model => {
      model.activityStatusCodeId = convert(model.isDone);
      isSaving.setTrue();
      try {
        const res = await api.addDiagnosis({
          activityId: activityKey,
          model
        });

        if (res && res.data > 0) {
          return res.data;
        }

        return 0;
      } finally {
        isSaving.setFalse();
      }
    },
    [activityKey]
  );

  const update = useCallback(
    async model => {
      model.activityStatusCodeId = convert(
        model.isDone,
        model.activityStatusCodeId
      );
      isSaving.setTrue();
      const res = await api.updateDiagnosis({
        activityId: activityKey,
        subjectTbDiagnosisId: model.subjectTbDiagnosisId,
        model
      });
      isSaving.setFalse();
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [activityKey]
  );

  const remove = useCallback(
    async model => {
      isSaving.setTrue();
      const res = await api.removeDiagnosis({
        activityId: activityKey,
        subjectTbDiagnosisId: model.subjectTbDiagnosisId,
        model
      });
      isSaving.setFalse();
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [isSaving.value]
  );

  return {
    add,
    update,
    remove,
    fetch: fetchItem,
    isFetching: isFetching.value,
    isSaving: isSaving.value
  };
};

export default useDiagnosis;
