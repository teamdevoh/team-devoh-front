import { useCallback } from 'react';
import { code } from '../../../constants';

const useFormStatus = () => {
  const convert = useCallback((isDone, activityStatusCodeId) => {
    if (activityStatusCodeId === code.FirstSign) {
      return code.Done;
    } else {
      return isDone === false ? code.Temp : code.Done;
    }
  }, []);

  return { convert };
};

export default useFormStatus;
