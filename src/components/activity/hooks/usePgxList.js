import { useEffect, useState } from 'react';
import api from '../api';
import { useBoolean } from '../../../hooks';
import { useNoteStatusWithAll } from '../../activity-note/hooks';
import { useQueryStatusWithAll } from '../../activity-query/hooks';
import { useFileStatusWithAll } from '../../activity-file/hooks';

const usePgxList = ({ tbdrugId, projectActivityId }) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(true);
  const { getNoteStatus } = useNoteStatusWithAll({ subjectId: tbdrugId });
  const { getQueryStatus } = useQueryStatusWithAll({ subjectId: tbdrugId });
  const { getFileStatus } = useFileStatusWithAll({ subjectId: tbdrugId });

  const fetchItems = async () => {
    loading.setTrue();
    const res = await api.fetchPgxList({ tbdrugId, projectActivityId });
    if (res && res.data) {
      res.data.forEach(item => {
        item.projectActivityId = projectActivityId;
        item.activityKeyId = item.tbdrugPgxId;
        item.subjectId = item.tbdrugId;
      });

      await getNoteStatus(res.data);
      await getQueryStatus(res.data);
      await getFileStatus(res.data);

      setItems(res.data);
    }
    loading.setFalse();
  };

  useEffect(
    () => {
      fetchItems();
    },
    [tbdrugId, projectActivityId]
  );

  return [{ items, loading: loading.value, fetch: fetchItems }, setItems];
};

export default usePgxList;
