import { useCallback } from 'react';
import { useBoolean } from '../../../hooks';
import api from '../api';
import useFormStatus from './useFormStatus';

const useInterview = ({ activityKey }) => {
  const isFetching = useBoolean(false);
  const isSaving = useBoolean(false);
  const { convert } = useFormStatus();

  const fetchItem = useCallback(
    async ({ subjectId, projectActivityId }) => {
      isFetching.setTrue();
      const res = await api.fetchInterview({
        subjectId,
        projectActivityId: projectActivityId
      });
      isFetching.setFalse();

      if (res && res.data) {
        return res.data;
      }
      return {};
    },
    [isFetching.value]
  );

  const add = useCallback(
    async model => {
      model.activityStatusCodeId = convert(model.isDone);
      isSaving.setTrue();
      try {
        const res = await api.addInterview({
          activityId: activityKey,
          model
        });

        if (res && res.data > 0) {
          return res.data;
        }
        return 0;
      } finally {
        isSaving.setFalse();
      }
    },
    [activityKey]
  );

  const update = useCallback(
    async model => {
      model.activityStatusCodeId = convert(
        model.isDone,
        model.activityStatusCodeId
      );
      isSaving.setTrue();
      const res = await api.updateInterview({
        activityId: activityKey,
        subjectInterviewId: model.subjectInterviewId,
        model
      });
      isSaving.setFalse();
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [activityKey]
  );

  return {
    add,
    update,
    fetch: fetchItem,
    isFetching: isFetching.value,
    isSaving: isSaving.value
  };
};

export default useInterview;
