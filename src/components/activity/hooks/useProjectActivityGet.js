import api from '../api';
import { useState } from 'react';
import { useEffect } from 'react';

const useProjectActivityGet = ({ projectId, activityGroupId, activityId }) => {
  const [item, setItem] = useState({});
  const fetchItem = async () => {
    const res = await api.fetchProjectActivity({
      projectId,
      activityGroupId,
      activityId
    });
    if (res && res.data) {
      setItem(res.data);
    }
  };

  useEffect(
    () => {
      fetchItem();
    },
    [projectId]
  );

  return item;
};

export default useProjectActivityGet;
