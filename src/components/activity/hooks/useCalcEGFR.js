import { useCallback } from 'react';

const useCalcEGFR = ({ sex, age, creatine }) => {
  const calcScrOverK = () => {
    return sex === 'Male' ? creatine / 0.9 : creatine / 0.7;
  };

  const calcAlpha = () => {
    return sex === 'Male' ? -0.411 : -0.329;
  };

  const calcConstant = () => {
    return sex === 'Male' ? 1.0 : 1.018;
  };

  const calcRace = () => {
    return 1;
  };

  const run = useCallback(
    () => {
      const scrOverK = calcScrOverK();
      const alpha = calcAlpha();
      const constant = calcConstant();
      const race = calcRace();

      if (creatine > 0) {
        let egfr = 0;
        if (scrOverK > 1) {
          egfr =
            141 *
            Math.pow(1, alpha) *
            Math.pow(scrOverK, -1.209) *
            Math.pow(0.993, age) *
            constant *
            race;
        } else {
          egfr =
            141 *
            Math.pow(scrOverK, alpha) *
            Math.pow(1, -1.209) *
            Math.pow(0.993, age) *
            constant *
            race;
        }

        return _.round(egfr, 1);
      } else {
        return '';
      }
    },
    [sex, age, creatine]
  );

  return { run };
};

export default useCalcEGFR;
