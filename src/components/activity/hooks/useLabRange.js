import { useEffect, useState } from 'react';
import { activity } from '../../../constants';
import api from '../api';

const useLabRange = ({ subjectId, lbType }) => {
  const lbchItems = activity.LabLBCHItems;
  const lbhemItems = activity.LabLBHEMItems;

  let ranges = [];

  Object.keys(lbchItems).forEach(key => {
    ranges.push({ item: key, low: 0, high: 0, unit: '' });
  });

  Object.keys(lbhemItems).forEach(key => {
    ranges.push({ item: key, low: 0, high: 0, unit: '' });
  });

  const [item, setItem] = useState({
    ranges: ranges
  });

  const fetchItem = async () => {
    const res = await api.fetchLabRanges({ subjectId, lbType });
    if (res && res.data) {
      setItem(res.data);
    }
  };

  useEffect(
    () => {
      fetchItem();
      return () => {};
    },
    [subjectId, lbType]
  );

  return [item];
};

export default useLabRange;
