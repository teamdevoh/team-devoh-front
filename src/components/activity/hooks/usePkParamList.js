import { useEffect, useState } from 'react';
import api from '../api';
import { useBoolean } from 'react-hanger';
import { useNoteStatusWithAll } from '../../activity-note/hooks';
import { useQueryStatusWithAll } from '../../activity-query/hooks';
import { useFileStatusWithAll } from '../../activity-file/hooks';

const usePkParamList = ({ subjectId, projectActivityId }) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(true);
  const { getNoteStatus } = useNoteStatusWithAll({ subjectId });
  const { getQueryStatus } = useQueryStatusWithAll({ subjectId });
  const { getFileStatus } = useFileStatusWithAll({ subjectId });
  const [isPermit, setIsPermit] = useState(true);

  const reject = err => {
    if (err.status === 403) {
      setIsPermit(false);
    }
  };

  const fetchItems = async () => {
    loading.setTrue();
    try {
      const res = await api.fetchPkParamList({
        subjectId,
        projectActivityId,
        reject
      });
      if (res && res.data) {
        res.data.forEach(item => {
          item.projectActivityId = projectActivityId;
          item.activityKeyId = item.subjectPkparamId;
        });

        await getNoteStatus(res.data);
        await getQueryStatus(res.data);
        await getFileStatus(res.data);

        setItems(res.data);
      }
    } catch (err) {}
    loading.setFalse();
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, projectActivityId]
  );

  return [
    { items, loading: loading.value, fetch: fetchItems, isPermit },
    setItems
  ];
};

export default usePkParamList;
