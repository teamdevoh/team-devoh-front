import { useNoteStatus } from '../../activity-note/hooks';
import { useQueryStatus } from '../../activity-query/hooks';
import { useFileStatus } from '../../activity-file/hooks';

const useStatus = ({
  subjectId,
  activityKeyId,
  projectActivityId,
  setRepeat
}) => {
  const key = activityKeyId === '' ? 0 : activityKeyId;

  const [{ hasNote }, setHasNote] = useNoteStatus({
    subjectId,
    activityKeyId: key,
    projectActivityId
  });

  const [{ query }, setQueryStatus] = useQueryStatus({
    subjectId,
    activityKeyId: key,
    projectActivityId
  });

  const [{ hasFile }, setHasFile] = useFileStatus({
    subjectId,
    activityKeyId: key,
    projectActivityId
  });

  return {
    note: {
      hasNote,
      setHasNote
    },
    qna: {
      query,
      setQueryStatus
    },
    file: {
      hasFile,
      setHasFile
    }
  };
};

export default useStatus;
