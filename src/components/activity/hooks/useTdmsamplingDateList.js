import { useEffect, useState } from 'react';
import api from '../api';

const useTdmsamplingDateList = ({ subjectId, subjectVisitId }) => {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchTdmsamplingListOfSubjectVisit({
      subjectId,
      subjectVisitId
    });
    if (res && res.data) {
      const newItem = res.data.map((item, index) => {
        return {
          text: item.sampleTime,
          value: item.subjectTdmsamplingId,
          data: { ...item }
        };
      });
      setItems(newItem);
    }

    setLoading(false);
  };

  useEffect(
    () => {
      if (subjectId > 0 && subjectVisitId > 0) {
        fetchItems();
      }
    },
    [subjectId, subjectVisitId]
  );

  return [{ items, loading }, setItems];
};

export default useTdmsamplingDateList;
