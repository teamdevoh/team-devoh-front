import { useCallback, useEffect, useState } from 'react';
import round from 'lodash/round';

const useBMI = ({ weight, height }) => {
  const [bmi, setBMI] = useState(0);

  const calcBMI = useCallback(
    () => {
      const rounded = round((weight / (height * height)) * 10000, 2);
      setBMI(rounded);
    },
    [weight, height]
  );

  useEffect(
    () => {
      if (height > 0 && weight > 0) {
        calcBMI();
      }
    },
    [weight, height]
  );

  return [bmi, setBMI];
};

export default useBMI;
