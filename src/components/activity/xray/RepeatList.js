import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [yesorNoorUKCodes] = useCodes({
      codeGroupId: codeGroup.YesorNoorUK
    });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['xrayDtc'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="two">
              {t('activity.xray.xrayDtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('activity.xray.xrayResult')}</Table.HeaderCell>
            <Table.HeaderCell width="two">
              {t('activity.xray.xrayCavityCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const yesorNoorUK = build.displayName({
              source: yesorNoorUKCodes,
              value: item.xrayCavityCodeId
            });

            return (
              <StyledTableRow
                key={item.subjectXrayId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectXrayId}
                    text={item.xrayDtc}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={item.xrayResult}>
                  {item.xrayResult}
                </Table.Cell>
                <Table.Cell title={yesorNoorUK}>{yesorNoorUK}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectXrayId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectXray',
                      prefixLocale: 'activity.xray',
                      skip: ['subjectXrayId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={4}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
