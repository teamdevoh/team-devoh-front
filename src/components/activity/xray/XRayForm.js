import React from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import { YesorNoorUK, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import DateInput from '../../input/DateInput';

const XRayForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.xray.xrayDtc')}</label>
        <DateInput
          name="xrayDtc"
          value={model.xrayDtc}
          placeholder="ukuk-uk-uk"
          options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
          onChange={onChange}
        />
        {validator.message(
          t('activity.xray.xrayDtc'),
          model.xrayDtc,
          'required'
        )}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.xray.xrayResult')}</label>
        <TextArea
          name="xrayResult"
          value={model.xrayResult}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group inline>
        <label>{t('activity.xray.xrayCavityCodeId')}</label>
        <YesorNoorUK
          name="xrayCavityCodeId"
          value={model.xrayCavityCodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.subjectXrayId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.xray.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default XRayForm;
