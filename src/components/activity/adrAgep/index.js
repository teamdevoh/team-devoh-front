import AdrAgepContainer from './AdrAgepContainer';
import AdrAgepForm from './AdrAgepForm';

// const AdrAgepContainer = asyncComponent(() => import('./AdrAgepContainer'));
// const AdrAgepForm = asyncComponent(() => import('./AdrAgepForm'));

export { AdrAgepContainer, AdrAgepForm };
