import React from 'react';
import { Form } from 'semantic-ui-react';
import { YesorNo, Adragep, SelectReason } from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';

const AdrAgepForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrAgep.adragepPusCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <Adragep
            name="adragepPusCodeId"
            value={model.adragepPusCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrAgep.adragepEryCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <Adragep
            name="adragepEryCodeId"
            value={model.adragepEryCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrAgep.adragepDistCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <Adragep
            name="adragepDistCodeId"
            value={model.adragepDistCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrAgep.adragepMuco')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="adragepMuco"
            value={model.adragepMuco}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrAgep.adragepOnset')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="adragepOnset"
            value={model.adragepOnset}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrAgep.adragepResol')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="adragepResol"
            value={model.adragepResol}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrAgep.adragepFever')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="adragepFever"
            value={model.adragepFever}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      {model.subjectAdrAgepid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.adrAgep.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default AdrAgepForm;
