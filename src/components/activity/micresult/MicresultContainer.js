import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '..';
import { AuthorInfo } from '../../collection';
import { MicresultForm, RepeatList } from '.';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { usePublishStatus, useMoveToNextStep } from '../../collection/hooks';
import { useMicresults, useMicresult, useActionStatus } from '../hooks';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { code } from '../../../constants';

const MicresultContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'subjectMicresultId';

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };
  const { submitted, isNext } = useActionStatus(updateRepeat);
  const publish = usePublishStatus({
    projectActivityId
  });
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const { add, update, remove, fetch, isSaving, isFetching } = useMicresult({
    activityKey
  });
  const [selectedId, setSelectedId] = useState(0);
  const [curSubjectTbdrugId, setCurSubjectTbdrugId] = useState(0);
  const [repeat, setRepeat] = useMicresults({ subjectId, projectActivityId });
  const t = useFormatMessage();

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    subjectMicresultId: '',
    subjectId: subjectId,
    subjectVisitId: 0,
    subjectTbdrugId: null,
    micstartdtc: null,
    micenddtc: null,
    exdrugId: null,
    exdrugoth: null,
    testmic: null,
    typemic: null,
    modified: null,
    author: '',
    isChanged: false,
    reason: ''
  };

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const { updateRepeatByKey } = useRepeat(key);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0 || curSubjectTbdrugId > 0) {
        fetchItem();
      }
    },
    [selectedId, curSubjectTbdrugId]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      subjectId,
      projectActivityId,
      subjectMicresultId: model[key],
      subjectTbdrugId: curSubjectTbdrugId
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            let updated = [...repeat.items];
            const filtered = updated.filter((item, index, arr) => {
              // 복용중이던 약물 update
              const isSubTbdrug =
                item.subjectTbdrugId !== null &&
                item.subjectTbdrugId === model.subjectTbdrugId;
              if (isSubTbdrug) {
                updated[index] = {
                  ...item,
                  ...model
                };
              }

              return isSubTbdrug;
            });

            if (filtered.length === 0) {
              updated = repeat.items.concat(model);
            }

            setRepeat(updated);
            const pickedActivity = publish.pickActivityValueBy(updated);
            publish.activityStatus(pickedActivity);

            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    ({ id, subjectTbdrugId }) => {
      setModel({ ...model, [key]: id });
      setSelectedId(id);
      setCurSubjectTbdrugId(subjectTbdrugId);
    },
    [model, selectedId, curSubjectTbdrugId]
  );

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        subjectMicresultId: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
      setCurSubjectTbdrugId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(
    item =>
      `${item[key]}` === `${selectedId}` &&
      `${item.subjectTbdrugId}` === `${curSubjectTbdrugId}`,
    [selectedId, curSubjectTbdrugId]
  );

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text={t('activity.micresult.title')} />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'SubjectMicresult',
                prefixLocale: 'activity.micresult',
                skip: [key, 'subjectTbdrugId']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <MicresultForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </MicresultForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default MicresultContainer;
