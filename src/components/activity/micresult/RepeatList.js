import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import helpers from '../../../helpers';
import { code, format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [drug] = useTbdrugList();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['resDtc'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              {t('activity.micresult.exdrugId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="1">
              {t('activity.common.exdose')}
            </Table.HeaderCell>
            <Table.HeaderCell width="3">
              {t('activity.common.exPeriod')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.micresult.micstartdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.micresult.micenddtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.micresult.testmic')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.micresult.typemic')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const {
              activityStatusCodeId,
              exdose,
              exdrugId,
              exdrugoth,
              exendtc,
              exstdtc,
              micenddtc,
              micstartdtc,
              subjectMicresultId,
              subjectTbdrugId,
              testmic,
              typemic
            } = item;

            const drugName = build.displayName({
              source: drug.items,
              value: exdrugId,
              other: exdrugoth,
              otherValue: code.TBDrugOther
            });

            const dose = exdose ? `${exdose} mg` : '';

            const exPeriod =
              exstdtc || exendtc ? `${exstdtc || ''} ~ ${exendtc || ''}` : '';

            const test = testmic ? `${testmic} μg/ml` : '';
            const type = typemic ? `${typemic} μg/ml` : '';

            const micstartDate = helpers.util.dateformat(
              micstartdtc,
              format.YYYY_MM_DD
            );
            const micendDate = helpers.util.dateformat(
              micenddtc,
              format.YYYY_MM_DD
            );

            return (
              <StyledTableRow
                key={`${subjectMicresultId}_${subjectTbdrugId}`}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={{
                      id: subjectMicresultId,
                      subjectTbdrugId: subjectTbdrugId
                    }}
                    text={drugName}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={dose}>{dose}</Table.Cell>
                <Table.Cell title={exPeriod}>{exPeriod}</Table.Cell>
                <Table.Cell title={micstartDate}>{micstartDate}</Table.Cell>
                <Table.Cell title={micendDate}>{micendDate}</Table.Cell>
                <Table.Cell title={test}>{test}</Table.Cell>
                <Table.Cell title={type}>{type}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={subjectMicresultId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectMicresult',
                      prefixLocale: 'activity.micresult',
                      skip: ['subjectMicresultId', 'subjectTbdrugId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={8}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
