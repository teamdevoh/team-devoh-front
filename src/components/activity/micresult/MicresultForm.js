import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { TbdrugList } from '../../collection-tbdrug/Selector';
import { SelectReason } from '../Selector';
import { DatePicker } from '../../datepicker';
import { code } from '../../../constants';

const MicresultForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.micresult.exdrugId')}</label>
        <TbdrugList
          name="exdrugId"
          value={model.exdrugId}
          onChange={onChange}
          disabled={model.subjectTbdrugId ? true : false}
        />
        {validator.message(
          t('activity.micresult.exdrugId'),
          model.exdrugId,
          'required'
        )}
      </Form.Field>
      {model.exdrugId === code.TBDrugOther && (
        <Form.Field required>
          <label>{t('activity.micresult.exdrugoth')}</label>
          <Input
            name="exdrugoth"
            value={model.exdrugoth || ''}
            onChange={onChange}
          />
          {validator.message(
            t('activity.micresult.exdrugoth'),
            model.exdrugoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Field>
        <label>{t('activity.micresult.micstartdtc')}</label>
        <DatePicker
          name="micstartdtc"
          value={model.micstartdtc}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.micresult.micenddtc')}</label>
        <DatePicker
          name="micenddtc"
          value={model.micenddtc}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.micresult.testmic')}</label>
        <Input
          name="testmic"
          value={model.testmic || ''}
          onChange={onChange}
          label={{ basic: true, content: 'μg/ml' }}
          labelPosition="right"
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.micresult.typemic')}</label>
        <Input
          name="typemic"
          value={model.typemic || ''}
          onChange={onChange}
          label={{ basic: true, content: 'μg/ml' }}
          labelPosition="right"
        />
      </Form.Field>
      {model.subjectMicresultId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.micresult.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default MicresultForm;
