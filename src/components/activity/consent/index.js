import ConsentContainer from './ConsentContainer';
import ConsentForm from './ConsentForm';

export { ConsentContainer, ConsentForm };
