import React from 'react';
import { Form, Input, Dropdown } from 'semantic-ui-react';
import {
  MyVisit,
  Preserve,
  Secondary,
  Identification,
  SelectReason
} from '../Selector';
import { DatePicker } from '../../datepicker';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';
import { code } from '../../../constants';

const ConsentForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
          visitTypeCode={code.SubjectAgree}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Field required>
        <label>{t('activity.consent.consdtc2')}</label>
        <DatePicker
          name="consdtc2"
          onChange={onChange}
          value={model.consdtc2}
        />
        {validator.message(
          t('activity.consent.consdtc2'),
          model.consdtc2,
          'required'
        )}
      </Form.Field>
      <Form.Group inline>
        <label>{t('activity.consent.preservecodeId')}</label>
        <Preserve
          name="preservecodeId"
          value={model.preservecodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.preservecodeId === code.Temporary && (
        <Form.Field required>
          <label>{t('activity.consent.preservedtc')}</label>
          <Input
            name="preservedtc"
            value={model.preservedtc || ''}
            onChange={onChange}
            label={
              <Dropdown
                name="preservedtcType"
                value={model.preservedtcType}
                options={[
                  { text: 'Years', value: 'Years' },
                  { text: 'Months', value: 'Months' }
                ]}
                onChange={onChange}
              />
            }
            labelPosition="right"
          />
          {validator.message(
            t('activity.consent.preservedtc'),
            model.preservedtc,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group grouped>
        <label>{t('activity.consent.secondarycodeId')}</label>
        <Secondary
          name="secondarycodeId"
          value={model.secondarycodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.consent.identificationcodeId')}</label>
        <Identification
          name="identificationcodeId"
          value={model.identificationcodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.subjectConsentId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.consent.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default ConsentForm;
