import React, { Fragment, useCallback } from 'react';
import { SaveButton, RemoveButton } from '../button';
import { useFormatMessage } from '../../hooks';
import Review from './Review';
import { code, role } from '../../constants';
import { notification } from '../modal';

const ActionBar = ({
  statusCode,
  subjectId,
  projectActivityId,
  activityKeyId,
  loading,
  isDone,
  onClick,
  onRemoveClick,
  hasRemoveButton = false
}) => {
  const t = useFormatMessage();

  const handleSaveClick = useCallback(
    event => {
      onClick(event, {
        name: 'save',
        activityStatusCodeId: isDone === true ? code.Done : code.Temp,
        projectActivityId
      });
    },
    [onClick]
  );

  const handleSaveNextClick = useCallback(
    event => {
      onClick(event, {
        name: 'next',
        activityStatusCodeId: isDone === true ? code.Done : code.Temp,
        projectActivityId
      });
    },
    [onClick]
  );

  const handleRemoveClick = () => {
    notification.confirm({
      title: t('delete.message'),
      content: '',
      onOK: onRemoveClick
    });
  };

  const shouldReview =
    statusCode === code.Done ||
    statusCode === code.FirstSign ||
    statusCode === code.SecondSign;

  return (
    <Fragment>
      {(statusCode === 0 ||
        statusCode === code.Temp ||
        statusCode === code.Done ||
        statusCode === code.FirstSign) && (
        <Fragment>
          <SaveButton
            allowedRole={role.Activity_Edit}
            name={t('common.save')}
            loading={loading}
            onClick={handleSaveClick}
          />
          <SaveButton
            allowedRole={role.Activity_Edit}
            name={t('common.savennext')}
            loading={loading}
            onClick={handleSaveNextClick}
          />
        </Fragment>
      )}
      {hasRemoveButton &&
        activityKeyId > 0 &&
        (statusCode === 0 ||
          statusCode === code.None ||
          statusCode === code.Temp ||
          statusCode === code.Done) && (
          <RemoveButton
            allowedRole={role.Activity_Edit}
            name={t('common.remove')}
            onClick={handleRemoveClick}
            disabled={loading}
          />
        )}
      {shouldReview && (
        <Review
          subjectId={subjectId}
          activityKeyId={activityKeyId}
          projectActivityId={projectActivityId}
          activityStatusCodeId={statusCode}
          onClick={onClick}
        />
      )}
    </Fragment>
  );
};

export default ActionBar;
