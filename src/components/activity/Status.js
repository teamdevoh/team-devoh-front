import React, { Fragment, memo, useCallback, useEffect, useState } from 'react';
import {
  IconQueryEmpty,
  IconQueryOpen,
  IconQueryAnswer,
  IconQueryClose,
  IconTemp,
  IconDone,
  IconNote,
  IconAudit,
  IconFile
} from '../collection/Status';
import { useBoolean } from '../../hooks';
import { NoteContainer } from '../activity-note';
import { Modal } from '../modal';
import { useSelector, useDispatch } from 'react-redux';
import { activity } from '../../states';
import { QueryContainer } from '../activity-query';
import { useQueryGroup } from '../activity-query/hooks';
import ActivityAuditContainer from '../activity-audit/ActivityAuditContainer';
import ReviewContainer from '../activity-review/ReviewContainer';
import FileContainer from '../activity-file/FileContainer';
import { useReviews } from '../activity-review/hooks';
import { Icon } from 'semantic-ui-react';
import { code } from '../../constants';

const QueryStatus = memo(
  ({
    subjectId,
    activityKeyId,
    projectActivityId,
    queryStatusCodeId,
    onClick,
    onQueryModalClosed
  }) => {
    const isModal = useBoolean(false);
    const dispatch = useDispatch();
    const queries = useSelector(activity.selectors.queries);
    const queryGroup = useQueryGroup(queries);

    const handleIconClick = useCallback(
      () => {
        isModal.setTrue();
      },
      [isModal.value, queries]
    );

    const handleOKClick = useCallback(
      () => {
        isModal.setFalse();
        if (onQueryModalClosed) {
          onQueryModalClosed({
            subjectId,
            activityKeyId,
            projectActivityId,
            queryStatusCodeId: queryGroup.getPublishCode()
          });
          dispatch(activity.actions.fetchQueries([]));
        }
      },
      [isModal.value, queryGroup]
    );

    return (
      <Fragment>
        {activityKeyId > 0 && (
          <Fragment>
            {(queryStatusCodeId === void 0 || queryStatusCodeId === 0) && (
              <IconQueryEmpty onClick={handleIconClick} />
            )}
            {queryStatusCodeId === code.QueryOpen && (
              <IconQueryOpen onClick={handleIconClick} />
            )}
            {queryStatusCodeId === code.QueryAnswer && (
              <IconQueryAnswer onClick={handleIconClick} />
            )}
            {queryStatusCodeId === code.QueryClose && (
              <IconQueryClose onClick={handleIconClick} />
            )}

            {isModal.value === true && (
              <Modal
                open={isModal.value}
                onOK={handleOKClick}
                title="Query"
                content={
                  <QueryContainer
                    subjectId={subjectId}
                    activityKeyId={activityKeyId}
                    projectActivityId={projectActivityId}
                  />
                }
              />
            )}
          </Fragment>
        )}
      </Fragment>
    );
  }
);

const ActivityStatus = memo(
  ({
    status,
    subjectId,
    activityKeyId,
    projectActivityId,
    onChangeStatusCb,
    projectId
  }) => {
    const [isSkippedSecond, setIsSkippedSecond] = useState(false);
    const [{ items }] = useReviews({
      subjectId,
      projectActivityId,
      activityKeyId
    });

    let rating = 0;
    if (status === code.FirstSign) {
      rating = 1;
    }
    if (status === code.SecondSign) {
      rating = 2;
    }
    if (status === code.ThirdSign) {
      rating = 3;
    }

    const isModal = useBoolean(false);

    const handleIconClick = useCallback(
      () => {
        isModal.toggle();
      },
      [isModal.value]
    );

    const handleOKClick = useCallback(
      () => {
        isModal.setFalse();
      },
      [isModal.value]
    );

    const checkSkipSecondSign = () => {
      const ordered = _.orderBy(items, 'activityReviewHistoryId', 'desc');

      const thirdSignIndex = _.findIndex(ordered, {
        activityStatusCodeId: status
      });
      const prevItem = ordered[thirdSignIndex + 1];
      const skippedSecondSign =
        prevItem.activityStatusCodeId !== code.SecondSign;

      setIsSkippedSecond(skippedSecondSign);
    };

    useEffect(
      () => {
        if (items.length > 0 && status === code.ThirdSign) {
          checkSkipSecondSign();
        }
      },
      [items, status]
    );

    return (
      <Fragment>
        {status === code.Temp && <IconTemp onClick={handleIconClick} />}
        {status === code.Done && <IconDone onClick={handleIconClick} />}
        {_.times(rating, index => {
          if (isSkippedSecond && index === 1) {
            return (
              <Icon key={index} name="star outline" onClick={handleIconClick} />
            );
          } else {
            return (
              <Icon
                key={index}
                name="star"
                color="yellow"
                onClick={handleIconClick}
              />
            );
          }
        })}
        {isModal.value === true && (
          <Modal
            open={isModal.value}
            onOK={handleOKClick}
            title="Review History"
            content={
              <ReviewContainer
                subjectId={subjectId}
                activityKeyId={activityKeyId}
                projectActivityId={projectActivityId}
                status={status}
                onChangeStatusCb={onChangeStatusCb}
                projectId={projectId}
              />
            }
          />
        )}
      </Fragment>
    );
  }
);

const NoteStatus = memo(
  ({
    subjectId,
    activityKeyId,
    projectActivityId,
    hasNote,
    onClick,
    onNoteModalClosed
  }) => {
    const isModal = useBoolean(false);
    const dispatch = useDispatch();
    const notes = useSelector(activity.selectors.notes);

    const handleIconClick = useCallback(
      () => {
        isModal.toggle();
      },
      [isModal.value]
    );

    const handleOKClick = useCallback(
      () => {
        isModal.setFalse();
        if (onNoteModalClosed) {
          onNoteModalClosed({
            subjectId,
            activityKeyId,
            projectActivityId,
            hasNote: notes.length > 0
          });
          dispatch(activity.actions.fetchNotes([]));
        }
      },
      [isModal.value, notes]
    );

    const options = {
      ...(hasNote ? {} : { color: 'grey' })
    };

    return (
      <Fragment>
        {activityKeyId > 0 && (
          <IconNote {...options} onClick={handleIconClick} />
        )}
        {isModal.value === true && (
          <Modal
            open={isModal.value}
            onOK={handleOKClick}
            title="Note"
            content={
              <NoteContainer
                subjectId={subjectId}
                activityKeyId={activityKeyId}
                projectActivityId={projectActivityId}
              />
            }
          />
        )}
      </Fragment>
    );
  }
);

const AuditStatus = memo(
  ({ activityKeyId, entityName, prefixLocale, skip }) => {
    const isModal = useBoolean(false);

    const handleIconClick = useCallback(
      () => {
        isModal.toggle();
      },
      [isModal.value]
    );

    const handleOKClick = useCallback(
      () => {
        isModal.setFalse();
      },
      [isModal.value]
    );

    return (
      <Fragment>
        {activityKeyId > 0 && <IconAudit onClick={handleIconClick} />}
        {isModal.value === true && (
          <Modal
            size="fullscreen"
            open={isModal.value}
            onOK={handleOKClick}
            title="Audit Trail"
            content={
              <ActivityAuditContainer
                activityKeyId={activityKeyId}
                entityName={entityName}
                prefixLocale={prefixLocale}
                skip={skip}
              />
            }
          />
        )}
      </Fragment>
    );
  }
);

const FileStatus = memo(
  ({
    subjectId,
    activityKeyId,
    projectActivityId,
    hasFile,
    onClick,
    onModalClosed
  }) => {
    const isModal = useBoolean(false);
    const hasFileOfRepeat = useSelector(activity.selectors.hasFile);

    const handleIconClick = useCallback(
      () => {
        isModal.toggle();
      },
      [isModal.value]
    );

    const handleOKClick = useCallback(
      () => {
        isModal.setFalse();
        if (onModalClosed) {
          onModalClosed({
            subjectId,
            activityKeyId,
            projectActivityId,
            hasFile: hasFileOfRepeat
          });
        }
      },
      [isModal.value, hasFileOfRepeat]
    );

    const options = {
      ...(hasFile ? {} : { color: 'grey' })
    };

    return (
      <Fragment>
        {activityKeyId > 0 && (
          <IconFile {...options} onClick={handleIconClick} />
        )}
        {isModal.value === true && (
          <Modal
            open={isModal.value}
            onOK={handleOKClick}
            title="File"
            content={
              <FileContainer
                subjectId={subjectId}
                activityKeyId={activityKeyId}
                projectActivityId={projectActivityId}
              />
            }
          />
        )}
      </Fragment>
    );
  }
);

export { QueryStatus, ActivityStatus, NoteStatus, AuditStatus, FileStatus };
