import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import helpers from '../../../helpers';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, codeGroup, format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [specCodes] = useCodes({ codeGroupId: codeGroup.AFBSpecCode });
    const [rifCodes] = useCodes({ codeGroupId: codeGroup.XpertRIFCode });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['lparesidtc'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              {t('activity.rapid.lparesidtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.rapid.lpaspecCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.rapid.inhresCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('activity.rapid.rifres')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.rapid.katgres')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.rapid.inhares')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.rapid.ahpcres')}</Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const specimen = build.displayName({
              source: specCodes,
              value: item.lpaspecCodeId,
              other: item.lpaspecoth,
              otherValue: code.AFBSpecOther
            });
            const rifInhres = build.displayName({
              source: rifCodes,
              value: item.inhresCodeId
            });
            const rifres = build.displayName({
              source: rifCodes,
              value: item.rifres
            });
            const rifKatgres = build.displayName({
              source: rifCodes,
              value: item.katgres
            });
            const rifInhares = build.displayName({
              source: rifCodes,
              value: item.inhares
            });
            const rifAhpcres = build.displayName({
              source: rifCodes,
              value: item.ahpcres
            });

            return (
              <StyledTableRow
                key={item.subjectRapidTestId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectRapidTestId}
                    text={helpers.util.dateformat(
                      item.lparesidtc,
                      format.YYYY_MM_DD
                    )}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={specimen}>{specimen}</Table.Cell>
                <Table.Cell title={rifInhres}>{rifInhres}</Table.Cell>
                <Table.Cell title={rifres}>{rifres}</Table.Cell>
                <Table.Cell title={rifKatgres}>{rifKatgres}</Table.Cell>
                <Table.Cell title={rifInhares}>{rifInhares}</Table.Cell>
                <Table.Cell title={rifAhpcres}>{rifAhpcres}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectRapidTestId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectRapidTest',
                      prefixLocale: 'activity.rapid',
                      skip: ['subjectRapidTestId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={8}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
