import React from 'react';
import { Form, Input, TextArea } from 'semantic-ui-react';
import { AFBSpec, XpertRIF, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DatePicker } from '../../datepicker';
import { code } from '../../../constants';

const RapidForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>
          {t('activity.rapid.lparesidtc')}
          {t('activity.rapid.lparesidtc.des')}
        </label>
        <DatePicker
          name="lparesidtc"
          value={model.lparesidtc}
          onChange={onChange}
        />
        {validator.message(
          t('activity.rapid.lparesidtc'),
          model.lparesidtc,
          'required'
        )}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.rapid.lpaspecCodeId')}</label>
        <AFBSpec
          name="lpaspecCodeId"
          value={model.lpaspecCodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.lpaspecCodeId === code.AFBSpecOther && (
        <Form.Field required>
          <label>
            {t('activity.rapid.lpaspecoth')}
            {t('activity.rapid.lpaspecoth.des')}
          </label>
          <Input
            name="lpaspecoth"
            value={model.lpaspecoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.rapid.lpaspecoth'),
            model.lpaspecoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.rapid.inhresCodeId')}</label>
          <XpertRIF
            name="inhresCodeId"
            value={model.inhresCodeId}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.rapid.rifres')}</label>
          <XpertRIF name="rifres" value={model.rifres} onChange={onChange} />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.rapid.katgres')}</label>
          <XpertRIF name="katgres" value={model.katgres} onChange={onChange} />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.rapid.inhares')}</label>
          <XpertRIF name="inhares" value={model.inhares} onChange={onChange} />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.rapid.ahpcres')}</label>
          <XpertRIF name="ahpcres" value={model.ahpcres} onChange={onChange} />
        </Form.Field>
      </Form.Group>
      {Number(projectId) === 147 && (
        <Form.Field>
          <label>{t('activity.rapid.comments')}</label>
          <TextArea
            name="comments"
            value={model.comments || ''}
            onChange={onChange}
          />
        </Form.Field>
      )}
      {model.subjectRapidTestId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.rapid.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default RapidForm;
