import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { YesorNo } from '../Selector';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { SelectReason } from '../Selector';
import { DatePicker } from '../../datepicker';
import { code } from '../../../constants';

const MictransferForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field widths="equal">
        <label>{t('activity.mictransfer.micspecdtc')}</label>
        <DatePicker
          name="micspecdtc"
          value={model.micspecdtc}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field widths="equal">
        <label>{t('activity.mictransfer.strainreqdtc')}</label>
        <DatePicker
          name="strainreqdtc"
          value={model.strainreqdtc}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field widths="equal">
        <label>{t('activity.mictransfer.straindelidtc')}</label>
        <DatePicker
          name="straindelidtc"
          value={model.straindelidtc}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.mictransfer.contam')}</label>
        <YesorNo name="contam" value={model.contam} onChange={onChange} />
      </Form.Group>
      <Form.Field widths="equal">
        <label>{t('activity.mictransfer.strainlabel')}</label>
        <Input
          name="strainlabel"
          value={model.strainlabel || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field widths="equal">
        <label>{t('activity.mictransfer.subculstor')}</label>
        <Input
          name="subculstor"
          value={model.subculstor || ''}
          onChange={onChange}
        />
      </Form.Field>
      {model.macroscopyCodeId === code.MacroscopyOther && (
        <Form.Field required>
          <label>{t('activity.mictransfer.macroscopyOth')}</label>
          <Input
            name="macroscopyOth"
            value={model.macroscopyOth || ''}
            onChange={onChange}
          />
          {validator.message(
            t('activity.mictransfer.macroscopyOth'),
            model.macroscopyOth,
            'required'
          )}
        </Form.Field>
      )}
      {model.subjectMictransferId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.mictransfer.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default MictransferForm;
