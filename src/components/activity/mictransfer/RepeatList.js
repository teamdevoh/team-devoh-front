import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import helpers from '../../../helpers';
import { format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              {t('activity.mictransfer.micspecdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.mictransfer.strainreqdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.mictransfer.straindelidtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.mictransfer.contam')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.mictransfer.strainlabel')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.mictransfer.subculstor')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {items.map(item => {
            const {
              activityStatusCodeId,
              contam,
              micspecdtc = '',
              straindelidtc,
              strainlabel,
              strainreqdtc,
              subculstor,
              subjectMictransferId
            } = item;

            const micspec = !!micspecdtc
              ? helpers.util.dateformat(micspecdtc, format.YYYY_MM_DD)
              : 'unknown';
            const contamination = contam ? 'Yes' : 'No';

            const strainreqDate = helpers.util.dateformat(
              strainreqdtc,
              format.YYYY_MM_DD
            );
            const straindeliDate = helpers.util.dateformat(
              straindelidtc,
              format.YYYY_MM_DD
            );

            return (
              <StyledTableRow
                key={subjectMictransferId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={subjectMictransferId}
                    text={micspec}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={strainreqDate}>{strainreqDate}</Table.Cell>
                <Table.Cell title={straindeliDate}>{straindeliDate}</Table.Cell>
                <Table.Cell title={contamination}>{contamination}</Table.Cell>
                <Table.Cell title={strainlabel}>{strainlabel}</Table.Cell>
                <Table.Cell title={subculstor}>{subculstor}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={subjectMictransferId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectMictransfer',
                      prefixLocale: 'activity.mictransfer',
                      skip: ['subjectMictransferId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={7}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
