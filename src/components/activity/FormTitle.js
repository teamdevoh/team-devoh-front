import React, { Fragment } from 'react';
import { Header, Divider } from 'semantic-ui-react';

const FormTitle = ({ text }) => {
  return (
    <Fragment>
      <Header color="blue" as="h3">
        {text}
      </Header>
      <Divider style={{ background: '#2185d0' }} clearing fitted />
    </Fragment>
  );
};

export default FormTitle;
