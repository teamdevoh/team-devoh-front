import React, { useCallback } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { useBuild } from '../hooks';
import { codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = ({
  subjectId,
  updateRepeat,
  projectActivityId,
  items,
  loading,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  refInfos,
  ethnicCodes,
  isSeleted
}) => {
  const t = useFormatMessage();
  const build = useBuild();
  const [clinicalFeaturesCodes] = useCodes({
    codeGroupId: codeGroup.ClinicalFeaturesCode
  });
  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return 'unknown';
    },
    [refInfos]
  );

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  return (
    <Table compact="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Significant factors</Table.HeaderCell>
          <Table.HeaderCell>Condition</Table.HeaderCell>
          <Table.HeaderCell>Effect on</Table.HeaderCell>
          <Table.HeaderCell>Clinical features</Table.HeaderCell>
          <Table.HeaderCell>Comments</Table.HeaderCell>
          <Table.HeaderCell>Reference1</Table.HeaderCell>
          <Table.HeaderCell>Reference2</Table.HeaderCell>
          <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map(item => {
          const {
            tbdrugFactorsId,
            significantfactors,
            condition,
            effectOn,
            clinicalFeaturesCodeId,
            clinicalFeatures,
            comments,
            referenceId1,
            referenceId2,
            activityStatusCodeId
          } = item;

          const clinicalFeaturesCode = build.displayName({
            source: clinicalFeaturesCodes,
            value: clinicalFeaturesCodeId
          });

          return (
            <StyledTableRow key={tbdrugFactorsId} selected={isSeleted(item)}>
              <Table.Cell>
                <ActivityItemAnchor
                  id={tbdrugFactorsId}
                  text={!!significantfactors ? significantfactors : 'unknown'}
                  onClick={onClick}
                />
              </Table.Cell>
              <Table.Cell>{condition}</Table.Cell>
              <Table.Cell>{effectOn}</Table.Cell>
              <Table.Cell>{`${clinicalFeaturesCode}${
                clinicalFeatures ? `(${clinicalFeatures})` : ''
              }`}</Table.Cell>
              <Table.Cell>{comments}</Table.Cell>
              <Table.Cell>{getRefTitle(referenceId1)}</Table.Cell>
              <Table.Cell>{getRefTitle(referenceId2)}</Table.Cell>
              <Table.Cell>
                <StatusWrapper
                  subjectId={subjectId}
                  onChangeStatusCb={handleChangeStatus(item)}
                  activityKeyId={tbdrugFactorsId}
                  projectActivityId={projectActivityId}
                  note={{
                    value: item.hasNote,
                    closed: onNoteModalClosed
                  }}
                  query={{
                    value: item.queryStatusCodeId,
                    closed: onQueryModalClosed
                  }}
                  file={{
                    value: item.hasFile,
                    closed: onFileModalClosed
                  }}
                  activity={{
                    value: activityStatusCodeId,
                    closed: () => {}
                  }}
                  audit={{
                    entityName: 'TbdrugFactors',
                    prefixLocale: 'activity.tbdrugFactors',
                    skip: ['tbdrugFactorsId', 'tbdrugId']
                  }}
                  repeatItems={items}
                />
              </Table.Cell>
            </StyledTableRow>
          );
        })}
        {loading && (
          <EmptyRowsViewForSemantic
            loading={true}
            columnLength={8}
            size="large"
          />
        )}
      </Table.Body>
    </Table>
  );
};

export default RepeatList;
