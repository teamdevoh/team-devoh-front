import React, { useCallback } from 'react';
import { Form, Input, TextArea, Dropdown } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '../';
import { useSubmit, useFormatMessage, useCodes } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import { SelectReason } from '../Selector';
import { codeGroup } from '../../../constants';

const FactorsAffectingForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  refInfos
}) => {
  const t = useFormatMessage();
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  const [clinicalFeaturesCodes] = useCodes({
    codeGroupId: codeGroup.ClinicalFeaturesCode
  });

  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return '';
    },
    [refInfos]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <label>Significant factors</label>
        <Input
          name="significantfactors"
          value={model.significantfactors || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Condition</label>
        <Input
          name="condition"
          value={model.condition || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Effect on</label>
        <Input
          name="effectOn"
          value={model.effectOn || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group>
        <Form.Field width="3" inline>
          <label>Clinical features</label>
          <Dropdown
            name="clinicalFeaturesCodeId"
            search
            fluid
            selection
            value={model.clinicalFeaturesCodeId}
            onChange={onChange}
            options={[
              {
                text: t('common.notSelected'),
                value: ''
              },
              ...clinicalFeaturesCodes
            ]}
          />
        </Form.Field>
        <Form.Field width="13" inline>
          <label> </label>
          <Input
            name="clinicalFeatures"
            value={model.clinicalFeatures || ''}
            onChange={onChange}
            placeholder="Clinical features other"
          />
        </Form.Field>
      </Form.Group>
      <Form.Field>
        <label>Comments</label>
        <TextArea
          name="comments"
          value={model.comments || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          label="Reference1"
          searchValue={getRefTitle(model.referenceId1)}
          name="referenceId1"
          value={model.referenceId1}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          label="Reference2"
          searchValue={getRefTitle(model.referenceId2)}
          name="referenceId2"
          value={model.referenceId2}
          onChange={onChange}
        />
      </Form.Field>
      {model.tbdrugFactorsId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="Factors affecting on PK"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default FactorsAffectingForm;
