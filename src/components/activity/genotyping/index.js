import GenotypingContainer from './GenotypingContainer';
import GenotypingForm from './GenotypingForm';

// const GenotypingContainer = asyncComponent(() => import('./GenotypingContainer'));
// const GenotypingForm = asyncComponent(() => import('./GenotypingForm'));

export { GenotypingContainer, GenotypingForm };
