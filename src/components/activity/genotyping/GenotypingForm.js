import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import {
  NatGen,
  Nat2PhenRes,
  Slco1b1GenRes,
  Slco1b1PhenRes,
  Oct2A270sGen,
  Oct2T199iGen,
  Oct2T201mGen,
  GenotypeMethod,
  SelectReason
} from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';

const GenotypingForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Group grouped>
        <label>{t('activity.genotyping.nat2GenResCodeId')}</label>
        <NatGen
          name="nat2GenResCodeId"
          value={model.nat2GenResCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Field>
        <label>{t('activity.genotyping.nat2GenResOther')}</label>
        <Input
          name="nat2GenResOther"
          value={model.nat2GenResOther || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.genotyping.nat2PhenResCodeId')}</label>
        <Nat2PhenRes
          name="nat2PhenResCodeId"
          value={model.nat2PhenResCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.genotyping.slco1b1GenResCodeId')}</label>
        <Slco1b1GenRes
          name="slco1b1GenResCodeId"
          value={model.slco1b1GenResCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Field>
        <label>{t('activity.genotyping.slco1b1GenResOther')}</label>
        <Input
          name="slco1b1GenResOther"
          value={model.slco1b1GenResOther || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.genotyping.slco1b1PhenResCodeId')}</label>
        <Slco1b1PhenRes
          name="slco1b1PhenResCodeId"
          value={model.slco1b1PhenResCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.genotyping.oct2A270sGenCodeId')}</label>
        <Oct2A270sGen
          name="oct2A270sGenCodeId"
          value={model.oct2A270sGenCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.genotyping.oct2T199iGenCodeId')}</label>
        <Oct2T199iGen
          name="oct2T199iGenCodeId"
          value={model.oct2T199iGenCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.genotyping.oct2T201mGenCodeId')}</label>
        <Oct2T201mGen
          name="oct2T201mGenCodeId"
          value={model.oct2T201mGenCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.genotyping.genotypeMethodCodeId')}</label>
        <GenotypeMethod
          name="genotypeMethodCodeId"
          value={model.genotypeMethodCodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.subjectGenotypingId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.genotyping.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default GenotypingForm;
