import React, { useCallback } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = ({
  subjectId,
  updateRepeat,
  projectActivityId,
  items,
  loading,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  refInfos,
  ethnicCodes,
  genericName,
  isSeleted
}) => {
  const t = useFormatMessage();
  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return 'unknown';
    },
    [refInfos]
  );

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  return (
    <Table compact="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Drug</Table.HeaderCell>
          <Table.HeaderCell>ADR/Toxicity</Table.HeaderCell>
          <Table.HeaderCell>Detail about Dosage</Table.HeaderCell>
          <Table.HeaderCell>Gene</Table.HeaderCell>
          <Table.HeaderCell>Variant</Table.HeaderCell>
          <Table.HeaderCell>Genotype</Table.HeaderCell>
          <Table.HeaderCell>Non Genetics factors</Table.HeaderCell>
          <Table.HeaderCell>Other Details</Table.HeaderCell>
          <Table.HeaderCell>Reference</Table.HeaderCell>
          <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map(item => {
          const {
            tbdrugToxicityId,
            referenceId,
            toxicity,
            detailAboutDosage,
            gene,
            variant,
            genotype,
            nonGeneticsFactors,
            otherDetail,
            activityStatusCodeId
          } = item;

          return (
            <StyledTableRow key={tbdrugToxicityId} selected={isSeleted(item)}>
              <Table.Cell>
                <ActivityItemAnchor
                  id={tbdrugToxicityId}
                  text={!!genericName ? genericName : 'unknown'}
                  onClick={onClick}
                />
              </Table.Cell>
              <Table.Cell>{toxicity}</Table.Cell>
              <Table.Cell>{detailAboutDosage}</Table.Cell>
              <Table.Cell>{gene}</Table.Cell>
              <Table.Cell>{variant}</Table.Cell>
              <Table.Cell>{genotype}</Table.Cell>
              <Table.Cell>{nonGeneticsFactors}</Table.Cell>
              <Table.Cell>{otherDetail}</Table.Cell>
              <Table.Cell>{getRefTitle(referenceId)}</Table.Cell>
              <Table.Cell>
                <StatusWrapper
                  subjectId={subjectId}
                  onChangeStatusCb={handleChangeStatus(item)}
                  activityKeyId={tbdrugToxicityId}
                  projectActivityId={projectActivityId}
                  note={{
                    value: item.hasNote,
                    closed: onNoteModalClosed
                  }}
                  query={{
                    value: item.queryStatusCodeId,
                    closed: onQueryModalClosed
                  }}
                  file={{
                    value: item.hasFile,
                    closed: onFileModalClosed
                  }}
                  activity={{
                    value: activityStatusCodeId,
                    closed: () => {}
                  }}
                  audit={{
                    entityName: 'TbdrugToxicity',
                    prefixLocale: 'activity.tbdrugToxicity',
                    skip: ['tbdrugToxicityId', 'tbdrugId']
                  }}
                  repeatItems={items}
                />
              </Table.Cell>
            </StyledTableRow>
          );
        })}
        {loading && (
          <EmptyRowsViewForSemantic
            loading={true}
            columnLength={10}
            size="large"
          />
        )}
      </Table.Body>
    </Table>
  );
};

export default RepeatList;
