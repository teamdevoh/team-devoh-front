import React, { useCallback } from 'react';
import { Form, Input, TextArea } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '..';
import { useSubmit } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import { SelectReason } from '../Selector';

const ToxicityForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  refInfos
}) => {
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return '';
    },
    [refInfos]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle(model.referenceId)}
          name="referenceId"
          value={model.referenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>ADR/Toxicity</label>
        <Input
          name="toxicity"
          value={model.toxicity || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Detail about Dosage</label>
        <Input
          name="detailAboutDosage"
          value={model.detailAboutDosage || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Gene</label>
        <Input name="gene" value={model.gene || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>Variant</label>
        <Input name="variant" value={model.variant || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>Genotype</label>
        <Input
          name="genotype"
          value={model.genotype || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Non genetics factors details</label>
        <Input
          name="nonGeneticsFactors"
          value={model.nonGeneticsFactors || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Other details</label>
        <Input
          name="otherDetail"
          value={model.otherDetail || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Comments</label>
        <TextArea
          name="comments"
          value={model.comments || ''}
          onChange={onChange}
        />
      </Form.Field>

      {model.tbdrugToxicityId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="Toxicity"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default ToxicityForm;
