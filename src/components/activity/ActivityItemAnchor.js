import React, { useCallback } from 'react';
import { Anchor } from '../button';

const ActivityItemAnchor = ({ id, text, onClick, projectActivityId }) => {
  const handleClick = useCallback(
    () => {
      onClick(id, projectActivityId);
    },
    [onClick]
  );
  return <Anchor text={text} onClick={handleClick} />;
};

export default ActivityItemAnchor;
