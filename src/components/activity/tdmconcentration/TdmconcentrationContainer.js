import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '..';
import { AuthorInfo } from '../../collection';
import { TdmconcentrationForm, RepeatList } from '.';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { usePublishStatus, useMoveToNextStep } from '../../collection/hooks';
import {
  useTdmconcentrationList,
  useTdmconcentration,
  useActionStatus,
  useTdmsamplingDateList
} from '../hooks';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { useParams } from 'react-router-dom';
import { code } from '../../../constants';

const TdmconcentrationContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const { activityGroupId } = useParams();
  const key = 'subjectTdmconcentrationId';

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };
  const { submitted, isNext } = useActionStatus(updateRepeat);
  const publish = usePublishStatus({
    projectActivityId
  });
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const {
    add,
    update,
    remove,
    fetch,
    isSaving,
    isFetching
  } = useTdmconcentration({
    activityKey
  });
  const [selectedId, setSelectedId] = useState(0);
  const [curSelectedKey, setCurSelectedKey] = useState(0);
  const [repeat, setRepeat] = useTdmconcentrationList({
    subjectId,
    projectActivityId
  });
  const t = useFormatMessage();

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    subjectTdmconcentrationId: '',
    subjectId: subjectId,
    subjectVisitId: 0,
    extrt: null,
    analysisSampleId: null,
    drugConc: null,
    drugConcUnit: 'μg/mL',
    exdose: null,
    exdrugId: null,
    exdrugoth: null,
    exendtc: null,
    exstdtc: null,
    medDatetime: null,
    pkQuest: null,
    pkReasonCodeId: null,
    pkReasonoth: null,
    pkqcPost1: null,
    pkqcPost2: null,
    pkqcPost3: null,
    pkqcPre1: null,
    pkqcPre2: null,
    pkqcPre3: null,
    pkstd1: null,
    pkstd2: null,
    pkstd3: null,
    pkstd4: null,
    pkstd5: null,
    pkstd6: null,
    resultdtc: null,
    sampleId: null,
    sampleTime: null,
    subjectTdmdrugId: null,
    subjectTdmsamplingId: null,
    modified: null,
    author: '',
    isChanged: false,
    reason: ''
  };

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const [
    { items: tdmSamplingDateItems, loading: tdmSamplingDateLoading }
  ] = useTdmsamplingDateList({
    subjectId,
    subjectVisitId: model.subjectVisitId
  });

  const { updateRepeatByKey } = useRepeat(key);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0 || curSelectedKey !== 0) {
        fetchItem();
      }
    },
    [selectedId, curSelectedKey]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      subjectId,
      projectActivityId,
      [key]: model[key],
      subjectTdmdrugId: model.subjectTdmdrugId,
      subjectTdmsamplingId: model.subjectTdmsamplingId
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            let updated = [...repeat.items];
            const filtered = updated.filter((item, index, arr) => {
              // 환자가 기존에 복용하던
              const isSubTbdrug =
                item.subjectTdmdrugId !== null &&
                item.subjectTdmdrugId === model.subjectTdmdrugId &&
                item.subjectTdmsamplingId === model.subjectTdmsamplingId;
              if (isSubTbdrug) {
                updated[index] = {
                  ...item,
                  ...model
                };
              }

              return isSubTbdrug;
            });

            if (filtered.length === 0) {
              updated = repeat.items.concat(model);
            }

            setRepeat(updated);
            const pickedActivity = publish.pickActivityValueBy(updated);
            publish.activityStatus(pickedActivity);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    ({ id, subjectTdmdrugId, subjectTdmsamplingId }) => {
      setModel({
        ...model,
        [key]: id,
        subjectTdmdrugId,
        subjectTdmsamplingId
      });
      setSelectedId(id);
      setCurSelectedKey(`${subjectTdmdrugId}_${subjectTdmsamplingId}`);
    }
  );

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        [key]: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
      setCurSelectedKey(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleVisitChange = (e, data) => {
    onChange(e, data);
    setModel({
      ...model,
      [data.name]: data.value,
      subjectTdmsamplingId: null
    });
  };

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  useEffect(
    () => {
      let sampleId = null;
      if (model.subjectTdmsamplingId > 0 && tdmSamplingDateItems.length > 0) {
        const sampleByVisit = tdmSamplingDateItems.filter(
          item => item.value === model.subjectTdmsamplingId
        );
        if (sampleByVisit.length > 0) {
          sampleId = sampleByVisit[0].data.sampleId;
          onChange(null, { name: 'sampleId', value: sampleId });
        }
      }
    },
    [model.subjectTdmsamplingId, tdmSamplingDateItems]
  );

  const isSeleted = useCallback(
    item =>
      `${item.subjectTdmdrugId}_${item.subjectTdmsamplingId}` ===
        curSelectedKey && `${item[key]}` === `${selectedId}`,
    [curSelectedKey, selectedId]
  );

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text={t('activity.tdmconcentration.title')} />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'SubjectTdmconcentration',
                prefixLocale: 'activity.tdmconcentration',
                skip: [key, 'subjectTbdrugId', 'subjectTdmdrugId']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <TdmconcentrationForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
            onVisitChange={handleVisitChange}
            tdmSamplingDateItems={tdmSamplingDateItems}
            tdmSamplingDateLoading={tdmSamplingDateLoading}
            activityGroupId={activityGroupId}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </TdmconcentrationForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default TdmconcentrationContainer;
