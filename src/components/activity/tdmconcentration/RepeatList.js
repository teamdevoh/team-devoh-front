import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { useVisitListOfSubjectWithDate } from '../../collection/hooks';
import helpers from '../../../helpers';
import { code, format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [visit] = useVisitListOfSubjectWithDate({
      subjectId,
      showShortDate: true
    });
    const [drug] = useTbdrugList({ showOnlyShortName: false });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(
      items,
      ['subjectVisitId', 'resultdtc', 'tad', 'genericName'],
      ['asc', 'asc', 'asc', 'asc']
    );

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="two">
              {t('activity.visit')}
            </Table.HeaderCell>
            <Table.HeaderCell title={t('activity.tdmconcentration.exdrug')}>
              {t('activity.tdmconcentration.exdrug')}
            </Table.HeaderCell>
            <Table.HeaderCell title={t('activity.common.exdose')}>
              {t('activity.common.exdose')}
            </Table.HeaderCell>
            <Table.HeaderCell
              width="two"
              title={t('activity.tdmconcentration.medDatetime')}
            >
              {t('activity.tdmconcentration.medDatetime')}
            </Table.HeaderCell>
            <Table.HeaderCell
              width="two"
              title={t('activity.tdmconcentration.sampleTime')}
            >
              {t('activity.tdmconcentration.sampleTime')}
            </Table.HeaderCell>
            <Table.HeaderCell
              width="one"
              title={t('activity.tdmconcentration.TAD')}
            >
              {t('activity.tdmconcentration.TAD')}
            </Table.HeaderCell>
            <Table.HeaderCell title={t('activity.tdmconcentration.sampleId')}>
              {t('activity.tdmconcentration.sampleId')}
            </Table.HeaderCell>
            <Table.HeaderCell title={t('activity.tdmconcentration.drugConc')}>
              {t('activity.tdmconcentration.drugConc')}
            </Table.HeaderCell>
            <Table.HeaderCell title={t('activity.tdmconcentration.resultdtc')}>
              {t('activity.tdmconcentration.resultdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell
              width="one"
              title={t('activity.tdmconcentration.pkQuest')}
            >
              {t('activity.tdmconcentration.pkQuest')}
            </Table.HeaderCell>
            <Table.HeaderCell width="two">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const {
              activityStatusCodeId,
              drugConc,
              drugConcUnit,
              exdose,
              exdrugId,
              exdrugoth,
              medDatetime,
              pkQuest,
              resultdtc,
              sampleId,
              sampleTime,
              subjectTdmconcentrationId,
              subjectTdmdrugId,
              subjectTdmsamplingId,
              subjectVisitId,
              tad
            } = item;

            const visitName = build.displayName({
              source: visit.items,
              value: subjectVisitId
            });

            const drugName = build.displayName({
              source: drug.items,
              value: exdrugId,
              other: exdrugoth,
              otherValue: code.TBDrugOther
            });

            const dose = exdose ? `${exdose} mg` : '';

            const drugConcStr = drugConc
              ? `${drugConc} ${drugConcUnit} : ''`
              : '';

            const questionable =
              pkQuest === true ? 'Yes' : pkQuest === false ? 'No' : '';

            const resultDate = helpers.util.dateformat(
              resultdtc,
              format.YYYY_MM_DD
            );

            return (
              <StyledTableRow
                key={`${subjectTdmconcentrationId}_${subjectTdmdrugId}_${subjectTdmsamplingId}`}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={{
                      id: subjectTdmconcentrationId,
                      subjectTdmdrugId: subjectTdmdrugId,
                      subjectTdmsamplingId
                    }}
                    text={visitName}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={drugName}>{drugName}</Table.Cell>
                <Table.Cell title={dose}>{dose}</Table.Cell>
                <Table.Cell title={medDatetime}>{medDatetime}</Table.Cell>
                <Table.Cell title={sampleTime}>{sampleTime}</Table.Cell>
                <Table.Cell title={tad}>{tad}</Table.Cell>
                <Table.Cell title={sampleId}>{sampleId}</Table.Cell>
                <Table.Cell title={drugConcStr}>{drugConcStr}</Table.Cell>
                <Table.Cell title={resultDate}>{resultDate}</Table.Cell>
                <Table.Cell title={questionable}>{questionable}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={subjectTdmconcentrationId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectTdmconcentration',
                      prefixLocale: 'activity.tdmconcentration',
                      skip: [
                        'subjectTdmconcentrationId',
                        'subjectTbdrugId',
                        'subjectTdmdrugId'
                      ]
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={11}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
