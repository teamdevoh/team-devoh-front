import React from 'react';
import { Form, Input, Dropdown } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { TbdrugList } from '../../collection-tbdrug/Selector';
import {
  MyVisitByActivityGroupId,
  YesorNo,
  PkReason,
  SelectReason
} from '../Selector';
import { DisabledtyleSInput, DisabledStyleSelect } from '../styles';
import { DatePicker } from '../../datepicker';
import { code } from '../../../constants';

const TdmconcentrationForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  tdmSamplingDateItems,
  onVisitChange,
  tdmSamplingDateLoading,
  activityGroupId
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisitByActivityGroupId
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onVisitChange}
          disabled={!!model.subjectTdmdrugId}
          activityGroupId={activityGroupId}
          visitTypeCode={code.TDMVisit}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Group widths="equal">
        {!!model.subjectTdmdrugId && (
          <Form.Field>
            <label>{t('activity.tdmconcentration.extrt')}</label>
            <DisabledtyleSInput
              value={model.extrt || ''}
              readOnly
              disabled
              fluid
            />
          </Form.Field>
        )}
        <Form.Field required>
          <label>{t('activity.tdmconcentration.exdrugId')}</label>
          <TbdrugList
            name="exdrugId"
            value={model.exdrugId}
            onChange={onChange}
            disabled={model.subjectTdmdrugId ? true : false}
          />
          {validator.message(
            t('activity.tdmconcentration.exdrugId'),
            model.exdrugId,
            'required'
          )}
        </Form.Field>
        {model.exdrugId === code.TBDrugOther && (
          <Form.Field required>
            <label>{t('activity.tdmconcentration.exdrugoth')}</label>
            <Input
              name="exdrugoth"
              value={model.exdrugoth || ''}
              onChange={onChange}
            />
            {validator.message(
              t('activity.tdmconcentration.exdrugoth'),
              model.exdrugoth,
              'required'
            )}
          </Form.Field>
        )}
      </Form.Group>
      <Form.Group widths="equal">
        {!!model.subjectTdmdrugId && (
          <Form.Field>
            <label>{t('activity.tdmconcentration.exdose')}</label>
            <DisabledtyleSInput
              value={model.exdose || ''}
              readOnly
              disabled
              fluid
            />
          </Form.Field>
        )}
        {!!model.subjectTdmdrugId && (
          <Form.Field>
            <label>{t('activity.tdmconcentration.medDatetime')}</label>
            <DisabledtyleSInput
              value={model.medDatetime || ''}
              readOnly
              disabled
              fluid
            />
          </Form.Field>
        )}
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.tdmconcentration.sampleId')}</label>
          <DisabledtyleSInput
            value={model.sampleId ? `${model.sampleId}` : '-'}
            readOnly
            disabled
            fluid
          />
        </Form.Field>
        <Form.Field required>
          <label>{t('activity.tdmconcentration.subjectTdmsamplingId')}</label>
          <DisabledStyleSelect
            name="subjectTdmsamplingId"
            item
            loading={tdmSamplingDateLoading}
            value={model.subjectTdmsamplingId}
            onChange={onChange}
            options={tdmSamplingDateItems}
            disabled={!!model.subjectTdmdrugId}
          />
          {validator.message(
            t('activity.tdmconcentration.subjectTdmsamplingId'),
            model.subjectTdmsamplingId,
            'required'
          )}
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.tdmconcentration.analysisSampleId')}</label>
          <Input
            name="analysisSampleId"
            value={model.analysisSampleId || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.drugConc')}</label>
          <Input
            name="drugConc"
            value={model.drugConc || ''}
            onChange={onChange}
            label={
              <Dropdown
                name="drugConcUnit"
                value={model.drugConcUnit}
                options={[
                  {
                    text: 'μg/mL',
                    value: 'μg/mL'
                  },
                  {
                    text: 'BLLOQ',
                    value: 'BLLOQ'
                  }
                ]}
                onChange={onChange}
              />
            }
            labelPosition="right"
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.tdmconcentration.resultdtc')}</label>
          <DatePicker
            name="resultdtc"
            value={model.resultdtc}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkQuest')}</label>
          <YesorNo name="pkQuest" value={model.pkQuest} onChange={onChange} />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkReasonCodeId')}</label>
          <PkReason
            name="pkReasonCodeId"
            value={model.pkReasonCodeId}
            onChange={onChange}
          />
        </Form.Field>
        {model.pkReasonCodeId === code.PkReasonOther && (
          <Form.Field required>
            <label>{t('activity.tdmconcentration.pkReasonoth')}</label>
            <Input
              name="pkReasonoth"
              value={model.pkReasonoth || ''}
              onChange={onChange}
            />
            {validator.message(
              t('activity.tdmconcentration.pkReasonoth'),
              model.pkReasonoth,
              'required'
            )}
          </Form.Field>
        )}
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkqcPre1')}</label>
          <Input
            name="pkqcPre1"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkqcPre1 || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkqcPre2')}</label>
          <Input
            name="pkqcPre2"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkqcPre2 || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkqcPre3')}</label>
          <Input
            name="pkqcPre3"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkqcPre3 || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkqcPost1')}</label>
          <Input
            name="pkqcPost1"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkqcPost1 || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkqcPost2')}</label>
          <Input
            name="pkqcPost2"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkqcPost2 || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkqcPost3')}</label>
          <Input
            name="pkqcPost3"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkqcPost3 || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkstd1')}</label>
          <Input
            name="pkstd1"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkstd1 || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkstd2')}</label>
          <Input
            name="pkstd2"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkstd2 || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkstd3')}</label>
          <Input
            name="pkstd3"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkstd3 || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkstd4')}</label>
          <Input
            name="pkstd4"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkstd4 || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkstd5')}</label>
          <Input
            name="pkstd5"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkstd5 || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.tdmconcentration.pkstd6')}</label>
          <Input
            name="pkstd6"
            label={{ basic: true, content: '%' }}
            labelPosition="right"
            placeholder=""
            value={model.pkstd6 || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {model.subjectTdmconcentrationId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.tdmconcentration.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default TdmconcentrationForm;
