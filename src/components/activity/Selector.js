import React, { useCallback, Fragment, useEffect, useState } from 'react';
import {
  Select,
  Icon,
  Dropdown,
  Radio,
  Form,
  Transition,
  Label,
  TextArea
} from 'semantic-ui-react';
import helpers from '../../helpers';
import { useCodes, useBoolean, useFormatMessage } from '../../hooks';
import { useVisitListOfSubjectWithDate } from '../collection/hooks';
import { DisabledStyleSelect } from './styles';
import styled from 'styled-components';
import { codeGroup } from '../../constants';

const AddVisitButtonWrap = styled.div`
  display: inline-flex;
  align-items: center;
  vertical-align: middle;
`;

const Uncheck = ({ name, clearValue, onClick }) => {
  const visible = useBoolean(true);

  const handleClick = useCallback(
    async (event, data) => {
      visible.toggle();
      await helpers.util.sleep(500);
      onClick(event, { name, value: clearValue });
    },
    [onClick]
  );
  return (
    <Transition animation="bounce" duration={500} visible={visible.value}>
      <Icon name="close" onClick={handleClick} style={{ cursor: 'pointer' }} />
    </Transition>
  );
};

const AddSubjectVisitButton = ({ projectId, subjectId }) => {
  const handleGoVisitClick = useCallback(() => {
    helpers.history.push(`/project/${projectId}/subject/${subjectId}/step/1`);
  }, []);

  return (
    <Icon
      name="add"
      className="link"
      size="large"
      onClick={handleGoVisitClick}
    />
  );
};

const MyVisit = ({
  name,
  projectId,
  subjectId,
  value,
  onChange,
  onAddClick,
  visitTypeCode,
  ...rest
}) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useVisitListOfSubjectWithDate({
    subjectId,
    visitTypeCode
  });

  return (
    <div>
      <DisabledStyleSelect
        name={name}
        item
        loading={loading}
        value={value}
        onChange={onChange}
        options={items}
        {...rest}
      />
      <AddVisitButtonWrap>
        <AddSubjectVisitButton projectId={projectId} subjectId={subjectId} />
        <Label basic color="teal" pointing="left">
          {t('add.visit.info')}
        </Label>
      </AddVisitButtonWrap>
    </div>
  );
};

const MyVisitByActivityGroupId = ({
  name,
  projectId,
  subjectId,
  value,
  onChange,
  onAddClick,
  visitTypeCode,
  activityGroupId,
  ...rest
}) => {
  const t = useFormatMessage();
  const [{ items, loading }, setItems] = useVisitListOfSubjectWithDate({
    subjectId,
    visitTypeCode
  });

  useEffect(
    () => {
      if (items.length > 0 && !!activityGroupId) {
        let newVisitOptions = [];
        if (Number(activityGroupId) === 5) {
          newVisitOptions = items.filter(item => item.data.visitOrder === 1);
        } else if (Number(activityGroupId) === 15) {
          newVisitOptions = items.filter(item => item.data.visitOrder > 1);
        }

        setItems(newVisitOptions);
      }
    },
    [items, activityGroupId]
  );

  return (
    <div>
      <DisabledStyleSelect
        name={name}
        item
        loading={loading}
        value={value}
        onChange={onChange}
        options={items}
        {...rest}
      />
      <AddVisitButtonWrap>
        <AddSubjectVisitButton projectId={projectId} subjectId={subjectId} />
        <Label basic color="teal" pointing="left">
          {t('add.visit.info')}
        </Label>
      </AddVisitButtonWrap>
    </div>
  );
};

const Sex = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.SexCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[{ key: 0, text: '-', value: 0 }, ...items]}
    />
  );
};

const Ethnic = ({ name, value, onChange, useAll = false }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.EthnicCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={useAll ? [{ key: 0, text: '-', value: 0 }, ...items] : items}
    />
  );
};

const SmokeStatus = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.SmokeCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[{ key: 0, text: '-', value: 0 }, ...items]}
    />
  );
};

const SmokeDuring = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.SmokDurCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={items}
    />
  );
};

const Preserve = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.PreserveCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const Secondary = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.SecondaryCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const Identification = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.IdentificationCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const TBSuscept = ({ name, value, onChange, multiple, defaultOption }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.TBSusceptCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={defaultOption ? [defaultOption, ...items] : items}
      multiple={multiple || false}
    />
  );
};

const TBDiagnosis = ({ name, value, onChange, defaultOption, multiple }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.TBDiagnosisCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={defaultOption ? [defaultOption, ...items] : items}
      multiple={multiple || false}
    />
  );
};

const TBTreatment = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.TBTreatmentCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const RetrtDrug = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.RetrtDrugCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const Comorbidity = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.ComorbidCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Dosfrq = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.DosfrqCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={items}
    />
  );
};

const Dosfrm = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.DosfrmCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={items}
    />
  );
};

const Route = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.RouteCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={items}
    />
  );
};

const AFBSpec = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.AFBSpecCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Mycobact = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.MycobactCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const AFBStainResult = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.AFBStainResultCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const AFBCultureSolid = ({ name, value, onChange, defaultOption }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.AFBCultureSolidCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={defaultOption ? [defaultOption, ...items] : items}
    />
  );
};

const AFBCultureLiquid = ({ name, value, onChange, defaultOption }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.AFBCultureLiquidCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={defaultOption ? [defaultOption, ...items] : items}
    />
  );
};

const XpertPCR = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.XpertPCRCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const XpertRIF = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.XpertRIFCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const IGRA = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.IGRACode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const PCRResult = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.PCRResultCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const YesorNoorUK = ({ name, value, onChange, width }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.YesorNoorUK });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value} width={width}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const YesorNo = ({ name, value, onChange, width }) => {
  return (
    <Fragment>
      <Form.Field width={width}>
        <Radio
          name={name}
          label="No"
          value={0}
          checked={value !== null && !!!value}
          onChange={onChange}
        />
        {value !== null &&
          !!!value && (
            <Uncheck name={name} clearValue={null} onClick={onChange} />
          )}
      </Form.Field>
      <Form.Field width={width}>
        <Radio
          name={name}
          label="Yes"
          value={1}
          checked={!!value}
          onChange={onChange}
        />
        {!!value && (
          <Uncheck name={name} clearValue={null} onClick={onChange} />
        )}
      </Form.Field>
    </Fragment>
  );
};

const DstResult = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.DstResultCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field width="three" key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const MetabolicEnzymeTransporter = ({ name, value, onChange }) => {
  const [items] = useCodes({
    codeGroupId: codeGroup.MetabolicEnzymeTransporterCode
  });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={items}
    />
  );
};

const ProcessType = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({
    codeGroupId: codeGroup.ProcessTypeCode
  });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const ExperimentSystem = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({
    codeGroupId: codeGroup.ExperimentsystemCode
  });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const DialysisMethod = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.DialysisMethodCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const HDS = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.HDSCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const Macroscopy = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.MacroscopyCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Urgent = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.UrgentCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const NatGen = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.NatGenCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Nat2PhenRes = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.Nat2PhenResCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Slco1b1GenRes = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.Slco1b1GenResCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Slco1b1PhenRes = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.Slco1b1PhenResCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Oct2A270sGen = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.Oct2A270sGenCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Oct2T199iGen = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.Oct2T199iGenCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Oct2T201mGen = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.Oct2T201mGenCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const GenotypeMethod = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.GenotypeMethodCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const FastFed = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.FastFedCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const AdrSeverity = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.AdrSeverityCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={items}
    />
  );
};

const Adrcon = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.AdrconCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={items}
    />
  );
};

const Adragep = ({ name, value, onChange, width }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.AdragepCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value} width={width}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const AdrdressEosin = ({ name, value, onChange, width }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.AdrdressEosinCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value} width={width}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const AdrDressOrgan = ({ name, value, onChange, width }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.AdrDressOrganCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value} width={width}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const AdrDressRsol = ({ name, value, onChange, width }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.AdrDressRsolCode });

  return (
    <Fragment>
      {items.map(item => {
        return (
          <Form.Field key={item.value} width={width}>
            <Radio
              name={name}
              label={item.text}
              value={item.value}
              checked={item.value === value}
              onChange={onChange}
            />
            {item.value === value && (
              <Uncheck name={name} clearValue={''} onClick={onChange} />
            )}
          </Form.Field>
        );
      })}
    </Fragment>
  );
};

const Casecon = ({ name, value, onChange, defaultOption }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.CaseconCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={defaultOption ? [defaultOption, ...items] : items}
    />
  );
};
const DigFin = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.DigFinCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const PkReason = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.PkReasonCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const Relapse = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.RelapseCode });

  return (
    <Dropdown
      name={name}
      search
      fluid
      selection
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('common.notSelected'),
          value: ''
        },
        ...items
      ]}
    />
  );
};

const SelectReason = ({ model, setModel, onChange, validator }) => {
  const t = useFormatMessage();
  const [value, setValue] = useState();
  let reason = '';

  useEffect(
    () => {
      if (model.reason !== value) {
        setValue(t('enter.directly'));
      }
    },
    [model.reason]
  );

  const handleChange = (e, { value }) => {
    setValue(value);
    if (value === t('enter.directly')) {
      reason = '';
    } else {
      reason = value;
    }

    setModel({
      ...model,
      reason
    });
  };
  return (
    <Fragment>
      <Form.Field required>
        <label>{t('activity.reason')}</label>
      </Form.Field>
      <Form.Field>
        <Radio
          label={t('correct.error')}
          name="selectReason"
          value={t('correct.error')}
          checked={value === t('correct.error')}
          onChange={handleChange}
        />
      </Form.Field>
      <Form.Field>
        <Radio
          label={t('change.data')}
          name="selectReason"
          value={t('change.data')}
          checked={value === t('change.data')}
          onChange={handleChange}
        />
      </Form.Field>
      <Form.Field>
        <Radio
          label={t('enter.the.missing.item')}
          name="selectReason"
          value={t('enter.the.missing.item')}
          checked={value === t('enter.the.missing.item')}
          onChange={handleChange}
        />
      </Form.Field>
      <Form.Field>
        <Radio
          label={t('enter.directly')}
          name="selectReason"
          value={t('enter.directly')}
          checked={value === t('enter.directly')}
          onChange={handleChange}
        />
      </Form.Field>
      <Form.Field required>
        <TextArea
          name="reason"
          value={model.reason || ''}
          onChange={onChange}
        />
        {validator.message(t('activity.reason'), model.reason, 'required')}
      </Form.Field>
    </Fragment>
  );
};

export {
  AddSubjectVisitButton,
  MyVisit,
  MyVisitByActivityGroupId,
  Sex,
  Ethnic,
  SmokeStatus,
  SmokeDuring,
  Preserve,
  Secondary,
  Identification,
  TBSuscept,
  TBDiagnosis,
  TBTreatment,
  RetrtDrug,
  Comorbidity,
  Uncheck,
  Dosfrq,
  Dosfrm,
  Route,
  AFBSpec,
  Mycobact,
  AFBStainResult,
  AFBCultureSolid,
  AFBCultureLiquid,
  XpertPCR,
  XpertRIF,
  IGRA,
  PCRResult,
  YesorNoorUK,
  YesorNo,
  DstResult,
  MetabolicEnzymeTransporter,
  ProcessType,
  ExperimentSystem,
  DialysisMethod,
  HDS,
  Macroscopy,
  Urgent,
  NatGen,
  Nat2PhenRes,
  Slco1b1GenRes,
  Slco1b1PhenRes,
  Oct2A270sGen,
  Oct2T199iGen,
  Oct2T201mGen,
  GenotypeMethod,
  FastFed,
  AdrSeverity,
  Adrcon,
  Adragep,
  AdrdressEosin,
  AdrDressOrgan,
  AdrDressRsol,
  Casecon,
  DigFin,
  PkReason,
  Relapse,
  SelectReason
};
