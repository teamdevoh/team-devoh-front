import React from 'react';
import './FormContainer.css';
import { Segment } from 'semantic-ui-react';
import find from 'lodash/find';
import Activities from './Activities';

const FormContainer = ({ activityId, ...rest }) => {
  const activity = find(Activities(rest), { key: Number(activityId) });
  if (!activity) {
    throw Error('Required is activity component');
  }

  return (
    <Segment raised className="activity-container">
      {activity.component}
    </Segment>
  );
};

export default FormContainer;
