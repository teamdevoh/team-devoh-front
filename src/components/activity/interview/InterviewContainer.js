import React, { Fragment, useCallback, useEffect } from 'react';
import { useFormFields, useFormatMessage } from '../../../hooks';
import { usePublishStatus, useMoveToNextStep } from '../../collection/hooks';
import { ActionBar, FormTitle, FormMetaBar } from '../';
import { AuthorInfo } from '../../collection';
import { notification } from '../../modal';
import { InterviewForm } from '.';
import { useInterview, useActionStatus } from '../hooks';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useStatus from '../hooks/useStatus';
import { code } from '../../../constants';

const InterviewContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    projectSiteId
  } = props;

  const key = 'subjectInterviewId';
  const handleChangeStatus = activityStatusCodeId => {
    setModel({
      ...model,
      activityStatusCodeId
    });
  };
  const { submitted, isNext } = useActionStatus(handleChangeStatus);
  const { add, update, fetch, isFetching, isSaving } = useInterview({
    activityKey
  });
  const publish = usePublishStatus({
    projectActivityId
  });
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const t = useFormatMessage();

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields({
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    subjectInterviewId: 0,
    subjectId: subjectId,
    subjectVisitId: '',
    regidtc: null,
    consdtc: null,
    tbIcver: '',
    siteId: '',
    siteOth: '',
    docName: '',
    brthdtc: null,
    age: null,
    sexcodeId: null,
    ethniccodeId: null,
    ethnicoth: '',
    smokecodeId: null,
    smokamt: '',
    smokdurcodeId: null,
    exSmoker: '',
    currentSmoker: '',
    modified: null,
    author: '',
    isChanged: false,
    reason: ''
  });

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(() => {
    fetchItem();
  }, []);

  const fetchItem = useCallback(
    async () => {
      const data = await fetch({ subjectId, projectActivityId });
      if (data[key] > 0) {
        setModel({
          ...model,
          ...data,
          projectActivityId: projectActivityId,
          isDone: data.activityStatusCodeId === code.Temp ? false : true
        });
      }
    },
    [model]
  );

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            publish.activityStatus(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId) {
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            publish.activityStatus(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleFileModalClosed = useCallback(value => {
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback(value => {
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback(value => {
    status.note.setHasNote(value);
  }, []);

  return (
    <Fragment>
      <LeavingGuard shouldBlock={model.isChanged} />
      <FormTitle text={t('activity.interview.title')} />
      <FormMetaBar>
        <AuthorInfo date={model.modified} name={model.author} />
        <StatusWrapper
          subjectId={subjectId}
          onChangeStatusCb={handleChangeStatus}
          activityKeyId={model[key]}
          projectActivityId={projectActivityId}
          note={{
            value: status.note.hasNote,
            closed: handleNoteModalClosed
          }}
          query={{
            value: status.qna.query.queryStatusCodeId,
            closed: handleQueryModalClosed
          }}
          file={{
            value: status.file.hasFile,
            closed: handleFileModalClosed
          }}
          activity={{
            value: model.activityStatusCodeId,
            closed: () => {}
          }}
          audit={{
            entityName: 'SubjectInterview',
            prefixLocale: 'activity.interview',
            skip: [key]
          }}
        />
      </FormMetaBar>
      <InterviewForm
        projectId={projectId}
        loading={isFetching}
        model={model}
        setModel={setModel}
        onChange={onChange}
        onSubmit={handleSubmit}
        projectSiteId={projectSiteId}
      >
        <ActionBar
          statusCode={model.activityStatusCodeId}
          isDone={model.isDone}
          subjectId={subjectId}
          projectActivityId={projectActivityId}
          activityKeyId={model[key]}
          loading={isSaving}
          onClick={submitted}
        />
      </InterviewForm>
    </Fragment>
  );
};

export default InterviewContainer;
