import React, { Fragment, useState, useEffect } from 'react';
import { Form, Input, Label, Dropdown } from 'semantic-ui-react';
import {
  MyVisit,
  Ethnic,
  SmokeStatus,
  SmokeDuring,
  Sex,
  SelectReason
} from '../Selector';
import { DatePicker } from '../../datepicker';
import { useSubmit, useFormatMessage } from '../../../hooks';
import helpers from '../../../helpers';
import { FormDoneOrTempBar } from '../';
import { code } from '../../../constants';
import { useProjectSiteList } from '../../project-manage/hooks';

const InterviewForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  projectSiteId
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);
  const [
    { items: projectSites, isLoading: projectSiteLoading }
  ] = useProjectSiteList({ projectId });
  const [options, setIOptions] = useState([]);

  useEffect(
    () => {
      if (projectSites.length > 0) {
        const newOptions = [];
        projectSites.forEach(item => {
          newOptions.push({ text: item.siteName, value: item.siteId });

          if (!!!model.siteId && item.projectSiteId === projectSiteId) {
            onChange(null, { name: 'siteId', value: item.siteId });
          }
        });

        setIOptions(newOptions);
      }
    },
    [projectSites, projectSites]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
          visitTypeCode={code.SubjectAgree}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('activity.interview.regidtc')}</label>
          <DatePicker
            name="regidtc"
            onChange={onChange}
            value={model.regidtc}
          />
          {validator.message(
            t('activity.interview.regidtc'),
            model.regidtc,
            'required'
          )}
        </Form.Field>
        <Form.Field required>
          <label>{t('activity.interview.consdtc')}</label>
          <DatePicker
            name="consdtc"
            onChange={onChange}
            value={model.consdtc}
          />
          {validator.message(
            t('activity.interview.consdtc'),
            model.consdtc,
            'required'
          )}
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('activity.interview.tbIcver')}</label>
          <Input name="tbIcver" value={model.tbIcver} onChange={onChange} />
          {validator.message(
            t('activity.interview.tbIcver'),
            model.tbIcver,
            'required'
          )}
        </Form.Field>
        <Form.Field required>
          <label>{t('activity.interview.siteId')}</label>
          <Dropdown
            name="siteId"
            search
            fluid
            selection
            value={model.siteId}
            onChange={onChange}
            options={options}
            loading={projectSiteLoading}
          />
          {validator.message(
            t('activity.interview.siteId'),
            model.siteId,
            'required'
          )}
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('activity.interview.docName')}</label>
          <Input name="docName" value={model.docName} onChange={onChange} />
          {validator.message(
            t('activity.interview.docName'),
            model.docName,
            'required'
          )}
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.brthdtc')}</label>
          <DatePicker
            name="brthdtc"
            value={model.brthdtc}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.age')}</label>
          <Label size="big" name="age">
            {(model.age = helpers.util.diffYears(model.brthdtc, model.consdtc))}
          </Label>
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.sexcodeId')}</label>
          <Sex name="sexcodeId" value={model.sexcodeId} onChange={onChange} />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('activity.interview.ethniccodeId')}</label>
          <Ethnic
            name="ethniccodeId"
            value={model.ethniccodeId}
            onChange={onChange}
          />
          {validator.message(
            t('activity.interview.ethniccodeId'),
            model.ethniccodeId,
            'required'
          )}
        </Form.Field>
        {model.ethniccodeId === 51 && (
          <Form.Field>
            <label>{t('activity.interview.ethnicoth')}</label>
            <Input
              name="ethnicoth"
              value={model.ethnicoth}
              onChange={onChange}
            />
          </Form.Field>
        )}
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('activity.interview.smokecodeId')}</label>
          <SmokeStatus
            name="smokecodeId"
            value={model.smokecodeId}
            onChange={onChange}
          />
          {validator.message(
            t('activity.interview.smokecodeId'),
            model.smokecodeId,
            'required'
          )}
        </Form.Field>
      </Form.Group>
      {/* only smoker */}
      {(model.smokecodeId === code.ExSmoker ||
        model.smokecodeId === code.CurrentSmoker) && (
        <Fragment>
          <Form.Group widths="equal">
            <Form.Field>
              <label>{t('activity.interview.smokamt')}</label>
              <Input name="smokamt" value={model.smokamt} onChange={onChange} />
            </Form.Field>
            <Form.Field>
              <label>{t('activity.interview.smokdurcodeId')}</label>
              <SmokeDuring
                name="smokdurcodeId"
                value={model.smokdurcodeId}
                onChange={onChange}
              />
            </Form.Field>
          </Form.Group>
          <Form.Group widths="equal">
            {model.smokecodeId === code.ExSmoker && (
              <Form.Field>
                <label>{t('activity.interview.exSmoker')}</label>
                <Input
                  name="exSmoker"
                  value={model.exSmoker === null ? '' : model.exSmoker}
                  onChange={onChange}
                />
              </Form.Field>
            )}
            {model.smokecodeId === code.CurrentSmoker && (
              <Form.Field>
                <label>{t('activity.interview.currentSmoker')}</label>
                <Input
                  name="currentSmoker"
                  value={model.currentSmoker}
                  onChange={onChange}
                />
              </Form.Field>
            )}
          </Form.Group>
        </Fragment>
      )}
      {model.subjectInterviewId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.interview.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>

      {children}
    </Form>
  );
};

export default InterviewForm;
