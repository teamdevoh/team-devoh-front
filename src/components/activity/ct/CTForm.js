import React from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { SelectReason } from '../Selector';
import DateInput from '../../input/DateInput';
import { format } from '../../../constants';

const CTForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.ct.ctDtc')}</label>
        <DateInput
          name="ctDtc"
          value={model.ctDtc}
          placeholder={format.YYYY_MM_UK}
          options={{ dateFormat: format.YYYY_MM_UK, separate: '-' }}
          onChange={onChange}
        />
        {validator.message(t('activity.ct.ctDtc'), model.ctDtc, 'required')}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.ct.ctResult')}</label>
        <TextArea name="ctResult" value={model.ctResult} onChange={onChange} />
      </Form.Field>
      {model.subjectCtid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.ct.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default CTForm;
