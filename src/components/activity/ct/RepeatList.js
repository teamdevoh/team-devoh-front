import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['ctDtc'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="two">
              {t('activity.ct.ctDtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('activity.ct.ctResult')}</Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            return (
              <StyledTableRow key={item.subjectCtid} selected={isSeleted(item)}>
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectCtid}
                    text={item.ctDtc}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={item.ctResult}>{item.ctResult}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectCtid}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectCt',
                      prefixLocale: 'activity.ct',
                      skip: ['subjectCtid']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={3}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
