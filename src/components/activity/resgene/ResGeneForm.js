import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DatePicker } from '../../datepicker';
import { TbdrugList } from '../../collection-tbdrug/Selector';
import { SelectReason } from '../Selector';
import { code } from '../../../constants';

const ResGeneForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.resgene.exdrugId')}</label>
        <TbdrugList
          name="exdrugId"
          value={model.exdrugId}
          onChange={onChange}
          disabled={model.subjectTbdrugId ? true : false}
        />
        {validator.message(
          t('activity.resgene.exdrugId'),
          model.exdrugId,
          'required'
        )}
      </Form.Field>
      {model.exdrugId === code.TBDrugOther && (
        <Form.Field required>
          <label>{t('activity.resgene.exdrugoth')}</label>
          <Input
            name="exdrugoth"
            value={model.exdrugoth || ''}
            onChange={onChange}
          />
          {validator.message(
            t('activity.resgene.exdrugoth'),
            model.exdrugoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Field>
        <label>{t('activity.resgene.resDtc')}</label>
        <DatePicker name="resDtc" value={model.resDtc} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.resgene.resiGene')}</label>
        <Input
          name="resiGene"
          value={model.resiGene || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.resgene.resMuta')}</label>
        <Input name="resMuta" value={model.resMuta || ''} onChange={onChange} />
      </Form.Field>
      {model.subjectResGeneId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.resgene.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default ResGeneForm;
