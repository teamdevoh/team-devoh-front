import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import helpers from '../../../helpers';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [drug] = useTbdrugList();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['resDtc'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              {t('activity.resgene.exdrugId')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('activity.common.exdose')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.common.exPeriod')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.resgene.resDtc')}</Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.resgene.resiGene')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('activity.resgene.resMuta')}</Table.HeaderCell>
            <Table.HeaderCell>{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const {
              activityStatusCodeId,
              exdrugId,
              exdrugoth,
              resDtc,
              resMuta,
              resiGene,
              subjectResGeneId,
              subjectTbdrugId,
              // subjectVisitId,
              exdose,
              exendtc,
              exstdtc
            } = item;

            const drugName = build.displayName({
              source: drug.items,
              value: exdrugId,
              other: exdrugoth,
              otherValue: code.TBDrugOther
            });

            const resDt = helpers.util.dateformat(resDtc, format.YYYY_MM_DD);

            const dose = exdose ? `${exdose} mg` : '';

            const exPeriod =
              exstdtc || exendtc ? `${exstdtc || ''} ~ ${exendtc || ''}` : '';

            return (
              <StyledTableRow
                key={`${subjectResGeneId}_${subjectTbdrugId}`}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={{
                      id: subjectResGeneId,
                      subjectTbdrugId: subjectTbdrugId
                    }}
                    text={drugName}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={dose}>{dose}</Table.Cell>
                <Table.Cell title={exPeriod}>{exPeriod}</Table.Cell>
                <Table.Cell title={resDt}>{resDt}</Table.Cell>
                <Table.Cell title={resiGene}>{resiGene}</Table.Cell>
                <Table.Cell title={resMuta}>{resMuta}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={subjectResGeneId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectResGene',
                      prefixLocale: 'activity.resgene',
                      skip: ['subjectResGeneId', 'subjectTbdrugId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={7}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
