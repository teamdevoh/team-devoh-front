import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '../';
import { BodyMeasureForm, RepeatList } from './';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { useBodies, useActionStatus, useBody, useBMI } from '../hooks';
import { AuthorInfo } from '../../collection';
import { LeavingGuard } from '../../modal';
import StatusWrapper from '../StatusWrapper';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { code } from '../../../constants';

const BodyMeasureContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'subjectBodyId';
  const { add, update, remove, fetch, isSaving, isFetching } = useBody({
    activityKey
  });

  const updateRepeat = (activityStatusCodeId, data = {}) => {
    let target = model;
    let setTarget = setModel;

    if (data[key] > 0) {
      target = data;
      if (data[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });

  const [repeat, setRepeat] = useBodies({ subjectId, projectActivityId });

  const t = useFormatMessage();

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    subjectBodyId: '',
    activityId: activityKey,
    subjectId: subjectId,
    subjectVisitId: '',
    height: 0,
    weight: 0,
    bmi: 0,
    modified: null,
    author: '',
    isChanged: false,
    reason: ''
  };
  const [selectedId, setSelectedId] = useState(0);
  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );
  const [bmi] = useBMI({ weight: model.weight, height: model.height });

  const { updateRepeatByKey } = useRepeat(key, projectActivityId);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  useEffect(
    () => {
      setModel({ ...model, bmi });
    },
    [bmi]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      subjectId,
      projectActivityId: model.projectActivityId || projectActivityId,
      subjectBodyId: model[key]
    });

    setModel({
      ...model,
      ...data,
      projectActivityId: data.projectActivityId || projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    (id, projectActivityId) => {
      setModel({ ...model, projectActivityId, [key]: id });
      setSelectedId(id);
    },
    [model, selectedId]
  );

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        subjectBodyId: 0,
        height:
          repeat.items.length > 0
            ? repeat.items[repeat.items.length - 1].height
            : 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={model.projectActivityId || projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text={t('activity.body.title')} />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={model.projectActivityId || projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'SubjectBody',
                prefixLocale: 'activity.body',
                skip: [key]
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <BodyMeasureForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={model.projectActivityId || projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </BodyMeasureForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default BodyMeasureContainer;
