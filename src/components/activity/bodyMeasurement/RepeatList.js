import React from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { useVisitListOfSubjectWithDate } from '../../collection/hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = ({
  subjectId,
  updateRepeat,
  projectActivityId,
  items,
  loading,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  isSeleted
}) => {
  const build = useBuild();
  const [visit] = useVisitListOfSubjectWithDate({
    subjectId
  });
  const t = useFormatMessage();

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  return (
    <Table compact="very" fixed singleLine>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell width="five">
            {t('activity.visit')}
          </Table.HeaderCell>
          <Table.HeaderCell>{t('activity.body.height')}</Table.HeaderCell>
          <Table.HeaderCell>{t('activity.body.weight')}</Table.HeaderCell>
          <Table.HeaderCell>{t('activity.body.bmi')}</Table.HeaderCell>
          <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map(item => {
          const visitName = build.displayName({
            source: visit.items,
            value: item.subjectVisitId
          });

          return (
            <StyledTableRow key={item.subjectBodyId} selected={isSeleted(item)}>
              <Table.Cell>
                <ActivityItemAnchor
                  id={item.subjectBodyId}
                  text={visitName}
                  projectActivityId={
                    item.projectActivityId || projectActivityId
                  }
                  onClick={onClick}
                />
              </Table.Cell>
              <Table.Cell>{item.height}</Table.Cell>
              <Table.Cell>{item.weight}</Table.Cell>
              <Table.Cell>{item.bmi}</Table.Cell>
              <Table.Cell>
                <StatusWrapper
                  subjectId={subjectId}
                  onChangeStatusCb={handleChangeStatus(item)}
                  activityKeyId={item.subjectBodyId}
                  projectActivityId={
                    item.projectActivityId || projectActivityId
                  }
                  note={{
                    value: item.hasNote,
                    closed: onNoteModalClosed
                  }}
                  query={{
                    value: item.queryStatusCodeId,
                    closed: onQueryModalClosed
                  }}
                  file={{
                    value: item.hasFile,
                    closed: onFileModalClosed
                  }}
                  activity={{
                    value: item.activityStatusCodeId,
                    closed: () => {}
                  }}
                  audit={{
                    entityName: 'SubjectBody',
                    prefixLocale: 'activity.body',
                    skip: ['subjectBodyId']
                  }}
                  repeatItems={items}
                />
              </Table.Cell>
            </StyledTableRow>
          );
        })}
        {loading && (
          <EmptyRowsViewForSemantic
            loading={true}
            columnLength={5}
            size="large"
          />
        )}
      </Table.Body>
    </Table>
  );
};

export default RepeatList;
