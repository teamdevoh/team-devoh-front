import React from 'react';
import { Form } from 'semantic-ui-react';
import { MyVisit, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '../';
import { DecimalInput } from '../../input';
import { useFormatMessage, useSubmit } from '../../../hooks';

const BodyMeasureForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('activity.body.height')}</label>
          <DecimalInput
            name="height"
            options={{ suffix: ' cm' }}
            value={model.height}
            onChange={onChange}
          />
          {validator.message(
            t('activity.body.height'),
            model.height,
            'required'
          )}
        </Form.Field>
        <Form.Field required>
          <label>{t('activity.body.weight')}</label>
          <DecimalInput
            name="weight"
            options={{ suffix: ' kg' }}
            value={model.weight}
            onChange={onChange}
          />
          {validator.message(
            t('activity.body.weight'),
            model.weight,
            'required'
          )}
        </Form.Field>
        <Form.Field required>
          <label>{t('activity.body.bmi')}</label>
          <DecimalInput name="bmi" value={model.bmi} onChange={onChange} />
          {validator.message(t('activity.body.bmi'), model.bmi, 'required')}
        </Form.Field>
      </Form.Group>
      {model.subjectBodyId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.body.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default BodyMeasureForm;
