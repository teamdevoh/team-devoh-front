import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import helpers from '../../../helpers';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, codeGroup, format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [specCodes] = useCodes({ codeGroupId: codeGroup.AFBSpecCode });
    const [pcrResultCodes] = useCodes({
      codeGroupId: codeGroup.PCRResultCode
    });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['pcrDtc'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>{t('activity.pcr.pcrDtc')}</Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.pcr.pcrSprcCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.pcr.pcrResultCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const specimen = build.displayName({
              source: specCodes,
              value: item.pcrSprcCodeId,
              other: item.pcrOth,
              otherValue: code.AFBSpecOther
            });
            const pcrResult = build.displayName({
              source: pcrResultCodes,
              value: item.pcrResultCodeId
            });

            return (
              <StyledTableRow
                key={item.subjectPcrtestId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectPcrtestId}
                    text={helpers.util.dateformat(
                      item.pcrDtc,
                      format.YYYY_MM_DD
                    )}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={specimen}>{specimen}</Table.Cell>
                <Table.Cell title={pcrResult}>{pcrResult}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectPcrtestId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectPcrtest',
                      prefixLocale: 'activity.pcr',
                      skip: ['subjectPcrtestId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={4}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
