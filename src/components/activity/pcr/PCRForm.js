import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { AFBSpec, PCRResult, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DatePicker } from '../../datepicker';
import { code } from '../../../constants';

const PCRForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>
          {t('activity.pcr.pcrDtc')}
          {t('activity.pcr.pcrDtc.des')}
        </label>
        <DatePicker name="pcrDtc" value={model.pcrDtc} onChange={onChange} />
        {validator.message(t('activity.pcr.pcrDtc'), model.pcrDtc, 'required')}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.pcr.pcrSprcCodeId')}</label>
        <AFBSpec
          name="pcrSprcCodeId"
          value={model.pcrSprcCodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.pcrSprcCodeId === code.AFBSpecOther && (
        <Form.Field required>
          <label>
            {t('activity.pcr.pcrOth')}
            {t('activity.pcr.pcrOth.des')}
          </label>
          <Input name="pcrOth" value={model.pcrOth} onChange={onChange} />
          {validator.message(
            t('activity.pcr.pcrOth'),
            model.pcrOth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group grouped>
        <label>{t('activity.pcr.pcrResultCodeId')}</label>
        <PCRResult
          name="pcrResultCodeId"
          value={model.pcrResultCodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.subjectPcrtestId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.pcr.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default PCRForm;
