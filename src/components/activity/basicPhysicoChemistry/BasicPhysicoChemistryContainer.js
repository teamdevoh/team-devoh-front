import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { useFormFields, useFormatMessage } from '../../../hooks';
import { usePublishStatus, useMoveToNextStep } from '../../collection/hooks';
import { ActionBar, FormTitle, FormMetaBar } from '../';
import { AuthorInfo } from '../../collection';
import { notification } from '../../modal';
import BasicPhysicoChemistryForm from './BasicPhysicoChemistryForm';
import { useBasicPhysicoChemistry, useActionStatus } from '../hooks';
import tbdrugApi from '../../collection-tbdrug/api';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useStatus from '../hooks/useStatus';
import { code } from '../../../constants';

const MAX_FIELD_CNT = 3;
const BasicPhysicoChemistryContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    metaboliteReferenceId
  } = props;
  const key = 'tbdrugPhysicoId';
  const handleChangeStatus = activityStatusCodeId => {
    setModel({
      ...model,
      activityStatusCodeId
    });
  };
  const { submitted, isNext } = useActionStatus(handleChangeStatus);
  const { add, update, fetch, isFetching, isSaving } = useBasicPhysicoChemistry(
    {
      activityKey
    }
  );
  const publish = usePublishStatus({
    projectActivityId
  });
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const [refInfos, setRefInfos] = useState({});
  const [addFieldCnt, setAddFieldCnt] = useState({
    logP: 1,
    logD: 1,
    pKa: 1,
    psa: 1
  });
  const t = useFormatMessage();

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields({
    tbdrugId: subjectId,
    [key]: 0,
    activityStatusCodeId: code.Temp,
    tbdrugType: null,
    tbdrugTypeReferenceId: null,
    formula: null,
    formulaReferenceId: null,
    mw: null,
    mwReferenceId: null,
    compounudType: null,
    compounudTypeReferenceId: null,
    logP1: null,
    logP1ReferenceId: null,
    logD1: null,
    logD1ReferenceId: null,
    pKa1: null,
    pKa1ReferenceId: null,
    psa1: null,
    psa1ReferenceId: null,
    hbd: null,
    hbdreferenceId: null,
    isChanged: false,
    reason: null
  });

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(() => {
    fetchItem();
  }, []);

  const fetchItem = useCallback(
    async () => {
      const data = await fetch({ tbdrugId: subjectId, projectActivityId });
      const newModel = {
        ...model,
        ...data,
        projectActivityId,
        isDone: data.activityStatusCodeId === code.Temp ? false : true
      };

      // reference title 받아오기
      const referenceIdArr = new Set();

      for (let key in newModel) {
        if (newModel.hasOwnProperty(key)) {
          if (
            key.toLowerCase().indexOf('referenceid') >= 0 &&
            !!newModel[key]
          ) {
            referenceIdArr.add(newModel[key]);
          }
        }
      }

      if (!!metaboliteReferenceId) {
        referenceIdArr.add(metaboliteReferenceId);
      }

      if (referenceIdArr.size > 0) {
        const refRes = await tbdrugApi.fetchReferencesByIds({
          referenceIds: [...referenceIdArr].join(',')
        });
        setRefInfos(refRes.data);
      }

      //
      const newAddFieldCnt = {
        ...addFieldCnt
      };
      for (let i = 1; i < MAX_FIELD_CNT; i++) {
        const curNum = i + 1;
        for (let key in addFieldCnt) {
          if (addFieldCnt.hasOwnProperty(key)) {
            const curKey = `${key}${curNum}`;
            if (!!newModel[curKey]) {
              newAddFieldCnt[key] = curNum;
            }
          }
        }
      }

      setAddFieldCnt(newAddFieldCnt);

      setModel(newModel);
    },
    [model]
  );

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update({ model });
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            publish.activityStatus(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add({ model });
      if (newId) {
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            publish.activityStatus(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleFileModalClosed = useCallback((value, updated) => {
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    status.note.setHasNote(value);
  }, []);

  return (
    <Fragment>
      <LeavingGuard shouldBlock={model.isChanged} />
      <FormTitle text="Basic Physico-Chemistry" />
      <FormMetaBar>
        <AuthorInfo date={model.modified} name={model.author} />
        <StatusWrapper
          subjectId={subjectId}
          onChangeStatusCb={handleChangeStatus}
          activityKeyId={model[key]}
          projectActivityId={projectActivityId}
          note={{
            value: status.note.hasNote,
            closed: handleNoteModalClosed
          }}
          query={{
            value: status.qna.query.queryStatusCodeId,
            closed: handleQueryModalClosed
          }}
          file={{
            value: status.file.hasFile,
            closed: handleFileModalClosed
          }}
          activity={{
            value: model.activityStatusCodeId,
            closed: () => {}
          }}
          audit={{
            entityName: 'TbdrugPhysico',
            prefixLocale: 'activity.physico',
            skip: [key, 'tbdrugId']
          }}
        />
      </FormMetaBar>
      <BasicPhysicoChemistryForm
        projectId={projectId}
        tbdrugId={subjectId}
        loading={isFetching.value}
        model={model}
        onChange={onChange}
        onSubmit={handleSubmit}
        refInfos={refInfos}
        setModel={setModel}
        addFieldCnt={addFieldCnt}
        setAddFieldCnt={setAddFieldCnt}
        MAX_FIELD_CNT={MAX_FIELD_CNT}
        {...props}
      >
        <ActionBar
          statusCode={model.activityStatusCodeId}
          isDone={model.isDone}
          subjectId={subjectId}
          projectActivityId={projectActivityId}
          activityKeyId={model[key]}
          loading={isSaving}
          onClick={submitted}
        />
      </BasicPhysicoChemistryForm>
    </Fragment>
  );
};

export default BasicPhysicoChemistryContainer;
