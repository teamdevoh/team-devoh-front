import React, { Fragment, useCallback } from 'react';
import {
  Form,
  Input,
  Popup,
  Button,
  Icon,
  Radio,
  Divider
} from 'semantic-ui-react';
import { Uncheck, SelectReason } from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import { FormDoneOrTempBar } from '../';
import helpers from '../../../helpers';
import { UncheckWrap, StyledLabel, StyledButton } from '../styles';

const BasicPhysicoChemistryForm = ({
  MAX_FIELD_CNT,
  children,
  projectId,
  tbdrugId,
  loading,
  model,
  onChange,
  onSubmit,
  refInfos,
  setModel,
  genericName,
  shortName,
  drugBankId,
  metabolite,
  metaboliteReferenceId,
  addFieldCnt,
  setAddFieldCnt
}) => {
  const t = useFormatMessage();
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  const handleOpenDrugBanck = e => {
    e.preventDefault();
    window.open(`https://www.drugbank.ca/drugs/${drugBankId}`, '_blank');
  };

  const handleAddField = key => () => {
    if (addFieldCnt[key] < MAX_FIELD_CNT) {
      const count = addFieldCnt[key] + 1;
      const modelKey = `${key}${count}`;

      setAddFieldCnt({
        ...addFieldCnt,
        [key]: count
      });

      setModel({
        ...model,
        [modelKey]: '',
        [`${modelKey}ReferenceId`]: ''
      });
    }
  };

  const handleRemoveField = key => () => {
    const orgTotalCount = addFieldCnt[key];
    const count = addFieldCnt[key] - 1;
    let newModel = { ...model };

    setAddFieldCnt({
      ...addFieldCnt,
      [key]: count
    });

    delete newModel[`${key}${orgTotalCount}`];
    delete newModel[`${key}${orgTotalCount}ReferenceId`];

    setModel(newModel);
  };

  const handleSelecte = modelName => title => {
    onChange(null, { name: modelName, value: title });
  };

  const getRefTitle = id => {
    if (typeof refInfos[id] !== 'undefined') {
      return refInfos[id].title;
    }

    return '';
  };

  const generateField = useCallback(
    ({ key, count, label, addBtnTitle }) => {
      const result = [];

      for (let i = 0; i < count; i++) {
        const modelCount = i + 1;
        const modelKey = `${key}${modelCount}`;

        const addBtnDisabled = (() => {
          for (let n = 0; n < addFieldCnt[key]; n++) {
            if (typeof model[`${key}${n + 1}`] !== 'undefined') {
              if (!model[`${key}${n + 1}`]) {
                return true;
              }
            }
          }
          return false;
        })();

        result.push(
          <Fragment key={modelKey}>
            <Form.Field>
              <StyledLabel>
                {label} {count > 1 && i + 1}
              </StyledLabel>
              {i === count - 1 && (
                // 추가된 마지막 필드에 x랑 add 버튼 둠
                <Fragment>
                  {i !== 0 && (
                    <StyledButton icon onClick={handleRemoveField(key)}>
                      <Icon name="x" />
                    </StyledButton>
                  )}
                  {count < MAX_FIELD_CNT && (
                    <StyledButton
                      type="button"
                      disabled={addBtnDisabled}
                      onClick={handleAddField(key)}
                    >
                      {addBtnTitle}
                    </StyledButton>
                  )}
                </Fragment>
              )}
              <Input
                name={modelKey}
                value={model[modelKey] || ''}
                onChange={onChange}
              />
            </Form.Field>
            <Form.Field>
              <SearchReferenceInput
                searchValue={getRefTitle([model[`${modelKey}ReferenceId`]])}
                name={`${modelKey}ReferenceId`}
                value={model[`${modelKey}ReferenceId`]}
                onChange={onChange}
                onSelecteItem={handleSelecte(`${modelKey}Reference`)}
              />
            </Form.Field>
          </Fragment>
        );
      }

      return result;
    },
    [model]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field widths="equal">
        <label>Generic Name</label>
        <a
          style={{ cursor: 'pointer' }}
          onClick={() => {
            helpers.history.push(
              `/project/${projectId}/collection/tbdrug/${tbdrugId}`
            );
          }}
        >
          {genericName}
        </a>
      </Form.Field>
      <Form.Field>
        <label>Short Name</label>
        {shortName}
      </Form.Field>
      <Form.Field>
        <label>Drug Bank Id</label>
        {drugBankId}
        <Popup
          trigger={
            <Button
              icon
              onClick={handleOpenDrugBanck}
              style={{ margin: 0, background: 'transparent' }}
              role="button"
              circular
            >
              <Icon name="globe" />
            </Button>
          }
          content={t('open.drug.bank')}
          inverted
        />
      </Form.Field>
      <Form.Field>
        <label>Metabolite</label>
        {metabolite}
      </Form.Field>
      <Form.Field>
        {(() => {
          const refTitle = getRefTitle([metaboliteReferenceId]);
          if (!!refTitle) {
            return `(Reference) ${refTitle}`;
          }
          return null;
        })()}
      </Form.Field>
      <Divider />
      <Form.Field>
        <label>Type</label>
        <Radio
          name="tbdrugType"
          label="active pharmacological agent"
          value="active pharmacological agent"
          checked={'active pharmacological agent' === model.tbdrugType}
          onChange={onChange}
        />
        {'active pharmacological agent' === model.tbdrugType && (
          <UncheckWrap>
            <Uncheck name="tbdrugType" clearValue={''} onClick={onChange} />
          </UncheckWrap>
        )}
        <Radio
          style={{ marginLeft: '10px' }}
          name="tbdrugType"
          label="prodrug"
          value="prodrug"
          checked={'prodrug' === model.tbdrugType}
          onChange={onChange}
        />
        {'prodrug' === model.tbdrugType && (
          <UncheckWrap>
            <Uncheck name="tbdrugType" clearValue={''} onClick={onChange} />
          </UncheckWrap>
        )}
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.tbdrugTypeReferenceId])}
          name="tbdrugTypeReferenceId"
          value={model.tbdrugTypeReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Formula</label>
        <Input name="formula" value={model.formula || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.formulaReferenceId])}
          name="formulaReferenceId"
          value={model.formulaReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>M.W</label>
        <Input name="mw" value={model.mw || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.mwReferenceId])}
          name="mwReferenceId"
          value={model.mwReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Compounud type</label>
        <Input
          name="compounudType"
          value={model.compounudType || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.compounudTypeReferenceId])}
          name="compounudTypeReferenceId"
          value={model.compounudTypeReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      {generateField({
        key: 'pKa',
        count: addFieldCnt.pKa,
        label: 'pKa',
        addBtnTitle: 'Add pKa'
      })}
      {generateField({
        key: 'logP',
        count: addFieldCnt.logP,
        label: 'LogP',
        addBtnTitle: 'Add LogP'
      })}
      {generateField({
        key: 'logD',
        count: addFieldCnt.logD,
        label: 'LogD',
        addBtnTitle: 'Add LogD'
      })}
      {generateField({
        key: 'psa',
        count: addFieldCnt.psa,
        label: 'PSA(Polar Surface Area, A˚)',
        addBtnTitle: 'Add PSA'
      })}
      <Form.Field>
        <label>{t('activity.physico.hbd')}</label>
        <Input name="hbd" value={model.hbd || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.hbdreferenceId])}
          name="hbdreferenceId"
          value={model.hbdreferenceId}
          onChange={onChange}
        />
      </Form.Field>
      {model.tbdrugPhysicoId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="Basic Physico-Chemistry"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default BasicPhysicoChemistryForm;
