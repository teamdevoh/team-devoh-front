import React from 'react';
import styled from 'styled-components';
import { Grid } from 'semantic-ui-react';

const LeftPanel = styled.div`
  float: left;
  font-size: 0.8em;
  color: rgba(0, 0, 0, 0.5);
`;
const RightPanel = styled.div`
  float: right;
`;

const FormMetaBar = ({ children }) => {
  return (
    <Grid>
      <Grid.Column width="ten">
        <LeftPanel>{children[0]}</LeftPanel>
      </Grid.Column>
      <Grid.Column width="six">
        <RightPanel>{children[1]}</RightPanel>
      </Grid.Column>
    </Grid>
  );
};

export default FormMetaBar;
