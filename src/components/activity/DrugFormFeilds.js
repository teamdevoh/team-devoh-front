import React, { Fragment } from 'react';
import { Form, Input, Dropdown, Radio, Divider } from 'semantic-ui-react';
import {
  FlexField,
  StyledCheckbox,
  StyledInput,
  SmallInput,
  DrugsFormGroup,
  FlexibleInput,
  RemoveFieldBtnWrap,
  UncheckWrap
} from './styles';
import { Uncheck } from './Selector';
import { TbdrugList } from '../collection-tbdrug/Selector';
import { useFormatMessage } from '../../hooks';

const AgeFeild = ({ onChange, model }) => {
  return (
    <Fragment>
      <Form.Group widths="equal">
        <FlexField>
          <SmallInput
            label="Mean"
            name="ageMean"
            value={model.ageMean || ''}
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <SmallInput
            label="SD"
            name="ageSd"
            onChange={onChange}
            value={model.ageSd || ''}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <SmallInput
            label="Median"
            name="ageMedian"
            value={model.ageMedian || ''}
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <SmallInput
            label="Min"
            name="ageMin"
            onChange={onChange}
            value={model.ageMin || ''}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <SmallInput
            label="Max"
            name="ageMax"
            value={model.ageMax || ''}
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField inline>
          <label>Unit</label>
          <Dropdown
            style={{ marginTop: 'auto', flex: 1 }}
            name="ageUnit"
            fluid
            selection
            value={model.ageUnit || 'Year'}
            onChange={onChange}
            options={[
              {
                text: 'Year',
                value: 'Year'
              },
              {
                text: 'Month',
                value: 'Month'
              },
              {
                text: 'Week',
                value: 'Week'
              }
            ]}
          />
        </FlexField>
      </Form.Group>
      <Form.Field>
        <Input
          label="Other"
          name="ageOther"
          value={model.ageOther || ''}
          onChange={onChange}
        />
      </Form.Field>
    </Fragment>
  );
};

const StatusFeild = ({ onChange, model, onCheckboxChange }) => {
  return (
    <Fragment>
      <FlexField>
        <StyledCheckbox
          label="TB"
          name="statusIsTb"
          checked={!!model.statusIsTb}
          onChange={onCheckboxChange}
        />
        <StyledInput
          label="Disease"
          placeholder="examle) Pulmonary TB, MDR-TB ..."
          name="statusTbDisease"
          value={model.statusTbDisease || ''}
          onChange={onChange}
          disabled={!!!model.statusIsTb}
        />
      </FlexField>
      <FlexField>
        <StyledCheckbox
          label="Healthy"
          name="statusIsHealthy"
          checked={!!model.statusIsHealthy}
          onChange={onCheckboxChange}
        />
        <StyledInput
          label="Disease"
          name="statusHealthyDisease"
          value={model.statusHealthyDisease || ''}
          onChange={onChange}
          disabled={!!!model.statusIsHealthy}
        />
      </FlexField>
      <Form.Field>
        <Input
          label="Other"
          name="statusOther"
          value={model.statusOther || ''}
          onChange={onChange}
        />
      </Form.Field>
    </Fragment>
  );
};

const DoseFeild = ({ onChange, model, modelKeyPair }) => {
  return (
    <Fragment>
      <Form.Group widths="equal">
        <Form.Field>
          <Input
            label="Meaning"
            name={modelKeyPair.meaning}
            value={model[modelKeyPair.meaning] || ''}
            onChange={onChange}
          />
        </Form.Field>
        <FlexField>
          <Input
            label="Dose"
            name={modelKeyPair.dose}
            onChange={onChange}
            value={model[modelKeyPair.dose] || ''}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <Input
            label="Unit"
            // placeholder="mg"
            name={modelKeyPair.unit}
            value={
              model[modelKeyPair.unit] === null ||
              typeof model[modelKeyPair.unit] === 'undefined'
                ? 'mg'
                : model[modelKeyPair.unit]
            }
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
      </Form.Group>
      <Form.Field>
        <Input
          label="Other"
          name={modelKeyPair.other}
          value={model[modelKeyPair.other] || ''}
          onChange={onChange}
        />
      </Form.Field>
    </Fragment>
  );
};

const FedStatusFeild = ({ onChange, model }) => {
  return (
    <Fragment>
      <FlexField>
        <Radio
          name="fedStatus"
          label="Fasting state"
          value="Fasting state"
          checked={'Fasting state' === model.fedStatus}
          onChange={onChange}
        />
        {'Fasting state' === model.fedStatus && (
          <UncheckWrap>
            <Uncheck name="fedStatus" clearValue={''} onClick={onChange} />
          </UncheckWrap>
        )}
        <Radio
          style={{ marginLeft: '10px' }}
          name="fedStatus"
          label="Fed state"
          value="Fed state"
          checked={'Fed state' === model.fedStatus}
          onChange={onChange}
        />
        {'Fed state' === model.fedStatus && (
          <UncheckWrap>
            <Uncheck name="fedStatus" clearValue={''} onClick={onChange} />
          </UncheckWrap>
        )}
        <Radio
          style={{ marginLeft: '10px' }}
          name="fedStatus"
          label="With Food"
          value="With Food"
          checked={'With Food' === model.fedStatus}
          onChange={onChange}
        />
        {'With Food' === model.fedStatus && (
          <UncheckWrap>
            <Uncheck name="fedStatus" clearValue={''} onClick={onChange} />
          </UncheckWrap>
        )}
      </FlexField>
      <Form.Field>
        <Input
          label="Other"
          name="fedStatusOther"
          value={model.fedStatusOther || ''}
          onChange={onChange}
        />
      </Form.Field>
    </Fragment>
  );
};

const ConcomitentyDrugsFeild = ({
  onChange,
  model,
  keyName,
  modelKey,
  i,
  count,
  onRemoveField
}) => {
  const t = useFormatMessage();

  return (
    <Fragment>
      {i !== 0 && <Divider />}
      <DrugsFormGroup widths="equal">
        <FlexField inline>
          <label style={{ position: 'relative' }}>
            {i === count - 1 &&
              i !== 0 && (
                <RemoveFieldBtnWrap>
                  <Uncheck onClick={onRemoveField(keyName)} />
                </RemoveFieldBtnWrap>
              )}
            Drug
          </label>
          <TbdrugList
            style={{ flex: 1 }}
            defaultOption={[
              {
                text: t('common.notSelected'),
                value: ''
              }
            ]}
            fluid
            name={`${modelKey}TbdrugId`}
            value={Number(model[`${modelKey}TbdrugId`])}
            onChange={onChange}
          />
        </FlexField>
        <FlexField>
          <FlexibleInput
            name={`${modelKey}Other`}
            placeholder="Other Drug"
            value={model[`${modelKey}Other`] || ''}
            onChange={onChange}
          />
        </FlexField>
        <FlexField inline>
          <label>Dose</label>
          <FlexibleInput
            style={{ flex: 1 }}
            name={`${modelKey}Dose`}
            value={model[`${modelKey}Dose`] || ''}
            onChange={onChange}
          />
        </FlexField>
        <FlexField>
          <FlexibleInput
            name={`${modelKey}Unit`}
            // placeholder="mg"
            value={
              model[`${modelKey}Unit`] === null ||
              typeof model[`${modelKey}Unit`] === 'undefined'
                ? 'mg'
                : model[`${modelKey}Unit`]
            }
            onChange={onChange}
          />
        </FlexField>
        <FlexField inline>
          <label>Etc</label>
          <FlexibleInput
            style={{ flex: 1 }}
            name={`${modelKey}Etc`}
            value={model[`${modelKey}Etc`] || ''}
            onChange={onChange}
          />
        </FlexField>
      </DrugsFormGroup>
    </Fragment>
  );
};

const PkCommonFeild = ({ onChange, model, modelKeyPair }) => {
  return (
    <Fragment>
      <Form.Group widths="equal">
        <Form.Field>
          <SmallInput
            label="Mean"
            name={modelKeyPair.mean}
            value={model[modelKeyPair.mean] || ''}
            onChange={onChange}
          />
        </Form.Field>
        <FlexField>
          <SmallInput
            label="SD"
            name={modelKeyPair.sd}
            value={model[modelKeyPair.sd] || ''}
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <SmallInput
            label="Median"
            name={modelKeyPair.median}
            value={model[modelKeyPair.median] || ''}
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <SmallInput
            label="Min"
            name={modelKeyPair.min}
            value={model[modelKeyPair.min] || ''}
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <SmallInput
            label="Max"
            name={modelKeyPair.max}
            value={model[modelKeyPair.max] || ''}
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
        <FlexField>
          <SmallInput
            label="Unit"
            name={modelKeyPair.unit}
            value={model[modelKeyPair.unit] || ''}
            onChange={onChange}
            style={{ marginTop: 'auto' }}
          />
        </FlexField>
      </Form.Group>
      <Form.Field>
        <Input
          label="Other"
          name={modelKeyPair.other}
          value={model[modelKeyPair.other] || ''}
          onChange={onChange}
        />
      </Form.Field>
    </Fragment>
  );
};

const PdCommonFeild = ({ onChange, model, modelKeyPair }) => {
  return (
    <Form.Group widths="equal">
      <Form.Field>
        <SmallInput
          label="Mean"
          name={modelKeyPair.mean}
          value={model[modelKeyPair.mean] || ''}
          onChange={onChange}
        />
      </Form.Field>
      <FlexField>
        <SmallInput
          label="SD"
          name={modelKeyPair.sd}
          value={model[modelKeyPair.sd] || ''}
          onChange={onChange}
          style={{ marginTop: 'auto' }}
        />
      </FlexField>
      <FlexField>
        <SmallInput
          label="Median"
          name={modelKeyPair.median}
          value={model[modelKeyPair.median] || ''}
          onChange={onChange}
          style={{ marginTop: 'auto' }}
        />
      </FlexField>
      <FlexField>
        <SmallInput
          label="Min"
          name={modelKeyPair.min}
          value={model[modelKeyPair.min] || ''}
          onChange={onChange}
          style={{ marginTop: 'auto' }}
        />
      </FlexField>
      <FlexField>
        <SmallInput
          label="Max"
          name={modelKeyPair.max}
          value={model[modelKeyPair.max] || ''}
          onChange={onChange}
          style={{ marginTop: 'auto' }}
        />
      </FlexField>
      <FlexField>
        <SmallInput
          label="Unit"
          name={modelKeyPair.unit}
          value={model[modelKeyPair.unit] || ''}
          onChange={onChange}
          style={{ marginTop: 'auto' }}
        />
      </FlexField>
    </Form.Group>
  );
};

export {
  AgeFeild,
  StatusFeild,
  DoseFeild,
  FedStatusFeild,
  ConcomitentyDrugsFeild,
  PkCommonFeild,
  PdCommonFeild
};
