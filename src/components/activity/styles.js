import styled from 'styled-components';
import {
  Form,
  Input,
  Button,
  Checkbox,
  Select,
  Responsive,
  Table
} from 'semantic-ui-react';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

export const FlexField = styled(Form.Field)`
  &&& {
    display: flex;
    align-items: center;
    flex-wrap: wrap;
  }
`;

export const StyledCheckbox = styled(Checkbox)`
  &&& {
    min-width: 200px;
  }
`;

export const StyledInput = styled(Input)`
  &&& {
    flex: 1;

    &.mq-small {
      @media only screen and (max-width: 375px) {
        & input {
          width: 80px !important;
        }
      }
    }
  }
`;

export const SubFieldWrap = styled.div`
  padding-left: 38px;
  ${({ open }) => {
    if (!open) {
      return 'display: none';
    }
  }};
`;

export const OpenFieldLabel = styled.label`
  cursor: pointer;
`;

export const OpenFieldToggleBtn = styled(Button)`
  &&& {
    background-color: transparent;
    padding: 0px;
    margin-left: 10px;

    & > i {
      font-size: 18px;
    }
  }
`;

export const SmallInput = styled(Input)`
  &&& {
    & input {
      width: 80px !important;
    }
    @media only screen and (max-width: 767px) & input {
      width: auto;
    }
  }
`;

export const DrugsFormGroup = styled(Form.Group)`
  &&& {
    @media only screen and (max-width: 1024px) {
      & {
        flex-wrap: wrap;
      }
    }
  }
`;

export const FlexibleInput = styled(Input)`
  &&& {
    @media only screen and (min-width: 767px) and (max-width: 1800px) {
      & input {
        width: 80px !important;
      }
    }
  }
`;

export const RemoveFieldBtnWrap = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  height: 100%;
  left: -28px;
`;

export const UncheckWrap = styled.div`
  & {
    display: inline-block;
    vertical-align: middle;
  }
  & > i {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const StyledB = styled.b`
  color: #db2828;
`;

export const DisabledStyleSelect = styled(Select)`
  &&&.disabled {
    opacity: 0.8;
    cursor: no-drop;
    pointer-events: auto;
  }
`;

export const DisabledtyleSInput = styled(Input)`
  &&&.disabled {
    opacity: 0.8;
    cursor: no-drop;
    pointer-events: auto;

    & input {
      opacity: 0.8;
      color: #000;
    }
  }
`;

export const StyledFormField = styled(Form.Field)`
  &&& {
    display: flex;
    flex-wrap: wrap;
    ${isMobile ? '' : 'justify-content: space-between;'};
  }
`;

export const FieldWrap = styled.div`
  & {
    ${isMobile
      ? 'width: 100%;'
      : `flex: 1;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: flex-end;`};
  }

  & > div:not(:first-child) {
    display: flex;
    flex-wrap: wrap;
    ${isMobile ? 'margin-top: 10px;' : 'margin-left: 10px;'};
  }
`;

export const StyledLabel = styled.label`
  display: inline-block !important;
`;

export const StyledButton = styled(Button)`
  &&& {
    margin-left: 10px;
    margin-bottom: 5px;
  }
`;

export const StyledTableRow = styled(Table.Row)`
  &&& {
    ${props => {
      if (props.selected) {
        return `background-color: #e4fbff !important`;
      }
    }};
  }
`;
