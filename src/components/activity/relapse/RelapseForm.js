import React from 'react';
import { Form } from 'semantic-ui-react';
import { Relapse, SelectReason } from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';

const RelapseForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Group grouped>
        <label>{t('activity.relapse.relapse6mCodeId')}</label>
        <Relapse
          name="relapse6mCodeId"
          value={model.relapse6mCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.relapse.relapse12mCodeId')}</label>
        <Relapse
          name="relapse12mCodeId"
          value={model.relapse12mCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.relapse.relapse24mCodeId')}</label>
        <Relapse
          name="relapse24mCodeId"
          value={model.relapse24mCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.relapse.relapseFinCodeId')}</label>
        <Relapse
          name="relapseFinCodeId"
          value={model.relapseFinCodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.subjectRelapseId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.relapse.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default RelapseForm;
