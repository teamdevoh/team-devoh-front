import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import helpers from '../../../helpers';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, codeGroup, format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [specCodes] = useCodes({ codeGroupId: codeGroup.AFBSpecCode });
    const [pcrCodes] = useCodes({ codeGroupId: codeGroup.XpertPCRCode });
    const [rifCodes] = useCodes({ codeGroupId: codeGroup.XpertRIFCode });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['xpertdtc'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="two">
              {t('activity.xpert.xpertdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.xpert.xpertspecimenCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">
              {t('activity.xpert.xpertpcrCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">
              {t('activity.xpert.xpertrifCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const specimen = build.displayName({
              source: specCodes,
              value: item.xpertspecimenCodeId,
              other: item.xpertspecothe,
              otherValue: code.AFBSpecOther
            });
            const pcr = build.displayName({
              source: pcrCodes,
              value: item.xpertpcrCodeId
            });
            const rif = build.displayName({
              source: rifCodes,
              value: item.xpertrifCodeId
            });

            return (
              <StyledTableRow
                key={item.subjectXpertId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectXpertId}
                    text={helpers.util.dateformat(
                      item.xpertdtc,
                      format.YYYY_MM_DD
                    )}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={specimen}>{specimen}</Table.Cell>
                <Table.Cell title={pcr}>{pcr}</Table.Cell>
                <Table.Cell title={rif}>{rif}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectXpertId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectXpert',
                      prefixLocale: 'activity.xpert',
                      skip: ['subjectXpertId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={5}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
