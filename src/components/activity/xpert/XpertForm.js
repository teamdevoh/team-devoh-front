import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { AFBSpec, XpertPCR, XpertRIF, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DatePicker } from '../../datepicker';
import { code } from '../../../constants';

const XpertForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>
          {t('activity.xpert.xpertdtc')}
          {t('activity.xpert.xpertdtc.des')}
        </label>
        <DatePicker
          name="xpertdtc"
          value={model.xpertdtc}
          onChange={onChange}
        />
        {validator.message(
          t('activity.xpert.xpertdtc'),
          model.xpertdtc,
          'required'
        )}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.xpert.xpertspecimenCodeId')}</label>
        <AFBSpec
          name="xpertspecimenCodeId"
          value={model.xpertspecimenCodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.xpertspecimenCodeId === code.AFBSpecOther && (
        <Form.Field required>
          <label>{t('activity.xpert.xpertspecothe')}</label>
          <Input
            name="xpertspecothe"
            value={model.xpertspecothe}
            onChange={onChange}
          />
          {validator.message(
            t('activity.xpert.xpertspecothe'),
            model.xpertspecothe,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.xpert.xpertpcrCodeId')}</label>
          <XpertPCR
            name="xpertpcrCodeId"
            value={model.xpertpcrCodeId}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.xpert.xpertrifCodeId')}</label>
          <XpertRIF
            name="xpertrifCodeId"
            value={model.xpertrifCodeId}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {model.subjectXpertId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.xpert.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default XpertForm;
