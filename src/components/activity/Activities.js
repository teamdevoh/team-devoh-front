import React from 'react';
import { useParams, useLocation } from 'react-router-dom';
import helpers from '../../helpers';
import { ConsentContainer } from './consent';
import { InterviewContainer } from './interview';
import { BodyMeasureContainer } from './bodyMeasurement';
import { ICREContainer } from './icre';
import { DiagnosisContainer } from './diagnosis';
import { TdmconcentrationContainer } from './tdmconcentration';
import { ComorbidContainer } from './comorbid';
import { TBDurgContainer } from './subTBDrug';
import { CMContainer } from './cm';
import { AdrAgepContainer } from './adrAgep';
import { AdrDressContainer } from './adrDress';
import { AdrSjstenContainer } from './adrSjsten';
import { AFBContainer } from './afb';
import { CaseConclusionContainer } from './caseConclusion';
import { XpertContainer } from './xpert';
import { RapidContainer } from './rapid';
import { IGRAContainer } from './igra';
import { PCRContainer } from './pcr';
import { XRayContainer } from './xray';
import { CTContainer } from './ct';
import { DSTContainer } from './dst';
import { MicresultContainer } from './micresult';
import { MicreportContainer } from './micreport';
import { ResGeneContainer } from './resgene';
import { TdmsamplingContainer } from './tdmsampling';
import { TdmreportContainer } from './tdmreport';
import { GenotypingContainer } from './genotyping';
import { MictransferContainer } from './mictransfer';
import { LBCHContainer } from './lbch';
import { LBHEMContainer } from './lbhem';
import { BasicPhysicoChemistryContainer } from './basicPhysicoChemistry';
import { AdmeBasicContainer } from './admeBasic';
import { InhibitionOrInductionContainer } from './inhibitionOrInduction';
import { MeTransporterContainer } from './meTransporter';
import { PkDataContainer } from './pkData';
import { FactorsAffectingContainer } from './factorsAffecting';
import { PgxInteractionsContainer } from './pgxInteractions';
import { PdDataContainer } from './pdData';
import { DdiContainer } from './ddi';
import { DialysisContainer } from './dialysis';
import { ElectrolyteContainer } from './electrolyte';
import { OtherExamContainer } from './otherexam';
import { ToxicityContainer } from './toxicity';
import { ModelingContainer } from './modeling';
import { TDMDrugContainer } from './tdmdrug';
import { ADRContainer } from './adr';
import { RelapseContainer } from './relapse';
import { activity } from '../../constants';
import { PkParamContainer } from './pkParameters';

const Activities = props => {
  const params = useParams();
  const location = useLocation();
  const query = helpers.qs.parse(location.search);

  return [
    {
      name: 'ADR',
      key: activity.ADR,
      component: (
        <ADRContainer
          activityKey={activity.ADR}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'ADR_AGEP',
      key: activity.ADR_AGEP,
      component: (
        <AdrAgepContainer
          activityKey={activity.ADR_AGEP}
          {...params}
          {...query}
        />
      )
    },
    {
      name: 'ADR_DRESS',
      key: activity.ADR_DRESS,
      component: (
        <AdrDressContainer
          activityKey={activity.ADR_DRESS}
          {...params}
          {...query}
        />
      )
    },
    {
      name: 'ADR_SJSTEN',
      key: activity.ADR_SJSTEN,
      component: (
        <AdrSjstenContainer
          activityKey={activity.ADR_SJSTEN}
          {...params}
          {...query}
        />
      )
    },
    {
      name: 'AFB',
      key: activity.AFB,
      component: (
        <AFBContainer
          activityKey={activity.AFB}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'CASE_CON',
      key: activity.CASE_CON,
      component: (
        <CaseConclusionContainer
          activityKey={activity.CASE_CON}
          {...params}
          {...query}
        />
      )
    },
    {
      name: 'COMED',
      key: activity.COMED,
      component: (
        <CMContainer
          activityKey={activity.COMED}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'COMORBID',
      key: activity.COMORBID,
      component: (
        <ComorbidContainer
          activityKey={activity.COMORBID}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'CONSENT',
      key: activity.CONSENT,
      component: (
        <ConsentContainer
          activityKey={activity.CONSENT}
          {...params}
          {...query}
        />
      )
    },
    {
      name: 'CT',
      key: activity.CT,
      component: (
        <CTContainer
          activityKey={activity.CT}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'DIALYSIS',
      key: activity.DIALYSIS,
      component: (
        <DialysisContainer
          activityKey={activity.DIALYSIS}
          {...params}
          {...query}
        />
      )
    },
    {
      name: 'DST',
      key: activity.DST,
      component: (
        <DSTContainer
          activityKey={activity.DST}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'ELECTROLYTE',
      key: activity.ELECTROLYTE,
      component: (
        <ElectrolyteContainer
          activityKey={activity.ELECTROLYTE}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'GENOTYPE',
      key: activity.GENOTYPE,
      component: (
        <GenotypingContainer
          activityKey={activity.GENOTYPE}
          {...params}
          {...query}
        />
      )
    },
    {
      name: 'ICRE',
      key: activity.ICRE,
      component: (
        <ICREContainer activityKey={activity.ICRE} {...params} {...query} />
      )
    },
    {
      name: 'IGRA',
      key: activity.IGRA,
      component: (
        <IGRAContainer
          activityKey={activity.IGRA}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'INTERVIEW',
      key: activity.INTERVIEW,
      component: (
        <InterviewContainer
          activityKey={activity.INTERVIEW}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'LBCH',
      key: activity.LBCH,
      component: (
        <LBCHContainer
          activityKey={activity.LBCH}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'LBHEM',
      key: activity.LBHEM,
      component: (
        <LBHEMContainer
          activityKey={activity.LBHEM}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'LPARESISTANT',
      key: activity.LPARESISTANT,
      component: (
        <RapidContainer
          activityKey={activity.LPARESISTANT}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'MEDICATION',
      key: activity.MEDICATION,
      component: (
        <TBDurgContainer
          activityKey={activity.MEDICATION}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'MIC_RESULT',
      key: activity.MIC_RESULT,
      component: (
        <MicresultContainer
          activityKey={activity.MIC_RESULT}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    { name: 'MIC_SETUP', key: activity.MIC_SETUP, component: <div /> },
    {
      name: 'MIC_TRNS',
      key: activity.MIC_TRNS,
      component: (
        <MictransferContainer
          activityKey={activity.MIC_TRNS}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'Other_exam',
      key: activity.Other_exam,
      component: (
        <OtherExamContainer
          activityKey={activity.Other_exam}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'PCR',
      key: activity.PCR,
      component: (
        <PCRContainer
          activityKey={activity.PCR}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'PP',
      key: activity.PP,
      component: (
        <PkParamContainer
          activityKey={activity.PP}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'RESISTANT_GENE',
      key: activity.RESISTANT_GENE,
      component: (
        <ResGeneContainer
          activityKey={activity.RESISTANT_GENE}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'SAMPLING',
      key: activity.SAMPLING,
      component: (
        <TdmsamplingContainer
          activityKey={activity.SAMPLING}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'TB_DIAGNOSIS',
      key: activity.TB_DIAGNOSIS,
      component: (
        <DiagnosisContainer
          activityKey={activity.TB_DIAGNOSIS}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'TB_PK',
      key: activity.TB_PK,
      component: (
        <TdmconcentrationContainer
          activityKey={activity.TB_PK}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'TDMREP',
      key: activity.TDMREP,
      component: (
        <TdmreportContainer
          activityKey={activity.TDMREP}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'BodyMeasurement',
      key: activity.VS,
      component: (
        <BodyMeasureContainer
          activityKey={activity.VS}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'XPERT',
      key: activity.XPERT,
      component: (
        <XpertContainer
          activityKey={activity.XPERT}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'XRAY',
      key: activity.XRAY,
      component: (
        <XRayContainer
          activityKey={activity.XRAY}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'BasicPhysicoChemistry',
      key: activity.BasicPhysico,
      component: (
        <BasicPhysicoChemistryContainer
          activityKey={activity.BasicPhysico}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'AdmeBasic',
      key: activity.AdmeBasic,
      component: (
        <AdmeBasicContainer
          activityKey={activity.AdmeBasic}
          {...params}
          {...query}
        />
      )
    },
    {
      name: 'InhibitionOrInduction',
      key: activity.Inhibition,
      component: (
        <InhibitionOrInductionContainer
          activityKey={activity.Inhibition}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'Me&Transporter',
      key: activity.MeTransporter,
      component: (
        <MeTransporterContainer
          activityKey={activity.MeTransporter}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'PkData',
      key: activity.PkData,
      component: (
        <PkDataContainer
          activityKey={activity.PkData}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'FactorsAffecting',
      key: activity.FactorsAffecting,
      component: (
        <FactorsAffectingContainer
          activityKey={activity.FactorsAffecting}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'PgxInteractions',
      key: activity.PgxInteractions,
      component: (
        <PgxInteractionsContainer
          activityKey={activity.PgxInteractions}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'PdData',
      key: activity.PdData,
      component: (
        <PdDataContainer
          activityKey={activity.PdData}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'Drug-drugInteractions',
      key: activity.DrugInteractions,
      component: (
        <DdiContainer
          activityKey={activity.DrugInteractions}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'ToxicityDataSheet',
      key: activity.ToxicityDataSheet,
      component: (
        <ToxicityContainer
          activityKey={activity.ToxicityDataSheet}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'Modeling',
      key: activity.Modeling,
      component: (
        <ModelingContainer
          activityKey={activity.Modeling}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'TDMAnti-TBDrug',
      key: activity.TDMAnti,
      component: (
        <TDMDrugContainer
          activityKey={activity.TDMAnti}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'MIC-Report',
      key: activity.MICReport,
      component: (
        <MicreportContainer
          activityKey={activity.MICReport}
          {...params}
          {...query}
          {...props}
        />
      )
    },
    {
      name: 'RelapseAfterEndOfTreatment',
      key: activity.Relapse,
      component: (
        <RelapseContainer
          activityKey={activity.Relapse}
          {...params}
          {...query}
          {...props}
        />
      )
    }
  ];
};

export default Activities;
