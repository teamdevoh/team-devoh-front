import AdrDressContainer from './AdrDressContainer';
import AdrDressForm from './AdrDressForm';

// const AdrDressContainer = asyncComponent(() => import('./AdrDressContainer'));
// const AdrDressForm = asyncComponent(() => import('./AdrDressForm'));

export { AdrDressContainer, AdrDressForm };
