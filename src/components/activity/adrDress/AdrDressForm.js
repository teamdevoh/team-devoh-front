import React from 'react';
import { Form } from 'semantic-ui-react';
import {
  YesorNoorUK,
  AdrdressEosin,
  AdrDressOrgan,
  AdrDressRsol,
  SelectReason
} from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';

const AdrDressForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrDress.adrdressFeverCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNoorUK
            name="adrdressFeverCodeId"
            value={model.adrdressFeverCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrDress.adrdressLnCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNoorUK
            name="adrdressLnCodeId"
            value={model.adrdressLnCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrDress.adrdressLympCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNoorUK
            name="adrdressLympCodeId"
            value={model.adrdressLympCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrDress.adrdressEosinCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <AdrdressEosin
            name="adrdressEosinCodeId"
            value={model.adrdressEosinCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrDress.adrdressOrganCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <AdrDressOrgan
            name="adrdressOrganCodeId"
            value={model.adrdressOrganCodeId}
            onChange={onChange}
            width="three"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrDress.adrdressResolCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <AdrDressRsol
            name="adrdressResolCodeId"
            value={model.adrdressResolCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrDress.adrdressDiffdCodeId')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNoorUK
            name="adrdressDiffdCodeId"
            value={model.adrdressDiffdCodeId}
            onChange={onChange}
            width="two"
          />
        </Form.Group>
      </Form.Group>
      {model.subjectAdrDressid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.adrDress.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default AdrDressForm;
