import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '..';
import { AuthorInfo } from '../../collection';
import { TBDrugForm, RepeatList } from '.';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import {
  useSubTBDrugs,
  useSubTBDrug,
  useActionStatus,
  useProjectActivityGet
} from '../hooks';
import { LeavingGuard } from '../../modal';
import StatusWrapper from '../StatusWrapper';
import LinkButton from '../../button/LinkButton';
import helpers from '../../../helpers';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import { activity, activityGroup, code } from '../../../constants';

const TBDurgContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'subjectTbdrugId';
  const tdmDrugActivity = useProjectActivityGet({
    projectId,
    activityGroupId: activityGroup.FirstTDM,
    activityId: activity.TDMAnti
  });

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };
  const [drug] = useTbdrugList();
  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const { add, update, remove, fetch, isSaving, isFetching } = useSubTBDrug({
    activityKey
  });
  const [selectedId, setSelectedId] = useState(0);
  const [repeat, setRepeat] = useSubTBDrugs({ subjectId, projectActivityId });
  const t = useFormatMessage();

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    subjectTbdrugId: '',
    subjectId: subjectId,
    subjectVisitId: 0,
    extrt: '',
    exdrugid: '',
    exdrugoth: '',
    exdecod: '',
    exdose: '',
    exdosfrqcodeId: '',
    exdosfrqoth: '',
    exdosfrmcodeId: '',
    exdosfrmoth: '',
    fdc: '',
    exroutecodeId: '',
    exrouthoth: '',
    exrouthinf: '',
    exstdtc: '',
    exendtc: '',
    modified: null,
    author: '',
    isChanged: false,
    reason: ''
  };

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const { updateRepeatByKey } = useRepeat(key);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      subjectId,
      projectActivityId,
      subjectTbdrugId: model[key]
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    id => {
      setModel({ ...model, subjectTbdrugId: id });
      setSelectedId(id);
    },
    [model, selectedId]
  );

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        subjectTbdrugId: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleMoveToTDM = useCallback(
    () => {
      helpers.history.push(
        `/project/${projectId}/subject/${subjectId}/activitygroup/5?projectActivityId=${
          tdmDrugActivity.projectActivityId
        }&activityId=47`
      );
    },
    [tdmDrugActivity]
  );

  const handleExdrugChange = (e, { name, value }) => {
    const newModel = {
      ...model,
      [name]: value
    };
    if (!!value) {
      const selectedDrug = drug.items.find(x => x.value === value);
      newModel['exdecod'] = !!selectedDrug ? selectedDrug.atccode : '';
    } else {
      newModel['exdecod'] = '';
    }

    setModel(newModel);
  };

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LinkButton
        name={t('activity.tdmdrug.title')}
        color="teal"
        onClick={handleMoveToTDM}
      />
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text={t('activity.subtbdrug.title')} />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'SubjectTbdrug',
                prefixLocale: 'activity.subtbdrug',
                skip: [key]
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <TBDrugForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
            onExdrugChange={handleExdrugChange}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </TBDrugForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default TBDurgContainer;
