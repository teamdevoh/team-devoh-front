import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { /*MyVisit,*/ Dosfrq, Dosfrm, Route, SelectReason } from '../Selector';
import { FormDoneOrTempBar, WHOATC } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { TbdrugList } from '../../collection-tbdrug/Selector';
import { DecimalInput, DateInput } from '../../input';
import { code } from '../../../constants';

const TBDrugForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  onExdrugChange
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  const handleExtrtBlur = e => {
    let { value } = e.currentTarget;

    value = value.trim().toLowerCase();

    if (value.indexOf('튜비스') >= 0 || value.indexOf('tubes') >= 0) {
      onChange(e, { name: 'fdc', value: true });
    } else {
      onChange(e, { name: 'fdc', value: false });
    }
  };

  return (
    <Form loading={loading} onSubmit={onValidate}>
      {/* <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field> */}
      <Form.Field>
        <label>
          {t('activity.subtbdrug.extrt')} {t('enter.brand.name')}
        </label>
        <Input
          name="extrt"
          value={model.extrt}
          onChange={onChange}
          onBlur={handleExtrtBlur}
        />
      </Form.Field>
      <Form.Field required>
        <label>{t('activity.subtbdrug.exdrugid')}</label>
        <TbdrugList
          name="exdrugid"
          value={model.exdrugid}
          onChange={onExdrugChange}
          hasCopyButton
        />
        {validator.message(
          t('activity.subtbdrug.exdrugid'),
          model.exdrugid,
          'required'
        )}
      </Form.Field>
      {model.exdrugid === code.TBDrugOther && (
        <Form.Field required>
          <label>{t('activity.subtbdrug.exdrugoth')}</label>
          <Input name="exdrugoth" value={model.exdrugoth} onChange={onChange} />
          {validator.message(
            t('activity.subtbdrug.exdrugoth'),
            model.exdrugoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.subtbdrug.exdecod')}</label>
          <WHOATC name="exdecod" value={model.exdecod} onChange={onChange} />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.subtbdrug.exdose')}</label>
          <Input
            name="exdose"
            value={model.exdose}
            label={{ basic: true, content: 'mg' }}
            labelPosition="right"
            placeholder=""
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Field>
        <label>{t('activity.subtbdrug.exdosfrqcodeId')}</label>
        <Dosfrq
          name="exdosfrqcodeId"
          value={model.exdosfrqcodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.exdosfrqcodeId === code.DosfrqOther && (
        <Form.Field required>
          <label>{t('activity.subtbdrug.exdosfrqoth')}</label>
          <Input
            name="exdosfrqoth"
            value={model.exdosfrqoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.subtbdrug.exdosfrqoth'),
            model.exdosfrqoth,
            'required'
          )}
        </Form.Field>
      )}

      <Form.Field>
        <label>{t('activity.subtbdrug.exdosfrmcodeId')}</label>
        <Dosfrm
          name="exdosfrmcodeId"
          value={model.exdosfrmcodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.exdosfrmcodeId === code.DosfrmOther && (
        <Form.Field required>
          <label>{t('activity.subtbdrug.exdosfrmoth')}</label>
          <Input
            name="exdosfrmoth"
            value={model.exdosfrmoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.subtbdrug.exdosfrmoth'),
            model.exdosfrmoth,
            'required'
          )}
        </Form.Field>
      )}

      <Form.Group inline>
        <label>{t('activity.subtbdrug.fdc')}</label>
        <Form.Radio
          name="fdc"
          label="Yes"
          value={true}
          checked={model.fdc === true}
          onChange={onChange}
        />
        <Form.Radio
          name="fdc"
          label="No"
          value={false}
          checked={model.fdc === false}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Field>
        <label>{t('activity.subtbdrug.exroutecodeId')}</label>
        <Route
          name="exroutecodeId"
          value={model.exroutecodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.exroutecodeId === code.RouteOther && (
        <Form.Field required>
          <label>{t('activity.subtbdrug.exrouthoth')}</label>
          <Input
            name="exrouthoth"
            value={model.exrouthoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.subtbdrug.exrouthoth'),
            model.exrouthoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group widths="equal">
        {model.exroutecodeId !== code.RouteOral && (
          <Form.Field>
            <label>{t('activity.subtbdrug.exrouthinf')}</label>
            <DecimalInput
              name="exrouthinf"
              options={{ suffix: ' hr' }}
              value={model.exrouthinf}
              onChange={onChange}
            />
          </Form.Field>
        )}
        <Form.Field>
          <label>
            {t('activity.subtbdrug.exstdtc')}
            {t('activity.subtbdrug.dtc.des')}
          </label>
          <DateInput
            name="exstdtc"
            value={model.exstdtc}
            placeholder="ukuk-uk-uk"
            options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.subtbdrug.exendtc')}
            {t('activity.subtbdrug.dtc.des')}
          </label>
          <DateInput
            name="exendtc"
            value={model.exendtc}
            placeholder="ukuk-uk-uk"
            options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>

      {model.subjectTbdrugId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.subtbdrug.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default TBDrugForm;
