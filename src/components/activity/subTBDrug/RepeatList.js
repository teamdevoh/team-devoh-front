import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const build = useBuild();
    const t = useFormatMessage();
    const [drug] = useTbdrugList();
    const [dosfrqCodes] = useCodes({ codeGroupId: codeGroup.DosfrqCode });
    const [dosfrmCodes] = useCodes({ codeGroupId: codeGroup.DosfrmCode });
    const [routeCodes] = useCodes({ codeGroupId: codeGroup.RouteCode });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['exstdtc', 'exendtc'], ['asc', 'asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="two">
              {t('activity.subtbdrug.extrt')}
            </Table.HeaderCell>
            <Table.HeaderCell width="two">
              {t('activity.subtbdrug.exdrugid')}
            </Table.HeaderCell>
            <Table.HeaderCell width="one">Dose</Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.subtbdrug.exdosfrqcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.subtbdrug.exdosfrmcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.subtbdrug.exroutecodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="two">
              {t('activity.subtbdrug.exstdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell width="two">
              {t('activity.subtbdrug.exendtc')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const drugName = build.displayName({
              source: drug.items,
              value: item.exdrugid,
              other: item.exdrugoth,
              otherValue: code.TBDrugOther
            });
            const frq = build.displayName({
              source: dosfrqCodes,
              value: item.exdosfrqcodeId,
              other: item.exdosfrqoth,
              otherValue: code.DosfrqOther
            });
            const frm = build.displayName({
              source: dosfrmCodes,
              value: item.exdosfrmcodeId,
              other: item.exdosfrmoth,
              otherValue: code.DosfrmOther
            });
            const route = build.displayName({
              source: routeCodes,
              value: item.exroutecodeId,
              other: item.exrouthoth,
              otherValue: code.RouteOther
            });

            return (
              <StyledTableRow
                key={item.subjectTbdrugId}
                selected={isSeleted(item)}
              >
                <Table.Cell title={item.extrt}>
                  <ActivityItemAnchor
                    id={item.subjectTbdrugId}
                    text={item.extrt}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={drugName}>
                  <ActivityItemAnchor
                    id={item.subjectTbdrugId}
                    text={drugName}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell>{item.exdose}</Table.Cell>
                <Table.Cell title={frq}>{frq}</Table.Cell>
                <Table.Cell title={frm}>{frm}</Table.Cell>
                <Table.Cell title={route}>{route}</Table.Cell>
                <Table.Cell>{item.exstdtc}</Table.Cell>
                <Table.Cell>{item.exendtc}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectTbdrugId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectTbdrug',
                      prefixLocale: 'activity.subtbdrug',
                      skip: ['subjectTbdrugId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={9}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
