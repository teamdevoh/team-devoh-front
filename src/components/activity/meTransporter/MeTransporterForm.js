import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import React, { Fragment } from 'react';
import { Form, Radio, Input } from 'semantic-ui-react';
import styled from 'styled-components';
import { FormDoneOrTempBar } from '../';
import { useSubmit } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import { Uncheck, ExperimentSystem, SelectReason } from '../Selector';
import { StyledFormField, FieldWrap } from '../styles';

const EnFFieldWrap = styled.div`
  display: flex;
  align-items: center;
  @media screen and (max-width: 768px) {
    flex-wrap: wrap;
  }

  & > div:not(:first-child) {
    display: flex;

    @media screen and (max-width: 768px) {
      flex-wrap: wrap;
      margin-top: 10px;
    }

    @media screen and (min-width: 768px) {
      margin-left: 10px;
    }
  }
`;

const MeTransporterForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  refInfos,
  kineticparameterUnitCodes,
  valueoriginCodes
}) => {
  const t = useFormatMessage();
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <SearchReferenceInput
          searchValue={
            !!refInfos[model.referenceId]
              ? refInfos[model.referenceId].title
              : ''
          }
          name="referenceId"
          value={model.referenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Metabolic Enzyme/Transporter</label>
        <EnFFieldWrap>
          <div style={{ width: '100%' }}>
            <Input
              name="enzymeTransporterName"
              value={model.enzymeTransporterName || ''}
              onChange={onChange}
              fluid
            />
          </div>
          <div>
            <Radio
              name="enzymeTransporterType"
              label="Enzyme"
              value="E"
              checked={'E' === model.enzymeTransporterType}
              onChange={onChange}
            />
            {'E' === model.enzymeTransporterType && (
              <Uncheck
                name="enzymeTransporterType"
                clearValue={null}
                onClick={onChange}
              />
            )}
            <Radio
              style={{ marginLeft: '10px' }}
              name="enzymeTransporterType"
              label="Transporter"
              value="T"
              checked={'T' === model.enzymeTransporterType}
              onChange={onChange}
            />
            {'T' === model.enzymeTransporterType && (
              <Uncheck
                name="enzymeTransporterType"
                clearValue={null}
                onClick={onChange}
              />
            )}
          </div>
        </EnFFieldWrap>
      </Form.Field>
      {/* //////////Kinetic parameter//////////// */}
      <Form.Field inline>
        <label>Kinetic parameter</label>
      </Form.Field>
      <StyledFormField inline>
        <span>Vmax Value</span>
        <FieldWrap>
          <Input
            name="kineticparameterVmaxValue"
            value={model.kineticparameterVmaxValue || ''}
            onChange={onChange}
          />
          <div>
            {kineticparameterUnitCodes.map((item, index) => {
              const { text, value } = item;
              return (
                <Fragment key={value}>
                  <Radio
                    style={index > 0 ? { marginLeft: '10px' } : null}
                    name="kineticparameterVmaxUnitCodeId"
                    label={text}
                    value={value}
                    checked={value === model.kineticparameterVmaxUnitCodeId}
                    onChange={onChange}
                  />
                  {value === model.kineticparameterVmaxUnitCodeId && (
                    <Uncheck
                      name="kineticparameterVmaxUnitCodeId"
                      clearValue={''}
                      onClick={onChange}
                    />
                  )}
                </Fragment>
              );
            })}
          </div>
          <Input
            name="kineticparameterVmaxUnitOther"
            value={model.kineticparameterVmaxUnitOther || ''}
            placeholder="Other unit"
            onChange={onChange}
          />
        </FieldWrap>
      </StyledFormField>
      <StyledFormField inline>
        <span>Km Value</span>
        <FieldWrap>
          <Input
            name="kineticparameterKmValue"
            value={model.kineticparameterKmValue || ''}
            onChange={onChange}
          />
          <div>
            {kineticparameterUnitCodes.map((item, index) => {
              const { text, value } = item;
              return (
                <Fragment key={value}>
                  <Radio
                    style={index > 0 ? { marginLeft: '10px' } : null}
                    name="kineticparameterKmUnitCodeId"
                    label={text}
                    value={value}
                    checked={value === model.kineticparameterKmUnitCodeId}
                    onChange={onChange}
                  />
                  {value === model.kineticparameterKmUnitCodeId && (
                    <Uncheck
                      name="kineticparameterKmUnitCodeId"
                      clearValue={''}
                      onClick={onChange}
                    />
                  )}
                </Fragment>
              );
            })}
          </div>
          <Input
            name="kineticparameterKmUnitOther"
            value={model.kineticparameterKmUnitOther || ''}
            placeholder="Other unit"
            onChange={onChange}
          />
        </FieldWrap>
      </StyledFormField>
      {/* //////////Kinetic parameter//////////// */}
      <Form.Field>
        <label>Experiment system</label>
        <ExperimentSystem
          name="experimentsystemCodeId"
          value={model.experimentsystemCodeId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group>
        <Form.Field width="13" inline>
          <label>{t('activity.tbdrugMeTransporter.clInt')}</label>
          <Input name="clInt" value={model.clInt || ''} onChange={onChange} />
        </Form.Field>
        <Form.Field width="3" inline>
          <label> </label>
          <Input
            name="clIntUnit"
            value={model.clIntUnit || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Field>
        <label>{t('activity.tbdrugMeTransporter.fuInc')}</label>
        <Input name="fuInc" value={model.fuInc || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.tbdrugMeTransporter.fuMic')}</label>
        <Input name="fuMic" value={model.fuMic || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>Value origin</label>
        {valueoriginCodes.map((item, index) => {
          const { text, value } = item;
          return (
            <Fragment key={value}>
              <Radio
                style={index > 0 ? { marginLeft: '10px' } : null}
                name="valueoriginCodeId"
                label={text}
                value={value}
                checked={value === model.valueoriginCodeId}
                onChange={onChange}
              />
              {value === model.valueoriginCodeId && (
                <Uncheck
                  name="valueoriginCodeId"
                  clearValue={''}
                  onClick={onChange}
                />
              )}
            </Fragment>
          );
        })}
      </Form.Field>
      <Form.Field>
        <label>Value origin description (if not PGRC)</label>
        <Input
          name="valueoriginDescription"
          value={model.valueoriginDescription || ''}
          onChange={onChange}
        />
      </Form.Field>
      {model.tbdrugMeTransporterId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="Metabolic Enzyme & Transporter (as Substrate)"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default MeTransporterForm;
