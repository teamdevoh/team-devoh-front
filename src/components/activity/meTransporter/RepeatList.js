import React, { useCallback } from 'react';
import { Table, Responsive } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { useBuild } from '../hooks';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsStickyViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;
const RepeatList = ({
  subjectId,
  updateRepeat,
  skip,
  projectActivityId,
  items,
  loading,
  refInfos,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  metabolicEnzymeTransporterCodes,
  kineticparameterUnitCodes,
  experimentsystemCodes,
  valueoriginCodes,
  isSeleted
}) => {
  const t = useFormatMessage();
  const build = useBuild();

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  const kineticDisplayName = useCallback(
    ({ value, unitCodeId, otherUnit }) => {
      let result = '';

      if (!!value) {
        const unit = build.displayName({
          source: kineticparameterUnitCodes,
          value: unitCodeId
        });

        result = `${value} ${unit}${!!otherUnit ? `(${otherUnit})` : ''}`;
      }

      return result;
    },
    [kineticparameterUnitCodes]
  );

  return (
    <div style={{ overflow: 'auto', margin: '1em 0' }}>
      <div
        style={
          isMobile
            ? {}
            : {
                flexShrink: 0,
                width: '150%'
              }
        }
      >
        <Table compact="very" fixed={!isMobile} singleLine={!isMobile}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Reference</Table.HeaderCell>
              <Table.HeaderCell>
                Metabolic
                <br />
                Enzyme/Transporter
              </Table.HeaderCell>
              <Table.HeaderCell>Kinetic parameter</Table.HeaderCell>
              <Table.HeaderCell>Experiment system</Table.HeaderCell>
              <Table.HeaderCell>CLint</Table.HeaderCell>
              <Table.HeaderCell>fuinc</Table.HeaderCell>
              <Table.HeaderCell>fumic</Table.HeaderCell>
              <Table.HeaderCell width="one">Value origin</Table.HeaderCell>
              <Table.HeaderCell>
                Value origin description origin
                <br />
                (if not PGRC)
              </Table.HeaderCell>
              <Table.HeaderCell>{t('status')}</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {items.map(item => {
              const {
                referenceId,
                tbdrugMeTransporterId,
                enzymeTransporterName,
                enzymeTransporterType,
                kineticparameterVmaxValue,
                kineticparameterVmaxUnitCodeId,
                kineticparameterVmaxUnitOther,
                kineticparameterKmValue,
                kineticparameterKmUnitCodeId,
                kineticparameterKmUnitOther,
                experimentsystemCodeId,
                clInt,
                clIntUnit,
                fuInc,
                fuMic,
                valueoriginCodeId,
                valueoriginDescription,
                activityStatusCodeId
              } = item;

              let metabolicEnzymeTransporter = [];

              if (!!enzymeTransporterName) {
                metabolicEnzymeTransporter.push(enzymeTransporterName);
              }

              if (enzymeTransporterType === 'E') {
                metabolicEnzymeTransporter.push('Enzyme');
              }
              if (enzymeTransporterType === 'T') {
                metabolicEnzymeTransporter.push('Transporter');
              }

              const experimentsystem = build.displayName({
                source: experimentsystemCodes,
                value: experimentsystemCodeId
              });
              const valueorigin = build.displayName({
                source: valueoriginCodes,
                value: valueoriginCodeId
              });

              // kinetic parameter
              const kineticparameterStr = [];

              const vmx = kineticDisplayName({
                value: kineticparameterVmaxValue,
                unitCodeId: kineticparameterVmaxUnitCodeId,
                otherUnit: kineticparameterVmaxUnitOther
              });
              const km = kineticDisplayName({
                value: kineticparameterKmValue,
                unitCodeId: kineticparameterKmUnitCodeId,
                otherUnit: kineticparameterKmUnitOther
              });
              if (!!vmx) {
                kineticparameterStr.push(`Vmax ${vmx}`);
              }
              if (!!km) {
                kineticparameterStr.push(`Km ${km}`);
              }

              const clIntStr = `${!!clInt ? clInt : ''}${
                !!clIntUnit ? ` (${clIntUnit})` : ''
              }`;

              return (
                <StyledTableRow
                  key={tbdrugMeTransporterId}
                  selected={isSeleted(item)}
                >
                  <Table.Cell>
                    <ActivityItemAnchor
                      id={tbdrugMeTransporterId}
                      text={
                        !!refInfos[referenceId]
                          ? refInfos[referenceId].title
                          : valueorigin !== ''
                            ? valueorigin
                            : 'unknown'
                      }
                      onClick={onClick}
                    />
                  </Table.Cell>
                  <Table.Cell title={metabolicEnzymeTransporter.join(', ')}>
                    {metabolicEnzymeTransporter.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={kineticparameterStr.join(' ,')}>
                    {kineticparameterStr.join(' ,')}
                  </Table.Cell>
                  <Table.Cell title={experimentsystem}>
                    {experimentsystem}
                  </Table.Cell>
                  <Table.Cell title={clIntStr}>{clIntStr}</Table.Cell>
                  <Table.Cell title={fuInc}>{fuInc}</Table.Cell>
                  <Table.Cell title={fuMic}>{fuMic}</Table.Cell>
                  <Table.Cell title={valueorigin}>{valueorigin}</Table.Cell>
                  <Table.Cell>{valueoriginDescription}</Table.Cell>
                  <Table.Cell>
                    <StatusWrapper
                      subjectId={subjectId}
                      onChangeStatusCb={handleChangeStatus(item)}
                      activityKeyId={tbdrugMeTransporterId}
                      projectActivityId={projectActivityId}
                      note={{
                        value: item.hasNote,
                        closed: onNoteModalClosed
                      }}
                      query={{
                        value: item.queryStatusCodeId,
                        closed: onQueryModalClosed
                      }}
                      file={{
                        value: item.hasFile,
                        closed: onFileModalClosed
                      }}
                      activity={{
                        value: activityStatusCodeId,
                        closed: () => {}
                      }}
                      audit={{
                        entityName: 'TbdrugMeTransporter',
                        prefixLocale: 'activity.tbdrugMeTransporter',
                        skip
                      }}
                      repeatItems={items}
                    />
                  </Table.Cell>
                </StyledTableRow>
              );
            })}
            {loading && (
              <EmptyRowsStickyViewForSemantic
                loading={true}
                columnLength={10}
                size="large"
              />
            )}
          </Table.Body>
        </Table>
      </div>
    </div>
  );
};

export default RepeatList;
