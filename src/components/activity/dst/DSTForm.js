import React, { useState, useEffect } from 'react';
import { Form, Header, Button, Icon } from 'semantic-ui-react';
import { DstResult, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import dstResultFields from './dstResultFields';
import './DST.css';
import { DatePicker } from '../../datepicker';
import { Susceptible } from '../../../constants/code';
import codeApi from '../../code-manage/api';
import { codeGroup } from '../../../constants';
import styled from 'styled-components';

const SelectAllButton = styled(Button)`
  &&& {
    margin-bottom: 16px;
  }
`;

const DSTForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChangeFields,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);
  const [susceptible, setSusceptible] = useState({});

  const fetchSusceptibleCodeId = async () => {
    const res = await codeApi.fetchCode({
      codeGroupId: codeGroup.DstResultCode,
      codeId: Susceptible
    });

    if (res && res.data) {
      setSusceptible({ ...res.data });
    }
  };

  useEffect(
    () => {
      fetchSusceptibleCodeId();
    },
    [Susceptible]
  );

  const handleCheckAllClick = () => {
    const newModel = {
      ...model
    };

    dstResultFields.forEach(name => {
      newModel[name] = Susceptible;
    });

    onChangeFields({ ...newModel });
  };

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>
          {t('activity.dst.dstDtc')}
          {t('activity.dst.dstDtc.des')}
        </label>
        <DatePicker name="dstDtc" value={model.dstDtc} onChange={onChange} />
        {validator.message(t('activity.dst.dstDtc'), model.dstDtc, 'required')}
      </Form.Field>
      <Header as="h4" dividing>
        {t('activity.dst.result')}
      </Header>
      <SelectAllButton
        icon
        basic
        labelPosition="left"
        type="button"
        onClick={handleCheckAllClick}
      >
        <Icon name="check" />
        {t('select.all', { name: susceptible.name })}
      </SelectAllButton>
      {dstResultFields.map(name => {
        return (
          <Form.Group className="dst-selected" key={name}>
            <label style={{ width: '280px' }}>
              {t(`activity.dst.${name}`)}
            </label>
            <DstResult name={name} value={model[name]} onChange={onChange} />
          </Form.Group>
        );
      })}
      {model.subjectDstid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.dst.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default DSTForm;
