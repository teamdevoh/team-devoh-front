const names = [
  'dstAmikacin30mcgCodeId',
  'dstEthambutol2mcgCodeId',
  'dstLevofloxacin2mcgCodeId',
  'dstMoxifloxacin2mcgCodeId',
  'dstOfloxacin4mcgCodeId',
  'dstRifampin40mcgCodeId',
  'dstStreptomycin10mcgCodeId',
  'dstCapreomycin40mcgCodeId',
  'dstCycloserine30mcgCodeId',
  'dstIsoniazid02mcgCodeId',
  'dstIsoniazid1mcgCodeId',
  'dstKanamycin30mcgCodeId',
  'dstPas1mcgCodeId',
  'dstProthionamide40mcgCodeId',
  'dstPyrazazinamideCodeId',
  'dstRifabutin20mcgCodeId',
  'dstLinezolid2mcgCodeId'
];

export default names;
