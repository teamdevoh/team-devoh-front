import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { useVisitListOfSubjectWithDate } from '../../collection/hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import helpers from '../../../helpers';
import { format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const build = useBuild();
    const [visit] = useVisitListOfSubjectWithDate({
      subjectId
    });

    const t = useFormatMessage();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="five">
              {t('activity.visit')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.tdmreport.tdmRepdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.tdmreport.tdmconf')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.tdmreport.tdmconfdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {items.map(item => {
            const {
              activityStatusCodeId,
              subjectTdmreportId,
              subjectVisitId,
              tdmRepdtc,
              tdmconf,
              tdmconfdtc
            } = item;

            const visitName = build.displayName({
              source: visit.items,
              value: subjectVisitId
            });

            const tdmRepDate = helpers.util.dateformat(
              tdmRepdtc,
              format.YYYY_MM_DD
            );

            const tdmconfDate = helpers.util.dateformat(
              tdmconfdtc,
              format.YYYY_MM_DD
            );

            return (
              <StyledTableRow
                key={subjectTdmreportId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={subjectTdmreportId}
                    text={visitName}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={tdmRepdtc}>{tdmRepDate}</Table.Cell>
                <Table.Cell title={tdmconf}>{tdmconf}</Table.Cell>
                <Table.Cell title={tdmconfDate}>{tdmconfDate}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={subjectTdmreportId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectTdmreport',
                      prefixLocale: 'activity.tdmreport',
                      skip: ['subjectTdmreportId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={5}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
