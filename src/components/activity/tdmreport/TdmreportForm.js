import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { MyVisit, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DatePicker } from '../../datepicker';

const TdmreportForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Field widths="equal">
        <label>{t('activity.tdmreport.tdmRepdtc')}</label>
        <DatePicker
          name="tdmRepdtc"
          value={model.tdmRepdtc}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field widths="equal">
        <label>{t('activity.tdmreport.tdmconf')}</label>
        <Input name="tdmconf" value={model.tdmconf || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field widths="equal">
        <label>{t('activity.tdmreport.tdmconfdtc')}</label>
        <DatePicker
          name="tdmconfdtc"
          value={model.tdmconfdtc}
          onChange={onChange}
        />
      </Form.Field>
      {model.subjectTdmreportId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.tdmreport.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default TdmreportForm;
