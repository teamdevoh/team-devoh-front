import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [electrolyteUnitCodes] = useCodes({
      codeGroupId: codeGroup.ElectrolyteUnitCode
    });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['electrolyteDate'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              {t('activity.electrolyte.electrolyteDate')}
            </Table.HeaderCell>
            <Table.HeaderCell>Potassium (K)</Table.HeaderCell>
            <Table.HeaderCell>Magnesium (Mg)</Table.HeaderCell>
            <Table.HeaderCell>Sodium (Na)</Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const kUnitCode = build.displayName({
              source: electrolyteUnitCodes,
              value: item.kUnitCodeId
            });
            const mgUnitCode = build.displayName({
              source: electrolyteUnitCodes,
              value: item.mgUnitCodeId
            });
            const naUnitCode = build.displayName({
              source: electrolyteUnitCodes,
              value: item.naUnitCodeId
            });

            return (
              <StyledTableRow
                key={item.subjectElectrolyteId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectElectrolyteId}
                    text={item.electrolyteDate}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell>
                  {item.k}
                  {kUnitCode}
                </Table.Cell>
                <Table.Cell>
                  {item.mg}
                  {mgUnitCode}
                </Table.Cell>
                <Table.Cell>
                  {item.na}
                  {naUnitCode}
                </Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectElectrolyteId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectElectrolyte',
                      prefixLocale: 'activity.electrolyte',
                      skip: ['subjectElectrolyteId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={5}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
