import React, { useEffect, useState } from 'react';
import { Form, Input, Dropdown } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DateInput } from '../../input';
import { SelectReason } from '../Selector';

const isNumber = value => {
  return !(value === '' || isNaN(value));
};

const validateCheck = model => {
  const { k, kUnitCodeId, mg, mgUnitCodeId, na, naUnitCodeId } = model;

  const isError = {
    k: false,
    mg: false,
    na: false
  };

  if (isNumber(k) && !kUnitCodeId) {
    isError.k = true;
  }
  if (isNumber(mg) && !mgUnitCodeId) {
    isError.mg = true;
  }
  if (isNumber(na) && !naUnitCodeId) {
    isError.na = true;
  }

  return isError;
};

const ElectrolyteForm = ({
  children,
  projectId,
  loading,
  electrolyteUnitCodes,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);
  const [error, setError] = useState({});

  const handleSubmit = () => {
    const isError = validateCheck(model);

    if (isError.k || isError.mg || isError.na) {
      setError({
        ...isError
      });
    } else {
      onValidate();
    }
  };

  useEffect(
    () => {
      const isError = validateCheck(model);

      setError({
        ...isError
      });
    },
    [model]
  );

  return (
    <Form loading={loading} onSubmit={handleSubmit}>
      <Form.Field required>
        <label>{t('activity.electrolyte.electrolyteDate')}</label>
        <DateInput
          name="electrolyteDate"
          value={model.electrolyteDate}
          placeholder="ukuk-uk-uk"
          options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
          onChange={onChange}
        />
        {validator.message(
          t('activity.electrolyte.electrolyteDate'),
          model.electrolyteDate,
          'required'
        )}
      </Form.Field>
      <Form.Group widths="equal">
        <Form.Field>
          <label>Potassium (K)</label>
          <Input
            name="k"
            value={model.k}
            onChange={onChange}
            label={
              <Dropdown
                name="kUnitCodeId"
                value={model.kUnitCodeId}
                options={electrolyteUnitCodes}
                onChange={onChange}
                placeholder="unit"
              />
            }
            labelPosition="right"
          />
          {error.k && (
            <div style={{ color: 'red' }}>{t('please.select.unit')}</div>
          )}
        </Form.Field>
        <Form.Field>
          <label>Magnesium (Mg)</label>
          <Input
            name="mg"
            value={model.mg}
            onChange={onChange}
            label={
              <Dropdown
                name="mgUnitCodeId"
                value={model.mgUnitCodeId}
                options={electrolyteUnitCodes}
                onChange={onChange}
                placeholder="unit"
              />
            }
            labelPosition="right"
          />
          {error.mg && (
            <div style={{ color: 'red' }}>{t('please.select.unit')}</div>
          )}
        </Form.Field>
        <Form.Field>
          <label>Sodium (Na)</label>
          <Input
            name="na"
            value={model.na}
            onChange={onChange}
            label={
              <Dropdown
                name="naUnitCodeId"
                value={model.naUnitCodeId}
                options={electrolyteUnitCodes}
                onChange={onChange}
                placeholder="unit"
              />
            }
            labelPosition="right"
          />
          {error.na && (
            <div style={{ color: 'red' }}>{t('please.select.unit')}</div>
          )}
        </Form.Field>
      </Form.Group>
      {model.subjectElectrolyteId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.electrolyte.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default ElectrolyteForm;
