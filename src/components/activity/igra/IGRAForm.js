import React from 'react';
import { Form } from 'semantic-ui-react';
import { IGRA, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DatePicker } from '../../datepicker';

const IGRAForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>
          {t('activity.igra.igraDtc')}
          {t('activity.igra.igraDtc.des')}
        </label>
        <DatePicker name="igraDtc" value={model.igraDtc} onChange={onChange} />
        {validator.message(
          t('activity.igra.igraDtc'),
          model.igraDtc,
          'required'
        )}
      </Form.Field>
      <Form.Field>
        <label>
          {t('activity.igra.igraCodeId')}
          {t('activity.igra.igraCodeId.des')}
        </label>
        <IGRA name="igraCodeId" value={model.igraCodeId} onChange={onChange} />
      </Form.Field>
      {model.subjectIgraid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.igra.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default IGRAForm;
