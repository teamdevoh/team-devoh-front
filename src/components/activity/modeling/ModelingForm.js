import React from 'react';
import { Form, Input, TextArea, Message } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { SelectReason } from '../Selector';
import { DatePicker } from '../../datepicker';
import FileContainer from '../../activity-file/FileContainer';

const ModelingForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  tbdrugId,
  activityKeyId,
  projectActivityId,
  onFileStatusChange,
  fetchFileList,
  setFetchFileList
}) => {
  const t = useFormatMessage();
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <label>{t('activity.tbdrugModel.modelDate')}</label>
        <DatePicker
          name="modelDate"
          onChange={onChange}
          value={model.modelDate}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.tbdrugModel.modelAuthor')}</label>
        <Input
          name="modelAuthor"
          value={model.modelAuthor || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.tbdrugModel.modelVersion')}</label>
        <Input
          name="modelVersion"
          value={model.modelVersion || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.tbdrugModel.modelDiscription')}</label>
        <TextArea
          name="modelDiscription"
          value={model.modelDiscription || ''}
          onChange={onChange}
        />
      </Form.Field>
      {model.tbdrugModelId > 0 ? (
        <Form.Field>
          <FileContainer
            subjectId={tbdrugId}
            activityKeyId={activityKeyId}
            projectActivityId={projectActivityId}
            setHasFile={onFileStatusChange}
            fetchFileList={fetchFileList}
            setFetchFileList={setFetchFileList}
          />
        </Form.Field>
      ) : (
        <Message>{t('file.upload.info.message')}</Message>
      )}

      {model.tbdrugModelId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="Modeling"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default ModelingForm;
