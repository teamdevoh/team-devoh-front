import React from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';
import helpers from '../../../helpers';
import { format } from '../../../constants';

const RepeatList = ({
  subjectId,
  updateRepeat,
  projectActivityId,
  items,
  loading,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  isSeleted
}) => {
  const t = useFormatMessage();

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  return (
    <Table compact="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>
            {t('activity.tbdrugModel.modelDate')}
          </Table.HeaderCell>
          <Table.HeaderCell>
            {t('activity.tbdrugModel.modelAuthor')}
          </Table.HeaderCell>
          <Table.HeaderCell>
            {t('activity.tbdrugModel.modelVersion')}
          </Table.HeaderCell>
          <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map(item => {
          const {
            activityStatusCodeId,
            tbdrugModelId,
            modelDate,
            modelAuthor,
            modelVersion
          } = item;

          const date = helpers.util.dateformat(modelDate, format.YYYY_MM_DD);

          return (
            <StyledTableRow key={tbdrugModelId} selected={isSeleted(item)}>
              <Table.Cell>
                <ActivityItemAnchor
                  id={tbdrugModelId}
                  text={!!date ? date : 'unknown'}
                  onClick={onClick}
                />
              </Table.Cell>
              <Table.Cell>{modelAuthor}</Table.Cell>
              <Table.Cell>{modelVersion}</Table.Cell>
              <Table.Cell>
                <StatusWrapper
                  subjectId={subjectId}
                  onChangeStatusCb={handleChangeStatus(item)}
                  activityKeyId={tbdrugModelId}
                  projectActivityId={projectActivityId}
                  note={{
                    value: item.hasNote,
                    closed: onNoteModalClosed
                  }}
                  query={{
                    value: item.queryStatusCodeId,
                    closed: onQueryModalClosed
                  }}
                  file={{
                    value: item.hasFile,
                    closed: onFileModalClosed
                  }}
                  activity={{
                    value: activityStatusCodeId,
                    closed: () => {}
                  }}
                  audit={{
                    entityName: 'TbdrugModel',
                    prefixLocale: 'activity.tbdrugModel',
                    skip: ['tbdrugModelId', 'tbdrugId']
                  }}
                  repeatItems={items}
                />
              </Table.Cell>
            </StyledTableRow>
          );
        })}
        {loading && (
          <EmptyRowsViewForSemantic
            loading={true}
            columnLength={4}
            size="large"
          />
        )}
      </Table.Body>
    </Table>
  );
};

export default RepeatList;
