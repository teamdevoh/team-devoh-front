import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '..';
import { ModelingForm, RepeatList } from './';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { useModelList, useActionStatus, useModel } from '../hooks';
import { AuthorInfo } from '../../collection';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useStatus from '../hooks/useStatus';
import useRepeat from '../hooks/useRepeat';
import { code } from '../../../constants';

const ModelingContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'tbdrugModelId';
  const { add, update, remove, fetch, isSaving, isFetching } = useModel({
    activityKey
  });

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const [repeat, setRepeat] = useModelList({
    tbdrugId: subjectId,
    projectActivityId
  });
  const t = useFormatMessage();

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    hasNote: false,
    isDone: false,
    tbdrugModelId: '',
    activityId: null,
    tbdrugId: subjectId,
    modelDate: null,
    modelAuthor: null,
    modelVersion: null,
    modelDiscription: null,
    reason: null,
    modified: null,
    author: null,
    isChanged: false
  };
  const [selectedId, setSelectedId] = useState(0);
  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const { updateRepeatByKey } = useRepeat(key);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  const [fetchFileList, setFetchFileList] = useState(false);

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      tbdrugId: subjectId,
      projectActivityId,
      tbdrugModelId: model[key]
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(id => {
    setModel({ ...model, tbdrugModelId: id });
    setSelectedId(id);
  }, []);

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        tbdrugModelId: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleTbdrugModelFileChange = value => {
    if (status.file.hasFile !== value) {
      status.file.setHasFile(value);
      const newRepeatItems = repeat.items.map(item => {
        if (item[key] === model[key]) {
          return {
            ...item,
            hasFile: value
          };
        } else {
          return item;
        }
      });

      setRepeat(newRepeatItems);
    }
  };

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
    setFetchFileList(true);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
        {...props}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text="Modeling" />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'TbdrugModel',
                prefixLocale: 'activity.tbdrugModel',
                skip: ['tbdrugModelId', 'tbdrugId']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <ModelingForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
            tbdrugId={subjectId}
            activityKeyId={model[key]}
            projectActivityId={projectActivityId}
            onFileStatusChange={handleTbdrugModelFileChange}
            fetchFileList={fetchFileList}
            setFetchFileList={setFetchFileList}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </ModelingForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default ModelingContainer;
