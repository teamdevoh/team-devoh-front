import React, { useCallback } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = ({
  subjectId,
  updateRepeat,
  projectActivityId,
  items,
  loading,
  refInfos,
  metabolicEnzymeTransporterCodes,
  admeinhibitionInductionCodes,
  processTypeCodes,
  kineticparameterUnitCodes,
  experimentsystemCodes,
  valueoriginCodes,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  isSeleted
}) => {
  const t = useFormatMessage();
  const build = useBuild();

  const kineticDisplayName = useCallback(
    ({ value, unitCodeId, otherUnit }) => {
      let result = '';

      if (!!value) {
        const unit = build.displayName({
          source: kineticparameterUnitCodes,
          value: unitCodeId
        });

        result = `${value} ${unit}(${otherUnit})`;
      }

      return result;
    },
    [kineticparameterUnitCodes]
  );

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  return (
    <Table compact="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Reference</Table.HeaderCell>
          <Table.HeaderCell>
            Metabolic
            <br />
            Enzyme/Transporter
          </Table.HeaderCell>
          <Table.HeaderCell>Inhibition/Induction</Table.HeaderCell>
          <Table.HeaderCell>Process type</Table.HeaderCell>
          <Table.HeaderCell>Kinetic parameter</Table.HeaderCell>
          <Table.HeaderCell>Experiment system</Table.HeaderCell>
          <Table.HeaderCell>Value origin</Table.HeaderCell>
          <Table.HeaderCell>
            Value origin description
            <br />
            (if not PGRC)
          </Table.HeaderCell>
          <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map(item => {
          const {
            referenceId,
            tbdrugInhibitionId,
            metabolicEnzymeTransporterCodeId,
            admeinhibitionInductionCodeId,
            processTypeCodeId,
            kineticparameterIc50value,
            kineticparameterIc50unitCodeId,
            kineticparameterIc50unitOther,
            kineticparameterKiValue,
            kineticparameterKiUnitCodeId,
            kineticparameterKiUnitOther,
            kineticparameterEmaxValue,
            kineticparameterEmaxUnitCodeId,
            kineticparameterEmaxUnitOther,
            kineticparameterEc50value,
            kineticparameterEc50unitCodeId,
            kineticparameterEc50unitOther,
            experimentsystemCodeId,
            valueoriginCodeId,
            valueoriginDescription,
            activityStatusCodeId
          } = item;

          const metabolicEnzymeTransporter = build.displayName({
            source: metabolicEnzymeTransporterCodes,
            value: metabolicEnzymeTransporterCodeId
          });
          const admeinhibitionInduction = build.displayName({
            source: admeinhibitionInductionCodes,
            value: admeinhibitionInductionCodeId
          });
          const processType = build.displayName({
            source: processTypeCodes,
            value: processTypeCodeId
          });
          const experimentsystem = build.displayName({
            source: experimentsystemCodes,
            value: experimentsystemCodeId
          });
          const valueorigin = build.displayName({
            source: valueoriginCodes,
            value: valueoriginCodeId
          });

          // kinetic parameter
          const kineticparameterStr = [];

          const ic50 = kineticDisplayName({
            value: kineticparameterIc50value,
            unitCodeId: kineticparameterIc50unitCodeId,
            otherUnit: kineticparameterIc50unitOther
          });
          const ki = kineticDisplayName({
            value: kineticparameterKiValue,
            unitCodeId: kineticparameterKiUnitCodeId,
            otherUnit: kineticparameterKiUnitOther
          });
          const emax = kineticDisplayName({
            value: kineticparameterEmaxValue,
            unitCodeId: kineticparameterEmaxUnitCodeId,
            otherUnit: kineticparameterEmaxUnitOther
          });
          const ec50 = kineticDisplayName({
            value: kineticparameterEc50value,
            unitCodeId: kineticparameterEc50unitCodeId,
            otherUnit: kineticparameterEc50unitOther
          });

          if (!!ic50) {
            kineticparameterStr.push(`IC50 ${ic50}`);
          }
          if (!!ki) {
            kineticparameterStr.push(`Ki ${ki}`);
          }
          if (!!emax) {
            kineticparameterStr.push(`Emax ${emax}`);
          }
          if (!!ec50) {
            kineticparameterStr.push(`EC50 ${ec50}`);
          }

          return (
            <StyledTableRow key={tbdrugInhibitionId} selected={isSeleted(item)}>
              <Table.Cell>
                <ActivityItemAnchor
                  id={tbdrugInhibitionId}
                  text={
                    !!refInfos[referenceId]
                      ? refInfos[referenceId].title
                      : valueorigin !== ''
                        ? valueorigin
                        : 'unknown'
                  }
                  onClick={onClick}
                />
              </Table.Cell>
              <Table.Cell title={metabolicEnzymeTransporter}>
                {metabolicEnzymeTransporter}
              </Table.Cell>
              <Table.Cell title={admeinhibitionInduction}>
                {admeinhibitionInduction}
              </Table.Cell>
              <Table.Cell title={processType}>{processType}</Table.Cell>
              <Table.Cell title={kineticparameterStr.join(' ,')}>
                {kineticparameterStr.join(' ,')}
              </Table.Cell>
              <Table.Cell title={experimentsystem}>
                {experimentsystem}
              </Table.Cell>
              <Table.Cell title={valueorigin}>{valueorigin}</Table.Cell>
              <Table.Cell>{valueoriginDescription}</Table.Cell>
              <Table.Cell>
                <StatusWrapper
                  subjectId={subjectId}
                  onChangeStatusCb={handleChangeStatus(item)}
                  activityKeyId={tbdrugInhibitionId}
                  projectActivityId={projectActivityId}
                  note={{
                    value: item.hasNote,
                    closed: onNoteModalClosed
                  }}
                  query={{
                    value: item.queryStatusCodeId,
                    closed: onQueryModalClosed
                  }}
                  file={{
                    value: item.hasFile,
                    closed: onFileModalClosed
                  }}
                  activity={{
                    value: activityStatusCodeId,
                    closed: () => {}
                  }}
                  audit={{
                    entityName: 'TbdrugInhibition',
                    prefixLocale: 'activity.tbdrugInhibition',
                    skip: ['tbdrugInhibitionId', 'tbdrugId']
                  }}
                  repeatItems={items}
                />
              </Table.Cell>
            </StyledTableRow>
          );
        })}
        {loading && (
          <EmptyRowsViewForSemantic
            loading={true}
            columnLength={9}
            size="large"
          />
        )}
      </Table.Body>
    </Table>
  );
};

export default RepeatList;
