import React, { Fragment, useCallback } from 'react';
import { Form, Radio, Input } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '../';
import { useSubmit } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import {
  MetabolicEnzymeTransporter,
  Uncheck,
  ProcessType,
  ExperimentSystem,
  SelectReason
} from '../Selector';
import { StyledFormField, FieldWrap } from '../styles';

const InhibitionOrInductionForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  refInfos,
  admeinhibitionInductionCodes,
  kineticparameterUnitCodes,
  valueoriginCodes
}) => {
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return '';
    },
    [refInfos]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle(model.referenceId)}
          name="referenceId"
          value={model.referenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Metabolic Enzyme/Transporter</label>
        <MetabolicEnzymeTransporter
          name="metabolicEnzymeTransporterCodeId"
          value={model.metabolicEnzymeTransporterCodeId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Inhibition/Induction</label>
        {admeinhibitionInductionCodes.map((item, index) => {
          const { text, value } = item;
          return (
            <Fragment key={value}>
              <Radio
                style={index > 0 ? { marginLeft: '10px' } : null}
                name="admeinhibitionInductionCodeId"
                label={text}
                value={value}
                checked={value === model.admeinhibitionInductionCodeId}
                onChange={onChange}
              />
              {value === model.admeinhibitionInductionCodeId && (
                <Uncheck
                  name="admeinhibitionInductionCodeId"
                  clearValue={''}
                  onClick={onChange}
                />
              )}
            </Fragment>
          );
        })}
      </Form.Field>
      <Form.Field>
        <label>Process type</label>
        <ProcessType
          name="processTypeCodeId"
          value={model.processTypeCodeId}
          onChange={onChange}
        />
      </Form.Field>
      {/* //////////Kinetic parameter//////////// */}
      <Form.Field inline>
        <label>Kinetic parameter</label>
      </Form.Field>
      <StyledFormField inline>
        <span>IC50 Value</span>
        <FieldWrap>
          <Input
            name="kineticparameterIc50value"
            value={model.kineticparameterIc50value || ''}
            onChange={onChange}
          />
          <div>
            {kineticparameterUnitCodes.map((item, index) => {
              const { text, value } = item;
              return (
                <Fragment key={value}>
                  <Radio
                    style={index > 0 ? { marginLeft: '10px' } : null}
                    name="kineticparameterIc50unitCodeId"
                    label={text}
                    value={value}
                    checked={value === model.kineticparameterIc50unitCodeId}
                    onChange={onChange}
                  />
                  {value === model.kineticparameterIc50unitCodeId && (
                    <Uncheck
                      name="kineticparameterIc50unitCodeId"
                      clearValue={''}
                      onClick={onChange}
                    />
                  )}
                </Fragment>
              );
            })}
          </div>
          <Input
            name="kineticparameterIc50unitOther"
            value={model.kineticparameterIc50unitOther || ''}
            placeholder="Other unit"
            onChange={onChange}
          />
        </FieldWrap>
      </StyledFormField>
      <StyledFormField inline>
        <span>Ki Value</span>
        <FieldWrap>
          <Input
            name="kineticparameterKiValue"
            value={model.kineticparameterKiValue || ''}
            onChange={onChange}
          />
          <div>
            {kineticparameterUnitCodes.map((item, index) => {
              const { text, value } = item;
              return (
                <Fragment key={value}>
                  <Radio
                    style={index > 0 ? { marginLeft: '10px' } : null}
                    name="kineticparameterKiUnitCodeId"
                    label={text}
                    value={value}
                    checked={value === model.kineticparameterKiUnitCodeId}
                    onChange={onChange}
                  />
                  {value === model.kineticparameterKiUnitCodeId && (
                    <Uncheck
                      name="kineticparameterKiUnitCodeId"
                      clearValue={''}
                      onClick={onChange}
                    />
                  )}
                </Fragment>
              );
            })}
          </div>
          <Input
            name="kineticparameterKiUnitOther"
            value={model.kineticparameterKiUnitOther || ''}
            placeholder="Other unit"
            onChange={onChange}
          />
        </FieldWrap>
      </StyledFormField>
      <StyledFormField inline>
        <span>Emax Value</span>
        <FieldWrap>
          <Input
            name="kineticparameterEmaxValue"
            value={model.kineticparameterEmaxValue || ''}
            onChange={onChange}
          />
          <div>
            {kineticparameterUnitCodes.map((item, index) => {
              const { text, value } = item;
              return (
                <Fragment key={value}>
                  <Radio
                    style={index > 0 ? { marginLeft: '10px' } : null}
                    name="kineticparameterEmaxUnitCodeId"
                    label={text}
                    value={value}
                    checked={value === model.kineticparameterEmaxUnitCodeId}
                    onChange={onChange}
                  />
                  {value === model.kineticparameterEmaxUnitCodeId && (
                    <Uncheck
                      name="kineticparameterEmaxUnitCodeId"
                      clearValue={''}
                      onClick={onChange}
                    />
                  )}
                </Fragment>
              );
            })}
          </div>
          <Input
            name="kineticparameterEmaxUnitOther"
            value={model.kineticparameterEmaxUnitOther || ''}
            placeholder="Other unit"
            onChange={onChange}
          />
        </FieldWrap>
      </StyledFormField>
      <StyledFormField inline>
        <span>EC50 Value</span>
        <FieldWrap>
          <Input
            name="kineticparameterEc50value"
            value={model.kineticparameterEc50value || ''}
            onChange={onChange}
          />
          <div>
            {kineticparameterUnitCodes.map((item, index) => {
              const { text, value } = item;
              return (
                <Fragment key={value}>
                  <Radio
                    style={index > 0 ? { marginLeft: '10px' } : null}
                    name="kineticparameterEc50unitCodeId"
                    label={text}
                    value={value}
                    checked={value === model.kineticparameterEc50unitCodeId}
                    onChange={onChange}
                  />
                  {value === model.kineticparameterEc50unitCodeId && (
                    <Uncheck
                      name="kineticparameterEc50unitCodeId"
                      clearValue={''}
                      onClick={onChange}
                    />
                  )}
                </Fragment>
              );
            })}
          </div>
          <Input
            name="kineticparameterEc50unitOther"
            value={model.kineticparameterEc50unitOther || ''}
            placeholder="Other unit"
            onChange={onChange}
          />
        </FieldWrap>
      </StyledFormField>
      {/* //////////Kinetic parameter//////////// */}
      <Form.Field>
        <label>Experiment system</label>
        <ExperimentSystem
          name="experimentsystemCodeId"
          value={model.experimentsystemCodeId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Value origin</label>
        {valueoriginCodes.map((item, index) => {
          const { text, value } = item;
          return (
            <Fragment key={value}>
              <Radio
                style={index > 0 ? { marginLeft: '10px' } : null}
                name="valueoriginCodeId"
                label={text}
                value={value}
                checked={value === model.valueoriginCodeId}
                onChange={onChange}
              />
              {value === model.valueoriginCodeId && (
                <Uncheck
                  name="valueoriginCodeId"
                  clearValue={''}
                  onClick={onChange}
                />
              )}
            </Fragment>
          );
        })}
      </Form.Field>
      <Form.Field>
        <label>Value origin description (if not PGRC)</label>
        <Input
          name="valueoriginDescription"
          value={model.valueoriginDescription || ''}
          onChange={onChange}
        />
      </Form.Field>
      {model.tbdrugInhibitionId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="ADME Inhibition or Induction  (as perpetrator)"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default InhibitionOrInductionForm;
