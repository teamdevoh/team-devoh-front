import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '../';
import { InhibitionOrInductionForm, RepeatList } from './';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields, useCodes } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { useInhibitions, useActionStatus, useInhibition } from '../hooks';
import { AuthorInfo } from '../../collection';
import tbdrugApi from '../../collection-tbdrug/api';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { code, codeGroup } from '../../../constants';

const InhibitionOrInductionContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'tbdrugInhibitionId';
  const { add, update, remove, fetch, isSaving, isFetching } = useInhibition({
    activityKey
  });

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const [repeat, setRepeat] = useInhibitions({
    tbdrugId: subjectId,
    projectActivityId
  });
  const t = useFormatMessage();

  const [metabolicEnzymeTransporterCodes] = useCodes({
    codeGroupId: codeGroup.MetabolicEnzymeTransporterCode
  });
  const [admeinhibitionInductionCodes] = useCodes({
    codeGroupId: codeGroup.ADMEInhibitionInductionCode
  });
  const [processTypeCodes] = useCodes({
    codeGroupId: codeGroup.ProcessTypeCode
  });
  const [kineticparameterUnitCodes] = useCodes({
    codeGroupId: codeGroup.KineticparameterUnitCode
  });
  const [experimentsystemCodes] = useCodes({
    codeGroupId: codeGroup.ExperimentsystemCode
  });
  const [valueoriginCodes] = useCodes({
    codeGroupId: codeGroup.ValueoriginCode
  });

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    modified: null,
    author: '',
    [key]: '',
    activityId: activityKey,
    tbdrugId: subjectId,
    referenceId: null,
    metabolicEnzymeTransporterCodeId: null,
    admeinhibitionInductionCodeId: null,
    processTypeCodeId: null,
    kineticparameterIc50value: null,
    kineticparameterIc50unitCodeId: null,
    kineticparameterIc50unitOther: null,
    kineticparameterKiValue: null,
    kineticparameterKiUnitCodeId: null,
    kineticparameterKiUnitOther: null,
    kineticparameterEmaxValue: null,
    kineticparameterEmaxUnitCodeId: null,
    kineticparameterEmaxUnitOther: null,
    kineticparameterEc50value: null,
    kineticparameterEc50unitCodeId: null,
    kineticparameterEc50unitOther: null,
    experimentsystemCodeId: null,
    valueoriginCodeId: null,
    valueoriginDescription: null,
    isChanged: false,
    reason: null
  };
  const [selectedId, setSelectedId] = useState(0);
  const [refInfos, setRefInfos] = useState({});
  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const { updateRepeatByKey } = useRepeat(key);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      tbdrugId: subjectId,
      projectActivityId,
      [key]: model[key]
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  useEffect(
    () => {
      // reference title 받아오기
      const referenceIdArr = new Set();

      for (let i = 0; i < repeat.items.length; i++) {
        if (!!repeat.items[i].referenceId) {
          referenceIdArr.add(repeat.items[i].referenceId);
        }
      }

      if (referenceIdArr.size > 0) {
        tbdrugApi
          .fetchReferencesByIds({
            referenceIds: [...referenceIdArr].join(',')
          })
          .then(res => {
            setRefInfos(res.data);
          });
      }
    },
    [repeat.items]
  );

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    id => {
      setModel({ ...model, [key]: id });
      setSelectedId(id);
    },
    [model, selectedId]
  );

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        [key]: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        refInfos={refInfos}
        metabolicEnzymeTransporterCodes={metabolicEnzymeTransporterCodes}
        admeinhibitionInductionCodes={admeinhibitionInductionCodes}
        processTypeCodes={processTypeCodes}
        kineticparameterUnitCodes={kineticparameterUnitCodes}
        experimentsystemCodes={experimentsystemCodes}
        valueoriginCodes={valueoriginCodes}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text="ADME Inhibition or Induction  (as perpetrator)" />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'TbdrugInhibition',
                prefixLocale: 'activity.tbdrugInhibition',
                skip: [key, 'tbdrugId']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <InhibitionOrInductionForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
            refInfos={refInfos}
            admeinhibitionInductionCodes={admeinhibitionInductionCodes}
            kineticparameterUnitCodes={kineticparameterUnitCodes}
            valueoriginCodes={valueoriginCodes}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </InhibitionOrInductionForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default InhibitionOrInductionContainer;
