import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '../';

import { PgxInteractionsForm, RepeatList } from './';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields, useCodes } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { usePgxList, useActionStatus, usePgx } from '../hooks';
import { AuthorInfo } from '../../collection';
import tbdrugApi from '../../collection-tbdrug/api';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { code, codeGroup } from '../../../constants';

const PgxInteractionsContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'tbdrugPgxId';
  const { add, update, remove, fetch, isSaving, isFetching } = usePgx({
    activityKey
  });

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };
  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const [repeat, setRepeat] = usePgxList({
    tbdrugId: subjectId,
    projectActivityId
  });
  const t = useFormatMessage();

  const [ethnicCodes] = useCodes({
    codeGroupId: codeGroup.EthnicCode
  });

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    hasNote: false,
    isDone: false,
    modified: null,
    author: '',
    [key]: '',
    activityId: activityKey,
    tbdrugId: subjectId,
    referenceId: null,
    gene: null,
    polymorphismRsid: null,
    polymorphismHgvs: null,
    polymorphismOther: null,
    frequencyIsAfrican: null,
    frequencyAfrican: null,
    frequencyIsAmerican: null,
    frequencyAmerican: null,
    frequencyIsEastAsian: null,
    frequencyEastAsian: null,
    frequencyIsEuropean: null,
    frequencyEuropean: null,
    frequencyIsSouthAsian: null,
    frequencySouthAsian: null,
    frequencyOther: null,
    studyIsInvitro: null,
    studyInvitro: null,
    studyIsInvivo: null,
    studyInvivo: null,
    studyIsClinical: null,
    studyClinical: null,
    studyOther: null,
    affectOnDrugAdme: null,
    affectOnDrugExposure: null,
    clinicalEfficacy: null,
    comments: null,
    isChanged: false,
    reason: null
  };
  const [selectedId, setSelectedId] = useState(0);
  const [refInfos, setRefInfos] = useState({});
  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const { updateRepeatByKey } = useRepeat(key);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  useEffect(
    () => {
      // reference title 받아오기
      const referenceIdArr = new Set();

      for (let i = 0; i < repeat.items.length; i++) {
        if (!!repeat.items[i].referenceId) {
          referenceIdArr.add(repeat.items[i].referenceId);
        }
      }

      if (referenceIdArr.size > 0) {
        tbdrugApi
          .fetchReferencesByIds({
            referenceIds: [...referenceIdArr].join(',')
          })
          .then(res => {
            setRefInfos(res.data);
          });
      }
    },
    [repeat.items]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      tbdrugId: subjectId,
      projectActivityId,
      [key]: model[key]
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(id => {
    setModel({ ...model, [key]: id });
    setSelectedId(id);
  }, []);

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        [key]: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        refInfos={refInfos}
        ethnicCodes={ethnicCodes}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text="PK DataPGx interactions" />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'TbdrugPgx',
                prefixLocale: 'activity.tbdrugPgx',
                skip: [key, 'tbdrugId']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <PgxInteractionsForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
            refInfos={refInfos}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </PgxInteractionsForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default PgxInteractionsContainer;
