import React, { useCallback } from 'react';
import { Form, Input, TextArea } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '../';
import { useSubmit } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import { SelectReason } from '../Selector';
import { FlexField, StyledCheckbox, StyledInput } from '../styles';

const PgxInteractionsForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  refInfos
}) => {
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return '';
    },
    [refInfos]
  );

  const handleCheckboxChange = (event, { name, checked }) => {
    onChange(event, { name, value: checked });
  };

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle(model.referenceId)}
          name="referenceId"
          value={model.referenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Gene</label>
        <Input name="gene" value={model.gene || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>Polymorphism</label>
        <Form.Group widths="equal">
          <Form.Field>
            <Input
              label="rsid"
              name="polymorphismRsid"
              value={model.polymorphismRsid || ''}
              onChange={onChange}
            />
          </Form.Field>
          <FlexField>
            <Input
              label="HGVS"
              name="polymorphismHgvs"
              value={model.polymorphismHgvs || ''}
              onChange={onChange}
              style={{ marginTop: 'auto' }}
            />
          </FlexField>
          <FlexField>
            <Input
              label="Other"
              name="polymorphismOther"
              value={model.polymorphismOther || ''}
              onChange={onChange}
              style={{ marginTop: 'auto' }}
            />
          </FlexField>
        </Form.Group>
      </Form.Field>
      <Form.Field>
        <label>Frequency</label>
        <FlexField>
          <StyledCheckbox
            name="frequencyIsAfrican"
            label="African"
            checked={!!model.frequencyIsAfrican}
            onChange={handleCheckboxChange}
          />
          <StyledInput
            name="frequencyAfrican"
            value={model.frequencyAfrican || ''}
            onChange={onChange}
            disabled={!!!model.frequencyIsAfrican}
          />
        </FlexField>
        <FlexField>
          <StyledCheckbox
            name="frequencyIsAmerican"
            label="American"
            checked={!!model.frequencyIsAmerican}
            onChange={handleCheckboxChange}
          />
          <StyledInput
            name="frequencyAmerican"
            value={model.frequencyAmerican || ''}
            onChange={onChange}
            disabled={!!!model.frequencyIsAmerican}
          />
        </FlexField>
        <FlexField>
          <StyledCheckbox
            name="frequencyIsEastAsian"
            label="East Asian"
            checked={!!model.frequencyIsEastAsian}
            onChange={handleCheckboxChange}
          />
          <StyledInput
            name="frequencyEastAsian"
            value={model.frequencyEastAsian || ''}
            onChange={onChange}
            disabled={!!!model.frequencyIsEastAsian}
          />
        </FlexField>
        <FlexField>
          <StyledCheckbox
            name="frequencyIsEuropean"
            label="European"
            checked={!!model.frequencyIsEuropean}
            onChange={handleCheckboxChange}
          />
          <StyledInput
            name="frequencyEuropean"
            value={model.frequencyEuropean || ''}
            onChange={onChange}
            disabled={!!!model.frequencyIsEuropean}
          />
        </FlexField>
        <FlexField>
          <StyledCheckbox
            name="frequencyIsSouthAsian"
            label="South Asian"
            checked={!!model.frequencyIsSouthAsian}
            onChange={handleCheckboxChange}
          />
          <StyledInput
            name="frequencySouthAsian"
            value={model.frequencySouthAsian || ''}
            onChange={onChange}
            disabled={!!!model.frequencyIsSouthAsian}
          />
        </FlexField>
        <Form.Field>
          <Input
            label="Other detail"
            name="frequencyOther"
            value={model.frequencyOther || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Field>
      <Form.Field>
        <label>Study</label>
        <FlexField>
          <StyledCheckbox
            name="studyIsInvitro"
            label="In vitro"
            checked={!!model.studyIsInvitro}
            onChange={handleCheckboxChange}
          />
          <StyledInput
            label="subject detail"
            name="studyInvitro"
            value={model.studyInvitro || ''}
            onChange={onChange}
            disabled={!!!model.studyIsInvitro}
          />
        </FlexField>
        <FlexField>
          <StyledCheckbox
            name="studyIsInvivo"
            label="In vivo"
            checked={!!model.studyIsInvivo}
            onChange={handleCheckboxChange}
          />
          <StyledInput
            label="subject detail"
            name="studyInvivo"
            value={model.studyInvivo || ''}
            onChange={onChange}
            disabled={!!!model.studyIsInvivo}
          />
        </FlexField>
        <FlexField>
          <StyledCheckbox
            name="studyIsClinical"
            label="Clinical"
            checked={!!model.studyIsClinical}
            onChange={handleCheckboxChange}
          />
          <StyledInput
            label="subject detail"
            name="studyClinical"
            value={model.studyClinical || ''}
            onChange={onChange}
            disabled={!!!model.studyIsClinical}
          />
        </FlexField>
        <Form.Field>
          <Input
            label="Other detail"
            name="studyOther"
            value={model.studyOther || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Field>
      <Form.Field>
        <label>Affect on drug</label>
        <Input
          label="ADME Details"
          name="affectOnDrugAdme"
          value={model.affectOnDrugAdme || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <Input
          label="Exposure Details"
          name="affectOnDrugExposure"
          value={model.affectOnDrugExposure || ''}
          onChange={onChange}
          // style={{ marginTop: 'auto' }}
        />
      </Form.Field>
      <Form.Field>
        <label>Comments</label>
        <TextArea
          name="comments"
          value={model.comments || ''}
          onChange={onChange}
        />
      </Form.Field>

      {model.tbdrugPgxId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="PK DataPGx interactions"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default PgxInteractionsForm;
