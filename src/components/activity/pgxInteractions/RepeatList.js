import React, { useCallback } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = ({
  subjectId,
  updateRepeat,
  projectActivityId,
  items,
  loading,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  refInfos,
  ethnicCodes,
  isSeleted
}) => {
  const t = useFormatMessage();
  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return 'unknown';
    },
    [refInfos]
  );

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  return (
    <Table compact="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Gene</Table.HeaderCell>
          <Table.HeaderCell>Polymorphism</Table.HeaderCell>
          <Table.HeaderCell>Frequency</Table.HeaderCell>
          <Table.HeaderCell>Study</Table.HeaderCell>
          <Table.HeaderCell>Affect on drug</Table.HeaderCell>
          <Table.HeaderCell>Comments</Table.HeaderCell>
          <Table.HeaderCell>Reference</Table.HeaderCell>
          <Table.HeaderCell width="two">{t('status')}</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map(item => {
          const {
            activityStatusCodeId,
            comments,
            affectOnDrugAdme,
            affectOnDrugExposure,
            frequencyIsAfrican,
            frequencyAfrican,
            frequencyIsAmerican,
            frequencyAmerican,
            frequencyIsEastAsian,
            frequencyEastAsian,
            frequencyIsEuropean,
            frequencyEuropean,
            frequencyIsSouthAsian,
            frequencySouthAsian,
            frequencyOther,
            gene,
            polymorphismRsid,
            polymorphismHgvs,
            polymorphismOther,
            referenceId,
            studyIsInvitro,
            studyInvitro,
            studyIsInvivo,
            studyInvivo,
            studyIsClinical,
            studyClinical,
            studyOther,
            tbdrugPgxId
          } = item;

          const polymorphism = [];
          const frequency = [];
          const study = [];
          const affectOnDrug = [];

          let result = '';

          // polymorphism
          if (!!polymorphismRsid) {
            polymorphism.push(`rsid: ${polymorphismRsid}`);
          }
          if (!!polymorphismHgvs) {
            polymorphism.push(`HGVS: ${polymorphismHgvs}`);
          }
          if (!!polymorphismOther) {
            polymorphism.push(`Other: ${polymorphismOther}`);
          }

          // frequency
          if (frequencyIsAfrican) {
            result = 'African';
            if (!!frequencyAfrican) {
              result += `: ${frequencyAfrican}`;
            }
            frequency.push(result);
          }
          if (frequencyIsAmerican) {
            result = 'American';
            if (!!frequencyAmerican) {
              result += `: ${frequencyAmerican}`;
            }
            frequency.push(result);
          }
          if (frequencyIsEastAsian) {
            result = 'East Asian';
            if (!!frequencyEastAsian) {
              result += `: ${frequencyEastAsian}`;
            }
            frequency.push(result);
          }
          if (frequencyIsEuropean) {
            result = 'European';
            if (!!frequencyEuropean) {
              result += `: ${frequencyEuropean}`;
            }
            frequency.push(result);
          }
          if (frequencyIsSouthAsian) {
            result = 'South Asian';
            if (!!frequencySouthAsian) {
              result += `: ${frequencySouthAsian}`;
            }
            frequency.push(result);
          }
          if (!!frequencyOther) {
            result = `Other detail: ${frequencyOther}`;
            frequency.push(result);
          }

          // study
          if (studyIsInvitro) {
            result = 'In vitro';
            if (!!studyInvitro) {
              result += `: ${studyInvitro}`;
            }
            study.push(result);
          }
          if (studyIsInvivo) {
            result = 'In vivo';
            if (!!studyInvivo) {
              result += `: ${studyInvivo}`;
            }
            study.push(result);
          }
          if (studyIsClinical) {
            result = 'Clinical';
            if (!!studyClinical) {
              result += `: ${studyClinical}`;
            }
            study.push(result);
          }
          if (!!studyOther) {
            result = `Other detail: ${studyOther}`;
            study.push(result);
          }

          if (!!affectOnDrugAdme) {
            affectOnDrug.push(`ADME Details: ${affectOnDrugAdme}`);
          }
          if (!!affectOnDrugExposure) {
            affectOnDrug.push(`Exposure Details: ${affectOnDrugExposure}`);
          }

          return (
            <StyledTableRow key={tbdrugPgxId} selected={isSeleted(item)}>
              <Table.Cell>
                <ActivityItemAnchor
                  id={tbdrugPgxId}
                  text={!!gene ? gene : 'unknown'}
                  onClick={onClick}
                />
              </Table.Cell>
              <Table.Cell title={polymorphism.join(', ')}>
                {polymorphism.join(', ')}
              </Table.Cell>
              <Table.Cell title={frequency.join(', ')}>
                {frequency.join(', ')}
              </Table.Cell>
              <Table.Cell title={study.join(', ')}>
                {study.join(', ')}
              </Table.Cell>
              <Table.Cell title={affectOnDrug.join(', ')}>
                {affectOnDrug.join(', ')}
              </Table.Cell>
              <Table.Cell>{comments}</Table.Cell>
              <Table.Cell>{getRefTitle(referenceId)}</Table.Cell>
              <Table.Cell>
                <StatusWrapper
                  subjectId={subjectId}
                  onChangeStatusCb={handleChangeStatus(item)}
                  activityKeyId={tbdrugPgxId}
                  projectActivityId={projectActivityId}
                  note={{
                    value: item.hasNote,
                    closed: onNoteModalClosed
                  }}
                  query={{
                    value: item.queryStatusCodeId,
                    closed: onQueryModalClosed
                  }}
                  file={{
                    value: item.hasFile,
                    closed: onFileModalClosed
                  }}
                  activity={{
                    value: activityStatusCodeId,
                    closed: () => {}
                  }}
                  audit={{
                    entityName: 'TbdrugPgx',
                    prefixLocale: 'activity.tbdrugPgx',
                    skip: ['tbdrugPgxId', 'tbdrugId']
                  }}
                  repeatItems={items}
                />
              </Table.Cell>
            </StyledTableRow>
          );
        })}
        {loading && (
          <EmptyRowsViewForSemantic
            loading={true}
            columnLength={8}
            size="large"
          />
        )}
      </Table.Body>
    </Table>
  );
};

export default RepeatList;
