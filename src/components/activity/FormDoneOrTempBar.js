import React from 'react';
import { Message, Checkbox } from 'semantic-ui-react';
import { code } from '../../constants';
import { useFormatMessage } from '../../hooks';

const FormDoneOrTempBar = ({ name, title, checked, onChange, statusCode }) => {
  const t = useFormatMessage();

  const handleChange = (event, data) => {
    onChange(event, { name: data.name, value: data.checked });
  };

  return (
    <Message className={checked ? 'positive' : 'info'}>
      <Message.Header>
        <Checkbox
          name={name}
          label={`[${title}] ${t('done')}`}
          checked={checked}
          onChange={handleChange}
          disabled={
            statusCode === code.FirstSign ||
            statusCode === code.SecondSign ||
            statusCode === code.ThirdSign
          }
        />
      </Message.Header>
    </Message>
  );
};

export default FormDoneOrTempBar;
