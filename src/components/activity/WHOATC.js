import React, { useCallback } from 'react';
import { Input } from 'semantic-ui-react';

const WHOATC = ({ name, value, onChange }) => {
  const handleClick = useCallback(() => {
    const features = 'width=1000, height=500, left=200, top=200';
    window.open('https://www.whocc.no/atc_ddd_index', 'whocc', features);
  }, []);

  return (
    <Input
      name={name}
      action={{
        color: 'teal',
        labelPosition: 'left',
        icon: 'search',
        content: 'WHO-ATC',
        type: 'button',
        onClick: handleClick
      }}
      actionPosition="left"
      value={value || ''}
      onChange={onChange}
    />
  );
};

export default WHOATC;
