import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { AdrSeverity, Adrcon, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import DateInput from '../../input/DateInput';
import { format } from '../../../constants';

const ADRForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  onFindMedDRAClick
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.adr.adrName')}</label>
        <Input name="adrName" value={model.adrName} onChange={onChange} />
        {validator.message(
          t('activity.adr.adrName'),
          model.adrName,
          'required'
        )}
      </Form.Field>

      <Form.Field>
        <label>{t('activity.adr.aedecod')}</label>
        <Input
          icon={{
            name: 'search',
            circular: true,
            link: true,
            onClick: onFindMedDRAClick
          }}
          name="aedecod"
          value={model.aedecod || ''}
          placeholder="Search..."
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.adr.aeptcd')}</label>
        <Input name="aeptcd" value={model.aeptcd || ''} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.adr.aesoc')}</label>
        <Input name="aesoc" value={model.aesoc || ''} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.adr.aesoccd')}</label>
        <Input name="aesoccd" value={model.aesoccd || ''} />
      </Form.Field>
      <Form.Field>
        <label>
          {t('activity.adr.adrOnsetDat')} {t('adrsetDat.info')}
        </label>
        <DateInput
          name="adrOnsetDat"
          value={model.adrOnsetDat}
          placeholder={format.YYYY_UK_UK}
          options={{ dateFormat: format.YYYY_UK_UK, separate: '-' }}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>
          {t('activity.adr.adrOffsetDat')} {t('adrsetDat.info')}
        </label>
        <DateInput
          name="adrOffsetDat"
          value={model.adrOffsetDat}
          placeholder={format.YYYY_UK_UK}
          options={{ dateFormat: format.YYYY_UK_UK, separate: '-' }}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.adr.adrSeverityCodeId')}</label>
        <AdrSeverity
          name="adrSeverityCodeId"
          value={model.adrSeverityCodeId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.adr.adrdrug')}</label>
        <Input name="adrdrug" value={model.adrdrug} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.adr.adrconCodeId')}</label>
        <Adrcon
          name="adrconCodeId"
          value={model.adrconCodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.subjectAdrid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.adr.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default ADRForm;
