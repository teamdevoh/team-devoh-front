import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [adrSeverityCodes] = useCodes({
      codeGroupId: codeGroup.AdrSeverityCode
    });
    const [adrconCodes] = useCodes({ codeGroupId: codeGroup.AdrconCode });
    const orderedItems = orderBy(items);

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>{t('activity.adr.adrName')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.adr.aedecod')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.adr.aesoc')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.adr.adrOnsetDat')}</Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.adr.adrOffsetDat')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.adr.adrSeverityCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('activity.adr.adrdrug')}</Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.adr.adrconCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const adrSeverity = build.displayName({
              source: adrSeverityCodes,
              value: item.adrSeverityCodeId
            });
            const adrcon = build.displayName({
              source: adrconCodes,
              value: item.adrconCodeId
            });

            return (
              <StyledTableRow
                key={item.subjectAdrid}
                selected={isSeleted(item)}
                style={
                  !!item.subjectLbchid
                    ? {
                        backgroundColor: '#FFFFB0'
                      }
                    : null
                }
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectAdrid}
                    text={item.adrName}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={item.aedecod}>{item.aedecod}</Table.Cell>
                <Table.Cell title={item.aesoc}>{item.aesoc}</Table.Cell>
                <Table.Cell title={item.adrOnsetDat}>
                  {item.adrOnsetDat}
                </Table.Cell>
                <Table.Cell title={item.adrOffsetDat}>
                  {item.adrOffsetDat}
                </Table.Cell>
                <Table.Cell title={adrSeverity}>{adrSeverity}</Table.Cell>
                <Table.Cell title={item.adrdrug}>{item.adrdrug}</Table.Cell>
                <Table.Cell title={adrcon}>{adrcon}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectAdrid}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectAdr',
                      prefixLocale: 'activity.adr',
                      skip: ['subjectAdrid', 'subjectLbchid']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={9}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
