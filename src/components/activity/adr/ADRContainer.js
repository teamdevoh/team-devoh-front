import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '..';
import { AuthorInfo } from '../../collection';
import { ADRForm, RepeatList } from '.';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { useADRs, useADR, useActionStatus } from '../hooks';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import { MedDRAModal } from '../../dictionary';
import { useBoolean } from '../../../hooks';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { code } from '../../../constants';

const ADRContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'subjectAdrid';

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const { add, update, remove, fetch, isSaving, isFetching } = useADR({
    activityKey
  });
  const [selectedId, setSelectedId] = useState(0);
  const [repeat, setRepeat] = useADRs({ subjectId, projectActivityId });
  const t = useFormatMessage();
  const medDRA = useBoolean(false);

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    subjectAdrid: '',
    subjectId: subjectId,
    subjectVisitId: '',
    adrName: '',
    aedecod: '',
    aeptcd: '',
    aesoc: '',
    aesoccd: '',
    adrOnsetDat: '',
    adrOffsetDat: '',
    adrSeverityCodeId: '',
    adrdrug: '',
    adrconCodeId: '',
    modified: '',
    isChanged: false,
    author: '',
    reason: ''
  };

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  const { updateRepeatByKey } = useRepeat(key);

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      subjectId,
      projectActivityId,
      subjectAdrid: model[key]
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    id => {
      setModel({ ...model, subjectAdrid: id, isChanged: false, reason: '' });
      setSelectedId(id);
    },
    [model, selectedId]
  );

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        subjectAdrid: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleFindMedDRAClick = () => {
    medDRA.setTrue();
  };

  const handleMedDRASelected = useCallback(item => {
    medDRA.setFalse();
    if (item.soc !== void 0 && item.pt !== void 0) {
      setModel({
        ...model,
        aedecod: item.pt.term,
        aeptcd: item.pt.code,
        aesoc: item.soc.term,
        aesoccd: item.soc.code,
        isChanged: true
      });
    }
  });

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  // useEffect(
  //   () => {
  //     onChange(null, { name: 'aedecod', value: model.aedecod });
  //   },
  //   [model.aedecod]
  // );
  // useEffect(
  //   () => {
  //     onChange(null, { name: 'aeptcd', value: model.aeptcd });
  //   },
  //   [model.aeptcd]
  // );
  // useEffect(
  //   () => {
  //     onChange(null, { name: 'aesoc', value: model.aesoc });
  //   },
  //   [model.aesoc]
  // );
  // useEffect(
  //   () => {
  //     onChange(null, { name: 'aesoccd', value: model.aesoccd });
  //   },
  //   [model.aesoccd]
  // );

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <MedDRAModal isOpen={medDRA.value} onOK={handleMedDRASelected} />
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text={t('activity.adr.title')} />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'SubjectAdr',
                prefixLocale: 'activity.adr',
                skip: ['subjectAdrid', 'subjectLbchid']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <ADRForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
            onFindMedDRAClick={handleFindMedDRAClick}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </ADRForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default ADRContainer;
