import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { YesorNo, SelectReason } from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';

const AdrSjstenForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrSjsten.sjstenAge40')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="sjstenAge40"
            value={model.sjstenAge40}
            onChange={onChange}
            width="one"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrSjsten.sjstenMal')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="sjstenMal"
            value={model.sjstenMal}
            onChange={onChange}
            width="one"
          />
        </Form.Group>
      </Form.Group>
      <Form.Field>
        <label>{t('activity.adrSjsten.sjstenBsa')}</label>
        <Input
          name="sjstenBsa"
          value={model.sjstenBsa || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrSjsten.sjstenTachy')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="sjstenTachy"
            value={model.sjstenTachy}
            onChange={onChange}
            width="one"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrSjsten.sjstenUrea')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="sjstenUrea"
            value={model.sjstenUrea}
            onChange={onChange}
            width="one"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrSjsten.sjstenGlu')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="sjstenGlu"
            value={model.sjstenGlu}
            onChange={onChange}
            width="one"
          />
        </Form.Group>
      </Form.Group>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.adrSjsten.sjstenBic')}</label>
        </Form.Field>
        <Form.Group>
          <YesorNo
            name="sjstenBic"
            value={model.sjstenBic}
            onChange={onChange}
            width="one"
          />
        </Form.Group>
      </Form.Group>
      {model.subjectAdrSjstenid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.adrSjsten.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default AdrSjstenForm;
