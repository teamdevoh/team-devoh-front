import AdrSjstenContainer from './AdrSjstenContainer';
import AdrSjstenForm from './AdrSjstenForm';

// const AdrSjstenContainer = asyncComponent(() => import('./AdrSjstenContainer'));
// const AdrSjstenForm = asyncComponent(() => import('./AdrSjstenForm'));

export { AdrSjstenContainer, AdrSjstenForm };
