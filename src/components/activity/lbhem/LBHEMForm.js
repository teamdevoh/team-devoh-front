import React from 'react';
import { Form } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '..';
import { activity } from '../../../constants';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DateInput, WarningMinMaxInput } from '../../input';
import { SelectReason } from '../Selector';

const LowSpan = ({ text }) => {
  return (
    <span style={{ color: activity.LabRanges.LOW.color.hex }}>{text}</span>
  );
};
const HighSpan = ({ text }) => {
  return (
    <span style={{ color: activity.LabRanges.HIGH.color.hex }}>{text}</span>
  );
};

const LBHEMForm = ({
  children,
  projectId,
  labRange,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  const lbhemItems = activity.LabLBHEMItems;

  const wbc = labRange.ranges.find(x => x.item === lbhemItems.WBC);
  const rbc = labRange.ranges.find(x => x.item === lbhemItems.RBC);
  const hgb = labRange.ranges.find(x => x.item === lbhemItems.HGB);
  const hct = labRange.ranges.find(x => x.item === lbhemItems.HCT);
  const plt = labRange.ranges.find(x => x.item === lbhemItems.PLT);
  const neu = labRange.ranges.find(x => x.item === lbhemItems.NEU);
  const lym = labRange.ranges.find(x => x.item === lbhemItems.LYM);
  const mon = labRange.ranges.find(x => x.item === lbhemItems.MON);
  const eos = labRange.ranges.find(x => x.item === lbhemItems.EOS);
  const bas = labRange.ranges.find(x => x.item === lbhemItems.BAS);
  const anc = labRange.ranges.find(x => x.item === lbhemItems.ANC);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>
          {t('activity.lbhem.lbdtc')}
          {t('activity.lbhem.lbdtc.des')}
        </label>
        <DateInput
          name="lbdtc"
          value={model.lbdtc}
          placeholder="ukuk-uk-uk"
          options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
          onChange={onChange}
        />
        {validator.message(t('activity.lbhem.lbdtc'), model.lbdtc, 'required')}
      </Form.Field>
      <Form.Group widths="equal">
        <Form.Field>
          <label>
            WBC (<LowSpan text={wbc.low} /> ~ <HighSpan text={wbc.high} />)
          </label>
          <WarningMinMaxInput
            name="wbc"
            value={model.wbc}
            min={wbc.low}
            max={wbc.high}
            unit={wbc.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            RBC (<LowSpan text={rbc.low} /> ~ <HighSpan text={rbc.high} />)
          </label>
          <WarningMinMaxInput
            name="rbc"
            value={model.rbc}
            min={rbc.low}
            max={rbc.high}
            unit={rbc.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.lbhem.hgb')} (<LowSpan text={hgb.low} /> ~{' '}
            <HighSpan text={hgb.high} />)
          </label>
          <WarningMinMaxInput
            name="hgb"
            value={model.hgb}
            min={hgb.low}
            max={hgb.high}
            unit={hgb.unit}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>
            {t('activity.lbhem.hct')} (<LowSpan text={hct.low} /> ~{' '}
            <HighSpan text={hct.high} />)
          </label>
          <WarningMinMaxInput
            name="hct"
            value={model.hct}
            min={hct.low}
            max={hct.high}
            unit={hct.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.lbhem.plt')} (<LowSpan text={plt.low} /> ~{' '}
            <HighSpan text={plt.high} />)
          </label>
          <WarningMinMaxInput
            name="plt"
            value={model.plt}
            min={plt.low}
            max={plt.high}
            unit={plt.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.lbhem.neu')} (<LowSpan text={neu.low} /> ~{' '}
            <HighSpan text={neu.high} />)
          </label>
          <WarningMinMaxInput
            name="neu"
            value={model.neu}
            min={neu.low}
            max={neu.high}
            unit={neu.unit}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>
            {t('activity.lbhem.lym')} (<LowSpan text={lym.low} /> ~{' '}
            <HighSpan text={lym.high} />)
          </label>
          <WarningMinMaxInput
            name="lym"
            value={model.lym}
            min={lym.low}
            max={lym.high}
            unit={lym.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.lbhem.mon')} (<LowSpan text={mon.low} /> ~{' '}
            <HighSpan text={mon.high} />)
          </label>
          <WarningMinMaxInput
            name="mon"
            value={model.mon}
            min={mon.low}
            max={mon.high}
            unit={mon.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.lbhem.eos')} (<LowSpan text={eos.low} /> ~{' '}
            <HighSpan text={eos.high} />)
          </label>
          <WarningMinMaxInput
            name="eos"
            value={model.eos}
            min={eos.low}
            max={eos.high}
            unit={eos.unit}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>
            {t('activity.lbhem.bas')} (<LowSpan text={bas.low} /> ~{' '}
            <HighSpan text={bas.high} />)
          </label>
          <WarningMinMaxInput
            name="bas"
            value={model.bas}
            min={bas.low}
            max={bas.high}
            unit={bas.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.lbhem.anc')} (<LowSpan text={anc.low} /> ~{' '}
            <HighSpan text={anc.high} />)
          </label>
          <WarningMinMaxInput
            name="anc"
            value={model.anc}
            min={anc.low}
            max={anc.high}
            unit={anc.unit}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {model.subjectLbhemid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.lbhem.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default LBHEMForm;
