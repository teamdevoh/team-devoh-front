import React, { memo, Fragment } from 'react';
import { Table, Label } from 'semantic-ui-react';
import { useFormatMessage, useVerifyRange, useTypeCheck } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import find from 'lodash/find';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { activity } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const CustomCell = ({ min, max, value, unit }) => {
  const { isNumeric } = useTypeCheck();
  const [level] = useVerifyRange({ min, max, value });

  const renderValueItem = () => {
    if (isNumeric(value)) {
      if (level && level !== activity.LabRanges.NORMAL.name) {
        return (
          <Label color={activity.LabRanges[level].color.name}>
            {value} {unit}
          </Label>
        );
      } else {
        return (
          <Fragment>
            {value} {unit}
          </Fragment>
        );
      }
    } else {
      return <Fragment>{value}</Fragment>;
    }
  };

  return <Table.Cell title={value}>{renderValueItem()}</Table.Cell>;
};

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    labRange,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['lbdtc'], ['asc']);

    let wbc = {};
    let rbc = {};
    let hgb = {};
    let hct = {};
    let plt = {};
    let neu = {};
    let lym = {};
    let mon = {};
    let eos = {};
    let bas = {};
    let anc = {};

    if (labRange.ranges) {
      const lbhemItems = activity.LabLBHEMItems;

      wbc = find(labRange.ranges, { item: lbhemItems.WBC });
      rbc = find(labRange.ranges, { item: lbhemItems.RBC });
      hgb = find(labRange.ranges, { item: lbhemItems.HGB });
      hct = find(labRange.ranges, { item: lbhemItems.HCT });
      plt = find(labRange.ranges, { item: lbhemItems.PLT });
      neu = find(labRange.ranges, { item: lbhemItems.NEU });
      lym = find(labRange.ranges, { item: lbhemItems.LYM });
      mon = find(labRange.ranges, { item: lbhemItems.MON });
      eos = find(labRange.ranges, { item: lbhemItems.EOS });
      bas = find(labRange.ranges, { item: lbhemItems.BAS });
      anc = find(labRange.ranges, { item: lbhemItems.ANC });
    }

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="two">
              {t('activity.lbhem.lbdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>WBC</Table.HeaderCell>
            <Table.HeaderCell>RBC</Table.HeaderCell>
            <Table.HeaderCell>HGB</Table.HeaderCell>
            <Table.HeaderCell>HCT</Table.HeaderCell>
            <Table.HeaderCell>PLT</Table.HeaderCell>
            <Table.HeaderCell>NEU</Table.HeaderCell>
            <Table.HeaderCell>LYM</Table.HeaderCell>
            <Table.HeaderCell>MON</Table.HeaderCell>
            <Table.HeaderCell>EOS</Table.HeaderCell>
            <Table.HeaderCell>BAS</Table.HeaderCell>
            <Table.HeaderCell>ANC</Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            return (
              <StyledTableRow
                key={item.subjectLbhemid}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectLbhemid}
                    text={item.lbdtc}
                    projectActivityId={
                      item.projectActivityId || projectActivityId
                    }
                    onClick={onClick}
                  />
                </Table.Cell>
                <CustomCell
                  min={wbc.low}
                  max={wbc.high}
                  value={item.wbc}
                  unit={wbc.unit}
                />
                <CustomCell
                  min={rbc.low}
                  max={rbc.high}
                  value={item.rbc}
                  unit={rbc.unit}
                />
                <CustomCell
                  min={hgb.low}
                  max={hgb.high}
                  value={item.hgb}
                  unit={hgb.unit}
                />
                <CustomCell
                  min={hct.low}
                  max={hct.high}
                  value={item.hct}
                  unit={hct.unit}
                />
                <CustomCell
                  min={plt.low}
                  max={plt.high}
                  value={item.plt}
                  unit={plt.unit}
                />
                <CustomCell
                  min={neu.low}
                  max={neu.high}
                  value={item.neu}
                  unit={neu.unit}
                />
                <CustomCell
                  min={lym.low}
                  max={lym.high}
                  value={item.lym}
                  unit={lym.unit}
                />
                <CustomCell
                  min={mon.low}
                  max={mon.high}
                  value={item.mon}
                  unit={mon.unit}
                />
                <CustomCell
                  min={eos.low}
                  max={eos.high}
                  value={item.eos}
                  unit={eos.unit}
                />
                <CustomCell
                  min={bas.low}
                  max={bas.high}
                  value={item.bas}
                  unit={bas.unit}
                />
                <CustomCell
                  min={anc.low}
                  max={anc.high}
                  value={item.anc}
                  unit={anc.unit}
                />
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectLbhemid}
                    projectActivityId={
                      item.projectActivityId || projectActivityId
                    }
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectLbhem',
                      prefixLocale: 'activity.lbhem',
                      skip: ['subjectLbhemid']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={13}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
