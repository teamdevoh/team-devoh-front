import React from 'react';
import { Table, Responsive } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import StatusWrapper from '../StatusWrapper';
import { code } from '../../../constants';
import { EmptyRowsStickyViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

export const pkCommonDisplayName = ({
  mean,
  sd,
  median,
  min,
  max,
  unit,
  other
}) => {
  const resultArr = [];
  let result = '';

  if (!!mean) {
    result = `Mean: ${mean}`;
    resultArr.push(result);
  }
  if (!!sd) {
    result = `SD: ${sd}`;
    resultArr.push(result);
  }
  if (!!median) {
    result = `Median: ${median}`;
    resultArr.push(result);
  }
  if (!!min || max) {
    result = `Min ~ Max: ${min || ''} ~ ${max || ''}`;
    resultArr.push(result);
  }
  if (resultArr.length > 0) {
    resultArr[resultArr.length - 1] += unit ? ` ${unit}` : '';
  }
  if (!!other) {
    result = `Other: ${other}`;
    resultArr.push(result);
  }

  return resultArr;
};

const RepeatList = ({
  subjectId,
  updateRepeat,
  skip,
  projectActivityId,
  items,
  loading,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  refInfos,
  MAX_FIELD_CNT,
  doseRouteCodes,
  dosfrqCodes,
  isSeleted
}) => {
  const t = useFormatMessage();
  const build = useBuild();
  const [drugs] = useTbdrugList();

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  return (
    <div style={{ overflow: 'auto', margin: '1em 0' }}>
      <div
        style={
          isMobile
            ? {}
            : {
                flexShrink: 0,
                width: '300%'
              }
        }
      >
        <Table compact="very" fixed={!isMobile} singleLine={!isMobile}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Reference</Table.HeaderCell>
              <Table.HeaderCell>Ethnicity</Table.HeaderCell>
              <Table.HeaderCell>Gender(Number of subject)</Table.HeaderCell>
              <Table.HeaderCell>Age</Table.HeaderCell>
              <Table.HeaderCell>Weight</Table.HeaderCell>
              <Table.HeaderCell>Status(disease/healthy)</Table.HeaderCell>
              <Table.HeaderCell>Drug Infomation</Table.HeaderCell>
              <Table.HeaderCell>Dose</Table.HeaderCell>
              <Table.HeaderCell>Fed Status</Table.HeaderCell>
              <Table.HeaderCell>Concomitent drugs</Table.HeaderCell>
              <Table.HeaderCell>AUC0-24</Table.HeaderCell>
              <Table.HeaderCell>AUC0–∞</Table.HeaderCell>
              <Table.HeaderCell>Cmax</Table.HeaderCell>
              <Table.HeaderCell>Tmax</Table.HeaderCell>
              <Table.HeaderCell>T1/2</Table.HeaderCell>
              <Table.HeaderCell>CL/F</Table.HeaderCell>
              <Table.HeaderCell>Ka</Table.HeaderCell>
              <Table.HeaderCell>Vd</Table.HeaderCell>
              <Table.HeaderCell>Vdss</Table.HeaderCell>
              <Table.HeaderCell>Ae0-24h</Table.HeaderCell>
              <Table.HeaderCell>Ae0-48h</Table.HeaderCell>
              <Table.HeaderCell>CLR</Table.HeaderCell>
              <Table.HeaderCell>Tlag</Table.HeaderCell>
              <Table.HeaderCell>Other PK parameter</Table.HeaderCell>
              <Table.HeaderCell>Concomitent</Table.HeaderCell>
              <Table.HeaderCell>{t('status')}</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {items.map(item => {
              const {
                tbdrugPkdataId,
                referenceId,
                ethinicIsAfrican,
                ethinicAfricanCountry,
                ethinicIsAmerican,
                ethinicAmericanCountry,
                ethinicIsEastAsian,
                ethinicEastAsianCountry,
                ethinicIsEuropean,
                ethinicEuropeanCountry,
                ethinicIsSouthAsian,
                ethinicSouthAsianCountry,
                ethinicOther,
                genderMale,
                genderFemale,
                genderTotal,
                genderUnit,
                genderOther,
                ageMean,
                ageSd,
                ageMedian,
                ageMin,
                ageMax,
                ageOther,
                ageUnit,
                statusIsTb,
                statusTbDisease,
                statusIsHealthy,
                statusHealthyDisease,
                statusOther,
                drugInforBrandName,
                drugInforDosageForm,
                doseRouteCodeId,
                doseRouteOther,
                dose,
                doseUnit,
                doseOther,
                fedStatus,
                fedStatusOther,
                auc024Mean,
                auc024Sd,
                auc024Median,
                auc024Min,
                auc024Max,
                auc024Unit,
                auc024Other,
                auc0InfiniteMean,
                auc0InfiniteSd,
                auc0InfiniteMedian,
                auc0InfiniteMin,
                auc0InfiniteMax,
                auc0InfiniteUnit,
                auc0InfiniteOther,
                cmaxMean,
                cmaxSd,
                cmaxMedian,
                cmaxMin,
                cmaxMax,
                cmaxUnit,
                cmaxOther,
                tmaxMean,
                tmaxSd,
                tmaxMedian,
                tmaxMin,
                tmaxMax,
                tmaxUnit,
                tmaxOther,
                t12Mean,
                t12Sd,
                t12Median,
                t12Min,
                t12Max,
                t12Unit,
                t12Other,
                clfMean,
                clfSd,
                clfMedian,
                clfMin,
                clfMax,
                clfUnit,
                clfOther,
                kaMean,
                kaSd,
                kaMedian,
                kaMin,
                kaMax,
                kaUnit,
                kaOther,
                vdMean,
                vdSd,
                vdMedian,
                vdMin,
                vdMax,
                vdUnit,
                vdOther,
                comments,
                activityStatusCodeId,
                weightMean,
                weightSd,
                weightMedian,
                weightMin,
                weightMax,
                weightUnit,
                weightOther,
                regimenCodeId,
                regimenOther,
                vdssMean,
                vdssSd,
                vdssMedian,
                vdssMin,
                vdssMax,
                vdssUnit,
                vdssOther,
                ae024hMean,
                ae024hSd,
                ae024hMedian,
                ae024hMin,
                ae024hMax,
                ae024hUnit,
                ae024hOther,
                ae048hMean,
                ae048hSd,
                ae048hMedian,
                ae048hMin,
                ae048hMax,
                ae048hUnit,
                ae048hOther,
                clRMean,
                clRSd,
                clRMedian,
                clRMin,
                clRMax,
                clRUnit,
                clROther,
                tlagMean,
                tlagSd,
                tlagMedian,
                tlagMin,
                tlagMax,
                tlagUnit,
                tlagOther,
                otherName,
                otherMean,
                otherSd,
                otherMedian,
                otherMin,
                otherMax,
                otherUnit,
                otherOther
              } = item;

              const referenceTitle = !!refInfos[referenceId]
                ? refInfos[referenceId].title
                : 'unknown';
              const ethinic = [];
              const gender = [];
              let age = [];
              let weight = [];
              const statusIs = [];
              const drugInfo = [];
              const doseArr = [];
              const fed = [];
              const concomitentDrug = [];
              let auc024 = [];
              let auc0Infinite = [];
              let cmax = [];
              let tmax = [];
              let t12 = [];
              let clf = [];
              let ka = [];
              let vd = [];
              let vdss = [];
              let ae024h = [];
              let ae048h = [];
              let clR = [];
              let tlag = [];
              let other = [];

              let result = '';

              if (ethinicIsAfrican) {
                result = 'African';
                if (!!ethinicAfricanCountry) {
                  result += `: ${ethinicAfricanCountry}`;
                }
                ethinic.push(result);
              }
              if (ethinicIsAmerican) {
                result = 'American';
                if (!!ethinicAmericanCountry) {
                  result += `: ${ethinicAmericanCountry}`;
                }
                ethinic.push(result);
              }
              if (ethinicIsEastAsian) {
                result = 'East Asian';
                if (!!ethinicEastAsianCountry) {
                  result += `: ${ethinicEastAsianCountry}`;
                }
                ethinic.push(result);
              }
              if (ethinicIsEuropean) {
                result = 'European';
                if (!!ethinicEuropeanCountry) {
                  result += `: ${ethinicEuropeanCountry}`;
                }
                ethinic.push(result);
              }
              if (ethinicIsSouthAsian) {
                result = 'South Asian';
                if (!!ethinicSouthAsianCountry) {
                  result += `: ${ethinicSouthAsianCountry}`;
                }
                ethinic.push(result);
              }
              if (!!ethinicOther) {
                result = `Other detail: ${ethinicOther}`;
                ethinic.push(result);
              }

              // gender
              if (!!genderTotal) {
                result = `N = ${genderTotal}`;

                if (!!genderMale && !!genderFemale) {
                  result += ` (M: ${genderMale}/F: ${genderFemale})`;
                } else {
                  if (!!genderMale) {
                    result += ` (M)`;
                  } else {
                    result += ` (F)`;
                  }
                }
                result += ` ${genderUnit || 'Count'}`;
                gender.push(result);
              }
              if (!!genderOther) {
                result = `Gender Other: ${genderOther}`;
                gender.push(result);
              }

              // age
              age = pkCommonDisplayName({
                mean: ageMean,
                sd: ageSd,
                median: ageMedian,
                min: ageMin,
                max: ageMax,
                unit: ageUnit || 'Year',
                other: ageOther
              });

              //weight
              weight = pkCommonDisplayName({
                mean: weightMean,
                sd: weightSd,
                median: weightMedian,
                min: weightMin,
                max: weightMax,
                unit: weightUnit,
                other: weightOther
              });

              // statusIs
              if (statusIsTb) {
                result = 'TB';
                if (!!statusTbDisease) {
                  result += `: ${statusTbDisease}`;
                }
                statusIs.push(result);
              }
              if (statusIsHealthy) {
                result = 'Healthy';
                if (!!statusHealthyDisease) {
                  result += `: ${statusHealthyDisease}`;
                }
                statusIs.push(result);
              }
              if (!!statusOther) {
                result = `Other: ${statusOther}`;
                statusIs.push(result);
              }

              // drug info
              if (!!drugInforBrandName) {
                drugInfo.push(drugInforBrandName);
              }
              if (!!drugInforDosageForm) {
                drugInfo.push(drugInforDosageForm);
              }

              // dose
              if (!!doseRouteCodeId) {
                result = build.displayName({
                  source: doseRouteCodes,
                  value: doseRouteCodeId,
                  other: doseRouteOther,
                  otherValue: code.DoseRouteOther
                });

                doseArr.push(result);
              }
              if (!!dose) {
                if (doseArr.length > 0) {
                  doseArr[doseArr.length - 1] += `: ${dose} ${doseUnit ||
                    'mg'}`;
                } else {
                  result = `${dose} ${doseUnit || 'mg'}`;
                  doseArr.push(result);
                }
              }
              if (!!regimenCodeId) {
                result = build.displayName({
                  source: dosfrqCodes,
                  value: regimenCodeId,
                  other: regimenOther,
                  otherValue: code.DosfrqOther
                });

                doseArr.push(`Regimen: ${result}`);
              }
              if (!!doseOther) {
                result = `Other: ${doseOther}`;
                doseArr.push(result);
              }

              // fed
              if (!!fedStatus) {
                result = fedStatus;
                fed.push(result);
              }
              if (!!fedStatusOther) {
                if (fed.length > 0) {
                  fed[fed.length - 1] += `: ${fedStatusOther}`;
                } else {
                  result = fedStatusOther;
                  fed.push(result);
                }
              }

              // tbdrug 약이름 : other 있으면 해주고 500 mg(etc 내용)
              // concomitentDrug
              for (let i = 0; i < MAX_FIELD_CNT; i++) {
                const curKey = `concomitentDrug${i + 1}`;
                result = '';

                if (!!item[`${curKey}TbdrugId`]) {
                  result = build.displayName({
                    source: drugs.items,
                    value: Number(item[`${curKey}TbdrugId`])
                  });
                }
                if (!!item[`${curKey}Other`]) {
                  result += result.length > 0 ? ': ' : '';
                  result += item[`${curKey}Other`];
                }
                if (!!item[`${curKey}Dose`]) {
                  result += ` ${item[`${curKey}Dose`]}`;
                  result += ` ${item[`${curKey}Unit`] || 'mg'}`;
                }
                if (!!item[`${curKey}Etc`]) {
                  result += `(${item[`${curKey}Etc`]})`;
                }
                if (result.length > 0) {
                  concomitentDrug.push(result);
                }
              }

              // auc024
              auc024 = pkCommonDisplayName({
                mean: auc024Mean,
                sd: auc024Sd,
                median: auc024Median,
                min: auc024Min,
                max: auc024Max,
                unit: auc024Unit,
                other: auc024Other
              });

              // auc0Infinite
              auc0Infinite = pkCommonDisplayName({
                mean: auc0InfiniteMean,
                sd: auc0InfiniteSd,
                median: auc0InfiniteMedian,
                min: auc0InfiniteMin,
                max: auc0InfiniteMax,
                unit: auc0InfiniteUnit,
                other: auc0InfiniteOther
              });

              // cmax
              cmax = pkCommonDisplayName({
                mean: cmaxMean,
                sd: cmaxSd,
                median: cmaxMedian,
                min: cmaxMin,
                max: cmaxMax,
                unit: cmaxUnit,
                other: cmaxOther
              });

              // tmax
              tmax = pkCommonDisplayName({
                mean: tmaxMean,
                sd: tmaxSd,
                median: tmaxMedian,
                min: tmaxMin,
                max: tmaxMax,
                unit: tmaxUnit,
                other: tmaxOther
              });

              // t12
              t12 = pkCommonDisplayName({
                mean: t12Mean,
                sd: t12Sd,
                median: t12Median,
                min: t12Min,
                max: t12Max,
                unit: t12Unit,
                other: t12Other
              });

              // clf
              clf = pkCommonDisplayName({
                mean: clfMean,
                sd: clfSd,
                median: clfMedian,
                min: clfMin,
                max: clfMax,
                unit: clfUnit,
                other: clfOther
              });

              // ka
              ka = pkCommonDisplayName({
                mean: kaMean,
                sd: kaSd,
                median: kaMedian,
                min: kaMin,
                max: kaMax,
                unit: kaUnit,
                other: kaOther
              });

              // vd
              vd = pkCommonDisplayName({
                mean: vdMean,
                sd: vdSd,
                median: vdMedian,
                min: vdMin,
                max: vdMax,
                unit: vdUnit,
                other: vdOther
              });

              // vdss
              vdss = pkCommonDisplayName({
                mean: vdssMean,
                sd: vdssSd,
                median: vdssMedian,
                min: vdssMin,
                max: vdssMax,
                unit: vdssUnit,
                other: vdssOther
              });

              // ae024h
              ae024h = pkCommonDisplayName({
                mean: ae024hMean,
                sd: ae024hSd,
                median: ae024hMedian,
                min: ae024hMin,
                max: ae024hMax,
                unit: ae024hUnit,
                other: ae024hOther
              });

              // ae048h
              ae048h = pkCommonDisplayName({
                mean: ae048hMean,
                sd: ae048hSd,
                median: ae048hMedian,
                min: ae048hMin,
                max: ae048hMax,
                unit: ae048hUnit,
                other: ae048hOther
              });

              // clR
              clR = pkCommonDisplayName({
                mean: clRMean,
                sd: clRSd,
                median: clRMedian,
                min: clRMin,
                max: clRMax,
                unit: clRUnit,
                other: clROther
              });

              // tlag
              tlag = pkCommonDisplayName({
                mean: tlagMean,
                sd: tlagSd,
                median: tlagMedian,
                min: tlagMin,
                max: tlagMax,
                unit: tlagUnit,
                other: tlagOther
              });

              // other
              other = pkCommonDisplayName({
                mean: otherMean,
                sd: otherSd,
                median: otherMedian,
                min: otherMin,
                max: otherMax,
                unit: otherUnit,
                other: otherOther
              });
              if (!!otherName) {
                other.unshift(`Name: ${otherName}`);
              }

              return (
                <StyledTableRow key={tbdrugPkdataId} selected={isSeleted(item)}>
                  <Table.Cell title={referenceTitle}>
                    <ActivityItemAnchor
                      id={tbdrugPkdataId}
                      text={referenceTitle}
                      onClick={onClick}
                    />
                  </Table.Cell>
                  <Table.Cell title={ethinic.join(', ')}>
                    {ethinic.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={gender.join(', ')}>
                    {gender.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={age.join(', ')}>
                    {age.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={weight.join(', ')}>
                    {weight.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={statusIs.join(', ')}>
                    {statusIs.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={drugInfo.join(', ')}>
                    {drugInfo.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={doseArr.join(', ')}>
                    {doseArr.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={fed.join(', ')}>
                    {fed.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={concomitentDrug.join(', ')}>
                    {concomitentDrug.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={auc024.join(', ')}>
                    {auc024.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={auc0Infinite.join(', ')}>
                    {auc0Infinite.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={cmax.join(', ')}>
                    {cmax.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={tmax.join(', ')}>
                    {tmax.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={t12.join(', ')}>
                    {t12.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={clf.join(', ')}>
                    {clf.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={ka.join(', ')}>{ka.join(', ')}</Table.Cell>
                  <Table.Cell title={vd.join(', ')}>{vd.join(', ')}</Table.Cell>
                  <Table.Cell title={vdss.join(', ')}>
                    {vdss.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={ae024h.join(', ')}>
                    {ae024h.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={ae048h.join(', ')}>
                    {ae048h.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={clR.join(', ')}>
                    {clR.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={tlag.join(', ')}>
                    {tlag.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={other.join(', ')}>
                    {other.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={comments}>{comments}</Table.Cell>
                  <Table.Cell>
                    <StatusWrapper
                      subjectId={subjectId}
                      onChangeStatusCb={handleChangeStatus(item)}
                      activityKeyId={tbdrugPkdataId}
                      projectActivityId={projectActivityId}
                      note={{
                        value: item.hasNote,
                        closed: onNoteModalClosed
                      }}
                      query={{
                        value: item.queryStatusCodeId,
                        closed: onQueryModalClosed
                      }}
                      file={{
                        value: item.hasFile,
                        closed: onFileModalClosed
                      }}
                      activity={{
                        value: activityStatusCodeId,
                        closed: () => {}
                      }}
                      audit={{
                        entityName: 'TbdrugPkdata',
                        prefixLocale: 'activity.tbdrugPkdata',
                        skip
                      }}
                      repeatItems={items}
                    />
                  </Table.Cell>
                </StyledTableRow>
              );
            })}
            {loading && (
              <EmptyRowsStickyViewForSemantic
                loading={true}
                columnLength={18}
                size="large"
              />
            )}
          </Table.Body>
        </Table>
      </div>
    </div>
  );
};

export default RepeatList;
