import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '../';
import { PkDataForm, RepeatList } from './';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields, useCodes } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { usePkDataList, useActionStatus, usePkData } from '../hooks';
import { AuthorInfo } from '../../collection';
import tbdrugApi from '../../collection-tbdrug/api';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { code, codeGroup } from '../../../constants';

const MAX_FIELD_CNT = 6;

const isOpen = (...args) => {
  for (let i = 0; i < args.length; i++) {
    let curValue = args[i];

    if (typeof curValue === 'string') {
      curValue = curValue.trim();
    }

    if (!!curValue) {
      return true;
    }
  }
  return false;
};

const PkDataContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'tbdrugPkdataId';
  const skip = [key, 'tbdrugId', 'doseMeaning'];
  const { add, update, remove, fetch, isSaving, isFetching } = usePkData({
    activityKey
  });

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const [repeat, setRepeat] = usePkDataList({
    tbdrugId: subjectId,
    projectActivityId
  });
  const t = useFormatMessage();

  const [doseRouteCodes] = useCodes({
    codeGroupId: codeGroup.DoseRouteCode
  });
  const [dosfrqCodes] = useCodes({
    codeGroupId: codeGroup.DosfrqCode
  });

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    hasNote: false,
    isDone: false,
    modified: null,
    author: '',
    [key]: '',
    activityId: activityKey,
    tbdrugId: subjectId,
    referenceId: null,
    ethinicIsAfrican: null,
    ethinicAfricanCountry: null,
    ethinicIsAmerican: null,
    ethinicAmericanCountry: null,
    ethinicIsEastAsian: null,
    ethinicEastAsianCountry: null,
    ethinicIsEuropean: null,
    ethinicEuropeanCountry: null,
    ethinicIsSouthAsian: null,
    ethinicSouthAsianCountry: null,
    ethinicOther: null,
    genderMale: null,
    genderFemale: null,
    genderTotal: null,
    genderUnit: 'Count',
    genderOther: null,
    ageMean: null,
    ageSd: null,
    ageMedian: null,
    ageMin: null,
    ageMax: null,
    ageOther: null,
    ageUnit: 'Year',
    statusIsTb: null,
    statusTbDisease: null,
    statusIsHealthy: null,
    statusHealthyDisease: null,
    statusOther: null,
    drugInforBrandName: null,
    drugInforDosageForm: null,
    doseRouteCodeId: null,
    doseRouteOther: null,
    dose: null,
    doseUnit: 'mg',
    doseOther: null,
    fedStatus: null,
    fedStatusOther: null,
    concomitentDrug1TbdrugId: null,
    concomitentDrug1Other: null,
    concomitentDrug1Dose: null,
    concomitentDrug1Unit: 'mg',
    concomitentDrug1Etc: null,
    concomitentDrug2TbdrugId: null,
    concomitentDrug2Other: null,
    concomitentDrug2Dose: null,
    concomitentDrug2Unit: 'mg',
    concomitentDrug2Etc: null,
    concomitentDrug3TbdrugId: null,
    concomitentDrug3Other: null,
    concomitentDrug3Dose: null,
    concomitentDrug3Unit: 'mg',
    concomitentDrug3Etc: null,
    concomitentDrug4TbdrugId: null,
    concomitentDrug4Other: null,
    concomitentDrug4Dose: null,
    concomitentDrug4Unit: 'mg',
    concomitentDrug4Etc: null,
    concomitentDrug5TbdrugId: null,
    concomitentDrug5Other: null,
    concomitentDrug5Dose: null,
    concomitentDrug5Unit: 'mg',
    concomitentDrug5Etc: null,
    concomitentDrug6TbdrugId: null,
    concomitentDrug6Other: null,
    concomitentDrug6Dose: null,
    concomitentDrug6Unit: 'mg',
    concomitentDrug6Etc: null,
    auc024Mean: null,
    auc024Sd: null,
    auc024Median: null,
    auc024Min: null,
    auc024Max: null,
    auc024Unit: null,
    auc024Other: null,
    auc0InfiniteMean: null,
    auc0InfiniteSd: null,
    auc0InfiniteMedian: null,
    auc0InfiniteMin: null,
    auc0InfiniteMax: null,
    auc0InfiniteUnit: null,
    auc0InfiniteOther: null,
    cmaxMean: null,
    cmaxSd: null,
    cmaxMedian: null,
    cmaxMin: null,
    cmaxMax: null,
    cmaxUnit: null,
    cmaxOther: null,
    tmaxMean: null,
    tmaxSd: null,
    tmaxMedian: null,
    tmaxMin: null,
    tmaxMax: null,
    tmaxUnit: null,
    tmaxOther: null,
    t12Mean: null,
    t12Sd: null,
    t12Median: null,
    t12Min: null,
    t12Max: null,
    t12Unit: null,
    t12Other: null,
    clfMean: null,
    clfSd: null,
    clfMedian: null,
    clfMin: null,
    clfMax: null,
    clfUnit: null,
    clfOther: null,
    kaMean: null,
    kaSd: null,
    kaMedian: null,
    kaMin: null,
    kaMax: null,
    kaUnit: null,
    kaOther: null,
    vdMean: null,
    vdSd: null,
    vdMedian: null,
    vdMin: null,
    vdMax: null,
    vdUnit: null,
    vdOther: null,
    comments: null,
    isChanged: false,
    reason: null,
    ae024hMax: null,
    ae024hMean: null,
    ae024hMedian: null,
    ae024hMin: null,
    ae024hOther: null,
    ae024hSd: null,
    ae024hUnit: null,
    ae048hMax: null,
    ae048hMean: null,
    ae048hMedian: null,
    ae048hMin: null,
    ae048hOther: null,
    ae048hSd: null,
    ae048hUnit: null,
    clRMax: null,
    clRMean: null,
    clRMedian: null,
    clRMin: null,
    clROther: null,
    clRSd: null,
    clRUnit: null,
    otherMax: null,
    otherMean: null,
    otherMedian: null,
    otherMin: null,
    otherName: null,
    otherOther: null,
    otherSd: null,
    otherUnit: null,
    regimenCodeId: null,
    regimenOther: null,
    tlagMax: null,
    tlagMean: null,
    tlagMedian: null,
    tlagMin: null,
    tlagOther: null,
    tlagSd: null,
    tlagUnit: null,
    vdssMax: null,
    vdssMean: null,
    vdssMedian: null,
    vdssMin: null,
    vdssOther: null,
    vdssSd: null,
    vdssUnit: null,
    weightMax: null,
    weightMean: null,
    weightMedian: null,
    weightMin: null,
    weightOther: null,
    weightSd: null,
    weightUnit: null
  };

  const initAddFieldCnt = {
    concomitentDrug: 1
  };

  const initOpenField = {
    ethnicity: false,
    gender: false,
    age: false,
    weight: false,
    status: false,
    drugInfo: false,
    dose: false,
    fed: false,
    concomitent: false,
    auc024: false,
    auc0: false,
    cmax: false,
    tmax: false,
    t12: false,
    clf: false,
    ka: false,
    vd: false,
    vdss: false,
    ae024h: false,
    ae048h: false,
    clR: false,
    tlag: false,
    other: false
  };

  const [selectedId, setSelectedId] = useState(0);
  const [refInfos, setRefInfos] = useState({});
  const [addFieldCnt, setAddFieldCnt] = useState(initAddFieldCnt);
  const [openField, setOpenField] = useState(initOpenField);
  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const { updateRepeatByKey } = useRepeat(key);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  useEffect(
    () => {
      // reference title 받아오기
      const referenceIdArr = new Set();

      for (let i = 0; i < repeat.items.length; i++) {
        if (!!repeat.items[i].referenceId) {
          referenceIdArr.add(repeat.items[i].referenceId);
        }
      }

      if (referenceIdArr.size > 0) {
        tbdrugApi
          .fetchReferencesByIds({
            referenceIds: [...referenceIdArr].join(',')
          })
          .then(res => {
            setRefInfos(res.data);
          });
      }
    },
    [repeat.items]
  );

  useEffect(
    () => {
      const { genderMale = 0, genderFemale = 0 } = model;
      setModel({
        ...model,
        genderTotal: Number(genderMale) + Number(genderFemale)
      });
    },
    [model.genderMale, model.genderFemale]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      tbdrugId: subjectId,
      projectActivityId,
      [key]: model[key]
    });

    const newModel = {
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    };
    const {
      ethinicIsAfrican,
      ethinicIsAmerican,
      ethinicIsEastAsian,
      ethinicIsEuropean,
      ethinicIsSouthAsian,
      ethinicOther,
      genderMale,
      genderFemale,
      genderTotal,
      genderOther,
      ageMean,
      ageSd,
      ageMedian,
      ageMin,
      ageMax,
      ageOther,
      statusIsTb,
      statusIsHealthy,
      statusOther,
      drugInforBrandName,
      drugInforDosageForm,
      doseRouteCodeId,
      dose,
      doseOther,
      fedStatus,
      fedStatusOther,
      concomitentDrug1TbdrugId,
      concomitentDrug1Other,
      concomitentDrug1Dose,
      concomitentDrug1Etc,
      concomitentDrug2TbdrugId,
      concomitentDrug2Other,
      concomitentDrug2Dose,
      concomitentDrug2Etc,
      concomitentDrug3TbdrugId,
      concomitentDrug3Other,
      concomitentDrug3Dose,
      concomitentDrug3Etc,
      concomitentDrug4TbdrugId,
      concomitentDrug4Other,
      concomitentDrug4Dose,
      concomitentDrug4Etc,
      concomitentDrug5TbdrugId,
      concomitentDrug5Other,
      concomitentDrug5Dose,
      concomitentDrug5Etc,
      concomitentDrug6TbdrugId,
      concomitentDrug6Other,
      concomitentDrug6Dose,
      concomitentDrug6Etc,
      auc024Mean,
      auc024Sd,
      auc024Median,
      auc024Min,
      auc024Max,
      auc024Other,
      auc0InfiniteMean,
      auc0InfiniteSd,
      auc0InfiniteMedian,
      auc0InfiniteMin,
      auc0InfiniteMax,
      auc0InfiniteOther,
      cmaxMean,
      cmaxSd,
      cmaxMedian,
      cmaxMin,
      cmaxMax,
      cmaxOther,
      tmaxMean,
      tmaxSd,
      tmaxMedian,
      tmaxMin,
      tmaxMax,
      tmaxOther,
      t12Mean,
      t12Sd,
      t12Median,
      t12Min,
      t12Max,
      t12Other,
      clfMean,
      clfSd,
      clfMedian,
      clfMin,
      clfMax,
      clfOther,
      kaMean,
      kaSd,
      kaMedian,
      kaMin,
      kaMax,
      kaOther,
      vdMean,
      vdSd,
      vdMedian,
      vdMin,
      vdMax,
      vdOther,
      weightMean,
      weightSd,
      weightMedian,
      weightMin,
      weightMax,
      weightUnit,
      weightOther,
      regimenCodeId,
      regimenOther,
      vdssMean,
      vdssSd,
      vdssMedian,
      vdssMin,
      vdssMax,
      vdssUnit,
      vdssOther,
      ae024hMean,
      ae024hSd,
      ae024hMedian,
      ae024hMin,
      ae024hMax,
      ae024hUnit,
      ae024hOther,
      ae048hMean,
      ae048hSd,
      ae048hMedian,
      ae048hMin,
      ae048hMax,
      ae048hUnit,
      ae048hOther,
      clRMean,
      clRSd,
      clRMedian,
      clRMin,
      clRMax,
      clRUnit,
      clROther,
      tlagMean,
      tlagSd,
      tlagMedian,
      tlagMin,
      tlagMax,
      tlagUnit,
      tlagOther,
      otherName,
      otherMean,
      otherSd,
      otherMedian,
      otherMin,
      otherMax,
      otherUnit,
      otherOther
    } = newModel;
    const newAddFieldCnt = {
      ...addFieldCnt
    };
    for (let i = 1; i < MAX_FIELD_CNT; i++) {
      const curNum = i + 1;
      for (let key in addFieldCnt) {
        if (addFieldCnt.hasOwnProperty(key)) {
          const curKey = `${key}${curNum}`;
          if (!!newModel[`${curKey}TbdrugId`] || !!newModel[`${curKey}Other`]) {
            newAddFieldCnt[key] = curNum;
          }
        }
      }
    }

    const newOpenField = {
      ethnicity: isOpen(
        ethinicIsAfrican,
        ethinicIsAmerican,
        ethinicIsEastAsian,
        ethinicIsEuropean,
        ethinicIsSouthAsian,
        ethinicOther
      ),
      gender: isOpen(genderMale, genderFemale, genderTotal, genderOther),
      age: isOpen(ageMean, ageSd, ageMedian, ageMin, ageMax, ageOther),
      weight: isOpen(
        weightMean,
        weightSd,
        weightMedian,
        weightMin,
        weightMax,
        weightUnit,
        weightOther
      ),
      status: isOpen(statusIsTb, statusIsHealthy, statusOther),
      drugInfo: isOpen(drugInforBrandName, drugInforDosageForm),
      dose: isOpen(
        doseRouteCodeId,
        dose,
        doseOther,
        regimenCodeId,
        regimenOther
      ),
      fed: isOpen(fedStatus, fedStatusOther),
      concomitent: isOpen(
        concomitentDrug1TbdrugId,
        concomitentDrug1Other,
        concomitentDrug1Dose,
        concomitentDrug1Etc,
        concomitentDrug2TbdrugId,
        concomitentDrug2Other,
        concomitentDrug2Dose,
        concomitentDrug2Etc,
        concomitentDrug3TbdrugId,
        concomitentDrug3Other,
        concomitentDrug3Dose,
        concomitentDrug3Etc,
        concomitentDrug4TbdrugId,
        concomitentDrug4Other,
        concomitentDrug4Dose,
        concomitentDrug4Etc,
        concomitentDrug5TbdrugId,
        concomitentDrug5Other,
        concomitentDrug5Dose,
        concomitentDrug5Etc,
        concomitentDrug6TbdrugId,
        concomitentDrug6Other,
        concomitentDrug6Dose,
        concomitentDrug6Etc
      ),
      auc024: isOpen(
        auc024Mean,
        auc024Sd,
        auc024Median,
        auc024Min,
        auc024Max,
        auc024Other
      ),
      auc0: isOpen(
        auc0InfiniteMean,
        auc0InfiniteSd,
        auc0InfiniteMedian,
        auc0InfiniteMin,
        auc0InfiniteMax,
        auc0InfiniteOther
      ),
      cmax: isOpen(cmaxMean, cmaxSd, cmaxMedian, cmaxMin, cmaxMax, cmaxOther),
      tmax: isOpen(tmaxMean, tmaxSd, tmaxMedian, tmaxMin, tmaxMax, tmaxOther),
      t12: isOpen(t12Mean, t12Sd, t12Median, t12Min, t12Max, t12Other),
      clf: isOpen(clfMean, clfSd, clfMedian, clfMin, clfMax, clfOther),
      ka: isOpen(kaMean, kaSd, kaMedian, kaMin, kaMax, kaOther),
      vd: isOpen(vdMean, vdSd, vdMedian, vdMin, vdMax, vdOther),
      vdss: isOpen(
        vdssMean,
        vdssSd,
        vdssMedian,
        vdssMin,
        vdssMax,
        vdssUnit,
        vdssOther
      ),
      ae024h: isOpen(
        ae024hMean,
        ae024hSd,
        ae024hMedian,
        ae024hMin,
        ae024hMax,
        ae024hUnit,
        ae024hOther
      ),
      ae048h: isOpen(
        ae048hMean,
        ae048hSd,
        ae048hMedian,
        ae048hMin,
        ae048hMax,
        ae048hUnit,
        ae048hOther
      ),
      clR: isOpen(clRMean, clRSd, clRMedian, clRMin, clRMax, clRUnit, clROther),
      tlag: isOpen(
        tlagMean,
        tlagSd,
        tlagMedian,
        tlagMin,
        tlagMax,
        tlagUnit,
        tlagOther
      ),
      other: isOpen(
        otherName,
        otherMean,
        otherSd,
        otherMedian,
        otherMin,
        otherMax,
        otherUnit,
        otherOther
      )
    };

    setOpenField(newOpenField);

    setAddFieldCnt(newAddFieldCnt);

    setModel(newModel);
  });

  const handleOpenFieldClick = (event, { name }) => {
    setOpenField({
      ...openField,
      [name]: !openField[name]
    });
  };

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(id => {
    setModel({ ...model, [key]: id });
    setSelectedId(id);
  }, []);

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        [key]: 0,
        activityStatusCodeId: code.Temp
      });
      setOpenField({ ...initOpenField });
      setAddFieldCnt({ ...initAddFieldCnt });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleTotalInputKeyUp = e => {
    if (Number(e.currentTarget.value) !== model.genderTotal) {
      setModel({
        ...model,
        genderMale: '',
        genderFemale: ''
      });
    }
  };

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        skip={skip}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        refInfos={refInfos}
        MAX_FIELD_CNT={MAX_FIELD_CNT}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        doseRouteCodes={doseRouteCodes}
        dosfrqCodes={dosfrqCodes}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text="PK Data" />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'TbdrugPkdata',
                prefixLocale: 'activity.tbdrugPkdata',
                skip
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <PkDataForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            onChange={onChange}
            onSubmit={handleSubmit}
            refInfos={refInfos}
            onTotalInputKeyUp={handleTotalInputKeyUp}
            MAX_FIELD_CNT={MAX_FIELD_CNT}
            addFieldCnt={addFieldCnt}
            setAddFieldCnt={setAddFieldCnt}
            setModel={setModel}
            openField={openField}
            onOpenFieldClick={handleOpenFieldClick}
            doseRouteCodes={doseRouteCodes}
            dosfrqCodes={dosfrqCodes}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </PkDataForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default PkDataContainer;
