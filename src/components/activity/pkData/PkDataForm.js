import React, { useCallback, useEffect, useState } from 'react';
import {
  Form,
  Input,
  TextArea,
  Dropdown,
  Button,
  Icon
} from 'semantic-ui-react';
import { FormDoneOrTempBar } from '../';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import {
  FlexField,
  StyledCheckbox,
  StyledInput,
  SubFieldWrap,
  OpenFieldLabel,
  OpenFieldToggleBtn,
  StyledB,
  SmallInput
} from '../styles';
import {
  AgeFeild,
  StatusFeild,
  FedStatusFeild,
  ConcomitentyDrugsFeild,
  PkCommonFeild
} from '../DrugFormFeilds';
import { SelectReason } from '../Selector';
import { code } from '../../../constants';

const PkDataForm = ({
  MAX_FIELD_CNT,
  children,
  projectId,
  loading,
  model,
  onChange,
  onSubmit,
  refInfos,
  onTotalInputKeyUp,
  addFieldCnt,
  setAddFieldCnt,
  setModel,
  openField,
  onOpenFieldClick,
  doseRouteCodes,
  dosfrqCodes
}) => {
  const t = useFormatMessage();
  const [{ onValidate, validator }] = useSubmit(onSubmit);
  const [regimenOptions, setRegimenegimenOptions] = useState([]);

  useEffect(
    () => {
      if (dosfrqCodes.length > 0) {
        const singleDoseIdx = dosfrqCodes.findIndex(
          item => Number(item.value) === Number(code.SingleDose)
        );

        if (singleDoseIdx < 0) {
          setRegimenegimenOptions([...dosfrqCodes]);
        } else {
          setRegimenegimenOptions([
            dosfrqCodes[singleDoseIdx],
            ...dosfrqCodes.slice(0, singleDoseIdx),
            ...dosfrqCodes.slice(singleDoseIdx + 1)
          ]);
        }
      }
    },
    [dosfrqCodes]
  );

  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return '';
    },
    [refInfos]
  );

  const handleCheckboxChange = (event, { name, checked }) => {
    onChange(event, { name, value: checked });
  };

  const handleAddField = e => {
    e.preventDefault();
    if (addFieldCnt.concomitentDrug < MAX_FIELD_CNT) {
      const count = addFieldCnt.concomitentDrug + 1;
      const modelKey = `concomitentDrug${count}`;

      setAddFieldCnt({
        ...addFieldCnt,
        concomitentDrug: count
      });

      setModel({
        ...model,
        [`${modelKey}TbdrugId`]: '',
        [`${modelKey}Other`]: '',
        [`${modelKey}Dose`]: '',
        [`${modelKey}Unit`]: 'mg'
      });
    }
  };

  const handleRemoveField = key => () => {
    const orgTotalCount = addFieldCnt[key];
    const count = addFieldCnt[key] - 1;
    let newModel = { ...model };

    setAddFieldCnt({
      ...addFieldCnt,
      [key]: count
    });

    delete newModel[`${key}${orgTotalCount}TbdrugId`];
    delete newModel[`${key}${orgTotalCount}Other`];
    delete newModel[`${key}${orgTotalCount}Dose`];
    delete newModel[`${key}${orgTotalCount}Unit`];

    setModel(newModel);
  };

  const generateField = useCallback(
    () => {
      const result = [];
      const key = 'concomitentDrug';
      const count = addFieldCnt[key];

      for (let i = 0; i < count; i++) {
        const modelCount = i + 1;
        const modelKey = `${key}${modelCount}`;

        result.push(
          <ConcomitentyDrugsFeild
            key={modelKey}
            onChange={onChange}
            model={model}
            keyName={key}
            modelKey={modelKey}
            i={i}
            count={count}
            onRemoveField={handleRemoveField}
          />
        );
      }

      return result;
    },
    [model]
  );

  const isAddDrugBtnDisabled = (() => {
    for (let n = 0; n < addFieldCnt.concomitentDrug; n++) {
      if (typeof model[`concomitentDrug${n + 1}TbdrugId`] !== 'undefined') {
        if (
          !model[`concomitentDrug${n + 1}TbdrugId`] &&
          !model[`concomitentDrug${n + 1}Other`]
        ) {
          return true;
        }
      }
    }
    return false;
  })();

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle(model.referenceId)}
          name="referenceId"
          value={model.referenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="ethnicity"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.ethnicity ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Ethnicity]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.ethnicity}>
          <FlexField>
            <StyledCheckbox
              name="ethinicIsAfrican"
              label="African"
              checked={!!model.ethinicIsAfrican}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              label="Countries"
              placeholder="examle) South Africa, Ghana..."
              name="ethinicAfricanCountry"
              value={model.ethinicAfricanCountry || ''}
              onChange={onChange}
              disabled={!!!model.ethinicIsAfrican}
            />
          </FlexField>
          <FlexField>
            <StyledCheckbox
              name="ethinicIsAmerican"
              label="American"
              checked={!!model.ethinicIsAmerican}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              label="Countries"
              name="ethinicAmericanCountry"
              value={model.ethinicAmericanCountry || ''}
              onChange={onChange}
              disabled={!!!model.ethinicIsAmerican}
            />
          </FlexField>
          <FlexField>
            <StyledCheckbox
              name="ethinicIsEastAsian"
              label="East Asian"
              checked={!!model.ethinicIsEastAsian}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              label="Countries"
              name="ethinicEastAsianCountry"
              value={model.ethinicEastAsianCountry || ''}
              onChange={onChange}
              disabled={!!!model.ethinicIsEastAsian}
            />
          </FlexField>
          <FlexField>
            <StyledCheckbox
              name="ethinicIsEuropean"
              label="European"
              checked={!!model.ethinicIsEuropean}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              label="Countries"
              name="ethinicEuropeanCountry"
              value={model.ethinicEuropeanCountry || ''}
              onChange={onChange}
              disabled={!!!model.ethinicIsEuropean}
            />
          </FlexField>
          <FlexField>
            <StyledCheckbox
              name="ethinicIsSouthAsian"
              label="South Asian"
              checked={!!model.ethinicIsSouthAsian}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              label="Countries"
              name="ethinicSouthAsianCountry"
              value={model.ethinicSouthAsianCountry || ''}
              onChange={onChange}
              disabled={!!!model.ethinicIsSouthAsian}
            />
          </FlexField>
          <Form.Field>
            <Input
              label="Other detail"
              name="ethinicOther"
              value={model.ethinicOther || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="gender"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.gender ? 'check' : 'outline'}`}
              color="teal"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Gender(Number of subject)]</StyledB> data in the
          Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.gender}>
          <Form.Group widths="equal">
            <Form.Field>
              <Input
                label="Male"
                name="genderMale"
                value={model.genderMale || ''}
                onChange={onChange}
                type="number"
              />
            </Form.Field>
            <FlexField>
              <Input
                label="Female"
                name="genderFemale"
                value={model.genderFemale || ''}
                onChange={onChange}
                type="number"
                style={{ marginTop: 'auto' }}
              />
            </FlexField>
            <FlexField>
              <Input
                label="Total"
                name="genderTotal"
                value={model.genderTotal || ''}
                onChange={onChange}
                type="number"
                style={{ marginTop: 'auto' }}
                onKeyUp={onTotalInputKeyUp}
              />
            </FlexField>
            <FlexField>
              <Dropdown
                style={{ marginTop: 'auto' }}
                name="genderUnit"
                fluid
                selection
                value={model.genderUnit || 'Count'}
                onChange={onChange}
                options={[
                  {
                    text: 'Count',
                    value: 'Count'
                  },
                  {
                    text: '%',
                    value: '%'
                  }
                ]}
              />
            </FlexField>
          </Form.Group>
          <Form.Field>
            <Input
              label="Gender Other"
              name="genderOther"
              value={model.genderOther || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="age"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.age ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Age]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.age}>
          <AgeFeild onChange={onChange} model={model} />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="weight"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.weight ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Weight]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.weight}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'weightMean',
              sd: 'weightSd',
              median: 'weightMedian',
              min: 'weightMin',
              max: 'weightMax',
              unit: 'weightUnit',
              other: 'weightOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="status"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.status ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Status(disease/healthy)]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.status}>
          <StatusFeild
            onChange={onChange}
            model={model}
            onCheckboxChange={handleCheckboxChange}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="drugInfo"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.drugInfo ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Drug Infomation]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.drugInfo}>
          <Form.Field>
            <Input
              label="Brand name (or Investigated name)"
              name="drugInforBrandName"
              value={model.drugInforBrandName || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Dosage form"
              name="drugInforDosageForm"
              value={model.drugInforDosageForm || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="dose"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.dose ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Dose]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.dose}>
          <Form.Group widths="equal">
            <Form.Field>
              <label>Administration Route</label>
              <Dropdown
                name="doseRouteCodeId"
                search
                fluid
                selection
                value={model.doseRouteCodeId}
                onChange={onChange}
                options={[
                  {
                    text: t('common.notSelected'),
                    value: ''
                  },
                  ...doseRouteCodes
                ]}
              />
            </Form.Field>
            {model.doseRouteCodeId === code.DoseRouteOther && (
              <FlexField>
                <SmallInput
                  label="Other"
                  name="doseRouteOther"
                  onChange={onChange}
                  value={model.doseRouteOther || ''}
                  style={{ marginTop: 'auto' }}
                />
              </FlexField>
            )}
            <FlexField>
              <SmallInput
                label="Dose"
                name="dose"
                onChange={onChange}
                value={model.dose || ''}
                style={{ marginTop: 'auto' }}
              />
            </FlexField>
            <FlexField>
              <SmallInput
                label="Unit"
                name="doseUnit"
                value={
                  model.doseUnit === null ||
                  typeof model.doseUnit === 'undefined'
                    ? 'mg'
                    : model.doseUnit
                }
                onChange={onChange}
                style={{ marginTop: 'auto' }}
              />
            </FlexField>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field>
              <label>Regimen</label>
              <Dropdown
                name="regimenCodeId"
                search
                fluid
                selection
                value={model.regimenCodeId}
                onChange={onChange}
                options={[
                  {
                    text: t('common.notSelected'),
                    value: ''
                  },
                  ...regimenOptions
                ]}
              />
            </Form.Field>
            <FlexField>
              <SmallInput
                label="Other"
                name="regimenOther"
                onChange={onChange}
                value={model.regimenOther || ''}
                style={{ marginTop: 'auto' }}
              />
            </FlexField>
          </Form.Group>
          <Form.Field>
            <Input
              label="Other"
              name="doseOther"
              value={model.doseOther || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>
      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="fed"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.fed ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Fed Status]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.fed}>
          <FedStatusFeild onChange={onChange} model={model} />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="concomitent"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.concomitent ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Concomitent drugs]</StyledB> data in the Reference?
          {addFieldCnt.concomitentDrug < MAX_FIELD_CNT && (
            <Button
              type="button"
              style={{
                marginLeft: '10px'
                // marginBottom: '5px'
              }}
              disabled={isAddDrugBtnDisabled}
              onClick={handleAddField}
            >
              Add Drug
            </Button>
          )}
        </OpenFieldLabel>
        <SubFieldWrap open={openField.concomitent}>
          {generateField()}
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="auc024"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.auc024 ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[AUC0-24]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.auc024}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'auc024Mean',
              sd: 'auc024Sd',
              median: 'auc024Median',
              min: 'auc024Min',
              max: 'auc024Max',
              unit: 'auc024Unit',
              other: 'auc024Other'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="auc0"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.auc0 ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[AUC0–∞]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.auc0}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'auc0InfiniteMean',
              sd: 'auc0InfiniteSd',
              median: 'auc0InfiniteMedian',
              min: 'auc0InfiniteMin',
              max: 'auc0InfiniteMax',
              unit: 'auc0InfiniteUnit',
              other: 'auc0InfiniteOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="cmax"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.cmax ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Cmax]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.cmax}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'cmaxMean',
              sd: 'cmaxSd',
              median: 'cmaxMedian',
              min: 'cmaxMin',
              max: 'cmaxMax',
              unit: 'cmaxUnit',
              other: 'cmaxOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="tmax"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.tmax ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Tmax]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.tmax}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'tmaxMean',
              sd: 'tmaxSd',
              median: 'tmaxMedian',
              min: 'tmaxMin',
              max: 'tmaxMax',
              unit: 'tmaxUnit',
              other: 'tmaxOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="t12"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.t12 ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[T1/2]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.t12}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 't12Mean',
              sd: 't12Sd',
              median: 't12Median',
              min: 't12Min',
              max: 't12Max',
              unit: 't12Unit',
              other: 't12Other'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="clf"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.clf ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[CL/F]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.clf}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'clfMean',
              sd: 'clfSd',
              median: 'clfMedian',
              min: 'clfMin',
              max: 'clfMax',
              unit: 'clfUnit',
              other: 'clfOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="ka"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.ka ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Ka]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.ka}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'kaMean',
              sd: 'kaSd',
              median: 'kaMedian',
              min: 'kaMin',
              max: 'kaMax',
              unit: 'kaUnit',
              other: 'kaOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="vd"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.vd ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Vd]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.vd}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'vdMean',
              sd: 'vdSd',
              median: 'vdMedian',
              min: 'vdMin',
              max: 'vdMax',
              unit: 'vdUnit',
              other: 'vdOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="vdss"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.vdss ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Vdss]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.vdss}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'vdssMean',
              sd: 'vdssSd',
              median: 'vdssMedian',
              min: 'vdssMin',
              max: 'vdssMax',
              unit: 'vdssUnit',
              other: 'vdssOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="ae024h"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.ae024h ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Ae0-24h: amount excretion unchanged]</StyledB> data in
          the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.ae024h}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'ae024hMean',
              sd: 'ae024hSd',
              median: 'ae024hMedian',
              min: 'ae024hMin',
              max: 'ae024hMax',
              unit: 'ae024hUnit',
              other: 'ae024hOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="ae048h"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.ae048h ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Ae0-48h: amount excretion unchanged]</StyledB> data in
          the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.ae048h}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'ae048hMean',
              sd: 'ae048hSd',
              median: 'ae048hMedian',
              min: 'ae048hMin',
              max: 'ae048hMax',
              unit: 'ae048hUnit',
              other: 'ae048hOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="clR"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.clR ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[CLR : renal clearance]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.clR}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'clRMean',
              sd: 'clRSd',
              median: 'clRMedian',
              min: 'clRMin',
              max: 'clRMax',
              unit: 'clRUnit',
              other: 'clROther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="tlag"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.tlag ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Tlag: Lag time]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.tlag}>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'tlagMean',
              sd: 'tlagSd',
              median: 'tlagMedian',
              min: 'tlagMin',
              max: 'tlagMax',
              unit: 'tlagUnit',
              other: 'tlagOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="other"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.other ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Other PK parameter]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.other}>
          <Form.Field>
            <Input
              label="Name"
              name="otherName"
              value={model.otherName || ''}
              onChange={onChange}
            />
          </Form.Field>
          <PkCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'otherMean',
              sd: 'otherSd',
              median: 'otherMedian',
              min: 'otherMin',
              max: 'otherMax',
              unit: 'otherUnit',
              other: 'otherOther'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <label>Comments/discussion</label>
        <TextArea
          name="comments"
          value={model.comments || ''}
          onChange={onChange}
        />
      </Form.Field>

      {model.tbdrugPkdataId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="PK Data"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default PkDataForm;
