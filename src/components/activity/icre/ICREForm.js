import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { MyVisit, SelectReason } from '../Selector';
import { DatePicker } from '../../datepicker';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';

const ICREForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Field required>
        <label>{t('activity.icre.reicdat')}</label>
        <DatePicker name="reicdat" onChange={onChange} value={model.reicdat} />
        {validator.message(
          t('activity.icre.reicdat'),
          model.reicdat,
          'required'
        )}
      </Form.Field>
      <Form.Field required>
        <label>{t('activity.icre.icver')}</label>
        <Input name="icver" value={model.icver} onChange={onChange} />
        {validator.message(t('activity.icre.icver'), model.icver, 'required')}
      </Form.Field>
      {model.subjectIcreid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.icre.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default ICREForm;
