import React, { Fragment, useCallback, useState } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { RoleAware } from '../auth';
import api from './api';
import { notification } from '../modal';
import { usePublishStatus } from '../collection/hooks';
import Star from '../animation/Star';
import { code, role } from '../../constants';
import helpers from '../../helpers';
import { useLocation } from 'react-router-dom';

const Review = ({
  subjectId,
  projectActivityId,
  activityKeyId,
  activityStatusCodeId,
  onClick
}) => {
  const props = {
    subjectId,
    projectActivityId,
    activityKeyId,
    activityStatusCodeId
  };
  const location = useLocation();
  const query = helpers.qs.parse(location.search);
  const [loading, setLoading] = useState(false);
  const publish = usePublishStatus({
    projectActivityId
  });

  const handleFirstClick = useCallback(
    async event => {
      try {
        setLoading(true);
        const res = await api.reviewFirst(props);
        if (res && res.data) {
          notification.Any({
            title: <Star />,
            text: '1st Review Completed',
            onClose: () => {
              sendStatus(code.FirstSign);
              onClick(event, {
                name: 'save',
                action: 'review',
                activityStatusCodeId: code.FirstSign
              });
            }
          });
        }
        setLoading(false);
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [onClick]
  );

  const handleSecondClick = useCallback(
    async event => {
      try {
        setLoading(true);
        const res = await api.reviewSecond(props);
        if (res && res.data) {
          notification.Any({
            title: (
              <Fragment>
                <Star />
                <Star />
              </Fragment>
            ),
            text: '2nd Review Completed',
            onClose: () => {
              sendStatus(code.SecondSign);
              onClick(event, {
                name: 'save',
                action: 'review',
                activityStatusCodeId: code.SecondSign
              });
            }
          });
        }
        setLoading(false);
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [onClick]
  );

  const handleThirdClick = useCallback(
    async event => {
      try {
        setLoading(true);
        const res = await api.reviewThird(props);
        if (res && res.data) {
          notification.Any({
            title: (
              <Fragment>
                <Star />
                <Star />
                <Star />
              </Fragment>
            ),
            text: '3rd Review Completed',
            onClose: () => {
              sendStatus(code.ThirdSign);
              onClick(event, {
                name: 'save',
                action: 'review',
                activityStatusCodeId: code.ThirdSign
              });
            }
          });
        }
        setLoading(false);
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [onClick]
  );

  const sendStatus = status => {
    if (query.projectActivityId === projectActivityId) {
      publish.activityStatus(status);
    }
  };

  return (
    <Fragment>
      {activityStatusCodeId === code.Done && (
        <RoleAware allowedRole={role.Review_1st}>
          <Button
            basic
            loading={loading}
            disabled={loading}
            onClick={handleFirstClick}
          >
            <Icon name="star" color="yellow" />
            1st Review
          </Button>
        </RoleAware>
      )}
      {activityStatusCodeId === code.FirstSign && (
        <RoleAware allowedRole={role.Review_2nd}>
          <Button
            basic
            loading={loading}
            disabled={loading}
            onClick={handleSecondClick}
          >
            <Icon name="star" color="yellow" />
            <Icon name="star" color="yellow" />
            2nd Review
          </Button>
        </RoleAware>
      )}
      {(activityStatusCodeId === code.FirstSign ||
        activityStatusCodeId === code.SecondSign) && (
        <RoleAware allowedRole={role.Review_3rd}>
          <Button
            basic
            loading={loading}
            disabled={loading}
            onClick={handleThirdClick}
          >
            <Icon name="star" color="yellow" />
            <Icon name="star" color="yellow" />
            <Icon name="star" color="yellow" />
            3rd Review
          </Button>
        </RoleAware>
      )}
    </Fragment>
  );
};

export default Review;
