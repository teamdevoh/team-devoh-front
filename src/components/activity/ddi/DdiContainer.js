import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '../';
import { DdiForm, RepeatList } from './';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields, useCodes } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { useDdiList, useActionStatus, useDdi } from '../hooks';
import { AuthorInfo } from '../../collection';
import tbdrugApi from '../../collection-tbdrug/api';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useStatus from '../hooks/useStatus';
import useRepeat from '../hooks/useRepeat';
import { code, codeGroup } from '../../../constants';

const MAX_FIELD_CNT = 4;
const DdiContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'tbdrugDdiId';
  const { add, update, remove, fetch, isSaving, isFetching } = useDdi({
    activityKey
  });

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const [repeat, setRepeat] = useDdiList({
    tbdrugId: subjectId,
    projectActivityId
  });
  const [addFieldCnt, setAddFieldCnt] = useState({
    perpetretorTbdrugId: 1
  });
  const t = useFormatMessage();

  const [ethnicCodes] = useCodes({
    codeGroupId: codeGroup.EthnicCode
  });

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    hasNote: false,
    isDone: false,
    modified: null,
    author: '',
    [key]: '',
    activityId: activityKey,
    tbdrugId: subjectId,
    victimTbdrugId: null,
    victimOther: null,
    perpetretorTbdrugId1: null,
    perpetretorOther: null,
    pkDdi: null,
    mechanismOfDdi: null,
    reference: null,
    isChanged: false,
    reason: null
  };
  const [selectedId, setSelectedId] = useState(0);
  const [refInfos, setRefInfos] = useState({});
  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const { updateRepeatByKey } = useRepeat(key);
  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  useEffect(
    () => {
      // reference title 받아오기
      const referenceIdArr = new Set();

      for (let i = 0; i < repeat.items.length; i++) {
        if (!!repeat.items[i].reference) {
          referenceIdArr.add(repeat.items[i].reference);
        }
      }

      if (referenceIdArr.size > 0) {
        tbdrugApi
          .fetchReferencesByIds({
            referenceIds: [...referenceIdArr].join(',')
          })
          .then(res => {
            setRefInfos(res.data);
          });
      }
    },
    [repeat.items]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      tbdrugId: subjectId,
      projectActivityId,
      [key]: model[key]
    });
    const newModel = {
      ...model,
      ...data,
      projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    };

    const newAddFieldCnt = {
      ...addFieldCnt
    };
    for (let i = 1; i < MAX_FIELD_CNT; i++) {
      const curNum = i + 1;
      for (let key in addFieldCnt) {
        if (addFieldCnt.hasOwnProperty(key)) {
          const curKey = `${key}${curNum}`;
          if (!!newModel[curKey]) {
            newAddFieldCnt[key] = curNum;
          }
        }
      }
    }

    setAddFieldCnt(newAddFieldCnt);

    setModel(newModel);
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(id => {
    setModel({ ...model, [key]: id });
    setSelectedId(id);
  }, []);

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        [key]: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        refInfos={refInfos}
        ethnicCodes={ethnicCodes}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        MAX_FIELD_CNT={MAX_FIELD_CNT}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text="Drug-drug Interactions" />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'TbdrugDdi',
                prefixLocale: 'activity.tbdrugDdi',
                skip: [key, 'tbdrugId']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <DdiForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            onChange={onChange}
            onSubmit={handleSubmit}
            refInfos={refInfos}
            addFieldCnt={addFieldCnt}
            setAddFieldCnt={setAddFieldCnt}
            MAX_FIELD_CNT={MAX_FIELD_CNT}
            setModel={setModel}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </DdiForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default DdiContainer;
