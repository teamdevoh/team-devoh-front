import React, { useCallback } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import find from 'lodash/find';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = ({
  subjectId,
  updateRepeat,
  projectActivityId,
  items,
  loading,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  refInfos,
  MAX_FIELD_CNT,
  ethnicCodes,
  isSeleted
}) => {
  const t = useFormatMessage();
  const [drugs] = useTbdrugList();

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  const getTbdrugName = id => {
    const found = find(drugs.items, {
      value: id
    });
    if (found) {
      return found.text;
    }
    return 'unknown';
  };

  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return 'unknown';
    },
    [refInfos]
  );

  return (
    <Table compact="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Reference</Table.HeaderCell>
          <Table.HeaderCell>Victim</Table.HeaderCell>
          <Table.HeaderCell>Victim Other</Table.HeaderCell>
          <Table.HeaderCell>Perpetrator</Table.HeaderCell>
          <Table.HeaderCell>Perpetrator Other</Table.HeaderCell>
          <Table.HeaderCell>PK-DDI</Table.HeaderCell>
          <Table.HeaderCell>Mechanism of DDI</Table.HeaderCell>
          <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map(item => {
          const {
            activityStatusCodeId,
            mechanismOfDdi,
            perpetretorOther,
            pkDdi,
            reference,
            tbdrugDdiId,
            victimOther,
            victimTbdrugId
          } = item;

          const perpetretorTbdrug = [];

          for (let i = 0; i < MAX_FIELD_CNT; i++) {
            const curKey = `perpetretorTbdrugId${i + 1}`;

            if (!!item[curKey]) {
              perpetretorTbdrug.push(getTbdrugName(item[curKey]));
            }
          }

          return (
            <StyledTableRow key={tbdrugDdiId} selected={isSeleted(item)}>
              <Table.Cell>
                <ActivityItemAnchor
                  id={tbdrugDdiId}
                  text={getRefTitle(reference)}
                  onClick={onClick}
                />
              </Table.Cell>
              <Table.Cell>{getTbdrugName(victimTbdrugId)}</Table.Cell>
              <Table.Cell>{victimOther}</Table.Cell>
              <Table.Cell>{perpetretorTbdrug.join(', ')}</Table.Cell>
              <Table.Cell>{perpetretorOther}</Table.Cell>
              <Table.Cell>{pkDdi}</Table.Cell>
              <Table.Cell>{mechanismOfDdi}</Table.Cell>
              <Table.Cell>
                <StatusWrapper
                  subjectId={subjectId}
                  onChangeStatusCb={handleChangeStatus(item)}
                  activityKeyId={tbdrugDdiId}
                  projectActivityId={projectActivityId}
                  note={{
                    value: item.hasNote,
                    closed: onNoteModalClosed
                  }}
                  query={{
                    value: item.queryStatusCodeId,
                    closed: onQueryModalClosed
                  }}
                  file={{
                    value: item.hasFile,
                    closed: onFileModalClosed
                  }}
                  activity={{
                    value: activityStatusCodeId,
                    closed: () => {}
                  }}
                  audit={{
                    entityName: 'TbdrugDdi',
                    prefixLocale: 'activity.tbdrugDdi',
                    skip: ['tbdrugDdiId', 'tbdrugId']
                  }}
                  repeatItems={items}
                />
              </Table.Cell>
            </StyledTableRow>
          );
        })}
        {loading && (
          <EmptyRowsViewForSemantic
            loading={true}
            columnLength={8}
            size="large"
          />
        )}
      </Table.Body>
    </Table>
  );
};

export default RepeatList;
