import React, { useCallback, Fragment } from 'react';
import { Form, Input, TextArea, Icon } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '../';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import { TbdrugList } from '../../collection-tbdrug/Selector';
import { SelectReason } from '../Selector';
import { StyledLabel, StyledButton } from '../styles';

const DdiForm = ({
  children,
  projectId,
  loading,
  model,
  onChange,
  onSubmit,
  refInfos,
  addFieldCnt,
  setAddFieldCnt,
  MAX_FIELD_CNT,
  setModel
}) => {
  const t = useFormatMessage();
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  const getRefTitle = useCallback(
    id => {
      if (typeof refInfos[id] !== 'undefined') {
        return refInfos[id].title;
      }

      return '';
    },
    [refInfos]
  );

  const handleAddField = key => () => {
    if (addFieldCnt[key] < MAX_FIELD_CNT) {
      const count = addFieldCnt[key] + 1;
      const modelKey = `${key}${count}`;

      setAddFieldCnt({
        ...addFieldCnt,
        [key]: count
      });

      setModel({
        ...model,
        [modelKey]: ''
      });
    }
  };
  const handleRemoveField = key => () => {
    const orgTotalCount = addFieldCnt[key];
    const count = addFieldCnt[key] - 1;
    let newModel = { ...model };

    setAddFieldCnt({
      ...addFieldCnt,
      [key]: count
    });

    delete newModel[`${key}${orgTotalCount}`];

    setModel(newModel);
  };

  const generateField = useCallback(
    ({ key, count, label, addBtnTitle }) => {
      const result = [];

      for (let i = 0; i < count; i++) {
        const modelCount = i + 1;
        const modelKey = `${key}${modelCount}`;

        const addBtnDisabled = (() => {
          for (let n = 0; n < addFieldCnt[key]; n++) {
            if (typeof model[`${key}${n + 1}`] !== 'undefined') {
              if (!model[`${key}${n + 1}`]) {
                return true;
              }
            }
          }
          return false;
        })();

        result.push(
          <Form.Field key={modelKey}>
            <StyledLabel>
              {label} {count > 1 && i + 1}
            </StyledLabel>
            {i === count - 1 && (
              // 추가된 마지막 필드에 x랑 add 버튼 둠
              <Fragment>
                {i !== 0 && (
                  <StyledButton icon onClick={handleRemoveField(key)}>
                    <Icon name="x" />
                  </StyledButton>
                )}
                {count < MAX_FIELD_CNT && (
                  <StyledButton
                    type="button"
                    disabled={addBtnDisabled}
                    onClick={handleAddField(key)}
                  >
                    {addBtnTitle}
                  </StyledButton>
                )}
              </Fragment>
            )}
            <TbdrugList
              defaultOption={[
                {
                  text: t('common.notSelected'),
                  value: ''
                }
              ]}
              fluid
              name={modelKey}
              value={model[modelKey]}
              onChange={onChange}
            />
          </Form.Field>
        );
      }

      return result;
    },
    [model]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle(model.reference)}
          name="reference"
          value={model.reference}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Victim</label>
        <TbdrugList
          defaultOption={[
            {
              text: t('common.notSelected'),
              value: ''
            }
          ]}
          fluid
          name="victimTbdrugId"
          value={model.victimTbdrugId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Victim Other</label>
        <Input
          name="victimOther"
          value={model.victimOther || ''}
          onChange={onChange}
        />
      </Form.Field>
      {generateField({
        key: 'perpetretorTbdrugId',
        count: addFieldCnt.perpetretorTbdrugId,
        label: 'Perpetrator',
        addBtnTitle: 'Add'
      })}
      <Form.Field>
        <label>Perpetrator Other</label>
        <Input
          name="perpetretorOther"
          value={model.perpetretorOther || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>PK-DDI</label>
        <TextArea name="pkDdi" value={model.pkDdi || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>Mechanism of DDI</label>
        <TextArea
          name="mechanismOfDdi"
          value={model.mechanismOfDdi || ''}
          onChange={onChange}
        />
      </Form.Field>
      {model.tbdrugDdiId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="Drug-drug Interactions"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default DdiForm;
