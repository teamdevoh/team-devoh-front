import React from 'react';

const LowSpan = ({ text }) => {
  return <span style={{ color: '#fbbd08' }}>{text}</span>;
};
const HighSpan = ({ text }) => {
  return <span style={{ color: 'red' }}>{text}</span>;
};

export { LowSpan, HighSpan };
