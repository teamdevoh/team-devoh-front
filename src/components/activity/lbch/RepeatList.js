import React, { memo, Fragment } from 'react';
import { Table, Label } from 'semantic-ui-react';
import { useFormatMessage, useVerifyRange, useTypeCheck } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import find from 'lodash/find';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { activity } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const CustomCell = ({ min, max, value, unit }) => {
  const { isNumeric } = useTypeCheck();
  const [level] = useVerifyRange({ min, max, value });

  const renderValueItem = () => {
    if (isNumeric(value)) {
      if (level && level !== activity.LabRanges.NORMAL.name) {
        return (
          <Label color={activity.LabRanges[level].color.name}>
            {value} {unit}
          </Label>
        );
      } else {
        return (
          <Fragment>
            {value} {unit}
          </Fragment>
        );
      }
    } else {
      return <Fragment>{value}</Fragment>;
    }
  };

  return <Table.Cell title={value}>{renderValueItem()}</Table.Cell>;
};

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    labRange,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const { isNumeric } = useTypeCheck();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['lbdtc'], ['asc']);

    let alb = {};
    let bun = {};
    let cr = {};
    let egfr = {};
    let tbil = {};
    let tpro = {};
    let ast = {};
    let alt = {};
    let alp = {};
    let ua = {};

    if (labRange.ranges) {
      const lbchItems = activity.LabLBCHItems;

      alb = find(labRange.ranges, { item: lbchItems.ALB });
      bun = find(labRange.ranges, { item: lbchItems.BUN });
      cr = find(labRange.ranges, { item: lbchItems.CR });
      egfr = find(labRange.ranges, { item: lbchItems.EGFR });
      tbil = find(labRange.ranges, { item: lbchItems.TBIL });
      tpro = find(labRange.ranges, { item: lbchItems.TPRO });
      ast = find(labRange.ranges, { item: lbchItems.AST });
      alt = find(labRange.ranges, { item: lbchItems.ALT });
      alp = find(labRange.ranges, { item: lbchItems.ALP });
      ua = find(labRange.ranges, { item: lbchItems.UA });
    }

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="two">
              {t('activity.lbch.lbdtc')}
            </Table.HeaderCell>
            <Table.HeaderCell>ALB</Table.HeaderCell>
            <Table.HeaderCell>BUN</Table.HeaderCell>
            <Table.HeaderCell>CR</Table.HeaderCell>
            <Table.HeaderCell>eGFR</Table.HeaderCell>
            <Table.HeaderCell>TBIL</Table.HeaderCell>
            <Table.HeaderCell>TPRO</Table.HeaderCell>
            <Table.HeaderCell>AST</Table.HeaderCell>
            <Table.HeaderCell>ALT</Table.HeaderCell>
            <Table.HeaderCell>ALP</Table.HeaderCell>
            <Table.HeaderCell>UA</Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            return (
              <StyledTableRow
                key={item.subjectLbchid}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectLbchid}
                    text={item.lbdtc}
                    projectActivityId={
                      item.projectActivityId || projectActivityId
                    }
                    onClick={onClick}
                  />
                </Table.Cell>
                <CustomCell
                  min={alb.low}
                  max={alb.high}
                  value={item.alb}
                  unit={alb.unit}
                />
                <CustomCell
                  min={bun.low}
                  max={bun.high}
                  value={item.bun}
                  unit={bun.unit}
                />
                <CustomCell
                  min={cr.low}
                  max={cr.high}
                  value={item.cr}
                  unit={cr.unit}
                />
                <Table.Cell>
                  {item.egfr}
                  {isNumeric(item.egfr) ? egfr.unit : ''}
                </Table.Cell>
                <CustomCell
                  min={tbil.low}
                  max={tbil.high}
                  value={item.tbil}
                  unit={tbil.unit}
                />
                <CustomCell
                  min={tpro.low}
                  max={tpro.high}
                  value={item.tpro}
                  unit={tpro.unit}
                />
                <CustomCell
                  min={ast.low}
                  max={ast.high}
                  value={item.ast}
                  unit={ast.unit}
                />
                <CustomCell
                  min={alt.low}
                  max={alt.high}
                  value={item.alt}
                  unit={alt.unit}
                />
                <CustomCell
                  min={alp.low}
                  max={alp.high}
                  value={item.alp}
                  unit={alp.unit}
                />
                <CustomCell
                  min={ua.low}
                  max={ua.high}
                  value={item.ua}
                  unit={ua.unit}
                />
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectLbchid}
                    projectActivityId={
                      item.projectActivityId || projectActivityId
                    }
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectLbch',
                      prefixLocale: 'activity.lbch',
                      skip: ['subjectLbchid']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={11}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
