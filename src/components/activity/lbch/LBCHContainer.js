import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '..';
import { AuthorInfo } from '../../collection';
import { LBCHForm, RepeatList } from '.';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { useLBCHs, useLBCH, useActionStatus, useLabRange } from '../hooks';
import { memo } from 'react';
import { LeavingGuard } from '../../modal';
import StatusWrapper from '../StatusWrapper';
import useStatus from '../hooks/useStatus';
import useRepeat from '../hooks/useRepeat';
import { activity, code } from '../../../constants';

const LBCHContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'subjectLbchid';

  const updateRepeat = (activityStatusCodeId, data = {}) => {
    let target = model;
    let setTarget = setModel;

    if (data[key] > 0) {
      target = data;
      if (data[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const { add, update, remove, fetch, isSaving, isFetching } = useLBCH({
    activityKey
  });
  const [selectedId, setSelectedId] = useState(0);
  const [repeat, setRepeat] = useLBCHs({ subjectId, projectActivityId });
  const [labRange] = useLabRange({ subjectId, lbType: activity.LabType.LBCH });
  const t = useFormatMessage();

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    subjectLbchid: '',
    subjectId: subjectId,
    subjectVisitId: '',
    lbdtc: '',
    alb: '',
    bun: '',
    cr: '',
    egfr: '',
    tbil: '',
    tpro: '',
    ast: '',
    alt: '',
    alp: '',
    ua: '',
    modified: '',
    author: '',
    isChanged: false,
    reason: ''
  };

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );
  const { updateRepeatByKey } = useRepeat(key, projectActivityId);
  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      subjectId,
      projectActivityId: model.projectActivityId || projectActivityId,
      subjectLbchid: model[key]
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: data.projectActivityId || projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    (id, projectActivityId) => {
      setModel({ ...model, projectActivityId, subjectLbchid: id });
      setSelectedId(id);
    },
    [model, selectedId]
  );

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        subjectLbchid: 0,
        activityStatusCodeId: code.Temp
      });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      {labRange.ranges &&
        labRange.ranges.length > 0 && (
          <Button onClick={handleAddClick}>{t('activity.add')}</Button>
        )}
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={model.projectActivityId || projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        labRange={labRange}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text={t('activity.lbch.title')} />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={model.projectActivityId || projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'SubjectLbch',
                prefixLocale: 'activity.lbch',
                skip: ['subjectLbchid']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <LBCHForm
            projectId={projectId}
            labRange={labRange}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={model.projectActivityId || projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </LBCHForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default memo(LBCHContainer);
