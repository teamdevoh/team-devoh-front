import React from 'react';
import { Form, Icon } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '..';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DateInput, WarningMinMaxInput } from '../../input';
import { useCalcEGFR } from '../hooks';
import { SelectReason } from '../Selector';
import { LowSpan, HighSpan } from './Range';
import { activity, code } from '../../../constants';

const LBCHForm = ({
  children,
  projectId,
  labRange,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  const lbchItems = activity.LabLBCHItems;

  const alb = labRange.ranges.find(x => x.item === lbchItems.ALB);
  const bun = labRange.ranges.find(x => x.item === lbchItems.BUN);
  const cr = labRange.ranges.find(x => x.item === lbchItems.CR);
  const egfr = labRange.ranges.find(x => x.item === lbchItems.EGFR);
  const tbil = labRange.ranges.find(x => x.item === lbchItems.TBIL);
  const tpro = labRange.ranges.find(x => x.item === lbchItems.TPRO);
  const ast = labRange.ranges.find(x => x.item === lbchItems.AST);
  const alt = labRange.ranges.find(x => x.item === lbchItems.ALT);
  const alp = labRange.ranges.find(x => x.item === lbchItems.ALP);
  const ua = labRange.ranges.find(x => x.item === lbchItems.UA);

  const calcegfr = useCalcEGFR({
    sex: labRange.sex === code.MALE ? 'Male' : 'Female',
    age: labRange.age,
    creatine: model.cr
  });

  const handleCalcEGFRClick = () => {
    const egfrValue = calcegfr.run();
    onChange(null, { name: 'egfr', value: egfrValue });
  };

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>
          {t('activity.lbch.lbdtc')}
          {t('activity.lbch.lbdtc.des')}
        </label>
        <DateInput
          name="lbdtc"
          value={model.lbdtc}
          placeholder="ukuk-uk-uk"
          options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
          onChange={onChange}
        />
        {validator.message(t('activity.lbch.lbdtc'), model.lbdtc, 'required')}
      </Form.Field>
      <Form.Group widths="equal">
        <Form.Field>
          <label>
            Albumin (<LowSpan text={alb.low} /> ~ <HighSpan text={alb.high} />)
          </label>
          <WarningMinMaxInput
            name="alb"
            value={model.alb}
            min={alb.low}
            max={alb.high}
            unit={alb.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            BUN (<LowSpan text={bun.low} /> ~ <HighSpan text={bun.high} />)
          </label>
          <WarningMinMaxInput
            name="bun"
            value={model.bun}
            min={bun.low}
            max={bun.high}
            unit={bun.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            Creatinine (<LowSpan text={cr.low} /> ~ <HighSpan text={cr.high} />)
          </label>
          <WarningMinMaxInput
            name="cr"
            value={model.cr}
            min={cr.low}
            max={cr.high}
            unit={cr.unit}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>
            eGFR (CKD-EPI)
            <Icon
              name="calculator"
              size="large"
              className="link"
              onClick={handleCalcEGFRClick}
            />
          </label>
          <WarningMinMaxInput
            name="egfr"
            value={model.egfr}
            min={egfr.low}
            max={egfr.high}
            unit={egfr.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            Total Bilirubin (<LowSpan text={tbil.low} /> ~{' '}
            <HighSpan text={tbil.high} />)
          </label>
          <WarningMinMaxInput
            name="tbil"
            value={model.tbil}
            min={tbil.low}
            max={tbil.high}
            unit={tbil.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            Total Protein (<LowSpan text={tpro.low} /> ~{' '}
            <HighSpan text={tpro.high} />)
          </label>
          <WarningMinMaxInput
            name="tpro"
            value={model.tpro}
            min={tpro.low}
            max={tpro.high}
            unit={tpro.unit}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>
            AST (SGOT) (<LowSpan text={ast.low} /> ~{' '}
            <HighSpan text={ast.high} />)
          </label>
          <WarningMinMaxInput
            name="ast"
            value={model.ast}
            min={ast.low}
            max={ast.high}
            unit={ast.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            ALT (SGPT) (<LowSpan text={alt.low} /> ~{' '}
            <HighSpan text={alt.high} />)
          </label>
          <WarningMinMaxInput
            name="alt"
            value={model.alt}
            min={alt.low}
            max={alt.high}
            unit={alt.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.lbch.alp')} (<LowSpan text={alp.low} /> ~{' '}
            <HighSpan text={alp.high} />)
          </label>
          <WarningMinMaxInput
            name="alp"
            value={model.alp}
            min={alp.low}
            max={alp.high}
            unit={alp.unit}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            Uric acid (<LowSpan text={ua.low} /> ~ <HighSpan text={ua.high} />)
          </label>
          <WarningMinMaxInput
            name="ua"
            value={model.ua}
            min={ua.low}
            max={ua.high}
            unit={ua.unit}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {model.subjectLbchid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.lbch.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default LBCHForm;
