import React from 'react';
import { Form, Input, TextArea } from 'semantic-ui-react';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DateInput } from '../../input';
import { SelectReason } from '../Selector';

const OtherExamForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.otherexam.otherExamDtc')}</label>
        <DateInput
          name="otherExamDtc"
          value={model.otherExamDtc}
          placeholder="ukuk-uk-uk"
          options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
          onChange={onChange}
        />
        {validator.message(
          t('activity.otherexam.otherExamDtc'),
          model.otherExamDtc,
          'required'
        )}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.otherexam.otherExam')}</label>
        <Input name="otherExam" value={model.otherExam} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.otherexam.otherExamRes')}</label>
        <TextArea
          name="otherExamRes"
          value={model.otherExamRes}
          onChange={onChange}
        />
      </Form.Field>
      {model.subjectOtherExamId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.otherexam.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default OtherExamForm;
