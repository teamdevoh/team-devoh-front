import React, { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { Message } from 'semantic-ui-react';
import helpers from '../../helpers';

const RequireVisit = () => {
  const { projectId, subjectId } = useParams();

  const handleClick = useCallback(() => {
    helpers.history.push(`/project/${projectId}/subject/${subjectId}/step/1`);
  }, []);

  return (
    <Message warning>
      <Message.Header>You must register before you can do that!</Message.Header>
      <p>
        <a style={{ cursor: 'pointer' }} onClick={handleClick}>
          Visit our schedule registration page, then try again.
        </a>
      </p>
    </Message>
  );
};

export default RequireVisit;
