import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { ActionBar, FormTitle, FormMetaBar } from '..';
import { AuthorInfo } from '../../collection';
import { TDMDrugForm, RepeatList } from '.';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import {
  useTDMDrugs,
  useTDMDrug,
  useActionStatus,
  useProjectActivityGet,
  useSubTBDrug
} from '../hooks';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import helpers from '../../../helpers';
import LinkButton from '../../button/LinkButton';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { Label } from 'semantic-ui-react';
import { AddSubjectVisitButton } from '../Selector';
import styled from 'styled-components';
import { activity, activityGroup, code } from '../../../constants';

const ButtonWrap = styled.div`
  display: inline-flex;
  flex-wrap: wrap;
  flex: 1;
  align-items: center;
  width: 100%;
`;

const RightButtonWrap = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
`;

const uniqueKey = model => {
  return `${model.subjectTbdrugId}|${model.subjectVisitId}`;
};

const TDMDrugContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const key = 'subjectTdmdrugId';
  const tbDrugActivity = useProjectActivityGet({
    projectId,
    activityGroupId: activityGroup.MedicationHistory,
    activityId: activity.MEDICATION
  });

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }

    let newItem = true;
    const newRepeat = repeat.items.map(item => {
      if (uniqueKey(item) === uniqueKey(target)) {
        newItem = false;
        return { ...item, ...target, activityStatusCodeId };
      } else {
        return item;
      }
    });

    if (newItem) {
      newRepeat.push(target);
    }

    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget,
      updatedRepeat: newRepeat
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const { add, update, remove, fetch, isSaving, isFetching } = useTDMDrug({
    activityKey
  });
  const [selectedId, setSelectedId] = useState('');
  const [repeat, setRepeat] = useTDMDrugs({ subjectId, projectActivityId });
  const t = useFormatMessage();

  const { fetch: fetchSubTBDrug } = useSubTBDrug({});
  const [subTbdrugModel, setSubTbdrugModel] = useState({});

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    isDone: false,
    subjectTbdrugId: '',
    [key]: '',
    subjectId: subjectId,
    subjectVisitId: '',
    medDatetimeAm: '',
    medDatetimeMd: '',
    medDatetimePm: '',
    fastFed: '',
    fastFedCodeId: '',
    endmealDatetime: '',
    modified: '',
    isChanged: false,
    author: '',
    reason: ''
  };

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const { updateRepeatByKey } = useRepeat();

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId.length > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      subjectId,
      projectActivityId,
      subjectTbdrugId: model.subjectTbdrugId,
      subjectVisitId: model.subjectVisitId
    });
    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true
    });

    // 복용중인 결핵 약물 조회
    const subTbdrugData = await fetchSubTBDrug({
      subjectId,
      projectActivityId: tbDrugActivity.projectActivityId,
      subjectTbdrugId: data.subjectTbdrugId
    });
    setSubTbdrugModel(subTbdrugData);
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    repeatItem => {
      setModel({
        ...model,
        subjectTbdrugId: repeatItem.subjectTbdrugId,
        subjectVisitId: repeatItem.subjectVisitId
      });
      setSelectedId(
        uniqueKey(repeatItem)
        //`${repeatItem.subjectTbdrugId}|${repeatItem.subjectVisitId}`
      );
    },
    [model, selectedId]
  );

  const handleMoveToTBDrug = useCallback(
    () => {
      helpers.history.push(
        `/project/${projectId}/subject/${subjectId}/activitygroup/2?projectActivityId=${
          tbDrugActivity.projectActivityId
        }&activityId=21`
      );
    },
    [tbDrugActivity]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    }
  };

  const isSeleted = useCallback(
    item => {
      return `${uniqueKey(item)}` === `${selectedId}`;
    },
    [selectedId]
  );

  return (
    <Fragment>
      <ButtonWrap>
        <LinkButton
          name={t('activity.subtbdrug.title')}
          color="teal"
          onClick={handleMoveToTBDrug}
        />
        <RightButtonWrap>
          <Label basic color="teal" pointing="right">
            {t('add.tdm.visit')}
          </Label>
          <AddSubjectVisitButton projectId={projectId} subjectId={subjectId} />
        </RightButtonWrap>
      </ButtonWrap>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model.subjectTbdrugId !== '' && (
        <Fragment>
          <FormTitle text={t('activity.tdmdrug.title')} />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'SubjectTdmdrug',
                prefixLocale: 'activity.tdmdrug',
                skip: [key, 'subjectTbdrugId']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <TDMDrugForm
            projectId={projectId}
            loading={isFetching}
            model={model}
            setModel={setModel}
            onChange={onChange}
            onSubmit={handleSubmit}
            subTbdrugModel={subTbdrugModel}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </TDMDrugForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default TDMDrugContainer;
