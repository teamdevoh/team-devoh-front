import React, { memo, Fragment } from 'react';
import { Table, Icon } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import helpers from '../../../helpers';
import { ActivityItemAnchor } from '../';
import StatusWrapper from '../StatusWrapper';
import { useBuild } from '../hooks';
import { codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [fastFedCodeCodes] = useCodes({
      codeGroupId: codeGroup.FastFedCode
    });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    return (
      <div style={{ overflowX: 'scroll' }}>
        <Table compact="very" singleLine>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width="two">
                {t('activity.subtbdrug.exdrugid')}
              </Table.HeaderCell>
              <Table.HeaderCell width="two">Dose</Table.HeaderCell>
              <Table.HeaderCell width="two">
                {t('activity.subtbdrug.exstdtc')}
              </Table.HeaderCell>
              <Table.HeaderCell width="two">
                {t('activity.subtbdrug.exendtc')}
              </Table.HeaderCell>
              <Table.HeaderCell width="four">
                {t('activity.visit')}
              </Table.HeaderCell>
              <Table.HeaderCell width="three">
                {t('activity.tdmdrug.medDatetime')}
              </Table.HeaderCell>
              <Table.HeaderCell width="two">
                {t('activity.tdmdrug.fastFedCodeId')}
              </Table.HeaderCell>
              <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {items.map((item, index) => {
              const fastFed = build.displayName({
                source: fastFedCodeCodes,
                value: item.fastFedCodeId
              });
              const exdose = item.exdose ? `${item.exdose} mg` : '';
              const hasVisitDate = item.visitDateTime ? true : false;

              const date = hasVisitDate
                ? `(${t('activity.visit')})${helpers.util.dateformat(
                    item.visitDateTime
                  )}`
                : `(${t('schedule.date')})${helpers.util.dateformat(
                    item.scheduleDateTime
                  )}`;

              return (
                <StyledTableRow key={index} selected={isSeleted(item)}>
                  <Table.Cell>
                    <ActivityItemAnchor
                      id={item.subjectTbdrugId}
                      text={item.genericName}
                      onClick={() => {
                        onClick(item);
                      }}
                    />
                  </Table.Cell>
                  <Table.Cell title={exdose}>{exdose}</Table.Cell>
                  <Table.Cell title={item.exstdtc}>{item.exstdtc}</Table.Cell>
                  <Table.Cell title={item.exendtc}>{item.exendtc}</Table.Cell>
                  <Table.Cell>{`${date}`}</Table.Cell>
                  <Table.Cell>
                    {item.medDatetimeAm && (
                      <Fragment>
                        <Icon
                          name="sun"
                          color="yellow"
                          title={t('activity.tdmdrug.medDatetimeAm')}
                        />
                        <span title={item.medDatetimeAm}>
                          {item.medDatetimeAm}
                        </span>
                      </Fragment>
                    )}
                    <br />
                    {item.medDatetimeMd && (
                      <Fragment>
                        <Icon
                          name="sun"
                          color="orange"
                          title={t('activity.tdmdrug.medDatetimeMd')}
                        />
                        <span title={item.medDatetimeMd}>
                          {item.medDatetimeMd}
                        </span>
                      </Fragment>
                    )}
                    <br />
                    {item.medDatetimePm && (
                      <Fragment>
                        <Icon
                          name="moon"
                          title={t('activity.tdmdrug.medDatetimePm')}
                        />
                        <span title={item.medDatetimePm}>
                          {item.medDatetimePm}
                        </span>
                      </Fragment>
                    )}
                  </Table.Cell>
                  <Table.Cell title={fastFed}>{fastFed}</Table.Cell>
                  <Table.Cell>
                    <StatusWrapper
                      subjectId={subjectId}
                      onChangeStatusCb={handleChangeStatus(item)}
                      activityKeyId={item.subjectTdmdrugId}
                      projectActivityId={projectActivityId}
                      note={{
                        value: item.hasNote,
                        closed: onNoteModalClosed
                      }}
                      query={{
                        value: item.queryStatusCodeId,
                        closed: onQueryModalClosed
                      }}
                      file={{
                        value: item.hasFile,
                        closed: onFileModalClosed
                      }}
                      activity={{
                        value: item.activityStatusCodeId,
                        closed: () => {}
                      }}
                      audit={{
                        entityName: 'SubjectTdmdrug',
                        prefixLocale: 'activity.tdmdrug',
                        skip: ['subjectTdmdrugId', 'subjectTbdrugId']
                      }}
                      repeatItems={items}
                    />
                  </Table.Cell>
                </StyledTableRow>
              );
            })}
            {loading && (
              <EmptyRowsViewForSemantic
                loading={true}
                columnLength={8}
                size="large"
              />
            )}
          </Table.Body>
        </Table>
      </div>
    );
  }
);

export default RepeatList;
