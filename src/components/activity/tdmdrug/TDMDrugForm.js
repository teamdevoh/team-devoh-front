import React, { useCallback } from 'react';
import { Form } from 'semantic-ui-react';
import { FastFed, MyVisit, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit, useCodes } from '../../../hooks';
import DateInput from '../../input/DateInput';
import { useBuild } from '../hooks';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import { code, codeGroup, format } from '../../../constants';

const TDMDrugForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  subTbdrugModel
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);
  const [drug] = useTbdrugList();
  const build = useBuild();

  const drugName = build.displayName({
    source: drug.items,
    value: subTbdrugModel.exdrugid,
    other: subTbdrugModel.exdrugoth,
    otherValue: code.TBDrugOther
  });
  const [dosfrqCodes] = useCodes({ codeGroupId: codeGroup.DosfrqCode });
  const [dosfrmCodes] = useCodes({ codeGroupId: codeGroup.DosfrmCode });
  const [routeCodes] = useCodes({ codeGroupId: codeGroup.RouteCode });

  const generateExdose = useCallback(
    () => {
      let dose = [];

      const {
        exdose,
        exdosfrmcodeId,
        exdosfrmoth,
        exdosfrqcodeId,
        exdosfrqoth,
        exroutecodeId,
        exrouthoth
      } = subTbdrugModel;

      if (!!exdose) {
        dose.push(`${exdose} mg`);
      }

      if (!!exdosfrqcodeId) {
        const frq = build.displayName({
          source: dosfrqCodes,
          value: exdosfrqcodeId,
          other: exdosfrqoth,
          otherValue: code.DosfrqOther
        });
        dose.push(frq);
      }

      if (!!exdosfrmcodeId) {
        const frm = build.displayName({
          source: dosfrmCodes,
          value: exdosfrmcodeId,
          other: exdosfrmoth,
          otherValue: code.DosfrmOther
        });
        dose.push(frm);
      }

      if (!!exroutecodeId) {
        const route = build.displayName({
          source: routeCodes,
          value: exroutecodeId,
          other: exrouthoth,
          otherValue: code.RouteOther
        });
        dose.push(route);
      }

      return dose.join(', ');
    },
    [subTbdrugModel]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <label>{t('activity.subtbdrug.drugName')}</label>
        {drugName}
        {`${subTbdrugModel.extrt ? ` - ${subTbdrugModel.extrt}` : ''}`}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.subtbdrug.exdose')}</label>
        {generateExdose()}
      </Form.Field>
      <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
          visitTypeCode={code.TDMVisit}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.tdmdrug.medDatetimeAm')}</label>
        <DateInput
          name="medDatetimeAm"
          value={model.medDatetimeAm}
          placeholder={format.YYYY_MM_DD_UKUK}
          options={{ dateFormat: format.YYYY_MM_DD_UKUK, separate: '-' }}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.tdmdrug.medDatetimeMd')}</label>
        <DateInput
          name="medDatetimeMd"
          value={model.medDatetimeMd}
          placeholder={format.YYYY_MM_DD_UKUK}
          options={{ dateFormat: format.YYYY_MM_DD_UKUK, separate: '-' }}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.tdmdrug.medDatetimePm')}</label>
        <DateInput
          name="medDatetimePm"
          value={model.medDatetimePm}
          placeholder={format.YYYY_MM_DD_UKUK}
          options={{ dateFormat: format.YYYY_MM_DD_UKUK, separate: '-' }}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.tdmdrug.fastFedCodeId')}</label>
        <FastFed
          name="fastFedCodeId"
          value={model.fastFedCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Field>
        <label>{t('activity.tdmdrug.endmealDatetime')}</label>
        <DateInput
          name="endmealDatetime"
          value={model.endmealDatetime}
          placeholder="ukuk-uk-uk uk:uk"
          options={{ dateFormat: 'ukuk-uk-uk uk:uk', separate: '-' }}
          onChange={onChange}
        />
      </Form.Field>
      {model.subjectTdmdrugId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.tdmdrug.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default TDMDrugForm;
