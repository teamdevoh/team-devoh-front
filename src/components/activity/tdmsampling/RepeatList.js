import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import { useVisitListOfSubjectWithDate } from '../../collection/hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const build = useBuild();
    const [visit] = useVisitListOfSubjectWithDate({
      subjectId
    });
    const [urgentCodes] = useCodes({ codeGroupId: codeGroup.UrgentCode });
    const [macroscopyCodes] = useCodes({
      codeGroupId: codeGroup.MacroscopyCode
    });
    const t = useFormatMessage();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="five">
              {t('activity.visit')}
            </Table.HeaderCell>
            <Table.HeaderCell>No</Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.tdmsampling.sampleId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.tdmsampling.sampleTime')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.tdmsampling.quickResults')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.tdmsampling.macroscopyCodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {items.map(item => {
            const {
              subjectTdmsamplingId,
              activityStatusCodeId,
              sampleId,
              sampleTime,
              urgentCodeId,
              macroscopyCodeId,
              macroscopyOth,
              sampleOrder
            } = item;
            const visitName = build.displayName({
              source: visit.items,
              value: item.subjectVisitId
            });

            const urgent = build.displayName({
              source: urgentCodes,
              value: urgentCodeId
            });
            const macroscopy = build.displayName({
              source: macroscopyCodes,
              value: macroscopyCodeId,
              other: code.MacroscopyOther,
              otherValue: macroscopyOth
            });

            return (
              <StyledTableRow
                key={subjectTdmsamplingId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={subjectTdmsamplingId}
                    text={visitName}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell>{sampleOrder}</Table.Cell>
                <Table.Cell>{sampleId}</Table.Cell>
                <Table.Cell title={sampleTime}>{sampleTime}</Table.Cell>
                <Table.Cell title={urgent}>{urgent}</Table.Cell>
                <Table.Cell title={macroscopy}>{macroscopy}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={subjectTdmsamplingId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectTdmsampling',
                      prefixLocale: 'activity.tdmsampling',
                      skip: [
                        'subjectTdmsamplingId',
                        'sampleOrder',
                        'samplingSeq',
                        'studyWsActivityId',
                        'projectSampleId'
                      ]
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={6}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
