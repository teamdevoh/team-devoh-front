import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import {
  MyVisitByActivityGroupId,
  Macroscopy,
  Urgent,
  SelectReason
} from '../Selector';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import DateInput from '../../input/DateInput';
import { code, format } from '../../../constants';

const TdmsamplingForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  activityGroupId
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisitByActivityGroupId
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
          activityGroupId={activityGroupId}
          visitTypeCode={code.TDMVisit}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Field widths="equal">
        <label>{t('activity.tdmsampling.sampleId')}</label>
        <Input
          name="sampleId"
          value={model.sampleId || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field required widths="equal">
        <label>{t('activity.tdmsampling.sampleTime')} (yyyy-MM-dd hh:mm)</label>
        <DateInput
          name="sampleTime"
          value={model.sampleTime}
          placeholder={format.YYYY_MM_DD_HHMM}
          options={{ dateFormat: format.YYYY_MM_DD_HHMM, separate: '-' }}
          onChange={onChange}
        />
        {validator.message(
          t('activity.tdmsampling.sampleTime'),
          model.sampleTime,
          'required'
        )}
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.tdmsampling.urgentCodeId')}</label>
        <Urgent
          name="urgentCodeId"
          value={model.urgentCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.tdmsampling.macroscopyCodeId')}</label>
        <Macroscopy
          name="macroscopyCodeId"
          value={model.macroscopyCodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.macroscopyCodeId === code.MacroscopyOther && (
        <Form.Field required>
          <label>{t('activity.tdmsampling.macroscopyOth')}</label>
          <Input
            name="macroscopyOth"
            value={model.macroscopyOth || ''}
            onChange={onChange}
          />
          {validator.message(
            t('activity.tdmsampling.macroscopyOth'),
            model.macroscopyOth,
            'required'
          )}
        </Form.Field>
      )}
      {model.subjectTdmsamplingId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.tdmsampling.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default TdmsamplingForm;
