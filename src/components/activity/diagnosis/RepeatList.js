import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [tbSusceptCodes] = useCodes({
      codeGroupId: codeGroup.TBSusceptCode
    });
    const [tbDiagnosisCodes] = useCodes({
      codeGroupId: codeGroup.TBDiagnosisCode
    });
    const [tbTreatmentCodes] = useCodes({
      codeGroupId: codeGroup.TBTreatmentCode
    });

    const orderedItems = orderBy(
      items,
      ['dateDiagnosis', 'tbStart'],
      ['asc', 'asc']
    );

    const handleClick = id => () => {
      let i = 0;
      for (i = 0; i < items.length; i++) {
        const { subjectTbDiagnosisId } = items[i];

        if (id === subjectTbDiagnosisId) {
          continue;
        }

        if (id > subjectTbDiagnosisId) {
          break;
        }
      }

      if (i === items.length) {
        onClick({ id, isFirstData: true });
      } else {
        // subjectTbDiagnosisId 제일 작은 값 아님
        onClick({ id, isFirstData: false });
      }
    };

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              {t('activity.diagnosis.dateDiagnosis')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.diagnosis.tbDiagnosiscodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.diagnosis.tbSusceptcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.diagnosis.tbStart')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.diagnosis.tbTreatment')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.diagnosis.prevDiag')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.diagnosis.retrtDtc')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const {
              dateDiagnosis,
              drugSuscepOth,
              prevDiag,
              retrtDtc,
              subjectTbDiagnosisId,
              tbDiagnosiscodeId,
              tbOtherSite,
              tbStart,
              tbSusceptcodeId,
              tbTreatmentcodeId
            } = item;

            const tbSuscept = build.displayName({
              source: tbSusceptCodes,
              value: tbSusceptcodeId,
              other: drugSuscepOth,
              otherValue: code.TBSusceptOther
            });

            const tbDiagnosis = build.displayName({
              source: tbDiagnosisCodes,
              value: tbDiagnosiscodeId,
              other: tbOtherSite,
              otherValue: code.TBDiagnosisOther
            });
            const tbTreatment = build.displayName({
              source: tbTreatmentCodes,
              value: tbTreatmentcodeId
            });

            return (
              <StyledTableRow
                key={subjectTbDiagnosisId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={subjectTbDiagnosisId}
                    text={!!dateDiagnosis ? dateDiagnosis : 'unknown'}
                    onClick={handleClick(subjectTbDiagnosisId)}
                  />
                </Table.Cell>
                <Table.Cell title={tbDiagnosis}>{tbDiagnosis}</Table.Cell>
                <Table.Cell title={tbSuscept}>{tbSuscept}</Table.Cell>
                <Table.Cell title={tbStart}>{tbStart}</Table.Cell>
                <Table.Cell title={tbTreatment}>{tbTreatment}</Table.Cell>
                <Table.Cell title={prevDiag}>{prevDiag}</Table.Cell>
                <Table.Cell title={retrtDtc}>{retrtDtc}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={subjectTbDiagnosisId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectTbDiagnosis',
                      prefixLocale: 'activity.diagnosis',
                      skip: ['subjectTbDiagnosisId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={8}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
