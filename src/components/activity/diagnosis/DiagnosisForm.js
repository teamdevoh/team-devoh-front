import React, { Fragment } from 'react';
import { Form, Input, Icon, Popup } from 'semantic-ui-react';
import {
  MyVisit,
  TBSuscept,
  TBDiagnosis,
  TBTreatment,
  RetrtDrug,
  SelectReason
} from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';
import DateInput from '../../input/DateInput';
import SusceptPopup from './SusceptPopup';
import { code, format } from '../../../constants';

const DiagnosisForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  showMoreForm
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.diagnosis.tbDiagnosiscodeId')}</label>
        <TBDiagnosis
          name="tbDiagnosiscodeId"
          defaultOption={{ key: 0, text: '-', value: 0 }}
          value={model.tbDiagnosiscodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.tbDiagnosiscodeId === code.TBDiagnosisOther && (
        <Form.Field required>
          <label>{t('activity.diagnosis.tbOtherSite')}</label>
          <Input
            name="tbOtherSite"
            value={model.tbOtherSite}
            onChange={onChange}
          />
          {validator.message(
            t('activity.diagnosis.tbOtherSite'),
            model.tbOtherSite,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group grouped>
        <label>
          {t('activity.diagnosis.tbSusceptcodeId')}
          <SusceptPopup />
        </label>
        <TBSuscept
          name="tbSusceptcodeId"
          defaultOption={{ key: 0, text: '-', value: 0 }}
          value={model.tbSusceptcodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.tbSusceptcodeId === code.TBSusceptOther && (
        <Form.Field required>
          <label>{t('activity.diagnosis.drugSuscepOth')}</label>
          <Input
            name="drugSuscepOth"
            value={model.drugSuscepOth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.diagnosis.drugSuscepOth'),
            model.drugSuscepOth,
            'required'
          )}
        </Form.Field>
      )}

      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.diagnosis.dateDiagnosis')}</label>
          <DateInput
            name="dateDiagnosis"
            value={model.dateDiagnosis}
            placeholder={format.YYYY_MM_UK}
            options={{ dateFormat: format.YYYY_MM_UK, separate: '-' }}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.diagnosis.tbStart')}</label>
          <DateInput
            name="tbStart"
            value={model.tbStart}
            placeholder={format.YYYY_UK_UK}
            options={{ dateFormat: format.YYYY_UK_UK, separate: '-' }}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {showMoreForm && (
        <Fragment>
          <Form.Group inline>
            <label>
              {t('activity.diagnosis.tbTreatmentcodeId')}
              <Popup
                wide="very"
                inverted
                trigger={<Icon name="question circle" />}
              >
                <Popup.Header>결핵 치료</Popup.Header>
                <Popup.Content>
                  <div>
                    1. 초치료 Initial treatment
                    <br />
                    이전에 결핵치료를 받은 적이 없거나, 1개월 미만의 결핵치료를
                    받은 환자.
                    <br />
                    Subjets who have never received TB treatment before, or who
                    have received TB treatment before for less than 1 month.
                    <br />
                    <br />
                    2. 재치료 Re-treatment
                    <br />
                    이전에 결핵 치료에 실패하였거나, 완치 후 재발되어 다시
                    치료하는 경우, 최소 2개월간 치료를 중단하였다가 다시
                    치료하는 경우.
                    <br />
                    If subjects failed to respond to their previous treatment,
                    suffer relapse after completion of previous treatment, or
                    are re-treated after stopping previous treatment for at
                    least 2 months.
                  </div>
                </Popup.Content>
              </Popup>
            </label>
            <TBTreatment
              name="tbTreatmentcodeId"
              value={model.tbTreatmentcodeId}
              onChange={onChange}
            />
          </Form.Group>
          {code.ReTreatment === model.tbTreatmentcodeId && (
            <Fragment>
              <Form.Group widths="equal">
                <Form.Field required>
                  <label>{t('activity.diagnosis.prevDiag')}</label>
                  <Input
                    name="prevDiag"
                    value={model.prevDiag}
                    onChange={onChange}
                  />
                  {validator.message(
                    t('activity.diagnosis.prevDiag'),
                    model.prevDiag,
                    'required'
                  )}
                </Form.Field>
                <Form.Field required>
                  <label>{t('activity.diagnosis.retrtDtc')}</label>
                  <DateInput
                    name="retrtDtc"
                    value={model.retrtDtc}
                    placeholder="ukuk-uk-uk"
                    options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
                    onChange={onChange}
                  />
                  {validator.message(
                    t('activity.diagnosis.retrtDtc'),
                    model.retrtDtc,
                    'required'
                  )}
                </Form.Field>
              </Form.Group>
              <Form.Group inline>
                <div className="field required">
                  <label>{t('activity.diagnosis.retrtDrugcodeId')}</label>
                </div>
                <RetrtDrug
                  name="retrtDrugcodeId"
                  value={model.retrtDrugcodeId}
                  onChange={onChange}
                />
                {validator.message(
                  t('activity.diagnosis.retrtDrugcodeId'),
                  model.retrtDrugcodeId,
                  'required'
                )}
              </Form.Group>
              {code.ReTrtDrugOther === model.retrtDrugcodeId && (
                <Form.Field required>
                  <label>{t('activity.diagnosis.retrtDrug2')}</label>
                  <Input
                    name="retrtDrug2"
                    value={model.retrtDrug2}
                    onChange={onChange}
                  />
                  {validator.message(
                    t('activity.diagnosis.retrtDrug2'),
                    model.retrtDrug2,
                    'required'
                  )}
                </Form.Field>
              )}
            </Fragment>
          )}
        </Fragment>
      )}
      {model.subjectTbDiagnosisId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.diagnosis.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default DiagnosisForm;
