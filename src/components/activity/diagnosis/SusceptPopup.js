import React from 'react';
import { Popup, Icon } from 'semantic-ui-react';

const SusceptPopup = () => {
  return (
    <Popup
      wide="very"
      inverted
      trigger={<Icon name="question circle" className="link" />}
    >
      <Popup.Header>TB drug resistance types</Popup.Header>
      <Popup.Content>
        <div>
          1) Mono-resistance tuberculosis : resistance to one first-line anti-TB
          drug only <br />
          2) Poly-resistance tuberculosis : resistance to more than one
          first-line anti-TB drug, other than both isoniazid and rifampicin
          <br />
          3) MDR-TB : resistance to at least both isoniazid and rifampicin{' '}
          <br />
          4) XDR-TB : resistance to any fluoroquinolone, and at least one of
          three second-line injectable drugs (capreomycin, kanamycin and
          amikacin), in addition to multidrug resistance
        </div>
      </Popup.Content>
    </Popup>
  );
};

export default SusceptPopup;
