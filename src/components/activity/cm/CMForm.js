import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { Dosfrq, Dosfrm, Route, SelectReason } from '../Selector';
import { FormDoneOrTempBar, WHOATC } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DecimalInput, DateInput } from '../../input';
import { code } from '../../../constants';

const CMForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.cm.cmtrt')}</label>
        <Input name="cmtrt" value={model.cmtrt} onChange={onChange} />
        {validator.message(t('activity.cm.cmtrt'), model.cmtrt, 'required')}
      </Form.Field>
      <Form.Field>
        <WHOATC name="cmdecod" value={model.cmdecod} onChange={onChange} />
      </Form.Field>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.cm.codrname')}</label>
          <Input
            name="codrname"
            value={model.codrname || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.cm.cmdose')}</label>
          <Input name="cmdose" value={model.cmdose || ''} onChange={onChange} />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.cm.cmdosu')}</label>
          <Input name="cmdosu" value={model.cmdosu || ''} onChange={onChange} />
        </Form.Field>
      </Form.Group>
      <Form.Field>
        <label>{t('activity.cm.cmdosfrqcodeId')}</label>
        <Dosfrq
          name="cmdosfrqcodeId"
          value={model.cmdosfrqcodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.cmdosfrqcodeId === code.DosfrqOther && (
        <Form.Field required>
          <label>{t('activity.cm.cmdosfrqoth')}</label>
          <Input
            name="cmdosfrqoth"
            value={model.cmdosfrqoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.cm.cmdosfrqoth'),
            model.cmdosfrqoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Field>
        <label>{t('activity.cm.cmroutecodeId')}</label>
        <Route
          name="cmroutecodeId"
          value={model.cmroutecodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.cmroutecodeId === code.RouteOther && (
        <Form.Field required>
          <label>{t('activity.cm.cmrouteoth')}</label>
          <Input
            name="cmrouteoth"
            value={model.cmrouteoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.cm.cmrouteoth'),
            model.cmrouteoth,
            'required'
          )}
        </Form.Field>
      )}
      {model.cmroutecodeId !== code.RouteOral && (
        <Form.Field>
          <label>{t('activity.cm.cmrouthinf')}</label>
          <DecimalInput
            name="cmrouthinf"
            options={{ suffix: ' hr' }}
            value={model.cmrouthinf}
            onChange={onChange}
          />
        </Form.Field>
      )}
      <Form.Field>
        <label>{t('activity.cm.cmdosfrmcodeId')}</label>
        <Dosfrm
          name="cmdosfrmcodeId"
          value={model.cmdosfrmcodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.cmdosfrmcodeId === code.DosfrmOther && (
        <Form.Field required>
          <label>{t('activity.cm.cmdosfrmoth')}</label>
          <Input
            name="cmdosfrmoth"
            value={model.cmdosfrmoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.cm.cmdosfrmoth'),
            model.cmdosfrmoth,
            'required'
          )}
        </Form.Field>
      )}

      <Form.Group widths="equal">
        <Form.Field>
          <label>
            {t('activity.cm.cmstdtc')}
            {t('activity.subtbdrug.dtc.des')}
          </label>
          <DateInput
            name="cmstdtc"
            value={model.cmstdtc}
            placeholder="ukuk-uk-uk"
            options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>
            {t('activity.cm.cmendtc')}
            {t('activity.subtbdrug.dtc.des')}
          </label>
          <DateInput
            name="cmendtc"
            value={model.cmendtc}
            placeholder="ukuk-uk-uk"
            options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {model.subjectCmdrugId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.cm.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default CMForm;
