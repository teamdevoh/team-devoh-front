import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, codeGroup } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [dosfrqCodes] = useCodes({ codeGroupId: codeGroup.DosfrqCode });
    const [dosfrmCodes] = useCodes({ codeGroupId: codeGroup.DosfrmCode });
    const [routeCodes] = useCodes({ codeGroupId: codeGroup.RouteCode });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['cmstdtc', 'cmstdtc'], ['asc', 'asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>{t('activity.cm.cmtrt')}</Table.HeaderCell>
            <Table.HeaderCell>Dose</Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.cm.cmdosfrqcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.cm.cmdosfrmcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.cm.cmroutecodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('activity.cm.cmstdtc')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.cm.cmendtc')}</Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const frq = build.displayName({
              source: dosfrqCodes,
              value: item.cmdosfrqcodeId,
              other: item.cmdosfrqoth,
              otherValue: code.DosfrqOther
            });

            const frm = build.displayName({
              source: dosfrmCodes,
              value: item.cmdosfrmcodeId,
              other: item.cmdosfrmoth,
              otherValue: code.DosfrmOther
            });

            const route = build.displayName({
              source: routeCodes,
              value: item.cmroutecodeId,
              other: item.cmrouteoth,
              otherValue: code.RouteOther
            });

            return (
              <StyledTableRow
                key={item.subjectCmdrugId}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectCmdrugId}
                    text={item.cmtrt}
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell>{item.cmdose}</Table.Cell>
                <Table.Cell title={frq}>{frq}</Table.Cell>
                <Table.Cell title={frm}>{frm}</Table.Cell>
                <Table.Cell title={route}>{route}</Table.Cell>
                <Table.Cell>{item.cmstdtc}</Table.Cell>
                <Table.Cell>{item.cmendtc}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectCmdrugId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectCmdrug',
                      prefixLocale: 'activity.cm',
                      skip: ['subjectCmdrugId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={8}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
