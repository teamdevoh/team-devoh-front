import React, { memo, useState, useEffect } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';
import { useVisitListOfSubjectWithDate } from '../../collection/hooks';
import { useTbdrugList } from '../../collection-tbdrug/hooks';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [list, setList] = useState([]);

    const [visit] = useVisitListOfSubjectWithDate({
      subjectId,
      showShortDate: true
    });
    const [drug] = useTbdrugList();

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    useEffect(
      () => {
        const newItems = items.map(item => {
          const newItem = {
            ...item,
            drugName: build.displayName({
              source: drug.items,
              value: item.exdrugId,
              other: item.exdrugoth,
              otherValue: code.TBDrugOther
            })
          };
          return newItem;
        });

        const orderedItems = orderBy(newItems, ['drugName'], ['asc']);

        setList([...orderedItems]);
      },
      [items, code.TBDrugOther, drug.items]
    );

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              {t('activity.pkparam.subjectVisitId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.pkparam.exdrugId')}
            </Table.HeaderCell>
            <Table.HeaderCell>{t('activity.pkparam.ppAuc')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.pkparam.aucMic')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.pkparam.ppCmax')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.pkparam.cmaxMic')}</Table.HeaderCell>
            <Table.HeaderCell>{t('activity.pkparam.ppCom')}</Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {list.map(item => {
            const {
              activityStatusCodeId,
              ppAuc,
              aucMic,
              ppCmax,
              cmaxMic,
              ppCom,
              subjectPkparamId,
              subjectTdmdrugId,
              subjectVisitId,
              drugName
            } = item;

            const visitName = build.displayName({
              source: visit.items,
              value: subjectVisitId
            });

            return (
              <StyledTableRow
                key={`${subjectPkparamId}-${subjectTdmdrugId}`}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={subjectPkparamId}
                    text={visitName}
                    onClick={() => {
                      onClick(item);
                    }}
                  />
                </Table.Cell>
                <Table.Cell title={drugName}>{drugName}</Table.Cell>
                <Table.Cell title={ppAuc}>{ppAuc}</Table.Cell>
                <Table.Cell title={aucMic}>{aucMic}</Table.Cell>
                <Table.Cell title={ppCmax}>{ppCmax}</Table.Cell>
                <Table.Cell title={cmaxMic}>{cmaxMic}</Table.Cell>
                <Table.Cell title={ppCom}>{ppCom}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={subjectPkparamId}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectPkparam',
                      prefixLocale: 'activity.pkparam',
                      skip: ['subjectPkparamId', 'subjectTdmdrugId']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={8}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
