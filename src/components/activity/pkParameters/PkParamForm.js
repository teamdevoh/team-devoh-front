import React, { useEffect, useState } from 'react';
import { Form, Input, TextArea } from 'semantic-ui-react';
import { MyVisitByActivityGroupId, SelectReason } from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';
import { code } from '../../../constants';
import { TbdrugList } from '../../collection-tbdrug/Selector';
import api from '../api';

const PkParamForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit,
  activityGroupId,
  subjectId
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);
  const [micTest, setMicTest] = useState([]);

  useEffect(
    () => {
      const fetchMicTest = async () => {
        if (model.exdrugId > 0) {
          const res = await api.fetchMicTestOfSubjectMicResult({
            subjectId,
            exdrugId: model.exdrugId
          });

          setMicTest(res.data);
        } else {
          setMicTest([]);
        }
      };

      fetchMicTest();
    },
    [model.exdrugId, subjectId]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>{t('activity.visit')}</label>
        <MyVisitByActivityGroupId
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
          activityGroupId={activityGroupId}
          disabled={!!model.subjectTdmdrugId}
          visitTypeCode={code.TDMVisit}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.pkparam.exdrugId')}</label>
        <TbdrugList
          name="exdrugId"
          value={model.exdrugId}
          onChange={onChange}
          disabled={!!model.subjectTdmdrugId}
          defaultOption={[
            {
              text: t('common.notSelected'),
              value: null
            }
          ]}
        />
      </Form.Field>
      {model.exdrugId === code.TBDrugOther && (
        <Form.Field required>
          <label>{t('activity.pkparam.exdrugoth')}</label>
          <Input
            name="exdrugoth"
            value={model.exdrugoth || ''}
            onChange={onChange}
            disabled={!!model.subjectTdmdrugId}
          />
          {validator.message(
            t('activity.pkparam.exdrugoth'),
            model.exdrugoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Field>
        <label>MIC of tested Mtb isolate</label>
        <Input readOnly value={micTest.join(', ')} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.pkparam.ppAuc')}</label>
        <Input name="ppAuc" value={model.ppAuc || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.pkparam.aucMic')}</label>
        <Input name="aucMic" value={model.aucMic || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.pkparam.ppCmax')}</label>
        <Input name="ppCmax" value={model.ppCmax || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.pkparam.cmaxMic')}</label>
        <Input name="cmaxMic" value={model.cmaxMic || ''} onChange={onChange} />
      </Form.Field>
      <Form.Field>
        <label>{t('activity.pkparam.ppCom')}</label>
        <TextArea name="ppCom" value={model.ppCom || ''} onChange={onChange} />
      </Form.Field>
      {model.subjectPkparamId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.pkparam.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default PkParamForm;
