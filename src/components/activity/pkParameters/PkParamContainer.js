import React, { Fragment, useEffect, useCallback, useState } from 'react';
import { Button, Message } from 'semantic-ui-react';
import { ActionBar, FormMetaBar, FormTitle } from '..';
import { AuthorInfo } from '../../collection';
import { notification } from '../../modal';
import { PkParamForm, RepeatList } from '.';
import { useFormFields, useFormatMessage } from '../../../hooks';
import { usePublishStatus, useMoveToNextStep } from '../../collection/hooks';
import { usePkParam, usePkParamList, useActionStatus } from '../hooks';
import { LeavingGuard } from '../../modal';
import StatusWrapper from '../StatusWrapper';
import useStatus from '../hooks/useStatus';
import useRepeat from '../hooks/useRepeat';
import { code } from '../../../constants';
import { useParams } from 'react-router-dom';

const PkParamContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const { activityGroupId } = useParams();
  const key = 'subjectPkparamId';

  const uniqueKey = useCallback(model => {
    return `${model.subjectTdmdrugId}|${model.subjectPkparamId}`;
  }, []);

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };
  const { submitted, isNext } = useActionStatus(updateRepeat);
  const publish = usePublishStatus({
    projectActivityId
  });
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const { add, update, remove, fetch, isSaving, isFetching } = usePkParam({
    activityKey
  });
  const [selectedId, setSelectedId] = useState('');
  const [repeat, setRepeat] = usePkParamList({
    subjectId,
    projectActivityId
  });

  const t = useFormatMessage();

  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: code.Temp,
    isDone: false,
    subjectPkparamId: '',
    subjectId: subjectId,
    subjectVisitId: null,
    subjectTdmdrugId: null,
    exdrugId: null,
    exdrugoth: null,
    ppAuc: null,
    aucMic: null,
    ppCmax: null,
    cmaxMic: null,
    ppCom: null,
    modified: null,
    author: '',
    isChanged: false,
    reason: ''
  };

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );
  const { updateRepeatByKey } = useRepeat(key);
  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId.length > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  const fetchItem = useCallback(async () => {
    const data = await fetch({
      subjectId,
      projectActivityId,
      [key]: model[key],
      subjectTdmdrugId: model.subjectTdmdrugId
    });
    if (data.activityStatusCodeId === null) {
      data.activityStatusCodeId = code.Temp;
    }

    setModel({
      ...model,
      ...data,
      projectActivityId: projectActivityId,
      isDone: data.activityStatusCodeId === code.Temp ? false : true,
      isChanged: false
    });
  });

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model.activityKeyId = newId;
        model[key] = newId;
        setSelectedId(uniqueKey(model));
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            let updated = [...repeat.items];
            const filtered = updated.filter((item, index, arr) => {
              const isTdmDrug =
                item.subjectTdmdrugId !== null &&
                item.subjectTdmdrugId === model.subjectTdmdrugId;

              if (isTdmDrug) {
                updated[index] = {
                  ...item,
                  ...model
                };
              }

              return isTdmDrug;
            });

            if (filtered.length === 0) {
              updated = repeat.items.concat(model);
            }

            setRepeat(updated);
            const pickedActivity = publish.pickActivityValueBy(updated);
            publish.activityStatus(pickedActivity);

            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(
    repeatItem => {
      setModel({
        ...model,
        [key]: repeatItem[key],
        subjectTdmdrugId: repeatItem.subjectTdmdrugId
      });
      setSelectedId(uniqueKey(repeatItem));
    },
    [model, selectedId]
  );

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        [key]: 0,
        activityStatusCodeId: code.Temp
      });

      setSelectedId('');
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(
    item => {
      return `${uniqueKey(item)}` === `${selectedId}`;
    },
    [selectedId]
  );

  return (
    <Fragment>
      {repeat.isPermit ? (
        <Fragment>
          <Button onClick={handleAddClick}>{t('activity.add')}</Button>
          <LeavingGuard shouldBlock={model.isChanged} />
          <RepeatList
            subjectId={subjectId}
            updateRepeat={updateRepeat}
            projectActivityId={projectActivityId}
            items={repeat.items}
            loading={repeat.loading}
            onClick={handleRepeatItemClick}
            onQueryModalClosed={handleQueryModalClosed}
            onFileModalClosed={handleFileModalClosed}
            onNoteModalClosed={handleNoteModalClosed}
            isSeleted={isSeleted}
          />
          {model[key] !== '' && (
            <Fragment>
              <FormTitle text={t('activity.pkparam.title')} />
              <FormMetaBar>
                <AuthorInfo date={model.modified} name={model.author} />
                <StatusWrapper
                  subjectId={subjectId}
                  onChangeStatusCb={updateRepeat}
                  activityKeyId={model[key]}
                  projectActivityId={projectActivityId}
                  note={{
                    value: status.note.hasNote,
                    closed: handleNoteModalClosed
                  }}
                  query={{
                    value: status.qna.query.queryStatusCodeId,
                    closed: handleQueryModalClosed
                  }}
                  file={{
                    value: status.file.hasFile,
                    closed: handleFileModalClosed
                  }}
                  activity={{
                    value: model.activityStatusCodeId,
                    closed: () => {}
                  }}
                  audit={{
                    entityName: 'SubjectPkparam',
                    prefixLocale: 'activity.pkparam',
                    skip: [key, 'subjectTdmdrugId']
                  }}
                  repeatItems={repeat.items}
                />
              </FormMetaBar>
              <PkParamForm
                projectId={projectId}
                loading={isFetching.value}
                model={model}
                setModel={setModel}
                onChange={onChange}
                onSubmit={handleSubmit}
                activityGroupId={activityGroupId}
                subjectId={subjectId}
              >
                <ActionBar
                  statusCode={model.activityStatusCodeId}
                  isDone={model.isDone}
                  subjectId={subjectId}
                  projectActivityId={projectActivityId}
                  activityKeyId={model[key]}
                  loading={isSaving}
                  onClick={submitted}
                  onRemoveClick={handleRemove}
                  hasRemoveButton={true}
                />
              </PkParamForm>
            </Fragment>
          )}
        </Fragment>
      ) : (
        <Message warning header="Permission Denied" />
      )}
    </Fragment>
  );
};

export default PkParamContainer;
