import React, { memo } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage, useCodes } from '../../../hooks';
import orderBy from 'lodash/orderBy';
import helpers from '../../../helpers';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import StatusWrapper from '../StatusWrapper';
import { code, codeGroup, format } from '../../../constants';
import { EmptyRowsViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const RepeatList = memo(
  ({
    subjectId,
    updateRepeat,
    projectActivityId,
    items,
    loading,
    onClick,
    onQueryModalClosed,
    onNoteModalClosed,
    onFileModalClosed,
    isSeleted
  }) => {
    const t = useFormatMessage();
    const build = useBuild();
    const [specCodes] = useCodes({ codeGroupId: codeGroup.AFBSpecCode });
    const [mycobactCodes] = useCodes({ codeGroupId: codeGroup.MycobactCode });
    const [stainResultCodes] = useCodes({
      codeGroupId: codeGroup.AFBStainResultCode
    });
    const [cultureSolidCodes] = useCodes({
      codeGroupId: codeGroup.AFBCultureSolidCode
    });
    const [cultureLiquidCodes] = useCodes({
      codeGroupId: codeGroup.AFBCultureLiquidCode
    });

    const handleChangeStatus = modelItem => activityStatusCodeId => {
      updateRepeat(activityStatusCodeId, modelItem);
    };

    const orderedItems = orderBy(items, ['afbDtc'], ['asc']);

    return (
      <Table compact="very" fixed singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>{t('activity.afb.afbDtc')}</Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.afb.afbSpeccodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.afb.mycobactcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.afb.afbStainResultcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.afb.afbCultureSolidcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {t('activity.afb.afbCultureLiquidcodeId')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('status')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderedItems.map(item => {
            const spec = build.displayName({
              source: specCodes,
              value: item.afbSpeccodeId,
              other: item.afbSpecoth,
              otherValue: code.AFBSpecOther
            });
            const mycobact = build.displayName({
              source: mycobactCodes,
              value: item.mycobactcodeId,
              other: item.mycobacoth,
              otherValue: code.MycobacOther
            });
            const stainResult = build.displayName({
              source: stainResultCodes,
              value: item.afbStainResultcodeId
            });
            const cultureSolid = build.displayName({
              source: cultureSolidCodes,
              value: item.afbCultureSolidcodeId
            });
            const cultureLiquid = build.displayName({
              source: cultureLiquidCodes,
              value: item.afbCultureLiquidcodeId
            });

            return (
              <StyledTableRow
                key={item.subjectAfbid}
                selected={isSeleted(item)}
              >
                <Table.Cell>
                  <ActivityItemAnchor
                    id={item.subjectAfbid}
                    text={
                      item.afbDtc
                        ? helpers.util.dateformat(
                            item.afbDtc,
                            format.YYYY_MM_DD
                          )
                        : 'unknown'
                    }
                    onClick={onClick}
                  />
                </Table.Cell>
                <Table.Cell title={spec}>{spec}</Table.Cell>
                <Table.Cell title={mycobact}>{mycobact}</Table.Cell>
                <Table.Cell title={stainResult}>{stainResult}</Table.Cell>
                <Table.Cell title={cultureSolid}>{cultureSolid}</Table.Cell>
                <Table.Cell title={cultureLiquid}>{cultureLiquid}</Table.Cell>
                <Table.Cell>
                  <StatusWrapper
                    subjectId={subjectId}
                    onChangeStatusCb={handleChangeStatus(item)}
                    activityKeyId={item.subjectAfbid}
                    projectActivityId={projectActivityId}
                    note={{
                      value: item.hasNote,
                      closed: onNoteModalClosed
                    }}
                    query={{
                      value: item.queryStatusCodeId,
                      closed: onQueryModalClosed
                    }}
                    file={{
                      value: item.hasFile,
                      closed: onFileModalClosed
                    }}
                    activity={{
                      value: item.activityStatusCodeId,
                      closed: () => {}
                    }}
                    audit={{
                      entityName: 'SubjectAfb',
                      prefixLocale: 'activity.afb',
                      skip: ['subjectAfbid']
                    }}
                    repeatItems={items}
                  />
                </Table.Cell>
              </StyledTableRow>
            );
          })}
          {loading && (
            <EmptyRowsViewForSemantic
              loading={true}
              columnLength={7}
              size="large"
            />
          )}
        </Table.Body>
      </Table>
    );
  }
);

export default RepeatList;
