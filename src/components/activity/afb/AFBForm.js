import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import {
  AFBSpec,
  Mycobact,
  AFBCultureLiquid,
  AFBCultureSolid,
  AFBStainResult,
  SelectReason
} from '../Selector';
import { FormDoneOrTempBar } from '../';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { DatePicker } from '../../datepicker';
import { code } from '../../../constants';

const AFBForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required>
        <label>
          {t('activity.afb.afbDtc')}
          {t('activity.afb.afbDtc.des')}
        </label>
        <DatePicker name="afbDtc" value={model.afbDtc} onChange={onChange} />
        {validator.message(t('activity.afb.afbDtc'), model.afbDtc, 'required')}
      </Form.Field>
      <Form.Field>
        <label>{t('activity.afb.afbSpeccodeId')}</label>
        <AFBSpec
          name="afbSpeccodeId"
          value={model.afbSpeccodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.afbSpeccodeId === code.AFBSpecOther && (
        <Form.Field required>
          <label>{t('activity.afb.afbSpecoth')}</label>
          <Input
            name="afbSpecoth"
            value={model.afbSpecoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.afb.afbSpecoth'),
            model.afbSpecoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Field>
        <label>{t('activity.afb.mycobactcodeId')}</label>
        <Mycobact
          name="mycobactcodeId"
          value={model.mycobactcodeId}
          onChange={onChange}
        />
      </Form.Field>
      {model.mycobactcodeId === code.MycobacOther && (
        <Form.Field required>
          <label>{t('activity.afb.mycobacoth')}</label>
          <Input
            name="mycobacoth"
            value={model.mycobacoth || ''}
            onChange={onChange}
          />
          {validator.message(
            t('activity.afb.mycobacoth'),
            model.mycobacoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.afb.afbStainResultcodeId')}</label>
          <AFBStainResult
            name="afbStainResultcodeId"
            value={model.afbStainResultcodeId}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.afb.afbCultureSolidcodeId')}</label>
          <AFBCultureSolid
            name="afbCultureSolidcodeId"
            value={model.afbCultureSolidcodeId}
            defaultOption={{
              text: t('common.notSelected'),
              value: ''
            }}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.afb.afbCultureLiquidcodeId')}</label>
          <AFBCultureLiquid
            name="afbCultureLiquidcodeId"
            value={model.afbCultureLiquidcodeId}
            defaultOption={{
              text: t('common.notSelected'),
              value: ''
            }}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {model.subjectAfbid > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.afb.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default AFBForm;
