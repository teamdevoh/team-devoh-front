import React, { Fragment, useCallback } from 'react';
import { Form, Input, Button, Icon, Dropdown } from 'semantic-ui-react';
import { useSubmit, useFormatMessage, useCodes } from '../../../hooks';
import styled from 'styled-components';
import { SearchReferenceInput } from '../../collection-tbdrug';
import { FormDoneOrTempBar } from '../';
import { SubFieldWrap } from '../styles';
import { codeGroup } from '../../../constants';
import { SelectReason } from '../Selector';

const StyledButton = styled(Button)`
  &&& {
    margin-left: 10px;
    margin-bottom: 5px;
  }
`;

const StyledLabel = styled.label`
  display: inline-block !important;
`;

const AdmeBasicForm = ({
  MAX_FIELD_CNT,
  children,
  projectId,
  loading,
  model,
  onChange,
  onSubmit,
  refInfos,
  setModel,
  addFieldCnt,
  setAddFieldCnt
}) => {
  const t = useFormatMessage();
  const [{ onValidate, validator }] = useSubmit(onSubmit);
  const [proteinBindingTypeCodes] = useCodes({
    codeGroupId: codeGroup.ProteinBindingTypeCode
  });

  const handleAddField = key => () => {
    if (addFieldCnt[key] < MAX_FIELD_CNT) {
      const count = addFieldCnt[key] + 1;
      const modelKey = `${key}${count}`;

      setAddFieldCnt({
        ...addFieldCnt,
        [key]: count
      });

      setModel({
        ...model,
        [modelKey]: '',
        [`${modelKey}ReferenceId`]: ''
      });
    }
  };

  const handleRemoveField = key => () => {
    const orgTotalCount = addFieldCnt[key];
    const count = addFieldCnt[key] - 1;
    let newModel = { ...model };

    setAddFieldCnt({
      ...addFieldCnt,
      [key]: count
    });

    delete newModel[`${key}${orgTotalCount}`];
    delete newModel[`${key}${orgTotalCount}ReferenceId`];

    setModel(newModel);
  };

  const handleSelecte = modelName => title => {
    onChange(null, { name: modelName, value: title });
  };

  const getRefTitle = id => {
    if (typeof refInfos[id] !== 'undefined') {
      return refInfos[id].title;
    }

    return '';
  };

  const generateField = useCallback(
    ({ key, count, label, addBtnTitle }) => {
      const result = [];

      for (let i = 0; i < count; i++) {
        const modelCount = i + 1;
        const modelKey = `${key}${modelCount}`;

        const addBtnDisabled = (() => {
          for (let n = 0; n < addFieldCnt[key]; n++) {
            if (typeof model[`${key}${n + 1}`] !== 'undefined') {
              if (!model[`${key}${n + 1}`]) {
                return true;
              }
            }
          }
          return false;
        })();

        result.push(
          <Fragment key={modelKey}>
            <Form.Field>
              <StyledLabel>
                {label} {count > 1 && i + 1}
              </StyledLabel>
              {i === count - 1 && (
                // 추가된 마지막 필드에 x랑 add 버튼 둠
                <Fragment>
                  {i !== 0 && (
                    <StyledButton icon onClick={handleRemoveField(key)}>
                      <Icon name="x" />
                    </StyledButton>
                  )}
                  {count < MAX_FIELD_CNT && (
                    <StyledButton
                      type="button"
                      disabled={addBtnDisabled}
                      onClick={handleAddField(key)}
                    >
                      {addBtnTitle}
                    </StyledButton>
                  )}
                </Fragment>
              )}
              <Input
                name={modelKey}
                value={model[modelKey] || ''}
                onChange={onChange}
              />
            </Form.Field>
            <Form.Field>
              <SearchReferenceInput
                searchValue={getRefTitle([model[`${modelKey}ReferenceId`]])}
                name={`${modelKey}ReferenceId`}
                value={model[`${modelKey}ReferenceId`]}
                onChange={onChange}
                onSelecteItem={handleSelecte(`${modelKey}Reference`)}
              />
            </Form.Field>
          </Fragment>
        );
      }

      return result;
    },
    [model]
  );

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Group>
        <Form.Field width="13" inline>
          <label>{t('activity.tbdrugAdmeBasic.fBioavailability')}</label>
          <Input
            name="fBioavailability"
            value={model.fBioavailability || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field width="3" inline>
          <label> </label>
          <Input
            name="fBioavailabilityUnit"
            value={model.fBioavailabilityUnit || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.fBioavailabilityReferenceId])}
          name="fBioavailabilityReferenceId"
          value={model.fBioavailabilityReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group>
        <Form.Field width="13" inline>
          <label>{t('activity.tbdrugAdmeBasic.mdck')}</label>
          <Input name="mdck" value={model.mdck || ''} onChange={onChange} />
        </Form.Field>
        <Form.Field width="3" inline>
          <label> </label>
          <Input
            name="mdckUnit"
            value={model.mdckUnit || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.mdckReferenceId])}
          name="mdckReferenceId"
          value={model.mdckReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group>
        <Form.Field width="13" inline>
          <label>{t('activity.tbdrugAdmeBasic.pampa')}</label>
          <Input name="pampa" value={model.pampa || ''} onChange={onChange} />
        </Form.Field>
        <Form.Field width="3" inline>
          <label> </label>
          <Input
            name="pampaUnit"
            value={model.pampaUnit || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.pampaReferenceId])}
          name="pampaReferenceId"
          value={model.pampaReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Group>
        <Form.Field width="13" inline>
          <label>{t('activity.tbdrugAdmeBasic.llcPk1')}</label>
          <Input name="llcPk1" value={model.llcPk1 || ''} onChange={onChange} />
        </Form.Field>
        <Form.Field width="3" inline>
          <label> </label>
          <Input
            name="llcPk1Unit"
            value={model.llcPk1Unit || ''}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.llcPk1ReferenceId])}
          name="llcPk1ReferenceId"
          value={model.llcPk1ReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      {generateField({
        key: 'caco',
        count: addFieldCnt.caco,
        label: 'Caco-2 permeability (cm/s)',
        addBtnTitle: 'Add Caco-2'
      })}
      {generateField({
        key: 'fa',
        count: addFieldCnt.fa,
        label: 'Fa (fraction of the drug available from dosage)',
        addBtnTitle: 'Add Fa'
      })}
      {generateField({
        key: 'fg',
        count: addFieldCnt.fg,
        label: 'Fg(fraction of escaping the first-pass gut wall metabolism)',
        addBtnTitle: 'Add Fg'
      })}
      {generateField({
        key: 'fu',
        count: addFieldCnt.fu,
        label: 'Fu, gut(unbound fraction in the gut)',
        addBtnTitle: 'Add Fu, gut'
      })}
      {generateField({
        key: 'bp',
        count: addFieldCnt.bp,
        label: 'Blood to plasma ratio (B/P)',
        addBtnTitle: 'Add B/P'
      })}
      <Form.Field>
        <label>Subjects and variables</label>
        <Input
          name="subjectVariables"
          value={model.subjectVariables || ''}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.subjectVariablesReferenceId])}
          name="subjectVariablesReferenceId"
          value={model.subjectVariablesReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <label>Protein binding</label>
        <Input name="protein" value={model.protein || ''} onChange={onChange} />
        <SubFieldWrap open>
          <Form.Group style={{ marginTop: '10px' }}>
            <Form.Field width="6" inline>
              <label>Binding Type</label>
              <Dropdown
                name="proteinBindingTypeCodeId"
                search
                fluid
                selection
                value={model.proteinBindingTypeCodeId}
                onChange={onChange}
                options={[
                  {
                    text: t('common.notSelected'),
                    value: ''
                  },
                  ...proteinBindingTypeCodes
                ]}
              />
            </Form.Field>
            <Form.Field width="10" inline>
              <label> </label>
              <Input
                name="proteinBindingTypeOther"
                value={model.proteinBindingTypeOther || ''}
                onChange={onChange}
                placeholder="Other text"
              />
            </Form.Field>
          </Form.Group>
        </SubFieldWrap>
      </Form.Field>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle([model.proteinReferenceId])}
          name="proteinReferenceId"
          value={model.proteinReferenceId}
          onChange={onChange}
        />
      </Form.Field>
      {model.tbdrugAdmeBasicId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="ADME Basic"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default AdmeBasicForm;
