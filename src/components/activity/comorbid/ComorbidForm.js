import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import { /*MyVisit,*/ Comorbidity, SelectReason } from '../Selector';
import { FormDoneOrTempBar } from '../';
import { DateInput } from '../../input';
import { useFormatMessage, useSubmit } from '../../../hooks';
import { code } from '../../../constants';

const ComorbidForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      {/* <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field> */}
      <Form.Group grouped>
        <Form.Field required>
          <label>{t('activity.comorbid.comorbidcodeId')}</label>
          <Comorbidity
            name="comorbidcodeId"
            value={model.comorbidcodeId}
            onChange={onChange}
          />
          {validator.message(
            t('activity.comorbid.comorbidcodeId'),
            model.comorbidcodeId,
            'required'
          )}
        </Form.Field>{' '}
      </Form.Group>
      {model.comorbidcodeId === code.ComorbidityOther && (
        <Form.Field required>
          <label>{t('activity.comorbid.comorbidoth')}</label>
          <Input
            name="comorbidoth"
            value={model.comorbidoth}
            onChange={onChange}
          />
          {validator.message(
            t('activity.comorbid.comorbidoth'),
            model.comorbidoth,
            'required'
          )}
        </Form.Field>
      )}
      <Form.Group widths="equal">
        <Form.Field required>
          <label>
            {t('activity.comorbid.comorbidDate')} (
            {t('activity.comorbid.date.placeholder')})
          </label>
          <DateInput
            name="comorbidDate"
            value={model.comorbidDate}
            placeholder="ukuk-uk-uk"
            options={{ dateFormat: 'ukuk-uk-uk', separate: '-' }}
            onChange={onChange}
          />
          {validator.message(
            t('activity.comorbid.comorbidDate'),
            model.comorbidDate,
            'required'
          )}
        </Form.Field>
      </Form.Group>
      {model.subjectComorbidId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.comorbid.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default ComorbidForm;
