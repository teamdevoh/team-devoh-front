import React, { useCallback } from 'react';
import { Table, Responsive } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import { ActivityItemAnchor } from '../';
import { useBuild } from '../hooks';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import StatusWrapper from '../StatusWrapper';
import { EmptyRowsStickyViewForSemantic } from '../../modules';
import { StyledTableRow } from '../styles';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;
const RepeatList = ({
  subjectId,
  updateRepeat,
  projectActivityId,
  items,
  loading,
  onClick,
  onQueryModalClosed,
  onNoteModalClosed,
  onFileModalClosed,
  refInfos,
  MAX_FIELD_CNT,
  isSeleted
}) => {
  const t = useFormatMessage();
  const build = useBuild();
  const [drugs] = useTbdrugList();

  const handleChangeStatus = modelItem => activityStatusCodeId => {
    updateRepeat(activityStatusCodeId, modelItem);
  };

  const pdCommonDisplayName = useCallback(
    ({ mean, sd, median, min, max, unit, other }) => {
      const resultArr = [];
      let result = '';

      if (!!mean) {
        result = `Mean: ${mean}`;
        resultArr.push(result);
      }
      if (!!sd) {
        result = `SD: ${sd}`;
        resultArr.push(result);
      }
      if (!!median) {
        result = `Median: ${median}`;
        resultArr.push(result);
      }
      if (!!min || max) {
        result = `Min ~ Max: ${min || ''} ~ ${max || ''}`;
        resultArr.push(result);
      }
      if (resultArr.length > 0) {
        resultArr[resultArr.length - 1] += unit ? ` ${unit}` : '';
      }
      if (!!other) {
        result = `Other: ${other}`;
        resultArr.push(result);
      }

      return resultArr;
    },
    []
  );

  return (
    <div style={{ overflow: 'auto', margin: '1em 0', display: 'flex' }}>
      <div
        style={
          isMobile
            ? {}
            : {
                flexShrink: 0,
                width: '300%'
              }
        }
      >
        <Table compact="very" fixed={!isMobile} singleLine={!isMobile}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Reference</Table.HeaderCell>
              <Table.HeaderCell>Subjects</Table.HeaderCell>
              <Table.HeaderCell>Experimental condition</Table.HeaderCell>
              <Table.HeaderCell>Study Brief</Table.HeaderCell>
              <Table.HeaderCell>Gender</Table.HeaderCell>
              <Table.HeaderCell>Age</Table.HeaderCell>
              <Table.HeaderCell>Status(disease/healthy)</Table.HeaderCell>
              <Table.HeaderCell>Dose and regimen in detail</Table.HeaderCell>
              <Table.HeaderCell>Fed Status</Table.HeaderCell>
              <Table.HeaderCell>Concomitenty used drugs</Table.HeaderCell>
              <Table.HeaderCell>Targated bacteria or pathogen</Table.HeaderCell>
              <Table.HeaderCell>CFU,M.tb</Table.HeaderCell>
              <Table.HeaderCell>CD4 counts</Table.HeaderCell>
              <Table.HeaderCell>MIC</Table.HeaderCell>
              <Table.HeaderCell>MBC</Table.HeaderCell>
              <Table.HeaderCell>MTC</Table.HeaderCell>
              <Table.HeaderCell>AUC/MIC</Table.HeaderCell>
              <Table.HeaderCell>Toxicity</Table.HeaderCell>
              <Table.HeaderCell>Final outcomes</Table.HeaderCell>
              <Table.HeaderCell width="one">
                Comments/discussion
              </Table.HeaderCell>
              <Table.HeaderCell>{t('status')}</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {items.map(item => {
              const {
                tbdrugPddataId,
                referenceId,

                subjectIsHuman,
                subjectHuman,
                subjectIsInvitro,
                subjectInvitro,
                subjectIsInvivo,
                subjectInvivo,
                subjectIsMurine,
                subjectMurine,
                subjectOther,

                subjectHumanSampleSize,
                subjectInvitroSampleSize,
                subjectInvivoSampleSize,
                subjectMurineSampleSize,

                experimentalCondition,

                studyBrief,

                genderIsMale,
                genderMale,
                genderIsFemale,
                genderFemale,
                genderUnit,
                genderOther,

                ageMean,
                ageSd,
                ageMedian,
                ageMin,
                ageMax,
                ageUnit,
                ageOther,

                statusIsTb,
                statusTbDisease,
                statusIsHealthy,
                statusHealthyDisease,
                statusOther,

                doseAndRegimenDrugMeaning,
                doseAndRegimenDrugDose,
                doseAndRegimenDrugUnit,
                doseAndRegimenDrugEtc,

                fedStatus,
                fedStatusOther,

                targatedBacteria,

                cfuMean,
                cfuSd,
                cfuMedian,
                cfuMin,
                cfuMax,
                cfuUnit,
                cfuOther,
                cfuPkconc,
                cfuSampleBrief,
                cfuAssayType,

                cd4CountMean,
                cd4CountSd,
                cd4CountMedian,
                cd4CountMin,
                cd4CountMax,
                cd4CountUnit,
                cd4CountOther,
                cd4SampleBrief,
                cd4AssayType,

                micMean,
                micSd,
                micMedian,
                micMin,
                micMax,
                micUnit,
                micOther,
                micSampleBrief,
                micAssayType,

                mbcMean,
                mbcSd,
                mbcMedian,
                mbcMin,
                mbcMax,
                mbcUnit,
                mbcOther,
                mbcSampleBrief,
                mbcAssayType,
                mtcMean,
                mtcSd,
                mtcMedian,
                mtcMin,
                mtcMax,
                mtcUnit,
                mtcOther,
                mtcSampleBrief,
                mtcAssayType,
                aucMicMean,
                aucMicSd,
                aucMicMedian,
                aucMicMin,
                aucMicMax,
                aucMicUnit,
                aucMicOther,
                aucMicSampleBrief,
                aucMicAssayType,
                toxicityMean,
                toxicitySd,
                toxicityMedian,
                toxicityMin,
                toxicityMax,
                toxicityUnit,
                toxicityOther,
                toxicitySampleBrief,
                toxicityMethodOfEvaluation,
                finalOutcomesMean,
                finalOutcomesSd,
                finalOutcomesMedian,
                finalOutcomesMin,
                finalOutcomesMax,
                finalOutcomesUnit,
                finalOutcomesOther,
                finalOutcomesSampleBrief,
                finalOutcomesMethodOfEvaluation,
                comments,
                activityStatusCodeId
              } = item;

              const referenceTitle = !!refInfos[referenceId]
                ? refInfos[referenceId].title
                : 'unknown';
              const subject = [];
              const gender = [];
              let age = [];
              const status = [];
              const dose = [];
              const fed = [];
              const concomitenty = [];
              let cfu = [];
              let cd4 = [];
              let mic = [];
              let mbc = [];
              let mtc = [];
              let aucMic = [];
              let toxicity = [];
              let finalOutcome = [];

              let result = '';
              // subject
              if (subjectIsHuman) {
                result = 'Human';
                if (!!subjectHumanSampleSize) {
                  result += ` Sample size: ${subjectHumanSampleSize}`;
                }
                if (!!subjectHuman) {
                  result += ` (Description: ${subjectHuman})`;
                }
                subject.push(result);
              }
              if (subjectIsInvitro) {
                result = 'In vitro';
                if (!!subjectInvitroSampleSize) {
                  result += ` Sample size: ${subjectInvitroSampleSize}`;
                }
                if (!!subjectInvitro) {
                  result += ` (Description: ${subjectInvitro})`;
                }
                subject.push(result);
              }
              if (subjectIsInvivo) {
                result = 'In vivo';
                if (!!subjectInvivoSampleSize) {
                  result += ` Sample size: ${subjectInvivoSampleSize}`;
                }
                if (!!subjectInvivo) {
                  result += ` (Description: ${subjectInvivo})`;
                }
                subject.push(result);
              }
              if (subjectIsMurine) {
                result = 'Murine model of M.tb';
                if (!!subjectMurineSampleSize) {
                  result += ` Sample size: ${subjectMurineSampleSize}`;
                }
                if (!!subjectMurine) {
                  result += ` (Description: ${subjectMurine})`;
                }
                subject.push(result);
              }
              if (!!subjectOther) {
                result = `Other: ${subjectOther}`;
                subject.push(result);
              }

              // gender
              if (genderIsMale) {
                result = 'Male';
                if (!!genderMale) {
                  result += `: ${genderMale}`;
                }
                gender.push(result);
              }
              if (genderIsFemale) {
                result = 'Female';
                if (!!genderFemale) {
                  result += `: ${genderFemale}`;
                }
                gender.push(result);
              }
              if (gender.length > 0) {
                gender[gender.length - 1] += ` ${genderUnit || 'Count'}`;
              }
              if (!!genderOther) {
                result = `Other: ${genderOther}`;
                gender.push(result);
              }

              // age
              age = pdCommonDisplayName({
                mean: ageMean,
                sd: ageSd,
                median: ageMedian,
                min: ageMin,
                max: ageMax,
                unit: ageUnit || 'Year',
                other: ageOther
              });

              // status
              if (statusIsTb) {
                result = 'TB';
                if (!!statusTbDisease) {
                  result += `: ${statusTbDisease}`;
                }
                status.push(result);
              }
              if (statusIsHealthy) {
                result = 'Healthy';
                if (!!statusHealthyDisease) {
                  result += `: ${statusHealthyDisease}`;
                }
                status.push(result);
              }
              if (!!statusOther) {
                result = `Other: ${statusOther}`;
                status.push(result);
              }

              // dose
              if (!!doseAndRegimenDrugMeaning) {
                result = doseAndRegimenDrugMeaning;
                dose.push(result);
              }
              if (!!doseAndRegimenDrugDose) {
                if (dose.length > 0) {
                  dose[
                    dose.length - 1
                  ] += `: ${doseAndRegimenDrugDose} ${doseAndRegimenDrugUnit ||
                    'mg'}`;
                } else {
                  result = `${doseAndRegimenDrugDose} ${doseAndRegimenDrugUnit ||
                    'mg'}`;
                  dose.push(result);
                }
              }
              if (!!doseAndRegimenDrugEtc) {
                result = `Other: ${doseAndRegimenDrugEtc}`;
                dose.push(result);
              }

              // fed
              if (!!fedStatus) {
                result = fedStatus;
                fed.push(result);
              }
              if (!!fedStatusOther) {
                if (fed.length > 0) {
                  fed[fed.length - 1] += `: ${fedStatusOther}`;
                } else {
                  result = fedStatusOther;
                  fed.push(result);
                }
              }

              // concomitenty
              for (let i = 0; i < MAX_FIELD_CNT; i++) {
                const curKey = `concomitentDrug${i + 1}`;
                result = '';

                if (!!item[`${curKey}TbdrugId`]) {
                  result = build.displayName({
                    source: drugs.items,
                    value: Number(item[`${curKey}TbdrugId`])
                  });
                }
                if (!!item[`${curKey}Other`]) {
                  result += result.length > 0 ? ': ' : '';
                  result += item[`${curKey}Other`];
                }
                if (!!item[`${curKey}Dose`]) {
                  result += ` ${item[`${curKey}Dose`]}`;
                  result += ` ${item[`${curKey}Unit`] || 'mg'}`;
                }
                if (!!item[`${curKey}Etc`]) {
                  result += `(${item[`${curKey}Etc`]})`;
                }
                if (result.length > 0) {
                  concomitenty.push(result);
                }
              }
              // cfu
              cfu = pdCommonDisplayName({
                mean: cfuMean,
                sd: cfuSd,
                median: cfuMedian,
                min: cfuMin,
                max: cfuMax,
                unit: cfuUnit,
                other: cfuOther
              });
              if (!!cfuPkconc) {
                result = `PK Conc: ${cfuPkconc}`;
                cfu.push(result);
              }
              if (!!cfuSampleBrief) {
                result = `Sample Brief: ${cfuSampleBrief}`;
                cfu.push(result);
              }
              if (!!cfuAssayType) {
                result = `Assay Type: ${cfuAssayType}`;
                cfu.push(result);
              }

              // cd4
              cd4 = pdCommonDisplayName({
                mean: cd4CountMean,
                sd: cd4CountSd,
                median: cd4CountMedian,
                min: cd4CountMin,
                max: cd4CountMax,
                unit: cd4CountUnit,
                other: cd4CountOther
              });
              if (!!cd4SampleBrief) {
                result = `Sample Brief: ${cd4SampleBrief}`;
                cd4.push(result);
              }
              if (!!cd4AssayType) {
                result = `Assay Type: ${cd4AssayType}`;
                cd4.push(result);
              }

              // mic
              mic = pdCommonDisplayName({
                mean: micMean,
                sd: micSd,
                median: micMedian,
                min: micMin,
                max: micMax,
                unit: micUnit,
                other: micOther
              });
              if (!!micSampleBrief) {
                result = `Sample Brief: ${micSampleBrief}`;
                mic.push(result);
              }
              if (!!micAssayType) {
                result = `Assay Type: ${micAssayType}`;
                mic.push(result);
              }

              // mbc
              mbc = pdCommonDisplayName({
                mean: mbcMean,
                sd: mbcSd,
                median: mbcMedian,
                min: mbcMin,
                max: mbcMax,
                unit: mbcUnit,
                other: mbcOther
              });
              if (!!mbcSampleBrief) {
                result = `Sample Brief: ${mbcSampleBrief}`;
                mbc.push(result);
              }
              if (!!mbcAssayType) {
                result = `Assay Type: ${mbcAssayType}`;
                mbc.push(result);
              }

              // mtc
              mtc = pdCommonDisplayName({
                mean: mtcMean,
                sd: mtcSd,
                median: mtcMedian,
                min: mtcMin,
                max: mtcMax,
                unit: mtcUnit,
                other: mtcOther
              });
              if (!!mtcSampleBrief) {
                result = `Sample Brief: ${mtcSampleBrief}`;
                mtc.push(result);
              }
              if (!!mtcAssayType) {
                result = `Assay Type: ${mtcAssayType}`;
                mtc.push(result);
              }

              // aucMic
              aucMic = pdCommonDisplayName({
                mean: aucMicMean,
                sd: aucMicSd,
                median: aucMicMedian,
                min: aucMicMin,
                max: aucMicMax,
                unit: aucMicUnit,
                other: aucMicOther
              });
              if (!!aucMicSampleBrief) {
                result = `Sample Brief: ${aucMicSampleBrief}`;
                aucMic.push(result);
              }
              if (!!aucMicAssayType) {
                result = `Assay Type: ${aucMicAssayType}`;
                aucMic.push(result);
              }

              // toxicity
              toxicity = pdCommonDisplayName({
                mean: toxicityMean,
                sd: toxicitySd,
                median: toxicityMedian,
                min: toxicityMin,
                max: toxicityMax,
                unit: toxicityUnit,
                other: toxicityOther
              });
              if (!!toxicitySampleBrief) {
                result = `Sample Brief: ${toxicitySampleBrief}`;
                toxicity.push(result);
              }
              if (!!toxicityMethodOfEvaluation) {
                result = `Method of evaluation: ${toxicityMethodOfEvaluation}`;
                toxicity.push(result);
              }

              // finalOutcome
              finalOutcome = pdCommonDisplayName({
                mean: finalOutcomesMean,
                sd: finalOutcomesSd,
                median: finalOutcomesMedian,
                min: finalOutcomesMin,
                max: finalOutcomesMax,
                unit: finalOutcomesUnit,
                other: finalOutcomesOther
              });
              if (!!finalOutcomesSampleBrief) {
                result = `Sample Brief: ${finalOutcomesSampleBrief}`;
                finalOutcome.push(result);
              }
              if (!!finalOutcomesMethodOfEvaluation) {
                result = `Method of evaluation: ${finalOutcomesMethodOfEvaluation}`;
                finalOutcome.push(result);
              }

              return (
                <StyledTableRow key={tbdrugPddataId} selected={isSeleted(item)}>
                  <Table.Cell title={referenceTitle}>
                    <ActivityItemAnchor
                      id={tbdrugPddataId}
                      text={referenceTitle}
                      onClick={onClick}
                    />
                  </Table.Cell>
                  <Table.Cell title={subject.join(', ')}>
                    {subject.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={experimentalCondition}>
                    {experimentalCondition}
                  </Table.Cell>
                  <Table.Cell title={studyBrief}>{studyBrief}</Table.Cell>
                  <Table.Cell title={gender.join(', ')}>
                    {gender.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={age.join(', ')}>
                    {age.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={status.join(', ')}>
                    {status.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={dose.join(', ')}>
                    {dose.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={fed.join(', ')}>
                    {fed.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={concomitenty.join(', ')}>
                    {concomitenty.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={targatedBacteria}>
                    {targatedBacteria}
                  </Table.Cell>
                  <Table.Cell title={cfu.join(', ')}>
                    {cfu.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={cd4.join(', ')}>
                    {cd4.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={mic.join(', ')}>
                    {mic.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={mbc.join(', ')}>
                    {mbc.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={mtc.join(', ')}>
                    {mtc.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={aucMic.join(', ')}>
                    {aucMic.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={toxicity.join(', ')}>
                    {toxicity.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={finalOutcome.join(', ')}>
                    {finalOutcome.join(', ')}
                  </Table.Cell>
                  <Table.Cell title={comments}>{comments}</Table.Cell>
                  <Table.Cell>
                    <StatusWrapper
                      subjectId={subjectId}
                      onChangeStatusCb={handleChangeStatus(item)}
                      activityKeyId={tbdrugPddataId}
                      projectActivityId={projectActivityId}
                      note={{
                        value: item.hasNote,
                        closed: onNoteModalClosed
                      }}
                      query={{
                        value: item.queryStatusCodeId,
                        closed: onQueryModalClosed
                      }}
                      file={{
                        value: item.hasFile,
                        closed: onFileModalClosed
                      }}
                      activity={{
                        value: activityStatusCodeId,
                        closed: () => {}
                      }}
                      audit={{
                        entityName: 'TbdrugPddata',
                        prefixLocale: 'activity.tbdrugPdData',
                        skip: ['tbdrugPddataId', 'tbdrugId']
                      }}
                      repeatItems={items}
                    />
                  </Table.Cell>
                </StyledTableRow>
              );
            })}
            {loading && (
              <EmptyRowsStickyViewForSemantic
                loading={true}
                columnLength={21}
                size="large"
              />
            )}
          </Table.Body>
        </Table>
      </div>
    </div>
  );
};

export default RepeatList;
