import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';
import { ActionBar, FormTitle, FormMetaBar } from '../';
import { useFormFields, useFormatMessage } from '../../../hooks';
import { useMoveToNextStep } from '../../collection/hooks';
import { AuthorInfo } from '../../collection';
import { notification } from '../../modal';
import { PdDataForm, RepeatList } from './';
import { usePdDataList, usePdData, useActionStatus } from '../hooks';
import tbdrugApi from '../../collection-tbdrug/api';
import { LeavingGuard } from '../../modal';
import { StatusWrapper } from '../';
import useRepeat from '../hooks/useRepeat';
import useStatus from '../hooks/useStatus';
import { code } from '../../../constants';

const MAX_FIELD_CNT = 6;

const isOpen = (...args) => {
  for (let i = 0; i < args.length; i++) {
    let curValue = args[i];

    if (typeof curValue === 'string') {
      curValue = curValue.trim();
    }

    if (!!curValue) {
      return true;
    }
  }
  return false;
};

const PdDataContainer = props => {
  const {
    activityKey,
    subjectId,
    projectId,
    projectActivityId,
    fetchActivityMenu
  } = props;
  const { add, update, remove, fetch, isSaving, isFetching } = usePdData({
    activityKey
  });

  const updateRepeat = (activityStatusCodeId, modelItem = {}) => {
    let target = model;
    let setTarget = setModel;

    if (modelItem[key] > 0) {
      target = modelItem;
      if (modelItem[key] !== model[key]) {
        setTarget = f => f;
      }
    }
    updateRepeatByKey({
      activityStatusCodeId,
      source: repeat.items,
      setSource: setRepeat,
      target,
      setTarget
    });
  };

  const { submitted, isNext } = useActionStatus(updateRepeat);
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const [repeat, setRepeat] = usePdDataList({
    tbdrugId: subjectId,
    projectActivityId
  });
  const t = useFormatMessage();
  const initModel = {
    projectActivityId: projectActivityId,
    activityStatusCodeId: 0,
    hasNote: false,
    isDone: false,
    modified: null,
    author: '',

    tbdrugPddataId: '',
    activityId: activityKey,
    tbdrugId: subjectId,

    referenceId: null,
    subjectIsHuman: null,
    subjectHuman: null,
    subjectHumanSampleSize: null,
    subjectIsInvitro: null,
    subjectInvitro: null,
    subjectInvitroSampleSize: null,
    subjectIsInvivo: null,
    subjectInvivo: null,
    subjectInvivoSampleSize: null,
    subjectIsMurine: null,
    subjectMurine: null,
    subjectMurineSampleSize: null,
    subjectOther: null,
    experimentalCondition: null,
    studyBrief: null,
    genderIsMale: null,
    genderMale: null,
    genderIsFemale: null,
    genderFemale: null,
    genderUnit: 'Count',
    genderOther: null,
    ageMean: null,
    ageSd: null,
    ageMedian: null,
    ageMin: null,
    ageMax: null,
    ageUnit: 'Year',
    ageOther: null,
    statusIsTb: false,
    statusTbDisease: null,
    statusIsHealthy: false,
    statusHealthyDisease: null,
    statusOther: null,
    doseAndRegimenDrugMeaning: null,
    doseAndRegimenDrugDose: null,
    doseAndRegimenDrugUnit: 'mg',
    doseAndRegimenDrugEtc: null,
    fedStatus: null,
    fedStatusOther: null,
    concomitentDrug1TbdrugId: null,
    concomitentDrug1Other: null,
    concomitentDrug1Dose: null,
    concomitentDrug1Unit: 'mg',
    concomitentDrug1Etc: null,
    concomitentDrug2TbdrugId: null,
    concomitentDrug2Other: null,
    concomitentDrug2Dose: null,
    concomitentDrug2Unit: 'mg',
    concomitentDrug2Etc: null,
    concomitentDrug3TbdrugId: null,
    concomitentDrug3Other: null,
    concomitentDrug3Dose: null,
    concomitentDrug3Unit: 'mg',
    concomitentDrug3Etc: null,
    concomitentDrug4TbdrugId: null,
    concomitentDrug4Other: null,
    concomitentDrug4Dose: null,
    concomitentDrug4Unit: 'mg',
    concomitentDrug4Etc: null,
    concomitentDrug5TbdrugId: null,
    concomitentDrug5Other: null,
    concomitentDrug5Dose: null,
    concomitentDrug5Unit: 'mg',
    concomitentDrug5Etc: null,
    concomitentDrug6TbdrugId: null,
    concomitentDrug6Other: null,
    concomitentDrug6Dose: null,
    concomitentDrug6Unit: 'mg',
    concomitentDrug6Etc: null,
    targatedBacteria: null,
    cfuMean: null,
    cfuSd: null,
    cfuMedian: null,
    cfuMin: null,
    cfuMax: null,
    cfuUnit: null,
    cfuOther: null,
    cfuPkconc: null,
    cfuSampleBrief: null,
    cfuAssayType: null,
    cd4CountMean: null,
    cd4CountSd: null,
    cd4CountMedian: null,
    cd4CountMin: null,
    cd4CountMax: null,
    cd4CountUnit: null,
    cd4CountOther: null,
    cd4SampleBrief: null,
    cd4AssayType: null,
    micMean: null,
    micSd: null,
    micMedian: null,
    micMin: null,
    micMax: null,
    micUnit: null,
    micOther: null,
    micSampleBrief: null,
    micAssayType: null,
    mbcMean: null,
    mbcSd: null,
    mbcMedian: null,
    mbcMin: null,
    mbcMax: null,
    mbcUnit: null,
    mbcOther: null,
    mbcSampleBrief: null,
    mbcAssayType: null,
    mtcMean: null,
    mtcSd: null,
    mtcMedian: null,
    mtcMin: null,
    mtcMax: null,
    mtcUnit: null,
    mtcOther: null,
    mtcSampleBrief: null,
    mtcAssayType: null,
    aucMicMean: null,
    aucMicSd: null,
    aucMicMedian: null,
    aucMicMin: null,
    aucMicMax: null,
    aucMicUnit: null,
    aucMicOther: null,
    aucMicSampleBrief: null,
    aucMicAssayType: null,
    toxicityMean: null,
    toxicitySd: null,
    toxicityMedian: null,
    toxicityMin: null,
    toxicityMax: null,
    toxicityUnit: null,
    toxicityOther: null,
    toxicitySampleBrief: null,
    toxicityMethodOfEvaluation: null,
    finalOutcomesMean: null,
    finalOutcomesSd: null,
    finalOutcomesMedian: null,
    finalOutcomesMin: null,
    finalOutcomesMax: null,
    finalOutcomesUnit: null,
    finalOutcomesOther: null,
    finalOutcomesSampleBrief: null,
    finalOutcomesMethodOfEvaluation: null,
    comments: null,
    isChanged: false,
    reason: null
  };

  const initAddFieldCnt = {
    concomitentDrug: 1
  };

  const initOpenField = {
    subjects: false,
    gender: false,
    age: false,
    status: false,
    dose: false,
    fed: false,
    concomitenty: false,
    cfu: false,
    cd4: false,
    mic: false,
    mbc: false,
    mtc: false,
    aucMic: false,
    toxicity: false,
    finalOutcome: false
  };

  const [selectedId, setSelectedId] = useState(0);
  const [refInfos, setRefInfos] = useState({});
  const [addFieldCnt, setAddFieldCnt] = useState(initAddFieldCnt);
  const [openField, setOpenField] = useState(initOpenField);
  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields(
    initModel
  );

  const key = 'tbdrugPddataId';
  const { updateRepeatByKey } = useRepeat(key);

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  useEffect(
    () => {
      if (selectedId > 0) {
        fetchItem();
      }
    },
    [selectedId]
  );

  useEffect(
    () => {
      // reference title 받아오기
      const referenceIdArr = new Set();

      for (let i = 0; i < repeat.items.length; i++) {
        if (!!repeat.items[i].referenceId) {
          referenceIdArr.add(repeat.items[i].referenceId);
        }
      }

      if (referenceIdArr.size > 0) {
        tbdrugApi
          .fetchReferencesByIds({
            referenceIds: [...referenceIdArr].join(',')
          })
          .then(res => {
            setRefInfos(res.data);
          });
      }
    },
    [repeat.items]
  );

  const fetchItem = useCallback(
    async () => {
      const data = await fetch({
        tbdrugId: subjectId,
        projectActivityId,
        tbdrugPddataId: model[key]
      });

      const newModel = {
        ...model,
        ...data,
        projectActivityId: projectActivityId,
        isDone: data.activityStatusCodeId === code.Temp ? false : true
      };

      //
      const {
        subjectIsHuman,
        subjectIsInvitro,
        subjectIsInvivo,
        subjectIsMurine,
        subjectOther,

        genderIsMale,
        genderIsFemale,
        genderOther,

        ageMean,
        ageSd,
        ageMedian,
        ageMin,
        ageMax,
        ageOther,

        statusIsTb,
        statusIsHealthy,
        statusOther,

        doseAndRegimenDrugMeaning,
        doseAndRegimenDrugDose,
        doseAndRegimenDrugEtc,

        fedStatus,
        fedStatusOther,

        concomitentDrug1TbdrugId,
        concomitentDrug1Other,
        concomitentDrug1Dose,
        concomitentDrug1Etc,
        concomitentDrug2TbdrugId,
        concomitentDrug2Other,
        concomitentDrug2Dose,
        concomitentDrug2Etc,
        concomitentDrug3TbdrugId,
        concomitentDrug3Other,
        concomitentDrug3Dose,
        concomitentDrug3Etc,
        concomitentDrug4TbdrugId,
        concomitentDrug4Other,
        concomitentDrug4Dose,
        concomitentDrug4Etc,
        concomitentDrug5TbdrugId,
        concomitentDrug5Other,
        concomitentDrug5Dose,
        concomitentDrug5Etc,
        concomitentDrug6TbdrugId,
        concomitentDrug6Other,
        concomitentDrug6Dose,
        concomitentDrug6Etc,

        cfuMean,
        cfuSd,
        cfuMedian,
        cfuMin,
        cfuMax,
        cfuOther,
        cfuPkconc,
        cfuSampleBrief,
        cfuAssayType,

        cd4CountMean,
        cd4CountSd,
        cd4CountMedian,
        cd4CountMin,
        cd4CountMax,
        cd4CountOther,
        cd4SampleBrief,
        cd4AssayType,

        micMean,
        micSd,
        micMedian,
        micMin,
        micMax,
        micOther,
        micSampleBrief,
        micAssayType,

        mbcMean,
        mbcSd,
        mbcMedian,
        mbcMin,
        mbcMax,
        mbcOther,
        mbcSampleBrief,
        mbcAssayType,
        mtcMean,
        mtcSd,
        mtcMedian,
        mtcMin,
        mtcMax,
        mtcOther,
        mtcSampleBrief,
        mtcAssayType,
        aucMicMean,
        aucMicSd,
        aucMicMedian,
        aucMicMin,
        aucMicMax,
        aucMicOther,
        aucMicSampleBrief,
        aucMicAssayType,
        toxicityMean,
        toxicitySd,
        toxicityMedian,
        toxicityMin,
        toxicityMax,
        toxicityOther,
        toxicitySampleBrief,
        toxicityMethodOfEvaluation,
        finalOutcomesMean,
        finalOutcomesSd,
        finalOutcomesMedian,
        finalOutcomesMin,
        finalOutcomesMax,
        finalOutcomesOther,
        finalOutcomesSampleBrief,
        finalOutcomesMethodOfEvaluation
      } = newModel;
      const newAddFieldCnt = {
        ...addFieldCnt
      };
      for (let i = 1; i < MAX_FIELD_CNT; i++) {
        const curNum = i + 1;
        for (let key in addFieldCnt) {
          if (addFieldCnt.hasOwnProperty(key)) {
            const curKey = `${key}${curNum}`;
            if (
              !!newModel[`${curKey}TbdrugId`] ||
              !!newModel[`${curKey}Other`]
            ) {
              newAddFieldCnt[key] = curNum;
            }
          }
        }
      }

      const newOpenField = {
        subjects: isOpen(
          subjectIsHuman,
          subjectIsInvitro,
          subjectIsInvivo,
          subjectIsMurine,
          subjectOther
        ),
        gender: isOpen(genderIsMale, genderIsFemale, genderOther),
        age: isOpen(ageMean, ageSd, ageMedian, ageMin, ageMax, ageOther),
        status: isOpen(statusIsTb, statusIsHealthy, statusOther),
        dose: isOpen(
          doseAndRegimenDrugMeaning,
          doseAndRegimenDrugDose,
          doseAndRegimenDrugEtc
        ),
        fed: isOpen(fedStatus, fedStatusOther),
        concomitenty: isOpen(
          concomitentDrug1TbdrugId,
          concomitentDrug1Other,
          concomitentDrug1Dose,
          concomitentDrug1Etc,
          concomitentDrug2TbdrugId,
          concomitentDrug2Other,
          concomitentDrug2Dose,
          concomitentDrug2Etc,
          concomitentDrug3TbdrugId,
          concomitentDrug3Other,
          concomitentDrug3Dose,
          concomitentDrug3Etc,
          concomitentDrug4TbdrugId,
          concomitentDrug4Other,
          concomitentDrug4Dose,
          concomitentDrug4Etc,
          concomitentDrug5TbdrugId,
          concomitentDrug5Other,
          concomitentDrug5Dose,
          concomitentDrug5Etc,
          concomitentDrug6TbdrugId,
          concomitentDrug6Other,
          concomitentDrug6Dose,
          concomitentDrug6Etc
        ),
        cfu: isOpen(
          cfuMean,
          cfuSd,
          cfuMedian,
          cfuMin,
          cfuMax,
          cfuOther,
          cfuPkconc,
          cfuSampleBrief,
          cfuAssayType
        ),
        cd4: isOpen(
          cd4CountMean,
          cd4CountSd,
          cd4CountMedian,
          cd4CountMin,
          cd4CountMax,
          cd4CountOther,
          cd4SampleBrief,
          cd4AssayType
        ),
        mic: isOpen(
          micMean,
          micSd,
          micMedian,
          micMin,
          micMax,
          micOther,
          micSampleBrief,
          micAssayType
        ),
        mbc: isOpen(
          mbcMean,
          mbcSd,
          mbcMedian,
          mbcMin,
          mbcMax,
          mbcOther,
          mbcSampleBrief,
          mbcAssayType
        ),
        mtc: isOpen(
          mtcMean,
          mtcSd,
          mtcMedian,
          mtcMin,
          mtcMax,
          mtcOther,
          mtcSampleBrief,
          mtcAssayType
        ),
        aucMic: isOpen(
          aucMicMean,
          aucMicSd,
          aucMicMedian,
          aucMicMin,
          aucMicMax,
          aucMicOther,
          aucMicSampleBrief,
          aucMicAssayType
        ),
        toxicity: isOpen(
          toxicityMean,
          toxicitySd,
          toxicityMedian,
          toxicityMin,
          toxicityMax,
          toxicityOther,
          toxicitySampleBrief,
          toxicityMethodOfEvaluation
        ),
        finalOutcome: isOpen(
          finalOutcomesMean,
          finalOutcomesSd,
          finalOutcomesMedian,
          finalOutcomesMin,
          finalOutcomesMax,
          finalOutcomesOther,
          finalOutcomesSampleBrief,
          finalOutcomesMethodOfEvaluation
        )
      };

      setOpenField(newOpenField);

      setAddFieldCnt(newAddFieldCnt);

      setModel(newModel);
    },
    [model]
  );

  const handleOpenFieldClick = (event, { name }) => {
    setOpenField({
      ...openField,
      [name]: !openField[name]
    });
  };

  const handleSubmit = async () => {
    if (model[key] > 0) {
      const isOK = await update(model);
      if (isOK) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } else {
      const newId = await add(model);
      if (newId > 0) {
        model[key] = newId;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            updateRepeat(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    }
  };

  const handleRepeatItemClick = useCallback(id => {
    setModel({ ...model, tbdrugPddataId: id });
    setSelectedId(id);
  }, []);

  const handleAddClick = useCallback(
    () => {
      setModel({
        ...initModel,
        tbdrugPddataId: 0,
        activityStatusCodeId: code.Temp
      });
      setOpenField({ ...initOpenField });
      setAddFieldCnt({ ...initAddFieldCnt });
      setSelectedId(0);
    },
    [repeat.items]
  );

  const handleFileModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback((value, updated) => {
    setRepeat(updated);
    status.note.setHasNote(value);
  }, []);

  const handleRemove = async () => {
    const result = await remove(model);

    if (result) {
      notification.info({
        title: t('common.alert.deleted'),
        onClose: () => {
          setModel({ ...initModel });
          repeat.fetch();
          fetchActivityMenu();
        },
        confirmButtonName: t('common.ok')
      });
    } else {
      notification.warning({
        title: t('cannot.deleted')
      });
    }
  };

  const isSeleted = useCallback(item => `${item[key]}` === `${selectedId}`, [
    selectedId
  ]);

  return (
    <Fragment>
      <Button onClick={handleAddClick}>{t('activity.add')}</Button>
      <LeavingGuard shouldBlock={model.isChanged} />
      <RepeatList
        subjectId={subjectId}
        updateRepeat={updateRepeat}
        projectActivityId={projectActivityId}
        items={repeat.items}
        loading={repeat.loading}
        refInfos={refInfos}
        MAX_FIELD_CNT={MAX_FIELD_CNT}
        onClick={handleRepeatItemClick}
        onQueryModalClosed={handleQueryModalClosed}
        onFileModalClosed={handleFileModalClosed}
        onNoteModalClosed={handleNoteModalClosed}
        isSeleted={isSeleted}
      />
      {model[key] !== '' && (
        <Fragment>
          <FormTitle text="PD Data" />
          <FormMetaBar>
            <AuthorInfo date={model.modified} name={model.author} />
            <StatusWrapper
              subjectId={subjectId}
              onChangeStatusCb={updateRepeat}
              activityKeyId={model[key]}
              projectActivityId={projectActivityId}
              note={{
                value: status.note.hasNote,
                closed: handleNoteModalClosed
              }}
              query={{
                value: status.qna.query.queryStatusCodeId,
                closed: handleQueryModalClosed
              }}
              file={{
                value: status.file.hasFile,
                closed: handleFileModalClosed
              }}
              activity={{
                value: model.activityStatusCodeId,
                closed: () => {}
              }}
              audit={{
                entityName: 'TbdrugPddata',
                prefixLocale: 'activity.tbdrugPdData',
                skip: [key, 'tbdrugId']
              }}
              repeatItems={repeat.items}
            />
          </FormMetaBar>
          <PdDataForm
            projectId={projectId}
            loading={isFetching.value}
            model={model}
            onChange={onChange}
            onSubmit={handleSubmit}
            refInfos={refInfos}
            setModel={setModel}
            addFieldCnt={addFieldCnt}
            setAddFieldCnt={setAddFieldCnt}
            MAX_FIELD_CNT={MAX_FIELD_CNT}
            openField={openField}
            onOpenFieldClick={handleOpenFieldClick}
          >
            <ActionBar
              statusCode={model.activityStatusCodeId}
              isDone={model.isDone}
              subjectId={subjectId}
              projectActivityId={projectActivityId}
              activityKeyId={model[key]}
              loading={isSaving}
              onClick={submitted}
              onRemoveClick={handleRemove}
              hasRemoveButton={true}
            />
          </PdDataForm>
        </Fragment>
      )}
    </Fragment>
  );
};

export default PdDataContainer;
