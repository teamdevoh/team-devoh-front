import React, { useCallback } from 'react';
import {
  Form,
  Input,
  Button,
  Icon,
  TextArea,
  Dropdown,
  Label
} from 'semantic-ui-react';
import { useSubmit } from '../../../hooks';
import { SearchReferenceInput } from '../../collection-tbdrug';
import { FormDoneOrTempBar } from '../';
import {
  FlexField,
  StyledCheckbox,
  StyledInput,
  SubFieldWrap,
  OpenFieldLabel,
  OpenFieldToggleBtn,
  StyledB
} from '../styles';
import {
  AgeFeild,
  StatusFeild,
  DoseFeild,
  FedStatusFeild,
  ConcomitentyDrugsFeild,
  PdCommonFeild
} from '../DrugFormFeilds';
import { SelectReason } from '../Selector';
import styled from 'styled-components';

const MediaQueryFlexField = styled(FlexField)`
  &&& {
    @media only screen and (max-width: 1024px) {
      & .checkbox {
        width: 100%;
        margin-bottom: 10px;
      }
    }

    & .input:last-child {
      margin-left: 20px;

      @media only screen and (max-width: 767px) {
        margin-left: 0px;
      }
    }
  }
`;

const PdDataForm = ({
  MAX_FIELD_CNT,
  children,
  projectId,
  loading,
  model,
  onChange,
  onSubmit,
  refInfos,
  setModel,
  addFieldCnt,
  setAddFieldCnt,
  openField,
  onOpenFieldClick
}) => {
  const [{ onValidate, validator }] = useSubmit(onSubmit);

  const getRefTitle = id => {
    if (typeof refInfos[id] !== 'undefined') {
      return refInfos[id].title;
    }

    return '';
  };

  const handleCheckboxChange = (event, { name, checked }) => {
    onChange(event, { name, value: checked });
  };

  const handleAddField = e => {
    e.preventDefault();
    if (addFieldCnt.concomitentDrug < MAX_FIELD_CNT) {
      const count = addFieldCnt.concomitentDrug + 1;
      const modelKey = `concomitentDrug${count}`;

      setAddFieldCnt({
        ...addFieldCnt,
        concomitentDrug: count
      });

      setModel({
        ...model,
        [`${modelKey}TbdrugId`]: '',
        [`${modelKey}Other`]: '',
        [`${modelKey}Dose`]: '',
        [`${modelKey}Unit`]: 'mg'
      });
    }
  };

  const handleRemoveField = key => () => {
    const orgTotalCount = addFieldCnt[key];
    const count = addFieldCnt[key] - 1;
    let newModel = { ...model };

    setAddFieldCnt({
      ...addFieldCnt,
      [key]: count
    });

    delete newModel[`${key}${orgTotalCount}TbdrugId`];
    delete newModel[`${key}${orgTotalCount}Other`];
    delete newModel[`${key}${orgTotalCount}Dose`];
    delete newModel[`${key}${orgTotalCount}Unit`];

    setModel(newModel);
  };

  const generateField = useCallback(
    () => {
      const result = [];
      const key = 'concomitentDrug';
      const count = addFieldCnt[key];

      for (let i = 0; i < count; i++) {
        const modelCount = i + 1;
        const modelKey = `${key}${modelCount}`;

        result.push(
          <ConcomitentyDrugsFeild
            key={modelKey}
            onChange={onChange}
            model={model}
            keyName={key}
            modelKey={modelKey}
            i={i}
            count={count}
            onRemoveField={handleRemoveField}
          />
        );
      }

      return result;
    },
    [model]
  );

  const isAddDrugBtnDisabled = (() => {
    for (let n = 0; n < addFieldCnt.concomitentDrug; n++) {
      if (typeof model[`concomitentDrug${n + 1}TbdrugId`] !== 'undefined') {
        if (
          !model[`concomitentDrug${n + 1}TbdrugId`] &&
          !model[`concomitentDrug${n + 1}Other`]
        ) {
          return true;
        }
      }
    }
    return false;
  })();

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field>
        <SearchReferenceInput
          searchValue={getRefTitle(model.referenceId)}
          name="referenceId"
          value={model.referenceId}
          onChange={onChange}
        />
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="subjects"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.subjects ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Subjects]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.subjects}>
          <MediaQueryFlexField>
            <StyledCheckbox
              name="subjectIsHuman"
              label="Human"
              checked={!!model.subjectIsHuman}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              className="mq-small"
              labelPosition="right"
              type="text"
              name="subjectHumanSampleSize"
              value={model.subjectHumanSampleSize || ''}
              onChange={onChange}
              disabled={!!!model.subjectIsHuman}
            >
              <Label>Sample size</Label>
              <input />
              <Label basic>count</Label>
            </StyledInput>
            <StyledInput
              className="mq-small"
              labelPosition="left"
              type="text"
              name="subjectHuman"
              value={model.subjectHuman || ''}
              onChange={onChange}
              disabled={!!!model.subjectIsHuman}
            >
              <Label>Description</Label>
              <input />
            </StyledInput>
          </MediaQueryFlexField>
          <MediaQueryFlexField>
            <StyledCheckbox
              name="subjectIsInvitro"
              label="In vitro"
              checked={!!model.subjectIsInvitro}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              className="mq-small"
              labelPosition="right"
              type="text"
              name="subjectInvitroSampleSize"
              value={model.subjectInvitroSampleSize || ''}
              onChange={onChange}
              disabled={!!!model.subjectIsInvitro}
            >
              <Label>Sample size</Label>
              <input />
              <Label basic>count</Label>
            </StyledInput>
            <StyledInput
              className="mq-small"
              labelPosition="left"
              type="text"
              name="subjectInvitro"
              value={model.subjectInvitro || ''}
              onChange={onChange}
              disabled={!!!model.subjectIsInvitro}
            >
              <Label>Description</Label>
              <input />
            </StyledInput>
          </MediaQueryFlexField>
          <MediaQueryFlexField>
            <StyledCheckbox
              name="subjectIsInvivo"
              label="In vivo"
              checked={!!model.subjectIsInvivo}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              className="mq-small"
              labelPosition="right"
              type="text"
              name="subjectInvivoSampleSize"
              value={model.subjectInvivoSampleSize || ''}
              onChange={onChange}
              disabled={!!!model.subjectIsInvivo}
            >
              <Label>Sample size</Label>
              <input />
              <Label basic>count</Label>
            </StyledInput>
            <StyledInput
              className="mq-small"
              labelPosition="left"
              type="text"
              name="subjectInvivo"
              value={model.subjectInvivo || ''}
              onChange={onChange}
              disabled={!!!model.subjectIsInvivo}
            >
              <Label>Description</Label>
              <input />
            </StyledInput>
          </MediaQueryFlexField>
          <MediaQueryFlexField>
            <StyledCheckbox
              name="subjectIsMurine"
              label="Murine model of M.tb"
              checked={!!model.subjectIsMurine}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              className="mq-small"
              labelPosition="right"
              type="text"
              name="subjectMurineSampleSize"
              value={model.subjectMurineSampleSize || ''}
              onChange={onChange}
              disabled={!!!model.subjectIsMurine}
            >
              <Label>Sample size</Label>
              <input />
              <Label basic>count</Label>
            </StyledInput>
            <StyledInput
              className="mq-small"
              labelPosition="left"
              type="text"
              name="subjectMurine"
              value={model.subjectMurine || ''}
              onChange={onChange}
              disabled={!!!model.subjectIsMurine}
            >
              <Label>Description</Label>
              <input />
            </StyledInput>
          </MediaQueryFlexField>
          <Form.Field>
            <Input
              name="subjectOther"
              label="Other"
              value={model.subjectOther || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <label>Experimental condition</label>
        <TextArea
          name="experimentalCondition"
          value={model.experimentalCondition || ''}
          onChange={onChange}
        />
      </Form.Field>

      <Form.Field>
        <label>Study Brief</label>
        <TextArea
          name="studyBrief"
          value={model.studyBrief || ''}
          onChange={onChange}
        />
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="gender"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.gender ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Gender]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.gender}>
          <FlexField>
            <StyledCheckbox
              name="genderIsMale"
              label="Male"
              checked={!!model.genderIsMale}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              name="genderMale"
              value={model.genderMale || ''}
              onChange={onChange}
              disabled={!!!model.genderIsMale}
            />
          </FlexField>
          <FlexField>
            <StyledCheckbox
              name="genderIsFemale"
              label="Female"
              checked={!!model.genderIsFemale}
              onChange={handleCheckboxChange}
            />
            <StyledInput
              name="genderFemale"
              value={model.genderFemale || ''}
              onChange={onChange}
              disabled={!!!model.genderIsFemale}
            />
          </FlexField>
          <FlexField>
            <Dropdown
              name="genderUnit"
              fluid
              selection
              value={model.genderUnit}
              onChange={onChange}
              options={[
                {
                  text: 'Count',
                  value: 'Count'
                },
                {
                  text: '%',
                  value: '%'
                }
              ]}
            />
          </FlexField>
          <Form.Field>
            <Input
              label="Other"
              name="genderOther"
              value={model.genderOther || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="age"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.age ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Age]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.age}>
          <AgeFeild onChange={onChange} model={model} />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="status"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.status ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Status(disease/healthy)]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.status}>
          <StatusFeild
            onChange={onChange}
            model={model}
            onCheckboxChange={handleCheckboxChange}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="dose"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.dose ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Dose and regimen in detail]</StyledB> data in the
          Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.dose}>
          <DoseFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              meaning: 'doseAndRegimenDrugMeaning',
              dose: 'doseAndRegimenDrugDose',
              unit: 'doseAndRegimenDrugUnit',
              other: 'doseAndRegimenDrugEtc'
            }}
          />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="fed"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.fed ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Fed Status]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.fed}>
          <FedStatusFeild onChange={onChange} model={model} />
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="concomitenty"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.concomitenty ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Concomitenty used drugs]</StyledB> data in the Reference?
          {addFieldCnt.concomitentDrug < MAX_FIELD_CNT && (
            <Button
              type="button"
              style={{
                marginLeft: '10px'
              }}
              disabled={isAddDrugBtnDisabled}
              onClick={handleAddField}
            >
              Add Drug
            </Button>
          )}
        </OpenFieldLabel>
        <SubFieldWrap open={openField.concomitenty}>
          {generateField()}
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <label>Targated bacteria or pathogen</label>
        <Input
          name="targatedBacteria"
          value={model.targatedBacteria || ''}
          onChange={onChange}
        />
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="cfu"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.cfu ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[CFU,M.tb]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.cfu}>
          <PdCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'cfuMean',
              sd: 'cfuSd',
              median: 'cfuMedian',
              min: 'cfuMin',
              max: 'cfuMax',
              unit: 'cfuUnit'
            }}
          />
          <Form.Field>
            <Input
              label="CFU,M.tb Other"
              name="cfuOther"
              value={model.cfuOther || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="PK Conc"
              name="cfuPkconc"
              value={model.cfuPkconc || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Sample Brief"
              name="cfuSampleBrief"
              value={model.cfuSampleBrief || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Assay Type"
              name="cfuAssayType"
              value={model.cfuAssayType || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="cd4"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.cd4 ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[CD4 counts]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.cd4}>
          <PdCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'cd4CountMean',
              sd: 'cd4CountSd',
              median: 'cd4CountMedian',
              min: 'cd4CountMin',
              max: 'cd4CountMax',
              unit: 'cd4CountUnit'
            }}
          />
          <Form.Field>
            <Input
              label="CD4 counts Other"
              name="cd4CountOther"
              value={model.cd4CountOther || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Sample Brief"
              name="cd4SampleBrief"
              value={model.cd4SampleBrief || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Assay Type"
              name="cd4AssayType"
              value={model.cd4AssayType || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="mic"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.mic ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[MIC]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.mic}>
          <PdCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'micMean',
              sd: 'micSd',
              median: 'micMedian',
              min: 'micMin',
              max: 'micMax',
              unit: 'micUnit'
            }}
          />
          <Form.Field>
            <Input
              label="MIC Other"
              name="micOther"
              value={model.micOther || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Sample Brief"
              name="micSampleBrief"
              value={model.micSampleBrief || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Assay Type"
              name="micAssayType"
              value={model.micAssayType || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="mbc"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.mbc ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[MBC]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.mbc}>
          <PdCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'mbcMean',
              sd: 'mbcSd',
              median: 'mbcMedian',
              min: 'mbcMin',
              max: 'mbcMax',
              unit: 'mbcUnit'
            }}
          />
          <Form.Field>
            <Input
              label="MBC Other"
              name="mbcOther"
              value={model.mbcOther || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Sample Brief"
              name="mbcSampleBrief"
              value={model.mbcSampleBrief || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Assay Type"
              name="mbcAssayType"
              value={model.mbcAssayType || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="mtc"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.mtc ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[MTC]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.mtc}>
          <PdCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'mtcMean',
              sd: 'mtcSd',
              median: 'mtcMedian',
              min: 'mtcMin',
              max: 'mtcMax',
              unit: 'mtcUnit'
            }}
          />
          <Form.Field>
            <Input
              label="MTC Other"
              name="mtcOther"
              value={model.mtcOther || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Sample Brief"
              name="mtcSampleBrief"
              value={model.mtcSampleBrief || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Assay Type"
              name="mtcAssayType"
              value={model.mtcAssayType || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="aucMic"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.aucMic ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[AUC/MIC]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.aucMic}>
          <PdCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'aucMicMean',
              sd: 'aucMicSd',
              median: 'aucMicMedian',
              min: 'aucMicMin',
              max: 'aucMicMax',
              unit: 'aucMicUnit'
            }}
          />
          <Form.Field>
            <Input
              label="AUC/MIC Other"
              name="aucMicOther"
              value={model.aucMicOther || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Sample Brief"
              name="aucMicSampleBrief"
              value={model.aucMicSampleBrief || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Assay Type"
              name="aucMicAssayType"
              value={model.aucMicAssayType || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="toxicity"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.toxicity ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Toxicity]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.toxicity}>
          <PdCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'toxicityMean',
              sd: 'toxicitySd',
              median: 'toxicityMedian',
              min: 'toxicityMin',
              max: 'toxicityMax',
              unit: 'toxicityUnit'
            }}
          />
          <Form.Field>
            <Input
              label="Toxicity Other"
              name="toxicityOther"
              value={model.toxicityOther || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Sample Brief"
              name="toxicitySampleBrief"
              value={model.toxicitySampleBrief || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Method of evaluation"
              name="toxicityMethodOfEvaluation"
              value={model.toxicityMethodOfEvaluation || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <OpenFieldLabel>
          <OpenFieldToggleBtn
            name="finalOutcome"
            onClick={onOpenFieldClick}
            type="button"
          >
            <Icon
              name={`circle ${openField.finalOutcome ? 'check' : 'outline'}`}
              color="teal"
              size="small"
            />
          </OpenFieldToggleBtn>
          Is <StyledB>[Final outcomes]</StyledB> data in the Reference?
        </OpenFieldLabel>
        <SubFieldWrap open={openField.finalOutcome}>
          <PdCommonFeild
            onChange={onChange}
            model={model}
            modelKeyPair={{
              mean: 'finalOutcomesMean',
              sd: 'finalOutcomesSd',
              median: 'finalOutcomesMedian',
              min: 'finalOutcomesMin',
              max: 'finalOutcomesMax',
              unit: 'finalOutcomesUnit'
            }}
          />
          <Form.Field>
            <Input
              label="FinalOutcome Other"
              name="finalOutcomesOther"
              value={model.finalOutcomesOther || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Sample Brief"
              name="finalOutcomesSampleBrief"
              value={model.finalOutcomesSampleBrief || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <Input
              label="Method of evaluation"
              name="finalOutcomesMethodOfEvaluation"
              value={model.finalOutcomesMethodOfEvaluation || ''}
              onChange={onChange}
            />
          </Form.Field>
        </SubFieldWrap>
      </Form.Field>

      <Form.Field>
        <label>Comments/discussion</label>
        <TextArea
          name="comments"
          value={model.comments || ''}
          onChange={onChange}
        />
      </Form.Field>

      {model.tbdrugPddataId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title="PD Data"
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default PdDataForm;
