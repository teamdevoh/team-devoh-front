import React, { Fragment } from 'react';
import {
  NoteStatus,
  QueryStatus,
  FileStatus,
  ActivityStatus,
  AuditStatus
} from './Status';
import { useRepeatStatus } from './hooks';
import { usePublishStatus } from '../collection/hooks';
import { useParams } from 'react-router-dom';

const StatusWrapper = ({
  subjectId,
  onChangeStatusCb,
  activityKeyId,
  projectActivityId,
  note,
  query,
  file,
  audit,
  activity,
  repeatItems
}) => {
  const { Refresh } = useRepeatStatus();
  const { projectId } = useParams();
  const publish = usePublishStatus({
    projectActivityId
  });

  const handleFileModalClosed = data => {
    if (repeatItems) {
      const updatedItems = Refresh({ ...data, items: repeatItems });
      file.closed(data.hasFile, updatedItems);
    } else {
      file.closed(data.hasFile, []);
    }
  };

  const handleQueryModalClosed = data => {
    let lastValue = 0;
    if (repeatItems) {
      const updatedItems = Refresh({ ...data, items: repeatItems });
      const filtered = updatedItems.filter(item => {
        return item.activityKeyId > 0 && item.queryStatusCodeId > 0;
      });
      lastValue = publish.pickQueryValueBy(filtered);
      query.closed(data.queryStatusCodeId, updatedItems);
    } else {
      lastValue = data.queryStatusCodeId;
      query.closed(data.queryStatusCodeId, []);
    }

    publish.queryStatus(lastValue);
  };

  const handleNoteClosed = data => {
    let lastValue = false;
    if (repeatItems) {
      const updatedItems = Refresh({ ...data, items: repeatItems });
      const filtered = updatedItems.filter(item => {
        return item.activityKeyId > 0 && item.hasNote !== void 0;
      });
      lastValue = publish.pickNoteValueBy(filtered);
      note.closed(data.hasNote, updatedItems);
    } else {
      lastValue = data.hasNote;
      note.closed(data.hasNote, []);
    }

    publish.noteStatus(lastValue);
  };

  return (
    <Fragment>
      {audit && (
        <AuditStatus
          activityKeyId={activityKeyId}
          entityName={audit.entityName}
          prefixLocale={audit.prefixLocale}
          skip={audit.skip}
        />
      )}
      {activity && (
        <ActivityStatus
          status={activity.value}
          subjectId={subjectId}
          activityKeyId={activityKeyId}
          projectActivityId={projectActivityId}
          onChangeStatusCb={onChangeStatusCb}
          projectId={projectId}
        />
      )}
      {file && (
        <FileStatus
          subjectId={subjectId}
          activityKeyId={activityKeyId}
          projectActivityId={projectActivityId}
          hasFile={file.value}
          onModalClosed={handleFileModalClosed}
        />
      )}
      {query && (
        <QueryStatus
          subjectId={subjectId}
          activityKeyId={activityKeyId}
          projectActivityId={projectActivityId}
          queryStatusCodeId={query.value}
          onQueryModalClosed={handleQueryModalClosed}
        />
      )}
      {note && (
        <NoteStatus
          subjectId={subjectId}
          activityKeyId={activityKeyId}
          projectActivityId={projectActivityId}
          hasNote={note.value}
          onNoteModalClosed={handleNoteClosed}
        />
      )}
    </Fragment>
  );
};

export default StatusWrapper;
