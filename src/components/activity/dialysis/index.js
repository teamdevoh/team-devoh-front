import DialysisContainer from './DialysisContainer';
import DialysisForm from './DialysisForm';

export { DialysisContainer, DialysisForm };
