import React from 'react';
import { Form } from 'semantic-ui-react';
import { MyVisit, DialysisMethod, HDS, SelectReason } from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';
import DateInput from '../../input/DateInput';

const DialysisForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisit
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.dialysis.dialysisMethodCodeId')}</label>
        <DialysisMethod
          name="dialysisMethodCodeId"
          value={model.dialysisMethodCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('activity.dialysis.hds')}</label>
        <HDS name="hds" value={model.hds} onChange={onChange} />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('activity.dialysis.hdstime')}</label>
          <DateInput
            name="hdstime"
            value={model.hdstime}
            placeholder="HH:mm"
            options={{ dateFormat: 'uk:uk', separate: '-' }}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.dialysis.hdetime')}</label>
          <DateInput
            name="hdetime"
            value={model.hdetime}
            placeholder="HH:mm"
            options={{ dateFormat: 'uk:uk', separate: '-' }}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {model.subjectDialysisId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.dialysis.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>

      {children}
    </Form>
  );
};

export default DialysisForm;
