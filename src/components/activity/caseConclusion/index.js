import CaseConclusionContainer from './CaseConclusionContainer';
import CaseConclusionForm from './CaseConclusionForm';

// const CaseConclusionContainer = asyncComponent(() => import('./CaseConclusionContainer'));
// const CaseConclusionForm = asyncComponent(() => import('./CaseConclusionForm'));

export { CaseConclusionContainer, CaseConclusionForm };
