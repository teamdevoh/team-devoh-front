import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import {
  Casecon,
  DigFin,
  MyVisitByActivityGroupId,
  SelectReason
} from '../Selector';
import { useSubmit, useFormatMessage } from '../../../hooks';
import { FormDoneOrTempBar } from '../';
import { DatePicker } from '../../datepicker';
import { code } from '../../../constants';

const CaseConclusionForm = ({
  children,
  projectId,
  loading,
  model,
  setModel,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <Form loading={loading} onSubmit={onValidate}>
      <Form.Field required widths="equal">
        <label>{t('activity.visit')}</label>
        <MyVisitByActivityGroupId
          name="subjectVisitId"
          projectId={projectId}
          subjectId={model.subjectId}
          value={model.subjectVisitId}
          onChange={onChange}
          visitTypeCode={code.CaseConclusion}
        />
        {validator.message(
          t('activity.visit'),
          model.subjectVisitId,
          'required'
        )}
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.caseConclusion.caseconCodeId')}</label>
        <Casecon
          name="caseconCodeId"
          value={model.caseconCodeId}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Field>
        <label>{t('activity.caseConclusion.condtc')}</label>
        <DatePicker name="condtc" value={model.condtc} onChange={onChange} />
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.caseConclusion.digFinCodeId')}</label>
        <DigFin
          name="digFinCodeId"
          value={model.digFinCodeId}
          onChange={onChange}
        />
      </Form.Group>
      {model.digFinCodeId === code.DigFinOther && (
        <Form.Field required>
          <label>{t('activity.caseConclusion.digFinOth')}</label>
          <Input
            name="digFinOth"
            value={model.digFinOth || ''}
            onChange={onChange}
          />
          {validator.message(
            t('activity.caseConclusion.digFinOth'),
            model.digFinOth,
            'required'
          )}
        </Form.Field>
      )}
      {model.subjectCaseConclusionId > 0 &&
        model.isChanged && (
          <SelectReason
            model={model}
            setModel={setModel}
            onChange={onChange}
            validator={validator}
          />
        )}
      <Form.Field>
        <FormDoneOrTempBar
          name="isDone"
          title={t('activity.caseConclusion.title')}
          checked={model.isDone}
          onChange={onChange}
          statusCode={model.activityStatusCodeId}
        />
      </Form.Field>
      {children}
    </Form>
  );
};

export default CaseConclusionForm;
