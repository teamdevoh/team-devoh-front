import React, { Fragment, useEffect, useCallback } from 'react';
import api from '../api';
import { ActionBar, FormMetaBar, FormTitle } from '../';
import { AuthorInfo } from '../../collection';
import { notification } from '../../modal';
import { CaseConclusionForm } from '.';
import { useFormFields, useBoolean, useFormatMessage } from '../../../hooks';
import { usePublishStatus, useMoveToNextStep } from '../../collection/hooks';
import { useFormStatus, useActionStatus } from '../hooks';
import { LeavingGuard } from '../../modal';
import StatusWrapper from '../StatusWrapper';
import useStatus from '../hooks/useStatus';
import { code } from '../../../constants';

const CaseConclusionContainer = props => {
  const { activityKey, subjectId, projectId, projectActivityId } = props;
  const key = 'subjectCaseConclusionId';
  const isFetching = useBoolean(true);
  const isSaving = useBoolean(false);
  const { convert } = useFormStatus();
  const handleChangeStatus = activityStatusCodeId => {
    setModel({
      ...model,
      activityStatusCodeId
    });
  };
  const { submitted, isNext } = useActionStatus(handleChangeStatus);
  const publish = usePublishStatus({
    projectActivityId
  });
  const { goToNext } = useMoveToNextStep({
    projectActivityId
  });
  const t = useFormatMessage();

  const [{ model, onChange, resetPrevModel }, setModel] = useFormFields({
    projectActivityId: projectActivityId,
    activityStatusCodeId: code.Temp,
    isDone: false,
    subjectCaseConclusionId: 0,
    subjectId: subjectId,
    subjectVisitId: '',
    caseconCodeId: null,
    condtc: null,
    digFinCodeId: null,
    digFinOth: null,
    modified: null,
    author: '',
    isChanged: false,
    reason: ''
  });

  const status = useStatus({
    subjectId,
    activityKeyId: model[key],
    projectActivityId
  });

  const fetchItem = async () => {
    const res = await api.fetchCaseConclusion({
      subjectId,
      projectActivityId: projectActivityId
    });
    if (res && res.data) {
      setModel({
        ...model,
        ...res.data,
        projectActivityId: projectActivityId,
        isDone: res.data.activityStatusCodeId === code.Temp ? false : true
      });
    }

    isFetching.setFalse();
  };

  useEffect(() => {
    fetchItem();
  }, []);

  const add = async () => {
    model.activityStatusCodeId = convert(model.isDone);
    isSaving.setTrue();
    try {
      const res = await api.addCaseConclusion({
        activityId: activityKey,
        model
      });
      if (res && res.data > 0) {
        model[key] = res.data;
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            resetPrevModel();
            publish.activityStatus(model.activityStatusCodeId);
            if (isNext) {
              goToNext();
            }
          }
        });
      }
    } finally {
      isSaving.setFalse();
    }
  };

  const update = async () => {
    model.activityStatusCodeId = convert(
      model.isDone,
      model.activityStatusCodeId
    );
    isSaving.setTrue();
    const res = await api.updateCaseConclusion({
      activityId: activityKey,
      subjectCaseConclusionId: model[key],
      model
    });
    if (res && res.data) {
      notification.success({
        title: t('common.alert.changed'),
        onClose: () => {
          resetPrevModel();
          publish.activityStatus(model.activityStatusCodeId);
          if (isNext) {
            goToNext();
          }
        }
      });
    }
    isSaving.setFalse();
  };

  const handleSubmit = async () => {
    if (model[key] > 0) {
      await update();
    } else {
      await add();
    }
  };

  const handleFileModalClosed = useCallback(value => {
    status.file.setHasFile(value);
  }, []);

  const handleQueryModalClosed = useCallback(value => {
    status.qna.setQueryStatus({
      ...status.qna.query,
      queryStatusCodeId: value
    });
  }, []);

  const handleNoteModalClosed = useCallback(value => {
    status.note.setHasNote(value);
  }, []);

  return (
    <Fragment>
      <LeavingGuard shouldBlock={model.isChanged} />
      <FormTitle text={t('activity.caseConclusion.title')} />
      <FormMetaBar>
        <AuthorInfo date={model.modified} name={model.author} />
        <StatusWrapper
          subjectId={subjectId}
          onChangeStatusCb={handleChangeStatus}
          activityKeyId={model[key]}
          projectActivityId={projectActivityId}
          note={{
            value: status.note.hasNote,
            closed: handleNoteModalClosed
          }}
          query={{
            value: status.qna.query.queryStatusCodeId,
            closed: handleQueryModalClosed
          }}
          file={{
            value: status.file.hasFile,
            closed: handleFileModalClosed
          }}
          activity={{
            value: model.activityStatusCodeId,
            closed: () => {}
          }}
          audit={{
            entityName: 'SubjectCaseConclusion',
            prefixLocale: 'activity.caseConclusion',
            skip: ['subjectCaseConclusionId']
          }}
        />
      </FormMetaBar>
      <CaseConclusionForm
        projectId={projectId}
        loading={isFetching.value}
        model={model}
        setModel={setModel}
        onChange={onChange}
        onSubmit={handleSubmit}
      >
        <ActionBar
          statusCode={model.activityStatusCodeId}
          isDone={model.isDone}
          subjectId={subjectId}
          projectActivityId={projectActivityId}
          activityKeyId={model[key]}
          loading={isSaving.value}
          onClick={submitted}
        />
      </CaseConclusionForm>
    </Fragment>
  );
};

export default CaseConclusionContainer;
