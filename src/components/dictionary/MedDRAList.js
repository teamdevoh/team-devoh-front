import React, { useState } from 'react';
import { List } from 'semantic-ui-react';
import MedDRAListItem from './MedDRAListItem';
import { useCallback } from 'react';

const MedDRAList = ({ onSelectedItem, keyword, dictionaryId, items }) => {
  const [breadCrumbs, setBreadCrumbs] = useState({});

  const handleItemClick = useCallback(
    data => {
      setBreadCrumbs(data);
      onSelectedItem(data);
    },
    [onSelectedItem]
  );

  return (
    <List>
      {items.map(item => {
        return (
          <MedDRAListItem
            key={`${item.id}${item.codeName}${item.code}`}
            dictionaryId={dictionaryId}
            keyword={keyword}
            item={item}
            breadCrumbs={breadCrumbs}
            onClick={handleItemClick}
          />
        );
      })}
    </List>
  );
};

export default MedDRAList;
