const SOC = '#2185d0';
const HLGT = '#21ba45';
const HLT = '#db2828';
const PT = '#6435c9';
const LLT = '#e03997';

export default {
  SOC,
  HLGT,
  HLT,
  PT,
  LLT
};
