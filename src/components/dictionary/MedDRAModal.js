import React from 'react';
import { Modal, notification } from '../modal';
import MedDRAContainer from './MedDRAContainer';
import types from './types';
import { useFormatMessage } from '../../hooks';

const MedDRAModal = ({ isOpen, onOK }) => {
  const t = useFormatMessage();

  const getKeyValuePair = value => {
    if (value && value.length > 0) {
      const keyValuePairs = value.split('|');
      if (keyValuePairs.length > 1) {
        const code = keyValuePairs[0];
        const term = keyValuePairs[1];
        return { code, term };
      }
    } else {
      return { code: '', term: '' };
    }
  };

  const handleApplyClick = item => {
    const soc = getKeyValuePair(item[types.SOC]);
    const hlgt = getKeyValuePair(item[types.HLGT]);
    const hlt = getKeyValuePair(item[types.HLT]);
    const pt = getKeyValuePair(item[types.PT]);
    const llt = getKeyValuePair(item[types.LLT]);

    if (soc.code.length > 0 && pt.code.length > 0) {
      onOK({ soc: soc, hlgt: hlgt, hlt: hlt, pt: pt, llt: llt });
    } else {
      notification.warning({
        title: t('dictionary.required.is.soc.pt'),
        confirmButtonName: t('common.ok')
      });
    }
  };

  const handleOKClick = () => {
    onOK({});
  };

  return (
    <Modal
      open={isOpen}
      onOK={handleOKClick}
      title="MedDRA"
      content={<MedDRAContainer onApplyClick={handleApplyClick} />}
    />
  );
};

export default MedDRAModal;
