import { useCallback, useState } from 'react';
import api from '../api';

const useLLT = ({ dictionaryId }) => {
  const limit = 50;
  const [total, setTotal] = useState(0);
  const [start, setStart] = useState(0);

  const fetchItems = useCallback(
    async params => {
      const res = await api.fetchLLTList({
        dictionaryId,
        ...params
      });
      if (res && res.data) {
        return res.data;
      } else {
        return [];
      }
    },
    [dictionaryId]
  );

  const fetchRevItems = useCallback(
    async params => {
      const res = await api.fetchReverseLLTList({
        dictionaryId,
        limit,
        ...params
      });
      if (res && res.data) {
        setTotal(res.data.paging.total);
        setStart(params.offset + limit);

        return res.data.items;
      } else {
        return [];
      }
    },
    [dictionaryId]
  );

  const fetch = useCallback(
    async params => {
      if (params.keyword) {
        return await fetchRevItems({ ...params });
      } else {
        return await fetchItems({ ...params });
      }
    },
    [dictionaryId]
  );

  return { total, start, fetch };
};

export default useLLT;
