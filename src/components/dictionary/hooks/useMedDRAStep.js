const useMedDRAStep = () => {
  const steps = [
    { id: 1, name: 'SOC' },
    { id: 2, name: 'HLGT' },
    { id: 3, name: 'HLT' },
    { id: 4, name: 'PT' },
    { id: 5, name: 'LLT' }
  ];

  const orderStepByReverse = isReverse => {
    const orderedSteps = _.orderBy(
      steps,
      ['id'],
      isReverse === true ? 'desc' : 'asc'
    );
    return orderedSteps;
  };

  const next = (codeName, isReverse) => {
    const orderedSteps = orderStepByReverse(isReverse);
    const foundStep = _.find(orderedSteps, { name: codeName });

    const nextSteps = _.filter(orderedSteps, step => {
      if (
        (isReverse && foundStep.id > step.id) ||
        (isReverse === false && foundStep.id < step.id)
      ) {
        return step;
      }
    });

    if (nextSteps.length > 0) {
      return nextSteps[0].name;
    } else {
      return '';
    }
  };

  const lowSteps = (codeName, isReverse) => {
    const orderedSteps = orderStepByReverse(isReverse);
    const foundStep = _.find(orderedSteps, { name: codeName });

    const nextSteps = _.filter(orderedSteps, step => {
      if (isReverse && foundStep.id > step.id) {
        return step.name;
      }
      if (isReverse === false && foundStep.id < step.id) {
        return step.name;
      }
    });

    return nextSteps;
  };

  return { next, lowSteps };
};

export default useMedDRAStep;
