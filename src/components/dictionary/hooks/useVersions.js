import api from '../api';
import { useCallback, useEffect, useState } from 'react';

const useVersions = () => {
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(async () => {
    const res = await api.fetchDicVersionList();
    if (res && res.data) {
      res.data.forEach(item => {
        const key = item.key;
        const value = item.value;

        item.value = key;
        item.text = `MedDRA ${value}`;
      });

      setItems(res.data);
    }
  }, []);

  useEffect(() => {
    fetchItems();
  }, []);

  return { items };
};

export default useVersions;
