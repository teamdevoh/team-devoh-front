import { useCallback } from 'react';
import api from '../api';

const useSoc = ({ dictionaryId }) => {
  const fetchItems = useCallback(
    async params => {
      const res = await api.fetchSOCList({ dictionaryId, ...params });
      if (res && res.data) {
        return res.data;
      } else {
        return [];
      }
    },
    [dictionaryId]
  );

  const fetchRevItems = useCallback(
    async params => {
      const res = await api.fetchReverseSOCList({
        dictionaryId,
        ...params
      });
      if (res && res.data) {
        return res.data;
      } else {
        return [];
      }
    },
    [dictionaryId]
  );

  const fetch = useCallback(
    async params => {
      if (params.keyword) {
        return await fetchRevItems({ ...params });
      } else {
        return await fetchItems({ ...params });
      }
    },
    [dictionaryId]
  );

  return { fetch: fetch };
};

export default useSoc;
