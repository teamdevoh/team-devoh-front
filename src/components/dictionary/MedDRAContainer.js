import React, { useCallback, useEffect, useState, Fragment } from 'react';
import { useSoc, useLLT, usePT } from './hooks';
import { MedDRAList, MedDRACodeBox, MedDRASearch } from './';
import { useInView } from 'react-intersection-observer';
import { Segment } from 'semantic-ui-react';
import { LoadingBall } from '../animation';

const MedDRAContainer = ({ onApplyClick }) => {
  const [keyword, setKeyword] = useState('');
  const [dictionaryId, setDicValue] = useState(0);
  const [selectionInfo, setSelectionInfo] = useState({});
  const [ref, inView, entry] = useInView({
    root: null,
    rootMargin: '200px',
    threshold: 0
  });
  const [loading, setLoading] = useState(false);

  const [items, setItems] = useState([]);
  const soc = useSoc({ dictionaryId });
  const pt = usePT({ dictionaryId });
  const llt = useLLT({ dictionaryId });

  const fetchSoc = async () => {
    const data = await soc.fetch({ keyword });
    setItems(data);
  };

  const fetchReverse = async () => {
    setLoading(true);
    let data = await llt.fetch({ keyword, offset: 0 });
    if (data.length === 0) {
      data = await pt.fetch({ keyword, offset: 0 });
    }

    setItems(data);
    setLoading(false);
  };

  useEffect(
    () => {
      if (dictionaryId > 0) {
        if (keyword.length > 0) {
          fetchReverse();
        } else {
          fetchSoc();
        }
      }
    },
    [dictionaryId, keyword]
  );

  useEffect(
    () => {
      if (inView && entry.isIntersecting) {
        if (items.length < llt.total) {
          refetchReverse(llt);
        }
        if (items.length < pt.total) {
          refetchReverse(pt);
        }
      }
    },
    [inView]
  );

  const refetchReverse = model => {
    if (items.length < model.total) {
      setLoading(true);
      model
        .fetch({ keyword, offset: model.start })
        .then(data => {
          const merged = items.concat(data);
          setItems(merged);
          setLoading(false);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  };

  const handleSelectedItem = useCallback(breadCrumbs => {
    setSelectionInfo(breadCrumbs);
  }, []);

  return (
    <Fragment>
      <MedDRASearch
        onSearchClick={value => {
          setKeyword(value);
        }}
        onVersionChange={value => {
          setDicValue(value);
        }}
        onApplyClick={() => {
          onApplyClick(selectionInfo);
        }}
        loading={loading}
      />
      <MedDRACodeBox breadCrumbs={selectionInfo} />
      <div id="medDRAContainer">
        <MedDRAList
          keyword={keyword}
          dictionaryId={dictionaryId}
          items={items}
          onSelectedItem={handleSelectedItem}
        />
        <div ref={ref}>
          {keyword.length > 0 && (
            <Segment basic>{loading && <LoadingBall />}</Segment>
          )}
        </div>
      </div>
    </Fragment>
  );
};

export default MedDRAContainer;
