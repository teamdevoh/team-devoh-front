import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState
} from 'react';
import { Input, Button, Divider, Select } from 'semantic-ui-react';
import { useVersions } from './hooks';
import { useFormatMessage } from '../../hooks';

const MedDRASearch = ({
  loading,
  onSearchClick,
  onVersionChange,
  onApplyClick
}) => {
  const refInput = useRef();
  const t = useFormatMessage();
  const dicVersion = useVersions();
  const [dicValue, setDicValue] = useState(0);

  const handleSearchClick = () => {
    onSearchClick(refInput.current.value);
  };

  const handleVersionChange = useCallback(
    (event, data) => {
      onVersionChange(data.value);
      setDicValue(data.value);
    },
    [dicVersion.items]
  );

  const handleKeyPress = useCallback((event, data) => {
    if (event.key.toLowerCase() === 'enter') {
      handleSearchClick();
    }
  }, []);

  useEffect(
    () => {
      if (dicVersion.items.length > 0) {
        onVersionChange(dicVersion.items[0].value);
        setDicValue(dicVersion.items[0].value);
      }
    },
    [dicVersion.items]
  );

  return (
    <Fragment>
      <Input
        type="text"
        placeholder="Search..."
        action
        onKeyPress={handleKeyPress}
      >
        <input ref={refInput} />
        <Select
          compact
          options={dicVersion.items}
          value={dicValue}
          onChange={handleVersionChange}
        />
        <Button type="button" onClick={handleSearchClick} loading={loading}>
          {t('common.search')}
        </Button>
        <Button color="yellow" onClick={onApplyClick}>
          {t('common.apply')}
        </Button>
      </Input>
      <Divider />
    </Fragment>
  );
};

export default MedDRASearch;
