import helpers from '../../helpers';

const fetchDicVersionList = () => {
  return helpers.Service.get(`api/dictionaries/versions`);
};

const fetchSOCList = ({ dictionaryId }) => {
  return helpers.Service.get(`api/dictionaries/${dictionaryId}/soc`);
};

const fetchHLGTList = ({ dictionaryId, socCode }) => {
  const params = helpers.qs.stringify({
    socCode
  });
  return helpers.Service.get(`api/dictionaries/${dictionaryId}/hlgt?${params}`);
};

const fetchHLTList = ({ dictionaryId, hlgtCode }) => {
  const params = helpers.qs.stringify({
    hlgtCode
  });
  return helpers.Service.get(`api/dictionaries/${dictionaryId}/hlt?${params}`);
};

const fetchPTList = ({ dictionaryId, hltCode }) => {
  const params = helpers.qs.stringify({
    hltCode
  });
  return helpers.Service.get(`api/dictionaries/${dictionaryId}/pt?${params}`);
};

const fetchLLTList = ({ dictionaryId, ptCode }) => {
  const params = helpers.qs.stringify({
    ptCode
  });
  return helpers.Service.get(`api/dictionaries/${dictionaryId}/llt?${params}`);
};

const fetchReverseLLTList = ({ dictionaryId, keyword, offset, limit }) => {
  const params = helpers.qs.stringify({
    keyword,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/dictionaries/${dictionaryId}/llt.reverse?${params}`
  );
};

const fetchReversePTList = ({
  dictionaryId,
  keyword,
  ptCode,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    keyword,
    ptCode,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/dictionaries/${dictionaryId}/pt.reverse?${params}`
  );
};

const fetchReverseHLTList = ({
  dictionaryId,
  keyword,
  ptCode,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    keyword,
    ptCode,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/dictionaries/${dictionaryId}/hlt.reverse?${params}`
  );
};

const fetchReverseHLGTList = ({
  dictionaryId,
  keyword,
  hltCode,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    keyword,
    hltCode,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/dictionaries/${dictionaryId}/hlgt.reverse?${params}`
  );
};

const fetchReverseSOCList = ({
  dictionaryId,
  keyword,
  hltCode,
  hlgtCode,
  ptCode
}) => {
  const params = helpers.qs.stringify({
    keyword,
    hltCode,
    hlgtCode,
    ptCode
  });
  return helpers.Service.get(
    `api/dictionaries/${dictionaryId}/soc.reverse?${params}`
  );
};

export default {
  fetchDicVersionList,
  fetchSOCList,
  fetchHLGTList,
  fetchHLTList,
  fetchPTList,
  fetchLLTList,
  fetchReverseLLTList,
  fetchReversePTList,
  fetchReverseHLTList,
  fetchReverseHLGTList,
  fetchReverseSOCList
};
