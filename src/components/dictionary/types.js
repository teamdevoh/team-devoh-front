const SOC = 'SOC';
const HLGT = 'HLGT';
const HLT = 'HLT';
const PT = 'PT';
const LLT = 'LLT';

export default {
  SOC,
  HLGT,
  HLT,
  PT,
  LLT
};
