import React, { Fragment } from 'react';
import { Table } from 'semantic-ui-react';
import types from './types';
import colors from './colors';

const MedDRACodeBox = ({ breadCrumbs }) => {
  const splitCodeWithName = crumb => {
    if (crumb && crumb.indexOf('|') > -1) {
      const codeWithName = crumb.split('|');
      return { code: codeWithName[0], name: codeWithName[1] };
    }
    return { code: '', name: '' };
  };

  const soc = splitCodeWithName(breadCrumbs[types.SOC]);
  const hlgt = splitCodeWithName(breadCrumbs[types.HLGT]);
  const hlt = splitCodeWithName(breadCrumbs[types.HLT]);
  const pt = splitCodeWithName(breadCrumbs[types.PT]);
  const llt = splitCodeWithName(breadCrumbs[types.LLT]);

  return (
    <Fragment>
      <Table basic size="small" compact="very">
        <Table.Body>
          <Table.Row>
            <Table.Cell width="five">
              <span style={{ color: colors.SOC }}>SOC Code</span> {soc.code}
            </Table.Cell>
            <Table.Cell width="eleven">
              <span style={{ color: colors.SOC }}>SOC Term</span> {soc.name}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell width="five">
              <span style={{ color: colors.HLGT }}>HLGT Code</span> {hlgt.code}
            </Table.Cell>
            <Table.Cell width="eleven">
              <span style={{ color: colors.HLGT }}>HLGT Term</span> {hlgt.name}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell width="five">
              <span style={{ color: colors.HLT }}>HLT Code</span> {hlt.code}
            </Table.Cell>
            <Table.Cell width="eleven">
              <span style={{ color: colors.HLT }}>HLT Term</span> {hlt.name}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell width="five">
              <span style={{ color: colors.PT }}>PT Code</span> {pt.code}
            </Table.Cell>
            <Table.Cell width="eleven">
              <span style={{ color: colors.PT }}>PT Term</span> {pt.name}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell width="five">
              <span style={{ color: colors.LLT }}>LLT Code</span> {llt.code}
            </Table.Cell>
            <Table.Cell width="eleven">
              <span style={{ color: colors.LLT }}>LLT Term</span> {llt.name}
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
    </Fragment>
  );
};

export default MedDRACodeBox;
