import React, { useState } from 'react';
import { List, Label, Segment } from 'semantic-ui-react';
import types from './types';
import { useBoolean } from '../../hooks';
import { useMedDRAStep, useHlgt, useHlt, usePT, useLLT, useSoc } from './hooks';
import { useCallback } from 'react';
import { useEffect } from 'react';
import colors from './colors';
import LoadingBall from '../animation/LoadingBall';

const MedDRAListItem = props => {
  const { dictionaryId, keyword, breadCrumbs, item, onClick } = props;

  const visible = useBoolean(false);
  const loading = useBoolean(false);
  const [items, setItems] = useState([]);
  const soc = useSoc({ dictionaryId });
  const hlgt = useHlgt({ dictionaryId });
  const hlt = useHlt({ dictionaryId });
  const pt = usePT({ dictionaryId });
  const llt = useLLT({ dictionaryId });

  const step = useMedDRAStep();

  useEffect(
    () => {
      setItems([]);
    },
    [keyword]
  );

  const findCodeByBreadCrumbs = crumb => {
    return crumb ? crumb.split('|')[0] : '';
  };

  const handleClick = useCallback(
    async () => {
      const crumbs = {
        ...breadCrumbs,
        [item.codeName]: `${item.code}|${item.name}`
      };
      visible.toggle();

      const lowSteps = step.lowSteps(item.codeName, keyword ? true : false);
      const nextName = step.next(item.codeName, keyword ? true : false);

      _.forEach(lowSteps, type => {
        if (crumbs[type.name]) {
          crumbs[type.name] = '';
        }
      });

      onClick(crumbs);

      let socCode = crumbs[types.SOC];
      let hltCode = crumbs[types.HLT];
      let hlgtCode = crumbs[types.HLGT];
      let ptCode = crumbs[types.PT];
      let lltCode = crumbs[types.LLT];

      socCode = findCodeByBreadCrumbs(socCode);
      hltCode = findCodeByBreadCrumbs(hltCode);
      hlgtCode = findCodeByBreadCrumbs(hlgtCode);
      ptCode = findCodeByBreadCrumbs(ptCode);
      lltCode = findCodeByBreadCrumbs(lltCode);

      if (items.length === 0) {
        loading.setTrue();
        if (nextName === types.SOC) {
          setItems(await soc.fetch({ keyword, hltCode, hlgtCode, ptCode }));
        } else if (nextName === types.HLGT) {
          setItems(
            await hlgt.fetch({ keyword, socCode: socCode, hltCode: hltCode })
          );
        } else if (nextName === types.HLT) {
          setItems(
            await hlt.fetch({
              keyword,
              hlgtCode: hlgtCode,
              ptCode: ptCode
            })
          );
        } else if (nextName === types.PT) {
          setItems(
            await pt.fetch({ keyword, hltCode: hltCode, ptCode: lltCode })
          );
        } else if (nextName === types.LLT) {
          setItems(await llt.fetch({ keyword, ptCode: ptCode }));
        }
        loading.setFalse();
      }
    },
    [dictionaryId, keyword, breadCrumbs, item, onClick, items]
  );

  const buildChildren = items => {
    return (
      <List.List style={{ display: visible.value === true ? 'block' : 'none' }}>
        {items.map(child => {
          return (
            <MedDRAListItem
              key={`${child.id}${child.codeName}${child.code}`}
              {...props}
              item={child}
            />
          );
        })}
      </List.List>
    );
  };

  return (
    <List.Item>
      {item.hasChildren && (
        <List.Icon
          name={visible.value === true ? 'minus square' : 'plus square'}
          style={{ cursor: 'pointer' }}
          onClick={handleClick}
        />
      )}
      <List.Content>
        <List.Header style={{ cursor: 'pointer' }} onClick={handleClick}>
          {item.isPrimary && <Label color="blue">{item.codeName}</Label>}
          {!item.isPrimary && (
            <span style={{ color: colors[item.codeName] }}>
              {item.codeName}
            </span>
          )}{' '}
          {item.name}
        </List.Header>
        {loading.value && (
          <Segment basic>
            <LoadingBall />
          </Segment>
        )}
        {items && items.length > 0 && buildChildren(items)}
      </List.Content>
    </List.Item>
  );
};

export default MedDRAListItem;
