import React from 'react';
import { Image, Table } from 'semantic-ui-react';
import styled from 'styled-components';
import { TableWrap, StyledTh } from '../case-review/styles';
import { EmptyRowsViewForSemantic } from '../modules';
import { StyledAtag } from './KoreaHospitalSites';

const Wrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;

  & > * {
    margin-right: 8px;
  }

  & > *:last-child {
    margin-right: 0;
  }
`;

const StyledImage = styled(Image)`
  &&& {
    height: 100%;
  }
`;

const InternationalHospitalSites = () => {
  const items = [
    {
      no: 51,
      country: 'Indonesia',
      name: 'Dr Soetomo AMC Hospital',
      startDate: '2020.02.06',
      url: 'https://rsudrsoetomo.jatimprov.go.id'
    },
    {
      no: 'Intended',
      country: 'Indonesia',
      name: 'Universitas Padjadjaran',
      startDate: null,
      url: 'https://www.unpad.ac.id'
    },
    {
      no: 'Intended',
      country: 'Thailand',
      name: 'King Chulalongkorn Memorial Hospital',
      startDate: null,
      url: 'https://chulalongkornhospital.go.th/kcmh'
    },
    {
      no: 'Intended',
      country: 'Vietnam',
      name: 'National Lung Hospital',
      startDate: null,
      url: 'https://bvptw.org'
    },
    {
      no: 'Intended',
      country: 'Myanmar',
      name: 'Advanced Molecular Research Centre',
      startDate: null,
      url: 'http://www.ammrc.co.in'
    }
  ];

  const handleSiteClick = url => () => {
    window.open(url);
  };

  return (
    <Wrap>
      <StyledImage src="images/cohort_visual_02.png" alt="" size="large" />
      <TableWrap maxHeight="100%" style={{ maxWidth: '560px' }}>
        <Table selectable>
          <Table.Header>
            <Table.Row>
              <StyledTh>No</StyledTh>
              <StyledTh>Country </StyledTh>
              <StyledTh>Site </StyledTh>
              <StyledTh>Start date</StyledTh>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {items.map(item => {
              const { no, country, name, startDate, url } = item;

              return (
                <Table.Row key={no + name}>
                  <Table.Cell>{no}</Table.Cell>
                  <Table.Cell>{country}</Table.Cell>
                  <Table.Cell>
                    <StyledAtag onClick={handleSiteClick(url)}>
                      {name}
                    </StyledAtag>
                  </Table.Cell>
                  <Table.Cell>{startDate || '-'}</Table.Cell>
                </Table.Row>
              );
            })}
            {items.length === 0 && (
              <EmptyRowsViewForSemantic loading={loading} columnLength={5} />
            )}
          </Table.Body>
        </Table>
      </TableWrap>
    </Wrap>
  );
};

export default InternationalHospitalSites;
