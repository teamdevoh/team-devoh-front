import React, { Fragment } from 'react';
import { Image, Header, Divider, Responsive } from 'semantic-ui-react';
import styled from 'styled-components';
import BasicStatistics from './BasicStatistics';
import KoreaHospitalSites from './KoreaHospitalSites';
import InternationalHospitalSites from './InternationalHospitalSites';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const Root = styled.div`
  & table td {
    box-sizing: border-box;
    padding-top: 4px !important;
    padding-bottom: 4px !important;
  }
`;

const MainHeader = styled.div`
  width: 100vw;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  margin: auto;
  ${isMobile ? 'width: 100%;' : 'width: 44vw;'};
`;

const Row = styled.div`
  display: flex;
  flex-direction: ${props => {
    return !!props.direction ? props.direction : 'row';
  }};
  flex-wrap: wrap;
  justify-content: space-around;
  width: 82vw;
  margin: auto;
  ${isMobile ? '' : 'padding: 16px;'};
`;

const ContentsWrap = styled.ol`
  padding: 8px;
`;

const Title = styled.div`
  align-items: center;
  display: flex;

  ${isMobile
    ? `padding-top: 32px;
    justify-content: center;`
    : ``};
`;

const StyledHeader = styled(Header)`
  &&& {
    font-size: 24px;
    font-weight: 700;
    color: #000;

    & > span {
      font-size: 16px;
      font-weight: 400;
    }
  }
`;

const SubHeader = styled(Header)`
  &&& {
    margin-top: 0;
  }
`;

const Contents = styled.div`
  padding: 16px;
  margin-bottom: 16px;
`;

const Footer = styled.footer`
  height: 220px;
  padding: 40px 0 0 0;
  background-color: #383f48;

  display: flex;
  justify-content: center;
  color: #fff;
`;

const InnerWrap = styled.div`
  // width: 1060px;

  @media screen and (max-width: 1060px) {
    & {
      width: auto;
    }
  }
`;

const StyledDl = styled.dl`
  display: flex;
  margin: 0;
`;

const StyledAddress = styled.address`
  font-size: 14px;
  line-height: 24px;
  color: #fff;
  word-break: keep-all;

  & > span {
    display: inline-block;
    width: 20px;
  }
`;

const StyledDiv = styled.div`
  margin-top: 15px;
  font-size: 12px;
  color: rgba(255, 255, 255, 0.4);
`;

const StyledImg = styled.div`
  display: inline-block;
  width: 308px;
  height: 50px;
  background: url(images/brandCI_eng.png) 0 top no-repeat;
  background-size: 308px;
`;

const Status = () => {
  return (
    <Fragment>
      <Root>
        <MainHeader style={{ padding: '16px' }}>
          <div>
            <StyledImg />
          </div>
          <Title>
            <Header
              as="h1"
              style={{
                fontSize: '2.5rem'
              }}
            >
              cPMTb Cohort Status
            </Header>
          </Title>
        </MainHeader>
        <Divider />
        <ContentsWrap>
          <Row direction="column">
            <StyledHeader as="h3">
              1. Basic statistics of the cPMTb cohort’s status{' '}
              <span>(up to now)</span>
            </StyledHeader>
            <Contents>
              <BasicStatistics />
            </Contents>
          </Row>
          <Row direction="column">
            <StyledHeader as="h3" style={{ marginBottom: 0 }}>
              2. List of hospital sites <span>(up to now)</span>
            </StyledHeader>
            <ContentsWrap>
              <Row direction="column">
                <SubHeader as="h3">2.1 Korea hospital sites</SubHeader>
                <Contents>
                  <KoreaHospitalSites />
                </Contents>
              </Row>
              <Row direction="column">
                <SubHeader as="h3">2.2 International hospital sites</SubHeader>
                <Contents>
                  <InternationalHospitalSites />
                </Contents>
              </Row>
            </ContentsWrap>
          </Row>
        </ContentsWrap>
      </Root>
      <Footer>
        <InnerWrap>
          <StyledDl>
            <dt>
              <Image src="images/logo.png" alt="" size="small" />
            </dt>
            <dd>
              <StyledAddress>
                Bokji-ro 75, Busangjin-gu, Busan, Korea
                <br />
                Tel : 051.890.5900
                <span />
                Fax : 051.890.5915
                <span />
                Email : cpmtb.info@gmail.com
              </StyledAddress>
              <StyledDiv>
                copyright &copy; center for Personalized Precision Medicine of
                Tuberculosis(cPMTb)
              </StyledDiv>
            </dd>
          </StyledDl>
        </InnerWrap>
      </Footer>
    </Fragment>
  );
};

export default Status;
