import helpers from '../../helpers';

const fetchCohortStatus = () => {
  return helpers.Service.get(`api/cohort/status`);
};

export default {
  fetchCohortStatus
};
