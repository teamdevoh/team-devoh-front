import React, { Fragment, useEffect, useState } from 'react';
import { Table, Responsive } from 'semantic-ui-react';
import styled from 'styled-components';
import { TableWrap, StyledTh } from '../case-review/styles';
import { EmptyRowsViewForSemantic } from '../modules';
import { useCohortStatus } from './hooks';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const Sub = styled.div`
  ${isMobile ? '' : 'padding-left: 32px;'};
`;

const BasicStatistics = () => {
  const [{ item, loading }] = useCohortStatus();
  const [data, setData] = useState({});

  useEffect(
    () => {
      const newItem = { ...item };
      for (let key in item) {
        if (item.hasOwnProperty(key)) {
          const cur = item[key];
          if (!isNaN(Number(cur)) && Number(cur) !== 0) {
            let [a, b = ''] = cur.toString().split('.');
            a = a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

            newItem[key] = a + `${!!b ? '.' : ''}${b}`;
          }
        }
      }

      setData({ ...newItem });
    },
    [item]
  );

  return (
    <TableWrap maxHeight="100%" style={{ maxWidth: '600px', margin: 'auto' }}>
      <Table selectable>
        <Table.Header>
          <Table.Row>
            <StyledTh>Criteria</StyledTh>
            <StyledTh>Value </StyledTh>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {Object.keys(data).length > 0 ? (
            <Fragment>
              <Table.Row>
                <Table.Cell>
                  <b>Total subjects</b>
                </Table.Cell>
                <Table.Cell>{data.totalSubject}</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell colSpan="2">
                  <b>Basic characteristics</b>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Male</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.male_Count} {`(${data.male_Per}%)`}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Age (year)</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.age_Avg} ±{data.age_Sd}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Body weight (kg)</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.weight_Avg} ±{data.weight_Sd}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Height (cm)</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.height_Avg} ±{data.height_Sd}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Smoker</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.smoker_Count} {`(${data.smoker_Per}%)`}
                </Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell colSpan="2">
                  <b>Genotyping</b>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>NAT2 genotype/phenotype result</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.nat2_Count} {`(${data.nat2_Per}%)`}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>SLCO1B1 genotype/phenotype result</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.slc01b1_Count} {`(${data.slc01b1_Per}%)`}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Wide-genome screening array</Sub>
                </Table.Cell>
                <Table.Cell>
                  <b>Coming soon</b>
                </Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell>
                  <b>Total TB drug’s TDM points</b>
                </Table.Cell>
                <Table.Cell>{data.tdm_Point}</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell>
                  <b>Total MIC arrays</b>
                </Table.Cell>
                <Table.Cell>{data.mic_Array}</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell colSpan="2">
                  <b>Biospecimens</b>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Plasma</Sub>
                </Table.Cell>
                <Table.Cell>{data.pl_Count}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Whole Blood</Sub>
                </Table.Cell>
                <Table.Cell>{data.wb_Count}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Dried Blood Spot</Sub>
                </Table.Cell>
                <Table.Cell>{data.dbs_Count}</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell colSpan="2">
                  <b>Treatment outcome</b>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Ongoing</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.onging_Count} {`(${data.onging_Per}%)`}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Completed</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.completed_Count} {`(${data.completed_Per}%)`}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <Sub>Drop out</Sub>
                </Table.Cell>
                <Table.Cell>
                  {data.dropout_Count} {`(${data.dropout_Per}%)`}
                </Table.Cell>
              </Table.Row>
            </Fragment>
          ) : (
            <EmptyRowsViewForSemantic loading={loading} columnLength={2} />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default BasicStatistics;
