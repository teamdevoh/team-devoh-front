import { useState, useEffect } from 'react';
import api from '../api';

const useCohortStatus = () => {
  const [item, setItem] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchItem = async () => {
      setLoading(true);
      try {
        const res = await api.fetchCohortStatus();
        if (res && res.data) {
          setItem(res.data);
        }
      } catch (err) {}

      setLoading(false);
    };

    fetchItem();
  }, []);

  return [{ item, loading }];
};

export default useCohortStatus;
