import React from 'react';
import { Image, Table } from 'semantic-ui-react';
import styled from 'styled-components';
import { TableWrap, StyledTh } from '../case-review/styles';
import { EmptyRowsViewForSemantic } from '../modules';

const Wrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;

  & > * {
    margin-right: 8px;
  }

  & > *:last-child {
    margin-right: 0;
  }
`;

const StyledImage = styled(Image)`
  &&& {
    height: 100%;
  }
`;

export const StyledAtag = styled.a`
  cursor: pointer;
`;

const KoreaHospitalSites = () => {
  const items = [
    {
      no: 1,
      name: 'Inje University Busan Paik Hospital',
      startDate: '2018.07.13',
      url: 'https://www.paik.ac.kr/busan/'
    },
    {
      no: 2,
      name: 'Inje University Haeundae Paik Hospital',
      startDate: '2018.11.07',
      url: 'https://www.paik.ac.kr/haeundae/'
    },
    {
      no: 3,
      name: 'Inje University Ilsan Paik Hospital',
      startDate: '2018.10.18',
      url: 'https://www.paik.ac.kr/ilsan/'
    },
    {
      no: 4,
      name: 'Masan National Tuberculosis Hospital',
      startDate: '2018.12.27',
      url: 'http://www.mnth.go.kr/main.do'
    },
    {
      no: 5,
      name: 'Inje University Sanggye Paik Hospital',
      startDate: '2019.01.30',
      url: 'https://www.paik.ac.kr/sanggye/'
    },
    {
      no: 6,
      name: 'Inje University Seoul Paik Hospital',
      startDate: '2019.06.12',
      url: 'https://www.paik.ac.kr/seoul/main.asp'
    },
    {
      no: 7,
      name: 'Pusan National University Hospital',
      startDate: '2019.06.13',
      url: 'https://www.pnuh.or.kr/pnuh/main/main.do?rbsIdx=1'
    },
    {
      no: 8,
      name: 'Kosin University Gospel Hospital',
      startDate: '2019.06.24',
      url: 'https://www.kosinmed.or.kr/'
    },
    {
      no: 9,
      name: 'Kyungpook National University Hospital',
      startDate: '2019.09.20',
      url: 'https://www.knuh.kr/index.asp'
    },
    {
      no: 10,
      name: 'Yeungnam University Medical Center',
      startDate: '2019.09.19',
      url: 'https://yumc.ac.kr:8443/yumc/index.do'
    },
    {
      no: 11,
      name: 'Korea University Medical Center(Guro Hospital)',
      startDate: '2019.09.25',
      url: 'http://guro.kumc.or.kr/main/index.do'
    },
    {
      no: 12,
      name: 'Dong-A University Hospital',
      startDate: '2019.10.02',
      url: 'https://www.damc.or.kr/main/main_2017.php'
    },
    {
      no: 13,
      name: 'Gyeongsang National University Changwon Hospital',
      startDate: '2019.12.09',
      url: 'https://www.gnuch.co.kr/gnuh/main/main.do?rbsIdx=1'
    },
    {
      no: 14,
      name: `The Catholic University of Korea Incheon St. Mary's Hospital`,
      startDate: '2020.02.05',
      url: 'https://www.cmcism.or.kr/'
    },
    {
      no: 15,
      name: 'Kangdong Sacred Heart Hospital',
      startDate: '2020.08.19',
      url: 'https://www.kdh.or.kr:446/'
    },
    {
      no: 16,
      name: `The Catholic University of Korea Seoul St. Mary's Hospital`,
      startDate: '2020.06.08',
      url: 'https://www.cmcseoul.or.kr/page/main'
    },
    {
      no: 17,
      name: `The Catholic University of Korea Daejeon St. Mary's Hospital`,
      startDate: '2020.06.26',
      url: 'https://www.cmcdj.or.kr/main.jsp?_MOBILE_PC=Y#'
    },
    {
      no: 18,
      name: `The Catholic University of Korea Eunpyeong St. Mary's Hospital`,
      startDate: '2020.06.19',
      url: 'https://www.cmcep.or.kr/page/main'
    },
    {
      no: 19,
      name: `The Catholic University of Korea Uijeongbu St. Mary's Hospital`,
      startDate: '2020.09.03',
      url: 'https://www.cmcujb.or.kr/page/main'
    },
    {
      no: 20,
      name: `The Catholic University of Korea, ST. Vincent’s Hospital`,
      startDate: '2020.07.10',
      url: 'https://www.cmcvincent.or.kr/page/main'
    }
  ];

  const handleSiteClick = url => () => {
    window.open(url);
  };

  return (
    <Wrap>
      <StyledImage src="images/cohort_visual_01.png" alt="" size="large" />
      <TableWrap maxHeight="100%" style={{ maxWidth: '560px' }}>
        <Table selectable>
          <Table.Header>
            <Table.Row>
              <StyledTh>No</StyledTh>
              <StyledTh>Site </StyledTh>
              <StyledTh>Start date</StyledTh>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {items.map(item => {
              const { no, name, startDate, url } = item;

              return (
                <Table.Row key={no}>
                  <Table.Cell>{no}</Table.Cell>
                  <Table.Cell>
                    <StyledAtag onClick={handleSiteClick(url)}>
                      {name}
                    </StyledAtag>
                  </Table.Cell>
                  <Table.Cell>{startDate}</Table.Cell>
                </Table.Row>
              );
            })}
            {items.length === 0 && (
              <EmptyRowsViewForSemantic loading={loading} columnLength={4} />
            )}
          </Table.Body>
        </Table>
      </TableWrap>
    </Wrap>
  );
};

export default KoreaHospitalSites;
