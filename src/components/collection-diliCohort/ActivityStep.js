import React, { Fragment } from 'react';
import { Step } from 'semantic-ui-react';

const StepWrapper = ({ name, value, length, index, active, onClick }) => {
  return (
    <Step
      style={{ padding: '1em' }}
      index={index}
      value={value}
      link
      active={active}
      onClick={onClick}
    >
      <Step.Content>
        <Step.Title>
          <span>{name} </span>
          <span style={{ color: 'red' }}>{length}</span>
        </Step.Title>
      </Step.Content>
    </Step>
  );
};

const items = [
  {
    key: 0,
    text: 'All',
    value: 0,
    length: 104
  },
  {
    key: 1,
    text: 'CASE',
    value: 1,
    length: 35
  },
  {
    key: 2,
    text: 'control',
    value: 2,
    length: 69
  }
];

const ActivityStep = ({ dili, onClick }) => {
  return (
    <Fragment>
      <Step.Group size="tiny" stackable="tablet">
        {items.map((item, index) => {
          const { key, text, value, length } = item;
          return (
            <StepWrapper
              key={key}
              value={value}
              index={index}
              name={text}
              length={length}
              active={Number(dili) === value}
              onClick={onClick}
            />
          );
        })}
      </Step.Group>
    </Fragment>
  );
};

export default ActivityStep;
