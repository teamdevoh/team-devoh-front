import React, { useState } from 'react';
import DashBoard from './DashBoard';

const DashBoardContainer = () => {
  const [filter, setFilter] = useState({
    dili: 0
  });

  const handleDiliChange = (event, { name, value }) => {
    setFilter({ ...filter, dili: value });
  };

  return <DashBoard {...filter} onDiliChange={handleDiliChange} />;
};

export default DashBoardContainer;
