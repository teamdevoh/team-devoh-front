import helpers from '../../helpers';

const fetchDiliCohort = ({ dili }) => {
  const params = helpers.qs.stringify({
    dili
  });

  return helpers.Service.get(`api/subjects/dilicohort?${params}`);
};

export default {
  fetchDiliCohort
};
