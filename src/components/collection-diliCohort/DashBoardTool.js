import React, { Fragment } from 'react';
import { CSVExport } from '../export';
import { useFormatMessage } from '../../hooks';

const DashBoardTool = ({ items }) => {
  const t = useFormatMessage();

  const fields = [
    {
      key: 'subjectNo',
      label: 'Subject No'
    },
    {
      key: 'sampleName',
      label: 'Sample Name'
    },
    { key: 'sampleType', label: 'Sample Type' },
    {
      key: 'sampleAmount',
      label: 'Sample Amount'
    },
    {
      key: 'dili',
      label: 'dili'
    },
    {
      key: 'gender',
      label: t('activity.interview.sexcodeId')
    },
    {
      key: 'age',
      label: t('activity.interview.age')
    },
    {
      key: 'height',
      label: t('common.height')
    },
    {
      key: 'weight',
      label: t('common.weight')
    },
    {
      key: 'bmi',
      label: 'BMI'
    },
    {
      key: 'asT1',
      label: 'AST1'
    },
    {
      key: 'alT1',
      label: 'ALT1'
    },
    {
      key: 'bil1',
      label: 'DILI'
    },
    {
      key: 'alP1',
      label: 'ALP1'
    },
    {
      key: 'asT2',
      label: 'AST2'
    },
    {
      key: 'alT2',
      label: 'ALT2'
    },
    {
      key: 'bil2',
      label: 'bil2'
    },
    {
      key: 'alP2',
      label: 'ALP2'
    },
    {
      key: 'eos',
      label: 'EOS'
    },
    {
      key: 'eosCount',
      label: 'EOS Count'
    }
  ];

  const fieldNames = [
    'Subject No',
    'Sample Name',
    'Sample Type',
    'Sample Amount',
    'DILI',
    'Gender',
    'Age',
    'Height',
    'Weight',
    'BMI',
    'AST1',
    'ALT1',
    'bil1',
    'ALP1',
    'AST2',
    'ALT2',
    'bil2',
    'ALP2',
    'EOS',
    'EOS Count'
  ];

  return (
    <Fragment>
      <CSVExport
        data={items}
        fileName="DILI_Cohort.csv"
        fields={fields}
        fieldNames={fieldNames}
      />
    </Fragment>
  );
};

export default DashBoardTool;
