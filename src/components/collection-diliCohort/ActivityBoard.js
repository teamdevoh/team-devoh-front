import React from 'react';
import { useFormatMessage } from '../../hooks';
import { DataGrid } from '../data-grid';

const ActivityBoard = ({ items, loading }) => {
  const t = useFormatMessage();

  const columns = [
    {
      key: 'subjectNo',
      name: 'Subject No'
    },
    {
      key: 'sampleName',
      name: 'Sample Name'
    },
    { key: 'sampleType', name: 'Sample Type' },
    {
      key: 'sampleAmount',
      name: 'Sample Amount'
    },
    {
      key: 'dili',
      name: 'DILI'
    },
    {
      key: 'gender',
      name: t('activity.interview.sexcodeId')
    },
    {
      key: 'age',
      name: t('activity.interview.age')
    },
    {
      key: 'height',
      name: t('common.height')
    },
    {
      key: 'weight',
      name: t('common.weight')
    },
    {
      key: 'bmi',
      name: 'BMI'
    },
    {
      key: 'asT1',
      name: 'AST1'
    },
    {
      key: 'alT1',
      name: 'ALT1'
    },
    {
      key: 'bil1',
      name: 'bil1'
    },
    {
      key: 'alP1',
      name: 'ALP1'
    },
    {
      key: 'asT2',
      name: 'AST2'
    },
    {
      key: 'alT2',
      name: 'ALT2'
    },
    {
      key: 'bil2',
      name: 'bil2'
    },
    {
      key: 'alP2',
      name: 'ALP2'
    },
    {
      key: 'eos',
      name: 'EOS'
    },
    {
      key: 'eosCount',
      name: 'EOS Count'
    }
  ];

  return (
    <DataGrid
      columns={columns}
      rows={items}
      minHeight={640}
      rowKey="subjectNo"
      loading={loading}
    />
  );
};

export default ActivityBoard;
