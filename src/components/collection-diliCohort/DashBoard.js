import React from 'react';
import ActivityBoard from './ActivityBoard';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useDiliCohortList } from './hooks';

const DashBoard = ({ dili, onDiliChange }) => {
  const { projectId } = useParams();
  const [{ items, loading }] = useDiliCohortList({
    dili
  });

  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onDiliChange={onDiliChange}
            dili={dili}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool items={items} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <ActivityBoard items={items} loading={loading} />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
