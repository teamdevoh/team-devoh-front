import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useDiliCohortList = ({ dili }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    try {
      const res = await api.fetchDiliCohort({
        dili
      });

      if (res && res.data) {
        setItems(res.data);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      fetchItems();
    },
    [dili]
  );

  return [{ items, loading }];
};

export default useDiliCohortList;
