import React, { Fragment } from 'react';
import { Grid } from 'semantic-ui-react';
import ActivityStep from './ActivityStep';

const DashBoardHeader = ({
  projectId,
  search,
  dili,
  onDiliChange,
  onSearch
}) => {
  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <ActivityStep dili={dili} onClick={onDiliChange} />
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
