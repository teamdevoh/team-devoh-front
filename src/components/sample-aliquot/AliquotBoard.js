import React from 'react';
import { Button } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import useUser from '../account-manage/hooks/useUser';
import { DataGrid } from '../data-grid';
import { format } from '../../constants';

const AliquotBoard = ({
  items,
  loading,
  onRemove,
  setAliquotInfo,
  rowRenderer
}) => {
  const t = useFormatMessage();
  const [{ profile }] = useUser();

  const columns = [
    {
      key: 'site',
      name: 'Site'
    },
    {
      key: 'subjectNo',
      name: 'Subject',
      width: 130,
      formatter: ({ value, row }) => {
        const { initial } = row;
        return value + (!!initial ? `/${initial}` : '');
      }
    },
    {
      key: 'visit',
      name: 'Visit',
      width: 130
    },
    {
      key: 'samplingDateTime',
      name: 'Sampling',
      width: 140
    },
    {
      key: 'aliquotBarcode',
      name: 'Aliquot ID',
      width: 140,
      formatter: ({ value, row }) => (
        <a
          style={{ cursor: 'pointer' }}
          onClick={() => {
            setAliquotInfo({ ...row });
          }}
        >
          {value}
        </a>
      )
    },
    {
      key: 'aliquotAnalyte',
      name: 'Analyte'
    },
    {
      key: 'aliquotDateTime',
      name: 'Aliquot Time',
      formatter: ({ value, row }) => {
        const { currentDateTime } = row;

        return helpers.util.dateformat(
          value === null ? currentDateTime : value,
          format.YYYY_MM_DD_HHMM
        );
      }
    },

    {
      key: 'aliquotVolume',
      name: 'Volume',
      formatter: ({ value, row }) => {
        const { unit } = row;
        return `${value} ${unit}`;
      }
    },
    {
      key: 'aliquotResult',
      name: 'Result',
      width: 70
    },
    {
      key: 'aliquotMemberName',
      name: 'User',
      width: 150,
      formatter: ({ value, row }) => {
        return value || profile.name;
      }
    },
    {
      key: 'remove',
      name: 'Remove',
      width: 110,
      formatter: ({ value, row }) => {
        const { sampleId, sampleAliquotId, aliquotDateTime, barcode } = row;
        return (
          <Button
            onClick={onRemove({
              sampleId,
              sampleAliquotId,
              aliquotDateTime,
              sampleBarcode: barcode
            })}
            size="tiny"
          >
            {t('folder.remove')}
          </Button>
        );
      }
    }
  ];

  return (
    <div>
      <DataGrid
        columns={columns}
        rows={items}
        minHeight={550}
        loading={loading}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        rowKey="sampleAliquotId"
        rowRenderer={rowRenderer}
      />
    </div>
  );
};

export default AliquotBoard;
