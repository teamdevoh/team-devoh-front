import React, { useState, useEffect } from 'react';
import { Grid, Form, Divider, Responsive } from 'semantic-ui-react';
import { StyledForm } from '../sample-status/styles';
import AnalyteBoard from './AnalyteBoard';
import styled from 'styled-components';
import notification from '../modal/notification';
import { useFormatMessage } from '../../hooks';
import { useSampleInfoByBarcode, useAliquotInfoByBarcode } from './hooks';
import helpers from '../../helpers';
import AliquotForm from './AliquotForm';
import { StyledInput } from './styles';
import { format } from '../../constants';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const StyledDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const AnalyteWrap = styled.div`
  ${prop => {
    if (prop.isMobile) {
      return `
        width: 100%;
      `;
    } else {
      return `
        width: 30%;
        margin-left: 20px
      `;
    }
  }};
`;

const DashBoardHeader = ({
  sampleInfo,
  setSampleInfo,
  aliquotInfo,
  setAliquotInfo,
  aliquotItems,
  onAliquotModify,
  analyteItems,
  analyteLoading,
  sampleIdEl,
  aliquotIdEl
}) => {
  const t = useFormatMessage();
  const [sampleBarcode, setSampleBarcode] = useState('');
  const [aliquotBarcode, setAliquotBarcode] = useState('');

  const [
    { loading: sampleInfoLoading, fetch: fetchSampleInfo }
  ] = useSampleInfoByBarcode();

  const [
    { loading: aliquotInfoLoading, fetch: fetchAliquotInfo }
  ] = useAliquotInfoByBarcode();

  useEffect(() => {
    sampleIdEl.current.focus();
  }, []);

  useEffect(
    () => {
      if (!sampleInfo.sampleId) {
        setSampleBarcode('');
        sampleIdEl.current.focus();
      }
      setAliquotInfo({});
      setAliquotBarcode('');
    },
    [sampleInfo.sampleId]
  );

  const handleSampleIdKeyUp = async e => {
    const value = e.target.value.trim();

    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      if (value !== '') {
        const sampleInfo = await fetchSampleInfo(value);
        if (sampleInfo) {
          if (sampleInfo.sampleId > 0) {
            setSampleInfo(sampleInfo);
            setSampleBarcode(value);
            aliquotIdEl.current.focus();
          } else {
            // 존재하지 않는 sample id
            setSampleInfo({});
            sampleIdEl.current.focus();
            setSampleBarcode('');
            notification.warning({
              title: t('does.not.exist', { name: 'Sample ID' })
            });
          }
        }
      } else {
        notification.warning({
          title: t('empty.info', { name: 'Sample ID' })
        });
      }
    }
  };

  const handleBarcodeChange = (event, { value, name }) => {
    if (name === 'sampleId') {
      setSampleBarcode(value);
    } else if (name === 'aliquotId') {
      setAliquotBarcode(value);
    }
  };

  const handleAliquotIdKeyUp = async e => {
    const value = e.target.value.trim();

    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      if (!!!sampleInfo.sampleId) {
        sampleIdEl.current.focus();
        notification.warning({
          title: t('empty.info', { name: 'Sample ID' })
        });
      } else if (value !== '') {
        //
        const aliquotInfo = await fetchAliquotInfo({
          sampleBarcode: sampleInfo.barcode,
          aliquotBarcode: value
        });
        if (aliquotInfo) {
          if (aliquotInfo.sampleAliquotId > 0) {
            const addedAliquot = aliquotItems.filter(
              item => item.aliquotBarcode === aliquotInfo.aliquotBarcode
            );

            if (addedAliquot.length > 0) {
              notification.warning({
                title: t('added.item', { name: 'Aliquot' })
              });
            } else {
              if (aliquotInfo.aliquotDateTime) {
                // 이미 aliquot 정보가 저장된 경우

                notification.confirm({
                  // title: t('delete.message'),
                  title: t('aliquot.modify.info', {
                    memberName: aliquotInfo.aliquotMemberName,
                    date: helpers.util.dateformat(
                      aliquotInfo.aliquotDateTime,
                      format.YYYY_MM_DD_HHMM
                    )
                  }),
                  content: '',
                  onOK: async () => {
                    setAliquotInfo(aliquotInfo);
                  }
                });
              } else {
                setAliquotInfo(aliquotInfo);
              }
            }
          } else {
            // 존재하지 않는 aliquot id || sample id와 일치 하지 않음
            setAliquotInfo({});
            notification.warning({
              title: t('fetch.aliquot.error.info', { name: 'Sample ID' })
            });
          }
          setAliquotBarcode('');
          aliquotIdEl.current.focus();
        }
      } else {
        notification.warning({
          title: t('empty.info', { name: 'Aliquot ID' })
        });
      }
    }
  };

  return (
    <StyledDiv>
      <Grid style={{ flex: 1 }}>
        <Grid.Row>
          <Grid.Column>
            <StyledForm>
              <Form.Group widths="equal">
                <Form.Field required>
                  <label>Sample ID</label>
                  <StyledInput
                    size="massive"
                    name="sampleId"
                    ref={sampleIdEl}
                    onKeyUp={handleSampleIdKeyUp}
                    onChange={handleBarcodeChange}
                    value={sampleBarcode}
                    loading={sampleInfoLoading}
                  />
                </Form.Field>
                <Form.Field required>
                  <label>Aliquot ID</label>
                  <StyledInput
                    size="massive"
                    name="aliquotId"
                    ref={aliquotIdEl}
                    onKeyUp={handleAliquotIdKeyUp}
                    onChange={handleBarcodeChange}
                    value={aliquotBarcode}
                    loading={aliquotInfoLoading}
                  />
                </Form.Field>
              </Form.Group>
            </StyledForm>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <StyledForm>
              <Form.Group widths="equal">
                <Form.Field>
                  <label>Project</label>
                  {sampleInfo.title || ''}
                </Form.Field>
                <Form.Field>
                  <label>Site</label>
                  {sampleInfo.site || ''}
                </Form.Field>
              </Form.Group>
              <Form.Group widths="equal">
                <Form.Field>
                  <label>Subject</label>
                  {sampleInfo.subjectNo || ''} / {sampleInfo.initial || ''}
                </Form.Field>
                <Form.Field>
                  <label>Visit</label>
                  {sampleInfo.visit || ''}
                </Form.Field>
                <Form.Field>
                  <label>Sampling</label>
                  {helpers.util.dateformat(
                    sampleInfo.samplingDateTime || '',
                    format.YYYY_MM_DD
                  )}
                </Form.Field>
              </Form.Group>
            </StyledForm>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <AnalyteWrap isMobile={isMobile}>
        <AnalyteBoard items={analyteItems} loading={analyteLoading} />
      </AnalyteWrap>
      <Grid style={{ flex: 1 }}>
        <Grid.Column>
          <Divider />
          <AliquotForm
            aliquotInfo={aliquotInfo}
            onAliquotModify={onAliquotModify}
          />
        </Grid.Column>
      </Grid>
    </StyledDiv>
  );
};

export default DashBoardHeader;
