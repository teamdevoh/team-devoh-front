import { Input } from 'semantic-ui-react';
import styled from 'styled-components';

export const StyledInput = styled(Input)`
  &&& {
    & > input {
      background-color: #fbbd08 !important;
    }
  }
`;
