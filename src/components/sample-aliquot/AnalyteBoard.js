import React, { Fragment } from 'react';
import styled from 'styled-components';
import { DataGrid } from '../data-grid';

const Highlight = styled.span`
  color: #db2828;
`;

const Aliquots = ({ value, row }) => {
  const { aliquots } = row;
  return (
    <Fragment>
      <Highlight>{aliquots}</Highlight> / {value}
    </Fragment>
  );
};

const AnalyteBoard = ({ items, loading }) => {
  const columns = [
    {
      key: 'aliquotAnalyte',
      name: 'Analyte'
    },
    {
      key: 'aliquotVolume',
      name: 'Volume',
      formatter: ({ value, row }) => {
        const { aliquotUnit } = row;
        return `${value} ${aliquotUnit}`;
      }
    },
    {
      key: 'aliquotCount',
      name: 'Aliquots',
      formatter: ({ value, row }) => {
        return Aliquots({ value, row });
      }
    }
  ];

  return (
    <DataGrid
      columns={columns}
      rows={items}
      rowKey="projectSampleAliquotId"
      minHeight={242}
      loading={loading}
    />
  );
};

export default AnalyteBoard;
