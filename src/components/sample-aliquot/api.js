import helpers from '../../helpers';

const fetchSampleInfoByBarcode = ({ sampleBarcode }) => {
  return helpers.Service.get(`api/samples/barcodes/${sampleBarcode}`);
};

const fetchSampleAliquotInfoListByBarcode = ({ sampleBarcode }) => {
  return helpers.Service.get(`api/samples/barcodes/${sampleBarcode}/aliquot`);
};

const fetchAliquotInfoByBarcode = ({ sampleBarcode, aliquotBarcode }) => {
  return helpers.Service.get(
    `api/samples/barcodes/${sampleBarcode}/aliquot/barcodes/${aliquotBarcode}`
  );
};

const updateAliquotInfo = ({ sampleId, model }) => {
  return helpers.Service.put(`api/samples/${sampleId}/aliquot`, model);
};

const removeAliquotInfo = ({ sampleId, sampleAliquotId }) => {
  return helpers.Service.delete(
    `api/samples/${sampleId}/aliquot/${sampleAliquotId}`
  );
};

export default {
  fetchSampleInfoByBarcode,
  fetchSampleAliquotInfoListByBarcode,
  fetchAliquotInfoByBarcode,
  updateAliquotInfo,
  removeAliquotInfo
};
