import React, { useEffect, useCallback } from 'react';
import { Form, Input, Button } from 'semantic-ui-react';
import { StyledForm } from '../sample-status/styles';
import styled from 'styled-components';
import notification from '../modal/notification';
import { useFormatMessage, useFormFields, useSubmit } from '../../hooks';
import helpers from '../../helpers';
import { DateTimePicker } from '../datepicker';
import { format } from '../../constants';

const FlexFormField = styled(Form.Field)`
  &&& {
    display: flex;
    align-items: center;
  }
`;

const AliquotIdWrap = styled.div`
  padding: 0.67857143em 1em;
  font-weight: 600;
  color: #db2828;
`;

const DashBoardHeader = ({ aliquotInfo, onAliquotModify }) => {
  const t = useFormatMessage();
  const [{ model, onChange }, setValues] = useFormFields({
    ...aliquotInfo
  });

  useEffect(
    () => {
      setValues({ ...aliquotInfo });
    },
    [aliquotInfo.sampleAliquotId]
  );

  const handleAliquotDateTimeChange = (e, { name, value }) => {
    const date = new Date(value);
    const utcDate = new Date(
      date.getUTCFullYear(),
      date.getUTCMonth(),
      date.getUTCDate(),
      date.getUTCHours(),
      date.getUTCMinutes()
    );
    const newAliquotDate = helpers.util.dateformat(
      utcDate,
      format.YYYY_MM_DD_HHMM
    );

    onChange(e, { name, value: newAliquotDate });
  };

  const handleSubmit = () => {
    if (model.sampleAliquotId) {
      onAliquotModify(model);
    } else {
      notification.warning({
        title: t('plz.enter.barcode')
      });
    }
  };

  const getLocalTime = useCallback(
    () => {
      const { aliquotDateTime, currentDateTime } = model;

      return (
        model.sampleAliquotId &&
        helpers.util.dateformat(
          aliquotDateTime === null ? currentDateTime : aliquotDateTime,
          format.YYYY_MM_DD_HHMM
        )
      );
    },
    [model.aliquotDateTime]
  );

  const [{ validator, onValidate }] = useSubmit(handleSubmit);

  return (
    <StyledForm onSubmit={onValidate}>
      <Form.Group widths="equal">
        <FlexFormField inline>
          <label>Aliquot ID / Analyte</label>
          <AliquotIdWrap>
            {aliquotInfo.aliquotBarcode || ''} /{' '}
            {aliquotInfo.aliquotAnalyte || ''}
          </AliquotIdWrap>
        </FlexFormField>
        <Form.Field inline required>
          <label>Aliquot Time</label>
          <DateTimePicker
            name="aliquotDateTime"
            onChange={handleAliquotDateTimeChange}
            value={getLocalTime()}
          />
        </Form.Field>
        <Form.Field inline required>
          <label>Volume</label>
          <Input
            name="aliquotVolume"
            value={model.aliquotVolume || ''}
            label={{
              basic: true,
              content: `${aliquotInfo.unit || 'ml'}`
            }}
            labelPosition="right"
            onChange={onChange}
          />
          {validator.message('Volume', model.aliquotVolume, 'required')}
        </Form.Field>
        <Form.Field inline required>
          <label>Result</label>
          <Input
            name="aliquotResult"
            value={model.aliquotResult || ''}
            onChange={onChange}
          />
          {validator.message('Result', model.aliquotResult, 'required')}
        </Form.Field>
        <Form.Field inline width="4" style={{ display: 'flex' }}>
          <Button primary style={{ marginLeft: 'auto' }}>
            {t('common.modify')}
          </Button>
        </Form.Field>
      </Form.Group>
    </StyledForm>
  );
};

export default DashBoardHeader;
