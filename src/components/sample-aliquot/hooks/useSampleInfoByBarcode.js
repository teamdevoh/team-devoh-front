import { useState } from 'react';
import api from '../api';

const useSampleInfoByBarcode = () => {
  const [loading, setLoading] = useState(false);

  const fetchItem = async sampleBarcode => {
    setLoading(true);
    try {
      const res = await api.fetchSampleInfoByBarcode({
        sampleBarcode: sampleBarcode.trim()
      });
      if (res && res.data) {
        return res.data;
      } else {
        return res;
      }
    } finally {
      setLoading(false);
    }
  };

  return [{ loading, fetch: fetchItem }];
};

export default useSampleInfoByBarcode;
