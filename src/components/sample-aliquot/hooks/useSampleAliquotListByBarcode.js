import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useSampleAliquotListByBarcode = ({ sampleBarcode }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async sampleBarcode => {
    setLoading(true);
    if (sampleBarcode) {
      const res = await api.fetchSampleAliquotInfoListByBarcode({
        sampleBarcode
      });

      if (res && res.data) {
        setItems(res.data);
      }
    } else {
      setItems([]);
    }
    setLoading(false);
  });

  useEffect(
    () => {
      fetchItems(sampleBarcode);
    },
    [sampleBarcode]
  );

  return [{ items, loading, fetch: fetchItems }];
};

export default useSampleAliquotListByBarcode;
