import { useState } from 'react';
import api from '../api';

const useAliquotInfoByBarcode = () => {
  const [loading, setLoading] = useState(false);

  const fetchItem = async ({ sampleBarcode, aliquotBarcode }) => {
    setLoading(true);
    try {
      const res = await api.fetchAliquotInfoByBarcode({
        sampleBarcode,
        aliquotBarcode
      });
      if (res && res.data) {
        return res.data;
      } else {
        return res;
      }
    } finally {
      setLoading(false);
    }
  };

  return [{ loading, fetch: fetchItem }];
};

export default useAliquotInfoByBarcode;
