import React, { useState, useEffect, useRef } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import AliquotBoard from './AliquotBoard';
import DashBoardTool from './DashBoardTool';
import DashBoardHeader from './DashBoardHeader';
import notification from '../modal/notification';
import { useFormatMessage } from '../../hooks';
import api from './api';
import { useSampleAliquotListByBarcode } from './hooks';
import { GridRowWrapper } from '../collection-tbdrug/styles';

const DashBoard = () => {
  const t = useFormatMessage();
  const sampleIdEl = useRef(null);
  const aliquotIdEl = useRef(null);
  const [sampleInfo, setSampleInfo] = useState({});
  const [aliquotInfo, setAliquotInfo] = useState({});
  const [aliquotItems, setAliquotItems] = useState([]);
  const [removeAliquotId, setRemoveAliquotId] = useState();

  const [
    { items: analyteItems, loading: analyteLoading, fetch: fetchAnalyteItems }
  ] = useSampleAliquotListByBarcode({
    sampleBarcode: sampleInfo.sampleId > 0 && sampleInfo.barcode
  });

  useEffect(
    () => {
      if (sampleInfo.barcode) {
        setAliquotItems([]);
      } else {
        setAliquotInfo({});
        setAliquotItems([]);
      }
    },
    [sampleInfo.barcode]
  );

  useEffect(
    () => {
      if (aliquotInfo.aliquotBarcode) {
        const addedAliquot = aliquotItems.filter(
          item => item.aliquotBarcode === aliquotInfo.aliquotBarcode
        );

        if (addedAliquot.length === 0) {
          setAliquotItems(aliquotItems.concat([{ ...aliquotInfo }]));
        }
      }
    },
    [aliquotInfo.aliquotBarcode]
  );

  useEffect(
    () => {
      if (removeAliquotId) {
        const newItems = aliquotItems.filter(
          item => removeAliquotId !== item.sampleAliquotId
        );

        setAliquotItems(newItems);
        setAliquotInfo({});
        setRemoveAliquotId();
        aliquotIdEl.current.focus();
      }
    },
    [removeAliquotId]
  );

  const handleAliquotRemove = ({
    sampleId,
    sampleAliquotId,
    aliquotDateTime,
    sampleBarcode
  }) => () => {
    if (aliquotDateTime) {
      // 기존 정보
      notification.confirm({
        title: t('remove.aliquot.already.exists'),
        content: '',
        onOK: async () => {
          try {
            const res = await api.removeAliquotInfo({
              sampleId,
              sampleAliquotId
            });
            if (res && res.data) {
              notification.success({
                title: t('common.ok'),
                onClose: () => {}
              });
              fetchAnalyteItems(sampleBarcode);
              setRemoveAliquotId(sampleAliquotId);
            }
          } catch (error) {}
        }
      });
    } else {
      setRemoveAliquotId(sampleAliquotId);
    }
  };

  const handleAliquotModify = newAliquotInfo => {
    const { sampleAliquotId } = newAliquotInfo;

    const newItems = aliquotItems.map(item => {
      if (item.sampleAliquotId === sampleAliquotId) {
        return newAliquotInfo;
      } else {
        return item;
      }
    });

    setAliquotItems(newItems);
  };

  const handleSaveAliquotClick = async () => {
    sampleIdEl.current.focus();
    const model = aliquotItems.map(item => {
      const {
        sampleAliquotId,
        aliquotDateTime,
        aliquotMemberId,
        aliquotVolume,
        aliquotResult
      } = item;
      return {
        sampleAliquotId,
        aliquotDateTime,
        aliquotMemberId,
        aliquotVolume,
        aliquotResult
      };
    });
    try {
      const res = await api.updateAliquotInfo({
        sampleId: sampleInfo.sampleId,
        model
      });

      if (res && res.data) {
        notification.success({
          title: t('common.ok'),
          onClose: () => {}
        });
        setSampleInfo({});
      }
    } catch (error) {
      console.log(error);
    }
  };

  /**
   * 선택된 aliquot 표시
   */
  const RowRenderer =
    // (highlightId) =>
    ({ renderBaseRow, ...props }) => {
      const { sampleAliquotId } = props.row;

      return (
        <GridRowWrapper
          // choosed={sampleAliquotId === highlightId}
          choosed={sampleAliquotId === aliquotInfo.sampleAliquotId}
        >
          {renderBaseRow(props)}
        </GridRowWrapper>
      );
    };

  return (
    <Segment basic>
      <Grid.Row>
        <Grid.Column width={16}>
          <DashBoardHeader
            sampleInfo={sampleInfo}
            setSampleInfo={setSampleInfo}
            aliquotInfo={aliquotInfo}
            setAliquotInfo={setAliquotInfo}
            aliquotItems={aliquotItems}
            onAliquotModify={handleAliquotModify}
            analyteItems={analyteItems}
            analyteLoading={analyteLoading}
            sampleIdEl={sampleIdEl}
            aliquotIdEl={aliquotIdEl}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool items={aliquotItems} onSave={handleSaveAliquotClick} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <AliquotBoard
            items={aliquotItems}
            onRemove={handleAliquotRemove}
            setAliquotInfo={setAliquotInfo}
            rowRenderer={RowRenderer}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
