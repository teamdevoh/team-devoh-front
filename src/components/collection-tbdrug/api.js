import helpers from '../../helpers';

const fetchTbdrugActivitiesBySort = ({
  projectId,
  projectMemberId,
  search,
  order = 'asc'
}) => {
  const params = helpers.qs.stringify({
    projectMemberId,
    search,
    order
  });

  return helpers.Service.get(
    `api/projects/${projectId}/tbdrugs.status.sort?${params}&$orderBy=shortName ${order}`
  );
};

const fetchReferences = ({ search, offset, limit }) => {
  const params = helpers.qs.stringify({
    search: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(`api/tbdrugs/references?${params}`);
};

const existReference = ({ title, pmid, referenceId }) => {
  const params = helpers.qs.stringify({
    title,
    pmid,
    referenceId
  });
  return helpers.Service.get(`api/tbdrugs/references.exist?${params}`);
};

const fetchReference = ({ referenceId }) => {
  return helpers.Service.get(`api/tbdrugs/references/${referenceId}`);
};

const fetchReferencesByIds = ({ referenceIds }) => {
  const params = helpers.qs.stringify({
    referenceIds
  });
  return helpers.Service.get(`api/tbdrugs/references.info?${params}`);
};

const fetchTbdrug = ({ tbdrugId }) => {
  return helpers.Service.get(`api/tbdrugs/${tbdrugId}`);
};

const addTbdrug = ({ model }) => {
  return helpers.Service.post(`api/tbdrug`, model);
};

const updateTbdrug = ({ tbdrugId, model }) => {
  return helpers.Service.put(`api/tbdrugs/${tbdrugId}`, model);
};

const fetchTbdrugCount = ({ projectId, projectMemberId, search }) => {
  const params = helpers.qs.stringify({
    projectMemberId,
    search
  });

  return helpers.Service.get(
    `api/projects/${projectId}/tbdrugs.status.count?${params}`
  );
};

const fetchTbdrugs = ({ offset, limit } = {}) => {
  const params = helpers.qs.stringify({
    offset,
    limit
  });
  return helpers.Service.get(`api/tbdrugs?${params}`);
};

const addReference = ({ model }) => {
  return helpers.Service.post(`api/tbdrugs/reference`, model);
};

const updateReference = ({ referenceId, model }) => {
  return helpers.Service.put(`api/tbdrugs/references/${referenceId}`, model);
};

const fetchReferenceActivities = ({ projectId, referenceId }) => {
  const params = helpers.qs.stringify({
    projectId
  });

  return helpers.Service.get(
    `api/tbdrugs/references/${referenceId}/activities?${params}`
  );
};

const removeReference = ({ projectId, referenceId }) => {
  const params = helpers.qs.stringify({
    projectId
  });

  return helpers.Service.delete(
    `api/tbdrugs/references/${referenceId}?${params}`
  );
};

const fetchReferenceAttachments = ({ referenceId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    referenceId,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/tbdrugs/references/${referenceId}/attach?${params}`
  );
};

const removeReferenceAttach = ({ referenceId, referenceAttachmentId }) => {
  return helpers.Service.delete(
    `api/tbdrugs/references/${referenceId}/attach/${referenceAttachmentId}`
  );
};

const addReferenceAttach = ({ referenceId, model }) => {
  return helpers.Service.post(
    `api/tbdrugs/references/${referenceId}/attach`,
    model
  );
};

const fetchArchivePresignedurl = ({ referenceId, referenceAttachmentId }) => {
  return helpers.Service.get(
    `api/tbdrugs/references/${referenceId}/attach/${referenceAttachmentId}/req.presignedurl`
  );
};

export default {
  fetchTbdrugActivitiesBySort,
  fetchReferences,
  existReference,
  fetchReference,
  fetchReferencesByIds,
  fetchTbdrug,
  addTbdrug,
  updateTbdrug,
  fetchTbdrugCount,
  fetchTbdrugs,
  addReference,
  updateReference,
  fetchReferenceActivities,
  removeReference,
  fetchReferenceAttachments,
  removeReferenceAttach,
  addReferenceAttach,
  fetchArchivePresignedurl
};
