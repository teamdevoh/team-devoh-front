import { asyncComponent } from '../modules';

const DashBoard = asyncComponent(() => import('./DashBoard'));
const SearchReferenceInput = asyncComponent(() =>
  import('./SearchReferenceInput')
);
const SearchReference = asyncComponent(() => import('./SearchReference'));

export { DashBoard, SearchReferenceInput, SearchReference };
