import React, { Fragment } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Icon, Popup } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { SortButton } from '../button';

const DashBoardTool = ({ onRequestSort, order }) => {
  const t = useFormatMessage();
  const history = useHistory();
  const { projectId } = useParams();

  const handleAddClick = () => {
    history.push(`/project/${projectId}/collection/tbdrug/0`);
  };

  return (
    <Fragment>
      <SortButton onClick={onRequestSort} curOrder={order} order="asc" />
      {' | '}
      <SortButton onClick={onRequestSort} curOrder={order} order="desc" />
      {' | '}
      <Popup
        trigger={
          <Icon
            name="plus"
            size="large"
            className="link"
            onClick={handleAddClick}
          />
        }
        content={t('add.new.tbdrug')}
        inverted
      />
    </Fragment>
  );
};

export default DashBoardTool;
