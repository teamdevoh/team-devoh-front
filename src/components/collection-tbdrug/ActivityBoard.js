import React from 'react';
import helpers from '../../helpers';
import { useParams } from 'react-router-dom';
import SummaryWrapper from './SummaryWrapper';
import { DataGrid } from '../data-grid';

const ActivityBoard = ({ items, activityCount, loading }) => {
  const { projectId } = useParams();

  const columns = [
    {
      key: 'shortName',
      name: 'Short',
      width: 90,
      formatter: ({ value, isScrolling, row }) => (
        <a
          style={{ cursor: 'pointer' }}
          onClick={() => {
            helpers.history.push(
              `/project/${projectId}/collection/tbdrug/${row.tbdrugId}`
            );
          }}
        >
          {value}
        </a>
      )
    },
    {
      key: 'genericName',
      name: 'Generic name'
    },
    {
      key: 'author',
      name: 'Charge',
      width: 130
    },
    { key: 'drugBankId', name: 'Drug Bank Id', width: 100 },
    {
      key: 'metabolite',
      name: 'Metabolite',
      width: 110
    },
    {
      key: 'adme',
      name: `ADME (${activityCount.adme})`,
      formatter: SummaryWrapper,
      hidden: activityCount.adme > 0 ? false : true,
      width: activityCount.adme > 0 ? 120 : -1
    },
    {
      key: 'pkData',
      name: `PK Data (${activityCount.pkData})`,
      formatter: SummaryWrapper,
      hidden: activityCount.pkData > 0 ? false : true,
      width: activityCount.pkData > 0 ? 120 : -1
    },
    {
      key: 'factorsAffecting',
      name: `Factors affecting (${activityCount.factorsAffecting})`,
      formatter: SummaryWrapper,
      hidden: activityCount.factorsAffecting > 0 ? false : true,
      width: activityCount.factorsAffecting > 0 ? 145 : -1
    },
    {
      key: 'pGxInteractions',
      name: `PGx interactions (${activityCount.pGxInteractions})`,
      formatter: SummaryWrapper,
      hidden: activityCount.pGxInteractions > 0 ? false : true,
      width: activityCount.pGxInteractions > 0 ? 140 : -1
    },
    {
      key: 'pdData',
      name: `PD Data (${activityCount.pdData})`,
      formatter: SummaryWrapper,
      hidden: activityCount.pdData > 0 ? false : true,
      width: activityCount.pdData > 0 ? 120 : -1
    },
    {
      key: 'ddi',
      name: `DDI (${activityCount.ddi})`,
      formatter: SummaryWrapper,
      hidden: activityCount.ddi > 0 ? false : true,
      width: activityCount.ddi > 0 ? 120 : -1
    },
    {
      key: 'toxicity',
      name: `Toxicity (${activityCount.toxicity})`,
      formatter: SummaryWrapper,
      hidden: activityCount.toxicity > 0 ? false : true,
      width: activityCount.toxicity > 0 ? 120 : -1
    },
    {
      key: 'model',
      name: `Modeling (${activityCount.model})`,
      formatter: SummaryWrapper,
      hidden: activityCount.model > 0 ? false : true,
      width: activityCount.model > 0 ? 120 : -1
    }
  ];

  return (
    <DataGrid
      columns={columns}
      rows={items}
      rowKey="tbdrugId"
      loading={loading}
      minHeight={650}
    />
  );
};

export default ActivityBoard;
