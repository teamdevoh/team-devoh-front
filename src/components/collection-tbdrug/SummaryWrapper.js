import React from 'react';
import ActivitySummary from '../collection/ActivitySummary';
import helpers from '../../helpers';
import { useParams } from 'react-router-dom';

const SummaryWrapper = props => {
  const { row } = props;
  const { projectId } = useParams();

  const handleStatusClick = () => {
    helpers.history.push(
      `/project/${projectId}/collection/tbdrug/${row.tbdrugId}/activitygroup/0`
    );
  };

  const handleModalStatusClick = item => () => {
    helpers.history.push(
      `/project/${projectId}/collection/tbdrug/${
        row.tbdrugId
      }/activitygroup/0?projectActivityId=${
        item.projectActivityId
      }&activityId=${item.activityId}`
    );
  };

  return (
    <ActivitySummary
      {...props}
      rowMasterKey="tbdrugId"
      modalTitle={`${row.genericName}${
        row.metabolite !== '' ? `-${row.metabolite}` : ''
      }`}
      onStatusClick={handleStatusClick}
      onModalStatusClick={handleModalStatusClick}
    />
  );
};
export default SummaryWrapper;
