import React, { useEffect } from 'react';
import { Dropdown, Button } from 'semantic-ui-react';
import { useProjectMemberKeyPair } from '../../hooks';
import { useTbdrugList } from './hooks';
import styled from 'styled-components';
import CopyToClipboard from '../modules/CopyToClipboard';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { useBuild } from '../activity/hooks';

const DisabledStyleDropdown = styled(Dropdown)`
  &&&.disabled {
    opacity: 0.8;
    pointer-events: auto;
    & > * {
      cursor: no-drop !important;
    }
  }
`;

const StyledButton = styled(Button)`
  &&& {
    min-width: 95px;
    & span {
      font-size: 1rem !important;
    }
  }
`;

const FlexDiv = styled.div`
  display: flex;
`;

const MemberList = ({ projectId, value, onChange, fluid = true }) => {
  const t = useFormatMessage();
  const member = useProjectMemberKeyPair({
    projectId,
    holder: t('common.all')
  });

  return (
    <Dropdown
      placeholder=""
      selection
      options={member.items}
      onChange={onChange}
      // defaultValue={0}
      value={value || 0}
      fluid={fluid}
    />
  );
};

const TbdrugList = ({
  value,
  onChange,
  defaultOption = [],
  hasCopyButton = false,
  defaultFirstValue = false,
  name,
  ...rest
}) => {
  const build = useBuild();
  const t = useFormatMessage();
  const [drugs] = useTbdrugList();
  const copyText = build.displayName({
    source: drugs.items,
    value
  });

  useEffect(
    () => {
      if (defaultFirstValue && drugs.items.length > 0 && value === null) {
        onChange(null, { name, value: drugs.items[0].value });
      }
    },
    [drugs.items, defaultFirstValue, value]
  );

  const TbdrugDropdown = (
    <DisabledStyleDropdown
      placeholder=""
      fluid
      search
      selection
      name={name}
      options={[...defaultOption, ...drugs.items]}
      onChange={onChange}
      value={value}
      {...rest}
    />
  );

  if (hasCopyButton) {
    return (
      <FlexDiv>
        <StyledButton type="button">
          <CopyToClipboard text={copyText} title={t('common.copy')} />
        </StyledButton>
        {TbdrugDropdown}
      </FlexDiv>
    );
  } else {
    return TbdrugDropdown;
  }
};

export { MemberList, TbdrugList };
