import React from 'react';
import { Container, Header } from 'semantic-ui-react';
import TbdrugForm from './TbdrugForm';
import { useParams } from 'react-router-dom';

const TbdrugContainer = () => {
  const { projectId, subjectId: tbdrugId } = useParams();

  return (
    <Container className="margin-0">
      <Header as="h3" dividing>
        TBDrug
      </Header>
      <TbdrugForm projectId={projectId} tbdrugId={tbdrugId} />
    </Container>
  );
};

export default TbdrugContainer;
