import React from 'react';
import { Table, Popup, Button, Icon } from 'semantic-ui-react';
import { TbdrugList } from './Selector';
import { useFormatMessage } from '../../hooks';

const ActivityHeader = ({
  onTbdrugChange,
  tbdrugId,
  genericName,
  shortName,
  drugBankId,
  metabolite
}) => {
  const t = useFormatMessage();

  const handleOpenDrugBanck = () => {
    window.open(`https://www.drugbank.ca/drugs/${drugBankId}`, '_blank');
  };

  return (
    <Table celled compact="very" size="small">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell width={3}>Drug</Table.HeaderCell>
          <Table.HeaderCell>Generic Name</Table.HeaderCell>
          <Table.HeaderCell>Short Name</Table.HeaderCell>
          <Table.HeaderCell>Drug Bank Id</Table.HeaderCell>
          <Table.HeaderCell>Metabolite</Table.HeaderCell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>
            <TbdrugList fluid value={tbdrugId} onChange={onTbdrugChange} />
          </Table.Cell>
          <Table.Cell>{genericName}</Table.Cell>
          <Table.Cell>{shortName}</Table.Cell>
          <Table.Cell>
            {drugBankId}
            <Popup
              trigger={
                <Button
                  icon
                  onClick={handleOpenDrugBanck}
                  style={{ margin: 0, background: 'transparent' }}
                  role="button"
                  circular
                >
                  <Icon name="globe" />
                </Button>
              }
              content={t('open.drug.bank')}
              inverted
            />
          </Table.Cell>
          <Table.Cell>{metabolite}</Table.Cell>
        </Table.Row>
      </Table.Header>
    </Table>
  );
};

export default ActivityHeader;
