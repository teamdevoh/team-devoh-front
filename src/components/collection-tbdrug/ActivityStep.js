import React, { useCallback, Fragment } from 'react';
import { Step } from 'semantic-ui-react';
import { useTbdrugCount } from './hooks';
import { useBoolean } from '../../hooks';
import ReferenceModalContainer from './ReferenceModalContainer';

const StepWrapper = ({ name, value, length, index, active, onClick }) => {
  return (
    <Step
      style={{ padding: '1em' }}
      index={index}
      value={value}
      link
      active={active}
      onClick={onClick}
    >
      <Step.Content>
        <Step.Title>
          <span>{name} </span>
          <span style={{ color: 'red' }}>{length}</span>
        </Step.Title>
      </Step.Content>
    </Step>
  );
};

const ActivityStep = ({ projectId, projectMemberId, search }) => {
  const [items] = useTbdrugCount({
    projectId,
    projectMemberId,
    search
  });
  const showModal = useBoolean(false);

  const handleClick = useCallback((event, { value }) => {
    if (value === 'reference') {
      showModal.setTrue();
    }
  });

  return (
    <Fragment>
      <Step.Group size="tiny" stackable="tablet">
        {items.map((item, index) => {
          return (
            <StepWrapper
              key={item.value}
              value={item.value}
              index={index}
              name={item.name}
              length={item.length}
              active={item.active}
              onClick={handleClick}
            />
          );
        })}
      </Step.Group>
      <ReferenceModalContainer
        showModal={showModal}
        searchVal=""
        onClose={showModal.setFalse}
      />
    </Fragment>
  );
};

export default ActivityStep;
