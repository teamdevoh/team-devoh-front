import React, { useState } from 'react';
import TbdrugRoutes from './TbdrugRoutes';
// import { useParams } from 'react-router-dom';

const DashBoardContainer = () => {
  // const { projectId } = useParams();
  const [filter, setFilter] = useState({
    projectMemberId: 0,
    search: ''
  });

  const handleMemberChange = (event, { name, value }) => {
    setFilter({ ...filter, projectMemberId: value });
  };

  const handleSearch = value => {
    setFilter({ ...filter, search: value });
  };

  return (
    <TbdrugRoutes
      filter={filter}
      onMemberChange={handleMemberChange}
      onSearch={handleSearch}
    />
  );
};

export default DashBoardContainer;
