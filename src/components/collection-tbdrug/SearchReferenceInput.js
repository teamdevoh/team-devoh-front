import React, { useState, Fragment, useEffect } from 'react';
import { Input, Icon, Button, Popup } from 'semantic-ui-react';
import { useBoolean, useFormatMessage } from '../../hooks';
import styled from 'styled-components';
import ReferenceModalContainer from './ReferenceModalContainer';

const FlexDiv = styled.div`
  display: flex;
`;

const StyledInput = styled(Input)`
  &&& {
    & > input {
      background-color: #8bb7e121;
    }
  }
`;

const SearchReferenceInput = ({
  searchValue = '',
  value,
  name,
  removeBtnTitle = '',
  label = 'Reference',
  onChange = f => f
}) => {
  const t = useFormatMessage();
  const showModal = useBoolean(false);
  const [searchVal, setSearchVal] = useState(searchValue);

  useEffect(
    () => {
      setSearchVal(searchValue);
    },
    [searchValue]
  );

  const handleOpenModal = async () => {
    showModal.setTrue();
  };

  const handleSearchInpuChange = (event, { value }) => {
    setSearchVal(value);
  };

  const handleKeyDown = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      event.preventDefault();
      showModal.setTrue();
    }
  };

  const handleSelecte = data => {
    setSearchVal(data.title);
  };

  const handleRemove = () => {
    onChange(null, { name, value: null });
  };

  return (
    <Fragment>
      <FlexDiv>
        <StyledInput
          label={label}
          icon={
            <Icon
              name="search"
              onClick={handleOpenModal}
              inverted
              circular
              link
            />
          }
          fluid
          placeholder="Search"
          onChange={handleSearchInpuChange}
          onKeyDown={handleKeyDown}
          value={searchVal}
          readOnly={!!value}
        />
        <Popup
          trigger={
            <Button
              icon
              disabled={!!!value}
              onClick={handleRemove}
              style={{ margin: 0 }}
            >
              <Icon name="x" />
            </Button>
          }
          content={t('remove.reference')}
          inverted
        />
      </FlexDiv>
      <ReferenceModalContainer
        showModal={showModal}
        searchVal={searchVal}
        onSelecte={handleSelecte}
        onClose={showModal.setFalse}
        name={name}
        value={value}
        onChange={onChange}
      />
    </Fragment>
  );
};

export default SearchReferenceInput;
