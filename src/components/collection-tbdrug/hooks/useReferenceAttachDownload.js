import { useState, useCallback } from 'react';
import helpers from '../../../helpers';

const useReferenceAttachDownload = referenceId => {
  const [loading, setLoading] = useState(false);

  const download = useCallback(async referenceAttachmentId => {
    setLoading(true);
    helpers.util
      .download(
        `api/tbdrugs/references/${referenceId}/attach/${referenceAttachmentId}`,
        percent => {
          if (percent === 100) {
            setLoading(false);
          }
        }
      )
      .catch(err => {
        setLoading(false);
      });
  }, []);

  return { download, loading };
};

export default useReferenceAttachDownload;
