import { useEffect, useState, useRef } from 'react';
import api from '../api';

const useReferences = (search = '') => {
  search = search.trim();
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);

  const [isDo, setIsDo] = useState();

  // api 중복 호출을 막기위한 로컬 변수
  const preSearchVal = useRef(null);

  const setPreSearchVal = newSearchVal => {
    preSearchVal.current = newSearchVal;
  };

  const fetchItems = async ({ offset }) => {
    setLoading(true);
    setPreSearchVal(search);
    const res = await api.fetchReferences({ offset, search });
    setItems(res.data.items);

    setLoading(false);
  };

  useEffect(
    () => {
      if (search !== preSearchVal.current) {
        fetchItems({ offset: 0 });
      }
    },
    [isDo]
  );

  return [{ items, loading }, setIsDo];
};

export default useReferences;
