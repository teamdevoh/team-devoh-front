import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useTbdrugCount = ({ projectId, projectMemberId, search }) => {
  const [status, setStatus] = useState([]);

  const fetchItem = useCallback(async () => {
    const res = await api.fetchTbdrugCount({
      projectId,
      projectMemberId,
      search
    });
    if (res && res.data) {
      const items = [
        {
          name: `Drug`,
          value: 'drug',
          length: res.data.drug,
          active: false
        },
        {
          name: `Reference`,
          value: 'reference',
          length: res.data.reference,
          active: false
        }
      ];

      setStatus(items);
    }
  });

  useEffect(
    () => {
      fetchItem();
    },
    [projectId, projectMemberId, search]
  );

  return [status, setStatus];
};

export default useTbdrugCount;
