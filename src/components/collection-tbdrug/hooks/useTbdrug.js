import { useState, useEffect } from 'react';
import { useFormFields } from '../../../hooks';
import api from '../api';

const initial = {
  genericName: '',
  shortName: '',
  drugBankId: '',
  metabolite: '',
  metaboliteReferenceId: '',
  atccode: null,
  isExdrug: null,
  remark: ''
};

const useTbdrug = tbdrugId => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields(initial);

  const fetchItem = async () => {
    setLoading(true);
    const res = await api.fetchTbdrug({ tbdrugId });
    setLoading(false);
    if (res && res.data) {
      setModel(res.data);
    }
  };

  const add = async () => {
    setLoading(true);
    const res = await api.addTbdrug({ model });
    setLoading(false);
    return res.data;
  };

  const edit = async () => {
    setLoading(true);
    const res = await api.updateTbdrug({ tbdrugId: model.tbdrugId, model });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  useEffect(
    () => {
      if (tbdrugId > 0) {
        fetchItem();
      } else {
        setModel(initial);
      }
    },
    [tbdrugId]
  );

  return [{ model, loading, onChange, onAdd: add, onEdit: edit }];
};

export default useTbdrug;
