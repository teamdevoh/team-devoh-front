import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useTbdrugActivityStatus = ({
  projectId,
  projectMemberId,
  search,
  order
}) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [activityCount, setActivityCount] = useState({
    adme: 0,
    pkData: 0,
    factorsAffecting: 0,
    pGxInteractions: 0,
    pdData: 0,
    ddi: 0,
    toxicity: 0
  });

  const fetchItems = useCallback(async () => {
    setLoading(true);
    const param = {
      projectId,
      projectMemberId,
      search,
      order
    };
    try {
      const res = await api.fetchTbdrugActivitiesBySort(param);

      if (res && res.data) {
        if (res.data.length > 0) {
          const firstRow = res.data[0];
          setActivityCount({
            adme: firstRow.adme.count,
            pkData: firstRow.pkData.count,
            factorsAffecting: firstRow.factorsAffecting.count,
            pGxInteractions: firstRow.pGxInteractions.count,
            pdData: firstRow.pdData.count,
            ddi: firstRow.ddi.count,
            toxicity: firstRow.toxicity.count,
            model: firstRow.model.count
          });
        }

        setItems(res.data);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, projectMemberId, search, order]
  );

  return [{ items, activityCount, loading }];
};

export default useTbdrugActivityStatus;
