import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import { generateDrugData } from './useTbdrugList';

const useExTbdrugList = ({ showOnlyShortName = false } = {}) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(async ({ offset }) => {
    setLoading(true);
    const res = await api.fetchTbdrugs();
    if (res && res.data) {
      const options = [];

      for (let i = 0; i < res.data.items.length; i++) {
        const item = res.data.items[i];

        if (item.isExdrug) {
          options.push(generateDrugData({ item, showOnlyShortName }));
        }
      }

      setLoading(false);
      setItems(options);
    }
  }, []);

  useEffect(() => {
    fetchItems({ offset: 0 });
  }, []);

  return [{ items, loading }];
};

export default useExTbdrugList;
