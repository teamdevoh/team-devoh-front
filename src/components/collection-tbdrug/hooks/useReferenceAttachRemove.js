import { useState } from 'react';
import api from '../api';

const useReferenceAttachRemove = referenceId => {
  const [loading, setLoading] = useState(false);

  const remove = async referenceAttachmentId => {
    setLoading(true);
    const res = await api.removeReferenceAttach({
      referenceId,
      referenceAttachmentId
    });
    setLoading(true);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  return { remove, loading };
};

export default useReferenceAttachRemove;
