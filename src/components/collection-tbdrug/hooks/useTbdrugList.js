import { useCallback, useEffect, useState } from 'react';
import api from '../api';

export const generateDrugData = ({ item, showOnlyShortName = false }) => {
  const { tbdrugId, genericName, shortName, isExdrug, atccode } = item;
  const metabolite = isExdrug ? '' : item.metabolite;

  return {
    text: showOnlyShortName
      ? `${shortName}${metabolite !== '' ? `-${metabolite}` : ''}`
      : `${genericName}${metabolite !== '' ? `-${metabolite}` : ''}`,
    value: tbdrugId,
    atccode
  };
};

const useTbdrugList = ({ showOnlyShortName = false } = {}) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(async ({ offset }) => {
    setLoading(true);
    const res = await api.fetchTbdrugs();
    if (res && res.data) {
      const options = res.data.items.map(item => {
        return generateDrugData({ item, showOnlyShortName });
      });

      setLoading(false);
      setItems(options);
    }
  }, []);

  useEffect(() => {
    fetchItems({ offset: 0 });
  }, []);

  return [{ items, loading }];
};

export default useTbdrugList;
