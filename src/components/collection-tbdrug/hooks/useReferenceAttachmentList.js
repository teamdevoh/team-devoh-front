import { useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';

const useReferenceAttachmentList = referenceId => {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchReferenceAttachments({ referenceId });
    let items = [];
    await res.data.forEach(async item => {
      const fileObj = await helpers.file.setFileInfo({
        ...item,
        id: item.referenceAttachmentId,
        thumbnailApiUrl: `api/tbdrugs/references/${referenceId}/attach.thumbnail/${
          item.referenceAttachmentId
        }`
      });
      items.push(fileObj);
    });

    setItems(items);
    setLoading(false);
  };

  useEffect(
    () => {
      if (referenceId > 0) {
        fetchItems();
      }
    },
    [referenceId]
  );

  return [{ items, loading }, setItems];
};

export default useReferenceAttachmentList;
