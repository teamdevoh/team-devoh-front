import { useState, useEffect } from 'react';
import { useFormFields } from '../../../hooks';
import api from '../api';

const initial = {
  pmid: '',
  authors: '',
  title: '',
  journal: '',
  year: '',
  volume: '',
  pages: '',
  remark: '',
  fileStorageIds: []
};

const useReference = (referenceId, projectId) => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields(initial);

  const fetchItem = async () => {
    setLoading(true);
    const res = await api.fetchReference({ referenceId });
    setLoading(false);
    if (res && res.data) {
      setModel(res.data);
    }
  };

  const add = async () => {
    setLoading(true);
    const res = await api.addReference({ model });
    setLoading(false);
    return res.data;
  };

  const edit = async () => {
    setLoading(true);
    const res = await api.updateReference({
      referenceId: model.referenceId,
      model
    });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  const remove = async () => {
    setLoading(true);
    const res = await api.removeReference({
      projectId,
      referenceId: model.referenceId
    });
    setLoading(false);
    if (res && res.data) {
      return res.data;
    }

    return false;
  };

  const checkRemove = async () => {
    setLoading(true);
    let list = [];
    let isOk = false;

    try {
      const res = await api.fetchReferenceActivities({
        projectId,
        referenceId: model.referenceId
      });
      if (res && res.data && res.data.length > 0) {
        list = res.data;
      } else {
        isOk = await remove();
      }
    } catch (error) {}
    setLoading(false);

    return { isOk, list };
  };

  useEffect(
    () => {
      if (referenceId > 0) {
        fetchItem();
      } else {
        setModel(initial);
      }
    },
    [referenceId]
  );

  return [
    {
      model,
      loading,
      onChange,
      onAdd: add,
      onEdit: edit,
      onRemove: checkRemove
    }
  ];
};

export default useReference;
