import React, { Fragment } from 'react';
import { useParams, useLocation } from 'react-router-dom';
import ActivityHeader from './ActivityHeader';
import { useTbdrug } from './hooks';
import ActivityMenu from './ActivityMenu';
import helpers from '../../helpers';

const TbdrugActivityContainer = () => {
  const { projectId, subjectId: tbdrugId, activityGroupId } = useParams();
  const search = useLocation().search;
  const qs = helpers.qs.parse(search);
  const [{ model: curTbdrug }] = useTbdrug(tbdrugId);

  const handleChangeTbdrug = (event, { value }) => {
    helpers.history.push(
      `/project/${projectId}/collection/tbdrug/${value}/activitygroup/${activityGroupId}`
    );
  };

  return (
    <Fragment>
      <ActivityHeader
        tbdrugId={tbdrugId}
        onTbdrugChange={handleChangeTbdrug}
        {...curTbdrug}
      />
      <ActivityMenu
        projectId={projectId}
        projectActivityId={qs.projectActivityId}
        activityGroupId={activityGroupId}
        tbdrugId={tbdrugId}
        {...curTbdrug}
      />
    </Fragment>
  );
};

export default TbdrugActivityContainer;
