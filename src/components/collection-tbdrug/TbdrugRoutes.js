import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { asyncComponent } from '../../components/modules';
import { Authorized } from '../../components/auth';
import { role } from '../../constants';

const CollectionTbdrugDataDashBoard = asyncComponent(() =>
  import('./DashBoard')
);
const TbdrugContainer = asyncComponent(() => import('./TbdrugContainer'));
const TbdrugActivityContainer = asyncComponent(() =>
  import('./TbdrugActivityContainer')
);

const TbdrugRoutes = ({ filter, onMemberChange, onSearch }) => {
  return (
    <Switch>
      <Route
        exact
        path="/project/:projectId/collection/tbdrug"
        render={props => {
          const Component = Authorized(
            CollectionTbdrugDataDashBoard,
            role.Activity_Read
          );
          return (
            <Component
              {...props}
              {...filter}
              onMemberChange={onMemberChange}
              onSearch={onSearch}
            />
          );
        }}
      />
      <Route
        exact
        path="/project/:projectId/collection/tbdrug/:subjectId"
        component={TbdrugContainer}
      />
      <Route
        path="/project/:projectId/collection/tbdrug/:subjectId/activitygroup/:activityGroupId"
        component={TbdrugActivityContainer}
      />
    </Switch>
  );
};

export default TbdrugRoutes;
