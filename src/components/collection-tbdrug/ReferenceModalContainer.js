import React, { useState, useEffect } from 'react';
import { Modal } from '../modal';
import { useBoolean, useFormatMessage } from '../../hooks';
import SearchReference from './SearchReference';
import ReferenceForm from './ReferenceForm';
import { useParams } from 'react-router-dom';

const ReferenceModalContainer = ({ showModal, ...rest }) => {
  const t = useFormatMessage();
  const { projectId } = useParams();
  const showReferenceForm = useBoolean(false);
  const [referenceId, setReferenceId] = useState(null);

  const handleBackClick = () => {
    showReferenceForm.setFalse();
  };

  const handleShowReferenceForm = referenceId => () => {
    setReferenceId(referenceId);
    showReferenceForm.setTrue();
  };

  useEffect(
    () => {
      if (!showModal.value) {
        showReferenceForm.setFalse();
      }
    },
    [showModal.value]
  );

  return (
    <Modal
      size="large"
      open={showModal.value}
      onOK={showModal.setFalse}
      title={
        showReferenceForm.value
          ? referenceId
            ? t('edit.reference')
            : t('add.reference')
          : 'Reference Search'
      }
      content={
        showReferenceForm.value ? (
          <ReferenceForm
            onBackClick={handleBackClick}
            referenceId={referenceId}
            projectId={projectId}
            setReferenceId={setReferenceId}
          />
        ) : (
          <SearchReference
            onShowReferenceForm={handleShowReferenceForm}
            {...rest}
          />
        )
      }
    />
  );
};

export default ReferenceModalContainer;
