import React from 'react';
import { Grid } from 'semantic-ui-react';
import { MemberList } from './Selector';
import SearchInput from '../input/SearchInput';
import ActivityStep from './ActivityStep';

const DashBoardHeader = ({
  projectId,
  search,
  projectMemberId,
  onMemberChange,
  onSearch
}) => {
  return (
    <Grid stackable>
      <Grid.Column width="4">
        <MemberList
          projectId={projectId}
          value={projectMemberId}
          onChange={onMemberChange}
        />
      </Grid.Column>
      <Grid.Column width="4">
        <SearchInput value={search} onClick={onSearch} />
      </Grid.Column>
      <Grid.Column width="8">
        <ActivityStep
          projectId={projectId}
          projectMemberId={projectMemberId}
          search={search}
        />
      </Grid.Column>
    </Grid>
  );
};

export default DashBoardHeader;
