import styled from 'styled-components';

export const GridRowWrapper = styled.div`
  cursor: pointer;
  ${props => {
    const { selected, choosed } = props;
    let result = '';

    if (selected) {
      result += `
        & .react-grid-Cell {
          background-color: #f8ffff;
        }`;
    }

    if (choosed) {
      result += `
        & .react-grid-Cell {
          background-color: aliceblue !important;
      `;
    }
    return result;
  }};
`;
