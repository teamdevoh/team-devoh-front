import React, { useRef, memo, useState, Fragment } from 'react';
import {
  Divider,
  Form,
  Input,
  TextArea,
  Container,
  Button,
  Table,
  Message
} from 'semantic-ui-react';
import { LinkButton } from '../button';
import useReference from './hooks/useReference';
import { SaveButton, RemoveButton } from '../button';
import { useFormatMessage, useSubmit, useBoolean } from '../../hooks';
import { notification } from '../modal';
import ReferenceFileUpload from './ReferenceFileUpload';
import { role } from '../../constants';
import api from './api';
import { Modal } from '../modal';

const ActivityList = ({ items }) => {
  return (
    <Table celled>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Activity</Table.HeaderCell>
          <Table.HeaderCell>TBDrug Name</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {items.map(item => {
          const { name, genericName } = item;

          return (
            <Table.Row key={`${name}-${genericName}`}>
              <Table.Cell>{name}</Table.Cell>
              <Table.Cell>{genericName}</Table.Cell>
            </Table.Row>
          );
        })}
      </Table.Body>
    </Table>
  );
};

const UploadWrapper = memo(({ referenceId, onChange }) => {
  const t = useFormatMessage();
  const isOpen = useBoolean(false);

  return (
    <Form.Field>
      <LinkButton
        color="twitter"
        icon="cloud upload"
        labelPosition="right"
        name={t('common.add.attachment')}
        onClick={isOpen.setTrue}
        type="button"
      />
      <ReferenceFileUpload
        isUpload={isOpen.value}
        onUploadClose={isOpen.setFalse}
        referenceId={referenceId}
        onChange={onChange}
      />
    </Form.Field>
  );
});

const ReferenceForm = ({
  projectId,
  referenceId,
  onBackClick,
  setReferenceId
}) => {
  const t = useFormatMessage();
  const [error, setError] = useState({
    pmid: false,
    title: false
  });
  const [{ model, loading, onChange, onAdd, onEdit, onRemove }] = useReference(
    Number(referenceId),
    projectId
  );
  const showReferenceActivity = useBoolean(false);
  const [usingActivityList, setUsingActivityList] = useState([]);

  const saveAndMove = useRef(false);

  const setSaveAndMove = val => {
    saveAndMove.current = val;
  };

  const getSaveAndMove = () => {
    return saveAndMove.current;
  };

  const handleMovePage = () => {
    const move = getSaveAndMove();
    if (move) {
      onBackClick();
    }
  };

  const handleSubmit = async () => {
    if (Number(referenceId) > 0) {
      const is = await onEdit();
      if (is) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: handleMovePage()
        });
      }
    } else {
      const newReferenceId = await onAdd();
      if (newReferenceId) {
        notification.success({
          title: t('common.alert.saved'),
          onClose: handleMovePage()
        });

        setReferenceId(newReferenceId);
      }
    }
  };

  const checkExistingReference = async () => {
    const mPmid = model.pmid || '';
    const mTitle = model.title || '';

    if (mPmid !== '' || mTitle !== '') {
      const res = await api.existReference({
        title: model.title,
        pmid: model.pmid,
        referenceId: model.referenceId
      });

      if (res.data) {
        const newError = {
          pmid: false,
          title: false
        };
        const pmid = res.data.pmid || '';
        const title = res.data.title || '';

        if (
          pmid !== '' &&
          pmid.toLocaleLowerCase().trim() === mPmid.toLocaleLowerCase().trim()
        ) {
          newError.pmid = true;
        }

        if (
          title.toLocaleLowerCase().trim() === mTitle.toLocaleLowerCase().trim()
        ) {
          newError.title = true;
        }

        setError({
          ...newError
        });
      } else {
        setError({
          pmid: false,
          title: false
        });
      }

      return res.data;
    } else {
      return false;
    }
  };

  const handleSaveClick = (saveAndMove = false) => async e => {
    e.preventDefault();
    setSaveAndMove(saveAndMove);
    const existing = await checkExistingReference();
    if (!existing) {
      onValidate();
    }
  };

  const handleBlur = async () => {
    await checkExistingReference();
  };

  const handleRemoveClick = () => {
    notification.confirm({
      title: t('delete.message'),
      content: '',
      onOK: async () => {
        const { isOk, list } = await onRemove();

        if (isOk) {
          setSaveAndMove(true);
          notification.info({
            title: t('common.alert.deleted'),
            onClose: handleMovePage,
            confirmButtonName: t('common.ok')
          });
        } else {
          showReferenceActivity.setTrue();
          setUsingActivityList(list);
        }
      }
    });
  };

  const [{ validator, onValidate }] = useSubmit(handleSubmit);

  return (
    <Fragment>
      <Container>
        <Form>
          <Form.Group widths="equal">
            <Form.Field
              control={Input}
              label="PMID"
              name="pmid"
              value={model.pmid || ''}
              onChange={onChange}
              onBlur={handleBlur}
              error={error.pmid}
            />
            <Form.Field>
              <label>1st Authors</label>
              <Input
                name="authors"
                value={model.authors || ''}
                onChange={onChange}
              />
            </Form.Field>
          </Form.Group>
          {error.pmid && (
            <span style={{ color: 'red' }}>{t('already.exists')}</span>
          )}
          <Form.Field
            required
            control={Input}
            label="Title"
            name="title"
            value={model.title || ''}
            onChange={onChange}
            onBlur={handleBlur}
            error={error.title}
          />
          {error.title && (
            <span style={{ color: 'red' }}>{t('already.exists')}</span>
          )}
          {validator.message('Title', model.title, 'required')}
          <Form.Group widths="equal">
            <Form.Field>
              <label>Journal</label>
              <Input
                name="journal"
                value={model.journal || ''}
                onChange={onChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Year</label>
              <Input name="year" value={model.year || ''} onChange={onChange} />
            </Form.Field>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field>
              <label>Volume</label>
              <Input
                name="volume"
                value={model.volume || ''}
                onChange={onChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Pages</label>
              <Input
                name="pages"
                value={model.pages || ''}
                onChange={onChange}
              />
            </Form.Field>
          </Form.Group>
          <Form.Field>
            <label>Remark</label>
            <TextArea
              name="remark"
              value={model.remark || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Divider />
          <Button
            type="button"
            content={t('common.back')}
            onClick={onBackClick}
          />
          <SaveButton
            allowedRole={role.Reference_Edit}
            name={t('common.save')}
            loading={loading}
            onClick={handleSaveClick(false)}
          />
          <SaveButton
            allowedRole={role.Reference_Edit}
            name={t('common.save.move')}
            loading={loading}
            onClick={handleSaveClick(true)}
          />
          {model.referenceId > 0 && (
            <RemoveButton
              allowedRole={role.Reference_Edit}
              name={t('common.remove')}
              onClick={handleRemoveClick}
              disabled={loading}
            />
          )}
          <Divider />
          <UploadWrapper referenceId={referenceId} onChange={onChange} />
        </Form>
      </Container>
      <Modal
        size="small"
        open={showReferenceActivity.value}
        onOK={showReferenceActivity.setFalse}
        title={t('activity.list.using.reference')}
        content={
          <Fragment>
            <Message warning>{t('using.reference.info')}</Message>
            <ActivityList items={usingActivityList} />
          </Fragment>
        }
      />
    </Fragment>
  );
};

export default ReferenceForm;
