import React, { useCallback, useEffect } from 'react';
import { Grid, Menu, Select, Icon, Loader } from 'semantic-ui-react';
import { useLocation } from 'react-router-dom';
import { FormContainer } from '../activity';
import helpers from '../../helpers';
import {
  useActivityGroupList,
  useActivitiesOfGroupList
} from '../collection/hooks';
import {
  IconTemp,
  IconDone,
  IconNone,
  Review,
  IconNote,
  IconQueryOpen,
  IconQueryAnswer,
  IconQueryClose
} from '../collection/Status';
import { code } from '../../constants';

const addOption = arr => {
  const newArray = [...arr];
  newArray.unshift({
    text: '---------------------------------------',
    value: 0
  });

  return newArray;
};

const ActivityMenu = props => {
  const { projectId, projectActivityId, activityGroupId, tbdrugId } = props;
  const location = useLocation();
  const [activityGroups] = useActivityGroupList({ projectId });
  const query = helpers.qs.parse(location.search);
  const [{ items, clearGroups, fetch, loading }] = useActivitiesOfGroupList({
    activityGroupId,
    subjectId: tbdrugId
  });

  const goActivity = ({ projectActivityId, activityId }) => {
    helpers.history.push(
      `/project/${projectId}/collection/tbdrug/${tbdrugId}/activitygroup/${activityGroupId}?projectActivityId=${projectActivityId}&activityId=${activityId}`
    );
  };

  const handleItemClick = useCallback(
    (event, data) => {
      const item = data.item;
      goActivity({
        projectActivityId: item.projectActivityId,
        activityId: item.activityId
      });
    },
    [activityGroupId, tbdrugId]
  );

  const handleClickBackToList = useCallback(
    () => {
      helpers.history.push(`/project/${projectId}/collection/tbdrug`);
    },
    [projectId]
  );

  useEffect(
    () => {
      if (items.length > 0 && tbdrugId > 0) {
        // For default select item
        if (projectActivityId === void 0) {
          const firstItem = items[0];
          goActivity({
            projectActivityId: firstItem.projectActivityId,
            activityId: firstItem.activityId
          });
        }
      }
    },
    [items]
  );

  const handleGroupChange = useCallback(
    (event, data) => {
      clearGroups();
      helpers.history.push(
        `/project/${projectId}/collection/tbdrug/${tbdrugId}/activitygroup/${
          data.value
        }`
      );
    },
    [projectId, tbdrugId]
  );

  useEffect(
    () => {
      return clearGroups;
    },
    [projectId]
  );

  return (
    <Grid stackable>
      <Grid.Column width={4}>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Select
                fluid
                options={addOption(activityGroups)}
                value={Number(activityGroupId)}
                onChange={handleGroupChange}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              {loading && <Loader active />}
              <Menu fluid vertical tabular stackable>
                {items.map(item => {
                  return (
                    <Menu.Item
                      key={item.projectActivityId}
                      active={
                        Number(projectActivityId) === item.projectActivityId
                      }
                      item={item}
                      onClick={handleItemClick}
                    >
                      <Grid>
                        <Grid.Column width="ten">{item.name}</Grid.Column>
                        <Grid.Column width="six">
                          {item.activityStatusCodeId === code.Temp && (
                            <IconTemp />
                          )}
                          {item.activityStatusCodeId === code.Done && (
                            <IconDone />
                          )}
                          {item.activityStatusCodeId === code.FirstSign && (
                            <span style={{ float: 'right' }}>
                              <Review number={1} />
                            </span>
                          )}
                          {item.activityStatusCodeId === code.SecondSign && (
                            <span style={{ float: 'right' }}>
                              <Review number={2} />
                            </span>
                          )}
                          {item.activityStatusCodeId === code.ThirdSign && (
                            <span style={{ float: 'right' }}>
                              <Review number={3} />
                            </span>
                          )}
                          {item.activityStatusCodeId === 0 && <IconNone />}
                          {item.queryStatusCodeId === code.QueryOpen && (
                            <IconQueryOpen />
                          )}
                          {item.queryStatusCodeId === code.QueryAnswer && (
                            <IconQueryAnswer />
                          )}
                          {item.queryStatusCodeId === code.QueryClose && (
                            <IconQueryClose />
                          )}
                          {item.hasNote && <IconNote />}
                        </Grid.Column>
                      </Grid>
                    </Menu.Item>
                  );
                })}
              </Menu>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Column>

      <Grid.Column width={12}>
        <a onClick={handleClickBackToList} style={{ cursor: 'pointer' }}>
          <Icon name="arrow left" />
          Go back to Collection List
        </a>
        {Number(query.activityId) > 0 && (
          <FormContainer
            activityId={Number(query.activityId)}
            fetchActivityMenu={fetch}
            {...props}
          />
        )}
      </Grid.Column>
    </Grid>
  );
};

export default ActivityMenu;
