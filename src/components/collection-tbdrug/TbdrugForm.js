import React, { useState, useRef, useEffect } from 'react';
import {
  Divider,
  Form,
  Input,
  TextArea,
  Container,
  Icon,
  Button,
  Popup,
  Checkbox
} from 'semantic-ui-react';
import { BackButton, SaveButton } from '../button';
import { useFormatMessage, useSubmit } from '../../hooks';
import { useHistory } from 'react-router-dom';
import { notification } from '../modal';
import { useTbdrug } from './hooks';
import api from './api';
import styled from 'styled-components';
import SearchReferenceInput from './SearchReferenceInput';
import { role } from '../../constants';

const FlexDiv = styled.div`
  display: flex;
`;

const TbdrugForm = ({ projectId, tbdrugId }) => {
  const t = useFormatMessage();
  const history = useHistory();
  const [{ model, loading, onChange, onAdd, onEdit }] = useTbdrug(
    Number(tbdrugId)
  );
  const [searchVal, setSearchVal] = useState('');
  const saveAndMove = useRef(false);

  const setSaveAndMove = val => {
    saveAndMove.current = val;
  };

  const getSaveAndMove = () => {
    return saveAndMove.current;
  };

  useEffect(
    () => {
      if (model.metaboliteReferenceId) {
        api
          .fetchReference({ referenceId: model.metaboliteReferenceId })
          .then(res => {
            setSearchVal(res.data.title);
          });
      } else {
        setSearchVal('');
      }
    },
    [model.metaboliteReferenceId]
  );

  const handleMovePage = tbdrugId => () => {
    const move = getSaveAndMove();
    if (move) {
      history.push(
        `/project/${projectId}/collection/tbdrug/${tbdrugId}/activitygroup/0`
      );
    } else {
      history.push(`/project/${projectId}/collection/tbdrug/${tbdrugId}`);
    }
  };

  const handleSubmit = async () => {
    if (Number(tbdrugId) > 0) {
      const is = await onEdit();
      if (is) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: handleMovePage(tbdrugId)
        });
      }
    } else {
      const newTbdrugId = await onAdd();
      if (newTbdrugId) {
        notification.success({
          title: t('common.alert.saved'),
          onClose: handleMovePage(newTbdrugId)
        });
      }
    }
  };

  const handleSaveClick = (saveAndMove = false) => () => {
    setSaveAndMove(saveAndMove);
    onValidate();
  };

  const handleOpenDrugBanck = () => {
    window.open(`https://www.drugbank.ca/drugs/${model.drugBankId}`, '_blank');
  };

  const handleOnCheckbox = (e, { name, checked }) => {
    onChange(e, { name, value: checked });
  };

  const [{ validator, onValidate }] = useSubmit(handleSubmit);

  return (
    <Container>
      <Form>
        <Form.Field required>
          <label>Generic Name</label>
          <Input
            name="genericName"
            value={model.genericName}
            onChange={onChange}
          />
          {validator.message('Generic Name', model.genericName, 'required')}
        </Form.Field>
        <Form.Field required>
          <label>Short Name</label>
          <Input name="shortName" value={model.shortName} onChange={onChange} />
          {validator.message('Short Name', model.shortName, 'required')}
        </Form.Field>
        <Form.Field required>
          <label>Drug Bank Id</label>
          <FlexDiv>
            <Input
              fluid
              name="drugBankId"
              value={model.drugBankId}
              onChange={onChange}
            />
            <Popup
              trigger={
                <Button
                  icon
                  onClick={handleOpenDrugBanck}
                  style={{ margin: 0 }}
                  role="button"
                >
                  <Icon name="globe" />
                </Button>
              }
              content={t('open.drug.bank')}
              inverted
            />
          </FlexDiv>
          {validator.message('Drug Bank Id', model.drugBankId, 'required')}
        </Form.Field>
        <Form.Field>
          <label>Metabolite</label>
          <Input
            name="metabolite"
            value={model.metabolite}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <SearchReferenceInput
            searchValue={searchVal}
            name="metaboliteReferenceId"
            value={model.metaboliteReferenceId}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>ATC Code</label>
          <Input
            name="atccode"
            value={model.atccode || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field
          inline
          style={{
            display: 'flex',
            alignItems: 'center'
          }}
        >
          <label>Main Drug</label>
          <Checkbox
            name="isExdrug"
            checked={!!model.isExdrug || false}
            onChange={handleOnCheckbox}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('remark')}</label>
          <TextArea name="remark" value={model.remark} onChange={onChange} />
        </Form.Field>
        <Divider />
        <BackButton name={t('common.back')} />
        <SaveButton
          allowedRole={role.Subject_Edit}
          name={t('common.save')}
          loading={loading}
          onClick={handleSaveClick(false)}
        />
        <SaveButton
          allowedRole={role.Subject_Edit}
          name={t('common.save.move')}
          loading={loading}
          onClick={handleSaveClick(true)}
        />
      </Form>
    </Container>
  );
};

export default TbdrugForm;
