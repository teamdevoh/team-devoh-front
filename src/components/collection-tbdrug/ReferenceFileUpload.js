import React, { useCallback, memo, useState, useEffect } from 'react';
import api from './api';
import helpers from '../../helpers';
import { UploadLayer, FileList, FakeFileList } from '../modules';
import { Dimmer, Loader, Segment, Button } from 'semantic-ui-react';
import { useUpload, useFormatMessage } from '../../hooks';
import toArray from 'lodash/toArray';
import { useUser } from '../account-manage/hooks';
import {
  useReferenceAttachmentList,
  useReferenceAttachDownload,
  useReferenceAttachRemove
} from './hooks';
import styled from 'styled-components';
import MultiDownloadButton from '../modules/MultiDownloadButton';
import FileViewerWrapper from '../viewer/FileViewerWrapper';

const OptionButton = styled(Button)`
  &&& {
    padding: 6px;
    background-color: transparent;
  }
`;

const FileDeleteButton = ({ onDeleteClick }) => (
  <OptionButton icon="delete" onClick={onDeleteClick} />
);

const ReferenceFileUpload = ({
  referenceId,
  isUpload,
  onUploadClose,
  onChange
}) => {
  const t = useFormatMessage();
  const file = useUpload();
  const [my] = useUser();
  const [attach, setFiles] = useReferenceAttachmentList(referenceId);
  const attachDown = useReferenceAttachDownload(referenceId);
  const attachRemove = useReferenceAttachRemove(referenceId);
  const [newFileStorageIds, setNewFileStorageIds] = useState(new Set());
  const [fileInfo, setFileInfo] = useState();
  const [viewerOpen, setViewerOpen] = useState(false);

  const handleRemoveAttach = useCallback(
    async referenceAttachmentId => {
      let onDelete = !referenceId;
      if (!!referenceId) {
        onDelete = await attachRemove.remove(referenceAttachmentId);
      }
      if (onDelete) {
        setFiles(
          toArray(attach.items).filter(file => {
            return file.key !== referenceAttachmentId;
          })
        );
      }
    },
    [attach.items]
  );

  const handleDrop = async acceptedFiles => {
    onUploadClose();

    file.upload(acceptedFiles, async (ids, acceptedFiles) => {
      const referenceAttachmentIds = {};
      if (!referenceId) {
        // 새 레퍼런스 작성일 때
        const newItems = new Set([...newFileStorageIds]);
        for (let i = 0; i < ids.length; i++) {
          newItems.add(ids[i]);
        }
        setNewFileStorageIds(newItems);
      } else {
        const res = await api.addReferenceAttach({
          referenceId,
          model: {
            fileStorageIds: ids
          }
        });

        const { data } = res;

        for (let fileStorageId in data) {
          if (data.hasOwnProperty(fileStorageId)) {
            referenceAttachmentIds[fileStorageId] =
              data[fileStorageId].referenceAttachmentId;
          }
        }
      }

      const newItems = [];

      for (let i = 0; i < ids.length; i++) {
        const id = referenceAttachmentIds[ids[i]] || ids[i];
        const acceptedFile = acceptedFiles[i];
        const { type, name, size } = acceptedFile;

        const fileObj = await helpers.file.setFileInfo({
          id,
          extension: type,
          originalName: name,
          size,
          creator: my.profile.name
        });

        newItems.push(fileObj);
      }

      setFiles(toArray(attach.items).concat(newItems));
    });
  };

  useEffect(
    () => {
      const newItem = [];
      for (let i = 0; i < attach.items.length; i++) {
        const { key } = attach.items[i];
        if (newFileStorageIds.has(key)) {
          newItem.push(key);
        }
      }

      setNewFileStorageIds(new Set([...newItem]));
      onChange(null, { name: 'fileStorageIds', value: [...newItem] });
    },
    [attach.items]
  );

  const getDownloadApi = useCallback(
    id => {
      return `api/tbdrugs/references/${referenceId}/attach/${id}`;
    },
    [referenceId]
  );

  const handleViewerClick = useCallback(
    async ({ fileInfo = viewer, id }) => {
      setFileInfo(fileInfo);
      setViewerOpen(true);
    },
    [attach.items]
  );

  const fetchSignedUrl = fileInfo => {
    return api.fetchArchivePresignedurl({
      referenceId,
      referenceAttachmentId: fileInfo.key
    });
  };

  const handleViewerClose = () => {
    setViewerOpen(false);
  };

  const getDownloadUrl = useCallback(
    key => {
      return `api/tbdrugs/references/${referenceId}/attach/${key}`;
    },
    [referenceId]
  );

  const isNew = !referenceId;
  return (
    <div>
      <FileViewerWrapper
        fileInfo={fileInfo}
        fetchSignedUrl={fetchSignedUrl}
        open={viewerOpen}
        onClose={handleViewerClose}
        getDownloadUrl={getDownloadUrl}
      />
      <UploadLayer
        open={isUpload}
        onClose={onUploadClose}
        title={t('common.attachment.upload')}
        onDrop={handleDrop}
      />
      <Segment>
        <Dimmer active={attachDown.loading} inverted>
          <Loader size="small">Loading</Loader>
        </Dimmer>

        <FileList
          files={attach.items}
          onDownloadClick={isNew ? undefined : attachDown.download}
          onDeleteClick={handleRemoveAttach}
          deleteButton={isNew ? FileDeleteButton : undefined}
          // downloadButton={isEdit ? undefined : FileDownloadButton}
          hasContextMenu={isNew ? false : true}
          MultiDownloadButton={MultiDownloadButton({ getDownloadApi })}
          onViewerClick={handleViewerClick}
        />
      </Segment>

      <FakeFileList items={file.fakes} />
    </div>
  );
};

export default memo(ReferenceFileUpload);
