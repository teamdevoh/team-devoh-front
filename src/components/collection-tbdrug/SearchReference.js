import React, { useState, useEffect } from 'react';
import { Input, Button, Grid, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useFormatMessage } from '../../hooks';
import useReferences from './hooks/useReferences';
import helpers from '../../helpers';
import { GridRowWrapper } from './styles';
import { DataGrid } from '../data-grid';

const Inline = styled.div`
  display: inline-block;
`;

const VTopBUtton = styled(Button)`
  &&& {
    vertical-align: top;
    padding: 12px;
    margin-left: 15px;
  }
`;

const FileWrap = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  margin: 10px 4px;
  max-width: 500px;
  cursor: pointer;
`;

/**
 * 선택된 맴버 표시해주기 위한
 * @param {int} curId
 * @param {json} selected
 */
const RowRenderer = (curId, selected = {}) => ({ renderBaseRow, ...props }) => {
  const { referenceId } = props.row;
  const { referenceId: choosedId } = selected === null ? {} : selected;

  return (
    <GridRowWrapper
      selected={curId === referenceId}
      choosed={referenceId === choosedId}
    >
      {renderBaseRow(props)}
    </GridRowWrapper>
  );
};

const columns = onAttachDown => [
  {
    key: 'pmid',
    name: 'PMID'
  },
  {
    key: 'authors',
    name: '1st Authors'
  },
  {
    key: 'title',
    name: 'Title',
    width: 450
  },
  {
    key: 'journal',
    name: 'Journal'
  },
  {
    key: 'year',
    name: 'Year'
  },
  {
    key: 'volume',
    name: 'Volume'
  },
  {
    key: 'pages',
    name: '1st Pages'
  },
  {
    key: 'file',
    name: 'File',
    width: 50,
    formatter: ({ row }) => {
      const { attachs, referenceId } = row;
      const fileList = [];

      for (let i = 0; i < attachs.length; i++) {
        const { referenceAttachmentId, originalName } = attachs[i];
        fileList.push(
          <FileWrap key={referenceAttachmentId} title={originalName}>
            <a onClick={onAttachDown({ referenceId, referenceAttachmentId })}>
              {originalName}
            </a>
          </FileWrap>
        );
      }

      if (fileList.length > 1) {
        const handleClick = e => {
          for (let i = 0; i < attachs.length; i++) {
            const { referenceAttachmentId } = attachs[i];
            onAttachDown({ referenceId, referenceAttachmentId })();
          }
        };

        fileList.unshift(
          <Button key="ddownload_all" onClick={handleClick}>
            Download All
          </Button>
        );
      }

      return fileList.length > 0 ? (
        <Popup
          content={fileList}
          onOpen={event => {
            event.stopPropagation();
            event.preventDefault();
          }}
          on="click"
          trigger={
            <Button
              icon="cloud download"
              style={{ backgroundColor: 'transparent' }}
            />
          }
        />
      ) : null;
    }
  }
];

const SearchReference = ({
  name,
  searchVal,
  value,
  onChange = f => f,
  onSelecte = f => f,
  onClose,
  selecte = true,
  onShowReferenceForm
}) => {
  const t = useFormatMessage();
  const [searchName, setSearchName] = useState(searchVal);
  const [{ items, loading: searchLoading }, setIsDo] = useReferences(
    searchName
  );
  const [selected, setSelected] = useState(null);
  const [tableInfoMessage, setTableInfoMessage] = useState(
    searchName === '' ? t('please.enter.a.search.term') : ''
  );
  const [loading, setLoading] = useState(false);

  const getMembers = () => {
    setIsDo(new Date());
    setTableInfoMessage(
      searchName === ''
        ? t('please.enter.a.search.term')
        : t('project.noresultsfound')
    );
  };

  useEffect(
    () => {
      setSelected(null);
    },
    [items]
  );

  const handleKeyDown = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      getMembers();
    }
  };

  /**
   * 모달창에서 검색 input onChange handle
   * @param {} event
   * @param {json} param1
   */
  const handleSearchNameChange = (event, { value }) => {
    setSearchName(value);
  };

  /**
   * 검색
   */
  const handleSearchMember = () => {
    getMembers();
  };

  /**
   * 선택 완료
   */
  const handleSelectReference = () => {
    onChange(null, {
      name,
      value: selected.referenceId
    });
    onSelecte(selected);
    onClose();
  };

  /**
   * grid row 클릭 이벤트
   * @param {int} index
   * @param {json} row
   */
  const handleRowClick = (index, row, ...rest) => {
    if (selected !== null && row.referenceId === selected.referenceId) {
      return;
    } else {
      setSelected(row);
    }
  };

  const handleRowDoubleClick = (index, row) => {
    const { referenceId } = row;
    onChange(null, { name, value: referenceId });
    onSelecte(row);
    if (selecte) {
      onClose();
    }
  };

  /**
   *
   * @param {json} param0
   */
  const handleAttachDown = ({ referenceId, referenceAttachmentId }) => () => {
    setLoading(true);
    helpers.util.download(
      `api/tbdrugs/references/${referenceId}/attach/${referenceAttachmentId}`,
      percent => {
        if (percent === 100) {
          setLoading(false);
        }
      }
    );
  };

  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <Inline>
            <Input
              action={{ icon: 'search', onClick: handleSearchMember }}
              value={searchName}
              onChange={handleSearchNameChange}
              onKeyDown={handleKeyDown}
              placeholder="Search Title, Pmid, Authors, Journal, Pages"
            />
          </Inline>
          {selecte && (
            <VTopBUtton
              type="button"
              icon="plus"
              onClick={handleSelectReference}
              disabled={selected === null}
              loading={loading}
            />
          )}
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <div>
            <DataGrid
              columns={columns(handleAttachDown)}
              rows={items}
              rowKey="referenceId"
              rowNumber={{ show: true, key: 'no', name: 'No' }}
              loading={searchLoading}
              onRowClick={handleRowClick}
              onRowDoubleClick={handleRowDoubleClick}
              rowRenderer={RowRenderer(value, selected)}
              emptyMessage={tableInfoMessage}
            />
          </div>
        </Grid.Column>
      </Grid.Row>
      {!!onShowReferenceForm && (
        <Grid.Row textAlign="right">
          <Grid.Column>
            <Button onClick={onShowReferenceForm()} loading={loading}>
              {t('add.reference')}
            </Button>
            <Button
              onClick={onShowReferenceForm(selected && selected.referenceId)}
              disabled={!!!selected}
              loading={loading}
            >
              {t('folder.edit')}
            </Button>
          </Grid.Column>
        </Grid.Row>
      )}
    </Grid>
  );
};

export default SearchReference;
