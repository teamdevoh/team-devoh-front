import React, { useState } from 'react';
import ActivityBoard from './ActivityBoard';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useTbdrugActivityStatus } from './hooks';

const DashBoard = ({ projectMemberId, search, onMemberChange, onSearch }) => {
  const [order, setOrder] = useState('asc');
  const { projectId } = useParams();
  const [{ items, activityCount, loading }] = useTbdrugActivityStatus({
    projectId,
    projectMemberId,
    search,
    order
  });

  const handleRequestSort = (event, order) => {
    setOrder(order === 'desc' ? 'desc' : 'asc');
  };

  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onMemberChange={onMemberChange}
            projectMemberId={projectMemberId}
            onSearch={onSearch}
            search={search}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool onRequestSort={handleRequestSort} order={order} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <ActivityBoard
            items={items}
            activityCount={activityCount}
            loading={loading}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
