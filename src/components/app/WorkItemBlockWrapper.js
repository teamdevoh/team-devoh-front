import React from 'react';
import { Grid } from 'semantic-ui-react';
import { RoleAware } from '../auth';
import map from 'lodash/map';
import WorkItemBlock from './WorkItemBlock';
import { useFormatMessage } from '../../hooks';

const WorkItemBlockWrapper = ({ items, onItemClick }) => {
  const t = useFormatMessage();

  return (
    <Grid columns={3} padded divided>
      {map(items, (item, index) => {
        if (item.role) {
          return (
            <RoleAware key={item.id} allowedRole={item.role}>
              <WorkItemBlock
                id={item.id}
                index={index}
                name={item.name}
                title={t(item.locale)}
                icon={item.icon}
                route={item.originalRoute}
                active={item.active}
                onClick={onItemClick}
              />
            </RoleAware>
          );
        } else {
          return (
            <WorkItemBlock
              key={item.id}
              id={item.id}
              index={index}
              name={item.name}
              title={t(item.locale)}
              icon={item.icon}
              route={item.originalRoute}
              onClick={onItemClick}
            />
          );
        }
      })}
    </Grid>
  );
};

export default WorkItemBlockWrapper;
