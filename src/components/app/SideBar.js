import React, { Fragment, useEffect } from 'react';
import './SideBar.css';
import { Sidebar, Menu, Responsive } from 'semantic-ui-react';
import { Modal } from '../modal';
import { useSelector } from 'react-redux';
import { project } from '../../states';
import {
  SideBarProjectContainer,
  SideBarSearchInput,
  SideBarTrail,
  SideBarButtonWrapper,
  NavigationMobile
} from './';
import { useSideBar, useProjectStatus } from './hooks';
import { useProjectItemListUp } from '../project-manage/hooks';
import useMenuStatus from './hooks/useMenuStatus';

const propTypes = {};

const defaultProps = {};

const SideBar = ({ children, Identity }) => {
  const sidebar = useSideBar();
  const projectItemListUp = useProjectItemListUp();
  const projectStatus = useProjectStatus();
  const menuStatus = useMenuStatus();

  const { selectedProject, projectItems, sidebarState } = useSelector(
    state => ({
      sidebarState: state.shared.sidebar,
      selectedProject: project.selectors.getSelectedItem(state),
      projectItems: project.selectors.getItemsWithFilter(state)
    })
  );

  const fetch = async () => {
    await projectItemListUp.fetch();
  };

  useEffect(() => {
    fetch();
    return () => {};
  }, []);

  const items = [
    <SideBarButtonWrapper />,
    <SideBarSearchInput />,
    <SideBarProjectContainer />
  ];

  return (
    <div>
      <Responsive minWidth={Responsive.onlyLargeScreen.minWidth}>
        <Sidebar.Pushable>
          <Sidebar
            as={Menu}
            visible={sidebarState.visible}
            size="large"
            width="wide"
            vertical
            inverted
            animation="push"
          >
            <SideBarTrail
              items={items}
              reverse={!sidebarState.visible}
              state={sidebarState.visible === true ? 'open' : 'close'}
            />
          </Sidebar>
          <Sidebar.Pusher>{children}</Sidebar.Pusher>
        </Sidebar.Pushable>
      </Responsive>
      <Responsive maxWidth={Responsive.onlyLargeScreen.minWidth}>
        <div>
          <Modal
            open={sidebarState.visible}
            onOK={sidebar.toggle}
            title="Navigation"
            content={
              <Fragment>
                <NavigationMobile
                  Identity={Identity}
                  selectedKey={selectedProject.key}
                  onClick={async item => {
                    const isRefreshed = await projectStatus.active(item);
                    if (isRefreshed) {
                      menuStatus.active(item);
                    }
                  }}
                  groups={projectItems}
                />
              </Fragment>
            }
          />
          <Sidebar.Pushable>
            <Sidebar.Pusher>{children}</Sidebar.Pusher>
          </Sidebar.Pushable>
        </div>
      </Responsive>
    </div>
  );
};

SideBar.propTypes = propTypes;
SideBar.defaultProps = defaultProps;

export default SideBar;
