import React, { useCallback, memo } from 'react';
import history from '../../helpers/history';
import { useFormatMessage } from '../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import { shared } from '../../states';
import map from 'lodash/map';
import { WorkItemBarWrapper } from './styles';
import WorkItem from './WorkItem';

const ProjectWorkItemBar = () => {
  const dispatch = useDispatch();
  const menuItems = useSelector(shared.selectors.getMenuItemsWithFilter);

  const t = useFormatMessage();

  const handleClick = useCallback(({ id, name, location }) => {
    dispatch(shared.actions.activeMenu(id, name));
    history.push(location);
  }, []);

  return (
    <WorkItemBarWrapper
      vertical
      icon="labeled"
      compact
      size="mini"
      borderless
      fixed="left"
    >
      {map(menuItems, item => {
        return (
          <WorkItem
            {...item}
            key={item.id}
            id={item.id}
            name={item.name}
            icon={item.icon}
            title={t(item.locale)}
            role={item.role}
            active={item.active}
            onClick={handleClick}
          />
        );
      })}
    </WorkItemBarWrapper>
  );
};

export default memo(ProjectWorkItemBar);
