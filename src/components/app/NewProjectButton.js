import React, { memo, useCallback } from 'react';
import { useFormatMessage } from '../../hooks';
import history from '../../helpers/history';
import { Button, Icon } from 'semantic-ui-react';
import { RoleAware } from '../auth';
import { role } from '../../constants';

const NewProjectButton = memo(({ onClick }) => {
  const t = useFormatMessage();

  const handleClick = useCallback(() => {
    onClick();
    history.push('/project/new');
  }, []);

  return (
    <RoleAware allowedRole={role.Project_Edit}>
      <Button
        primary
        color="yellow"
        icon
        fluid
        size="small"
        labelPosition="right"
        onClick={handleClick}
      >
        {t('project.my')}
        <Icon name="add" />
      </Button>
    </RoleAware>
  );
});

export default NewProjectButton;
