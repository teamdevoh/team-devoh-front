import React from 'react';
import { Icon } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';

const HeaderProjectInfo = ({ projectTitle, roleName }) => {
  const t = useFormatMessage();
  return (
    <div>
      <Icon name="warehouse" />
      {t('project')}
      {' : '}
      {projectTitle}
      {' | '}
      <Icon name="user" />
      {t('role')}
      {' : '}
      {roleName}
    </div>
  );
};

export default HeaderProjectInfo;
