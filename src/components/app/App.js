import React, { Fragment, useEffect, useCallback } from 'react';
import './App.css';
import { Body, Header, SideBar } from './';
import { Router } from 'react-router-dom';
import helpers from '../../helpers';
import history from '../../helpers/history';
import { InjectIntlContext } from '@comparaonline/react-intl-hooks';
import * as firebase from 'firebase/app';
import 'firebase/messaging';
import { api } from '../account-manage';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import useUser from '../account-manage/hooks/useUser';
import { config } from '../../constants';

firebase.initializeApp(config.FCM);

const App = ({ children }) => {
  const [user] = useUser();

  if (firebase.messaging.isSupported()) {
    useEffect(
      () => {
        if (user.isAuth) {
          reqPermission();
        }
      },
      [user.isAuth]
    );

    const messaging = firebase.messaging();

    const reqPermission = useCallback(() => {
      messaging.requestPermission().then(async () => {
        const token = await messaging.getToken();
        const res = await api.editPushToken({
          memberId: user.profile.id,
          token
        });
        if (res.data) {
          console.log('updated token');
        }
      });
    });

    messaging.onTokenRefresh(async () => {
      const refreshedToken = await messaging.getToken();
      const res = await api.editPushToken({
        memberId: user.profile.id,
        refreshedToken
      });
      if (res.data) {
        console.log('Token refreshed.');
      }
    });

    messaging.onMessage(payload => {
      toast(
        <div>
          <p>{payload.notification.title}</p>
          <p>{payload.notification.body}</p>
        </div>
      );
    });
  }

  const elements =
    user.isAuth && user.profile.securityPledge ? (
      <Fragment>
        <Header Identity={helpers.Identity} />
        <SideBar Identity={helpers.Identity}>
          <Body children={children} />
        </SideBar>
        <ToastContainer position="bottom-right" />
      </Fragment>
    ) : (
      <Fragment>{children}</Fragment>
    );
  return (
    <Router history={history}>
      <InjectIntlContext>{elements}</InjectIntlContext>
    </Router>
  );
};

export default App;
