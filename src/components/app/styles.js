import styled from 'styled-components';
import { Grid, Icon, Divider, Menu } from 'semantic-ui-react';
import helpers from '../../helpers';

export const BodyWrapper = styled(Grid)`
  && {
    margin-top: 1em;
    margin-bottom: 0.5em;
  }
`;

export const BodyLeftColumn = styled(Grid.Column).attrs({ width: 'one' })`
  height: 100vh;
`;

export const BodyCenterColumn = styled(Grid.Column)`
  ${helpers.util.isMobile()
    ? {
        marginLeft: '0.2em',
        marginRight: '0.2em'
      }
    : {}};
`;

export const WorkItemBarWrapper = styled(Menu)`
  &&& {
    position: fixed;
    display: flex;
    flex-direction: column;
    top: 0px;
    bottom: 0px;
    overflow-x: hidden;
    flex: 1;
    background: #f6f1f0;
  }
`;

export const Hamburger = styled(Icon).attrs({
  className: 'HOMEPG',
  name: 'content'
})`
  &&& {
    cursor: pointer;
    margin-right: 1em !important;
  }
`;

export const DividerFitted = styled(Divider)`
  && {
    background-color: yellow;
    margin-top: 0.2em;
    margin-bottom: 0.2em;
  }
`;
