import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useFormatMessage } from '../../hooks';
import { shared } from '../../states';
import { WorkItemBarWrapper } from '../app/styles';
import WorkItem from './WorkItem';

const ConsoleWorkItemBar = () => {
  const t = useFormatMessage();
  const history = useHistory();
  const dispatch = useDispatch();
  const menuItems = useSelector(shared.selectors.getMenuItemsWithFilter);

  const handleClick = useCallback(({ id, name, location }) => {
    dispatch(shared.actions.activeMenu(id, name));
    history.push(location);
  }, []);

  return (
    <WorkItemBarWrapper
      vertical
      icon="labeled"
      compact
      size="mini"
      borderless
      fixed="left"
    >
      {menuItems.map(item => {
        return (
          <WorkItem
            {...item}
            key={item.id}
            id={item.id}
            name={item.name}
            icon={item.icon}
            title={t(item.locale)}
            role={item.role}
            active={item.active}
            onClick={handleClick}
          />
        );
      })}
    </WorkItemBarWrapper>
  );
};

export default ConsoleWorkItemBar;
