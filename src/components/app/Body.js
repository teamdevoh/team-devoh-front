import React, { Fragment } from 'react';
import { ProjectWorkItemBar, ConsoleWorkItemBar } from './';
import { Location } from '../account-manage';
import { BodyWrapper, BodyLeftColumn, BodyCenterColumn } from './styles';
import { Responsive } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import { project } from '../../states';

const Body = ({ children }) => {
  const item = useSelector(project.selectors.getSelectedItem);

  return (
    <Fragment>
      <Responsive minWidth={Responsive.onlyLargeScreen.minWidth}>
        <BodyWrapper>
          <Location />
          <BodyLeftColumn>
            {item.key > 0 ? <ProjectWorkItemBar /> : <ConsoleWorkItemBar />}
          </BodyLeftColumn>
          <BodyCenterColumn width="fifteen">{children}</BodyCenterColumn>
        </BodyWrapper>
      </Responsive>
      <Responsive maxWidth={Responsive.onlyLargeScreen.minWidth}>
        <BodyWrapper>
          <Location />
          <BodyCenterColumn width="sixteen">{children}</BodyCenterColumn>
        </BodyWrapper>
      </Responsive>
    </Fragment>
  );
};

export default Body;
