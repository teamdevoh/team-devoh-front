import React from 'react';
import { useSideBar } from './hooks';
import { Hamburger } from './styles';

const HamburgerButton = () => {
  const sidebar = useSideBar();

  return <Hamburger onClick={sidebar.toggle} />;
};

export default HamburgerButton;
