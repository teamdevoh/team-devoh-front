import React, { useCallback, Fragment } from 'react';
import PropTypes from 'prop-types';
import history from '../../helpers/history';
import { Menu, Divider, Header } from 'semantic-ui-react';
import { LinkButton } from '../button';
import { useBoolean, useFormatMessage } from '../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import { project, shared } from '../../states';
import {
  WorkItemBlockWrapper,
  SideBarButtonWrapper,
  SideBarProjectContainer,
  SideBarSearchInput
} from './';
import { useSideBar } from './hooks';

const propTypes = {
  groups: PropTypes.array,
  onClick: PropTypes.func,
  selectedKey: PropTypes.number
};

const defaultProps = {};

const NavigationProjectMobile = ({ groups, onClick, selectedKey }) => {
  const t = useFormatMessage();
  const { selectedProject, menuItems, consoleItems } = useSelector(state => ({
    selectedProject: project.selectors.getSelectedItem(state),
    menuItems: shared.selectors.getMenuItemsWithFilter(state),
    consoleItems: shared.selectors.getConsoleItems(state)
  }));

  const navigator = useBoolean(selectedProject.key > 0 ? false : true);
  const viewConsole = useBoolean(false);
  const sidebar = useSideBar();

  const dispatch = useDispatch();

  const toggleView = () => {
    if (selectedProject.key > 0) {
      navigator.toggle();
    }
  };

  const handleProjectItemClick = useCallback(
    project => {
      navigator.toggle();
      onClick(project);
    },
    [onClick]
  );

  const handleWorkItemClick = useCallback(({ key, name, location }) => {
    dispatch(shared.actions.activeMenu(key, name));
    sidebar.toggle();
    history.push(location);
  }, []);

  const isFunctionView = !navigator.value;

  return (
    <Fragment>
      {!viewConsole.value && (
        <Fragment>
          <LinkButton
            fluid={true}
            color="red"
            icon={isFunctionView ? 'left arrow' : 'right arrow'}
            labelPosition={isFunctionView ? 'left' : 'right'}
            name={isFunctionView ? t('project') : t('function')}
            onClick={toggleView}
          />
          <Divider />
          {isFunctionView &&
            selectedProject.key > 0 && (
              <WorkItemBlockWrapper
                items={menuItems}
                onItemClick={handleWorkItemClick}
              />
            )}
          {!isFunctionView && (
            <Menu vertical fluid>
              <SideBarButtonWrapper onConsoleClick={viewConsole.toggle} />
              <SideBarSearchInput />
              <SideBarProjectContainer onItemClick={handleProjectItemClick} />
            </Menu>
          )}
        </Fragment>
      )}
      {viewConsole.value && (
        <Fragment>
          <LinkButton
            fluid={true}
            color="red"
            icon={'left arrow'}
            labelPosition={'left'}
            name={t('project')}
            onClick={viewConsole.toggle}
          />
          <Header as="h4">Console</Header>
          <Divider />
          <WorkItemBlockWrapper
            items={consoleItems}
            onItemClick={handleWorkItemClick}
          />
        </Fragment>
      )}
    </Fragment>
  );
};

NavigationProjectMobile.propTypes = propTypes;
NavigationProjectMobile.defaultProps = defaultProps;

export default NavigationProjectMobile;
