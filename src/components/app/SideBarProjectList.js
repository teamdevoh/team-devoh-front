import React, { memo } from 'react';
import { Menu } from 'semantic-ui-react';
import map from 'lodash/map';
import { SideBarProjectListItem } from './';

const SideBarProjectList = memo(({ items, selectedKey, onClick }) => {
  return (
    <Menu.Menu>
      {map(items, item => {
        return (
          <SideBarProjectListItem
            key={item.projectId}
            {...item}
            active={item.projectId.toString() === selectedKey.toString()}
            onClick={onClick}
          />
        );
      })}
    </Menu.Menu>
  );
});

export default SideBarProjectList;
