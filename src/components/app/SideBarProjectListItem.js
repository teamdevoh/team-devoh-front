import React, { useCallback } from 'react';
import { Menu } from 'semantic-ui-react';

const SideBarProjectListItem = ({
  active,
  projectId,
  localTitle,
  title,
  periodFrom,
  periodTo,
  protocolNo,
  projectTeam,
  variable,
  onClick
}) => {
  const handleItemClick = useCallback(
    () => {
      onClick({
        projectId,
        title,
        localTitle,
        projectTeam,
        periodFrom,
        periodTo,
        protocolNo,
        variable
      });
    },
    [onClick]
  );

  return (
    <Menu.Item as="a" active={active} onClick={handleItemClick}>
      {title}
    </Menu.Item>
  );
};

export default SideBarProjectListItem;
