import React, { useCallback } from 'react';
import { Grid, Icon } from 'semantic-ui-react';

const WorkItemBlock = ({
  id,
  index,
  name,
  title,
  icon,
  active,
  route,
  onClick
}) => {
  const colors = [
    'red',
    'orange',
    'yellow',
    'olive',
    'green',
    'teal',
    'blue',
    'violet',
    'purple',
    'pink',
    'brown',
    'grey',
    'black'
  ];

  const handleClick = useCallback(() => {
    onClick({ key: id, name, location: route });
  });

  const color = colors[index % colors.length];
  if (!color) {
    throw new Error('Require is color value define');
  }

  return (
    <Grid.Column textAlign="center" color={color} onClick={handleClick}>
      <Icon name={icon} size="big" disabled={active ? false : true} />
      <p>{title}</p>
    </Grid.Column>
  );
};

export default WorkItemBlock;
