import React, { memo, useCallback } from 'react';
import { Icon, Menu } from 'semantic-ui-react';
import { RoleAware } from '../auth';

const WorkItem = memo(
  ({ id, name, originalRoute, icon, title, role, active, onClick }) => {
    const handleClick = useCallback(
      () => {
        onClick({ id, name, location: originalRoute });
      },
      [id, name, originalRoute, onClick]
    );

    const MenuItem = (
      <Menu.Item as="a" active={active} color="red" onClick={handleClick}>
        <Icon name={icon} />
        {title.split('\n').map((singleText, index) => {
          return (
            <span key={index}>
              {singleText}
              <br />
            </span>
          );
        })}
      </Menu.Item>
    );
    if (role) {
      return <RoleAware allowedRole={role}>{MenuItem}</RoleAware>;
    } else {
      return MenuItem;
    }
  }
);

export default WorkItem;
