import React, { memo } from 'react';
import { Keyframes, animated } from 'react-spring/renderprops';

// Creates a keyframed trail
const Trail = Keyframes.Trail({
  peek: [
    { x: 0, opacity: 1, from: { x: -100, opacity: 0 }, delay: 600 },
    { x: -100, opacity: 0, delay: 0 }
  ],
  open: { x: 0, opacity: 1, delay: 100 },
  close: { x: -100, opacity: 0, delay: 0 }
});

const SideBarTrail = memo(({ items, reverse, state }) => {
  return (
    <Trail
      native={true}
      items={items}
      keys={items.map((_, i) => i)}
      reverse={reverse}
      state={state}
    >
      {(item, i) => ({ x, ...props }) => {
        return (
          <animated.div
            style={{
              transform: x.interpolate(x => `translate3d(${x}%,0,0)`),
              ...props
            }}
          >
            {item}
          </animated.div>
        );
      }}
    </Trail>
  );
});

export default SideBarTrail;
