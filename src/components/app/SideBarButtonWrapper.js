import React from 'react';
import { Grid, Menu } from 'semantic-ui-react';
import { ConsoleRedirectButton, NewProjectButton } from './';
import { useSideBar } from './hooks';

const SideBarButtonWrapper = ({ onConsoleClick }) => {
  const sidebar = useSideBar();

  return (
    <Menu.Item>
      <Grid columns="equal">
        <Grid.Column>
          <ConsoleRedirectButton onClick={onConsoleClick} />
        </Grid.Column>
        <Grid.Column>
          <NewProjectButton onClick={sidebar.toggle} />
        </Grid.Column>
      </Grid>
    </Menu.Item>
  );
};

export default SideBarButtonWrapper;
