import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Header, Menu } from 'semantic-ui-react';
import { project } from '../../states';
import { useMenuStatus, useProjectStatus } from './hooks';
import SideBarProjectList from './SideBarProjectList';
import map from 'lodash/map';

export const SideBarProjectContainer = ({ onItemClick }) => {
  const projectStatus = useProjectStatus();
  const menuStatus = useMenuStatus();

  const { selectedProject, items } = useSelector(state => ({
    selectedProject: project.selectors.getSelectedItem(state),
    items: project.selectors.getItemsWithFilter(state)
  }));

  return (
    <Fragment>
      {map(items, item => {
        if (item.projects.length > 0) {
          return (
            <Menu.Item key={item.projectGroupId}>
              <Menu.Header as={Header} color="olive">
                {item.groupName}
              </Menu.Header>
              <SideBarProjectList
                items={item.projects}
                selectedKey={selectedProject.key}
                onClick={async item => {
                  const isRefreshed = await projectStatus.active(item);
                  if (isRefreshed) {
                    menuStatus.active(item);
                    onItemClick && onItemClick(item);
                  }
                }}
              />
            </Menu.Item>
          );
        } else {
          return <Fragment key={item.projectGroupId} />;
        }
      })}
    </Fragment>
  );
};

export default SideBarProjectContainer;
