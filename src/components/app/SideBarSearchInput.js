import React, { useCallback, memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Menu, Grid, Input } from 'semantic-ui-react';
import { useFormatMessage, useBoolean } from '../../hooks';
import { project } from '../../states';

const SideBarSearchInput = memo(() => {
  const t = useFormatMessage();
  const isLoading = useBoolean(false);
  const dispatch = useDispatch();
  const filter = useSelector(project.selectors.getFilter);

  const handleSearch = useCallback((event, data) => {
    dispatch(project.actions.filter({ name: data.value }));
  }, []);

  return (
    <Menu.Item>
      <Grid>
        <Grid.Column floated="left" width={16}>
          <Input
            fluid
            icon="search"
            placeholder={t('common.search')}
            loading={isLoading.value}
            onChange={handleSearch}
            value={filter.name}
            size="mini"
          />
        </Grid.Column>
      </Grid>
    </Menu.Item>
  );
});

export default SideBarSearchInput;
