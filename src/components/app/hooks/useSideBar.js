import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { shared } from '../../../states';

const useSideBar = () => {
  const dispatch = useDispatch();

  const toggle = useCallback(() => {
    dispatch(shared.actions.toggleSideBar());
  }, []);

  return { toggle };
};

export default useSideBar;
