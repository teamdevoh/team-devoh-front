import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { navigation } from '../../../constants';
import history from '../../../helpers/history';
import { shared, subject } from '../../../states';

const getStartRouteByEachProject = (name = '', projectId) => {
  const defaultRoute = {
    route: `/project/${projectId}/conversation`,
    key: 'conversation',
    name: 'Conversation'
  };

  const workItemKey = navigation.FirstWorkByVariable[name.toLowerCase()];
  if (workItemKey) {
    const workItem = navigation.WorkItem.Project.find(item => {
      return item.id === workItemKey;
    });

    const route = workItem.originalRoute
      .replace('{projectId}', projectId)
      .replace('{variable}', name);

    return {
      defaultRoute,
      route: route,
      key: workItem.id,
      name: workItem.name
    };
  }

  return defaultRoute;
};

const useMenuStatus = () => {
  const dispatch = useDispatch();

  const active = useCallback(({ projectId, variable }) => {
    if (!!variable && variable.toLowerCase() === 'cohort') {
      dispatch(subject.actions.clearFilter());
    }

    const { route, key, name } = getStartRouteByEachProject(
      variable === null ? '' : variable,
      projectId
    );

    dispatch(shared.actions.activeMenu(key, name));

    history.push(route);
  }, []);

  return { active };
};
export default useMenuStatus;
