import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { project } from '../../../states';
import { useToken } from '../../account-manage/hooks';

const useProjectStatus = () => {
  const dispatch = useDispatch();
  const token = useToken();

  const active = useCallback(
    async ({
      projectId,
      localTitle,
      title,
      projectTeam,
      periodFrom,
      periodTo,
      protocolNo,
      variable
    }) => {
      const resfreshed = await token.refresh(projectId);
      if (resfreshed) {
        dispatch(
          project.actions.selectedItem({
            key: projectId,
            name: localTitle,
            engName: title,
            role: projectTeam,
            periodFrom: periodFrom,
            periodTo: periodTo,
            protocolNo: protocolNo,
            variable: variable
          })
        );
      }

      return resfreshed;
    },
    []
  );

  return { active };
};
export default useProjectStatus;
