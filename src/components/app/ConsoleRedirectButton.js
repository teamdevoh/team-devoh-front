import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Button, Icon } from 'semantic-ui-react';
import { shared } from '../../states';

const ConsoleRedirectButton = ({ onClick }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const handleClick = useCallback(() => {
    dispatch(shared.actions.activeMenu('console', 'Console'));
    history.push('/');
    onClick && onClick();
  }, []);

  return (
    <Button
      color="yellow"
      icon
      fluid
      size="small"
      labelPosition="right"
      onClick={handleClick}
    >
      Console
      <Icon name="game" />
    </Button>
  );
};

export default ConsoleRedirectButton;
