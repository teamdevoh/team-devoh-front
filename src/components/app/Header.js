import React, { memo, useCallback, Fragment } from 'react';
import './Header.css';
import { Menu, Visibility, Responsive } from 'semantic-ui-react';
import { Logo } from '../logo';
import { UserContextMenu } from '../account-manage';
import styled from 'styled-components';
import { useFormatMessage, useBoolean } from '../../hooks';
import { useSelector, useDispatch } from 'react-redux';
import { project, shared } from '../../states';
import Joyride, { STATUS } from 'react-joyride';
import helpers from '../../helpers';
import { DividerFitted } from './styles';
import { HamburgerButton, HeaderProjectInfo } from './';

const HamburgerWrapper = ({ onClick }) => {
  const t = useFormatMessage();
  const dispatch = useDispatch();
  const sidebar = useSelector(shared.selectors.getSideBar);

  const FINISHED = 'FINISHED';
  const steps = [
    {
      target: '.HOMEPG',
      content: <div>{t('playguid.home.start')}</div>
    }
  ];

  const handleJoyrideCallback = useCallback(
    ({ action, index, status, type }) => {
      if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status)) {
        if (!sidebar.visible) {
          dispatch(shared.actions.visibleSideBar(true));
        }
        window.localStorage.setItem(helpers.storagekeys.HomePG, FINISHED);
      }
    },
    []
  );

  const styles = {
    options: {
      arrowColor: '#e3ffeb',
      backgroundColor: '#e3ffeb',
      primaryColor: '#78fc19',
      textColor: '#004a14',
      width: 300,
      zIndex: 1000
    }
  };

  return (
    <Fragment>
      {window.localStorage.getItem(helpers.storagekeys.HomePG) !== FINISHED && (
        <Joyride
          steps={steps}
          styles={styles}
          callback={handleJoyrideCallback}
        />
      )}
      <HamburgerButton />
    </Fragment>
  );
};

const StyledHeaderWrapper = styled(Menu)`
  ${props => {
    if (props.fixed && props.fixed === 'top') {
      return {
        border: '1px solid black',
        boxShadow: '0px 3px 5px rgba(0, 0, 0, .87)',
        height: '3em'
      };
    } else {
      return {
        border: 'none',
        borderRadius: 0,
        boxShadow: 'none',
        height: '3em',
        transition: 'box-shadow 0.5s ease, padding 0.5s ease'
      };
    }
  }};
`;

const HeaderWrapper = ({ fixed, children }) => {
  return (
    <StyledHeaderWrapper
      inverted
      color="black"
      borderless={true}
      fixed={fixed === true ? 'top' : null}
    >
      {children}
    </StyledHeaderWrapper>
  );
};

const Header = memo(() => {
  const menuFixed = useBoolean(false);
  const isMobile =
    Responsive.onlyMobile.maxWidth > window.screen.width ? true : false;

  const selectedProject = useSelector(project.selectors.getSelectedItem);

  const stickTopMenu = useCallback(() => {
    menuFixed.setTrue();
  }, []);

  const unStickTopMenu = useCallback(() => {
    menuFixed.setFalse();
  }, []);

  return (
    <Visibility
      onBottomPassed={stickTopMenu}
      onBottomVisible={unStickTopMenu}
      once={false}
    >
      <HeaderWrapper fixed={menuFixed.value}>
        <Menu.Item position="left">
          <HamburgerWrapper />
          <div>
            <Logo />
            {!isMobile && selectedProject.key > 0 && <DividerFitted />}
            {!isMobile &&
              selectedProject.key > 0 && (
                <HeaderProjectInfo
                  projectTitle={selectedProject.name}
                  roleName={selectedProject.role}
                />
              )}
          </div>
        </Menu.Item>
        <Menu.Item position="right">
          <UserContextMenu />
        </Menu.Item>
      </HeaderWrapper>
    </Visibility>
  );
});

export default Header;
