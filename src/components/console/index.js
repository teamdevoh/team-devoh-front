import { asyncComponent } from '../modules';

const ConsoleWrapper = asyncComponent(() => import('./ConsoleWrapper'));
const Console = asyncComponent(() => import('./Console'));
const ConsoleHeader = asyncComponent(() => import('./ConsoleHeader'));
const ConsoleProjectCard = asyncComponent(() => import('./ConsoleProjectCard'));
const ConsoleProjectCardNewProject = asyncComponent(() => import('./ConsoleProjectCardNewProject'));
const StatisticMember = asyncComponent(() => import('./StatisticMember'));
const StatisticSite = asyncComponent(() => import('./StatisticSite'));
const StatisticMyTask = asyncComponent(() => import('./StatisticMyTask'));
const StatisticTaskProgress = asyncComponent(() => import('./StatisticTaskProgress'));
const TaskProgressContainer = asyncComponent(() => import('./TaskProgressContainer'));
const TaskProgressHeader = asyncComponent(() => import('./TaskProgressHeader'));
const TaskProgressList = asyncComponent(() => import('./TaskProgressList'));
const TaskProgressListItem = asyncComponent(() => import('./TaskProgressListItem'));
const TaskSchedulerContainer = asyncComponent(() => import('./TaskSchedulerContainer'));

export {
  ConsoleWrapper,
  Console,
  ConsoleHeader,
  ConsoleProjectCard,
  ConsoleProjectCardNewProject,
  StatisticMember,
  StatisticSite,
  StatisticMyTask,
  StatisticTaskProgress,
  TaskProgressContainer,
  TaskProgressHeader,
  TaskProgressList,
  TaskProgressListItem,
  TaskSchedulerContainer
};