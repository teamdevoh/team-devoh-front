import React from 'react';
import { Header } from 'semantic-ui-react';

const TaskScheduleHeader = () => {
  return (
    <Header as="h3" dividing>
      Task Schedule
    </Header>
  );
};

export default TaskScheduleHeader;
