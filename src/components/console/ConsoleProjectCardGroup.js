import React, { Fragment } from 'react';
import map from 'lodash/map';
import { useSelector } from 'react-redux';
import { project } from '../../states';
import { Card, Header, Transition } from 'semantic-ui-react';
import { ConsoleProjectCard, ConsoleProjectCardNewProject } from './';

const ConsoleProjectCardGroup = () => {
  const projectGroups = useSelector(project.selectors.getItemsWithFilter);

  return (
    <Fragment>
      {map(projectGroups, (projectGroup, index) => {
        return (
          <Fragment key={projectGroup.projectGroupId}>
            <Header as="h2" color="olive">
              {projectGroup.groupName}
            </Header>
            <Transition.Group as={Card.Group} duration={200} stackable>
              {projectGroup.projects.map(project => {
                return (
                  <ConsoleProjectCard
                    key={project.projectId}
                    groupName={projectGroup.groupName}
                    projectTeam={project.projectTeam}
                    projectId={project.projectId}
                    variable={project.variable}
                  />
                );
              })}
              <ConsoleProjectCardNewProject
                projectGroupId={projectGroup.projectGroupId}
              />
            </Transition.Group>
          </Fragment>
        );
      })}
    </Fragment>
  );
};

export default ConsoleProjectCardGroup;
