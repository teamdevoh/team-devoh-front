import React, { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Dropdown, Form, Grid, Icon, Table } from 'semantic-ui-react';
import { project } from '../../states';
import TaskSchedulerGroup from './TaskSchedulerGroup';
import moment from 'moment';
import { useFormatMessage, useToKeyPairs } from '../../hooks';
import { useProjectMemberProjectAll } from './hooks';
import TaskScheduleHeader from './TaskScheduleHeader';

const TaskSchedulerContainer = () => {
  const t = useFormatMessage();
  const [projects, setProjects] = useState([]);
  const [projectIds, setprojectIds] = useState([]);
  const [selectedProjectId, setSelectedProjectId] = useState(0);
  const [selectedProjectIdsByMember, setSelectedProjectIdsByMember] = useState(
    []
  );
  const member = useProjectMemberProjectAll(projectIds);

  const projectOption = useToKeyPairs({
    key: 'projectId',
    text: 'title',
    source: projects
  });
  const memberOption = useToKeyPairs({
    key: 'memberId',
    text: 'memberName',
    source: member.items
  });

  const handleProjectChange = useCallback((event, data) => {
    setSelectedProjectId(data.value);
  }, []);

  const handleMemberChange = useCallback(
    (event, data) => {
      if (data.value === 0) {
        setSelectedProjectIdsByMember([]);
      } else {
        const selectedMembers = _.filter(member.items, {
          memberId: data.value
        });
        let ids = [];
        selectedMembers.forEach(selectedMember => {
          ids.push(selectedMember.projectId);
        });
        setSelectedProjectIdsByMember(ids);
      }
    },
    [member.items]
  );

  const getTargetDates = standardMonth => {
    const prevMonth = moment(standardMonth).subtract(1, 'months');
    const nextMonth = moment(standardMonth).add(1, 'months');

    return [
      prevMonth.format('YYYY-MM'),
      standardMonth.format('YYYY-MM'),
      nextMonth.format('YYYY-MM')
    ];
  };

  const [scheduleDates, setScheduleDates] = useState(getTargetDates(moment()));
  const projectGroups = useSelector(project.selectors.getItems);

  useEffect(
    () => {
      let onlyProjects = [];
      let projectIds = [];
      projectGroups.forEach(projectGroup => {
        projectGroup.projects.forEach(project => {
          onlyProjects.push(project);
          projectIds.push(project.projectId);
        });
      });

      setProjects(onlyProjects);
      setprojectIds(projectIds);
    },
    [projectGroups]
  );

  const handleDateHeaderClick = useCallback(
    index => {
      const standardMonth = scheduleDates[1];
      const resetStandardMonth =
        index === 0
          ? moment(standardMonth).subtract(1, 'months')
          : index === 2
            ? moment(standardMonth).add(1, 'months')
            : moment(standardMonth);

      setScheduleDates(getTargetDates(resetStandardMonth));
    },
    [scheduleDates]
  );

  return (
    <Grid>
      <Grid.Row>
        <Grid.Column width="sixteen">
          <TaskScheduleHeader />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="sixteen">
          <Form>
            <Form.Group widths="equal">
              <Form.Field>
                <label>{t('project')}</label>
                <Dropdown
                  placeholder="Project"
                  fluid
                  search
                  selection
                  defaultValue={0}
                  onChange={handleProjectChange}
                  options={[
                    { key: 0, text: 'All', value: 0 },
                    ...projectOption.items
                  ]}
                />
              </Form.Field>
              <Form.Field>
                <label>{t('project.member')}</label>
                <Dropdown
                  placeholder={t('project.member')}
                  fluid
                  search
                  selection
                  defaultValue={0}
                  onChange={handleMemberChange}
                  options={[
                    { key: 0, text: 'All', value: 0 },
                    ...memberOption.items
                  ]}
                />
              </Form.Field>
            </Form.Group>
          </Form>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="sixteen">
          <Table celled structured compact="very">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell width="three">
                  {t('project')}
                </Table.HeaderCell>
                <Table.HeaderCell width="four">{t('task')}</Table.HeaderCell>
                {scheduleDates.map((scheduleDate, index) => {
                  return (
                    <Table.HeaderCell
                      key={index}
                      textAlign="center"
                      width="three"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        handleDateHeaderClick(index);
                      }}
                    >
                      {index === 0 && <Icon name="angle left" />}
                      {scheduleDate}
                      {index === 2 && <Icon name="angle right" />}
                    </Table.HeaderCell>
                  );
                })}
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {_.filter(projects, project => {
                return (
                  (selectedProjectId === 0 ||
                    project.projectId === selectedProjectId) &&
                  (selectedProjectIdsByMember.length === 0 ||
                    selectedProjectIdsByMember.indexOf(project.projectId) > -1)
                );
              }).map(project => {
                return (
                  <TaskSchedulerGroup
                    key={project.projectId}
                    project={project}
                    scheduleDates={scheduleDates}
                  />
                );
              })}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default TaskSchedulerContainer;
