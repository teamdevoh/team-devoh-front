import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Dropdown, Form } from 'semantic-ui-react';
import { project } from '../../states';
import {
  useProjectMemberProjectAll,
  useProjedtSiteMyProjectAll
} from './hooks';
import { useToKeyPairs } from '../../hooks';
import { TaskProgressHeader, TaskProgressList } from './';

const TaskProgressContainer = () => {
  const [projectItems, setProjectItems] = useState([]);
  const [filterProject, setFilterProject] = useState([]);
  const [filterMember, setFilterMember] = useState([]);
  const [filterSite, setFilterSite] = useState([]);
  const [projectIds, setProjectIds] = useState([]);
  const items = useSelector(project.selectors.getItems);
  const site = useProjedtSiteMyProjectAll(projectIds);
  const member = useProjectMemberProjectAll(projectIds);
  const projectOption = useToKeyPairs({
    key: 'projectId',
    text: 'title',
    source: projectItems
  });
  const memberOption = useToKeyPairs({
    key: 'memberId',
    text: 'memberName',
    source: member.items
  });
  const siteOption = useToKeyPairs({
    key: 'siteId',
    text: 'siteName',
    source: site.items
  });

  useEffect(
    () => {
      let projects = [];
      let projectIds = [];

      items.forEach(item => {
        item.projects.forEach(projectItem => {
          projects.push(projectItem);
          projectIds.push(projectItem.projectId);
        });
      });

      setProjectItems(projects);
      setProjectIds(projectIds);
    },
    [items]
  );

  const handleProjectChange = useCallback(
    (event, data) => {
      let filterProjectIds = [];
      projectItems.forEach(item => {
        if (item.projectId === data.value) {
          filterProjectIds.push(item.projectId);
        }
      });

      setFilterProject(filterProjectIds);
    },
    [projectItems]
  );

  const handleMemberChange = useCallback(
    (event, data) => {
      let filterProjectIds = [];
      member.items.forEach(item => {
        if (item.memberId === data.value) {
          filterProjectIds.push(item.projectId);
        }
      });

      setFilterMember(filterProjectIds);
    },
    [member.items]
  );

  const handleSiteChange = useCallback(
    (event, data) => {
      let filterProjectIds = [];
      site.items.forEach(item => {
        if (item.siteId === data.value) {
          filterProjectIds.push(item.projectId);
        }
      });

      setFilterSite(filterProjectIds);
    },
    [site.items]
  );

  const getItemsWithFilter = () => {
    return projectItems.filter(item => {
      return (
        (filterProject.length === 0 ||
          filterProject.indexOf(item.projectId) > -1) &&
        (filterMember.length === 0 ||
          filterMember.indexOf(item.projectId) > -1) &&
        (filterSite.length === 0 || filterSite.indexOf(item.projectId) > -1)
      );
    });
  };

  return (
    <Fragment>
      <TaskProgressHeader />
      <Form>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Project</label>
            <Dropdown
              placeholder="Project"
              fluid
              search
              selection
              defaultValue={0}
              onChange={handleProjectChange}
              options={[
                { key: 0, text: 'All', value: 0 },
                ...projectOption.items
              ]}
            />
          </Form.Field>
          <Form.Field>
            <label>Member</label>
            <Dropdown
              placeholder="State"
              fluid
              search
              selection
              defaultValue={0}
              onChange={handleMemberChange}
              options={[
                { key: 0, text: 'All', value: 0 },
                ...memberOption.items
              ]}
            />
          </Form.Field>
          <Form.Field>
            <label>Site</label>
            <Dropdown
              placeholder="State"
              fluid
              search
              selection
              defaultValue={0}
              onChange={handleSiteChange}
              options={[{ key: 0, text: 'All', value: 0 }, ...siteOption.items]}
            />
          </Form.Field>
        </Form.Group>
      </Form>
      <TaskProgressList projects={getItemsWithFilter()} sites={site.items} />
    </Fragment>
  );
};

export default TaskProgressContainer;
