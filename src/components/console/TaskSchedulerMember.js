import React, { Fragment } from 'react';
import { useFormatMessage } from '../../hooks';
import useTaskMembers from './hooks/useTaskMembers';

const TaskSchedulerMember = ({
  projectId,
  taskItemId,
  projectMembers = []
}) => {
  const t = useFormatMessage();
  const { item } = useTaskMembers({ projectId, taskItemId });

  let members = [];
  item.projectMemberIds.forEach(projectMemberId => {
    const member = _.find(projectMembers, { value: projectMemberId });
    if (member) {
      members.push(member.text);
    }
  });

  return (
    <Fragment>
      {members.length > 0 ? members.join(', ') : t('task.assigned.not.count')}
    </Fragment>
  );
};

export default TaskSchedulerMember;
