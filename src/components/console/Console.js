import React from 'react';
import { Grid } from 'semantic-ui-react';
import ConsoleProjectCardGroup from './ConsoleProjectCardGroup';
import { ConsoleWrapper } from './';
import SideBarSearchInput from '../app/SideBarSearchInput';

const Console = () => {
  return (
    <ConsoleWrapper>
      <Grid.Row>
        <Grid.Column style={{ height: '100vh', overflow: 'auto' }}>
          <SideBarSearchInput />
          <ConsoleProjectCardGroup />
        </Grid.Column>
      </Grid.Row>
    </ConsoleWrapper>
  );
};

export default Console;
