import { useEffect, useState } from 'react';
import api from '../../project-manage/api';

const useProjectMemberProjectAll = (ids = []) => {
  const [items, setItems] = useState([]);

  useEffect(
    () => {
      if (ids.length > 0) {
        let requests = [];
        ids.forEach(id => {
          requests.push(api.fetchProjectMembers({ projectId: id }));
        });

        let data = [];
        const promises = Promise.all(requests);
        promises.then(responses => {
          responses.forEach(res => {
            data.push(...res.data.items);
          });

          setItems(data);
        });
      }
    },
    [ids]
  );

  return { items };
};

export default useProjectMemberProjectAll;
