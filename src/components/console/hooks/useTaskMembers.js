import { useEffect, useState } from 'react';
import api from '../../task-manage/api';

const useTaskMembers = ({ projectId, taskItemId }) => {
  const [item, setItem] = useState({ projectMemberIds: [] });

  const fetchItems = async () => {
    const res = await api.fetchTask({ projectId, taskItemId });
    if (res && res.data) {
      setItem(res.data);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, taskItemId]
  );

  return { item };
};

export default useTaskMembers;
