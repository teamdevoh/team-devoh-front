import { useEffect, useState } from 'react';

const useOnlyTask = ({ tasks }) => {
  const [items, setItems] = useState([]);

  const build = () => {
    const taskItems = _.filter(tasks, { isTask: true });

    let removeTargets = [];

    taskItems.forEach(taskItem => {
      let parents = [];
      taskItem.parents = '';
      recursiveTask(
        taskItem.parentTaskItemId,
        parents,
        taskItem,
        removeTargets
      );
    });

    _.remove(taskItems, task => {
      return removeTargets.indexOf(task.taskItemId) > -1;
    });

    setItems(taskItems);
  };

  const recursiveTask = (
    parentTaskItemId,
    parents,
    taskItem,
    removeTargets
  ) => {
    const parent = _.find(tasks, { taskItemId: parentTaskItemId });
    if (parent) {
      parents.push(parent);
      recursiveTask(parent.parentTaskItemId, parents, taskItem, removeTargets);
    } else {
      if (parentTaskItemId === 0) {
        taskItem.parents = _.map(_.reverse(parents), 'title').join(' > ');
      } else {
        removeTargets.push(taskItem.taskItemId);
      }
    }
  };

  useEffect(
    () => {
      build();
    },
    [tasks]
  );

  return { items };
};

export default useOnlyTask;
