import React from 'react';
import { Header } from 'semantic-ui-react';

const TaskProgressHeader = () => {
  return (
    <Header as="h3" dividing>
      Task Progress
    </Header>
  );
};

export default TaskProgressHeader;
