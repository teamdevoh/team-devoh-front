import React, { Fragment, useCallback, useState } from 'react';
import { Table } from 'semantic-ui-react';
import { TaskProgressListItem } from '.';
import { useFormatMessage } from '../../hooks';
import TaskProgressStatusBar from './TaskProgressStatusBar';

const TaskProgressList = ({ projects, sites }) => {
  const t = useFormatMessage();
  const [selectedStatusCodes, setSelectedStatusCodes] = useState([]);

  const handleStatusBarChange = useCallback(statusCodes => {
    setSelectedStatusCodes(statusCodes);
  }, []);

  return (
    <Fragment>
      <TaskProgressStatusBar
        projects={projects}
        onChange={handleStatusBarChange}
      />
      <Table celled selectable striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="four">{t('project')}</Table.HeaderCell>
            <Table.HeaderCell width="two">
              {t('project.protocolno')}
            </Table.HeaderCell>
            <Table.HeaderCell width="two">{t('project.site')}</Table.HeaderCell>
            <Table.HeaderCell width="two">{t('pi')}</Table.HeaderCell>
            <Table.HeaderCell width="three">
              {t('task.schedule')}
            </Table.HeaderCell>
            <Table.HeaderCell width="three">{t('task.rate')}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {_.filter(projects, project => {
            return (
              selectedStatusCodes.length === 0 ||
              selectedStatusCodes.indexOf(project.statusCode.toString()) > -1
            );
          }).map(item => {
            const sitesByProject = _.filter(sites, {
              projectId: item.projectId
            });
            return (
              <TaskProgressListItem
                key={item.projectId}
                projectId={item.projectId}
                title={item.title}
                sites={sitesByProject}
              />
            );
          })}
        </Table.Body>
      </Table>
    </Fragment>
  );
};

export default TaskProgressList;
