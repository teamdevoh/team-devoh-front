import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { Card, Icon } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';

const ConsoleProjectCardNewProject = ({ projectGroupId }) => {
  const history = useHistory();
  const t = useFormatMessage();

  const handleClick = useCallback(
    () => {
      history.push(`/project/new?projectGroupId=${projectGroupId}`);
    },
    [projectGroupId]
  );

  return (
    <Card link raised style={{ minHeight: '250px' }} onClick={handleClick}>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          minHeight: '250px'
        }}
      >
        <div>
          <Icon name="plus" />
        </div>
        <div>{t('project.add')}</div>
      </div>
    </Card>
  );
};

export default ConsoleProjectCardNewProject;
