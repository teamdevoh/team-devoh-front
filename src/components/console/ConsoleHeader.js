import React, { Fragment } from 'react';
import { Header } from 'semantic-ui-react';

const ConsoleHeader = ({ title }) => {
  return (
    <Fragment>
      <Header as="h2" dividing>
        {title}
      </Header>
    </Fragment>
  );
};

export default ConsoleHeader;
