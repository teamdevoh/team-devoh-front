import React, { Fragment, useCallback } from 'react';
import { Grid, Header, Placeholder } from 'semantic-ui-react';
import { codeGroup, format } from '../../constants';
import helpers from '../../helpers';
import { useCodes, useFormatMessage } from '../../hooks';
import { useMenuStatus, useProjectStatus } from '../app/hooks';
import { useProjectInfo } from '../project-manage/hooks';
import { useTodos } from '../task-manage/hooks';
import { CardExtraColumn, SpanWithBlack, StyledCard } from './styles';
import { useHistory } from 'react-router-dom';
import {
  StatisticSite,
  StatisticMember,
  StatisticMyTask,
  StatisticTaskProgress
} from './';

const ConsoleProjectCard = ({ projectId, projectTeam, variable }) => {
  const t = useFormatMessage();
  const history = useHistory();
  const projectStatus = useProjectStatus();
  const menuStatus = useMenuStatus();
  const projectInfo = useProjectInfo({ projectId });
  const todo = useTodos({ projectId });
  const [projectStatusCodes] = useCodes({
    codeGroupId: codeGroup.ProjectStatusCode
  });

  const foundProjectStatus = _.find(projectStatusCodes, {
    key: projectInfo.item.statusCode
  });

  const switchProject = useCallback(
    async () => {
      const isRefreshed = await projectStatus.active({
        projectId: projectId,
        localTitle: projectInfo.item.localTitle,
        title: projectInfo.item.title,
        projectTeam: projectTeam,
        periodFrom: projectInfo.item.periodFrom,
        periodTo: projectInfo.item.periodTo,
        protocolNo: projectInfo.item.protocolNo,
        variable: variable
      });
      return isRefreshed;
    },
    [projectInfo, projectId, projectTeam, variable]
  );

  const handleCardClick = useCallback(
    async () => {
      const isRefreshed = await switchProject();
      if (isRefreshed) {
        menuStatus.active({ projectId, variable });
      }
    },
    [projectInfo, projectId, projectTeam, variable]
  );

  const handleSiteClick = useCallback(
    async () => {
      const isRefreshed = await switchProject();
      if (isRefreshed) {
        history.push(`/project/${projectId}/site`);
      }
    },
    [projectInfo, projectId, projectTeam, variable]
  );

  const handleMemberClick = useCallback(
    async () => {
      const isRefreshed = await switchProject();
      if (isRefreshed) {
        history.push(`/project/${projectId}/member`);
      }
    },
    [projectInfo, projectId, projectTeam, variable]
  );

  const handleMyTaskClick = useCallback(
    async () => {
      const isRefreshed = await switchProject();
      if (isRefreshed) {
        history.push(`/project/${projectId}/task/portal/my`);
      }
    },
    [projectInfo, projectId, projectTeam, variable]
  );

  const handleTaskRateClick = useCallback(
    async () => {
      const isRefreshed = await switchProject();
      if (isRefreshed) {
        history.push(`/project/${projectId}/task/portal/all`);
      }
    },
    [projectInfo, projectId, projectTeam, variable]
  );

  return (
    <StyledCard link raised>
      {projectInfo.loading ? (
        <Placeholder>
          <Placeholder.Image square />
        </Placeholder>
      ) : (
        <StyledCard.Content onClick={handleCardClick}>
          <StyledCard.Header>{projectInfo.item.title}</StyledCard.Header>
          <StyledCard.Meta>{projectInfo.item.localTitle}</StyledCard.Meta>
          <StyledCard.Description>
            <div>
              {helpers.util.dateRange(
                projectInfo.item.periodFrom,
                projectInfo.item.periodTo,
                format.YYYY_MM_DD
              )}
            </div>
            {projectInfo.item.note && (
              <div style={{ paddingTop: '0.5rem' }}>
                <Header as="h4" dividing>
                  Note
                </Header>
                {projectInfo.item.note}
              </div>
            )}
          </StyledCard.Description>
        </StyledCard.Content>
      )}
      <StyledCard.Content extra onClick={handleCardClick}>
        {projectInfo.loading ? (
          <Fragment>
            <Placeholder>
              <Placeholder.Header>
                <Placeholder.Line length="medium" />
                <Placeholder.Line length="short" />
                <Placeholder.Line length="long" />
              </Placeholder.Header>
            </Placeholder>
          </Fragment>
        ) : (
          <Grid columns={2}>
            <CardExtraColumn width="sixteen">
              {t('project.protocolno')}:
              <SpanWithBlack>{projectInfo.item.protocolNo}</SpanWithBlack>
            </CardExtraColumn>
            <CardExtraColumn width="sixteen">
              {t('project.status')}:
              <SpanWithBlack>
                {foundProjectStatus ? foundProjectStatus.text : ''}
              </SpanWithBlack>
            </CardExtraColumn>
            <CardExtraColumn width="sixteen">
              {t('role')}:<SpanWithBlack>{projectTeam}</SpanWithBlack>
            </CardExtraColumn>
          </Grid>
        )}
      </StyledCard.Content>
      <StyledCard.Content extra>
        <Grid columns={2}>
          <Grid.Column width="four" onClick={handleSiteClick}>
            <StatisticSite
              value={projectInfo.item.siteCount}
              isLoading={projectInfo.loading}
            />
          </Grid.Column>
          <Grid.Column width="four" onClick={handleMemberClick}>
            <StatisticMember
              value={projectInfo.item.memberCount}
              isLoading={projectInfo.loading}
            />
          </Grid.Column>
          <Grid.Column width="four" onClick={handleMyTaskClick}>
            <StatisticMyTask
              value={todo.items.length}
              isLoading={todo.loading}
            />
          </Grid.Column>
          <Grid.Column width="four" onClick={handleTaskRateClick}>
            <StatisticTaskProgress
              value={projectInfo.item.taskProgress}
              isLoading={projectInfo.loading}
            />
          </Grid.Column>
        </Grid>
      </StyledCard.Content>
    </StyledCard>
  );
};

export default ConsoleProjectCard;
