import React, { useCallback, useEffect, useState } from 'react';
import StepGroup from '../element/StepGroup';
import groupBy from 'lodash/groupBy';

const TaskProgressStatusBar = ({ projects, onChange }) => {
  const initStatus = [
    {
      name: 'Register',
      code: [],
      value: 'Register',
      active: true,
      length: 0
    },
    {
      name: 'Ongoing',
      code: ['27', '28'],
      value: 'Ongoing',
      active: false,
      length: 0
    },
    {
      name: 'Complete',
      code: ['29'],
      value: 'Complete',
      active: false,
      length: 0
    },
    {
      name: 'Expected',
      code: ['24', '25', '26'],
      value: 'Expected',
      active: false,
      length: 0
    },
    {
      name: 'Discontinuation',
      code: ['30'],
      value: 'Discontinuation',
      active: false,
      length: 0
    }
  ];

  const [statusSet, setStatusSet] = useState(initStatus);

  const sum = (value, predicate) => {
    const item = _.find(initStatus, predicate);
    item.length += value;
  };

  const calcLength = () => {
    const statusGroups = groupBy(projects, 'statusCode');

    Object.keys(statusGroups).forEach(key => {
      sum(statusGroups[key].length, breadCrumb => {
        return breadCrumb.code.indexOf(key) > -1;
      });
      sum(statusGroups[key].length, { value: 'Register' });
    });

    setStatusSet(initStatus);
  };

  useEffect(
    () => {
      calcLength();
    },
    [projects]
  );

  const handleStepClick = useCallback(
    (event, data) => {
      let selectedStepCodes = [];

      statusSet.forEach(status => {
        if (status.value === data.value) {
          status.active = true;
          selectedStepCodes = status.code;
        } else {
          status.active = false;
        }
      });

      onChange(selectedStepCodes);
    },
    [statusSet]
  );

  return <StepGroup items={statusSet} onClick={handleStepClick} />;
};

export default TaskProgressStatusBar;
