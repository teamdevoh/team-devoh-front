import React, { Fragment } from 'react';
import { Label } from 'semantic-ui-react';
import helpers from '../../helpers';
import TaskSchedulerBarTag from './TaskSchedulerBarTag';
import TaskSchedulerMember from './TaskSchedulerMember';
import TaskSchedulerProgressBar from './TaskSchedulerProgressBar';

const TaskSchedulerBar = ({
  projectId,
  item,
  color,
  members,
  scheduleDates = []
}) => {
  const util = helpers.util;

  return (
    <Fragment>
      <TaskSchedulerProgressBar
        color={color}
        percent={item.rate}
        from={item.scheduleFromDate}
        to={item.scheduleToDate}
        scheduleDates={scheduleDates}
      />
      <Label color="black" size="tiny">
        <p style={{ marginBottom: 0 }}>
          <TaskSchedulerMember
            projectId={projectId}
            taskItemId={item.taskItemId}
            projectMembers={members}
          />
        </p>
        <p style={{ marginBottom: 0 }}>
          {util.dateformat(item.scheduleFromDate, 'YYYY-MM-DD')} ~
          {util.dateformat(item.scheduleToDate, 'YYYY-MM-DD')}
        </p>
        {item.tags && (
          <p>
            <TaskSchedulerBarTag tags={item.tags} />
          </p>
        )}
      </Label>
    </Fragment>
  );
};

export default TaskSchedulerBar;
