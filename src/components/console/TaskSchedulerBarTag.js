import React, { Fragment } from 'react';
import { Label } from 'semantic-ui-react';

const TaskSchedulerBarTag = ({ tags }) => {
  return (
    <Fragment>
      {tags.map((tag, i) => {
        return (
          <Label size="mini" color={tag.color.toLowerCase()} key={i}>
            {tag.name}
          </Label>
        );
      })}
    </Fragment>
  );
};

export default TaskSchedulerBarTag;
