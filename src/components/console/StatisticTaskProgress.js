import React, { Fragment } from 'react';
import { Icon, Placeholder, Statistic } from 'semantic-ui-react';

const StatisticTaskProgress = ({ isLoading, value }) => {
  return (
    <Fragment>
      {isLoading ? (
        <Placeholder>
          <Placeholder.Image style={{ height: '45px' }} />
        </Placeholder>
      ) : (
        <Statistic color="blue" size="mini">
          <Statistic.Value>{value}%</Statistic.Value>
          <Statistic.Label>
            <a>
              <Icon.Group size="large">
                <Icon name="tasks" />
                <Icon corner name="percent" />
              </Icon.Group>
            </a>
          </Statistic.Label>
        </Statistic>
      )}
    </Fragment>
  );
};

export default StatisticTaskProgress;
