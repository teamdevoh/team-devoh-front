import React from 'react';
import { useDispatch } from 'react-redux';
import { Grid } from 'semantic-ui-react';
import { project } from '../../states';
import { ConsoleHeader } from './';
import { StyledConsoleWrapper } from './styles';

const ConsoleWrapper = ({ children, title }) => {
  const dispatch = useDispatch();
  dispatch(project.actions.clearSelectedItem());

  return (
    <StyledConsoleWrapper>
      {title && (
        <Grid.Row>
          <Grid.Column>
            <ConsoleHeader title={title} />
          </Grid.Column>
        </Grid.Row>
      )}
      {children}
    </StyledConsoleWrapper>
  );
};

export default ConsoleWrapper;
