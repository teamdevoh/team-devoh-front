import React from 'react';
import { Progress } from 'semantic-ui-react';
import helpers from '../../helpers';

const TaskSchedulerProgressBar = ({
  color,
  percent,
  from,
  to,
  scheduleDates = []
}) => {
  const util = helpers.util;

  const width = 100;
  let scheduleRange = { from: scheduleDates[0], to: scheduleDates[2] };
  let diffDays = { from: 0, to: 0 };

  scheduleDates.forEach(scheduleDate => {
    if (scheduleDate === util.dateformat(from, 'YYYY-MM')) {
      scheduleRange.from = scheduleDate;

      diffDays.from = util.diffDays(`${scheduleDate}-01`, from);
    }
    if (scheduleDate === util.dateformat(to, 'YYYY-MM')) {
      scheduleRange.to = scheduleDate;
      const lastDayOfMonth = util.lastDayOfMonth(`${scheduleDate}-01`);

      diffDays.to = util.diffDays(to, lastDayOfMonth.format('YYYY-MM-DD'));
    }
  });

  const getAllDays = (rangeFrom, rangeTo) => {
    const allDays = util.diffDays(
      `${rangeFrom}-01`,
      util.lastDayOfMonth(`${rangeTo}-01`)
    );

    return allDays;
  };

  const getPosition = (allDays, fromDiffDays, toDiffDays) => {
    const eachDayWidth = width / allDays;

    return {
      start: eachDayWidth * fromDiffDays,
      end: eachDayWidth * toDiffDays
    };
  };

  const allDays = getAllDays(scheduleRange.from, scheduleRange.to);
  const position = getPosition(allDays, diffDays.from, diffDays.to);

  return (
    <Progress
      color={color}
      percent={percent}
      size="small"
      progress
      style={{
        width: `${width - position.start - position.end}%`,
        marginLeft: `${position.start}%`,
        marginRight: `${position.end}%`,
        marginBottom: '3px'
      }}
    />
  );
};

export default TaskSchedulerProgressBar;
