import styled from 'styled-components';
import { Card, Grid } from 'semantic-ui-react';

export const StyledConsoleWrapper = styled(Grid)`
  &&& {
    background: linear-gradient(
      to right,
      rgba(30, 75, 115, 1),
      rgba(255, 255, 255, 0)
    );
  }
`;

export const CardExtraColumn = styled(Grid.Column)`
  &&&& {
    padding-bottom: 0rem;
  }
`;

export const StyledCard = styled(Card)`
  &&&& {
    height: 400px;
    &:hover {
      background-color: yellow;
    }
  }
`;

export const SpanWithBlack = styled.span`
  color: black;
`;
