import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Label, Table } from 'semantic-ui-react';
import { useProjectInfo } from '../project-manage/hooks';
import { TaskListContainer } from '../task-manage';

const TaskProgressListItem = ({ projectId, title, sites }) => {
  const [visibleTask, setVisibleTask] = useState(false);
  const projectInfo = useProjectInfo({ projectId });
  const [siteName, setSiteName] = useState('');
  const [siteCount, setSiteCount] = useState(0);
  const [pi, setPI] = useState('');

  useEffect(
    () => {
      if (sites.length > 0) {
        setSiteName(`${sites[0].siteName}`);
        setSiteCount(sites.length - 1);
      }
    },
    [sites]
  );

  useEffect(
    () => {
      setPI('');
      if (projectInfo.item.ciMemberViews.length > 0) {
        setPI(projectInfo.item.ciMemberViews[0].memberName);
      } else {
        if (projectInfo.item.piMemberViews.length > 0) {
          setPI(projectInfo.item.piMemberViews[0].memberName);
        }
      }
    },
    [
      projectId,
      sites,
      projectInfo.item.ciMemberViews,
      projectInfo.item.piMemberViews
    ]
  );

  const handleTitleClick = useCallback(
    () => {
      setVisibleTask(!visibleTask);
    },
    [visibleTask]
  );

  return (
    <Fragment>
      <Table.Row>
        <Table.Cell>
          <a className="link" onClick={handleTitleClick}>
            {title}
          </a>
        </Table.Cell>
        <Table.Cell>{projectInfo.item.protocolNo}</Table.Cell>
        <Table.Cell>
          {siteName} {siteCount > 0 && <Label color="teal">{siteCount}+</Label>}
        </Table.Cell>
        <Table.Cell>{pi}</Table.Cell>
        <Table.Cell>{projectInfo.item.taskSchedule}</Table.Cell>
        <Table.Cell>{projectInfo.item.taskProgress} %</Table.Cell>
      </Table.Row>
      {visibleTask && (
        <Table.Row>
          <Table.Cell collapsing colSpan="6">
            <TaskListContainer projectId={projectId} />
          </Table.Cell>
        </Table.Row>
      )}
    </Fragment>
  );
};

export default TaskProgressListItem;
