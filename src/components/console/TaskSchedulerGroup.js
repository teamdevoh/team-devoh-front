import React, { Fragment, useCallback, useState } from 'react';
import { Table } from 'semantic-ui-react';
import helpers from '../../helpers';
import { useFormatMessage, useProjectMemberKeyPair } from '../../hooks';
import { useTaskList } from '../task-manage/hooks';
import TaskSchedulerBar from './TaskSchedulerBar';
import moment from 'moment';
import { useOnlyTask } from './hooks';
import { TaskEditLayer } from '../task-manage';
import { notification } from '../modal';

const TaskSchedulerGroup = ({ project, scheduleDates }) => {
  const t = useFormatMessage();
  const [selectedTaskItemId, setSelectedTaskItemId] = useState(0);
  const [showTaskEdit, setShowTaskEdit] = useState(false);
  const task = useTaskList({ projectId: project.projectId });
  const { items } = useOnlyTask({ tasks: task.items });
  const member = useProjectMemberKeyPair({
    projectId: project.projectId
  });

  const getTargetItems = taskItems => {
    const filteredItems = _.filter(taskItems, item => {
      const from = helpers.util.dateformat(item.scheduleFromDate, 'YYYY-MM');
      const to = helpers.util.dateformat(item.scheduleToDate, 'YYYY-MM');

      const positions = isBetween(from, to);
      if (positions.length > 0) {
        return item;
      }
    });
    return filteredItems;
  };

  const isBetween = (from, to) => {
    let positions = [];
    scheduleDates.forEach((scheduleDate, index) => {
      if (scheduleDate >= from && scheduleDate <= to) {
        positions.push(index);
      }
    });
    return positions;
  };

  const handleTaskClick = useCallback(taskItemId => {
    setSelectedTaskItemId(taskItemId);
    setShowTaskEdit(true);
  }, []);

  const targetItems = _.orderBy(
    getTargetItems(items),
    'scheduleFromDate',
    'asc'
  );

  return (
    <Fragment>
      {targetItems.map((item, index) => {
        const from = helpers.util.dateformat(item.scheduleFromDate, 'YYYY-MM');
        const to = helpers.util.dateformat(item.scheduleToDate, 'YYYY-MM');

        const positions = isBetween(from, to);
        let skips = 0;

        const isOver =
          moment().format('YYYY-MM-DD') >
          helpers.util.dateformat(item.scheduleToDate, 'YYYY-MM-DD');

        const barColor =
          item.rate === 100
            ? 'green'
            : isOver && item.rate < 100
              ? 'red'
              : 'blue';

        return (
          <Table.Row key={item.taskItemId}>
            {index === 0 && (
              <Table.Cell rowSpan={targetItems.length}>
                {project.title}
              </Table.Cell>
            )}
            <Table.Cell>
              <a
                className="link"
                onClick={() => {
                  handleTaskClick(item.taskItemId);
                }}
              >
                {item.parents.length > 0
                  ? `${item.parents} > ${item.title}`
                  : item.title}
              </a>
            </Table.Cell>

            {_.times(4 - positions.length, n => {
              if (positions[n - skips] === n) {
                skips += positions.length;
                return (
                  <Table.Cell
                    key={n}
                    textAlign="center"
                    style={{ paddingLeft: 0, paddingRight: 0 }}
                    colSpan={positions.length}
                  >
                    <TaskSchedulerBar
                      projectId={project.projectId}
                      item={item}
                      members={member.items}
                      color={barColor}
                      scheduleDates={scheduleDates}
                    />
                  </Table.Cell>
                );
              } else {
                skips += 1;
                return (
                  <Table.Cell
                    key={n}
                    style={{ paddingLeft: 0, paddingRight: 0 }}
                    colSpan="1"
                  />
                );
              }
            })}
          </Table.Row>
        );
      })}

      {showTaskEdit && (
        <TaskEditLayer
          open={showTaskEdit}
          onClose={() => {
            setShowTaskEdit(false);
          }}
          title={t('task.title.edit')}
          projectId={project.projectId}
          taskItemId={selectedTaskItemId}
          onSaved={model => {
            setShowTaskEdit(false);
            notification.success({
              title: t('common.alert.changed'),
              onClose: () => {}
            });
            task.refresh();
          }}
          onRemoved={() => {
            notification.success({
              title: t('common.alert.deleted'),
              onClose: () => {}
            });
            task.refresh();
          }}
        />
      )}
    </Fragment>
  );
};

export default TaskSchedulerGroup;
