import React from 'react';
import { DataGrid } from '../data-grid';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { Responsive } from 'semantic-ui-react';
import { format } from '../../constants';

const IdentityLog = ({ items, loading, onRowVisibleEnd }) => {
  const isMobile =
    Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;
  const t = useFormatMessage();

  const columns = [
    {
      key: 'subjectNo',
      name: t('collection.subject.no')
    },
    {
      key: 'name',
      name: t('collection.subject.name')
    },
    {
      key: 'initial',
      name: t('collection.subject.initial')
    },
    {
      key: 'hospitalNo',
      name: t('collection.hospital.no')
    },
    {
      key: 'brthdtc',
      name: t('activity.interview.brthdtc'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      )
    },
    {
      key: 'consdtc',
      name: t('date.signed.ic'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      )
    },
    {
      key: 'subjectStatusName',
      name: t('status')
    },
    {
      key: 'contactNo',
      name: t('collection.subject.contact')
    },
    {
      key: 'address',
      name: t('collection.subject.address')
    }
  ];

  return (
    <div style={{ marginRight: '1em' }}>
      <DataGrid
        columns={columns}
        rows={items}
        loading={loading}
        minHeight={isMobile ? 200 : 600}
        onRowVisibleEnd={onRowVisibleEnd}
        rowKey="subjectId"
        rowNumber={{ show: true, key: 'no', name: 'No' }}
      />
    </div>
  );
};

export default IdentityLog;
