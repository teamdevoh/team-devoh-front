import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';
import { useFormatMessage } from '../../../hooks';
import { format, role } from '../../../constants';

const useSubjectIdentityLogs = ({
  projectId,
  projectSiteId,
  subjectStatusCodeId
}) => {
  const [total, setTotal] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [exportLoading, setExportLoading] = useState(false);
  const t = useFormatMessage();

  const fetchItems = useCallback(
    async ({ offset, limit }) => {
      if (helpers.Identity.hasRole(role.SubjectIdentityLog_Read)) {
        setLoading(true);
        try {
          const res = await api.fetchSubjectIdentityLogs({
            projectId,
            projectSiteId,
            subjectStatusCodeId,
            limit,
            offset
          });
          if (res && res.data) {
            setTotal(res.data.paging.total);
          }
          return res;
        } catch (error) {
        } finally {
          setLoading(false);
        }
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  const fetchOriginItems = useCallback(
    async ({ authKey }) => {
      if (helpers.Identity.hasRole(role.SubjectIdentityLog_Read)) {
        setLoading(true);
        try {
          const res = await api.fetchSubjectIdentityOriginLogs({
            projectId,
            projectSiteId,
            subjectStatusCodeId,
            authKey
          });
          return res;
        } catch (error) {
        } finally {
          setLoading(false);
        }
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  const resetItems = useCallback(
    async () => {
      const res = await fetchItems({ offset: 0, limit: 50 });
      if (res && res.data) {
        setItems(res.data.items);
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  const more = useCallback(
    async () => {
      if (total > items.length) {
        const res = await fetchItems({ offset: items.length, limit: 50 });
        if (res && res.data) {
          setItems(items.concat(res.data.items));
        }
      }
    },
    [items, total]
  );

  const exportCSV = useCallback(
    async ({ fileName, authKey }) => {
      try {
        setExportLoading(true);
        const res = await fetchOriginItems({ authKey });
        if (res && res.data) {
          res.data.items.forEach(item => {
            item.brthdtc = helpers.util.dateformat(
              item.brthdtc,
              format.YYYY_MM_DD
            );
            item.consdtc = helpers.util.dateformat(
              item.consdtc,
              format.YYYY_MM_DD
            );
          });

          helpers.util.exportCSV({
            fileName: `${fileName}.csv`,
            data: res.data.items,
            fields: [
              { key: 'subjectNo' },
              { key: 'name' },
              { key: 'initial' },
              { key: 'hospitalNo' },
              { key: 'brthdtc' },
              { key: 'consdtc' },
              { key: 'subjectStatusName' },
              { key: 'contactNo' },
              { key: 'address' }
            ],
            fieldNames: [
              t('collection.subject.no'),
              t('collection.subject.name'),
              t('collection.subject.initial'),
              t('collection.hospital.no'),
              t('activity.interview.brthdtc'),
              t('date.signed.ic'),
              t('status'),
              t('collection.subject.contact'),
              t('collection.subject.address')
            ]
          });

          return res.data.items;
        }
        return [];
      } catch (error) {
      } finally {
        setExportLoading(false);
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  useEffect(
    () => {
      if (projectId && projectSiteId > 0) {
        setItems([]);
        resetItems();
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  return { items, loading, total, more, exportLoading, exportCSV };
};

export default useSubjectIdentityLogs;
