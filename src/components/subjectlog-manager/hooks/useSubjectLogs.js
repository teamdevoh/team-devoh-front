import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import { useFormatMessage } from '../../../hooks';
import helpers from '../../../helpers';
import { format } from '../../../constants';

const useSubjectLogs = ({ projectId, projectSiteId, subjectStatusCodeId }) => {
  const [total, setTotal] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const t = useFormatMessage();

  const fetchItems = useCallback(
    async ({ offset, limit }) => {
      setLoading(true);
      try {
        const res = await api.fetchSubjectLogs({
          projectId,
          projectSiteId,
          subjectStatusCodeId,
          limit,
          offset
        });
        if (res && res.data) {
          setTotal(res.data.paging.total);
        }
        return res;
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  const resetItems = useCallback(
    async () => {
      const res = await fetchItems({ offset: 0, limit: 50 });
      if (res && res.data) {
        setItems(res.data.items);
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  const more = useCallback(
    async () => {
      if (total > items.length) {
        const res = await fetchItems({ offset: items.length, limit: 50 });
        if (res && res.data) {
          setItems(items.concat(res.data.items));
        }
      }
    },
    [items, total]
  );

  const exportCSV = useCallback(
    async fileName => {
      const res = await fetchItems({ offset: null, limit: null });
      if (res && res.data) {
        res.data.items.forEach(item => {
          item.brthdtc = helpers.util.dateformat(
            item.brthdtc,
            format.YYYY_MM_DD
          );
          item.consdtcSigned = helpers.util.dateformat(
            item.consdtcSigned,
            format.YYYY_MM_DD
          );
          item.consdtcScreeningTests = helpers.util.dateformat(
            item.consdtcScreeningTests,
            format.YYYY_MM_DD
          );
          item.consdtcRandom = helpers.util.dateformat(
            item.consdtcRandom,
            format.YYYY_MM_DD
          );
          item.dropoutDate = helpers.util.dateformat(
            item.dropoutDate,
            format.YYYY_MM_DD
          );
        });
        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          data: res.data.items,
          fields: [
            { key: 'initial' },
            { key: 'sex' },
            { key: 'brthdtc' },
            { key: 'consdtcSigned' },
            { key: 'consdtcScreeningTests' },
            { key: 'consdtcRandom' },
            { key: 'subjectNo' },
            { key: 'subjectStatusName' },
            { key: 'dropoutDate' },
            { key: 'dropoutReason' },
            { key: 'dropoutDetail' }
          ],
          fieldNames: [
            t('collection.subject.initial'),
            t('activity.interview.sexcodeId'),
            t('activity.interview.brthdtc'),
            t('date.signed.ic'),
            t('date.screening.tests'),
            t('date.random'),
            t('collection.subject.no'),
            t('status'),
            t('collection.subject.dropout.date'),
            t('subject.dropout.reason'),
            t('dropout.detail')
          ]
        });

        return res.data.items;
      }
      return [];
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  useEffect(
    () => {
      if (projectId && projectSiteId > 0) {
        setItems([]);
        resetItems();
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  return { items, loading, total, more, exportCSV };
};

export default useSubjectLogs;
