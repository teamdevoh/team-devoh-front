import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import { useFormatMessage } from '../../../hooks';
import helpers from '../../../helpers';
import { format } from '../../../constants';

const useSubjectVisitLogs = ({
  projectId,
  projectSiteId,
  subjectStatusCodeId
}) => {
  const [total, setTotal] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const t = useFormatMessage();

  const fetchItems = useCallback(
    async ({ offset, limit }) => {
      setLoading(true);
      try {
        const res = await api.fetchSubjectVisitLogs({
          projectId,
          projectSiteId,
          subjectStatusCodeId,
          limit,
          offset
        });
        if (res && res.data) {
          setTotal(res.data.paging.total);
        }
        return res;
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  const resetItems = useCallback(
    async () => {
      const res = await fetchItems({ offset: 0, limit: 50 });
      if (res && res.data) {
        setItems(res.data.items);
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  const more = useCallback(
    async () => {
      if (total > items.length) {
        const res = await fetchItems({ offset: items.length, limit: 50 });
        if (res && res.data) {
          setItems(items.concat(res.data.items));
        }
      }
    },
    [items, total]
  );

  const exportCSV = useCallback(
    async fileName => {
      const res = await fetchItems({ offset: null, limit: null });
      if (res && res.data) {
        res.data.items.forEach(item => {
          item.enroll = helpers.util.dateformat(item.enroll, format.YYYY_MM_DD);
          item.firstTdm = helpers.util.dateformat(
            item.firstTdm,
            format.YYYY_MM_DD
          );
          item.secondTdm = helpers.util.dateformat(
            item.secondTdm,
            format.YYYY_MM_DD
          );
          item.thirdTdm = helpers.util.dateformat(
            item.thirdTdm,
            format.YYYY_MM_DD
          );
          item.fourthTdm = helpers.util.dateformat(
            item.fourthTdm,
            format.YYYY_MM_DD
          );
          item.reicdat = helpers.util.dateformat(
            item.reicdat,
            format.YYYY_MM_DD
          );
        });
        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          data: res.data.items,
          fields: [
            {
              key: 'subjectNo'
            },
            {
              key: 'enroll'
            },
            {
              key: 'firstTdm'
            },
            {
              key: 'secondTdm'
            },
            {
              key: 'thirdTdm'
            },
            {
              key: 'fourthTdm'
            },
            {
              key: 'unscheduledTdm'
            },
            {
              key: 'reicdat'
            },
            {
              key: 'caseConclusion'
            }
          ],
          fieldNames: [
            t('collection.subject.no'),
            t('subject.enroll'),
            '1st TDM',
            '2nd TDM',
            '3rd TDM',
            '4th TDM',
            'Unscheduled Visit',
            t('activity.icre.title'),
            t('activity.caseConclusion.title')
          ]
        });

        return res.data.items;
      }
      return [];
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  useEffect(
    () => {
      if (projectId && projectSiteId > 0) {
        setItems([]);
        resetItems();
      }
    },
    [projectId, projectSiteId, subjectStatusCodeId]
  );

  return { items, loading, total, more, exportCSV };
};

export default useSubjectVisitLogs;
