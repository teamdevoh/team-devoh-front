import { asyncComponent } from '../modules';

const SubjectLogContainer = asyncComponent(() =>
  import('./SubjectLogContainer')
);

export default SubjectLogContainer;
