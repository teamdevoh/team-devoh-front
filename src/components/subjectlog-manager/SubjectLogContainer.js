import React, { useCallback, useEffect, useState, Fragment } from 'react';
import {
  Tab,
  Dropdown,
  Form,
  Header,
  Grid,
  Button,
  Input
} from 'semantic-ui-react';
import IdentityLog from './IdentityLog';
import Log from './Log';
import VisitLog from './VisitLog';
import { useFormatMessage, useCodes } from '../../hooks';
import { useParams } from 'react-router-dom';
import useSubjectLogs from './hooks/useSubjectLogs';
import useSubjectVisitLogs from './hooks/useSubjectVisitLogs';
import useSubjectIdentityLogs from './hooks/useSubjectIdentityLogs';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';
import helpers from '../../helpers';
import { useIdentityVerify } from '../account-manage/hooks';
import { notification } from '../modal';
import { code, role } from '../../constants';

const SubjectLogContainer = () => {
  const t = useFormatMessage();
  const identityVerify = useIdentityVerify();
  const [selectedSiteId, setSelectedSiteId] = useState(0);
  const [selectedSiteName, setSelectedSiteName] = useState('');
  const [authKey, setAuthKey] = useState('');
  const [selectedSubjectStatusId, setSelectedSubjectStatusId] = useState(0);
  const { projectId, index } = useParams();
  // const location = useLocation();
  // const params = helpers.qs.parse(location.search);

  const [sites] = useMyProjectSiteList({ projectId });
  const [subjectStatus] = useCodes({ codeGroupId: code.Completed });

  const subjectLog = useSubjectLogs({
    projectId: projectId,
    projectSiteId: selectedSiteId,
    subjectStatusCodeId: selectedSubjectStatusId
  });

  const subjectVisitLog = useSubjectVisitLogs({
    projectId: projectId,
    projectSiteId: selectedSiteId,
    subjectStatusCodeId: selectedSubjectStatusId
  });

  const subjectIdentityLog = useSubjectIdentityLogs({
    projectId: projectId,
    projectSiteId: selectedSiteId,
    subjectStatusCodeId: selectedSubjectStatusId
  });

  const handleTabChange = useCallback((event, data) => {
    switch (data.activeIndex) {
      case 0:
        helpers.history.push(`/project/${projectId}/subject/log/0`);
        break;
      case 1:
        helpers.history.push(`/project/${projectId}/subject/log/1`);
        break;
      case 2:
        helpers.history.push(`/project/${projectId}/subject/log/2`);
        break;
      default:
        break;
    }
  }, []);

  const handleChangeProjectSite = useCallback((event, data) => {
    const selectedSite = _.find(data.options, opt => {
      return opt.value === data.value;
    });
    setSelectedSiteName(selectedSite.text);
    setSelectedSiteId(data.value);
  }, []);

  const handleChangeSubjectStatus = useCallback((event, data) => {
    setSelectedSubjectStatusId(data.value);
  }, []);

  const handleExportSubjectLog = useCallback(
    () => {
      subjectLog.exportCSV(`Subject Log ${selectedSiteName}`);
    },
    [selectedSiteName]
  );

  const handleExportSubjectVisitLog = useCallback(
    () => {
      subjectVisitLog.exportCSV(`Subject Visit Log ${selectedSiteName}`);
    },
    [selectedSiteName]
  );

  const handleExportSubjectIdentityLog = useCallback(
    () => {
      if (authKey) {
        subjectIdentityLog.exportCSV({
          fileName: `Subject Identification Log ${selectedSiteName}`,
          authKey: authKey
        });
      }
    },
    [selectedSiteName, authKey]
  );

  const handleIdentityVerifyClick = useCallback(async () => {
    await identityVerify.send();
  });

  const handleAuthKeyChange = useCallback((event, data) => {
    setAuthKey(data.value);
  }, []);

  useEffect(
    () => {
      if (sites.length > 0) {
        const defaultItem = sites[0];
        setSelectedSiteId(defaultItem.value);
        setSelectedSiteName(defaultItem.text);
      }
    },
    [sites]
  );

  let panes = [
    {
      menuItem: `${t('subject.log')} : ${subjectLog.total}`,
      render: () => (
        <Tab.Pane basic active={index === '0'}>
          <Button
            positive
            icon="download"
            content="CSV Export"
            onClick={handleExportSubjectLog}
          />
          <Log
            items={subjectLog.items}
            loading={subjectLog.loading}
            onRowVisibleEnd={subjectLog.more}
          />
        </Tab.Pane>
      )
    }
  ];

  if (helpers.Identity.hasRole(role.SubjectVisitLog_Read)) {
    panes.push({
      menuItem: `${t('subject.visit.log')} : ${subjectVisitLog.total}`,
      render: () => (
        <Tab.Pane basic active={index === '1'}>
          <Button
            positive
            icon="download"
            content="CSV Export"
            onClick={handleExportSubjectVisitLog}
          />
          <VisitLog
            projectId={projectId}
            items={subjectVisitLog.items}
            loading={subjectVisitLog.loading}
            onRowVisibleEnd={subjectVisitLog.more}
          />
        </Tab.Pane>
      )
    });
  }

  if (helpers.Identity.hasRole(role.SubjectIdentityLog_Read)) {
    panes.push({
      menuItem: `${t('subject.identification.log')} : ${
        subjectIdentityLog.total
      }`,
      render: () => (
        <Tab.Pane basic active={index === '2'}>
          <Button
            disabled={identityVerify.loading}
            loading={identityVerify.loading}
            onClick={handleIdentityVerifyClick}
          >
            {t('common.identity.verification')}
          </Button>
          <Input
            action={{
              color: 'green',
              labelPosition: 'left',
              disabled:
                authKey.length === 0 || subjectIdentityLog.exportLoading,
              loading: subjectIdentityLog.exportLoading,
              icon: 'key',
              content: 'CSV Export',
              onClick: handleExportSubjectIdentityLog
            }}
            onChange={handleAuthKeyChange}
            placeholder="12345"
          />
          <IdentityLog
            items={subjectIdentityLog.items}
            loading={subjectIdentityLog.loading}
            onRowVisibleEnd={subjectIdentityLog.more}
          />
        </Tab.Pane>
      )
    });
  }

  useEffect(
    () => {
      if (identityVerify.isSent) {
        notification.info({
          title: t('common.identity.verification.email.sent'),
          text: '',
          confirmButtonName: 'OK',
          onClose: () => {}
        });
      }
    },
    [identityVerify.isSent]
  );

  return (
    <Fragment>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as="h3" dividing>
              Subject Log
              <Header.Subheader>You can see all Subject Logs</Header.Subheader>
              <CopyRedirectUrl />
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Form>
              <Form.Group>
                <Form.Field width="five">
                  <label>{t('site')}</label>
                  <Dropdown
                    search
                    fluid
                    selection
                    options={sites}
                    value={selectedSiteId}
                    onChange={handleChangeProjectSite}
                  />
                </Form.Field>
                <Form.Field width="five">
                  <label>{t('status')}</label>
                  <Dropdown
                    search
                    fluid
                    selection
                    options={[{ key: 0, text: '-', value: 0 }].concat(
                      subjectStatus
                    )}
                    value={selectedSubjectStatusId}
                    onChange={handleChangeSubjectStatus}
                  />
                </Form.Field>
              </Form.Group>
            </Form>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Tab
              panes={panes}
              onTabChange={handleTabChange}
              activeIndex={index}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Fragment>
  );
};

export default SubjectLogContainer;
