import React from 'react';
import { DataGrid } from '../data-grid';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { Responsive } from 'semantic-ui-react';
import { format } from '../../constants';

const VisitLog = ({ projectId, items, loading, onRowVisibleEnd }) => {
  const isMobile =
    Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;
  const t = useFormatMessage();

  const columns = [
    {
      key: 'subjectNo',
      name: t('collection.subject.no'),
      formatter: ({ value, row }) => {
        const { subjectId, initial, age, sex } = row;
        return (
          <a
            style={{ cursor: 'pointer' }}
            onClick={() => {
              helpers.history.push(
                `/project/${projectId}/subject/${subjectId}/step/1`
              );
            }}
          >
            {`${value}/${initial}/${!!age ? age : ''}/${
              !!sex ? sex.substr(0, 1) : ''
            }`}
          </a>
        );
      },
      width: 190
    },
    {
      key: 'enroll',
      name: t('subject.enroll'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      ),
      width: 100
    },
    {
      key: 'firstTdm',
      name: '1st TDM',
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      ),
      width: 100
    },
    {
      key: 'secondTdm',
      name: '2nd TDM',
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      ),
      width: 100
    },
    {
      key: 'thirdTdm',
      name: '3rd TDM',
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      ),
      width: 100
    },
    {
      key: 'fourthTdm',
      name: '4th TDM',
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      ),
      width: 100
    },
    {
      key: 'unscheduledTdm',
      name: 'Unscheduled Visit'
    },
    {
      key: 'reicdat',
      name: t('activity.icre.title'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      ),
      width: 100
    },
    {
      key: 'caseConclusion',
      name: t('activity.caseConclusion.title')
    }
  ];

  return (
    <div style={{ marginRight: '1em' }}>
      <DataGrid
        columns={columns}
        rows={items}
        loading={loading}
        minHeight={isMobile ? 200 : 600}
        onRowVisibleEnd={onRowVisibleEnd}
        rowKey="subjectId"
        rowNumber={{ show: true, key: 'no', name: 'No' }}
      />
    </div>
  );
};

export default VisitLog;
