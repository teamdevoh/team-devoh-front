import helpers from '../../helpers';

const fetchSubjectLogs = ({
  projectId,
  projectSiteId,
  subjectStatusCodeId,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    subjectStatusCodeId,
    offset,
    limit
  });
  return helpers.Service.get(`api/subjects/logs?${params}`);
};

const fetchSubjectVisitLogs = ({
  projectId,
  projectSiteId,
  subjectStatusCodeId,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    subjectStatusCodeId,
    offset,
    limit
  });
  return helpers.Service.get(`api/subjects/logs.visit?${params}`);
};

const fetchSubjectIdentityLogs = ({
  projectId,
  projectSiteId,
  subjectStatusCodeId,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    subjectStatusCodeId,
    offset,
    limit
  });
  return helpers.Service.get(`api/subjects/logs.identity?${params}`);
};

const fetchSubjectIdentityOriginLogs = ({
  projectId,
  projectSiteId,
  subjectStatusCodeId,
  authKey,
  expire,
  loginName
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    subjectStatusCodeId,
    authKey,
    expire,
    loginName
  });
  return helpers.Service.get(`api/subjects/logs.identity.origin?${params}`);
};

export default {
  fetchSubjectLogs,
  fetchSubjectVisitLogs,
  fetchSubjectIdentityLogs,
  fetchSubjectIdentityOriginLogs
};
