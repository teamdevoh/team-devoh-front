import React from 'react';
import { DataGrid } from '../data-grid';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { Responsive } from 'semantic-ui-react';
import { format } from '../../constants';

const Log = ({ items, loading, onRowVisibleEnd }) => {
  const isMobile =
    Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;
  const t = useFormatMessage();

  const columns = [
    {
      key: 'initial',
      name: t('collection.subject.initial')
    },
    {
      key: 'sex',
      name: t('activity.interview.sexcodeId')
    },
    {
      key: 'brthdtc',
      name: t('activity.interview.brthdtc'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      )
    },
    {
      key: 'consdtcSigned',
      name: t('date.signed.ic'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      )
    },
    {
      key: 'consdtcScreeningTests',
      name: t('date.screening.tests'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      )
    },
    {
      key: 'consdtcRandom',
      name: t('date.random'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      )
    },
    {
      key: 'subjectNo',
      name: t('collection.subject.no')
    },
    {
      key: 'subjectStatusName',
      name: t('status')
    },

    {
      key: 'dropoutDate',
      name: t('collection.subject.dropout.date'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD)}</div>
      )
    },

    {
      key: 'dropoutReason',
      name: t('subject.dropout.reason')
    },
    {
      key: 'dropoutDetail',
      name: t('dropout.detail')
    }
  ];

  return (
    <div style={{ marginRight: '1em' }}>
      <DataGrid
        columns={columns}
        rows={items}
        loading={loading}
        minHeight={isMobile ? 200 : 600}
        onRowVisibleEnd={onRowVisibleEnd}
        rowKey="subjectId"
        rowNumber={{ show: true, key: 'no', name: 'No' }}
      />
    </div>
  );
};

export default Log;
