import React, { useEffect, useState } from 'react';
import { useReviews } from './hooks';
import { Button, Table } from 'semantic-ui-react';
import helpers from '../../helpers';
import { code } from '../../constants';
import { useFormatMessage } from '../../hooks';
import { useUser } from '../account-manage/hooks';
import api from './api';
import { useActivitiesOfGroupList } from '../collection/hooks';
import { notification } from '../modal';
import { useParams } from 'react-router-dom';
import { StyledTh, TableWrap } from '../case-review/styles';
import { EmptyRowsViewForSemantic } from '../modules';
import projectMemberApi from '../project-manage/api';

const ReviewContainer = ({
  subjectId,
  projectActivityId,
  activityKeyId,
  status,
  onChangeStatusCb = f => f,
  projectId
}) => {
  const t = useFormatMessage();
  const { activityGroupId } = useParams();
  const [revocableArhId, setRevocableArhId] = useState(0);
  const [{ profile }] = useUser();
  const [teamId, setTeamId] = useState();
  const [{ items, loading, fetch }] = useReviews({
    subjectId,
    projectActivityId,
    activityKeyId
  });

  const [{ fetch: fetchActivityMenu }] = useActivitiesOfGroupList({
    activityGroupId,
    subjectId
  });

  const getStatusName = value => {
    switch (value) {
      case code.Temp:
        return 'Temp';
      case code.Done:
        return 'Done';
      case code.FirstSign:
        return 'First Sign';
      case code.SecondSign:
        return 'Second Sign';
      case code.ThirdSign:
        return 'Third Sign';
      default:
        return 'None';
    }
  };

  const hanldeCancelClick = activityReviewId => () => {
    // 컨펌 창 넣기!!
    notification.confirm({
      title: t('confirm.cancel'),
      content: '',
      onOK: async () => {
        try {
          const res = await api.revertReview({
            subjectId,
            projectActivityId,
            activityKeyId,
            activityReviewId
          });

          if (res && res.data > 0) {
            onChangeStatusCb(res.data);
            fetch();
            fetchActivityMenu();
          }
        } catch (err) {}
      }
    });
  };

  useEffect(
    () => {
      const signCodes = new Set([
        code.FirstSign,
        code.SecondSign,
        code.ThirdSign
      ]);
      let activityReviewHistoryId = 0;
      for (let i = items.length - 1; i >= 0; i--) {
        const item = items[i];
        if (
          item.isCancel !== true &&
          signCodes.has(item.activityStatusCodeId) &&
          Number(status) === Number(item.activityStatusCodeId) &&
          Number(teamId) === Number(item.teamId)
        ) {
          activityReviewHistoryId = item.activityReviewHistoryId;
          break;
        }
      }

      setRevocableArhId(activityReviewHistoryId);
    },
    [items, teamId]
  );

  useEffect(() => {
    const fetchProjectMember = async () => {
      const res = await projectMemberApi.fetchProjectMember({
        projectId,
        id: profile.id
      });

      if (res && res.data) {
        setTeamId(res.data.teamId);
      }
    };

    fetchProjectMember();
  }, []);

  return (
    <TableWrap maxHeight="63vh">
      <Table selectable>
        <Table.Header>
          <Table.Row>
            <StyledTh>Date Time</StyledTh>
            <StyledTh>{t('project.member')}</StyledTh>
            <StyledTh>{t('status')}</StyledTh>
            <StyledTh>Review Cancel</StyledTh>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {items.map(item => {
            return (
              <Table.Row key={item.activityReviewHistoryId}>
                <Table.Cell>
                  {helpers.util.dateformat(item.reviewDateTime)}
                </Table.Cell>
                <Table.Cell>{item.author}</Table.Cell>
                <Table.Cell>
                  {getStatusName(item.activityStatusCodeId)}
                  {item.isCancel && ' is Canceled'}
                </Table.Cell>
                <Table.Cell>
                  {revocableArhId === item.activityReviewHistoryId && (
                    <Button onClick={hanldeCancelClick(item.activityReviewId)}>
                      {t('common.cancel')}
                    </Button>
                  )}
                </Table.Cell>
              </Table.Row>
            );
          })}
          {items.length === 0 && (
            <EmptyRowsViewForSemantic loading={loading} columnLength={4} />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default ReviewContainer;
