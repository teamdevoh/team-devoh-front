import { useCallback, useEffect } from 'react';
import api from '../api';
import { useBoolean } from '../../../hooks';
import { useState } from 'react';

const useReviews = ({ subjectId, activityKeyId, projectActivityId }) => {
  const [items, setItems] = useState([]);
  const isLoading = useBoolean(false);

  const fetchItems = useCallback(
    async () => {
      isLoading.setTrue();
      const res = await api.fetchReviews({
        subjectId,
        activityKeyId,
        projectActivityId
      });
      if (res && res.data) {
        setItems(res.data);
      }
      isLoading.setFalse();
    },
    [subjectId, activityKeyId]
  );

  useEffect(
    () => {
      if (activityKeyId !== '') {
        fetchItems();
      }
    },
    [subjectId, activityKeyId]
  );

  return [{ items, loading: isLoading.value, fetch: fetchItems }];
};

export default useReviews;
