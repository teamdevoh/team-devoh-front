import helpers from '../../helpers';

const fetchReviews = ({ subjectId, activityKeyId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/reviews?${params}`
  );
};

const revertReview = ({
  subjectId,
  projectActivityId,
  activityKeyId,
  activityReviewId
}) => {
  return helpers.Service.put(
    `api/subjects/${subjectId}/activities/${activityKeyId}/review.revert`,
    {
      projectActivityId,
      activityReviewId
    }
  );
};

export default {
  fetchReviews,
  revertReview
};
