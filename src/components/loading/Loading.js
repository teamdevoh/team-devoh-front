import React from 'react';
import { Button } from 'semantic-ui-react';

export default function Loading(props) {
  if (props.isLoading) {
    if (props.timedOut) {
      return (
        <div>
          Taking a long time... <button onClick={props.retry}>Retry</button>
        </div>
      );
    } else if (props.pastDelay) {
      return <div>Loading...</div>;
    } else {
      return null;
    }
  } else if (props.error) {
    return (
      <div>
        Error! <Button onClick={props.retry}>Retry</Button>
      </div>
    );
  } else {
    return null;
  }
}
