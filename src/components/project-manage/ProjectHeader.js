import React, { memo } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Header,
  Label,
  List,
  Popup,
  Segment
} from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import styled from 'styled-components';

const Wrapper = styled(Segment)`
  min-height: 90px;
`;
const HeaderWrapper = styled(Header)`
  font-size: 1.2em;
  margin-bottom: 0;
`;

const HeaderCorner = memo(({ onClick }) => {
  const t = useFormatMessage();
  return (
    <Popup
      trigger={
        <Label
          as="a"
          color="red"
          corner="right"
          icon="edit outline"
          onClick={onClick}
        />
      }
      content={t('project.edit')}
      position="left center"
    />
  );
});

const Meta = ({ title, engTitle, period, protocolNo }) => {
  return (
    <HeaderWrapper inverted>
      {engTitle}
      <Header.Subheader>
        {title}
        <List>
          <List.Item>
            <List.Content>{period}</List.Content>
          </List.Item>
          <List.Item>
            <List.Content>{protocolNo}</List.Content>
          </List.Item>
        </List>
      </Header.Subheader>
    </HeaderWrapper>
  );
};

const propTypes = {
  period: PropTypes.string,
  protocolNo: PropTypes.string,
  title: PropTypes.string.isRequired
};

const defaultProps = {
  period: '',
  protocolNo: ''
};

const ProjectHeader = ({ period, protocolNo, title, engTitle, onClick }) => {
  return (
    <Wrapper inverted textAlign="center" vertical>
      <Container text>
        <Meta
          title={title}
          engTitle={engTitle}
          period={period}
          protocolNo={protocolNo}
        />
        <HeaderCorner onClick={onClick} />
      </Container>
    </Wrapper>
  );
};

ProjectHeader.propTypes = propTypes;
ProjectHeader.defaultProps = defaultProps;

export default memo(ProjectHeader);
