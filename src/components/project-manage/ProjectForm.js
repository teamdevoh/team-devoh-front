import React, { Fragment, useMemo } from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  Divider,
  Input,
  TextArea,
  Dropdown,
  Checkbox
} from 'semantic-ui-react';
import { DateRangePicker } from '../datepicker';
import { useFormatMessage, useSubmit, useCodes } from '../../hooks';
import { ProjectGroupSelect } from './';
import TagListContainer from '../../components/modules/TagListContainer';
import { codeGroup } from '../../constants';

const propTypes = {
  /**
   * Data model
   */
  model: PropTypes.shape({
    projectGroupId: PropTypes.oneOfType(PropTypes.number, PropTypes.string),
    title: PropTypes.string,
    protocolNo: PropTypes.string,
    period: PropTypes.array,
    note: PropTypes.string
  }),
  /**
   * Called on change.
   * @param {ChangeEvent} event React's original SyntheticEvent.
   * @param {object} data All props and a proposed value.
   */
  onChange: PropTypes.func,
  /**
   * The HTML form submit handler.
   */
  onSubmit: PropTypes.func
};

const defaultProps = {};

/**
 * 프로젝트 상태 dropdown
 * @param {json} props
 */
const ProjectStatusSelector = ({ name, onChange, value }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.ProjectStatusCode });

  return (
    <Dropdown
      name={name}
      options={items}
      value={value}
      search
      selection
      onChange={onChange}
    />
  );
};

/**
 * codeGroupId 에 따른 checkbox list 만들어주는 컴포넌트
 * @param {json} props
 */
const CheckBoxList = ({
  name,
  codeGroupId,
  values = new Set(),
  onChange,
  inputValue,
  validateMessage
}) => {
  const [items] = useCodes({ codeGroupId });
  /**
   * checkbox onClick event
   * @param {event} e
   * @param {json} param1
   */
  const handleChane = (e, { name, value, checked }) => {
    const newValues = new Set(values);

    if (checked) {
      newValues.add(value);
    } else {
      newValues.delete(value);
    }

    onChange(e, { name, value: newValues.size === 0 ? null : newValues });
  };

  return items.map((code, index, arr) => {
    const { text, value } = code;
    const otherCode = codeGroupId === 7 ? 38 : codeGroupId === 8 ? 43 : null;

    return (
      <Fragment key={value}>
        <Form.Field>
          <Checkbox
            name={`${name}Codes`}
            onClick={handleChane}
            label={text}
            value={value}
            checked={values !== null && values.has(value)}
          />
        </Form.Field>
        {// 현제 otherCode 이면 그 옆에 input 창
        value === otherCode && (
          <Form.Field width="8">
            <Input
              fluid
              name={`${name}Other`}
              value={inputValue === null ? '' : inputValue}
              onChange={onChange}
              disabled={
                values === null ||
                // other체크되어 있지 않거나
                !values.has(otherCode)
              }
            />
            {/* {validateMessage} */}
          </Form.Field>
        )}
      </Fragment>
    );
  });
};

const getTagArray = tagString => {
  const result = [];
  if (tagString !== null) {
    const tempArray = tagString.split('#');
    for (let i = 0; i < tempArray.length; i++) {
      const tag = tempArray[i];
      if (tag.length > 0) {
        result.push(`#${tag}`);
      }
    }
  }

  return result;
};

const ProjectForm = ({ children, onChange, model, onSubmit }) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);
  const tagArray = useMemo(() => getTagArray(model.tag), [model]);

  /**
   * 태그정보 업데이트되면 form model update 및 state 변경해주기 위한 이벤트
   * @param {array} newTags
   */
  const handleUpdateTags = newTags => {
    onChange(null, { name: 'tag', value: newTags.join('') });
  };

  /**
   * enter 로 태그 추가시 submit 막기위한 이벤트
   * @param {object} event
   */
  const handleOnKeyDown = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      event.preventDefault();
    }
  };

  return (
    <Form onSubmit={onValidate}>
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('project.group')}</label>
          <ProjectGroupSelect
            name="projectGroupId"
            value={model.projectGroupId}
            onChange={onChange}
          />
          {validator.message(
            t('project.group'),
            model.projectGroupId,
            'required'
          )}
        </Form.Field>
      </Form.Group>
      {/* 프로젝트 타이틀 */}
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('project.title')}</label>
          <Input name="title" value={model.title} onChange={onChange} />
          {validator.message(t('project.title'), model.title, 'required')}
        </Form.Field>
      </Form.Group>
      {/* 프로젝트 타이틀 local */}
      <Form.Group widths="equal">
        <Form.Field required>
          <label>{t('project.title.locallanguage')}</label>
          <Input
            name="localTitle"
            value={model.localTitle}
            onChange={onChange}
          />
          {validator.message(
            t('project.title.locallanguage'),
            model.localTitle,
            'required'
          )}
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.protocolno')}</label>
          <Input
            name="protocolNo"
            value={model.protocolNo}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('project.period')}</label>
          <DateRangePicker
            name="period"
            value={model.period}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      {/* 프로젝트 상태 */}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.status')}</label>
          <Form.Group inline>
            <Form.Field>
              <ProjectStatusSelector
                name="statusCode"
                value={model.statusCode}
                onChange={onChange}
              />
            </Form.Field>
            <Input
              name="statusRemark"
              value={model.statusRemark}
              onChange={onChange}
            />
          </Form.Group>
        </Form.Field>
      </Form.Group>
      {/* 사용한 시료 */}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.used.sample')}</label>
          <Form.Group inline>
            <CheckBoxList
              name="usedSample"
              codeGroupId={codeGroup.UsedSampleCode}
              onChange={onChange}
              values={model.usedSampleCodes}
              inputValue={model.usedSampleOther}
              // validateMessage={validator.message(
              //   t('project.used.sample'),
              //   model.usedSampleOther,
              //   'required'
              // )}
            />
          </Form.Group>
        </Form.Field>
      </Form.Group>
      {/* 생성 데이터 */}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.data.type.description')}</label>
          <Form.Group inline>
            <CheckBoxList
              name="createDataType"
              codeGroupId={codeGroup.CreateDataTypeCode}
              onChange={onChange}
              values={model.createDataTypeCodes}
              inputValue={model.createDataTypeOther}
              // validateMessage={validator.message(
              //   t('project.data.type.description'),
              //   model.createDataTypeOther,
              //   'required'
              // )}
            />
          </Form.Group>
        </Form.Field>
      </Form.Group>
      {/* 태그 */}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('task.tag')}</label>
          <TagListContainer
            items={tagArray}
            onUpdate={handleUpdateTags}
            onKeyDown={handleOnKeyDown}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.note')}</label>
          <TextArea name="note" value={model.note} onChange={onChange} />
        </Form.Field>
      </Form.Group>
      <Divider />
      {children}
    </Form>
  );
};

ProjectForm.propTypes = propTypes;
ProjectForm.defaultProps = defaultProps;

export default ProjectForm;
