import React, { Component } from 'react';
import { Header, Grid, Tab, Label, Menu, List } from 'semantic-ui-react';
import { injectIntl } from 'react-intl';
import { SuggestInput, Api } from './';
import ProjectAttachments from './ProjectAttachments';
import { Modal, notification } from '../modal';
import { TaskEdit } from '../task-manage';
import helpers from '../../helpers';
import map from 'lodash/map';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';

class ProjectSearchDashBoard extends Component {
  constructor(props) {
    super(props);

    const params = helpers.qs.parse(props.location.search);

    this.state = {
      query: params.keyword,
      summary: {
        tasks: 0,
        attachments: 0
      },
      active: 0,
      items: [],
      taskShow: false,
      selectedTaskItemId: 0
    };

    this.getSummary = this.getSummary.bind(this);
    this.getTasks = this.getTasks.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    if (this._isMounted && this.state.query) {
      this.getSummary();
      this.getTasks();
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getSummary() {
    const { match } = this.props;
    const projectId = match.params.projectId;
    const { query } = this.state;
    Api.fetchSearchingSummary({ projectId, search: query }).then(res => {
      if (this._isMounted) {
        this.setState({ summary: res.data });
      }
    });
  }

  getTasks() {
    const { match } = this.props;
    const projectId = match.params.projectId;
    const { query } = this.state;
    if (query) {
      Api.fetchTasksSearching({ projectId, search: query }).then(res => {
        if (this._isMounted) {
          this.setState({ items: res.data });
        }
      });
    }
  }

  render() {
    const { getTasks } = this;
    const { match, intl, history } = this.props;
    const projectId = match.params.projectId;
    const { query, summary } = this.state;

    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Header as="h3" dividing>
                {intl.formatMessage({ id: 'project.search' })}
                <Header.Subheader>
                  {intl.formatMessage({ id: 'project.search.description' })}
                </Header.Subheader>
                <CopyRedirectUrl />
              </Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <SuggestInput
                projectId={projectId}
                value={this.state.query}
                onSelect={value => {
                  const encodedKeyword = encodeURIComponent(value);
                  history.push(
                    `/project/${projectId}/search?keyword=${encodedKeyword}`
                  );
                }}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <Tab
                onTabChange={(event, data) => {
                  this.setState({ active: data.activeIndex });
                }}
                activeIndex={this.state.active}
                menu={{ secondary: true, pointing: true, borderless: true }}
                panes={[
                  {
                    menuItem: (
                      <Menu.Item key="task">
                        {intl.formatMessage({ id: 'task' })}
                        <Label color="teal">{summary.tasks}</Label>
                      </Menu.Item>
                    ),
                    render: () => (
                      <Tab.Pane key="task" attached={false} basic>
                        <List divided verticalAlign="middle">
                          {map(this.state.items, item => {
                            return (
                              <List.Item
                                key={item.id}
                                as="a"
                                onClick={() => {
                                  this.setState({
                                    taskShow: true,
                                    selectedTaskItemId: item.id
                                  });
                                }}
                              >
                                {item.title}
                              </List.Item>
                            );
                          })}
                        </List>
                      </Tab.Pane>
                    )
                  },
                  {
                    menuItem: (
                      <Menu.Item key="attachment">
                        {intl.formatMessage({ id: 'comment.attachment' })}
                        <Label color="teal">{summary.attachments}</Label>
                      </Menu.Item>
                    ),
                    render: () => (
                      <Tab.Pane key="attachment" attached={false} basic>
                        <ProjectAttachments
                          projectId={projectId}
                          query={query}
                        />
                      </Tab.Pane>
                    )
                  }
                ]}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16} />
          </Grid.Row>
        </Grid>
        <Modal
          open={this.state.taskShow}
          onOK={() => {
            this.setState({ taskShow: false });
          }}
          title={intl.formatMessage({ id: 'task.title.edit' })}
          content={
            <TaskEdit
              projectId={projectId}
              taskItemId={this.state.selectedTaskItemId}
              onSaved={model => {
                this.setState({ taskShow: false }, () => {
                  notification.success({
                    title: intl.formatMessage({
                      id: 'common.alert.changed'
                    }),
                    onClose: () => {
                      getTasks();
                    }
                  });
                });
              }}
              onRemoved={() => {
                this.setState({ taskShow: false }, () => {
                  notification.success({
                    title: intl.formatMessage({
                      id: 'common.alert.deleted'
                    }),
                    onClose: () => {
                      getTasks();
                    }
                  });
                });
              }}
            />
          }
        />
      </div>
    );
  }
}

export default injectIntl(ProjectSearchDashBoard);
