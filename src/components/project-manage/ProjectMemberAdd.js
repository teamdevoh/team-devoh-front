import React, { Fragment, useState } from 'react';
import { ProjectMemberHeader, ProjectMemberForm } from './';
import { CancelButton, SaveButton } from '../button';
import { notification } from '../modal';
import { Grid, Message, Input } from 'semantic-ui-react';
import { useBoolean, useFormatMessage } from '../../hooks';
import useProjectMemberAdd from './hooks/useProjectMemberAdd';
import { useRouteMatch, useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { Modal } from '../modal';
import SearchMember from './SearchMember';
import { role } from '../../constants';

const NotSelectWrapper = styled(Grid.Row)`
  &&& {
    ${props => {
      return {
        display: props.visible
      };
    }};
  }
`;

const SelectWrapper = styled(Grid.Row)`
  &&& {
    ${props => {
      return {
        display: props.visible
      };
    }};
  }
`;

const ProjectMemberAdd = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const { projectId } = match.params;
  const t = useFormatMessage();
  const [{ model, loading, onChange, onAdd }] = useProjectMemberAdd(projectId);
  const showMemberModal = useBoolean(false);
  const [name, setName] = useState('');

  const handleSubmit = async () => {
    const is = await onAdd();
    if (is) {
      notification.success({
        title: t('common.alert.added'),
        onClose: () => {
          history.push(`/project/${projectId}/member`);
        }
      });
    }
  };

  const handleNameChange = (event, { value }) => {
    setName(value);
  };

  const handleKeyDown = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      showMemberModal.setTrue();
    }
  };

  /**
   * 사용자 검색 모달창 열기
   */
  const handleOpenMemberModal = async () => {
    showMemberModal.setTrue();
  };

  const handleSelecteMember = data => {
    setName(data.memberName);
  };

  return (
    <Fragment>
      <ProjectMemberHeader />
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Input
              action={{ icon: 'search', onClick: handleOpenMemberModal }}
              placeholder={t('project.member.add')}
              onChange={handleNameChange}
              onKeyDown={handleKeyDown}
              value={name}
            />
          </Grid.Column>
        </Grid.Row>
        <NotSelectWrapper visible={model.memberId ? 'none' : 'block'}>
          <Grid.Column>
            <Message visible>{t('project.member.select')}</Message>
          </Grid.Column>
        </NotSelectWrapper>
        <SelectWrapper visible={model.memberId ? 'block' : 'none'}>
          <Grid.Column>
            <ProjectMemberForm
              projectId={projectId}
              model={model}
              onChange={onChange}
              onSubmit={handleSubmit}
            >
              <CancelButton
                to={`/project/${projectId}/member`}
                name={t('common.cancel')}
              />
              <SaveButton
                allowedRole={role.ProjectMember_Edit}
                name={t('common.save')}
                loading={loading}
              />
            </ProjectMemberForm>
          </Grid.Column>
        </SelectWrapper>
      </Grid>
      <Modal
        open={showMemberModal.value}
        onOK={showMemberModal.setFalse}
        title={t('project.member')}
        content={
          <SearchMember
            name={name}
            memberId={model.memberId}
            projectId={projectId}
            onChange={onChange}
            onSelecteMember={handleSelecteMember}
            onClose={showMemberModal.setFalse}
          />
        }
      />
    </Fragment>
  );
};

export default ProjectMemberAdd;
