import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import { injectIntl } from 'react-intl';
import { Header } from 'semantic-ui-react';

const ProjectSiteHeader = onlyUpdateForKeys([])(({ intl }) => {
  return (
    <Header as="h3" dividing>
      {intl.formatMessage({ id: 'project.site.title' })}
      <Header.Subheader>
        {intl.formatMessage({ id: 'project.site.title.description' })}
      </Header.Subheader>
    </Header>
  );
});

export default injectIntl(ProjectSiteHeader);
