import React, { useCallback } from 'react';
import history from '../../helpers/history';
import { AddLinkButton } from '../button';
import { Grid, Icon, Loader } from 'semantic-ui-react';
import { ProjectSiteHeader, ProjectSiteList } from './';
import { useRouteMatch } from 'react-router-dom';
import { useFetch, ListModel, useFormatMessage } from '../../hooks';
import Anchor from '../button/Anchor';

const ProjectSiteListAll = () => {
  const match = useRouteMatch();
  const t = useFormatMessage();
  const projectId = match.params.projectId;

  const url = `api/projects/${projectId}/sites`;
  const [site] = useFetch(url, new ListModel());

  const handleGoProjectHome = useCallback(() => {
    history.push(`/project/${projectId}`);
  }, []);

  const handleClick = useCallback(siteId => {
    history.push(`/project/${projectId}/site/edit/${siteId}`);
  }, []);

  return (
    <div>
      <ProjectSiteHeader />
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <AddLinkButton
              name={t('project.site.add')}
              to={`/project/${projectId}/site/new`}
            />
            <AddLinkButton
              name={t('project.member.add')}
              to={`/project/${projectId}/member`}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Icon name="arrow left" />
            <Anchor text={t('project.start')} onClick={handleGoProjectHome} />
            <ProjectSiteList items={site.data.items} onClick={handleClick} />
            {site.loading && <Loader active inline="centered" />}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default ProjectSiteListAll;
