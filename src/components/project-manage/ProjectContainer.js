import React from 'react';
import { Grid, Breadcrumb } from 'semantic-ui-react';

const ProjectContainer = ({ children }) => {
  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <Breadcrumb size="small">
            <Breadcrumb.Section link>Home</Breadcrumb.Section>
            <Breadcrumb.Divider icon="right chevron" />
            <Breadcrumb.Section link>Project</Breadcrumb.Section>
          </Breadcrumb>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>{children}</Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default ProjectContainer;
