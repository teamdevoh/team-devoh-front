import React, { memo, useCallback } from 'react';
import { List } from 'semantic-ui-react';
import helpers from '../../helpers';
import { TaskTransition } from '../task-manage';
import { useFormatMessage } from '../../hooks';
import map from 'lodash/map';
import { format } from '../../constants';

const ProjectSiteList = ({ items, onClick }) => {
  return (
    <List celled>
      {map(items, projectSite => {
        return (
          <SiteListItem
            key={projectSite.projectSiteId}
            siteId={projectSite.siteId}
            siteName={projectSite.siteName}
            irbno={projectSite.irbno}
            targetCount={projectSite.targetCount ? projectSite.targetCount : 0}
            subjectPrefix={projectSite.subjectPrefix}
            fromdate={projectSite.scheduleFromDate}
            todate={projectSite.scheduleToDate}
            onClick={onClick}
          />
        );
      })}
    </List>
  );
};

const SiteListItem = ({
  siteId,
  siteName,
  irbno,
  targetCount,
  subjectPrefix,
  fromdate,
  todate,
  onClick
}) => {
  const t = useFormatMessage();

  const handleClick = useCallback(() => {
    onClick(siteId);
  }, []);

  return (
    <TaskTransition>
      <List.Item>
        <List.Content>
          <List.Header>
            <a onClick={handleClick}>{siteName}</a>
          </List.Header>
          {irbno && <div>IRB {irbno}</div>}
          <div>
            {t('targetcount', {
              count: targetCount
            })}
          </div>
          {subjectPrefix && (
            <div>
              {t('subject.number.prefix')} {subjectPrefix}
            </div>
          )}
          {fromdate && (
            <div>
              {helpers.util.dateRange(fromdate, todate, format.YYYY_MM_DD)}
            </div>
          )}
        </List.Content>
      </List.Item>
    </TaskTransition>
  );
};

export default memo(ProjectSiteList);
