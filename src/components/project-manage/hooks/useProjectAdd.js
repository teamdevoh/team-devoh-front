import { useState } from 'react';
import { useFormFields } from '../../../hooks';
import helpers from '../../../helpers';
import api from '../api';
import { format } from '../../../constants';

const useProjectAdd = ({ projectGroupId }) => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields({
    teamId: '',
    projectGroupId: projectGroupId || '',
    title: '',
    localTitle: '',
    protocolNo: '',
    period: [],
    periodFrom: '',
    periodTo: '',
    // 프로젝트 진행 상태
    statusCode: 24,
    statusRemark: '',
    // 사용한 시료
    usedSampleCodes: null,
    // 사용한 시료 other 값
    usedSampleOther: '',
    // 생성 데이터
    createDataTypeCodes: null,
    // 생성 데이터 other 값
    createDataTypeOther: '',
    tag: null,
    note: ''
  });

  const add = async () => {
    if (model.period) {
      const dateformat = helpers.util.dateformat;
      model.periodFrom = model.period[0]
        ? dateformat(model.period[0], format.YYYY_MM_DD)
        : null;
      model.periodTo = model.period[1]
        ? dateformat(model.period[1], format.YYYY_MM_DD)
        : null;
    }

    setLoading(true);
    const res = await api.addProject({ model });
    setLoading(false);
    return res.data;
  };

  return [{ model, loading, onChange, onAdd: add }, setModel];
};

export default useProjectAdd;
