import { useEffect, useState } from 'react';
import api from '../api';

const useProjectMemberList = ({ projectId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchProjectMembers({ projectId });
      if (res && res.data) {
        setItems(res.data.items);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId]
  );

  return { items, loading };
};

export default useProjectMemberList;
