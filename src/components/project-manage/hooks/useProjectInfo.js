import { useEffect, useState } from 'react';
import api from '../api';

const useProjectInfo = ({ projectId }) => {
  const [loading, setLoading] = useState(false);
  const [item, setItem] = useState({
    protocolNo: '',
    taskSchedule: '',
    taskProgress: '',
    ciMemberViews: [],
    piMemberViews: []
  });

  const fetchItem = async () => {
    try {
      setLoading(true);
      const res = await api.fetchProject({ projectId });
      if (res && res.data) {
        setItem(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItem();
    },
    [projectId]
  );

  return { item, loading };
};

export default useProjectInfo;
