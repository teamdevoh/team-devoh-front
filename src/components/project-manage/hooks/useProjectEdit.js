import { useEffect, useState } from 'react';
import { useFormFields } from '../../../hooks';
import helpers from '../../../helpers';
import api from '../api';
import { format } from '../../../constants';

const useProjectEdit = projectId => {
  const [loading, setLoading] = useState();
  const [{ model, onChange }, setModel] = useFormFields({
    projectId: '',
    projectGroupId: '',
    title: '',
    localTitle: '',
    protocolNo: '',
    periodFrom: '',
    periodTo: '',
    period: [],
    note: '',
    statusCode: null,
    statusRemark: '',
    // 사용한 시료
    usedSampleCodes: null,
    // 사용한 시료 other 값
    usedSampleOther: '',
    // 생성 데이터
    createDataTypeCodes: null,
    // 생성 데이터 other 값
    createDataTypeOther: '',
    tag: null
  });

  const edit = async () => {
    if (model.period) {
      const dateformat = helpers.util.dateformat;
      model.periodFrom = model.period[0]
        ? dateformat(model.period[0], format.YYYY_MM_DD)
        : null;
      model.periodTo = model.period[1]
        ? dateformat(model.period[1], format.YYYY_MM_DD)
        : null;
    }

    setLoading(true);
    const res = await api.updateProject({ projectId, model });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  const remove = async () => {
    setLoading(true);
    const res = await api.removeProject({ projectId });
    setLoading(true);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  const fetchItem = async () => {
    try {
      const res = await api.fetchProject({ projectId });
      if (res.data) {
        const { usedSampleCodes, createDataTypeCodes } = res.data;
        const dates = [];
        dates.push(res.data.periodFrom);
        dates.push(res.data.periodTo);
        res.data.period = dates;

        res.data.usedSampleCodes = new Set(usedSampleCodes);
        res.data.createDataTypeCodes = new Set(createDataTypeCodes);

        setModel(res.data);
      }
    } catch (error) {}
  };

  useEffect(
    () => {
      fetchItem();
    },
    [projectId]
  );

  return [
    { model, loading, onChange, onEdit: edit, onRemove: remove },
    setModel
  ];
};

export default useProjectEdit;
