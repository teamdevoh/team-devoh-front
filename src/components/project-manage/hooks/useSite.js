import { useState, useEffect } from 'react';
import { useFormFields } from '../../../hooks';
import api from '../api';

const initial = {
  address: null,
  addressDetail: null,
  businessNo: null,
  description: null,
  fax: null,
  name: null,
  officePhone: null
};

const useSite = siteId => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields(initial);

  const fetchItem = async () => {
    setLoading(true);
    const res = await api.fetchSite({ siteId });
    setLoading(false);
    if (res && res.data) {
      setModel(res.data);
    }
  };

  const add = async () => {
    setLoading(true);
    const res = await api.addSite({ model });
    setLoading(false);
    return res.data;
  };

  const edit = async () => {
    setLoading(true);
    const res = await api.updateSite({
      model
    });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  useEffect(
    () => {
      if (siteId > 0) {
        fetchItem();
      } else {
        setModel(initial);
      }
    },
    [siteId]
  );

  return [
    {
      model,
      loading,
      onChange,
      onAdd: add,
      onEdit: edit
    }
  ];
};

export default useSite;
