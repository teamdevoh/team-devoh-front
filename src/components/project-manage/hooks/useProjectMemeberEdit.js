import api from '../api';
import { useEffect } from 'react';
import { useFormFields } from '../../../hooks';
import { useState } from 'react';
import helpers from '../../../helpers';
import { format } from '../../../constants';

const useProjectMemeberEdit = (projectId, memberId) => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields({
    teamId: '',
    rate: '',
    isTerminate: false,
    scheduleDate: [],
    note: '',
    projectSites: []
  });

  const fetchItem = async () => {
    const res = await api.fetchProjectMember({ projectId, id: memberId });
    const dates = [];
    dates.push(res.data.scheduleFromDate);
    dates.push(res.data.scheduleToDate);
    res.data.scheduleDate = dates;

    setModel(res.data);
  };

  const edit = async () => {
    const dateformat = helpers.util.dateformat;
    if (model.scheduleDate) {
      model.scheduleFromDate = model.scheduleDate[0]
        ? dateformat(model.scheduleDate[0], format.YYYY_MM_DD)
        : null;
      model.scheduleToDate = model.scheduleDate[1]
        ? dateformat(model.scheduleDate[1], format.YYYY_MM_DD)
        : null;
    }

    setLoading(true);
    const res = await api.updateProjectMember({ projectId, memberId, model });
    setLoading(false);
    if (res && res.data) {
      return res.data;
    }

    return false;
  };

  const remove = async () => {
    setLoading(true);
    const res = await api.removeProjectMember({ projectId, memberId });
    setLoading(false);
    if (res && res.data) {
      return res.data;
    }

    return false;
  };

  useEffect(
    () => {
      fetchItem();
    },
    [memberId]
  );

  return [
    { model, loading, onChange, onEdit: edit, onRemove: remove },
    setModel
  ];
};

export default useProjectMemeberEdit;
