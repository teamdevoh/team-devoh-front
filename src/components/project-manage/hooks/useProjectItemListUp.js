import { useDispatch } from 'react-redux';
import { project } from '../../../states';
import api from '../api';

const useProjectItemListUp = () => {
  const dispatch = useDispatch();

  const fetch = async () => {
    try {
      const res = await api.fetchProjects({});
      if (res && res.data) {
        dispatch(project.actions.fetchItems(res.data.items));
      }
    } catch (error) {}
  };

  return { fetch };
};

export default useProjectItemListUp;
