import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import { useBoolean } from 'react-hanger';

const useProjectTeamList = ({ search, onlyUseProject }) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(true);

  const fetchItems = useCallback(async () => {
    try {
      const res = await api.fetchTeams({ search, onlyUseProject });

      if (res && res.data) {
        setItems(
          res.data.items.map(item => {
            const { teamName, teamId } = item;
            return {
              text: teamName,
              value: teamId
            };
          })
        );
      }
    } finally {
      loading.setFalse();
    }
  }, []);

  useEffect(
    () => {
      fetchItems();
    },
    [search, onlyUseProject]
  );

  return [{ items, isLoading: loading.value }, setItems];
};

export default useProjectTeamList;
