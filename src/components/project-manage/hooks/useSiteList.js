import { useState, useEffect } from 'react';
import api from '../api';

const useSiteList = ({ search = '' }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = async ({ search }) => {
    setLoading(true);
    try {
      const res = await api.fetchSites({ search });

      if (res && res.data) {
        setItems(res.data.items);
      }
    } catch (error) {}
    setLoading(false);
  };

  useEffect(
    () => {
      fetchItems({ search });
    },
    [search]
  );

  return { items, loading, fetchItems };
};

export default useSiteList;
