import { useEffect } from 'react';
import api from '../api';
import { useState } from 'react';
import helpers from '../../../helpers';
import { useFormFields } from '../../../hooks';
import { format } from '../../../constants';

const useProjectSiteEdit = ({ projectId, siteId }) => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields({
    projectSiteId: '',
    irbno: '',
    targetCount: '',
    subjectPrefix: '',
    scheduleDate: [],
    note: ''
  });

  useEffect(
    () => {
      getItem();
    },
    [siteId]
  );

  const getItem = async () => {
    const res = await api.fetchProjectSite({ projectId, siteId });
    if (res && res.data) {
      const dates = [];
      dates.push(res.data.scheduleFromDate);
      dates.push(res.data.scheduleToDate);
      res.data.scheduleDate = dates;

      setModel(res.data);
    }
  };

  const update = async () => {
    if (model.scheduleDate) {
      model.scheduleFromDate = model.scheduleDate[0]
        ? helpers.util.dateformat(model.scheduleDate[0], format.YYYY_MM_DD)
        : null;
      model.scheduleToDate = model.scheduleDate[1]
        ? helpers.util.dateformat(model.scheduleDate[1], format.YYYY_MM_DD)
        : null;
    }

    setLoading(true);
    const res = await api.updateProjectSite({ projectId, siteId, model });
    setLoading(false);
    return res;
  };

  const remove = async () => {
    setLoading(true);
    const res = await api.removeProjectSite({ projectId, siteId });
    setLoading(false);
    return res;
  };

  return [
    { model, loading, onChange, onEdit: update, onRemove: remove },
    setModel
  ];
};

export default useProjectSiteEdit;
