import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import { useBoolean } from 'react-hanger';

const useProjectSiteList = ({ projectId }) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(true);

  const fetchItems = useCallback(async () => {
    const res = await api.fetchProjectSites({ projectId });
    if (res && res.data) {
      setItems(res.data.items);
      loading.setFalse();
    }
  }, []);

  useEffect(
    () => {
      fetchItems();
    },
    [projectId]
  );

  return [{ items, isLoading: loading.value }, setItems];
};

export default useProjectSiteList;
