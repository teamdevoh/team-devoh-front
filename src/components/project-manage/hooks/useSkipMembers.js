import { useEffect, useState, useRef } from 'react';
import api from '../api';

const useSkipMembers = (search, projectId, siteId) => {
  search = search.trim();
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);

  const [isDo, setIsDo] = useState(null);

  // api 중복 호출을 막기위한 로컬 변수
  const preSearchVal = useRef(null);

  const setPreSearchVal = newSearchVal => {
    preSearchVal.current = newSearchVal;
  };

  const fetchItems = async () => {
    setLoading(true);
    setPreSearchVal(search);
    const res = await api.fetchMembersNotIn({
      search,
      projectId,
      siteId
    });
    setItems(res.data);

    setLoading(false);
  };

  useEffect(
    () => {
      if (
        // search.length >= 2 &&
        !!isDo &&
        search !== preSearchVal.current
      ) {
        fetchItems();
      }
    },
    [isDo]
  );

  useEffect(
    () => {
      fetchItems();
    },
    [siteId]
  );

  return [{ items, loading }, setIsDo];
};

export default useSkipMembers;
