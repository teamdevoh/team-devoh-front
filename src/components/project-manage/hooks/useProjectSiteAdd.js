import api from '../api';
import { useState } from 'react';
import helpers from '../../../helpers';
import { useFormFields } from '../../../hooks';
import { format } from '../../../constants';

const useProjectSiteAdd = projectId => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields({
    irbno: '',
    targetCount: '',
    subjectPrefix: '',
    scheduleDate: [],
    note: '',
    siteId: 0
  });

  const add = async () => {
    if (model.scheduleDate) {
      model.scheduleFromDate = helpers.util.dateformat(
        model.scheduleDate[0],
        format.YYYY_MM_DD
      );
      model.scheduleToDate = helpers.util.dateformat(
        model.scheduleDate[1],
        format.YYYY_MM_DD
      );
    }

    setLoading(true);
    const res = await api.addProjectSite({
      projectId: projectId,
      siteId: model.siteId,
      model
    });

    setLoading(false);
    return res;
  };

  return [{ model, loading, onAdd: add, onChange }, setModel];
};

export default useProjectSiteAdd;
