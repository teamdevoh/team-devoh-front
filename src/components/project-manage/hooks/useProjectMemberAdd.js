import { useFormFields } from '../../../hooks';
import { useState } from 'react';
import helpers from '../../../helpers';
import api from '../api';
import { format } from '../../../constants';

const useProjectMemberAdd = projectId => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }] = useFormFields({
    teamId: '',
    rate: '',
    isTerminate: false,
    scheduleDate: [],
    note: '',
    projectSites: [],
    memberId: 0
  });

  const add = async () => {
    const dateformat = helpers.util.dateformat;

    if (model.scheduleDate) {
      model.scheduleFromDate = model.scheduleDate[0]
        ? dateformat(model.scheduleDate[0], format.YYYY_MM_DD)
        : null;
      model.scheduleToDate = model.scheduleDate[1]
        ? dateformat(model.scheduleDate[1], format.YYYY_MM_DD)
        : null;
    }

    setLoading(true);
    const res = await api.addProjectMember({
      projectId,
      memberId: model.memberId,
      model
    });
    setLoading(false);

    if (res && res.data) {
      return res.data;
    }
    return false;
  };

  return [{ model, loading, onChange, onAdd: add }];
};

export default useProjectMemberAdd;
