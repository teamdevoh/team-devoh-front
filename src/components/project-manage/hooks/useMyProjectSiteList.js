import api from '../api';
import { useState } from 'react';
import { useEffect } from 'react';
import helpers from '../../../helpers';

const useMyProjectSiteList = ({ projectId, showSubjectPrefix = false }) => {
  const [items, setItems] = useState([]);
  const fetchItems = async () => {
    const res = await api.fetchMyProjectSites({
      projectId,
      memberId: helpers.Identity.profile.id
    });

    let options = [];
    if (res && res.data) {
      res.data.forEach(item => {
        const { subjectPrefix } = item;

        let prefix = !!subjectPrefix ? subjectPrefix : '';
        const prefixNum = prefix.replace(/[^0-9]/g, '');

        prefix = prefixNum.length === 0 ? prefix : prefixNum;
        const subPreFix = showSubjectPrefix
          ? prefix + `${prefix.length > 0 ? '. ' : ''}`
          : '';

        options.push({
          text: subPreFix + item.name,
          value: item.projectSiteId,
          subjectprefix: subjectPrefix,
          siteid: item.siteId
        });
      });
      setItems(options);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId]
  );

  return [items];
};

export default useMyProjectSiteList;
