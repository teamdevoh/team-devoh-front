import React, { useState, useCallback, useEffect } from 'react';
import { Line } from 'react-chartjs-2/dist/react-chartjs-2';
import api from './api';
import helpers from '../../helpers';
import { DatePicker } from '../datepicker';
import { Grid, Label, Icon } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import maxBy from 'lodash/maxBy';
import orderBy from 'lodash/orderBy';
import filter from 'lodash/filter';
import { format } from '../../constants';

const ProjectStatus = ({ projectId }) => {
  const t = useFormatMessage();
  const [data, setData] = useState({});
  const [historyDate, setHistoryDate] = useState(
    helpers.util.dateformat(new Date(), format.YYYY_MM_DD)
  );

  let isSubscribed = true;
  useEffect(() => {
    fetchData();
    return () => (isSubscribed = false);
  }, []);

  const fetchData = useCallback(async () => {
    const today = helpers.util.dateformat(new Date(), format.YYYY_MM_DD);
    const result = await getData(today);

    if (isSubscribed) {
      setData({
        labels: result.labels,
        datasets: [
          {
            label: t('task.actual'),
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(225,0,0,0.4)',
            borderColor: 'red', // The main line color
            borderCapStyle: 'square',
            borderDash: [], // try [5, 15] for instance
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'black',
            pointBackgroundColor: 'white',
            pointBorderWidth: 1,
            pointHoverRadius: 8,
            pointHoverBackgroundColor: 'yellow',
            pointHoverBorderColor: 'brown',
            pointHoverBorderWidth: 2,
            pointRadius: 4,
            pointHitRadius: 10,
            data: result.actuals,
            spanGaps: true
          },
          {
            label: t('task.expected'),
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(167,105,0,0.4)',
            borderColor: 'rgb(167, 105, 0)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'white',
            pointBackgroundColor: 'black',
            pointBorderWidth: 1,
            pointHoverRadius: 8,
            pointHoverBackgroundColor: 'brown',
            pointHoverBorderColor: 'yellow',
            pointHoverBorderWidth: 2,
            pointRadius: 4,
            pointHitRadius: 10,
            // notice the gap in the data and the spanGaps: false
            data: result.expecteds,
            spanGaps: false
          }
        ]
      });
    }
  }, []);

  const appendHistory = useCallback(async value => {
    const util = helpers.util;

    let datasets = data.datasets;
    let newDatasets = [];

    newDatasets.push(datasets[0]);
    newDatasets.push(datasets[1]);

    const result = await getData(value);
    if (
      result.expecteds.length > 0 &&
      util.dateformat(result.currentDate, format.YYYY_MM_DD) !== value
    ) {
      const appendSet = [
        {
          label: `${value}-${t('task.actual')}`,
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(225,0,0,0.4)',
          borderColor: 'pink', // The main line color
          borderCapStyle: 'square',
          borderDash: [], // try [5, 15] for instance
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'black',
          pointBackgroundColor: 'white',
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: 'yellow',
          pointHoverBorderColor: 'brown',
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          data: result.actuals,
          spanGaps: true
        },
        {
          label: `${value}-${t('task.expected')}`,
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(167,105,0,0.4)',
          borderColor: 'yellow',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'white',
          pointBackgroundColor: 'black',
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: 'brown',
          pointHoverBorderColor: 'yellow',
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: false
          data: result.expecteds,
          spanGaps: false
        }
      ];

      newDatasets.push(appendSet[0]);
      newDatasets.push(appendSet[1]);
    }

    setData({ datasets: newDatasets });
  }, []);

  const getData = useCallback(async date => {
    const res = await api.fetchProjectStatus({ projectId, historyDate: date });

    const util = helpers.util;
    const currentDate = res.data.serverDate;
    const items = res.data.taskSnapshots;

    const orderedItems = orderBy(items, ['scheduleFromDate'], ['asc']);
    const max = maxBy(items, 'scheduleToDate');

    const labels = [];
    const actuals = [];
    const expecteds = [];

    let actual = 0;
    let projectExpected = 0;
    let monthExpected = 0;
    if (orderedItems.length > 0) {
      let startDate =
        data && data.labels.length > 0
          ? data.labels[0]
          : orderedItems[0].scheduleFromDate;

      let endDate =
        data && data.labels.length > 0
          ? data.labels[data.labels.length - 1]
          : max.scheduleToDate;

      labels.push(util.dateformat(startDate, format.YYYY_MM));

      const months = util.diffMonths(startDate, endDate);

      for (let i = 0; i < months; i++) {
        if (i > 0) {
          startDate = util.addMonth(startDate, 1).format(format.YYYY_MM);

          if (util.dateformat(currentDate, format.YYYY_MM) === startDate) {
            startDate = util.dateformat(currentDate, format.YYYY_MM_DD);
          }

          labels.push(startDate);
        }

        const target = util.dateformat(startDate, format.YYYY_MM);

        const itemsByDate = filter(orderedItems, source => {
          return (
            util.dateformat(source.scheduleFromDate, format.YYYY_MM) <=
              target &&
            util.dateformat(source.scheduleToDate, format.YYYY_MM) >= target
          );
        });

        if (itemsByDate.length > 0) {
          for (let j = 0; j < itemsByDate.length; j++) {
            // first day of month
            const firstDayOfMonth = util
              .firstDayOfMonth(target)
              .format(format.YYYY_MM_DD);
            // last day of month
            const lastDayOfMonth = util
              .lastDayOfMonth(target)
              .format(format.YYYY_MM_DD);

            const item = itemsByDate[j];
            const days = util.diffDays(
              firstDayOfMonth <= item.scheduleFromDate
                ? item.scheduleFromDate
                : firstDayOfMonth,
              lastDayOfMonth <= item.scheduleToDate
                ? lastDayOfMonth
                : item.scheduleToDate
            );

            // Expected rate of month
            monthExpected += days * item.weight * 100;
            // total expected rate of month
            projectExpected += days * item.weight * 100;

            // this time actual rate
            if (util.dateformat(currentDate, format.YYYY_MM) >= target) {
              actual += days * item.weight * item.rate;
            }
          }

          if (util.dateformat(currentDate, format.YYYY_MM) >= target) {
            actuals.push(actual);
          }
        } else {
          actuals.push({});
        }

        if (
          util.dateformat(orderedItems[0].scheduleFromDate, format.YYYY_MM) <=
            target &&
          util.dateformat(max.scheduleToDate, format.YYYY_MM) >= target
        ) {
          expecteds.push(monthExpected);
        } else {
          expecteds.push({});
        }
      }
    }

    // Create actual values
    for (let i = 0; i < actuals.length; i++) {
      const data = actuals[i];
      actuals[i] = Math.round((data / projectExpected) * 100);
    }

    // Create expected values
    for (let i = 0; i < expecteds.length; i++) {
      let data = expecteds[i];
      expecteds[i] = Math.round((data / projectExpected) * 100);
    }

    return {
      currentDate,
      labels,
      actuals,
      expecteds
    };
  }, []);

  const handleChangeHistory = useCallback(
    (obj, { value }) => {
      if (value !== historyDate) {
        setHistoryDate(value);
        appendHistory(value);
      }
    },
    [historyDate]
  );

  return (
    <Grid>
      <Grid.Column width={16} textAlign="right">
        <Label.Group size="large">
          <Label color="orange">
            <Icon name="history" />
            {t('common.histories')}
          </Label>
          <DatePicker
            name="history"
            onChange={handleChangeHistory}
            value={historyDate}
          />
        </Label.Group>
      </Grid.Column>
      <Grid.Column width="sixteen">
        <div
          style={{
            height: '300px',
            overflow: 'auto'
          }}
        >
          <Line
            data={data}
            height={250}
            options={{
              maintainAspectRatio: false,
              scales: {
                yAxes: [
                  {
                    ticks: {
                      beginAtZero: true
                    },
                    scaleLabel: {
                      display: true,
                      labelString: t('task.rate'),
                      fontSize: 20
                    }
                  }
                ]
              }
            }}
          />
        </div>
      </Grid.Column>
    </Grid>
  );
};

export default ProjectStatus;
