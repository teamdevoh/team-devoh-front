import React, { useCallback } from 'react';
import { ProjectContainer, ProjectForm } from './';
import { notification, Confirm } from '../modal';
import { CancelButton, RemoveButton, SaveButton } from '../button';
import { useFormatMessage, useBoolean } from '../../hooks';
import { useDispatch } from 'react-redux';
import { project } from '../../states';
import { useRouteMatch, useHistory } from 'react-router-dom';
import useProjectEdit from './hooks/useProjectEdit';
import { role } from '../../constants';

const ProjectEdit = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const { projectId } = match.params;
  const t = useFormatMessage();
  const isRemove = useBoolean(false);
  const [{ model, loading, onChange, onEdit, onRemove }] = useProjectEdit(
    projectId
  );
  const dispatch = useDispatch();

  const handleSubmit = useCallback(
    async () => {
      const is = await onEdit();
      if (is) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            dispatch(project.actions.edit(model));
            dispatch(
              project.actions.selectedItem({
                key: projectId,
                name: model.localTitle,
                engName: model.title,
                periodFrom: model.periodFrom,
                periodTo: model.periodTo,
                protocolNo: model.protocolNo
              })
            );
            history.push(`/project/${projectId}`);
          }
        });
      }
    },
    [model]
  );

  const handleRemove = useCallback(
    async () => {
      const is = await onRemove();
      if (is) {
        notification.success({
          title: t('common.alert.deleted'),
          onClose: () => {
            dispatch(project.actions.remove(projectId));
            history.replace('/');
          }
        });
      }
    },
    [model]
  );

  return (
    <ProjectContainer>
      <ProjectForm model={model} onChange={onChange} onSubmit={handleSubmit}>
        <CancelButton to={`/project/${projectId}`} name={t('common.cancel')} />
        <SaveButton
          allowedRole={role.Project_Edit}
          name={t('common.save')}
          loading={loading}
        />
        <RemoveButton
          allowedRole={role.Project_Edit}
          name={t('common.remove')}
          onClick={isRemove.setTrue}
          disabled={!projectId}
        />
        <Confirm
          title={t('project.delete.title')}
          content={t('project.delete')}
          open={isRemove.value}
          onOK={handleRemove}
          loading={loading}
          onCancel={isRemove.setFalse}
        />
      </ProjectForm>
    </ProjectContainer>
  );
};

export default ProjectEdit;
