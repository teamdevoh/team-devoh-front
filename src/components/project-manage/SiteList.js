import React, { Fragment } from 'react';
import { Input, Button, Grid } from 'semantic-ui-react';
import styled from 'styled-components';
import { useFormatMessage } from '../../hooks';
import SiteListTable from './SiteListTable';

const Inline = styled.div`
  display: inline-block;
`;

const SiteList = ({
  items,
  loading,
  onAddSiteClick,
  onEditSiteClick,
  onRowClick,
  selectedSiteId,
  onSearch,
  onSearchChange,
  searchValue
}) => {
  const t = useFormatMessage();

  const handleSearch = () => {
    onSearch();
  };

  const handleSearchButtonClick = args => {
    handleSearch();
  };

  const handleKeyDown = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      handleSearch();
    }
  };

  return (
    <Fragment>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Inline>
              <Input
                action={{ icon: 'search', onClick: handleSearchButtonClick }}
                onChange={onSearchChange}
                onKeyDown={handleKeyDown}
                placeholder="Search"
              />
            </Inline>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div>
              <SiteListTable
                items={items}
                loading={loading}
                onRowClick={onRowClick}
                selectedSiteId={selectedSiteId}
                searchValue={searchValue}
              />
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row textAlign="right">
          <Grid.Column>
            <Button onClick={onAddSiteClick}>{t('add.site')}</Button>
            <Button onClick={onEditSiteClick} disabled={!!!selectedSiteId}>
              {t('folder.edit')}
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Fragment>
  );
};

export default SiteList;
