import React, { useState, useEffect, useCallback, memo } from 'react';
import { Doughnut } from 'react-chartjs-2/dist/react-chartjs-2';
import api from './api';
import { Dropdown, Grid } from 'semantic-ui-react';
import { useProjectMemberKeyPair, useFormatMessage } from '../../hooks';

const MemberList = memo(({ projectId, onChange }) => {
  const member = useProjectMemberKeyPair({
    projectId,
    holder: '----------------------'
  });

  return (
    <Dropdown
      placeholder=""
      selection
      options={member.items}
      onChange={onChange}
      defaultValue={0}
    />
  );
});

const ProjectTaskSectionStatus = ({ projectId }) => {
  const t = useFormatMessage();
  const [item, setItem] = useState({ labels: [], datasets: [] });

  const chartOptions = {
    maintainAspectRatio: true,
    legend: {
      position: 'right',
      labels: {
        boxWidth: 10,
        filter: (item, data) => {
          item.text = `${item.text} : ${data.datasets[0].data[item.index]}`;
          return item;
        }
      }
    }
  };

  let isSubscribed = true;

  useEffect(() => {
    getData(0);

    return () => (isSubscribed = false);
  }, []);

  const getData = useCallback(async value => {
    try {
      const res = await api.fetchTaskSectionStatus({
        projectId,
        projectMemberId: value
      });
      const data = [
        {
          name: t('task.chart.open'),
          value: res.data.open
        },
        {
          name: t('task.chart.ongoing'),
          value: res.data.ongoing
        },
        {
          name: t('task.chart.complete'),
          value: res.data.complete
        },
        {
          name: t('task.chart.overduedate'),
          value: res.data.overDueDate
        }
      ];

      if (isSubscribed) {
        setItem({
          labels: [data[0].name, data[1].name, data[2].name, data[3].name],
          datasets: [
            {
              data: [
                data[0].value,
                data[1].value,
                data[2].value,
                data[3].value
              ],
              backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
              hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']
            }
          ]
        });
      }
    } catch (e) {
    } finally {
    }
  }, []);

  const handleMemberChange = useCallback((event, data) => {
    getData(data.value);
  }, []);

  return (
    <Grid>
      <Grid.Column width={16} textAlign="right">
        <MemberList projectId={projectId} onChange={handleMemberChange} />
      </Grid.Column>
      <Grid.Column width={16}>
        <Doughnut height={50} options={chartOptions} data={item} />
      </Grid.Column>
    </Grid>
  );
};

export default ProjectTaskSectionStatus;
