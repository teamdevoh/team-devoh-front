import React, { useRef, useState } from 'react';
import {
  Divider,
  Form,
  Input,
  TextArea,
  Container,
  Button
} from 'semantic-ui-react';
import { useSite } from './hooks';
import { SaveButton } from '../button';
import { useFormatMessage, useSubmit } from '../../hooks';
import { notification } from '../modal';
import { role } from '../../constants';
import api from './api';

const SiteForm = ({ siteId, onBackClick, setSiteId }) => {
  const t = useFormatMessage();
  const [error, setError] = useState({
    name: false
  });
  const [{ model, loading, onChange, onAdd, onEdit }] = useSite(Number(siteId));

  const saveAndMove = useRef(false);

  const setSaveAndMove = val => {
    saveAndMove.current = val;
  };

  const getSaveAndMove = () => {
    return saveAndMove.current;
  };

  const handleMovePage = () => {
    const move = getSaveAndMove();
    if (move) {
      onBackClick();
    }
  };

  const handleSubmit = async () => {
    if (Number(siteId) > 0) {
      const is = await onEdit();
      if (is) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: handleMovePage()
        });
      }
    } else {
      const newSiteId = await onAdd();
      if (newSiteId) {
        notification.success({
          title: t('common.alert.saved'),
          onClose: handleMovePage()
        });

        setSiteId(newSiteId);
      }
    }
  };

  const checkExistingSiteName = async () => {
    let siteName = model.name || '';
    siteName = siteName.trim().toLocaleLowerCase();
    let isExist = false;

    if (siteName !== '') {
      const res = await api.isExistingSite({
        siteId: model.siteId,
        name: siteName
      });

      isExist = res.data;
    }

    if (isExist) {
      setError({
        ...error,
        name: true
      });
    } else {
      setError({
        ...error,
        name: false
      });
    }

    return isExist;
  };

  const handleSaveClick = (saveAndMove = false) => async e => {
    e.preventDefault();
    setSaveAndMove(saveAndMove);
    const existing = await checkExistingSiteName();
    if (!existing) {
      onValidate();
    }
  };

  const handleBlur = async () => {
    await checkExistingSiteName();
  };

  const [{ validator, onValidate }] = useSubmit(handleSubmit);

  return (
    <Container>
      <Form>
        <Form.Group widths="equal">
          <Form.Field
            control={Input}
            label={t('name')}
            name="name"
            value={model.name || ''}
            onChange={onChange}
            onBlur={handleBlur}
            error={error.name}
            required
          />
          <Form.Field>
            <label>{t('business.reg.number')}</label>
            <Input
              name="businessNo"
              value={model.businessNo || ''}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        {error.name && (
          <span style={{ color: 'red' }}>{t('already.exists')}</span>
        )}
        {validator.message(t('name'), model.name, 'required')}
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('collection.subject.address')}</label>
            <Input
              name="address"
              value={model.address || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <label>{t('detailed.address')}</label>
            <Input
              name="addressDetail"
              value={model.addressDetail || ''}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('phone.numberr')}</label>
            <Input
              name="officePhone"
              value={model.officePhone || ''}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <label>Fax</label>
            <Input name="fax" value={model.fax || ''} onChange={onChange} />
          </Form.Field>
        </Form.Group>
        <Form.Field>
          <label>{t('folder.description')}</label>
          <TextArea
            name="description"
            value={model.description || ''}
            onChange={onChange}
          />
        </Form.Field>
        <Divider />
        <Button
          type="button"
          content={t('common.back')}
          onClick={onBackClick}
        />
        <SaveButton
          allowedRole={role.Site_Edit}
          name={t('common.save')}
          loading={loading}
          onClick={handleSaveClick(false)}
        />
        <SaveButton
          allowedRole={role.Site_Edit}
          name={t('common.save.move')}
          loading={loading}
          onClick={handleSaveClick(true)}
        />
      </Form>
    </Container>
  );
};

export default SiteForm;
