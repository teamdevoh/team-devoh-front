import React, { useState, useEffect, useCallback } from 'react';
import { HorizontalBar } from 'react-chartjs-2/dist/react-chartjs-2';
import api from './api';
import { useFormatMessage } from '../../hooks';
import './styles.css';

const ProjectTaskAssignedStatus = ({ projectId }) => {
  const t = useFormatMessage();
  const [item, setItem] = useState({});

  let isSubscribed = false;
  useEffect(() => {
    fetchData();
    return () => {
      isSubscribed = false;
    };
  }, []);

  const fetchData = useCallback(async () => {
    isSubscribed = true;
    const res = await api.fetchTaskAssignedStatus({ projectId });
    const labels = [];
    const values = [];
    for (let i = 0; i < res.data.length; i++) {
      const item = res.data[i];
      if (item.memberId === 0) {
        labels.unshift(t('task.assigned.not.count'));
        values.unshift(item.assignedTaskCount);
      } else {
        labels.push(item.memberName);
        values.push(item.assignedTaskCount);
      }
    }

    if (isSubscribed) {
      setItem({
        labels: labels,
        datasets: [
          {
            label: t('task'),
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: values
          }
        ]
      });
    }
  }, []);

  return (
    <div
      style={{
        height: '200px',
        overflowY: 'auto'
      }}
    >
      <div style={{ height: '100vh' }}>
        <HorizontalBar
          options={{
            height: 50,
            maintainAspectRatio: false
          }}
          data={item}
        />
      </div>
    </div>
  );
};

export default ProjectTaskAssignedStatus;
