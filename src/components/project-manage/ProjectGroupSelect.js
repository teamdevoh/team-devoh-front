import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'semantic-ui-react';
import api from './api';
import { useFormatMessage } from '../../hooks';
import map from 'lodash/map';

const propTypes = {
  /**
   * Shorthand for primary content.
   */
  name: PropTypes.string.isRequired,
  /**
   * Called when the user attempts to change the value.
   * @param {SyntheticEvent} event React's original SyntheticEvent.
   * @param {object} data All props and proposed value.
   */
  onChange: PropTypes.func.isRequired,
  /**
   * Search value
   */
  search: PropTypes.string,
  /**
   * Current value or value array if multiple. Creates a controlled component.
   */
  value: PropTypes.oneOfType(PropTypes.number, PropTypes.string)
};

const defaultProps = {};

const ProjectGroupSelect = ({ name, onChange, search, value }) => {
  const t = useFormatMessage();
  const [items, setItems] = useState([]);

  useEffect(() => {
    api.fetchProjectGroups({ search }).then(res => {
      const groups = [];
      map(res.data, group => {
        groups.push({
          key: group.projectGroupId,
          text: group.name,
          value: group.projectGroupId
        });
      });

      setItems(groups);
    });

    return () => {};
  }, []);

  return (
    <Dropdown
      name={name}
      options={items}
      fluid
      selection
      placeholder={t('project.group')}
      value={value}
      onChange={onChange}
    />
  );
};

ProjectGroupSelect.propTypes = propTypes;
ProjectGroupSelect.defaultProps = defaultProps;

export default ProjectGroupSelect;
