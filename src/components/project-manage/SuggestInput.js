import React, { Component } from 'react';
import { Search } from 'semantic-ui-react';
import { injectIntl } from 'react-intl';
import api from './api';
import debounce from 'lodash/debounce';

class SuggestInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      isLoading: false,
      results: [],
      value: props.value || '',
      isResultSelect: false
    };

    this.handleResultSelect = this.handleResultSelect.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleResultSelect(e, { result }) {
    const { onSelect } = this.props;
    this.setState({ value: result.title, isResultSelect: true, isOpen: false });

    onSelect(result.title);
  }

  handleSearchChange(e, { value }) {
    const { projectId } = this.props;

    if (this._isMounted) {
      this.setState({
        isLoading: true,
        isResultSelect: false,
        value,
        results: []
      });

      setTimeout(() => {
        if (this.state.value.length < 1) {
          return this.setState({ isLoading: false, results: [], value: '' });
        }

        api
          .fetchSuggest({ projectId, search: this.state.value.toLowerCase() })
          .then(res => {
            this.setState({
              isLoading: false,
              results: res.data
            });
          });
      }, 300);
    }
  }

  render() {
    const { onSelect, intl } = this.props;
    const { isLoading, isOpen, value, results, isResultSelect } = this.state;

    return (
      <Search
        noResultsMessage={intl.formatMessage({ id: 'project.noresultsfound' })}
        loading={isLoading}
        onResultSelect={this.handleResultSelect}
        onSearchChange={debounce(this.handleSearchChange, 500, {
          leading: true
        })}
        open={isOpen}
        results={results}
        value={value}
        onKeyUp={event => {
          if (event.keyCode === 13 && !isResultSelect) {
            this.setState({
              value: event.target.value,
              isResultSelect: true,
              isOpen: false
            });
            onSelect(event.target.value);
          } else {
            if (!isResultSelect) {
              this.setState({ isOpen: true });
            }
          }
        }}
      />
    );
  }
}

export default injectIntl(SuggestInput);
