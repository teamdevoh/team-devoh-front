import React, { Fragment } from 'react';
import { BackButton, SaveButton, RemoveButton } from '../button';
import { Confirm, notification } from '../modal';
import { ProjectSiteHeader, ProjectSiteForm } from './';
import { useRouteMatch, useHistory } from 'react-router-dom';
import useProjectSiteEdit from './hooks/useProjectSiteEdit';
import { useFormatMessage, useBoolean } from '../../hooks';
import { role } from '../../constants';

const ProjectSiteEdit = () => {
  const t = useFormatMessage();
  const match = useRouteMatch();
  const history = useHistory();
  const { projectId, siteId } = match.params;
  const [projectSite] = useProjectSiteEdit({ projectId, siteId });
  const isRemove = useBoolean(false);

  const handleSubmit = async () => {
    try {
      const res = await projectSite.onEdit();
      if (res.data) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            history.goBack();
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleRemove = async () => {
    try {
      const res = await projectSite.onRemove();
      if (res.data) {
        notification.success({
          title: t('common.alert.deleted'),
          onClose: () => {
            history.goBack();
          }
        });
      }
    } catch (error) {}
  };

  return (
    <Fragment>
      <ProjectSiteHeader />
      <ProjectSiteForm
        projectId={projectId}
        model={projectSite.model}
        onChange={projectSite.onChange}
        onSubmit={handleSubmit}
      >
        <BackButton name={t('common.back')} />
        <SaveButton
          allowedRole={role.ProjectSite_Edit}
          name={t('common.save')}
          loading={projectSite.loading}
        />
        <RemoveButton
          allowedRole={role.ProjectSite_Edit}
          name={t('common.remove')}
          onClick={isRemove.setTrue}
          disabled={!projectSite.model.projectSiteId}
        />
        <Confirm
          title={t('project.delete.title.projectsite')}
          content={t('project.delete.projectsite')}
          open={isRemove.value}
          onOK={handleRemove}
          loading={projectSite.loading}
          onCancel={isRemove.setFalse}
        />
      </ProjectSiteForm>
    </Fragment>
  );
};

export default ProjectSiteEdit;
