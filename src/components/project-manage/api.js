import helpers from '../../helpers';

const fetchTeams = ({ search, onlyUseProject, offset, limit }) => {
  const params = helpers.qs.stringify({
    name: search === void 0 ? '' : search,
    onlyUseProject,
    offset,
    limit
  });
  return helpers.Service.get(`api/teams?${params}`);
};

const fetchProjectGroups = ({ search, offset, limit }) => {
  const params = helpers.qs.stringify({
    title: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(`api/projectgroups?${params}`);
};

const fetchProjects = ({ search, offset, limit }) => {
  const params = helpers.qs.stringify({
    title: search === void 0 ? '' : search,
    offset,
    limit
  });

  return helpers.Service.get(`api/projects?${params}`);
};

const fetchProject = ({ projectId }) => {
  return helpers.Service.get(`api/projects/${projectId}`);
};

const fetchProjectStatus = ({ projectId, historyDate }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/status?historyDate=${historyDate}`
  );
};

const fetchTaskSectionStatus = ({ projectId, projectMemberId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/status.tasks?projectMemberId=${projectMemberId}`
  );
};

const fetchTaskAssignedStatus = ({ projectId }) => {
  return helpers.Service.get(`api/projects/${projectId}/status.assigned`);
};

const fetchSites = ({ search, offset, limit }) => {
  const params = helpers.qs.stringify({
    name: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(`api/sites?${params}`);
};

const isExistingSite = ({ siteId, name }) => {
  const params = helpers.qs.stringify({
    siteId,
    name
  });
  return helpers.Service.get(`api/sites/exist?${params}`);
};

const fetchSite = ({ siteId }) => {
  return helpers.Service.get(`api/sites/${siteId}`);
};

const addSite = ({ model }) => {
  return helpers.Service.post(`api/sites`, model);
};

const updateSite = ({ model }) => {
  return helpers.Service.put(`api/sites`, model);
};

const fetchProjectSites = ({ projectId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    name: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(`api/projects/${projectId}/sites?${params}`);
};

const fetchProjectSite = ({ projectId, siteId }) => {
  return helpers.Service.get(`api/projects/${projectId}/sites/${siteId}`);
};

const fetchMembersNotIn = ({ projectId, search, siteId, offset, limit }) => {
  const params = helpers.qs.stringify({
    memberName: search === void 0 ? '' : search,
    siteId,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projects/${projectId}/members.general?${params}`
  );
};

const fetchProjectMembers = ({
  projectId,
  search,
  offset,
  limit,
  projectSiteId
}) => {
  const params = helpers.qs.stringify({
    memberName: search === void 0 ? '' : search,
    projectSiteId,
    offset,
    limit
  });
  return helpers.Service.get(`api/projects/${projectId}/members?${params}`);
};

const fetchProjectMember = ({ projectId, id }) => {
  return helpers.Service.get(`api/projects/${projectId}/members/${id}`);
};

const fetchSuggest = ({ projectId, search }) => {
  const params = helpers.qs.stringify({
    title: search
  });
  return helpers.Service.get(`api/projects/${projectId}/suggest?${params}`);
};

const fetchTasksSearching = ({ projectId, search }) => {
  const params = helpers.qs.stringify({
    title: search
  });
  return helpers.Service.get(
    `api/projects/${projectId}/searching.tasks?${params}`
  );
};

const fetchTaskAttachsSearching = ({ projectId, search }) => {
  const params = helpers.qs.stringify({
    title: search
  });
  return helpers.Service.get(
    `api/projects/${projectId}/searching.attachs?${params}`
  );
};

const fetchSearchingSummary = ({ projectId, search }) => {
  const params = helpers.qs.stringify({
    title: search
  });
  return helpers.Service.get(
    `api/projects/${projectId}/searching.summary?${params}`
  );
};

const addProject = ({ model }) => {
  return helpers.Service.post('api/projects', model);
};

const updateProject = ({ projectId, model }) => {
  return helpers.Service.put(`api/projects/${projectId}`, model);
};

const removeProject = ({ projectId }) => {
  return helpers.Service.delete(`api/projects/${projectId}`);
};

const addProjectSite = ({ projectId, siteId, model }) => {
  return helpers.Service.post(
    `api/projects/${projectId}/sites/${siteId}`,
    model
  );
};

const updateProjectSite = ({ projectId, siteId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/sites/${siteId}`,
    model
  );
};

const removeProjectSite = ({ projectId, siteId }) => {
  return helpers.Service.delete(`api/projects/${projectId}/sites/${siteId}`);
};

const addProjectMember = ({ projectId, memberId, model }) => {
  return helpers.Service.post(
    `api/projects/${projectId}/members/${memberId}`,
    model
  );
};

const updateProjectMember = ({ projectId, memberId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/members/${memberId}`,
    model
  );
};

const removeProjectMember = ({ projectId, memberId }) => {
  return helpers.Service.delete(
    `api/projects/${projectId}/members/${memberId}`
  );
};

/**
 * 맴버별 참여중인 프로젝트 리스트 반환
 * @param {json} param0
 */
const fetchProjectsByMember = ({ memberId }) => {
  return helpers.Service.get(`api/Members/${memberId}/projects`);
};

const fetchMyProjectSites = ({ projectId, memberId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/members/${memberId}/sites`
  );
};

const fetchMemberSiteList = ({ offset, limit } = {}) => {
  const params = helpers.qs.stringify({
    offset,
    limit
  });
  return helpers.Service.get(`api/members/sites?${params}`);
};

export default {
  addProject,
  updateProject,
  removeProject,
  addProjectSite,
  updateProjectSite,
  removeProjectSite,
  addProjectMember,
  updateProjectMember,
  removeProjectMember,
  fetchTeams,
  fetchProjectGroups,
  fetchProjects,
  fetchProject,
  fetchProjectStatus,
  fetchTaskSectionStatus,
  fetchTaskAssignedStatus,
  fetchSites,
  isExistingSite,
  fetchSite,
  addSite,
  updateSite,
  fetchProjectSites,
  fetchProjectSite,
  fetchMembersNotIn,
  fetchProjectMembers,
  fetchProjectMember,
  fetchSuggest,
  fetchTasksSearching,
  fetchTaskAttachsSearching,
  fetchSearchingSummary,
  fetchProjectsByMember,
  fetchMyProjectSites,
  fetchMemberSiteList
};
