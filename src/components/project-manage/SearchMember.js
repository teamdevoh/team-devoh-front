import React, { useState, useEffect } from 'react';
import { Input, Button, Grid, Form, Select } from 'semantic-ui-react';
import styled from 'styled-components';
import { useFormatMessage } from '../../hooks';
import useSkipMembers from './hooks/useSkipMembers';
import { DataGrid } from '../data-grid';
import api from './api';

const GridRowWrapper = styled.div`
  cursor: pointer;
  ${props => {
    const { selected } = props;
    let result = '';

    if (selected) {
      result += `
        & .react-grid-Cell {
          background-color: #f8ffff;
        }`;
    }

    return result;
  }};
`;

// const InfoWrapper = styled.div`
//   color: #455;
//   margin-top: 7px;
// `;

/**
 * 선택된 맴버 표시해주기 위한
 * @param {int} curMemberId
 * @param {json} selectedMember
 */
const RowRenderer = curMemberId => ({ renderBaseRow, ...props }) => {
  const { memberId } = props.row;

  return (
    <GridRowWrapper selected={curMemberId === memberId}>
      {renderBaseRow(props)}
    </GridRowWrapper>
  );
};

const SearchMember = ({
  name,
  memberId,
  projectId,
  onChange,
  onSelecteMember,
  onClose
}) => {
  const t = useFormatMessage();
  const [searchName, setSearchName] = useState(name);
  const [siteId, setSiteId] = useState(0);
  const [{ items, loading }, setIsDo] = useSkipMembers(
    searchName,
    projectId,
    siteId
  );
  const [selectedMember, setSelectedMember] = useState(null);
  const [tableInfoMessage, setTableInfoMessage] = useState(
    searchName === '' ? t('please.enter.a.search.term') : ''
  );
  const [siteItems, setSIteItems] = useState([]);

  const columns = [
    {
      key: 'siteName',
      name: t('project.member.site')
    },
    {
      key: 'memberName',
      name: t('user.name')
    },
    {
      key: 'position',
      name: t('user.position')
    }
  ];

  const getMembers = () => {
    setIsDo(new Date());
    setTableInfoMessage(
      searchName === ''
        ? t('please.enter.a.search.term')
        : t('user.already.or.no')
    );
  };

  useEffect(
    () => {
      setSelectedMember(null);
    },
    [items]
  );

  useEffect(() => {
    const fetch = async () => {
      const res = await api.fetchMemberSiteList();
      const options = [];
      for (let i = 0; i < res.data.items.length; i++) {
        const item = res.data.items[i];
        options.push({
          text: item.name,
          value: item.siteId
        });
      }

      setSIteItems(options);
    };
    fetch();
  }, []);

  const handleKeyDown = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      getMembers(event.target.value, projectId);
    }
  };

  /**
   * 모달창에서 이름 검색 input onChange handle
   * @param {} event
   * @param {json} param1
   */
  const handleSearchNameChange = (event, { value }) => {
    setSearchName(value);
  };

  /**
   * 사용자 검색
   */
  const handleSearchMember = () => {
    getMembers(searchName, projectId);
  };

  /**
   * 사용자 선택 완료
   */
  const handleSelectMember = () => {
    onChange(null, { name: 'memberId', value: selectedMember.memberId });
    onSelecteMember(selectedMember);
    onClose();
  };

  /**
   * grid row 클릭 이벤트
   * @param {int} index
   * @param {json} row
   */
  const handleRowClick = (index, row, ...rest) => {
    if (selectedMember !== null && row.memberId === selectedMember.memberId) {
      return;
    } else {
      setSelectedMember(row);
    }
  };

  const handleRowDoubleClick = (index, row) => {
    const { memberId } = row;
    onChange(null, { name: 'memberId', value: memberId });
    onSelecteMember(row);
    onClose();
  };

  const handleProjectSiteChange = (e, { value }) => {
    setSiteId(value);
  };

  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <Form>
            <Form.Group widths="equal">
              <Form.Field>
                <label>{t('project.site')}</label>
                <Select
                  item
                  name="siteId"
                  value={siteId}
                  onChange={handleProjectSiteChange}
                  options={[
                    {
                      text: t('all'),
                      value: 0
                    }
                  ].concat(siteItems)}
                />
              </Form.Field>
              <Form.Field>
                <label>{t('name')}</label>
                <Input
                  action={{ icon: 'search', onClick: handleSearchMember }}
                  value={searchName}
                  onChange={handleSearchNameChange}
                  onKeyDown={handleKeyDown}
                />
              </Form.Field>
            </Form.Group>
          </Form>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row
        style={{
          paddingTop: '0px'
        }}
      >
        <Grid.Column>
          <Button
            type="button"
            icon="plus"
            onClick={handleSelectMember}
            disabled={selectedMember === null}
          />
          <div>
            <DataGrid
              columns={columns}
              loading={loading}
              rows={items}
              rowKey="memberId"
              rowNumber={{ show: true, key: 'no', name: 'No' }}
              onRowClick={handleRowClick}
              onRowDoubleClick={handleRowDoubleClick}
              rowRenderer={RowRenderer(memberId)}
              emptyMessage={tableInfoMessage}
            />
          </div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default SearchMember;
