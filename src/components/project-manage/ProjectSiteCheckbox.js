import React, { Component } from 'react';
import { Checkbox } from 'semantic-ui-react';
import api from './api';
import map from 'lodash/map';
import styled from 'styled-components';

const CheckboxWrap = styled.div`
  display: flex;
  flex-direction: column;
`;

class ProjectSiteCheckbox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      projectSites: []
    };

    this.handleCheck = this.handleCheck.bind(this);
  }

  componentDidMount() {
    this.getProjectSites();
  }

  getProjectSites() {
    const { projectId } = this.props;
    api.fetchProjectSites({ projectId }).then(res => {
      this.setState({ projectSites: res.data.items });
    });
  }

  handleCheck(event, data, projectSiteId) {
    const { value, onChange } = this.props;
    const items = value;
    const index = items.indexOf(projectSiteId);
    if (data.checked) {
      if (index === -1) {
        items.push(projectSiteId);
      }
    } else {
      items.splice(index, 1);
    }

    onChange(event, { name: data.name, value: items });
  }

  render() {
    const { handleCheck } = this;
    const { name, value } = this.props;
    const { projectSites } = this.state;

    return (
      <CheckboxWrap>
        {map(projectSites, projectSite => {
          return (
            <Checkbox
              key={projectSite.projectSiteId}
              name={name}
              checked={value.indexOf(projectSite.projectSiteId) > -1}
              label={projectSite.siteName}
              onChange={(event, data) => {
                handleCheck(event, data, projectSite.projectSiteId);
              }}
            />
          );
        })}
      </CheckboxWrap>
    );
  }
}

export default ProjectSiteCheckbox;
