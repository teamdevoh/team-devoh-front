import React, { useState, Fragment, useRef } from 'react';
import { Button } from 'semantic-ui-react';
import { useFormatMessage, useBoolean } from '../../hooks';
import SiteList from './SiteList';
import SiteForm from './SiteForm';
import { Modal } from '../modal';
import { useSiteList } from './hooks';

const SiteListContainer = ({ itemRefresh }) => {
  const t = useFormatMessage();
  const sites = useSiteList({ search: '' });
  const [siteId, setSiteId] = useState();
  const showModal = useBoolean(false);
  const showSiteForm = useBoolean(false);
  const search = useRef('');

  const setSearch = value => {
    search.current = value;
  };

  const getSearch = () => {
    return search.current;
  };

  const resetListModal = () => {
    setSearch('');
    setSiteId();
  };

  const handleSiteListMOepn = () => {
    sites.fetchItems({ search: '' });
    showModal.setTrue();
  };

  const handleAddSiteClick = () => {
    setSiteId();
    showSiteForm.setTrue();
  };
  const handleEditSiteClick = () => {
    showSiteForm.setTrue();
  };

  const handleRowClick = ({ siteId: selectedSiteId }) => () => {
    setSiteId(siteId === selectedSiteId ? null : selectedSiteId);
  };

  const handleSiteListClose = () => {
    itemRefresh();
    showModal.setFalse();

    resetListModal();
  };

  const handleSiteFormClose = () => {
    sites.fetchItems({ search: getSearch() });
    showSiteForm.setFalse();
  };

  const handleSearch = () => {
    setSiteId();
    sites.fetchItems({ search: getSearch() });
  };

  const handleSearchChange = (e, { value }) => {
    setSearch(value);
  };

  return (
    <Fragment>
      <Button type="button" icon="plus" onClick={handleSiteListMOepn} />
      <Modal
        size="fullscreen"
        open={showModal.value}
        onOK={handleSiteListClose}
        title={t('project.member.site')}
        content={
          <SiteList
            items={sites.items}
            loading={sites.loading}
            onAddSiteClick={handleAddSiteClick}
            onEditSiteClick={handleEditSiteClick}
            onRowClick={handleRowClick}
            selectedSiteId={siteId}
            onSearch={handleSearch}
            onSearchChange={handleSearchChange}
            searchValue={getSearch()}
          />
        }
      />
      <Modal
        size="large"
        open={showSiteForm.value}
        onOK={handleSiteFormClose}
        title={siteId > 0 ? t('edit.site') : t('add.site')}
        content={
          <SiteForm
            siteId={siteId}
            setSiteId={setSiteId}
            onBackClick={handleSiteFormClose}
          />
        }
      />
    </Fragment>
  );
};

export default SiteListContainer;
