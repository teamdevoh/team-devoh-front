import React, { Fragment, useState, useEffect } from 'react';
import { Form, Divider, TextArea, Button } from 'semantic-ui-react';
import styled from 'styled-components';
import { DateRangePicker } from '../datepicker';
import { TeamSelect, ProjectSiteCheckbox } from './';
import { PercentageInput } from '../input';
import { useFormatMessage, useSubmit, useBoolean } from '../../hooks';
import { Modal } from '../modal';
import helpers from '../../helpers';
import api from './api';
import { DataGrid } from '../data-grid';
import { format } from '../../constants';

const RateWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const IconButton = styled(Button)`
  &&& {
    font-size: 30px;
    padding: 0;
    margin: 0;
    margin-left: 5px;
    height: 100%;
  }
`;

const ProjectMemberForm = ({
  children,
  projectId,
  model,
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);
  const showInfoModal = useBoolean(false);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const columns = [
    {
      key: 'title',
      name: t('project')
    },
    {
      key: 'projectStatus',
      name: t('status'),
      width: 100
    },
    {
      key: 'role',
      name: t('role'),
      width: 70
    },
    {
      key: 'rate',
      name: t('project.member.rate'),
      formatter: ({ value }) => {
        return value === null ? '' : `${value}%`;
      },
      width: 80
    },
    {
      key: 'period',
      name: t('project.member.schedule'),
      formatter: ({ row }) => {
        const dateformat = helpers.util.dateformat;
        const { scheduleFromDate, scheduleToDate } = row;
        let result = '';

        if (scheduleFromDate && scheduleToDate) {
          result = `${dateformat(
            scheduleFromDate,
            format.YYYY_MM_DD
          )} ~ ${dateformat(scheduleToDate, format.YYYY_MM_DD)}`;
        }

        return result;
      },
      width: 195
    }
  ];

  useEffect(
    () => {
      if (showInfoModal.value && model.memberId !== 0) {
        setLoading(true);
        api
          .fetchProjectsByMember({ memberId: model.memberId })
          .then(response => {
            setItems(response.data.items);
          });

        setLoading(false);
      }
    },
    [model.memberId, showInfoModal]
  );

  const handleOpenInfoModal = () => {
    showInfoModal.setTrue();
  };

  return (
    <Fragment>
      <Form onSubmit={onValidate}>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('project.member')}</label>
            <div>{model.memberName}</div>
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('project.member.team')}</label>
            <TeamSelect
              name="teamId"
              placeholder={t('project.member.team')}
              value={model.teamId}
              onChange={onChange}
            />
            {validator.message(
              t('project.member.team'),
              model.teamId,
              'required'
            )}
          </Form.Field>
          <Form.Field>
            <label>{t('project.member.rate')}</label>
            <RateWrapper>
              <PercentageInput
                name="rate"
                placeholder={t('project.member.rate')}
                value={model.rate}
                onChange={onChange}
              />
              {/* 맴버 에게 할당된 프로젝트 내역 확인 버튼 */}
              <IconButton
                type="button"
                icon="address card"
                onClick={handleOpenInfoModal}
              />
            </RateWrapper>
          </Form.Field>
          <Form.Field>
            <label>{t('project.member.schedule')}</label>
            <DateRangePicker
              name="scheduleDate"
              value={model.scheduleDate}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group>
          <Form.Field>
            <label>{t('project.site')}</label>
            <ProjectSiteCheckbox
              name="projectSites"
              projectId={projectId}
              value={model.projectSites}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('project.sitenote')}</label>
            <TextArea
              name="note"
              value={model.note == null ? '' : model.note}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Divider />
        {children}
      </Form>
      <Modal
        open={showInfoModal.value}
        onOK={showInfoModal.setFalse}
        title={t('project.participating')}
        content={
          <div>
            <DataGrid
              columns={columns}
              rows={items}
              loading={loading}
              rowNumber={{ show: true, key: 'no', name: 'No' }}
              rowKey="title"
            />
          </div>
        }
      />
    </Fragment>
  );
};

export default ProjectMemberForm;
