import React, { Component } from 'react';
import { Dropdown } from 'semantic-ui-react';
import api from './api';
import { RoleAware } from '../auth';
import map from 'lodash/map';
import { role } from '../../constants';

class TeamSelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      teams: []
    };
  }
  componentDidMount() {
    this.getTeams();
  }

  getTeams() {
    const { search } = this.props;
    api.fetchTeams({ search, onlyUseProject: true }).then(res => {
      const items = [];
      map(res.data.items, team => {
        items.push({ text: team.teamName, value: team.teamId });
      });

      this.setState({ teams: items });
    });
  }

  render() {
    const { name, onChange, placeholder, value } = this.props;
    const { teams } = this.state;

    return (
      <RoleAware allowedRole={role.Team_Read}>
        <Dropdown
          name={name}
          fluid
          selection
          options={teams}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
        />
      </RoleAware>
    );
  }
}
export default TeamSelect;
