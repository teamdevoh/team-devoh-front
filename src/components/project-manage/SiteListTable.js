import React, { useState, useEffect } from 'react';
import { Table } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { TableWrap, StyledTh } from '../case-review/styles';
import { EmptyRowsViewForSemantic } from '../modules';
import memberApi from '../account-manage/api';
import styled from 'styled-components';

const StyledRow = styled(Table.Row)`
  &&& {
    cursor: pointer;

    ${props => {
      if (props.selected) {
        return 'background-color: aliceblue;';
      }
    }};
  }
`;

const SiteListTable = ({
  items,
  loading,
  onRowClick,
  selectedSiteId,
  searchValue
}) => {
  const t = useFormatMessage();
  const [members, setMembers] = useState({});

  const fetchMemberList = async () => {
    const res = await memberApi.fetchMembers({ isApprove: true });

    const newMembers = {};
    res.data.items.forEach(item => {
      newMembers[item.memberId] = {
        ...item
      };
    });
    setMembers(newMembers);
  };

  useEffect(() => {
    fetchMemberList();
  }, []);

  return (
    <TableWrap maxHeight="483px">
      <Table celled selectable>
        <Table.Header>
          <Table.Row>
            <StyledTh singleLine>No</StyledTh>
            <StyledTh>{t('name')}</StyledTh>
            <StyledTh>{t('business.reg.number')}</StyledTh>
            <StyledTh>{t('collection.subject.address')}</StyledTh>
            <StyledTh>{t('detailed.address')}</StyledTh>
            <StyledTh>{t('phone.numberr')}</StyledTh>
            <StyledTh>Fax</StyledTh>
            <StyledTh>{t('folder.description')}</StyledTh>
            <StyledTh>{t('project.member')}</StyledTh>
            <StyledTh>{t('common.date')}</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {items.map((item, index) => {
            return (
              <StyledRow
                key={item.siteId}
                onClick={onRowClick({ ...item })}
                selected={selectedSiteId === item.siteId}
              >
                <Table.Cell>{index + 1}</Table.Cell>
                <Table.Cell>{item.name}</Table.Cell>
                <Table.Cell>{item.businessNo}</Table.Cell>
                <Table.Cell>{item.address}</Table.Cell>
                <Table.Cell>{item.addressDetail}</Table.Cell>
                <Table.Cell>{item.officePhone}</Table.Cell>
                <Table.Cell>{item.fax}</Table.Cell>
                <Table.Cell>{item.description}</Table.Cell>
                <Table.Cell>
                  {members[item.modifier] && members[item.modifier].memberName}
                </Table.Cell>
                <Table.Cell>{item.modified.substr(0, 10)}</Table.Cell>
              </StyledRow>
            );
          })}
          {items.length === 0 && (
            <EmptyRowsViewForSemantic
              loading={loading}
              columnLength={10}
              message={
                searchValue.length > 0 ? t('project.noresultsfound') : false
              }
            />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default SiteListTable;
