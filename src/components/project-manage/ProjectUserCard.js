import React, { Component } from 'react';
import { Header, Grid, Image } from 'semantic-ui-react';
import { injectIntl } from 'react-intl';
import { api as AmApi } from '../account-manage';
import { Api } from '../project-manage';
import { Modal } from '../modal';
import helpers from '../../helpers';
import map from 'lodash/map';

class ProjectUserCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      largeFace: '',
      profile: {
        mySites: [],
        projectTeam: ''
      }
    };
    this.getUserDetail = this.getUserDetail.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async getUserDetail() {
    const { memberId, projectId } = this.props;
    await AmApi.fetchMember(memberId).then(res => {
      if (res.data.myFaceFileStorageId) {
        helpers.util.getFace(res.data.myFaceFileStorageId, 100).then(url => {
          this.setState({ largeFace: url });
        });
      }
      this.setState({ profile: res.data, show: true });
    });

    let mySites = [];
    const promises = [
      Api.fetchProjectSites({ projectId }),
      Api.fetchProjectMember({ projectId, id: memberId })
    ];
    const result = await Promise.all(promises);
    const sites = result[0].data.items;
    const member = result[1].data;
    for (let i = 0; i < sites.length; i++) {
      const site = sites[i];
      if (member.projectSites.indexOf(site.projectSiteId) > -1) {
        mySites.push(site.siteName);
      }
    }

    if (this._isMounted) {
      this.setState({
        profile: {
          ...this.state.profile,
          mySites: mySites,
          projectTeam: member.teamName
        }
      });
    }
  }

  render() {
    const { getUserDetail } = this;
    const { className, faceUrl, intl, style, isAvatar } = this.props;
    const { largeFace, profile, show } = this.state;

    const options = {
      ...(!isAvatar && { size: 'mini' }),
      style: {
        cursor: 'pointer',
        height: isAvatar ? null : '30px',
        width: isAvatar ? null : '30px'
      }
    };

    return (
      <div className={className} style={style}>
        <Image
          src={faceUrl}
          rounded
          avatar={isAvatar}
          onClick={getUserDetail}
          {...options}
        />
        <Modal
          open={show}
          onOK={() => {
            this.setState({ show: false });
          }}
          title={intl.formatMessage({ id: 'user.profile' })}
          content={
            <Grid>
              <Grid.Column>
                <div>
                  <Image circular src={largeFace} />
                  <Header dividing>
                    {intl.formatMessage({ id: 'user.name' })}
                  </Header>
                  <p>{profile.memberName}</p>
                  <Header dividing>
                    {intl.formatMessage({ id: 'user.id' })}
                  </Header>
                  <p>{profile.loginName}</p>
                  <Header dividing>
                    {intl.formatMessage({ id: 'user.email' })}
                  </Header>
                  <p>
                    <a
                      href={`mailto:${`${profile.email1}@${profile.email2}`}`}
                    >{`${profile.email1}@${profile.email2}`}</a>
                  </p>
                  <Header dividing>
                    {intl.formatMessage({ id: 'user.cellphone' })}
                  </Header>
                  <a href={`tel:${profile.cellPhone}`}>{profile.cellPhone}</a>
                  <Header dividing>
                    {intl.formatMessage({ id: 'user.officephone' })}
                  </Header>
                  <a href={`tel:${profile.officePhone}`}>
                    {profile.officePhone}
                  </a>
                  <Header dividing>
                    {intl.formatMessage({ id: 'user.department' })}
                  </Header>
                  <p>{profile.department}</p>
                  <Header dividing>
                    {intl.formatMessage({ id: 'user.position' })}
                  </Header>
                  <p>{profile.position}</p>
                  <Header dividing>
                    {intl.formatMessage({ id: 'user.description' })}
                  </Header>
                  <p>{profile.description}</p>
                  <Header dividing>{intl.formatMessage({ id: 'role' })}</Header>
                  <p>{profile.projectTeam}</p>
                  <Header dividing>{intl.formatMessage({ id: 'team' })}</Header>
                  <ul>
                    {map(profile.team, team => {
                      return <li key={`${team.teamId}`}>{team.teamName}</li>;
                    })}
                  </ul>
                  <Header dividing>
                    {intl.formatMessage({ id: 'project.site' })}
                  </Header>
                  <ul>
                    {map(profile.mySites, (mySite, index) => {
                      return <li key={index}>{mySite}</li>;
                    })}
                  </ul>
                </div>
              </Grid.Column>
            </Grid>
          }
        />
      </div>
    );
  }
}

export default injectIntl(ProjectUserCard);
