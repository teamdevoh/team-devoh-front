import React from 'react';
import { Divider, Form, Input, TextArea } from 'semantic-ui-react';
import { DateRangePicker } from '../datepicker';
import { IntegerInput } from '../input';
import { useFormatMessage, useSubmit } from '../../hooks';

const ProjectSiteForm = ({ children, model, onChange, onSubmit }) => {
  const t = useFormatMessage();
  const [{ onValidate }] = useSubmit(onSubmit);

  return (
    <Form onSubmit={onValidate}>
      {model.siteId > 0 && (
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('project.site')}</label>
            <div>{model.siteName}</div>
          </Form.Field>
        </Form.Group>
      )}
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.irbno')}</label>
          <Input name="irbno" value={model.irbno || ''} onChange={onChange} />
        </Form.Field>
        <Form.Field>
          <label>{t('project.targetcount')}</label>
          <IntegerInput
            name="targetCount"
            placeholder={t('project.targetcount')}
            value={model.targetCount}
            onChange={onChange}
          />
        </Form.Field>
        {/* 대상자 번호 고정문자 */}
        <Form.Field>
          <label>{t('subject.number.prefix')}</label>
          <Input
            name="subjectPrefix"
            placeholder={t('subject.number.prefix')}
            value={model.subjectPrefix}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('project.siteschedule')}</label>
          <DateRangePicker
            name="scheduleDate"
            value={model.scheduleDate}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('project.sitenote')}</label>
          <TextArea name="note" value={model.note || ''} onChange={onChange} />
        </Form.Field>
      </Form.Group>
      <Divider />
      {children}
    </Form>
  );
};

export default ProjectSiteForm;
