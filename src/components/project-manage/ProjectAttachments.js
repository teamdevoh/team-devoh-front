import React, { useState, useCallback, useEffect } from 'react';
import { Button, Item } from 'semantic-ui-react';
import helpers from '../../helpers';
import { Api } from './';
import history from '../../helpers/history';
import { Confirm } from '../modal';
import { useFormatMessage } from '../../hooks';
import styled from 'styled-components';
import map from 'lodash/map';
import find from 'lodash/find';
import orderBy from 'lodash/orderBy';

const StyledMeta = styled(Item.Meta)`
  word-break: break-word;
`;

const Meta = ({ value }) => {
  return <StyledMeta>{value}</StyledMeta>;
};

const TagLink = ({ tag, onClick }) => {
  const handleClick = useCallback(() => {
    onClick(tag.trim());
  }, []);

  return <a onClick={handleClick}>{`#${tag}  `}</a>;
};

const Tags = ({ projectId, tags }) => {
  const t = useFormatMessage();
  const [search, setSearch] = useState({
    open: false,
    tag: '',
    location: ''
  });

  const handleTagClick = useCallback(tag => {
    const encodedKeyword = encodeURIComponent(tag);
    setSearch({
      open: true,
      tag,
      location: `/project/${projectId}/search?keyword=${encodedKeyword}`
    });
  }, []);

  const handleGoSearch = useCallback(
    () => {
      history.push(search.location);
    },
    [search.location]
  );

  const handleClose = useCallback(() => {
    setSearch({ ...this.search, open: false });
  }, []);

  return (
    <React.Fragment>
      {tags &&
        map(tags.split('#'), tag => {
          if (tag) {
            return <TagLink key={tag} tag={tag} onClick={handleTagClick} />;
          }
        })}
      <Confirm
        title={t('common.search')}
        content={t('project.search.is', { name: search.tag })}
        open={search.open}
        onOK={handleGoSearch}
        onCancel={handleClose}
      />
    </React.Fragment>
  );
};

const DownloadButton = ({ taskItemId, taskAttachmentId }) => {
  const handleDownload = useCallback(() => {
    helpers.util.download(
      `api/tasks/${taskItemId}/attach/${taskAttachmentId}`,
      percent => {}
    );
  }, []);

  return (
    <Button
      size="tiny"
      circular
      color="twitter"
      icon="cloud download"
      onClick={handleDownload}
    />
  );
};

const ProjectAttachments = ({ projectId, query }) => {
  const [attaches, setAttaches] = useState([]);

  const imageNames = [
    { name: 'images/pdf.png', type: 'pdf' },
    { name: 'images/word.png', type: 'doc' },
    { name: 'images/word.png', type: 'docx' },
    { name: 'images/excel.png', type: 'xls' },
    { name: 'images/excel.png', type: 'xlsx' },
    { name: 'images/notepad.png', type: 'txt' },
    { name: 'images/notepad.png', type: 'csv' },
    { name: 'images/powerpoint.png', type: 'ppt' },
    { name: 'images/powerpoint.png', type: 'pptx' },
    { name: 'images/notepad.png', type: 'hwp' }
  ];

  const getImageName = type => {
    const item = find(imageNames, { type: type });
    if (item) {
      return item.name;
    } else {
      return 'images/file.png';
    }
  };

  let isSubscribed = false;
  useEffect(() => {
    isSubscribed = true;
    getAttachs();
    return () => {
      isSubscribed = false;
    };
  }, []);

  const getAttachs = useCallback(async () => {
    if (query) {
      const res = await Api.fetchTaskAttachsSearching({
        projectId,
        search: query
      });

      if (isSubscribed) {
        const files = [];

        map(res.data, async item => {
          const filenames = item.title.split('.');
          const extension = filenames[filenames.length - 1].toLowerCase();

          const fileObj = await helpers.file.setFileInfo({
            ...item,
            id: item.id,
            originalName: item.title,
            extension: `image/${extension}`,
            thumbnailApiUrl: `api/tasks/${item.taskItemId}/attach.thumbnail/${
              item.id
            }`
          });

          files.push({
            ...fileObj,
            taskItemId: item.taskItemId
          });
          if (res.data.length === files.length) {
            if (isSubscribed) {
              setAttaches(orderBy(files, ['key']));
            }
          }
        });
      }
    }
  }, []);

  return (
    <div>
      <Item.Group divided>
        {map(attaches, file => {
          let imageUrl = '';
          if (file.type.indexOf('image') > -1) {
            imageUrl = file.imageUrl;
          } else {
            imageUrl = getImageName(file.type);
          }

          return (
            <Item key={file.key}>
              <Item.Image src={imageUrl} size="tiny" />
              <Item.Content>
                <Meta value={file.name} />
                <Item.Description>
                  <Tags tags={file.tags} projectId={projectId} />
                </Item.Description>
                <Item.Extra>
                  <DownloadButton
                    taskItemId={file.taskItemId}
                    taskAttachmentId={file.key}
                  />
                </Item.Extra>
              </Item.Content>
            </Item>
          );
        })}
      </Item.Group>
    </div>
  );
};

export default ProjectAttachments;
