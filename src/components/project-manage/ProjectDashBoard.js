import React, { useCallback, useState, memo, Fragment } from 'react';
import {
  Container,
  Header,
  Grid,
  Responsive,
  Divider,
  Icon,
  Label,
  Menu,
  Loader,
  Card
} from 'semantic-ui-react';
import { RoleAware } from '../auth';
import {
  ProjectHeader,
  ProjectStatus,
  ProjectMemberList,
  ProjectTaskSectionStatus,
  ProjectSiteList,
  ProjectTaskAssignedStatus
} from './';
import { TaskTodoList, TaskNotifyContainer } from '../task-manage';
import { notification } from '../modal';
import TaskEditLayer from '../task-manage/TaskEditLayer';
import {
  useProjectMembers,
  useFetch,
  ListModel,
  useFormatMessage
} from '../../hooks';
import helpers from '../../helpers';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { project } from '../../states';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { format, role } from '../../constants';

const StyledTitle = styled.div`
  textalign: left;
`;

const ProjectCompContainer = styled(Grid)`
  && {
    margin-top: 0.5em;
  }
`;

const MainHeader = () => {
  const selectedProject = useSelector(project.selectors.getSelectedItem);

  const t = useFormatMessage();

  const period = helpers.util.dateRange(
    selectedProject.periodFrom,
    selectedProject.periodTo,
    format.YYYY_MM_DD
  );

  const handleClick = useCallback(() => {
    helpers.history.push(`/project/${selectedProject.key}/edit`);
  }, []);

  return (
    <ProjectHeader
      title={selectedProject.name}
      engTitle={selectedProject.engName}
      period={period}
      protocolNo={`${t('project.protocolno')} : ${selectedProject.protocolNo}`}
      onClick={handleClick}
    />
  );
};

const ChartHeader = ({ icon, title }) => {
  return (
    <Fragment>
      <Header size="small">
        <Fragment>
          <Icon name={icon} />
          {title}
        </Fragment>
      </Header>
      <Divider />
    </Fragment>
  );
};

const Status = memo(({ projectId }) => {
  const t = useFormatMessage();
  return (
    <Fragment>
      <ChartHeader icon="chart line" title={t('project.status')} />
      <RoleAware allowedRole={role.Task_Read}>
        <ProjectStatus projectId={projectId} />
      </RoleAware>
    </Fragment>
  );
});

const ProgressStatus = memo(({ projectId }) => {
  const t = useFormatMessage();
  return (
    <Fragment>
      <ChartHeader icon="chart pie" title={t('project.task.progres.status')} />
      <Container>
        <RoleAware allowedRole={role.Task_Read}>
          <ProjectTaskSectionStatus projectId={projectId} />
        </RoleAware>
      </Container>
    </Fragment>
  );
});

const TaskStatus = memo(({ projectId }) => {
  const t = useFormatMessage();
  return (
    <Fragment>
      <ChartHeader
        icon="chart bar outline"
        title={t('project.task.assigned.status')}
      />
      <Container>
        <RoleAware allowedRole={role.Task_Read}>
          <ProjectTaskAssignedStatus projectId={projectId} />
        </RoleAware>
      </Container>
    </Fragment>
  );
});

const MyInfoHeader = memo(({ icon, isMore, onClick, title }) => {
  return (
    <StyledTitle>
      <Menu borderless compact size="mini">
        <Menu.Item as="a" onClick={onClick}>
          <Icon name={icon} />
          {title}
          {isMore && (
            <Label color="teal" floating>
              {`3+`}
            </Label>
          )}
        </Menu.Item>
      </Menu>
    </StyledTitle>
  );
});

const Todos = memo(({ projectId }) => {
  const history = useHistory();
  const t = useFormatMessage();
  const [task, setTask] = useState({
    show: false,
    taskItemId: 0
  });
  const url = `api/projects/${projectId}/todos`;
  const query = helpers.qs.stringify({
    name: '',
    offset: 0,
    limit: 3
  });
  const [todo, doFetch] = useFetch(`${url}?${query}`, new ListModel());

  const handleClickTodo = useCallback(({ id }) => {
    setTask({
      show: true,
      taskItemId: id
    });
  }, []);

  const handleGoMyTask = useCallback(() => {
    history.push(`/project/${projectId}/task/portal/my`);
  }, []);

  const handleCloseTask = useCallback(() => {
    setTask({ show: false });
  }, []);

  const handleSaveTask = useCallback(model => {
    setTask({ show: false });
    notification.success({
      title: t('common.alert.changed'),
      onClose: () => {
        doFetch(`${url}?${query}&bust=${Date.now()}`);
      }
    });
  }, []);

  const handleRemoveTask = useCallback(() => {
    setTask({ show: false });
    notification.success({
      title: t('common.alert.deleted'),
      onClose: () => {
        doFetch(`${url}?${query}&bust=${Date.now()}`);
      }
    });
  }, []);

  return (
    <Fragment>
      <MyInfoHeader
        onClick={handleGoMyTask}
        icon="tasks"
        title={t('project.my.tasks')}
        isMore={todo.data.paging.total > 3}
      />
      <RoleAware allowedRole={role.Task_Read}>
        <TaskTodoList
          projectId={projectId}
          tasks={todo.data.items}
          onItemClick={handleClickTodo}
        />
      </RoleAware>
      {todo.loading && <Loader active inline="centered" />}

      <TaskEditLayer
        open={task.show}
        onClose={handleCloseTask}
        title={t('task.title.edit')}
        projectId={projectId}
        taskItemId={task.taskItemId}
        onSaved={handleSaveTask}
        onRemoved={handleRemoveTask}
      />
    </Fragment>
  );
});

const Sites = memo(({ projectId }) => {
  const history = useHistory();
  const t = useFormatMessage();
  const url = `api/projects/${projectId}/sites`;
  const query = helpers.qs.stringify({
    name: '',
    offset: 0,
    limit: 3
  });

  const [site] = useFetch(`${url}?${query}`, new ListModel());

  const handleGoProjectSiteList = useCallback(() => {
    history.push(`/project/${projectId}/site`);
  }, []);

  const handleGoProjectSiteDetail = useCallback(siteId => {
    history.push(`/project/${projectId}/site/edit/${siteId}`);
  }, []);

  return (
    <Fragment>
      <MyInfoHeader
        onClick={handleGoProjectSiteList}
        icon="building"
        title={t('project.member.site')}
        isMore={site.data.paging.total > 3}
      />
      <RoleAware allowedRole={role.ProjectSite_Read}>
        <ProjectSiteList
          items={site.data.items}
          onClick={handleGoProjectSiteDetail}
        />
      </RoleAware>
      {site.loading && <Loader active inline="centered" />}
    </Fragment>
  );
});

const Members = memo(({ projectId }) => {
  const history = useHistory();
  const t = useFormatMessage();
  const url = `api/projects/${projectId}/members`;
  let query = helpers.qs.stringify({
    offset: 0,
    limit: 3
  });
  const [member] = useProjectMembers(`${url}?${query}`, new ListModel());

  const handleGoMemberClick = useCallback(() => {
    history.push(`/project/${projectId}/member`);
  }, []);

  return (
    <Fragment>
      <MyInfoHeader
        onClick={handleGoMemberClick}
        icon="user"
        title={t('project.member')}
        isMore={member.data.paging.total > 3}
      />
      <RoleAware allowedRole={role.ProjectMember_Read}>
        <ProjectMemberList projectId={projectId} items={member.data.items} />
      </RoleAware>
      {member.loading && <Loader active inline="centered" />}
    </Fragment>
  );
});

const ProjectDashBoard = () => {
  const match = useRouteMatch();
  const { projectId } = match.params;
  return (
    <Fragment>
      <Grid stackable>
        <Grid.Row>
          <Grid.Column widescreen={13} largeScreen={12} mobile={16}>
            <Fragment>
              <MainHeader projectId={projectId} />
              <ProjectCompContainer stackable>
                <Grid.Row>
                  <Grid.Column width="sixteen">
                    <Card
                      fluid
                      raised
                      style={{
                        padding: '2em 2em 2em 2em'
                      }}
                    >
                      <Status projectId={projectId} />
                    </Card>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width="eight">
                    <Card
                      fluid
                      raised
                      style={{ height: '300px', padding: '2em 2em 2em 2em' }}
                    >
                      <ProgressStatus projectId={projectId} />
                    </Card>
                  </Grid.Column>
                  <Grid.Column width="eight">
                    <Card
                      fluid
                      raised
                      style={{ height: '300px', padding: '2em 2em 2em 2em' }}
                    >
                      <TaskStatus projectId={projectId} />
                    </Card>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width="five">
                    <Card
                      fluid
                      raised
                      style={{ height: '280px', padding: '2em 2em 2em 2em' }}
                    >
                      <Todos projectId={projectId} />
                    </Card>
                  </Grid.Column>
                  <Grid.Column width="six">
                    <Card
                      fluid
                      raised
                      style={{ height: '280px', padding: '2em 2em 2em 2em' }}
                    >
                      <Sites projectId={projectId} />
                    </Card>
                  </Grid.Column>
                  <Grid.Column width="five">
                    <Card
                      fluid
                      raised
                      style={{ height: '280px', padding: '2em 2em 2em 2em' }}
                    >
                      <Members projectId={projectId} />
                    </Card>
                  </Grid.Column>
                </Grid.Row>
              </ProjectCompContainer>
            </Fragment>
          </Grid.Column>
          <Grid.Column widescreen={3} largeScreen={3}>
            <Responsive minWidth={Responsive.onlyLargeScreen.minWidth}>
              <RoleAware allowedRole={role.Task_Read}>
                <TaskNotifyContainer />
              </RoleAware>
            </Responsive>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Fragment>
  );
};

export default ProjectDashBoard;
