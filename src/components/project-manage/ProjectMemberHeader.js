import React from 'react';
import { Header } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';

const ProjectMemberHeader = () => {
  const t = useFormatMessage();
  return (
    <Header as="h3" dividing>
      {t('project.member.title')}
      <Header.Subheader>
        {t('project.member.title.description')}
      </Header.Subheader>
    </Header>
  );
};

export default ProjectMemberHeader;
