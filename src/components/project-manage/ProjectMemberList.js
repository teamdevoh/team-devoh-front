import React, { useCallback, useState, memo, Fragment } from 'react';
import PropTypes from 'prop-types';
import helpers from '../../helpers';
import { List, Icon } from 'semantic-ui-react';
import LazyImage from '../modules/LazyImage';
import { useFormatMessage, useFace } from '../../hooks';
import styled from 'styled-components';
import map from 'lodash/map';
import { EmptyRowsView } from '../modules';
import { format } from '../../constants';

const StyledLazyImage = styled(LazyImage)`
  display: inline-block;
`;
const StyledListContent = styled(List.Content)`
  display: inline-block;
  vertical-align: middle;
`;

const StyledSiteLink = styled.div`
  margin-bottom: 0.2em;
`;

const ProjectSite = ({ projectId, id, name }) => {
  const handleClick = useCallback(() => {
    helpers.history.push(`/project/${projectId}/site/edit/${id}`);
  }, []);

  return (
    <StyledSiteLink>
      <a onClick={handleClick}>{name}</a>
    </StyledSiteLink>
  );
};

const Face = memo(({ storageId }) => {
  const [faceUrl] = useFace(storageId);
  return <StyledLazyImage src={faceUrl} avatar={true} />;
});

const ProjectMemberListItem = memo(
  ({
    projectId,
    storageId,
    memberName,
    teamName,
    rate,
    scheduleFromDate,
    scheduleToDate,
    projectSites,
    onItemClick,
    memberId
  }) => {
    const t = useFormatMessage();
    const [more, setMore] = useState(false);
    const handleItemClick = useCallback(() => {
      onItemClick(memberId);
    }, []);

    const handleMoreClick = useCallback(
      () => {
        setMore(!more);
      },
      [more]
    );
    return (
      <List.Item>
        <Face storageId={storageId} />
        <StyledListContent>
          <List.Header>
            <a onClick={handleItemClick}>{memberName}</a>
            {`(${teamName})`}
          </List.Header>
          {rate && <div>{t('rate', { rate: rate ? rate : 0 })}</div>}
          {scheduleFromDate && (
            <div>
              {t('project.member.schedule.is', {
                date: helpers.util.dateRange(
                  scheduleFromDate,
                  scheduleToDate,
                  format.YYYY_MM_DD
                )
              })}
            </div>
          )}
          {projectSites.length > 0 && (
            <div onClick={handleMoreClick}>
              <a>
                {more ? 'Show Less' : `Show Sites More ${projectSites.length}`}
              </a>
              <Icon name={more ? 'angle up' : 'angle down'} />
            </div>
          )}
          {more &&
            map(projectSites, projectSite => {
              return (
                <ProjectSite
                  key={projectSite.key}
                  id={projectSite.key}
                  projectId={projectId}
                  name={projectSite.value}
                />
              );
            })}
        </StyledListContent>
      </List.Item>
    );
  }
);

const propTypes = {
  projectId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  items: PropTypes.array
};

const defaultProps = {};

const ProjectMemberList = ({ projectId, items, loading }) => {
  const handleItemClick = useCallback(memberId => {
    helpers.history.push(`/project/${projectId}/member/edit/${memberId}`);
  }, []);

  return (
    <Fragment>
      {items.length === 0 &&
        loading === false && (
          <EmptyRowsView loading={loading} style={{ position: 'relative' }} />
        )}
      <List celled>
        {map(items, item => {
          return (
            <ProjectMemberListItem
              projectId={projectId}
              onItemClick={handleItemClick}
              key={item.projectMemberId}
              memberId={item.memberId}
              storageId={item.myFaceFileStorageId}
              memberName={item.memberName}
              teamName={item.teamName}
              rate={item.rate}
              scheduleFromDate={item.scheduleFromDate}
              scheduleToDate={item.scheduleToDate}
              projectSites={item.projectSites}
            />
          );
        })}
      </List>
    </Fragment>
  );
};

ProjectMemberList.propTypes = propTypes;
ProjectMemberList.defaultProps = defaultProps;

export default ProjectMemberList;
