import React, { Fragment } from 'react';
import { BackButton, SaveButton, RemoveButton } from '../button';
import { Confirm, notification } from '../modal';
import { ProjectMemberHeader, ProjectMemberForm } from './';
import useProjectMemeberEdit from './hooks/useProjectMemeberEdit';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { useFormatMessage, useBoolean } from '../../hooks';
import { role } from '../../constants';

const ProjectMemberEdit = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const t = useFormatMessage();
  const isRemove = useBoolean(false);
  const { projectId, memberId } = match.params;
  const [member] = useProjectMemeberEdit(projectId, memberId);

  const handleSubmit = async () => {
    const is = await member.onEdit();
    if (is) {
      notification.success({
        title: t('common.alert.changed'),
        onClose: () => {
          history.goBack();
        }
      });
    }
  };

  const handleRemove = async () => {
    const is = await member.onRemove();
    if (is) {
      notification.success({
        title: t('common.alert.deleted'),
        onClose: () => {
          history.goBack();
        }
      });
    }
  };

  return (
    <Fragment>
      <ProjectMemberHeader />
      <ProjectMemberForm
        projectId={projectId}
        model={member.model}
        onChange={member.onChange}
        onSubmit={handleSubmit}
      >
        <BackButton name={t('common.back')} />
        <SaveButton
          allowedRole={role.ProjectMember_Edit}
          name={t('common.save')}
          loading={member.loading}
        />
        <RemoveButton
          allowedRole={role.ProjectMember_Edit}
          name={t('common.remove')}
          onClick={isRemove.setTrue}
          disabled={!member.model.projectMemberId}
        />
        <Confirm
          title={t('project.member.delete.title')}
          content={t('project.member.delete', {
            memberName: member.model.memberName
          })}
          open={isRemove.value}
          onOK={handleRemove}
          loading={member.loading}
          onCancel={isRemove.setFalse}
        />
      </ProjectMemberForm>
    </Fragment>
  );
};

export default ProjectMemberEdit;
