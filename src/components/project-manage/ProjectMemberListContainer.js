import React, { useCallback, useEffect } from 'react';
import history from '../../helpers/history';
import { AddLinkButton } from '../button';
import { ProjectMemberHeader, ProjectMemberList } from './';
import { Grid, Icon, Loader, Form, Divider, Select } from 'semantic-ui-react';
import { RoleAware } from '../auth';
import {
  useProjectMembers,
  useFormatMessage,
  ListModel,
  useFormFields
} from '../../hooks';
import Anchor from '../button/Anchor';
import { ProjectSite } from '../sample-status/Selector';
import { StyledForm } from '../sample-status/styles';
import helpers from '../../helpers';
import { useProjectTeamList } from './hooks';
import { role } from '../../constants';

const Teams = ({ value, onChange, loading, name }) => {
  const t = useFormatMessage();
  const [{ items, isLoading }] = useProjectTeamList({ onlyUseProject: true });

  let options = [
    {
      text: t('all'),
      value: 0
    }
  ];

  return (
    <Select
      item
      value={value}
      onChange={onChange}
      options={options.concat(items)}
      loading={loading || isLoading}
      name={name}
    />
  );
};

const ProjectMemberListContainer = ({ match }) => {
  const projectId = match.params.projectId;
  const t = useFormatMessage();
  const [member, setUrl] = useProjectMembers(``, new ListModel());
  const [{ model, onChange }] = useFormFields({
    projectSiteId: 0,
    teamId: 0
  });

  const handleGoHome = useCallback(() => {
    history.push(`/project/${projectId}`);
  }, []);

  useEffect(
    () => {
      const { projectSiteId, teamId } = model;
      const params = helpers.qs.stringify({
        projectSiteId,
        teamId
      });
      const url = `api/projects/${projectId}/members?${params}`;
      setUrl(url);
    },
    [model]
  );

  return (
    <div>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <ProjectMemberHeader />
            <AddLinkButton
              name={t('project.member.add')}
              to={`/project/${projectId}/member/new`}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Icon name="arrow left" />
            <Anchor text={t('project.start')} onClick={handleGoHome} />
            <Divider />
            <div style={{ display: 'flex' }}>
              <RoleAware allowedRole={role.ProjectSite_Read}>
                <StyledForm style={{ marginRight: '15px' }}>
                  <Form.Field inline>
                    <label>{t('project.site')}</label>
                    <ProjectSite
                      projectId={projectId}
                      name="projectSiteId"
                      value={model.projectSiteId}
                      onChange={onChange}
                      loading={member.loading}
                    />
                  </Form.Field>
                </StyledForm>
              </RoleAware>
              <RoleAware allowedRole={role.Team_Read}>
                <StyledForm>
                  <Form.Field inline>
                    <label>{t('project.member.team')}</label>
                    <Teams
                      name="teamId"
                      placeholder={t('project.member.team')}
                      value={model.teamId}
                      onChange={onChange}
                      loading={member.loading}
                    />
                  </Form.Field>
                </StyledForm>
              </RoleAware>
            </div>
            <RoleAware allowedRole={role.ProjectMember_Read}>
              <ProjectMemberList
                projectId={projectId}
                items={member.data.items}
                loading={member.loading}
              />
            </RoleAware>
            {member.loading && <Loader active inline="centered" />}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default ProjectMemberListContainer;
