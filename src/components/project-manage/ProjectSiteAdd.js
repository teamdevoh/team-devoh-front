import React, { Fragment, useMemo } from 'react';
import { ProjectSiteHeader, ProjectSiteForm } from './';
import { CancelButton, SaveButton } from '../button';
import api from './api';
import { notification } from '../modal';
import { Form, Grid, Dropdown, Responsive } from 'semantic-ui-react';
import useProjectSiteAdd from './hooks/useProjectSiteAdd';
import { useEffect } from 'react';
import { useState } from 'react';
import { useFormatMessage } from '../../hooks';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { role } from '../../constants';
import styled from 'styled-components';
import SiteListContainer from './SiteListContainer';
import { useSiteList } from './hooks';

const Wrap = styled.div`
  display: flex;
`;

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const SelectSite = ({ projectId, name, value, onChange }) => {
  const t = useFormatMessage();
  const [siteIdOfProjectSite, setSiteIdOfProjectSite] = useState(new Set([]));
  const [loading, setLoading] = useState(false);
  const sites = useSiteList({ search: '' });
  const options = useMemo(
    () => {
      const newOptions = [];
      const addableSites = sites.items.filter(
        site => !siteIdOfProjectSite.has(site.siteId)
      );

      for (let i = 0; i < addableSites.length; i++) {
        const { name, siteId } = addableSites[i];
        newOptions.push({ text: name, value: siteId });
      }

      return newOptions;
    },
    [sites.items, siteIdOfProjectSite]
  );

  const fetchProjectSites = async () => {
    try {
      const res = await api.fetchProjectSites({
        projectId
      });

      if (res && res.data) {
        const siteIds = new Set([]);
        res.data.items.forEach(item => siteIds.add(item.siteId));

        setSiteIdOfProjectSite(siteIds);
      }
    } catch (error) {}
    setLoading(false);
  };

  useEffect(
    () => {
      fetchProjectSites();
    },
    [projectId]
  );

  const handleRefreshSites = () => {
    sites.fetchItems({ search: '' });
  };

  return (
    <Fragment>
      <Wrap>
        <Dropdown
          name={name}
          options={options}
          placeholder={t('project.site')}
          value={value}
          onChange={onChange}
          search
          fluid
          selection
        />
        <SiteListContainer
          items={sites.items}
          loading={loading}
          itemRefresh={handleRefreshSites}
        />
      </Wrap>
    </Fragment>
  );
};

const ProjectSiteAdd = () => {
  const t = useFormatMessage();
  const match = useRouteMatch();
  const { projectId } = match.params;
  const history = useHistory();
  const [projectSite] = useProjectSiteAdd(projectId);

  const handleSubmit = async () => {
    try {
      const res = await projectSite.onAdd();
      if (res.data) {
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            history.push(`/project/${projectId}/site`);
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Fragment>
      <ProjectSiteHeader />
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Form>
              <Form.Field required width={isMobile ? 16 : 8}>
                <label>{t('project.site')}</label>
                <SelectSite
                  required
                  projectId={projectId}
                  name="siteId"
                  value={projectSite.model.siteId}
                  onChange={projectSite.onChange}
                />
              </Form.Field>
            </Form>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <ProjectSiteForm
              model={projectSite.model}
              onChange={projectSite.onChange}
              onSubmit={handleSubmit}
            >
              <CancelButton
                to={`/project/${projectId}/site`}
                name={t('common.cancel')}
              />
              <SaveButton
                allowedRole={role.ProjectSite_Edit}
                name={t('common.save')}
                disabled={projectSite.model.siteId ? false : true}
                loading={projectSite.loading}
              />
            </ProjectSiteForm>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Fragment>
  );
};

export default ProjectSiteAdd;
