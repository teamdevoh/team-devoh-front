import React, { useState, useCallback } from 'react';
import ProjectContainer from './ProjectContainer';
import ProjectForm from './ProjectForm';
import amApi from '../account-manage/api';
import { notification } from '../modal';
import { CancelButton, SaveButton } from '../button';
import { TeamSelect } from './';
import { Grid, Message } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import useProjectAdd from './hooks/useProjectAdd';
import { role } from '../../constants';
import { useProjectItemListUp } from './hooks';
import { useProjectStatus } from '../app/hooks';
import { useQuery } from '../../hooks/useQuery';

const ProjectAdd = ({ history }) => {
  const t = useFormatMessage();
  const query = useQuery();

  const projectGroupId = query.get('projectGroupId');
  const [{ model, loading, onChange, onAdd }] = useProjectAdd({
    projectGroupId: projectGroupId ? Number(projectGroupId) : ''
  });
  const [teamName, setTeamName] = useState('');
  const projectItemListUp = useProjectItemListUp();
  const projectStatus = useProjectStatus();

  const handleTeamChange = useCallback(
    (event, { name, value }) => {
      setTeamName(event.target.innerText);
      onChange(event, { name, value });
    },
    [model]
  );

  const handleSubmit = useCallback(
    async () => {
      const newId = await onAdd();
      await amApi.refreshToken(newId);

      notification.success({
        title: t('common.alert.added'),
        onClose: async () => {
          await projectItemListUp.fetch();
          const isRefreshed = await projectStatus.active({
            projectId: newId,
            localTitle: model.localTitle,
            title: model.title,
            projectTeam: teamName,
            periodFrom: model.periodFrom,
            periodTo: model.periodTo,
            protocolNo: model.protocolNo,
            variable: ''
          });

          if (isRefreshed) {
            history.push(`/project/${newId}/site`);
          }
        }
      });
    },
    [model]
  );

  return (
    <ProjectContainer>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            {model.teamId === '' && (
              <Message>
                <Message.Header>{t('project.add.myrole.title')}</Message.Header>
                <p>{t('project.add.myrole.description')}</p>
              </Message>
            )}
            <TeamSelect
              name="teamId"
              placeholder={t('project.add.myrole.description')}
              value={model.teamId}
              onChange={handleTeamChange}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {model.teamId && (
              <ProjectForm
                model={model}
                onChange={onChange}
                onSubmit={handleSubmit}
              >
                <CancelButton to="/" name={t('common.cancel')} />
                <SaveButton
                  allowedRole={role.Project_Edit}
                  name={t('common.save')}
                  loading={loading}
                />
              </ProjectForm>
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </ProjectContainer>
  );
};

export default ProjectAdd;
