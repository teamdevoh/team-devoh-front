import React from 'react';
import { Placeholder } from 'semantic-ui-react';
import { asyncComponent } from '../modules';

const ProjectForm = asyncComponent(() =>
  import('./ProjectForm')
);
const ProjectUserCard = asyncComponent(() =>
  import('./ProjectUserCard')
);
const ProjectStatus = asyncComponent(() =>
  import('./ProjectStatus'));
const ProjectMemberList = asyncComponent(() =>
  import('./ProjectMemberList')
);
const ProjectTaskSectionStatus = asyncComponent(() =>
  import('./ProjectTaskSectionStatus'));
const ProjectSiteHeader = asyncComponent(() =>
  import('./ProjectSiteHeader')
);
const ProjectTaskAssignedStatus = asyncComponent(() =>
  import('./ProjectTaskAssignedStatus')
);
const ProjectSiteForm = asyncComponent(() =>
  import('./ProjectSiteForm')
);
const ProjectMemberHeader = asyncComponent(() =>
  import('./ProjectMemberHeader')
);
const ProjectMemberForm = asyncComponent(() =>
  import('./ProjectMemberForm')
);
const ProjectGroupSelect = asyncComponent(() =>
  import('./ProjectGroupSelect')
);
const TeamSelect = asyncComponent(() =>
  import('./TeamSelect')
);
const ProjectSiteCheckbox = asyncComponent(() =>
  import('./ProjectSiteCheckbox')
);
const SuggestInput = asyncComponent(() =>
  import('./SuggestInput')
);
const ProjectSearchDashBoard = asyncComponent(() =>
  import('./ProjectSearchDashBoard')
);

export {
  ProjectForm,
  ProjectUserCard,
  ProjectStatus,
  ProjectMemberList,
  ProjectTaskSectionStatus,
  ProjectTaskAssignedStatus,
  ProjectSiteHeader,
  ProjectSiteForm,
  ProjectMemberHeader,
  ProjectMemberForm,
  ProjectGroupSelect,
  TeamSelect,
  ProjectSiteCheckbox,
  SuggestInput,
  ProjectSearchDashBoard
};

export { default as Api } from './api';
export { default as ProjectHeader } from './ProjectHeader';
export { default as ProjectContainer } from './ProjectContainer';
export { default as ProjectSiteList } from './ProjectSiteList';
