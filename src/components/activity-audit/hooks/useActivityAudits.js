import { useEffect, useState } from 'react';
import api from '../api';

const useActivityAudits = ({ activityKeyId, entityName }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const limit = 50;
  const [total, setTotal] = useState(0);
  const [start, setStart] = useState(0);

  const fetchItems = async ({ offset }) => {
    setLoading(true);
    const res = await api.fetchActivityAudits({
      activityKeyId,
      entityName,
      limit,
      offset
    });
    if (res && res.data) {
      const merged = items.concat(res.data.items);

      setLoading(false);
      setItems(merged);
      setTotal(res.data.paging.total);
      setStart(offset + limit);
    }
  };

  useEffect(
    () => {
      if (activityKeyId > 0) {
        fetchItems({ offset: 0 });
      }
    },
    [activityKeyId]
  );

  return [{ items, loading, start, total, refresh: fetchItems }];
};

export default useActivityAudits;
