import React, { useEffect, Fragment } from 'react';
import { useInView } from 'react-intersection-observer';
import { Loader } from 'semantic-ui-react';
import { useActivityAudits } from './hooks';
import ActivityAuditList from './ActivityAuditList';

const ActivityAuditContainer = ({
  activityKeyId,
  entityName,
  prefixLocale,
  skip
}) => {
  const [ref, inView, entry] = useInView({
    root: null,
    rootMargin: '200px',
    threshold: 0
  });
  const [{ items, loading, total, start, refresh }] = useActivityAudits({
    activityKeyId,
    entityName
  });

  useEffect(
    () => {
      if (inView && items.length < total && entry.isIntersecting) {
        refresh({ offset: start });
      }
    },
    [inView]
  );

  return (
    <Fragment>
      <ActivityAuditList
        items={items}
        prefixLocale={prefixLocale}
        skip={skip}
      />
      <div ref={ref}>
        <Loader active={loading} inline="centered" size="mini" />
      </div>
    </Fragment>
  );
};

export default ActivityAuditContainer;
