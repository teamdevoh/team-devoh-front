import React, { Fragment } from 'react';
import { Grid } from 'semantic-ui-react';
import map from 'lodash/map';
import styled from 'styled-components';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import remove from 'lodash/remove';
import orderBy from 'lodash/orderBy';

const AuditMetaColumn = styled(Grid.Column)`
  &&&& {
    display: flex;
    align-items: center;
  }
`;

const PropertyMetaColumn = styled(Grid.Column)`
  &&&& {
    padding: 0 !important;
  }
`;

const PropertyItem = ({ prefixLocale, item }) => {
  const t = useFormatMessage();

  const isReason = item.name === 'reason';

  return (
    <Grid.Row>
      <PropertyMetaColumn width={4} style={{ color: isReason ? 'red' : '' }}>
        {isReason ? t('reason') : t(`${prefixLocale}.${item.name}`)}
      </PropertyMetaColumn>
      {isReason && (
        <PropertyMetaColumn width={12}>{item.newValue}</PropertyMetaColumn>
      )}
      {!isReason && (
        <Fragment>
          <PropertyMetaColumn width={6}>{item.oldValue}</PropertyMetaColumn>
          <PropertyMetaColumn width={6}>{item.newValue}</PropertyMetaColumn>
        </Fragment>
      )}
    </Grid.Row>
  );
};

const ActivityAuditList = ({ items, prefixLocale, skip }) => {
  let skipProperties = [
    'creator',
    'created',
    'modified',
    'modifier',
    'isRemove',
    'activityId',
    'subjectId',
    'conductionActivityId',
    'subjectVisitId'
  ].concat(skip);

  let isEven = true;

  return (
    <Grid celled="internally" style={{ overflowX: 'auto', minWidth: '1200px' }}>
      <Grid.Row style={{ backgroundColor: '#f9fafb' }}>
        <Grid.Column width={2}>Time</Grid.Column>
        <Grid.Column width={1}>State</Grid.Column>
        <Grid.Column width={12}>Item</Grid.Column>
        <Grid.Column width={1}>Author</Grid.Column>
      </Grid.Row>
      {map(items, (item, index) => {
        let skips = skipProperties;
        if (item.state === 0) {
          skips = skips.concat('reason');
        }

        remove(item.properties, property => {
          if (skips.indexOf(property.name) > -1) {
            return true;
          } else {
            property.order = property.name === 'reason' ? 999 : 0;
            return false;
          }
        });

        if (item.properties.length > 0) {
          isEven = !isEven;
          return (
            <Grid.Row key={item.auditEntryID}>
              <AuditMetaColumn
                width={2}
                style={{ backgroundColor: isEven ? '#FAF9F9' : '' }}
              >
                {helpers.util.dateformat(item.createdDate)}
              </AuditMetaColumn>
              <AuditMetaColumn
                width={1}
                style={{ backgroundColor: isEven ? '#FAF9F9' : '' }}
              >
                {item.state === 0 ? 'Added' : 'Modified'}
              </AuditMetaColumn>
              <Grid.Column
                width={12}
                style={{ backgroundColor: isEven ? '#FAF9F9' : '' }}
              >
                <Grid celled="internally">
                  {orderBy(item.properties, ['order'], 'asc').map(
                    (property, index) => {
                      return (
                        <PropertyItem
                          key={property.auditEntityPropertyId}
                          skip={skip}
                          prefixLocale={prefixLocale}
                          item={property}
                        />
                      );
                    }
                  )}
                </Grid>
              </Grid.Column>
              <AuditMetaColumn
                width={1}
                style={{ backgroundColor: isEven ? '#FAF9F9' : '' }}
              >
                {item.author}
              </AuditMetaColumn>
            </Grid.Row>
          );
        } else {
          return <Fragment />;
        }
      })}
    </Grid>
  );
};

export default ActivityAuditList;
