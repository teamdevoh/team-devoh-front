import helpers from '../../helpers';

const fetchActivityAudits = ({
  activityKeyId,
  entityName,
  search,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    title: search === void 0 ? '' : search,
    entityName,
    offset,
    limit
  });

  return helpers.Service.get(
    `api/activities/${activityKeyId}/audittrails?${params}`
  );
};

export default {
  fetchActivityAudits
};
