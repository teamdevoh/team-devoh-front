import React, { useState } from 'react';
import { Label } from 'semantic-ui-react';
import api from './api';
import helpers from '../../helpers';
import { StyledCSVExport } from './styles';
import { FlexDiv, StyledDetail } from '../sample-barcode/styles';
import { role } from '../../constants';
import { RoleAware } from '../auth';

const DashBoardTool = ({
  total,
  projectId,
  projectSiteId,
  subjectId,
  tbdrugId
}) => {
  const [loading, setLoading] = useState(false);

  const columns = [
    {
      key: 'site',
      name: 'Site'
    },
    {
      key: 'subject',
      name: 'Subject'
    },
    { key: 'initial', name: 'Initial' },
    {
      key: 'visit',
      name: 'Visit'
    },
    {
      key: 'date',
      name: 'Date'
    },
    {
      key: 'time',
      name: 'Time'
    },
    {
      key: 'doc_Name',
      name: 'DOC_NAME'
    },
    {
      key: 'sex',
      name: 'SEX'
    },
    {
      key: 'age',
      name: 'AGE'
    },
    {
      key: 'smoke',
      name: 'SMOKE'
    },
    {
      key: 'weight',
      name: 'WEIGHT'
    },
    {
      key: 'height',
      name: 'HEIGHT'
    },
    {
      key: 'tb_Diagnosis',
      name: 'TB_DIAGNOSIS'
    },
    {
      key: 'tb_Start',
      name: 'TB_START'
    },
    {
      key: 'tb_Treatment',
      name: 'TB_TREATMENT'
    },
    {
      key: 'tb_Suscept',
      name: 'TB_SUSCEPT'
    },
    {
      key: 'casecon',
      name: 'CASECON'
    },
    {
      key: 'dig_Fin',
      name: 'DIG_FIN'
    },
    {
      key: 'nat2_Gen',
      name: 'NAT2_GEN'
    },
    {
      key: 'nat2_Phe',
      name: 'NAT2_PHE'
    },
    {
      key: 'slco1b1_Gen',
      name: 'SLCO1B1_GEN'
    },
    {
      key: 'slco1b1_Phe',
      name: 'SLCO1B1_PHE'
    },
    {
      key: 'excombi',
      name: 'EXCOMBI'
    },
    {
      key: 'exdrug',
      name: 'EXDRUG'
    },
    {
      key: 'exdosfrq',
      name: 'EXDOSFRQ'
    },
    {
      key: 'exstdtc',
      name: 'EXSTDTC'
    },
    {
      key: 'exendtc',
      name: 'EXENDTC'
    },
    {
      key: 'exdose',
      name: 'EXDOSE'
    },
    {
      key: 'exrouthinf',
      name: 'EXROUTHINF'
    },
    {
      key: 'fast_Fed',
      name: 'FAST_FED'
    },
    {
      key: 'endmeal_Datetime',
      name: 'ENDMEAL_DATETIME'
    },
    {
      key: 'resultdtc',
      name: 'RESULTDTC'
    },
    {
      key: 'tad',
      name: 'TAD(hr)'
    },
    {
      key: 'drug_Conc',
      name: 'DRUG_CONC'
    },
    {
      key: 'auc',
      name: 'AUC'
    },
    {
      key: 'cmax',
      name: 'Cmax'
    },
    {
      key: 'mic',
      name: 'MIC'
    },
    {
      key: 'typemic',
      name: 'TYPE MIC'
    },
    {
      key: 'pk_Com',
      name: 'PK Comment'
    },
    {
      key: 'pk_Quest',
      name: 'PK_QUEST'
    },
    {
      key: 'pk_Reaon',
      name: 'PK_REASON'
    },
    {
      key: 'alb',
      name: 'ALB'
    },
    {
      key: 'bun',
      name: 'BUN'
    },
    {
      key: 'cr',
      name: 'CR'
    },
    {
      key: 'egfr',
      name: 'EGFR'
    },
    {
      key: 'tbil',
      name: 'TBIL'
    },
    {
      key: 'tpro',
      name: 'TPRO'
    },
    {
      key: 'ast',
      name: 'AST'
    },
    {
      key: 'alt',
      name: 'ALT'
    },
    {
      key: 'alp',
      name: 'ALP'
    },
    {
      key: 'ua',
      name: 'UA'
    },
    {
      key: 'wbc',
      name: 'WBC'
    },
    {
      key: 'rbc',
      name: 'RBC'
    },
    {
      key: 'hgb',
      name: 'HGB'
    },
    {
      key: 'hct',
      name: 'HCT'
    },
    {
      key: 'plt',
      name: 'PLT'
    },
    {
      key: 'neu',
      name: 'NEU'
    },
    {
      key: 'lym',
      name: 'LYM'
    },
    {
      key: 'mon',
      name: 'MON'
    },
    {
      key: 'eos',
      name: 'EOS'
    },
    {
      key: 'bas',
      name: 'BAS'
    },
    {
      key: 'anc',
      name: 'ANC'
    },
    {
      key: 'comorbid',
      name: 'COMORBID'
    },
    {
      key: 'cmtrt',
      name: 'CMTRT'
    },
    {
      key: 'cmdecod',
      name: 'CMDECOD'
    },
    {
      key: 'codrname',
      name: 'CODRNAME'
    },
    {
      key: 'cmdose',
      name: 'CMDOSE'
    },
    {
      key: 'cmdosu',
      name: 'CMDOSU'
    },
    {
      key: 'cmdosfrq',
      name: 'CMDOSFRQ'
    },
    {
      key: 'cmroute',
      name: 'CMROUTE'
    },
    {
      key: 'cmrouthinf',
      name: 'CMROUTHINF'
    },
    {
      key: 'cmdosfrm',
      name: 'CMDOSFRM'
    },
    {
      key: 'cmstdtc',
      name: 'CMSTDTC'
    },
    {
      key: 'cmendtc',
      name: 'CMENDTC'
    },
    {
      key: 'adr_Name',
      name: 'ADR_NAME'
    },
    {
      key: 'aedecod',
      name: 'AEDECOD'
    },
    {
      key: 'aeptcd',
      name: 'AEPTCD'
    },
    {
      key: 'aesoc',
      name: 'AESOC'
    },
    {
      key: 'aesoccd',
      name: 'AESOCCD'
    },
    {
      key: 'adr_Onset_Dat',
      name: 'ADR_ONSET_DAT'
    },
    {
      key: 'adr_Offset_Dat',
      name: 'ADR_OFFSET_DAT'
    },
    {
      key: 'adr_Severity',
      name: 'ADR_SEVERITY'
    },
    {
      key: 'adrdrug',
      name: 'ADRDRUG'
    },
    {
      key: 'adrcon',
      name: 'ADRCON'
    }
  ];

  const handleCSVExportClick = async () => {
    // 전체 데이터
    setLoading(true);
    try {
      const res = await api.fetchExportTdmList({
        projectId,
        projectSiteId,
        subjectId,
        tbdrugId
      });

      const fieldNames = columns.map(item => item.name);
      const fields = columns.map(item => {
        return {
          ...item,
          value: (dataElement, field, data) => {
            if (item.key === 'mic' && dataElement[item.key]) {
              dataElement[item.key] = String(dataElement[item.key])
                .replaceAll('&gt;', '>')
                .replaceAll('&lt;', '<');
            }

            return dataElement[item.key]
              ? String(dataElement[item.key]).replaceAll(',', '|')
              : dataElement[item.key];
          }
        };
      });

      if (res && res.data) {
        helpers.util.exportCSV({
          fileName: 'TDM_List.csv',
          fields,
          fieldNames,
          data: res.data.items
        });
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <FlexDiv>
      <Label size="big">
        Results
        <StyledDetail>{total}</StyledDetail>
      </Label>
      <RoleAware allowedRole={role.TDM_Export_Excel_Read}>
        <StyledCSVExport
          positive
          loading={loading}
          onClick={handleCSVExportClick}
          content="CSV Export"
        />
      </RoleAware>
    </FlexDiv>
  );
};

export default DashBoardTool;
