import { asyncComponent } from '../modules';

const DashBoard = asyncComponent(() => import('./DashBoard'));

export { DashBoard };
