import { Button } from 'semantic-ui-react';
import styled from 'styled-components';

export const StyledCSVExport = styled(Button)`
  &&& {
    margin-left: auto;
    margin-right: 0;
  }
`;
