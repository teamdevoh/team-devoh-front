import React, { useState } from 'react';
import TdmBoard from './TdmBoard';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useTdmList } from './hooks';

const DashBoard = () => {
  const { projectId } = useParams();

  const [filter, setFilter] = useState({
    projectSiteId: 0,
    subjectId: 0,
    tbdrugId: 0
  });

  const handleFilterChange = (event, { name, value }) => {
    let newFilter = { ...filter, [name]: value };

    if (name === 'projectSiteId') {
      newFilter = {
        ...newFilter,
        subjectId: 0
      };
    }

    setFilter({ ...newFilter });
  };

  const [{ items, loading, start, total, refresh }] = useTdmList({
    projectId,
    ...filter
  });

  const handleRowVisibleEnd = () => {
    if (items.length < total) {
      refresh({ offset: start });
    }
  };

  return (
    <Segment basic>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onFilterChange={handleFilterChange}
            filter={filter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool
            total={total}
            loading={loading}
            projectId={projectId}
            {...filter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <TdmBoard
            items={items}
            loading={loading}
            onRowVisibleEnd={handleRowVisibleEnd}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
