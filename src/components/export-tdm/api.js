import helpers from '../../helpers';

const fetchExportTdmList = ({
  projectId,
  projectSiteId,
  subjectId,
  tbdrugId,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    tbdrugId,
    offset,
    limit
  });

  return helpers.Service.get(
    `api/projects/${projectId}/site/${projectSiteId}/subjects/${subjectId}/tdm?${params}`
  );
};

export default {
  fetchExportTdmList
};
