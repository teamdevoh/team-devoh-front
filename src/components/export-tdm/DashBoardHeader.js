import React, { Fragment } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { ProjectSite } from '../collection/Selector';
import { Subjects } from '../collection/Selector';
import { StyledForm } from '../sample-status/styles';
import { TbdrugList } from '../collection-tbdrug/Selector';

const DashBoardHeader = ({ projectId, filter, onFilterChange }) => {
  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <StyledForm>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Site</label>
                <ProjectSite
                  projectId={projectId}
                  name="projectSiteId"
                  value={filter.projectSiteId}
                  onChange={onFilterChange}
                  isSelect
                />
              </Form.Field>
              <Form.Field>
                <label>Subject</label>
                <Subjects
                  projectSiteId={filter.projectSiteId}
                  name="subjectId"
                  value={filter.subjectId}
                  onChange={onFilterChange}
                  reset={filter.projectSiteId === 0}
                  hasAll
                />
              </Form.Field>
              <Form.Field>
                <label>TB Drug</label>
                <TbdrugList
                  defaultOption={[
                    {
                      text: '-----------------------------------------',
                      value: 0
                    }
                  ]}
                  fluid
                  name="tbdrugId"
                  value={filter.tbdrugId}
                  onChange={onFilterChange}
                />
              </Form.Field>
            </Form.Group>
          </StyledForm>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
