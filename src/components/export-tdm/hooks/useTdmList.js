import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const limit = 50;

const useSampleCollectionList = ({
  projectId,
  projectSiteId,
  subjectId,
  tbdrugId
}) => {
  const [total, setTotal] = useState(0);
  const [start, setStart] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async ({ offset, orgItems = items }) => {
    setLoading(true);
    try {
      const res = await api.fetchExportTdmList({
        projectId,
        projectSiteId,
        subjectId,
        tbdrugId,
        offset,
        limit
      });

      if (res && res.data) {
        const newItems = orgItems.concat(res.data.items);
        setItems(newItems);
        setStart(offset + limit);
        setTotal(res.data.paging.total);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      fetchItems({ offset: 0, orgItems: [] });
    },
    [projectId, projectSiteId, subjectId, tbdrugId]
  );

  return [{ items, loading, start, total, refresh: fetchItems }];
};

export default useSampleCollectionList;
