import React from 'react';
import { DataGrid } from '../data-grid';

const TdmBoard = ({ items, loading, onRowVisibleEnd }) => {
  const columns = [
    {
      key: 'site',
      name: 'Site',
      width: 145
    },
    {
      key: 'subject',
      name: 'Subject',
      width: 90
    },
    { key: 'initial', name: 'Initial' },
    {
      key: 'visit',
      name: 'Visit',
      width: 120
    },
    {
      key: 'date',
      name: 'Date',
      width: 110
    },
    {
      key: 'time',
      name: 'Time'
    },
    {
      key: 'doc_Name',
      name: 'DOC_NAME',
      width: 90
    },
    {
      key: 'sex',
      name: 'SEX'
    },
    {
      key: 'age',
      name: 'AGE'
    },
    {
      key: 'smoke',
      name: 'SMOKE'
    },
    {
      key: 'weight',
      name: 'WEIGHT'
    },
    {
      key: 'height',
      name: 'HEIGHT'
    },
    {
      key: 'tb_Diagnosis',
      name: 'TB_DIAGNOSIS',
      width: 120
    },
    {
      key: 'tb_Start',
      name: 'TB_START',
      width: 110
    },
    {
      key: 'tb_Treatment',
      name: 'TB_TREATMENT',
      width: 130
    },
    {
      key: 'tb_Suscept',
      name: 'TB_SUSCEPT',
      width: 130
    },
    {
      key: 'casecon',
      name: 'CASECON'
    },
    {
      key: 'dig_Fin',
      name: 'DIG_FIN'
    },
    {
      key: 'nat2_Gen',
      name: 'NAT2_GEN'
    },
    {
      key: 'nat2_Phe',
      name: 'NAT2_PHE'
    },
    {
      key: 'slco1b1_Gen',
      name: 'SLCO1B1_GEN'
    },
    {
      key: 'slco1b1_Phe',
      name: 'SLCO1B1_PHE'
    },
    {
      key: 'excombi',
      name: 'EXCOMBI'
    },
    {
      key: 'exdrug',
      name: 'EXDRUG'
    },
    {
      key: 'exdosfrq',
      name: 'EXDOSFRQ'
    },
    {
      key: 'exstdtc',
      name: 'EXSTDTC'
    },
    {
      key: 'exendtc',
      name: 'EXENDTC'
    },
    {
      key: 'exdose',
      name: 'EXDOSE'
    },
    {
      key: 'exrouthinf',
      name: 'EXROUTHINF'
    },
    {
      key: 'fast_Fed',
      name: 'FAST_FED'
    },
    {
      key: 'endmeal_Datetime',
      name: 'ENDMEAL_DATETIME'
    },
    {
      key: 'resultdtc',
      name: 'RESULTDTC'
    },
    {
      key: 'tad',
      name: 'TAD(hr)'
    },
    {
      key: 'drug_Conc',
      name: 'DRUG_CONC'
    },
    {
      key: 'auc',
      name: 'AUC'
    },
    {
      key: 'cmax',
      name: 'Cmax'
    },
    {
      key: 'mic',
      name: 'MIC',
      formatter: ({ value }) => {
        return value
          ? String(value)
              .replaceAll('&gt;', '>')
              .replaceAll('&lt;', '<')
          : value;
      }
    },
    {
      key: 'typemic',
      name: 'TYPE MIC'
    },
    {
      key: 'pk_Com',
      name: 'PK Comment'
    },
    {
      key: 'pk_Quest',
      name: 'PK_QUEST'
    },
    {
      key: 'pk_Reaon',
      name: 'PK_REASON'
    },
    {
      key: 'alb',
      name: 'ALB'
    },
    {
      key: 'bun',
      name: 'BUN'
    },
    {
      key: 'cr',
      name: 'CR'
    },
    {
      key: 'egfr',
      name: 'EGFR'
    },
    {
      key: 'tbil',
      name: 'TBIL'
    },
    {
      key: 'tpro',
      name: 'TPRO'
    },
    {
      key: 'ast',
      name: 'AST'
    },
    {
      key: 'alt',
      name: 'ALT'
    },
    {
      key: 'alp',
      name: 'ALP'
    },
    {
      key: 'ua',
      name: 'UA'
    },
    {
      key: 'wbc',
      name: 'WBC'
    },
    {
      key: 'rbc',
      name: 'RBC'
    },
    {
      key: 'hgb',
      name: 'HGB'
    },
    {
      key: 'hct',
      name: 'HCT'
    },
    {
      key: 'plt',
      name: 'PLT'
    },
    {
      key: 'neu',
      name: 'NEU'
    },
    {
      key: 'lym',
      name: 'LYM'
    },
    {
      key: 'mon',
      name: 'MON'
    },
    {
      key: 'eos',
      name: 'EOS'
    },
    {
      key: 'bas',
      name: 'BAS'
    },
    {
      key: 'anc',
      name: 'ANC'
    },
    {
      key: 'comorbid',
      name: 'COMORBID',
      width: 90
    },
    {
      key: 'cmtrt',
      name: 'CMTRT',
      width: 170
    },
    {
      key: 'cmdecod',
      name: 'CMDECOD',
      width: 90
    },
    {
      key: 'codrname',
      name: 'CODRNAME',
      width: 170
    },
    {
      key: 'cmdose',
      name: 'CMDOSE'
    },
    {
      key: 'cmdosu',
      name: 'CMDOSU'
    },
    {
      key: 'cmdosfrq',
      name: 'CMDOSFRQ',
      width: 150
    },
    {
      key: 'cmroute',
      name: 'CMROUTE'
    },
    {
      key: 'cmrouthinf',
      name: 'CMROUTHINF'
    },
    {
      key: 'cmdosfrm',
      name: 'CMDOSFRM'
    },
    {
      key: 'cmstdtc',
      name: 'CMSTDTC',
      width: 110
    },
    {
      key: 'cmendtc',
      name: 'CMENDTC',
      width: 110
    },
    {
      key: 'adr_Name',
      name: 'ADR_NAME'
    },
    {
      key: 'aedecod',
      name: 'AEDECOD'
    },
    {
      key: 'aeptcd',
      name: 'AEPTCD'
    },
    {
      key: 'aesoc',
      name: 'AESOC'
    },
    {
      key: 'aesoccd',
      name: 'AESOCCD'
    },
    {
      key: 'adr_Onset_Dat',
      name: 'ADR_ONSET_DAT'
    },
    {
      key: 'adr_Offset_Dat',
      name: 'ADR_OFFSET_DAT'
    },
    {
      key: 'adr_Severity',
      name: 'ADR_SEVERITY'
    },
    {
      key: 'adrdrug',
      name: 'ADRDRUG'
    },
    {
      key: 'adrcon',
      name: 'ADRCON'
    }
  ];

  return (
    <DataGrid
      columns={columns}
      minHeight={680}
      rowNumber={{ show: true, key: 'no', name: 'No' }}
      rowKey="rowNum"
      rows={items}
      loading={loading}
      onRowVisibleEnd={onRowVisibleEnd}
    />
  );
};

export default TdmBoard;
