import React from 'react';
import PropTypes from 'prop-types';
import 'flatpickr/dist/themes/material_green.css';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/l10n/ko.js';
import helpers from '../../helpers';
import { Icon, Button } from 'semantic-ui-react';

const propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  /**
   * onChange gets triggered when the user selects a date, or changes the time on a selected date.
   * @param {Array} selectedDates React's original SyntheticEvent.
   * @param {string} dateStr  a string representation of the latest selected Date object by the user. The string is formatted as per the dateFormat option.
   * @param {object} instance the flatpickr object, containing various methods and properties.
   */
  onChange: PropTypes.func,
  /**
   * see https://flatpickr.js.org/examples/#datetime
   */
  options: PropTypes.object
};

const defaultProps = {
  value: ''
};

const DateTimePicker = ({ name, onChange, options, value }) => {
  const baseOptions = {
    mode: 'single',
    enableTime: true,
    locale: helpers.util.getLocale(),
    allowInput: true,
    ...options
  };

  const handleChange = (obj, date) => {
    onChange(null, { name, value: date });
  };

  const handleTodayClick = () => {
    onChange(null, { name, value: helpers.util.dateformat(new Date()) });
  };

  return (
    <div className="ui left icon action input">
      <Flatpickr
        name={name}
        value={value}
        options={baseOptions}
        onChange={handleChange}
      />
      <Icon color="orange" name="calendar alternate outline" />
      <Button type="button" icon basic onClick={handleTodayClick}>
        <Icon color="green" name="calendar check outline" />
      </Button>
    </div>
  );
};

DateTimePicker.propTypes = propTypes;
DateTimePicker.defaultProps = defaultProps;

export default DateTimePicker;
