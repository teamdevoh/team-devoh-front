import React, { useCallback, useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import 'flatpickr/dist/themes/material_green.css';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/l10n/ko.js';
import helpers from '../../helpers';
import { Icon, Button } from 'semantic-ui-react';
import { format } from '../../constants';
import moment from 'moment';
import { useFormatMessage } from '../../hooks';
import styled from 'styled-components';

const propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  /**
   * onChange gets triggered when the user selects a date, or changes the time on a selected date.
   * @param {Array} selectedDates React's original SyntheticEvent.
   * @param {string} dateStr  a string representation of the latest selected Date object by the user. The string is formatted as per the dateFormat option.
   * @param {object} instance the flatpickr object, containing various methods and properties.
   */
  onChange: PropTypes.func,
  /**
   * see https://flatpickr.js.org/examples/#basic
   */
  options: PropTypes.object
};

const ErrorMessage = styled.div`
  color: red;
`;

const defaultProps = {};

const DatePicker = ({ name, onChange, options, value }) => {
  const t = useFormatMessage();
  const [error, setError] = useState(false);

  const baseOptions = {
    mode: 'single',
    locale: helpers.util.getLocale(),
    allowInput: true,
    parseDate: (datestr, format) => {
      const date = moment(datestr);

      if (date.isValid()) {
        setError(false);
      } else {
        setError(true);
        return null;
      }

      date.format(format.YYYY_MM_DD);
      return date.toDate();
    },
    ...options
  };

  const handleTodayClick = useCallback(
    () => {
      onChange(null, {
        name,
        value: `${helpers.util.dateformat(
          new Date(),
          format.YYYY_MM_DD
        )}T00:00:00`
      });
    },
    [onChange]
  );

  const handleChange = useCallback(
    (dates, currentDateString) => {
      onChange(null, {
        name,
        value: !!currentDateString ? `${currentDateString}T00:00:00` : null
      });
    },
    [onChange]
  );

  const handleBlur = e => {
    const { value } = e.target;

    if (!!value) {
      const newDate = moment(value);
      let isValid = null;

      if (newDate.isValid()) {
        isValid = true;
      } else {
        isValid = false;
        setError(true);
      }

      onChange(null, {
        name,
        value: isValid ? `${newDate.format(format.YYYY_MM_DD)}T00:00:00` : null
      });
    }
  };

  return (
    <Fragment>
      <div className="ui left icon action input">
        <Flatpickr
          name={name}
          value={value}
          options={baseOptions}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder={format.YYYY_MM_DD}
        />
        <Icon color="orange" name="calendar alternate outline" />
        <Button type="button" icon basic onClick={handleTodayClick}>
          <Icon color="green" name="calendar check outline" />
        </Button>
      </div>
      {error && <ErrorMessage>{t('check.the.date')}</ErrorMessage>}
    </Fragment>
  );
};

DatePicker.propTypes = propTypes;
DatePicker.defaultProps = defaultProps;

export default DatePicker;
