import React from 'react';
import { Input } from 'semantic-ui-react';
import { asyncComponent } from '../modules';

const DatePicker = asyncComponent(() =>
  import('./DatePicker'), <Input disabled />
);
const DateRangePicker = asyncComponent(() =>
  import('./DateRangePicker'), <Input disabled />
);
const DateTimePicker = asyncComponent(() =>
  import('./DateTimePicker')
);
const TimePicker = asyncComponent(() =>
  import('./TimePicker')
);

export {
  DatePicker,
  DateRangePicker,
  DateTimePicker,
  TimePicker
};
