import React from 'react';
import PropTypes from 'prop-types';
import 'flatpickr/dist/themes/material_green.css';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/l10n/ko.js';
import { Icon } from 'semantic-ui-react';
import helpers from '../../helpers';

const propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.array,
  /**
   * onChange gets triggered when the user selects a date, or changes the time on a selected date.
   * @param {Array} selectedDates React's original SyntheticEvent.
   * @param {string} dateStr  a string representation of the latest selected Date object by the user. The string is formatted as per the dateFormat option.
   * @param {object} instance the flatpickr object, containing various methods and properties.
   */
  onChange: PropTypes.func,
  /**
   * see https://flatpickr.js.org/examples/#range-calendar
   */
  options: PropTypes.object
};

const defaultProps = {
  value: []
};

const DateRangePicker = ({
  name,
  onChange,
  options,
  value,
  disabled = false,
  className = ''
}) => {
  const baseOptions = {
    mode: 'range',
    locale: helpers.util.getLocale(),
    allowInput: true,
    ...options
  };

  const handleChange = date => {
    onChange(null, { name, value: date });
  };

  return (
    <div className={`ui left icon input ${className}`}>
      <Flatpickr
        name={name}
        value={value}
        options={baseOptions}
        onChange={handleChange}
        disabled={disabled}
      />
      <Icon name="calendar alternate outline" />
    </div>
  );
};

DateRangePicker.propTypes = propTypes;
DateRangePicker.defaultProps = defaultProps;

export default DateRangePicker;
