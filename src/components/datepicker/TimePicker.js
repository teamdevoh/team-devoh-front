import React from 'react';
import PropTypes from 'prop-types';
import 'flatpickr/dist/themes/material_green.css';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/l10n/ko.js';
import helpers from '../../helpers';
import { Icon } from 'semantic-ui-react';

const propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  /**
   * onChange gets triggered when the user selects a date, or changes the time on a selected date.
   * @param {Array} selectedDates React's original SyntheticEvent.
   * @param {string} dateStr  a string representation of the latest selected Date object by the user. The string is formatted as per the dateFormat option.
   * @param {object} instance the flatpickr object, containing various methods and properties.
   */
  onChange: PropTypes.func,
  /**
   * see https://flatpickr.js.org/examples/#time-picker
   */
  options: PropTypes.object
};

const defaultProps = {
  value: ''
};

const TimePicker = ({ name, onChange, options, value }) => {
  const baseOptions = {
    mode: 'time',
    locale: helpers.util.getLocale(),
    enableTime: true,
    noCalendar: true,
    ...options
  };

  return (
    <div className="ui left icon input">
      <Flatpickr
        name={name}
        value={value}
        options={baseOptions}
        onChange={onChange}
      />
      <Icon name="clock outline" />
    </div>
  );
};

TimePicker.propTypes = propTypes;
TimePicker.defaultProps = defaultProps;

export default TimePicker;
