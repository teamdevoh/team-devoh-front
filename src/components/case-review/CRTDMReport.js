import React, { useState } from 'react';
import { Header, Segment } from 'semantic-ui-react';
import useSubjectsForTDMReport from '../tdm-report/hooks/useSubjectsForTDMReport';
import { SubjectTDMVisit, TDMReportViewer } from '../tdm-report';

const CRTDMReport = ({ projectId, projectSiteId, subjectId }) => {
  const tdmReport = useSubjectsForTDMReport({
    projectId,
    projectSiteId,
    subjectId
  });
  const [selectedVisit, setSelectedVisit] = useState({
    visitOrder: 0,
    resultDTC: '',
    sampleTimes: []
  });

  const handleSubjectVisitClick = item => {
    setSelectedVisit({ ...item });
  };

  return (
    <div>
      <Segment style={{ height: '800px', overflow: 'auto' }}>
        <Header as="h4">Select Of TDM Visit</Header>
        <SubjectTDMVisit
          subjectId={subjectId}
          items={tdmReport.items}
          onRowClick={handleSubjectVisitClick}
          initFirstSelected={true}
        />
        {selectedVisit.subjectVisitId &&
          subjectId && (
            <TDMReportViewer
              subjectId={subjectId}
              subjectVisitId={Number(selectedVisit.subjectVisitId)}
              initFirstSelected={true}
            />
          )}
      </Segment>
    </div>
  );
};

export default CRTDMReport;
