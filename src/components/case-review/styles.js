import styled from 'styled-components';
import { Table } from 'semantic-ui-react';

export const TableWrap = styled.div`
  width: 100%;
  max-height: 80vh;
  overflow: auto;

  ${props => {
    if (!!props.maxHeight) {
      return `max-height: ${props.maxHeight}`;
    }
  }};
`;

export const StyledTh = styled(Table.HeaderCell)`
  &&& {
    position: sticky;
    top: 0px;
    z-index: 10;

    ${props => {
      if (!!props.top) {
        return `top: ${props.top}`;
      }
    }};

    @media only screen and (max-width: 767px) {
      position: inherit;
      top: inherit;
    }
  }
`;
