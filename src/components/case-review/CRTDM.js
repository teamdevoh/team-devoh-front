import React, { useState } from 'react';
import { useGetProjectActivityId } from '../collection/hooks';
import { useTDMConcentrationForCR } from './hooks';
import { useFormatMessage } from '../../hooks';
import { useBuild } from '../activity/hooks';
import { activity, activityGroup, code } from '../../constants';
import { Table } from 'semantic-ui-react';
import { SortableHeader } from './CROtherTest';
import helpers from '../../helpers';
import { TableWrap, StyledTh } from './styles';
import { EmptyRowsViewForSemantic } from '../modules';

const CRTDM = ({ subjectId, visit, drug }) => {
  const t = useFormatMessage();
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState();

  const build = useBuild();
  const tdmFirst = useGetProjectActivityId({
    activityGroupId: activityGroup.FirstTDM,
    subjectId,
    activityId: activity.TB_PK
  });
  const tdmFU = useGetProjectActivityId({
    activityGroupId: activityGroup.FUTDM,
    subjectId,
    activityId: activity.TB_PK
  });
  const tdm = useTDMConcentrationForCR(subjectId, tdmFirst.id, tdmFU.id);

  const handleSort = property => () => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);

    tdm.setItems(
      helpers.util.sort(tdm.items, property, isAsc ? 'desc' : 'asc')
    );
  };

  return (
    <TableWrap>
      <Table celled selectable>
        <Table.Header>
          <Table.Row>
            <StyledTh singleLine>No</StyledTh>
            <SortableHeader
              onClick={handleSort('subjectVisitId')}
              orderByKey="subjectVisitId"
              order={order}
              orderBy={orderBy}
            >
              {t('activity.visit')}
            </SortableHeader>
            <SortableHeader
              onClick={handleSort('genericName')}
              orderByKey="genericName"
              order={order}
              orderBy={orderBy}
            >
              {t('activity.tdmconcentration.exdrug')}
            </SortableHeader>
            <SortableHeader
              onClick={handleSort('dose')}
              orderByKey="dose"
              order={order}
              orderBy={orderBy}
            >
              {t('activity.common.exdose')}
            </SortableHeader>
            <SortableHeader
              onClick={handleSort('medDatetime')}
              orderByKey="medDatetime"
              order={order}
              orderBy={orderBy}
            >
              {t('activity.tdmconcentration.medDatetime')}
            </SortableHeader>
            <SortableHeader
              onClick={handleSort('sampleTime')}
              orderByKey="sampleTime"
              order={order}
              orderBy={orderBy}
            >
              {t('activity.tdmconcentration.sampleTime')}
            </SortableHeader>
            <StyledTh>{t('activity.tdmconcentration.TAD')}</StyledTh>
            <StyledTh>{t('activity.tdmconcentration.drugConc')}</StyledTh>
            <StyledTh>AUC</StyledTh>
            <StyledTh>Cmax</StyledTh>
            <StyledTh>{t('activity.tdmconcentration.resultdtc')}</StyledTh>
            <StyledTh>{t('activity.tdmconcentration.pkQuest')}</StyledTh>
            <StyledTh>{t('activity.tdmconcentration.sampleId')}</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {tdm.items.map((item, index) => {
            return (
              <Table.Row key={item.key}>
                <Table.Cell>{index + 1}</Table.Cell>
                <Table.Cell>
                  {build.displayName({
                    source: visit.items,
                    value: item.subjectVisitId
                  })}
                </Table.Cell>
                <Table.Cell>
                  {build.displayName({
                    source: drug.items,
                    value: item.exdrugId,
                    other: item.exdrugoth,
                    otherValue: code.TBDrugOther
                  })}
                </Table.Cell>
                <Table.Cell>{item.dose}</Table.Cell>
                <Table.Cell>{item.medDatetime}</Table.Cell>
                <Table.Cell>{item.sampleTime}</Table.Cell>
                <Table.Cell>{item.tad}</Table.Cell>
                <Table.Cell>{item.drugConcValueUnit}</Table.Cell>
                <Table.Cell>{item.ppAuc}</Table.Cell>
                <Table.Cell>{item.ppCmax}</Table.Cell>
                <Table.Cell>{item.resultDate}</Table.Cell>
                <Table.Cell>{item.questionable}</Table.Cell>
                <Table.Cell>{item.sampleId}</Table.Cell>
              </Table.Row>
            );
          })}
          {tdm.items.length === 0 && (
            <EmptyRowsViewForSemantic loading={tdm.loading} columnLength={11} />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default CRTDM;
