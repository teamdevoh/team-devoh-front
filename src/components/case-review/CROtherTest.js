import React, { Fragment, useState } from 'react';
import useOtherTestsForCR from './hooks/useOtherTestsForCR';
import { Table, Header, Icon } from 'semantic-ui-react';
import helpers from '../../helpers';
import styled from 'styled-components';
import { TableWrap, StyledTh } from './styles';
import { EmptyRowsViewForSemantic } from '../modules';

const SortableHeaderCell = styled(StyledTh)`
  &&& {
    cursor: pointer;

    & i {
      opacity: 0.4;
    }

    &:hover i {
      opacity: 1;
    }
  }
`;

const OrderIcon = ({ orderByKey, order, orderBy }) => {
  const isUpDown =
    orderByKey !== orderBy || order.toLowerCase() === 'asc' ? 'up' : 'down';
  return (
    <Icon
      style={orderByKey === orderBy ? { opacity: 1 } : null}
      name={`arrow ${isUpDown}`}
    />
  );
};

export const SortableHeader = ({
  children,
  orderByKey,
  order,
  orderBy,
  width,
  singleLine = false,
  as,
  onClick = f => f
}) => {
  return (
    <SortableHeaderCell
      width={width}
      singleLine={singleLine}
      onClick={onClick}
      as={as}
    >
      <div style={{ display: 'flex' }}>
        <OrderIcon orderByKey={orderByKey} order={order} orderBy={orderBy} />
        {children}
      </div>
    </SortableHeaderCell>
  );
};

const CROtherTest = ({ subjectId }) => {
  const otherTest = useOtherTestsForCR({ subjectId });
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('date');

  const handleSort = property => () => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);

    otherTest.setItems(
      helpers.util.sort(otherTest.items, property, isAsc ? 'desc' : 'asc')
    );
  };

  return (
    <TableWrap>
      <Table celled selectable>
        <Table.Header>
          <Table.Row>
            <StyledTh width="1" singleLine>
              No
            </StyledTh>
            <SortableHeader
              width="1"
              singleLine
              onClick={handleSort('activityName')}
              orderByKey="activityName"
              order={order}
              orderBy={orderBy}
            >
              Activity
            </SortableHeader>
            <SortableHeader
              width="1"
              onClick={handleSort('date')}
              orderByKey="date"
              order={order}
              orderBy={orderBy}
            >
              Date
            </SortableHeader>
            <SortableHeader
              width="3"
              onClick={handleSort('name')}
              orderByKey="name"
              order={order}
              orderBy={orderBy}
            >
              Test
            </SortableHeader>
            <StyledTh>Value</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {otherTest.items.map((item, index) => {
            return (
              <Table.Row key={item.no}>
                <Table.Cell>{index + 1}</Table.Cell>
                <Table.Cell>
                  <Header as="h4" textAlign="center">
                    {item.activityName}
                  </Header>
                </Table.Cell>
                <Table.Cell singleLine>{item.date}</Table.Cell>
                <Table.Cell>{item.name}</Table.Cell>
                <Table.Cell>
                  {item.item.split('\n').map((value, index) => {
                    return (
                      <Fragment key={index}>
                        {value}
                        {value && <br />}
                      </Fragment>
                    );
                  })}
                </Table.Cell>
              </Table.Row>
            );
          })}
          {otherTest.items.length === 0 && (
            <EmptyRowsViewForSemantic
              loading={otherTest.loading}
              columnLength={5}
            />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default CROtherTest;
