import React from 'react';
import { useMICResultsForCR } from './hooks';
import { useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import { format } from '../../constants';
import { Table } from 'semantic-ui-react';
import { TableWrap, StyledTh } from './styles';
import { EmptyRowsViewForSemantic } from '../modules';

const CRMICResults = ({ subjectId }) => {
  const mic = useMICResultsForCR(subjectId);
  const t = useFormatMessage();

  return (
    <TableWrap>
      <Table celled selectable>
        <Table.Header>
          <Table.Row>
            <StyledTh singleLine>No</StyledTh>
            <StyledTh>{t('activity.subtbdrug.exdrugid')}</StyledTh>
            <StyledTh>{t('activity.subtbdrug.exdose')}</StyledTh>
            <StyledTh>{t('activity.common.exPeriod')}</StyledTh>
            <StyledTh>{t('activity.micresult.micstartdtc')}</StyledTh>
            <StyledTh>{t('activity.micresult.micenddtc')}</StyledTh>
            <StyledTh>{t('activity.micresult.testmic')}</StyledTh>
            <StyledTh>{t('activity.micresult.typemic')}</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {mic.items.map((item, index) => {
            return (
              <Table.Row key={item.subjectMicresultId}>
                <Table.Cell>{index + 1}</Table.Cell>
                <Table.Cell>{item.genericName}</Table.Cell>
                <Table.Cell>
                  {item.exdose ? `${item.exdose} mg` : ''}
                </Table.Cell>
                <Table.Cell>
                  {' '}
                  {helpers.util.dateRange(
                    item.exstdtc,
                    item.exendtc,
                    format.YYYY_MM_DD
                  )}
                </Table.Cell>
                <Table.Cell>
                  {helpers.util.dateformat(item.micstartdtc, format.YYYY_MM_DD)}
                </Table.Cell>
                <Table.Cell>
                  {helpers.util.dateformat(item.micenddtc, format.YYYY_MM_DD)}
                </Table.Cell>
                <Table.Cell>
                  {item.testmic ? `${item.testmic} μg/ml` : ''}
                </Table.Cell>
                <Table.Cell>
                  {item.typemic ? `${item.typemic} μg/ml` : ''}
                </Table.Cell>
              </Table.Row>
            );
          })}
          {mic.items.length === 0 && (
            <EmptyRowsViewForSemantic loading={mic.loading} columnLength={8} />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default CRMICResults;
