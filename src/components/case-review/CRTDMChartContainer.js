import React, { useEffect, useState, Fragment } from 'react';
import { Grid, Accordion, Icon, Header } from 'semantic-ui-react';
import { role } from '../../constants';
import { RoleAware } from '../auth';
import CRScatterExportCSV from './CRScatterExportCSV';
import { useDrugSubjects, useDrugConcToNormalize } from './hooks';
import TDMDosageWrapper from './TDMDosageWrapper';
import TDMNormalWrapper from './TDMNormalWrapper';
import TDMDrugWrapper from './TDMDrugWrapper';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { AccordionTitle } from '../tdm-scatter-plot/ChartWrapper';

export const chartDataGenerate = source => {
  const data = source[0].tbdrugPrediction.map(item => [
    item.tad,
    item.drugConc
  ]);
  const newChartData = {
    name: 'Population Prediction',
    color: '#db2828',
    data: [...data],
    type: 'line'
  };

  return newChartData;
};

const getNormalizedPredictionData = ({ dose, source }) => {
  return source.filter(item => Number(item.dose) === Number(dose));
};

const CRTDMChartContainer = ({ subjectId, exDrugId, predictItems }) => {
  const t = useFormatMessage();
  const { items, loading } = useDrugSubjects(subjectId, exDrugId);
  const normalized = useDrugConcToNormalize({ source: items });
  const [normalPredictItems, setNormalPredictItems] = useState([]);

  const [activeIndex, setActiveIndex] = useState(-1);

  const handleAccordionClick = (e, titleProps) => {
    const { index } = titleProps;
    const newIndex = activeIndex === index ? -1 : index;

    setActiveIndex(newIndex);
  };

  useEffect(
    () => {
      setNormalPredictItems(
        getNormalizedPredictionData({
          dose: normalized.exdose,
          source: predictItems
        })
      );
    },
    [predictItems, normalized]
  );

  useEffect(
    () => {
      setActiveIndex(-1);
    },
    [exDrugId]
  );

  return (
    <Fragment>
      <RoleAware allowedRole={role.SubjectDrugDosageOrigin_Read}>
        <CRScatterExportCSV exdrugId={exDrugId} />
      </RoleAware>
      <Grid columns={2} stackable>
        <Grid.Row>
          <TDMDrugWrapper
            subjectId={Number(subjectId)}
            exdrugId={exDrugId}
            loading={loading}
            source={items}
          />
        </Grid.Row>
      </Grid>
      <Accordion>
        <AccordionTitle
          active={activeIndex === 0}
          index={0}
          onClick={handleAccordionClick}
        >
          <Header as="h4">
            <Icon name="dropdown" />
            {t('standard.dose')}
          </Header>
        </AccordionTitle>
        <Accordion.Content active={activeIndex === 0}>
          <Grid columns={2} stackable>
            <Grid.Row>
              <TDMNormalWrapper
                subjectId={Number(subjectId)}
                exdrugId={exDrugId}
                loading={loading}
                source={items}
                predictItems={normalPredictItems}
                normalized={normalized}
              />
            </Grid.Row>
          </Grid>
        </Accordion.Content>
        <AccordionTitle
          active={activeIndex === 1}
          index={1}
          onClick={handleAccordionClick}
        >
          <Header as="h4">
            <Icon name="dropdown" />
            {t('same.dose')}
          </Header>
        </AccordionTitle>
        <Accordion.Content active={activeIndex === 1}>
          <Grid columns={2} stackable>
            <Grid.Row>
              <TDMDosageWrapper
                subjectId={Number(subjectId)}
                exDrugId={exDrugId}
                loading={loading}
                source={items}
                predictItems={normalPredictItems}
                normalizedDose={normalized.exdose}
              />
            </Grid.Row>
          </Grid>
        </Accordion.Content>
      </Accordion>
    </Fragment>
  );
};

export default CRTDMChartContainer;
