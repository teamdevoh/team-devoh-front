import React, { Fragment } from 'react';
import { useTypeCheck, useVerifyRange } from '../../hooks';
import { Label } from 'semantic-ui-react';
import { activity } from '../../constants';

const CRLabRangeLabel = ({ min, max, value, unit }) => {
  const { isNumeric } = useTypeCheck();
  const [level] = useVerifyRange({ min, max, value });

  const renderValueItem = () => {
    if (isNumeric(value)) {
      if (level && level !== activity.LabRanges.NORMAL.name) {
        return (
          <Label color={activity.LabRanges[level].color.name}>
            {value} {unit}
          </Label>
        );
      } else {
        return (
          <Fragment>
            {value} {unit}
          </Fragment>
        );
      }
    } else {
      return <Fragment>{value}</Fragment>;
    }
  };

  return <div>{renderValueItem()}</div>;
};

export default CRLabRangeLabel;
