import React, { Fragment } from 'react';
import { Grid, Loader } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import AdjustChart from './AdjustChart';
import { useSubjectDrug } from './hooks';
import OriginalChart from './OriginalChart';

const TDMNormalWrapper = ({
  subjectId,
  exdrugId,
  loading,
  predictItems = [],
  normalized = {}
}) => {
  const data = useSubjectDrug(
    subjectId,
    exdrugId,
    normalized.source,
    predictItems
  );
  const t = useFormatMessage();

  return (
    <Fragment>
      <Grid.Column>
        <Loader active={loading} />
        <OriginalChart
          title={t('cr.tdm.scatter.normalized', { dose: normalized.exdose })}
          subjectId={subjectId}
          exDrugId={exdrugId}
          data={data}
        />
      </Grid.Column>
      <Grid.Column>
        <Loader active={loading} />
        <AdjustChart
          title={t('cr.tdm.scatter.normalized', { dose: normalized.exdose })}
          subjectId={subjectId}
          exDrugId={exdrugId}
          data={data}
        />
      </Grid.Column>
    </Fragment>
  );
};

export default TDMNormalWrapper;
