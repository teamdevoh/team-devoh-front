import React from 'react';
import { Header } from 'semantic-ui-react';

const CRSubTitle = ({ title }) => {
  return <Header as="h3">{title}</Header>;
};

export default CRSubTitle;
