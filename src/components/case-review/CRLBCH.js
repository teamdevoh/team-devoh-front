import React, { Fragment } from 'react';
import { useGetProjectActivityId } from '../collection/hooks';
import orderBy from 'lodash/orderBy';
import find from 'lodash/find';
import { useTypeCheck, useFormatMessage, useBoolean } from '../../hooks';
import CRLabRangeLabel from './CRLabRangeLabel';
import { useLBCHForCR } from './hooks';
import { useSelector } from 'react-redux';
import { subject } from '../../states';
import { Checkbox, Loader, Table } from 'semantic-ui-react';
import { LowSpan, HighSpan } from '../activity/lbch/Range';
import { useLabRange } from '../activity/hooks';
import { activity, activityGroup } from '../../constants';
import { TableWrap, StyledTh } from './styles';
import { EmptyRowsViewForSemantic } from '../modules';

const CRLBCH = ({ subjectId }) => {
  const [labRangeLBCH] = useLabRange({
    subjectId,
    lbType: activity.LabType.LBCH
  });
  const showTEP = useBoolean(true);
  const { isNumeric } = useTypeCheck();
  const t = useFormatMessage();
  const lbch = useGetProjectActivityId({
    activityGroupId: activityGroup.ADR,
    subjectId,
    activityId: activity.LBCH
  });

  const { items, loading } = useLBCHForCR({
    subjectId,
    projectActivityId: lbch.id
  });

  let progressItems = [];

  const timeEvent = useSelector(subject.selectors.getCRItemsWithFilter);

  const timeEventGroups = _.groupBy(timeEvent.items, item => {
    return [item.activityId, item.originStartDate, item.originEndDate];
  });

  Object.keys(timeEventGroups).forEach(key => {
    const group = timeEventGroups[key];
    let summary = '';

    if (group.length > 1) {
      group.forEach(item => {
        summary += item.summary + '\n';
      });
    }

    const item = group[0];
    progressItems.push({
      key: item.key,
      activityName: item.activityName,
      originStartDate: item.originStartDate,
      originEndDate: item.originEndDate,
      startDate: item.startDate,
      endDate: item.endDate,
      summary: summary ? summary : item.summary,
      isLab: false
    });
  });

  items.forEach(item => {
    progressItems.push({
      key: `${item.activityId}_${item.subjectLbchid}`,
      activityName: activity.LabType.LBCH,
      originStartDate: item.lbdtc,
      originEndDate: item.lbdtc,
      startDate: item.lbdtc,
      endDate: item.lbdtc,
      summary: '',
      isLab: true,
      alb: item.alb,
      alp: item.alp,
      alt: item.alt,
      ast: item.ast,
      bun: item.bun,
      cr: item.cr,
      egfr: item.egfr,
      tbil: item.tbil,
      tpro: item.tpro,
      ua: item.ua
    });
  });

  const filteredItems = showTEP.value
    ? progressItems
    : _.filter(progressItems, { isLab: true });

  const orderedItems = orderBy(filteredItems, ['startDate'], ['asc']);
  const lbchItems = activity.LabLBCHItems;

  const alb = find(labRangeLBCH.ranges, { item: lbchItems.ALB });
  const bun = find(labRangeLBCH.ranges, { item: lbchItems.BUN });
  const cr = find(labRangeLBCH.ranges, { item: lbchItems.CR });
  const egfr = find(labRangeLBCH.ranges, { item: lbchItems.EGFR });
  const tbil = find(labRangeLBCH.ranges, { item: lbchItems.TBIL });
  const tpro = find(labRangeLBCH.ranges, { item: lbchItems.TPRO });
  const ast = find(labRangeLBCH.ranges, { item: lbchItems.AST });
  const alt = find(labRangeLBCH.ranges, { item: lbchItems.ALT });
  const alp = find(labRangeLBCH.ranges, { item: lbchItems.ALP });
  const ua = find(labRangeLBCH.ranges, { item: lbchItems.UA });

  return (
    <Fragment>
      <Loader active={loading} />
      <Checkbox
        toggle
        label="Time Event Progress"
        checked={showTEP.value}
        onChange={showTEP.toggle}
      />
      <TableWrap>
        <Table fixed celled singleLine selectable>
          <Table.Header>
            <Table.Row>
              <StyledTh style={{ width: '200px' }}>Date</StyledTh>
              <StyledTh width="two">{t('activity')}</StyledTh>
              <StyledTh width="three">Item</StyledTh>
              <StyledTh title="Albumin">
                Albumin
                <br />(<LowSpan text={alb.low} /> ~ <HighSpan text={alb.high} />
                )
              </StyledTh>
              <StyledTh title="BUN">
                BUN
                <br />(<LowSpan text={bun.low} /> ~ <HighSpan text={bun.high} />
                )
              </StyledTh>
              <StyledTh title="Creatinine">
                Creatinine
                <br />(<LowSpan text={cr.low} /> ~ <HighSpan text={cr.high} />)
              </StyledTh>
              <StyledTh title="eGFR (CKD-EPI)">eGFR (CKD-EPI)</StyledTh>
              <StyledTh title="TBIL">
                TBIL
                <br />(<LowSpan text={tbil.low} /> ~{' '}
                <HighSpan text={tbil.high} />)
              </StyledTh>
              <StyledTh title="Total Protein">
                Total Protein
                <br />(<LowSpan text={tpro.low} /> ~{' '}
                <HighSpan text={tpro.high} />)
              </StyledTh>
              <StyledTh title="AST (SGOT)">
                AST (SGOT)
                <br />(<LowSpan text={ast.low} /> ~ <HighSpan text={ast.high} />
                )
              </StyledTh>
              <StyledTh title="ALT (SGPT)">
                ALT (SGPT)
                <br />(<LowSpan text={alt.low} /> ~ <HighSpan text={alt.high} />
                )
              </StyledTh>
              <StyledTh title="ALP">
                ALP
                <br />(<LowSpan text={alp.low} /> ~ <HighSpan text={alp.high} />
                )
              </StyledTh>
              <StyledTh title="Uric acid">
                Uric acid
                <br />(<LowSpan text={ua.low} /> ~ <HighSpan text={ua.high} />)
              </StyledTh>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {orderedItems.map(item => {
              const summary = item.summary.replace(/\|\|/g, ' ');
              return (
                <Table.Row
                  key={item.key}
                  style={{
                    fontStyle: item.isLab ? 'normal' : 'italic',
                    fontWeight: item.isLab ? 'bold' : 'lighter'
                  }}
                >
                  <Table.Cell>
                    {item.originStartDate === item.originEndDate
                      ? item.originStartDate
                      : `${item.originStartDate} ~ ${item.originEndDate}`}
                  </Table.Cell>
                  <Table.Cell>{item.activityName}</Table.Cell>
                  <Table.Cell title={summary}>
                    {summary.split('\n').map((value, index) => {
                      return <div key={index}>{value}</div>;
                    })}
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={alb.low}
                      max={alb.high}
                      value={item.alb}
                      unit={alb.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={bun.low}
                      max={bun.high}
                      value={item.bun}
                      unit={bun.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={cr.low}
                      max={cr.high}
                      value={item.cr}
                      unit={cr.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    {isNumeric(item.egfr) ? egfr.unit : ''}
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={tbil.low}
                      max={tbil.high}
                      value={item.tbil}
                      unit={tbil.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={tpro.low}
                      max={tpro.high}
                      value={item.tpro}
                      unit={tpro.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={ast.low}
                      max={ast.high}
                      value={item.ast}
                      unit={ast.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={alt.low}
                      max={alt.high}
                      value={item.alt}
                      unit={alt.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={alp.low}
                      max={alp.high}
                      value={item.alp}
                      unit={alp.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={ua.low}
                      max={ua.high}
                      value={item.ua}
                      unit={ua.unit}
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
            {orderedItems.length === 0 && (
              <EmptyRowsViewForSemantic loading={loading} columnLength={12} />
            )}
          </Table.Body>
        </Table>
      </TableWrap>
    </Fragment>
  );
};

export default CRLBCH;
