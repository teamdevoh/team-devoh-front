import React from 'react';
import { Header } from 'semantic-ui-react';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';

const CRTitle = () => {
  return (
    <Header as="h3" dividing>
      Case Review
      <Header.Subheader>Time Event Progress</Header.Subheader>
      <CopyRedirectUrl />
    </Header>
  );
};

export default CRTitle;
