import React, { useEffect } from 'react';
import { Header, Segment } from 'semantic-ui-react';
import { useBoolean, useFormatMessage } from '../../hooks';
import CRTDMChart from './CRTDMChart';
import FakeScatter from './FakeScatter';
import { useAdjustTAD } from './hooks';

const AdjustChart = ({ subjectId, exDrugId, title, data, show = false }) => {
  const showAdjust = useBoolean(show);
  const t = useFormatMessage();
  const adjust = useAdjustTAD();
  data = data.filter(item => item.pkQuest !== true);

  useEffect(
    () => {
      if (show === false) {
        showAdjust.setFalse();
      }
    },
    [subjectId, exDrugId, show]
  );

  return (
    <Segment basic onClick={showAdjust.setTrue}>
      <Header as="h4">
        {title ? `${title} - ` : ''}
        {t('cr.tdm.scatter.adjust.tad')}
      </Header>
      {showAdjust.value === false && <FakeScatter />}
      {showAdjust.value === true && (
        <CRTDMChart subjectId={subjectId} series={adjust.exec(data)} />
      )}
    </Segment>
  );
};

export default AdjustChart;
