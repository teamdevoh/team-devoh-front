import React from 'react';
import { asyncComponent } from '../modules';
import { Placeholder } from 'semantic-ui-react';

const CRContainer = asyncComponent(() => import('./CRContainer'));

export { CRContainer };
