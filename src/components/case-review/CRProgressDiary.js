import React from 'react';
import { Tab } from 'semantic-ui-react';

import CRProgress from './CRProgress';
import CRSubTitle from './CRSubTitle';
import CRSummary from './CRSummary';
import CRTDMChartWrapper from './CRTDMChartWrapper';

const CRProgressDiary = ({ projectId, subjectId, projectSiteId }) => {
  return (
    <Tab
      menu={{ secondary: true, pointing: true }}
      panes={[
        {
          menuItem: 'Time Event Progress',
          render: () => (
            <div
            // style={{ height: '680px', overflow: 'auto' }}
            >
              <CRProgress />
            </div>
          )
        },
        {
          menuItem: 'TDM',
          render: () => (
            <CRTDMChartWrapper
              projectId={projectId}
              subjectId={subjectId}
              projectSiteId={projectSiteId}
            />
          )
        },
        {
          menuItem: 'Summary',
          render: () => (
            <React.Fragment>
              <CRSubTitle title="Summary" />
              <div
              // style={{ height: '680px', overflow: 'auto' }}
              >
                <CRSummary subjectId={subjectId} />
              </div>
            </React.Fragment>
          )
        }
      ]}
    />
  );
};

export default CRProgressDiary;
