import React, { useEffect, useState, memo, Fragment, useCallback } from 'react';
import moment from 'moment';
import Chart from 'react-apexcharts';
import helpers from '../../helpers';
import { useSelector } from 'react-redux';
import { subject } from '../../states';
import { Checkbox, Grid } from 'semantic-ui-react';

let dumpData = [];

const options = {
  plotOptions: {
    bar: {
      horizontal: true,
      distributed: true,
      dataLabels: {
        hideOverflowingLabels: false
      }
    }
  },
  dataLabels: {
    enabled: true,
    enabledOnSeries: undefined,
    formatter: (val, opts) => {
      const start = moment(val[0]);
      const end = moment(val[1]);

      let startDate = helpers.util.dateformat(start).replace(' 00:00', '');
      let endDate = helpers.util.dateformat(end).replace(' 00:00', '');

      if (dumpData[opts.dataPointIndex].hasStartUK) {
        startDate = dumpData[opts.dataPointIndex].originStartDate;
      }

      if (dumpData[opts.dataPointIndex].hasEndUK) {
        endDate = dumpData[opts.dataPointIndex].originEndDate;
      }

      if (start.isSame(end) || startDate === endDate) {
        return startDate;
      } else {
        if (startDate.length > 0 && endDate.length > 0) {
          return `${startDate} ~ ${endDate}`;
        }
      }
    },
    offsetX: 0,
    offsetY: 0,
    style: {
      fontSize: '12px',
      fontWeight: 'bold',
      colors: undefined
    },
    background: {
      enabled: true,
      // foreColor: '#fff',
      padding: 5,
      borderRadius: 0,
      borderWidth: 0,
      // borderColor: '#fff',
      opacity: 0.9,
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: 'red',
        opacity: 0.45
      }
    },
    dropShadow: {
      enabled: false,
      top: 1,
      left: 1,
      blur: 1,
      color: '#000',
      opacity: 0.45
    }
  },
  xaxis: {
    type: 'datetime'
  },
  tooltip: {
    enabled: true,
    enabledOnSeries: undefined,
    custom: undefined,
    y: {
      show: false,
      title: {
        formatter: (aa, bb, cc) => {}
      },
      formatter: (val, opts) => {
        return '';
      }
    }
  },
  grid: {
    row: {
      colors: ['#f3f4f5', '#fff'],
      opacity: 1
    }
  }
};

const CRProgress = () => {
  const timeEvent = useSelector(subject.selectors.getCRItemsWithFilter);
  const [selectedActivities, setSelectedActivities] = useState([]);
  const [chartHeight, setChartHeight] = useState('100px');
  const [chartState, setChartState] = useState({
    series: [],
    options: {}
  });

  const buildData = items => {
    let data = [];
    const colors = ['#008FFB', '#00E396', '#775DD0', '#FEB019', '#FF4560'];

    dumpData = items;

    let minDate = null;
    let maxDate = null;

    minDate = moment(
      items.find(item => {
        return item.startDate && item.hasStartUK === false;
      })
    );

    maxDate = moment(
      items.find(item => {
        return item.endDate && item.hasEndUK === false;
      })
    );

    items.forEach((item, index) => {
      let startDate = item.startDate;
      let endDate = item.endDate;

      if (item.hasStartUK) {
        const year =
          item.startDate.substr(0, 4) === '0000'
            ? minDate.year().toString()
            : item.startDate.substr(0, 4);
        const month =
          item.startDate.substr(5, 2) === '00'
            ? minDate.month()
            : item.startDate.substr(5, 2);
        const day =
          item.startDate.substr(8, 2) === '00'
            ? minDate.day()
            : item.startDate.substr(8, 2);
        const hour =
          item.startDate.substr(11, 2) === '00'
            ? minDate.hour()
            : item.startDate.substr(11, 2);
        const minute =
          item.startDate.substr(14, 2) === '00'
            ? minDate.minute()
            : item.startDate.substr(14, 2);

        const time = hour && minute ? ` ${hour}:${minute}` : '';
        startDate = `${year}-${month}-${day}${time}`;
      }

      if (item.hasEndUK) {
        const year =
          item.endDate.substr(0, 4) === '0000'
            ? maxDate.year()
            : item.endDate.substr(0, 4);
        const month =
          item.endDate.substr(5, 2) === '00'
            ? maxDate.month()
            : item.endDate.substr(5, 2);
        const day =
          item.endDate.substr(8, 2) === '00'
            ? maxDate.day()
            : item.endDate.substr(8, 2);
        const hour =
          item.endDate.substr(11, 2) === '00'
            ? maxDate.hour()
            : item.endDate.substr(11, 2);
        const minute =
          item.endDate.substr(14, 2) === '00'
            ? maxDate.minute()
            : item.endDate.substr(14, 2);

        const time = hour && minute ? ` ${hour}:${minute}` : '';
        endDate = `${year}-${month}-${day}${time}`;
      }

      let x = [item.activityName];
      item.summary.split('||').forEach(value => {
        if (value) {
          x.push(value);
        }
      });

      endDate = endDate ? endDate : startDate;

      data.push({
        x: x,
        y: [moment(startDate).valueOf(), moment(endDate).valueOf()],
        fillColor: colors[index % 5]
      });
    });

    setChartState({
      series: [{ data }],
      options: options
    });
  };

  useEffect(
    () => {
      let data = [];
      const activityGroups = _.groupBy(timeEvent.items, 'activityId');
      Object.keys(activityGroups).forEach(activityId => {
        data.push({
          id: activityId,
          name: activityGroups[activityId][0].activityName,
          checked: true
        });
      });

      setSelectedActivities(data);
      buildData(timeEvent.items);
      setChartHeight(`${70 * timeEvent.items.length}px`);
    },
    [timeEvent.items]
  );

  const handleActivityChecked = useCallback(
    (event, data) => {
      const foundItem = _.find(selectedActivities, { id: data.value });
      foundItem.checked = data.checked;

      setSelectedActivities(selectedActivities);

      const filteredItems = _.filter(timeEvent.items, item => {
        const foundItem = _.find(selectedActivities, {
          id: item.activityId.toString()
        });

        if (foundItem.checked) {
          return item;
        }
      });

      buildData(filteredItems);
      setChartHeight(`${70 * filteredItems.length}px`);
    },
    [selectedActivities]
  );

  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column width="sixteen">
          <Grid columns={6}>
            {selectedActivities.map((item, index) => {
              return (
                <Grid.Column key={item.id}>
                  <Checkbox
                    label={item.name}
                    value={item.id}
                    checked={item.checked}
                    onChange={handleActivityChecked}
                  />
                </Grid.Column>
              );
            })}
          </Grid>
        </Grid.Column>
      </Grid>
      <Chart
        options={options}
        series={chartState.series}
        type="rangeBar"
        width="100%"
        height={chartHeight}
      />
    </Fragment>
  );
};

export default memo(CRProgress);
