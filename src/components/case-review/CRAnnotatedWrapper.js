import React, { useCallback } from 'react';
import { Tab } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import CRMICResults from './CRMICResults';
import CRTimeEvent from './CRTimeEvent';
import CRADRContainer from './CRADRContainer';
import CRTDMContainer from './CRTDMContainer';
import CRReport from './CRReport';
import helpers from '../../helpers';
import CROtherTest from './CROtherTest';
import CRLBCH from './CRLBCH';
import CRLBHEM from './CRLBHEM';

const CRTab = () => {
  const params = useParams();
  const subjectId = params.subjectId;

  const download = useCallback(
    async ({ activityKeyId, activityAttachmentId }) => {
      helpers.util
        .download(
          `api/subjects/${subjectId}/activities/${activityKeyId}/attachs/${activityAttachmentId}`,
          percent => {}
        )
        .catch(err => {});
    },
    []
  );

  return (
    <Tab
      menu={{ secondary: true, pointing: true }}
      panes={[
        {
          menuItem: 'Time Event',
          render: () => <CRTimeEvent subjectId={subjectId} />
        },
        {
          menuItem: 'TDM',
          render: () => <CRTDMContainer subjectId={subjectId} />
        },
        {
          menuItem: 'MIC',
          render: () => <CRMICResults subjectId={subjectId} />
        },
        {
          menuItem: 'LBCH',
          render: () => <CRLBCH subjectId={subjectId} />
        },
        {
          menuItem: 'LBHEM',
          render: () => <CRLBHEM subjectId={subjectId} />
        },
        {
          menuItem: 'Other Test',
          render: () => <CROtherTest subjectId={subjectId} />
        },
        {
          menuItem: 'ADR',
          render: () => <CRADRContainer subjectId={subjectId} />
        },
        {
          menuItem: 'Report',
          render: () => <CRReport subjectId={subjectId} download={download} />
        }
      ]}
    />
  );
};

export default CRTab;
