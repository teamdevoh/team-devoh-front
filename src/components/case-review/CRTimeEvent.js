import React from 'react';
import { subject } from '../../states';
import { useSelector } from 'react-redux';
import { Table } from 'semantic-ui-react';
import { TableWrap, StyledTh } from './styles';
import { EmptyRowsViewForSemantic } from '../modules';

const CRTimeEvent = ({ subjectId }) => {
  const timeEvent = useSelector(subject.selectors.getCRItemsWithFilter);

  return (
    <TableWrap>
      <Table celled selectable>
        <Table.Header>
          <Table.Row>
            <StyledTh singleLine>No</StyledTh>
            <StyledTh>Event</StyledTh>
            <StyledTh>Event name</StyledTh>
            <StyledTh>Event Date</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {timeEvent.items.map((item, index) => {
            return (
              <Table.Row key={item.key}>
                <Table.Cell>{index + 1}</Table.Cell>
                <Table.Cell>{item.activityName}</Table.Cell>
                <Table.Cell>{item.summary.replace(/\|\|/g, ' ')}</Table.Cell>
                <Table.Cell>
                  {item.originStartDate === item.originEndDate
                    ? item.originStartDate
                    : `${item.originStartDate} ~ ${item.originEndDate}`}
                </Table.Cell>
              </Table.Row>
            );
          })}
          {timeEvent.items.length === 0 && (
            <EmptyRowsViewForSemantic
              loading={timeEvent.loading}
              columnLength={4}
            />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default CRTimeEvent;
