import React, { Fragment, useCallback } from 'react';
import { Grid, Container } from 'semantic-ui-react';
import { useSummaryForCR } from './hooks';
import { useCodes, useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import { useTbdrugList } from '../collection-tbdrug/hooks';
import styled from 'styled-components';
import { EmptyRowsView } from '../modules';
import { code, codeGroup, format } from '../../constants';

const RootGrid = styled(Grid)`
  &&& {
    margin: 0;
    border: 1px solid #d0d0d0;
  }
`;

const GridRow = styled(Grid.Row)`
  &&& {
    padding-top: 0;
    padding-bottom: 0;
    border-bottom: 1px solid #d0d0d0;

    &:last-child {
      border-bottom: none;
    }
  }
`;

const HeaderColumn = styled(Grid.Column)`
  &&& {
    background-color: #f9f9f9;
  }
`;

const HeaderContent = styled.div`
  padding: 8px 1px;
`;

const Contents = styled.div`
  padding-top: 8px;
  padding-bottom: 8px;
`;

const getCodeName = (codes = [], codeId, otherCodeId, other) => {
  let name = '';
  if (!!codeId) {
    if (codeId === otherCodeId) {
      name = other;
    } else {
      const foundCode = codes.filter(code => code.value === codeId)[0];
      name = foundCode && foundCode.text;
    }
  }

  return name;
};

const getDateRange = (start, end) => {
  if (!!start || !!end) {
    return `${start || ''} ~ ${end || ''}`;
  } else {
    return '';
  }
};

const gridWidth = {
  title: 3,
  contents: 13
};

const CRSummary = ({ subjectId }) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useSummaryForCR(subjectId);
  const [tbSusceptCodes] = useCodes({ codeGroupId: codeGroup.TBSusceptCode });
  const [tbDiagnosisCodes] = useCodes({
    codeGroupId: codeGroup.TBDiagnosisCode
  });
  const [tbTreatmentCodes] = useCodes({
    codeGroupId: codeGroup.TBTreatmentCode
  });
  const [retrtDrugCodes] = useCodes({
    codeGroupId: codeGroup.RetrtDrugCode
  });
  const [afbSpecCodes] = useCodes({ codeGroupId: codeGroup.AFBSpecCode });
  const [afbStainResultCodes] = useCodes({
    codeGroupId: codeGroup.AFBStainResultCode
  });
  const [afbCultureSolidCodes] = useCodes({
    codeGroupId: codeGroup.AFBCultureSolidCode
  });
  const [afbCultureLiquidCodes] = useCodes({
    codeGroupId: codeGroup.AFBCultureLiquidCode
  });
  const [pcrResultCodes] = useCodes({
    codeGroupId: codeGroup.PCRResultCode
  });
  const [comorbidCodes] = useCodes({
    codeGroupId: codeGroup.ComorbidCode
  });
  const [dosfrqCodes] = useCodes({
    codeGroupId: codeGroup.DosfrqCode
  });
  const [routeCodes] = useCodes({
    codeGroupId: codeGroup.RouteCode
  });
  const [dosfrmCodes] = useCodes({
    codeGroupId: codeGroup.DosfrmCode
  });
  const [{ items: tbdrugs }] = useTbdrugList();

  const generateDiagnostics = useCallback(
    () => {
      return items.diagnosis.map(item => {
        const {
          subjectTbDiagnosisId,
          dateDiagnosis,
          tbSusceptcodeId,
          drugSuscepOth,
          tbDiagnosiscodeId,
          tbOtherSite
        } = item;

        const contents = [];

        contents.push(
          getCodeName(
            tbSusceptCodes,
            tbSusceptcodeId,
            code.TBSusceptOther,
            drugSuscepOth
          )
        );
        contents.push(
          getCodeName(
            tbDiagnosisCodes,
            tbDiagnosiscodeId,
            code.TBDiagnosisOther,
            tbOtherSite
          )
        );

        return (
          <div key={subjectTbDiagnosisId}>
            {dateDiagnosis} : {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [items.diagnosis, tbSusceptCodes, tbDiagnosisCodes]
  );

  const generateTBTreatment = useCallback(
    () => {
      const result = [];
      const existContents = new Set();

      for (let i = 0; i < items.diagnosis.length; i++) {
        const {
          subjectTbDiagnosisId,
          tbTreatmentcodeId,
          prevDiag,
          retrtDtc,
          retrtDrugcodeId,
          retrtDrug2
        } = items.diagnosis[i];

        if (!!tbTreatmentcodeId) {
          // 결핵 치료 등록되어있는 경우
          const contents = [];

          contents.push(prevDiag);
          contents.push(retrtDtc);
          contents.push(getCodeName(retrtDrugCodes, retrtDrugcodeId));
          contents.push(retrtDrug2);

          const rowStr = contents.filter(text => !!text).join(', ');
          if (!existContents.has(rowStr)) {
            // 내용 중복 제거
            existContents.add(rowStr);

            result.push(
              <div key={subjectTbDiagnosisId}>
                {getCodeName(tbTreatmentCodes, tbTreatmentcodeId)}
                {rowStr && ` : ${rowStr}`}
              </div>
            );
          }
        }
      }

      return result;
    },
    [items.diagnosis, tbTreatmentCodes, retrtDrugCodes]
  );

  const generateTBStart = useCallback(
    () => {
      const result = [];
      const existContents = new Set();

      for (let i = 0; i < items.diagnosis.length; i++) {
        const { subjectTbDiagnosisId, tbStart } = items.diagnosis[i];

        if (!!tbStart) {
          // 결핵 치료 시작일이 등록되어있는 경우

          if (!existContents.has(tbStart)) {
            // 내용 중복 제거
            existContents.add(tbStart);

            result.push(<div key={subjectTbDiagnosisId}>{tbStart}</div>);
          }
        }
      }

      return result;
    },
    [items.diagnosis]
  );

  const generateBody = useCallback(
    () => {
      return items.body.map(item => {
        const { subjectBodyId, visitDateTime, weight, height, bmi } = item;

        const contents = [];

        contents.push(`Weight-${weight}kg`);
        contents.push(`Height-${height}cm`);
        contents.push(`BMI-${bmi}`);

        return (
          <div key={subjectBodyId}>
            {helpers.util.dateformat(visitDateTime, format.YYYY_MM_DD)} :{' '}
            {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [items.body]
  );

  const generateAfb = useCallback(
    () => {
      return items.afb.map(item => {
        const {
          subjectAfbid,
          afbDtc,
          afbSpeccodeId,
          afbSpecoth,
          afbStainResultcodeId,
          afbCultureSolidcodeId,
          afbCultureLiquidcodeId
        } = item;

        const contents = [];

        contents.push(
          getCodeName(
            afbSpecCodes,
            afbSpeccodeId,
            code.AFBSpecOther,
            afbSpecoth
          )
        );
        contents.push(getCodeName(afbStainResultCodes, afbStainResultcodeId));
        contents.push(getCodeName(afbCultureSolidCodes, afbCultureSolidcodeId));
        contents.push(
          getCodeName(afbCultureLiquidCodes, afbCultureLiquidcodeId)
        );

        return (
          <div key={subjectAfbid}>
            {helpers.util.dateformat(afbDtc, format.YYYY_MM_DD)} :{' '}
            {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [
      items.afb,
      afbSpecCodes,
      afbStainResultCodes,
      afbCultureSolidCodes,
      afbCultureLiquidCodes
    ]
  );

  const generatePCR = useCallback(
    () => {
      return items.pcr.map(item => {
        const {
          subjectPcrtestId,
          pcrDtc,
          pcrSprcCodeId,
          pcrOth,
          pcrResultCodeId
        } = item;

        const contents = [];
        contents.push(
          getCodeName(afbSpecCodes, pcrSprcCodeId, code.AFBSpecOther, pcrOth)
        );
        contents.push(getCodeName(pcrResultCodes, pcrResultCodeId));

        return (
          <div key={subjectPcrtestId}>
            {helpers.util.dateformat(pcrDtc, format.YYYY_MM_DD)} :{' '}
            {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [items.pcr, afbSpecCodes, pcrResultCodes]
  );

  const generateLBCH = useCallback(
    () => {
      return items.lbch.map(item => {
        const { subjectLbchid, lbdtc, cr, ast, alt } = item;

        const contents = [];
        contents.push(`CR-${cr || ''}`);
        contents.push(`AST-${ast || ''}`);
        contents.push(`ALT-${alt || ''}`);

        return (
          <div key={subjectLbchid}>
            {lbdtc} : {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [items.lbch]
  );

  const generateLBHEM = useCallback(
    () => {
      return items.lbhem.map(item => {
        const { subjectLbhemid, lbdtc, eos, hct } = item;

        const contents = [];
        contents.push(`EOS-${eos || ''}`);
        contents.push(`HCT-${hct || ''}`);

        return (
          <div key={subjectLbhemid}>
            {lbdtc} : {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [items.lbhem]
  );

  const generateComorbidity = useCallback(
    () => {
      return items.comorbid.map(item => {
        const {
          subjectComorbidId,
          comorbidDate,
          comorbidcodeId,
          comorbidoth
        } = item;

        const contents = [];
        contents.push(
          getCodeName(
            comorbidCodes,
            comorbidcodeId,
            code.ComorbidityOther,
            comorbidoth
          )
        );

        return (
          <div key={subjectComorbidId}>
            {comorbidDate} : {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [items.comorbid, comorbidCodes]
  );

  const generateCM = useCallback(
    () => {
      return items.cm.map(item => {
        const {
          subjectCmdrugId,
          cmtrt,
          codrname,
          cmdose,
          cmdosu,
          cmdosfrqcodeId,
          cmdosfrqoth,
          cmroutecodeId,
          cmrouteoth,
          cmrouthinf,
          cmdosfrmcodeId,
          cmdosfrmoth,
          cmstdtc,
          cmendtc
        } = item;

        const contents = [];
        contents.push(codrname);
        contents.push(`${cmdose} ${cmdosu}`);
        contents.push(
          getCodeName(
            dosfrqCodes,
            cmdosfrqcodeId,
            code.DosfrqOther,
            cmdosfrqoth
          )
        );
        contents.push(
          getCodeName(routeCodes, cmroutecodeId, code.RouteOther, cmrouteoth)
        );
        contents.push(cmrouthinf);
        contents.push(
          getCodeName(
            dosfrmCodes,
            cmdosfrmcodeId,
            code.DosfrmOther,
            cmdosfrmoth
          )
        );
        contents.push(getDateRange(cmstdtc, cmendtc));

        return (
          <div key={subjectCmdrugId}>
            {cmtrt} : {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [items.cm, dosfrqCodes, routeCodes, dosfrmCodes]
  );

  const generateSTbd = useCallback(
    () => {
      return items.sTbd.map(item => {
        const {
          subjectTbdrugId,
          exdrugid,
          exdrugoth,
          // genericName,
          extrt,
          exdose,
          exdosfrqcodeId,
          exdosfrqoth,
          exdosfrmcodeId,
          exdosfrmoth,
          exrouthinf,
          exroutecodeId,
          exrouthoth,
          exstdtc,
          exendtc
        } = item;

        const contents = [];
        contents.push(extrt);
        contents.push(!!exdose ? `${exdose} mg` : '');
        contents.push(
          getCodeName(
            dosfrqCodes,
            exdosfrqcodeId,
            code.DosfrqOther,
            exdosfrqoth
          )
        );
        contents.push(
          getCodeName(
            dosfrmCodes,
            exdosfrmcodeId,
            code.DosfrqOther,
            exdosfrmoth
          )
        );
        contents.push(exrouthinf);
        contents.push(
          getCodeName(routeCodes, exroutecodeId, code.RouteOther, exrouthoth)
        );
        contents.push(getDateRange(exstdtc, exendtc));

        return (
          <div key={subjectTbdrugId}>
            {getCodeName(tbdrugs, exdrugid, code.TBDrugOther, exdrugoth)} :{' '}
            {contents.filter(text => !!text).join(', ')}
          </div>
        );
      });
    },
    [items.sTbd, tbdrugs, dosfrqCodes, dosfrmCodes, routeCodes]
  );

  return (
    <Container fluid>
      {loading && <EmptyRowsView loading={loading} />}
      <RootGrid columns={2} divided stackable>
        {items.length !== 0 && (
          <Fragment>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>Diagnostics</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateDiagnostics()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>
                  {t('activity.diagnosis.tbTreatment')}
                </HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateTBTreatment()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>{t('tbStart')}</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateTBStart()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>Physical Measurement</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateBody()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>AFB</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateAfb()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>PCR</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generatePCR()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>LBCH</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateLBCH()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>LBHEM</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateLBHEM()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>Comorbidity</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateComorbidity()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>Co-Administrated Drug</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateCM()}</Contents>
              </Grid.Column>
            </GridRow>
            <GridRow>
              <HeaderColumn width={gridWidth.title}>
                <HeaderContent>Anti-TB Medication</HeaderContent>
              </HeaderColumn>
              <Grid.Column width={gridWidth.contents}>
                <Contents>{generateSTbd()}</Contents>
              </Grid.Column>
            </GridRow>
          </Fragment>
        )}
      </RootGrid>
    </Container>
  );
};

export default CRSummary;
