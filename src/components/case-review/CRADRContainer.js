import React, { Fragment } from 'react';
import CRADR from './CRADR';
import { useCodes } from '../../hooks';
import { codeGroup } from '../../constants';

const CRADRContainer = ({ subjectId }) => {
  const [adrSeverityCodes] = useCodes({
    codeGroupId: codeGroup.AdrSeverityCode
  });
  const [adrconCodes] = useCodes({ codeGroupId: codeGroup.AdrconCode });

  if (adrSeverityCodes.length > 0 && adrconCodes.length > 0) {
    return (
      <CRADR
        subjectId={subjectId}
        adrSeverityCodes={adrSeverityCodes}
        adrconCodes={adrconCodes}
      />
    );
  } else return <Fragment />;
};

export default CRADRContainer;
