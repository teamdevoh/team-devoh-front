import helpers from '../../helpers';

const fetchSubjectCR = subjectId => {
  return helpers.Service.get(`api/subjects/${subjectId}/cr`);
};

const fetchActivitiesCR = subjectId => {
  return helpers.Service.get(`api/subjects/${subjectId}/activities/cr`);
};

const fetchCRMICResultsForCR = subjectId => {
  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/cr/micresults`
  );
};

const fetchReportsForTDM = subjectId => {
  return helpers.Service.get(
    `/api/subjects/${subjectId}/activities/cr/reports/tdm`
  );
};

const fetchReportsForMIC = subjectId => {
  return helpers.Service.get(
    `/api/subjects/${subjectId}/activities/cr/reports/mic`
  );
};

const fetchOtherTestsForCR = subjectId => {
  return helpers.Service.get(`/api/subjects/${subjectId}/activities/cr/tests`);
};

const fetchSummaryForCR = subjectId => {
  return helpers.Service.get(`api/subjects/${subjectId}/activities/cr/summary`);
};

const fetchMyDrugs = subjectId => {
  return helpers.Service.get(`api/subjects/${subjectId}/drugs`);
};

const fetchSubjectDrugDosageForCR = exdrugId => {
  return helpers.Service.get(`api/subjects/drugs/${exdrugId}`);
};

const fetchSubjectDrugDosageOrigin = exdrugId => {
  return helpers.Service.get(`api/subjects/drugs.origin/${exdrugId}`);
};

const fetchTbdrugPrediction = () => {
  return helpers.Service.get(`api/drugs/prediction`);
};

export default {
  fetchSubjectCR,
  fetchActivitiesCR,
  fetchCRMICResultsForCR,
  fetchReportsForTDM,
  fetchReportsForMIC,
  fetchOtherTestsForCR,
  fetchSummaryForCR,
  fetchMyDrugs,
  fetchSubjectDrugDosageForCR,
  fetchSubjectDrugDosageOrigin,
  fetchTbdrugPrediction
};
