import React, { Fragment, useEffect, useState } from 'react';
import { Grid, Loader } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import { useCodes, useFormatMessage } from '../../hooks';
import { useBuild } from '../activity/hooks';
import AdjustChart from './AdjustChart';
import { useSubjectExdose } from './hooks';
import OriginalChart from './OriginalChart';

const TDMDosageWrapper = ({
  subjectId,
  exDrugId,
  loading,
  source,
  predictItems = [],
  normalizedDose
}) => {
  const t = useFormatMessage();
  const build = useBuild();
  const [dosfrqCodes] = useCodes({ codeGroupId: codeGroup.DosfrqCode });
  const { data, exdosfrqcodeId } = useSubjectExdose(
    subjectId,
    exDrugId,
    source,
    predictItems,
    normalizedDose
  );
  const [exdosfrq, setExdosfrq] = useState('');

  useEffect(
    () => {
      const newExdosfrq = build.displayName({
        source: dosfrqCodes,
        value: exdosfrqcodeId
      });
      setExdosfrq(newExdosfrq);
    },
    [exdosfrqcodeId, dosfrqCodes]
  );

  return (
    <Fragment>
      {data.map((item, index) => {
        return (
          <Fragment key={index}>
            <Grid.Column>
              <Loader active={loading} />
              <OriginalChart
                title={`${t('cr.tdm.scatter.same.dose', {
                  dose: item.name
                })}${!!exdosfrq ? `, ${exdosfrq}` : ''}`}
                subjectId={subjectId}
                exDrugId={exDrugId}
                data={item.data}
              />
            </Grid.Column>
            <Grid.Column>
              <Loader active={loading} />
              <AdjustChart
                title={`${t('cr.tdm.scatter.same.dose', {
                  dose: item.name
                })}${!!exdosfrq ? `, ${exdosfrq}` : ''}`}
                subjectId={subjectId}
                exDrugId={exDrugId}
                data={item.data}
              />
            </Grid.Column>
          </Fragment>
        );
      })}
    </Fragment>
  );
};

export default TDMDosageWrapper;
