import React, { useCallback } from 'react';
import { Button } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import helpers from '../../helpers';
import { useBoolean, useCodes, useFormatMessage } from '../../hooks';
import { useBuild } from '../activity/hooks';
import useSubjectDrugOrigin from './hooks/useSubjectDrugOrigin';

const CRScatterExportCSV = ({ exdrugId }) => {
  const t = useFormatMessage();
  const loading = useBoolean(false);
  const subjectDrugOrigin = useSubjectDrugOrigin();
  const build = useBuild();

  const [dosfrqCodes] = useCodes({ codeGroupId: codeGroup.DosfrqCode });
  const [dosfrmCodes] = useCodes({ codeGroupId: codeGroup.DosfrmCode });
  const [routeCodes] = useCodes({ codeGroupId: codeGroup.RouteCode });
  const [fastFedCodeCodes] = useCodes({
    codeGroupId: codeGroup.FastFedCode
  });

  const handleClick = useCallback(
    async () => {
      loading.setTrue();
      try {
        const data = await subjectDrugOrigin.fetchItems(exdrugId);

        if (data.length > 0) {
          let shortName = data[0].shortName;

          data.forEach(item => {
            const visitOrdinal = helpers.util.getOrdinal(item.visitOrder);
            item.visitName = `${visitOrdinal} ${item.visitTypeName}`;

            const {
              exdosfrqcodeId,
              exdosfrmcodeId,
              exroutecodeId,
              fastFedCodeId
            } = item;

            item.exdosfrqcodeId = build.displayName({
              source: dosfrqCodes,
              value: exdosfrqcodeId
            });

            item.exdosfrmcodeId = build.displayName({
              source: dosfrmCodes,
              value: exdosfrmcodeId
            });

            item.exroutecodeId = build.displayName({
              source: routeCodes,
              value: exroutecodeId
            });

            item.fastFedCodeId = build.displayName({
              source: fastFedCodeCodes,
              value: fastFedCodeId
            });
          });

          helpers.util.exportCSV({
            fileName: `TDM Concentration ${shortName}.csv`,
            data: data,
            fields: [
              { key: 'subjectNo' },
              { key: 'visitName' },
              { key: 'shortName' },
              { key: 'exdose' },
              { key: 'exdosfrqcodeId' },
              { key: 'exdosfrqoth' },
              { key: 'exdosfrmcodeId' },
              { key: 'exdosfrmoth' },
              { key: 'fdc' },
              { key: 'exroutecodeId' },
              { key: 'exrouthoth' },
              { key: 'exstdtc' },
              { key: 'exendtc' },
              { key: 'fastFedCodeId' },
              { key: 'endmealDatetime' },
              { key: 'medDatetime' },
              { key: 'sampleTime' },
              { key: 'tad' },
              { key: 'drugConc' },
              { key: 'drugConcUnit' }
            ],
            fieldNames: [
              t('collection.subject.no'),
              t('activity.visit'),
              t('activity.subtbdrug.exdrugid'),
              `${t('activity.common.exdose')}(mg)`,
              t('activity.subtbdrug.exdosfrqcodeId'),
              t('activity.subtbdrug.exdosfrqoth'),
              t('activity.subtbdrug.exdosfrmcodeId'),
              t('activity.subtbdrug.exdosfrmoth'),
              t('activity.subtbdrug.fdc'),
              t('activity.subtbdrug.exroutecodeId'),
              t('activity.subtbdrug.exrouthoth'),
              t('activity.subtbdrug.exstdtc'),
              t('activity.subtbdrug.exendtc'),
              t('activity.tdmdrug.fastFedCodeId'),
              t('activity.tdmdrug.endmealDatetime'),
              t('activity.tdmconcentration.medDatetime'),
              t('activity.tdmconcentration.sampleTime'),
              `${t('activity.tdmconcentration.TAD')}(hr)`,
              t('activity.tdmconcentration.drugConc'),
              t('activity.tdmconcentration.drugConcUnit')
            ]
          });
        }
      } catch (error) {
      } finally {
        loading.setFalse();
      }
    },
    [exdrugId, dosfrqCodes, dosfrmCodes, routeCodes, fastFedCodeCodes]
  );

  return (
    <Button
      positive
      icon="download"
      content={t('common.export')}
      loading={loading.value}
      disabled={loading.value}
      onClick={handleClick}
    />
  );
};

export default CRScatterExportCSV;
