import React, { useEffect, useState } from 'react';
import { useInView } from 'react-intersection-observer';
import { Header, Segment } from 'semantic-ui-react';
import CRTDMChart from './CRTDMChart';

const OriginalChart = ({
  subjectId,
  exDrugId,
  title,
  data,
  showAlways = false
}) => {
  const [preExDrugId] = useState(exDrugId);
  const [triggerOnce, setTriggerOnce] = useState(true);
  const [ref, inView] = useInView({
    root: null,
    rootMargin: '200px',
    threshold: 0,
    triggerOnce: triggerOnce
  });

  useEffect(
    () => {
      if (inView) {
        setTriggerOnce(true);
      }
    },
    [inView]
  );

  useEffect(
    () => {
      if (preExDrugId !== exDrugId) {
        setTriggerOnce(false);
      }
    },
    [exDrugId]
  );

  return (
    <Segment basic>
      <Header as="h4">{title ? title : '\u00A0'}</Header>
      <div ref={ref}>
        {(showAlways || inView) && (
          <CRTDMChart subjectId={subjectId} series={data} />
        )}
      </div>
    </Segment>
  );
};

export default OriginalChart;
