import React from 'react';
import CRTDM from './CRTDM';
import { useVisitListOfSubjectWithDate } from '../collection/hooks';
import { useTbdrugList } from '../collection-tbdrug/hooks';
import { Placeholder } from 'semantic-ui-react';

const CRTDMContainer = ({ subjectId }) => {
  const [visit] = useVisitListOfSubjectWithDate({
    subjectId,
    showShortDate: true
  });
  const [drug] = useTbdrugList({ showOnlyShortName: false });

  if (visit.items.length > 0 && drug.items.length > 0) {
    return <CRTDM subjectId={subjectId} visit={visit} drug={drug} />;
  } else {
    return (
      <Placeholder>
        <Placeholder.Line length="full" />
        <Placeholder.Line length="very long" />
        <Placeholder.Line length="long" />
        <Placeholder.Line length="medium" />
        <Placeholder.Line length="short" />
        <Placeholder.Line length="very short" />
        <Placeholder.Line length="medium" />
        <Placeholder.Line length="short" />
        <Placeholder.Line length="very short" />
      </Placeholder>
    );
  }
};

export default CRTDMContainer;
