import React from 'react';
import { useADRForCR } from './hooks';
import { useGetProjectActivityId } from '../collection/hooks';
import { useFormatMessage } from '../../hooks';
import { useBuild } from '../activity/hooks';
import orderBy from 'lodash/orderBy';
import { activity, activityGroup } from '../../constants';
import { Table } from 'semantic-ui-react';
import { TableWrap, StyledTh } from './styles';
import { EmptyRowsViewForSemantic } from '../modules';

const CRADR = ({ subjectId, adrSeverityCodes, adrconCodes }) => {
  const t = useFormatMessage();
  const build = useBuild();
  const adr = useGetProjectActivityId({
    activityGroupId: activityGroup.ADR,
    subjectId,
    activityId: activity.ADR
  });

  const { items, loading } = useADRForCR({
    subjectId,
    projectActivityId: adr.id
  });

  const orderedItems = orderBy(items, 'adrOnsetDat', 'asc');

  return (
    <TableWrap>
      <Table celled selectable>
        <Table.Header>
          <Table.Row>
            <StyledTh singleLine>No</StyledTh>
            <StyledTh>{t('activity.adr.adrName')}</StyledTh>
            <StyledTh>{t('activity.adr.adrOnsetDat')}</StyledTh>
            <StyledTh>{t('activity.adr.adrOffsetDat')}</StyledTh>
            <StyledTh>{t('activity.adr.adrSeverityCodeId')}</StyledTh>
            <StyledTh>{t('activity.adr.adrdrug')}</StyledTh>
            <StyledTh>{t('activity.adr.adrconCodeId')}</StyledTh>
            <StyledTh>{t('activity.adr.aedecod')}</StyledTh>
            <StyledTh>{t('activity.adr.aesoc')}</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {orderedItems.map((item, index) => {
            return (
              <Table.Row key={item.subjectAdrid}>
                <Table.Cell>{index + 1}</Table.Cell>
                <Table.Cell>{item.adrName}</Table.Cell>
                <Table.Cell>{item.adrOnsetDat}</Table.Cell>
                <Table.Cell>{item.adrOffsetDat}</Table.Cell>
                <Table.Cell>
                  {build.displayName({
                    source: adrSeverityCodes,
                    value: item.adrSeverityCodeId
                  })}
                </Table.Cell>
                <Table.Cell>{item.adrdrug}</Table.Cell>
                <Table.Cell>
                  {build.displayName({
                    source: adrconCodes,
                    value: item.adrconCodeId
                  })}
                </Table.Cell>
                <Table.Cell>{item.aedecod}</Table.Cell>
                <Table.Cell>{item.aesoc}</Table.Cell>
              </Table.Row>
            );
          })}
          {orderedItems.length === 0 && (
            <EmptyRowsViewForSemantic loading={loading} columnLength={9} />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default CRADR;
