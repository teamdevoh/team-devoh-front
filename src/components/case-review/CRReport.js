import React from 'react';
import FileList from '../modules/FileList';
import { useReportsForTDM } from './hooks';
import { useCallback } from 'react';
import { Header, Container } from 'semantic-ui-react';
import useReportsForMIC from './hooks/useReportsForMIC';

const CRTDMReport = ({ subjectId, download }) => {
  const tdm = useReportsForTDM({ subjectId });
  const mic = useReportsForMIC({ subjectId });

  const handleTDMDownload = useCallback(
    key => {
      const foundItem = _.find(tdm.items, { key: key });
      download({
        activityKeyId: foundItem.activityKeyId,
        activityAttachmentId: key
      });
    },
    [tdm.items]
  );

  const handleMICDownload = useCallback(
    key => {
      const foundItem = _.find(mic.items, { key: key });
      download({
        activityKeyId: foundItem.activityKeyId,
        activityAttachmentId: key
      });
    },
    [mic.items]
  );

  return (
    <Container text>
      <Header dividing>Therapeutic Drug Monitoring Report</Header>
      <FileList files={tdm.items} onDownloadClick={handleTDMDownload} />
      <Header dividing>MIC Report</Header>
      <FileList files={mic.items} onDownloadClick={handleMICDownload} />
    </Container>
  );
};

export default CRTDMReport;
