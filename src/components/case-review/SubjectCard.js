import React from 'react';
import { Card, Image, Header } from 'semantic-ui-react';
import { useFakeFace } from './hooks';

const SubjectCard = ({ item, loading }) => {
  const fakeFace = useFakeFace();
  const fakeImg = fakeFace.get(item.sexCodeId);

  return (
    <Card fluid>
      <Card.Content>
        <Image floated="right" size="mini" src={`images/${fakeImg}`} />
        <Card.Header>
          {item.subjectNo} / {item.initial}
        </Card.Header>
        <Card.Meta>{item.siteName}</Card.Meta>
        <Card.Description>
          <Header sub dividing>
            Investigator
          </Header>
          <div style={{ textAlign: 'right' }}>{item.docName}</div>
          <Header sub dividing>
            ICF
          </Header>
          <div style={{ textAlign: 'right' }}>{item.consdtc}</div>
          <Header sub dividing>
            Gender
          </Header>
          <div style={{ textAlign: 'right' }}>{item.sexCodeName}</div>
          <Header sub dividing>
            DOB
          </Header>
          <div style={{ textAlign: 'right' }}>{item.brthdtc}</div>
          <Header sub dividing>
            AGE
          </Header>
          <div style={{ textAlign: 'right' }}>{item.age}</div>
          <Header sub dividing>
            SMOKING
          </Header>
          <div style={{ textAlign: 'right' }}>
            {item.smokeName} {item.smokeDurName} {item.exSmoker}{' '}
            {item.currentSmoker}
          </div>
          <Header sub dividing>
            NAT2
          </Header>
          <div style={{ textAlign: 'right' }}>
            <div style={{ color: 'blue' }}>{item.nat2GenRes}</div>
            <div style={{ color: 'blue' }}>{item.nat2GenResOther}</div>
            <div>{item.nat2PhenRes}</div>
          </div>
          <Header sub dividing>
            SLCO1B1
          </Header>
          <div style={{ textAlign: 'right' }}>
            <div style={{ color: 'blue' }}>{item.slco1B1GenRes}</div>
            <div style={{ color: 'blue' }}>{item.slco1b1GenResOther}</div>
            <div>{item.slco1B1PhenRes}</div>
          </div>
          <Header sub dividing>
            Method
          </Header>
          <div style={{ textAlign: 'right' }}>
            <div>{item.genotypeMethod}</div>
          </div>
          {item.caseConclusion !== '' && (
            <React.Fragment>
              <Header sub dividing>
                Case Conclusion
              </Header>
              <div style={{ textAlign: 'right' }}>
                <div>
                  {item.caseConclusion}, {item.condtc}, {item.digFin}
                </div>
              </div>
            </React.Fragment>
          )}
        </Card.Description>
      </Card.Content>
      {/* <Card.Content extra>
        <div className="ui two buttons">
          <Button basic color="green">
            Approve
          </Button>
          <Button basic color="red">
            Decline
          </Button>
        </div>
      </Card.Content> */}
    </Card>
  );
};

export default SubjectCard;
