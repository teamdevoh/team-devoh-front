import React, { Fragment } from 'react';
import { useGetProjectActivityId } from '../collection/hooks';
import orderBy from 'lodash/orderBy';
import find from 'lodash/find';
import { useBoolean, useFormatMessage } from '../../hooks';
import CRLabRangeLabel from './CRLabRangeLabel';
import { useLBHEMForCR } from './hooks';
import { useLabRange } from '../activity/hooks';
import { Checkbox, Loader, Table } from 'semantic-ui-react';
import { HighSpan, LowSpan } from '../activity/lbch/Range';
import { useSelector } from 'react-redux';
import { subject } from '../../states';
import { activity, activityGroup } from '../../constants';
import { TableWrap, StyledTh } from './styles';
import { EmptyRowsViewForSemantic } from '../modules';

const CRLBHEM = ({ subjectId, labRange }) => {
  const [labRangeLBHEM] = useLabRange({
    subjectId,
    lbType: activity.LabType.LBHEM
  });
  const showTEP = useBoolean(true);
  const t = useFormatMessage();
  const lbhem = useGetProjectActivityId({
    activityGroupId: activityGroup.ADR,
    subjectId,
    activityId: activity.LBHEM
  });

  const { items, loading } = useLBHEMForCR({
    subjectId,
    projectActivityId: lbhem.id
  });

  let progressItems = [];

  const timeEvent = useSelector(subject.selectors.getCRItemsWithFilter);

  const timeEventGroups = _.groupBy(timeEvent.items, item => {
    return [item.activityId, item.originStartDate, item.originEndDate];
  });

  Object.keys(timeEventGroups).forEach(key => {
    const group = timeEventGroups[key];
    let summary = '';

    if (group.length > 1) {
      group.forEach(item => {
        summary += item.summary + '\n';
      });
    }

    const item = group[0];
    progressItems.push({
      key: item.key,
      activityName: item.activityName,
      originStartDate: item.originStartDate,
      originEndDate: item.originEndDate,
      startDate: item.startDate,
      endDate: item.endDate,
      summary: summary ? summary : item.summary,
      isLab: false
    });
  });

  items.forEach(item => {
    progressItems.push({
      key: `${item.activityId}_${item.subjectLbhemid}`,
      activityName: activity.LabType.LBHEM,
      originStartDate: item.lbdtc,
      originEndDate: item.lbdtc,
      startDate: item.lbdtc,
      endDate: item.lbdtc,
      summary: '',
      isLab: true,
      wbc: item.wbc,
      rbc: item.rbc,
      hgb: item.hgb,
      hct: item.hct,
      plt: item.plt,
      neu: item.neu,
      lym: item.lym,
      mon: item.mon,
      eos: item.eos,
      bas: item.bas,
      anc: item.anc
    });
  });

  const filteredItems = showTEP.value
    ? progressItems
    : _.filter(progressItems, { isLab: true });

  const orderedItems = orderBy(filteredItems, ['startDate'], ['asc']);

  const lbhemItems = activity.LabLBHEMItems;

  const wbc = find(labRangeLBHEM.ranges, { item: lbhemItems.WBC });
  const rbc = find(labRangeLBHEM.ranges, { item: lbhemItems.RBC });
  const hgb = find(labRangeLBHEM.ranges, { item: lbhemItems.HGB });
  const hct = find(labRangeLBHEM.ranges, { item: lbhemItems.HCT });
  const plt = find(labRangeLBHEM.ranges, { item: lbhemItems.PLT });
  const neu = find(labRangeLBHEM.ranges, { item: lbhemItems.NEU });
  const lym = find(labRangeLBHEM.ranges, { item: lbhemItems.LYM });
  const mon = find(labRangeLBHEM.ranges, { item: lbhemItems.MON });
  const eos = find(labRangeLBHEM.ranges, { item: lbhemItems.EOS });
  const bas = find(labRangeLBHEM.ranges, { item: lbhemItems.BAS });
  const anc = find(labRangeLBHEM.ranges, { item: lbhemItems.ANC });

  return (
    <Fragment>
      <Loader active={loading} />
      <Checkbox
        toggle
        label="Time Event Progress"
        checked={showTEP.value}
        onChange={showTEP.toggle}
      />
      <TableWrap>
        <Table fixed celled singleLine selectable>
          <Table.Header>
            <Table.Row>
              <StyledTh style={{ width: '200px' }}>Date</StyledTh>
              <StyledTh width="two">{t('activity')}</StyledTh>
              <StyledTh width="three">Item</StyledTh>
              <StyledTh title="WBC">
                WBC
                <br />(<LowSpan text={wbc.low} /> ~ <HighSpan text={wbc.high} />
                )
              </StyledTh>
              <StyledTh title="RBC">
                RBC
                <br />(<LowSpan text={rbc.low} /> ~ <HighSpan text={rbc.high} />
                )
              </StyledTh>
              <StyledTh title="Hemoglobin">
                Hemoglobin
                <br />(<LowSpan text={hgb.low} /> ~ <HighSpan text={hgb.high} />
                )
              </StyledTh>
              <StyledTh title="Hematocrit">
                Hematocrit
                <br />(<LowSpan text={hct.low} /> ~ <HighSpan text={hct.high} />
                )
              </StyledTh>
              <StyledTh title="Platelet">
                Platelet
                <br />(<LowSpan text={plt.low} /> ~ <HighSpan text={plt.high} />
                )
              </StyledTh>
              <StyledTh title="Neutrophil">
                Neutrophil
                <br />(<LowSpan text={neu.low} /> ~ <HighSpan text={neu.high} />
                )
              </StyledTh>
              <StyledTh title="Lymphocyte">
                Lymphocyte
                <br />(<LowSpan text={lym.low} /> ~ <HighSpan text={lym.high} />
                )
              </StyledTh>
              <StyledTh title="Monocyte">
                Monocyte
                <br />(<LowSpan text={mon.low} /> ~ <HighSpan text={mon.high} />
                )
              </StyledTh>
              <StyledTh title="Eosinophil">
                Eosinophil
                <br />(<LowSpan text={eos.low} /> ~ <HighSpan text={eos.high} />
                )
              </StyledTh>
              <StyledTh title="Basophil">
                Basophil
                <br />(<LowSpan text={bas.low} /> ~ <HighSpan text={bas.high} />
                )
              </StyledTh>
              <StyledTh title="Absolute neutrophil count (ANC)">
                Absolute neutrophil count (ANC)
                <br />(<LowSpan text={anc.low} /> ~ <HighSpan text={anc.high} />
                )
              </StyledTh>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {orderedItems.map(item => {
              const summary = item.summary.replace(/\|\|/g, ' ');
              return (
                <Table.Row
                  key={item.key}
                  style={{
                    fontStyle: item.isLab ? 'normal' : 'italic',
                    fontWeight: item.isLab ? 'bold' : 'lighter'
                  }}
                >
                  <Table.Cell>
                    {item.originStartDate === item.originEndDate
                      ? item.originStartDate
                      : `${item.originStartDate} ~ ${item.originEndDate}`}
                  </Table.Cell>
                  <Table.Cell>{item.activityName}</Table.Cell>
                  <Table.Cell title={summary}>
                    {summary.split('\n').map((value, index) => {
                      return <div key={index}>{value}</div>;
                    })}
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={wbc.low}
                      max={wbc.high}
                      value={item.wbc}
                      unit={wbc.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={rbc.low}
                      max={rbc.high}
                      value={item.rbc}
                      unit={rbc.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={hgb.low}
                      max={hgb.high}
                      value={item.hgb}
                      unit={hgb.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={hct.low}
                      max={hct.high}
                      value={item.hct}
                      unit={hct.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={plt.low}
                      max={plt.high}
                      value={item.plt}
                      unit={plt.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={neu.low}
                      max={neu.high}
                      value={item.neu}
                      unit={neu.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={lym.low}
                      max={lym.high}
                      value={item.lym}
                      unit={lym.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={mon.low}
                      max={mon.high}
                      value={item.mon}
                      unit={mon.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={eos.low}
                      max={eos.high}
                      value={item.eos}
                      unit={eos.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={bas.low}
                      max={bas.high}
                      value={item.bas}
                      unit={bas.unit}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CRLabRangeLabel
                      min={anc.low}
                      max={anc.high}
                      value={item.anc}
                      unit={anc.unit}
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
            {orderedItems.length === 0 && (
              <EmptyRowsViewForSemantic loading={loading} columnLength={14} />
            )}
          </Table.Body>
        </Table>
      </TableWrap>
    </Fragment>
  );
};

export default CRLBHEM;
