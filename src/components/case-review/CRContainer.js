import React, { useCallback, useEffect, useState } from 'react';
import CRTitle from './CRTitle';
import SubjectCard from './SubjectCard';
import { Dropdown, Form, Grid, Header } from 'semantic-ui-react';
import CRProgressDiary from './CRProgressDiary';
import CRAnnotatedWrapper from './CRAnnotatedWrapper';
import { useHistory, useParams } from 'react-router-dom';
import { useSubjectCR, useTimeEventDispatch } from './hooks';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import { Subjects } from '../collection/Selector';
import { useFormatMessage } from '../../hooks';

const CRContainer = () => {
  const t = useFormatMessage();
  const params = useParams();
  const history = useHistory();
  const [sites] = useMyProjectSiteList({ projectId: params.projectId });
  const [selectedProjectSiteId, setSelectedProjectSiteId] = useState(0);
  const subjectCR = useSubjectCR(params.subjectId);
  useTimeEventDispatch(params.subjectId);

  const handleProjectSiteChange = useCallback((event, data) => {
    setSelectedProjectSiteId(data.value);
  }, []);

  const handleSubjectChange = useCallback((event, data) => {
    history.push(
      `/project/${params.projectId}/subject/${data.value}/casereview`
    );
  }, []);

  useEffect(
    () => {
      if (sites.length > 0 && subjectCR.item.subjectId > 0) {
        const currentSite = _.find(sites, { text: subjectCR.item.siteName });
        setSelectedProjectSiteId(currentSite.value);
      }
    },
    [sites, subjectCR.item]
  );

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column>
          <CRTitle />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="sixteen">
          <Form>
            <Form.Group>
              <Form.Field width="three">
                <label>{t('site')}</label>
                <Dropdown
                  fluid
                  search
                  selection
                  options={sites}
                  value={selectedProjectSiteId}
                  onChange={handleProjectSiteChange}
                />
              </Form.Field>
              <Form.Field width="three">
                <label>{t('subject')}</label>
                <Subjects
                  fluid
                  projectSiteId={selectedProjectSiteId}
                  value={Number(params.subjectId)}
                  onChange={handleSubjectChange}
                />
              </Form.Field>
            </Form.Group>
          </Form>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="three">
          <Header as="h3">{t('subject')}</Header>
          <SubjectCard {...subjectCR} />
        </Grid.Column>
        <Grid.Column width="thirteen">
          <CRProgressDiary
            projectId={params.projectId}
            subjectId={params.subjectId}
            projectSiteId={selectedProjectSiteId}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="sixteen">
          <CRAnnotatedWrapper />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default CRContainer;
