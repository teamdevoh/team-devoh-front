import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useTbdrugPredictionList = () => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    const res = await api.fetchTbdrugPrediction();

    if (res && res.data) {
      setItems(res.data);
    }
    setLoading(false);
  });

  useEffect(() => {
    fetchItems();
  }, []);

  return [{ items, loading }];
};

export default useTbdrugPredictionList;
