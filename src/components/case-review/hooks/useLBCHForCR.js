import { useEffect, useState } from 'react';
import api from '../../activity/api';

const useLBCHForCR = ({ subjectId, projectActivityId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    setLoading(true);
    try {
      const res = await api.fetchLBCHs({ subjectId, projectActivityId });
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, projectActivityId]
  );
  return { items, loading };
};

export default useLBCHForCR;
