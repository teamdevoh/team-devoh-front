import { useMemo } from 'react';
import groupBy from 'lodash/groupBy';
import maxBy from 'lodash/maxBy';

const useDrugConcToNormalize = ({ source, normailzeExDose }) => {
  const getNormalizeExDose = () => {
    const exDoseGroups = groupBy(source, 'exdose');

    const normalizeExDose = maxBy(
      Object.keys(exDoseGroups),
      exDoseGroupKey => exDoseGroups[exDoseGroupKey].length
    );

    return Number(normalizeExDose);
  };

  const replaceToNormailzeDrugConc = normailzeExDose => {
    const newItems = source.map(item => {
      let drugConc = item.drugConc;
      if (item.exdose !== normailzeExDose.toString()) {
        drugConc =
          (Number(item.drugConc) / Number(item.exdose)) * normailzeExDose;
      }

      return {
        ...item,
        drugConc
      };
    });
    // source.forEach(item => {
    //   if (item.exdose !== normailzeExDose.toString()) {
    //     item.drugConc =
    //       (Number(item.drugConc) / Number(item.exdose)) * normailzeExDose;
    //   }
    // });
    return { exdose: normailzeExDose, source: newItems };
  };

  return useMemo(
    () => {
      if (source && source.length > 0) {
        if (!!!normailzeExDose) {
          normailzeExDose = getNormalizeExDose();
        }
        return replaceToNormailzeDrugConc(normailzeExDose);
      }
      return [];
    },
    [source, normailzeExDose]
  );
};

export default useDrugConcToNormalize;
