import { useState } from 'react';
import api from '../api';
import { useEffect } from 'react';

const useSummaryForCR = subjectId => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = async () => {
    setLoading(true);
    try {
      const res = await api.fetchSummaryForCR(subjectId);
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId]
  );

  return [{ items, loading }];
};

export default useSummaryForCR;
