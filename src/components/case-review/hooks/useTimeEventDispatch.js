import { useEffect } from 'react';
import api from '../api';
import { useDispatch } from 'react-redux';
import { subject } from '../../../states';

const useTimeEvent = subjectId => {
  const dispatch = useDispatch();

  const fetchItems = async () => {
    dispatch(subject.actions.isCRLoading(true));
    try {
      const res = await api.fetchActivitiesCR(subjectId);
      if (res && res.data) {
        res.data.forEach(item => {
          item['key'] = `${item.activityId}_${item.activityKeyId}`;
        });
        dispatch(subject.actions.fetchCRList(res.data));
      }
    } catch (error) {
    } finally {
      dispatch(subject.actions.isCRLoading(false));
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId]
  );
};

export default useTimeEvent;
