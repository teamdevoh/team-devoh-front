import { useState } from 'react';
import api from '../api';
import { useEffect } from 'react';
import helpers from '../../../helpers';

const useSubjectCR = subjectId => {
  const [item, setItem] = useState({
    subjectId: '',
    siteName: '',
    subjectNo: '',
    initial: '',
    docName: '',
    consdtc: '',
    sexCodeId: 0,
    sexCodeName: '',
    brthdtc: '',
    age: '',
    smokeName: '',
    smokeDurName: '',
    exSmoker: '',
    currentSmoker: '',
    nat2GenRes: '',
    nat2PhenRes: '',
    slco1B1GenRes: '',
    slco1B1PhenRes: '',
    nat2GenResOther: '',
    slco1b1GenResOther: '',
    genotypeMethod: '',
    caseConclusion: '',
    condtc: '',
    digFin: ''
  });
  const [loading, setLoading] = useState(false);

  const fetchItem = async () => {
    setLoading(true);
    try {
      const res = await api.fetchSubjectCR(subjectId);
      if (res && res.data) {
        setItem(res.data);
      }
      if (res && res.data === null) {
        helpers.history.goBack();
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItem();
    },
    [subjectId]
  );

  return { item, loading };
};

export default useSubjectCR;
