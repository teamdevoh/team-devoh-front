import { useEffect, useState } from 'react';
import api from '../api';

const useDrugSubjects = (subjectId, exdrugId) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchSubjectDrugDosageForCR(exdrugId);
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, exdrugId]
  );

  return { items, loading };
};

export default useDrugSubjects;
