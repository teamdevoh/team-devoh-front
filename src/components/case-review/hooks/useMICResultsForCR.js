import { useState } from 'react';
import api from '../api';
import { useEffect } from 'react';

const fetchCRMICResultsForCR = subjectId => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = async () => {
    setLoading(true);
    try {
      const res = await api.fetchCRMICResultsForCR(subjectId);
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId]
  );

  return { items, loading };
};

export default fetchCRMICResultsForCR;
