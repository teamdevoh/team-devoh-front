const useAdjustTAD = () => {
  const exec = (data = []) => {
    const copiedData = _.cloneDeep(data);
    copiedData.forEach(series => {
      series.data.forEach(axis => {
        let x = axis[0];
        if (x < 0) {
          x += 24;
        }
        if (x > 24) {
          x -= 24;
        }
        axis[0] = x;
      });
    });

    return copiedData;
  };

  return { exec };
};

export default useAdjustTAD;
