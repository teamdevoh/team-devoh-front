import { useMemo } from 'react';
import { useFakeFace } from '.';
import useScatterAxis from './useScatterAxis';
import { chartDataGenerate } from '../CRTDMChartContainer';

const useSubjectExdose = (
  subjectId,
  exdrugId,
  source,
  predictItems = [],
  normalizedDose
) => {
  const axis = useScatterAxis();
  const fakeFace = useFakeFace();

  const generateData = () => {
    const dataSet = [];

    const data = source || [];

    const currentSubject = _.filter(data, { subjectId: subjectId });
    const ordered = _.orderBy(currentSubject, 'exdose', 'asc');
    const exDoseGroups = _.groupBy(ordered, 'exdose');
    const curSubExdosfrqcodeId =
      currentSubject[0] && currentSubject[0].exdosfrqcodeId;

    Object.keys(exDoseGroups).forEach(key => {
      let others = {
        name: 'Except for current subject',
        data: [],
        subjects: [],
        type: 'scatter'
      };

      const exDoseGroup = exDoseGroups[key];

      const my = axis.buildTarget(exDoseGroup);

      data.forEach(item => {
        const xy = [item.tad, Number(item.drugConc)];
        if (
          _.findIndex(exDoseGroup, {
            exdose: item.exdose,
            exdosfrqcodeId: item.exdosfrqcodeId
          }) > -1
        ) {
          others.data.push(xy);
          others.subjects.push({
            subjectId: item.subjectId,
            siteName: item.siteName,
            subjectNo: item.subjectNo,
            img: fakeFace.get(item.sexcodeId),
            initial: item.initial,
            drugConcUnit: item.drugConcUnit
          });
        }
      });

      dataSet.push({ name: key, data: [others, ...my] });

      if (Number(key) === Number(normalizedDose)) {
        const items = predictItems.filter(
          item =>
            Number(item.dose) === Number(normalizedDose) &&
            Number(item.exdosfrqcodeId) === Number(curSubExdosfrqcodeId)
        );

        if (items.length > 0) {
          const predictData = chartDataGenerate(items);
          dataSet[dataSet.length - 1].data.push(predictData);
        }
      }
    });

    return { data: dataSet, exdosfrqcodeId: curSubExdosfrqcodeId };
  };

  return useMemo(() => generateData(), [subjectId, source, predictItems]);
};

export default useSubjectExdose;
