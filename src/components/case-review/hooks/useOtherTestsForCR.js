import api from '../api';
import { useState } from 'react';
import { useEffect } from 'react';

const useOtherTestsForCR = ({ subjectId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    setLoading(true);
    try {
      const res = await api.fetchOtherTestsForCR(subjectId);
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId]
  );

  return { items, loading, setItems };
};

export default useOtherTestsForCR;
