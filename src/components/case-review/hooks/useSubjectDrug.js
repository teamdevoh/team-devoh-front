import { useMemo } from 'react';
import useScatterAxis from './useScatterAxis';
import { chartDataGenerate } from '../CRTDMChartContainer';

const useSubjectDrug = (subjectId, exdrugId, source, predictItems = []) => {
  const axis = useScatterAxis();

  const generateData = () => {
    const data = source ? source : [];

    const currentSubject = _.filter(data, { subjectId: subjectId });

    const my = axis.buildTarget(currentSubject);
    const others = axis.buildOther(data);

    const dataSet = [others, ...my];

    if (predictItems.length > 0) {
      dataSet.push(chartDataGenerate(predictItems));
    }
    return dataSet;
  };

  return useMemo(() => generateData(), [exdrugId, source, predictItems]);
};

export default useSubjectDrug;
