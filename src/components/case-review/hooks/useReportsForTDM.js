import { useState, useEffect } from 'react';
import api from '../api';
import helpers from '../../../helpers';

const useReportsForTDM = ({ subjectId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    setLoading(true);
    try {
      const res = await api.fetchReportsForTDM(subjectId);
      if (res && res.data) {
        let items = [];
        await res.data.forEach(async item => {
          const fileObj = await helpers.file.setFileInfo({
            ...item,
            id: item.activityAttachmentId,
            thumbnailApiUrl: `api/subjects/{subjectId}/activities/{activityKeyId}/attachs.thumbnail/${
              item.activityAttachmentId
            }`
          });
          fileObj.activityKeyId = item.activityKeyId;
          items.push(fileObj);
        });

        setItems(items);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId]
  );

  return { items, loading };
};

export default useReportsForTDM;
