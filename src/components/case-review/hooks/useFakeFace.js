import { code } from '../../../constants';

const useFakeFace = () => {
  const get = sexCodeId => {
    return sexCodeId === code.MALE ? 'steve.jpg' : 'molly.png';
  };

  return { get };
};

export default useFakeFace;
