import { useFakeFace } from '.';
import helpers from '../../../helpers';

const useScatterAxis = () => {
  const fakeFace = useFakeFace();

  const buildTarget = (targets = []) => {
    const visitGroups = _.groupBy(targets, 'subjectVisitId');

    let my = [];

    Object.keys(visitGroups).forEach(key => {
      const visitGroup = visitGroups[key];

      let items = [];
      let subjects = [];

      visitGroup.forEach(item => {
        const xy = [item.tad, Number(item.drugConc)];
        items.push(xy);
        subjects.push({
          subjectId: item.subjectId,
          siteName: item.siteName,
          subjectNo: item.subjectNo,
          img: fakeFace.get(item.sexcodeId),
          initial: item.initial,
          drugConcUnit: item.drugConcUnit,
          exdose: item.exdose
        });
      });

      my.push({
        name: `${helpers.util.getOrdinal(visitGroup[0].visitOrder)}${
          visitGroup[0].visitTypeName
        }`,
        type: 'scatter',
        data: items,
        subjects: subjects
      });
    });

    return my;
  };

  const buildOther = (targets = []) => {
    let other = {
      name: 'Except for current subject',
      type: 'scatter',
      data: [],
      subjects: []
    };

    targets.forEach(item => {
      const xy = [item.tad, Number(item.drugConc)];
      other.data.push(xy);
      other.subjects.push({
        subjectId: item.subjectId,
        siteName: item.siteName,
        subjectNo: item.subjectNo,
        img: fakeFace.get(item.sexcodeId),
        initial: item.initial,
        drugConcUnit: item.drugConcUnit,
        exdose: item.exdose
      });
    });

    return other;
  };

  return { buildTarget, buildOther };
};

export default useScatterAxis;
