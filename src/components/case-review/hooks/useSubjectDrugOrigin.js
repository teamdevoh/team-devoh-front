import { useCallback } from 'react';
import api from '../api';

const useSubjectDrugOrigin = () => {
  const fetchItems = useCallback(async exdrugId => {
    const res = await api.fetchSubjectDrugDosageOrigin(exdrugId);
    if (res && res.data) {
      return _.orderBy(res.data, 'subjectNo', 'asc');
    }
    return [];
  }, []);

  return { fetchItems };
};

export default useSubjectDrugOrigin;
