import { useEffect, useState } from 'react';
import api from '../api';

const useMyDrugs = subjectId => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchMyDrugs(subjectId);
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId]
  );

  return { items, loading };
};

export default useMyDrugs;
