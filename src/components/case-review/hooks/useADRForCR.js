import { useEffect, useState } from 'react';
import api from '../../activity/api';

const useADRForCR = ({ subjectId, projectActivityId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = async () => {
    setLoading(true);
    try {
      const res = await api.fetchADRs({ subjectId, projectActivityId });
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, projectActivityId]
  );

  return { items, loading };
};

export default useADRForCR;
