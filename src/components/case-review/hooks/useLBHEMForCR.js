import api from '../../activity/api';
import { useEffect, useState } from 'react';

const useLBHEMForCR = ({ subjectId, projectActivityId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    setLoading(true);
    try {
      const res = await api.fetchLBHEMs({ subjectId, projectActivityId });
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, projectActivityId]
  );

  return { items, loading };
};

export default useLBHEMForCR;
