import { useEffect, useState, useCallback } from 'react';
import api from '../../activity/api';
import helpers from '../../../helpers';
import { format } from '../../../constants';

const useTDMConcentrationForCR = (
  subjectId,
  firstTDMProjectActivityId,
  fuTDMProjectActivityId
) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(
    async () => {
      setLoading(true);
      try {
        const firstTDM = await api.fetchTdmconcentrationList({
          subjectId,
          projectActivityId: firstTDMProjectActivityId
        });
        const fuTDM = await api.fetchTdmconcentrationList({
          subjectId,
          projectActivityId: fuTDMProjectActivityId
        });

        const orderedItems = _.orderBy(
          [...firstTDM.data, ...fuTDM.data],
          ['subjectVisitId', 'genericName', 'medDatetime'],
          ['asc', 'asc', 'asc']
        );

        setNewItems(orderedItems);
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [subjectId, firstTDMProjectActivityId, fuTDMProjectActivityId]
  );

  const setNewItems = orderedItems => {
    setItems(
      orderedItems.map(item => {
        item.key = `${item.subjectTdmconcentrationId}_${
          item.subjectTdmdrugId
        }_${item.subjectTdmsamplingId}`;

        item.dose = item.exdose ? `${item.exdose} mg` : '';

        item.drugConcValueUnit = item.drugConc
          ? `${item.drugConc} ${item.drugConcUnit}`
          : '';

        item.questionable =
          item.pkQuest === true ? 'Yes' : item.pkQuest === false ? 'No' : '';

        item.resultDate = helpers.util.dateformat(
          item.resultdtc,
          format.YYYY_MM_DD
        );
        return item;
      })
    );
  };

  useEffect(
    () => {
      if (firstTDMProjectActivityId > 0 && fuTDMProjectActivityId > 0) {
        fetchItems();
      }
    },
    [subjectId, firstTDMProjectActivityId, fuTDMProjectActivityId]
  );

  return { items, loading, setItems: setNewItems };
};

export default useTDMConcentrationForCR;
