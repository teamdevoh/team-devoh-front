import React, { useState, useEffect, useRef } from 'react';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { Message, Tab, Table } from 'semantic-ui-react';
import helpers from '../../helpers';
import CRTDMChartContainer from './CRTDMChartContainer';
import { useMyDrugs, useTbdrugPredictionList } from './hooks';
import { useGetProjectActivityId } from '../collection/hooks';
import { activity, activityGroup, role } from '../../constants';

import CRTDMReport from './CRTDMReport';

const getPredictions = ({ tbdrugId, source }) => {
  return source.filter(item => Number(item.tbdrugId) === Number(tbdrugId));
};

const CRTDMChartWrapper = ({ projectId, subjectId, projectSiteId }) => {
  const myDrug = useMyDrugs(subjectId);
  const [{ items: predictItems }] = useTbdrugPredictionList();
  const [activeIndex, setActiveIndex] = useState(0);

  const t = useFormatMessage();
  let firstTDM = {
    activityGroupId: activityGroup.FirstTDM,
    projectActivityId: 0
  };
  let fuTDM = {
    activityGroupId: activityGroup.FUTDM,
    projectActivityId: 0
  };
  const firstPA = useGetProjectActivityId({
    activityGroupId: firstTDM.activityGroupId,
    subjectId,
    activityId: activity.TB_PK
  });
  const fuPA = useGetProjectActivityId({
    activityGroupId: fuTDM.activityGroupId,
    subjectId,
    activityId: activity.TB_PK
  });
  firstTDM.projectActivityId = firstPA.id;
  fuTDM.projectActivityId = fuPA.id;

  const [isDone, setIsDone] = useState(false);
  const [panes, setPanes] = useState([]);
  const priorPaneLength = useRef();

  const setPriorPaneLength = data => {
    priorPaneLength.current = data;
  };

  const getPriorPaneLength = () => {
    return priorPaneLength.current;
  };

  useEffect(
    () => {
      const roles = helpers.Identity.profile.roles;
      const drugs = _.groupBy(myDrug.items, 'exDrugId');

      let newPanes = [];
      const exDrugIds = Object.keys(drugs);

      exDrugIds.forEach(exDrugId => {
        newPanes.push({
          menuItem: drugs[exDrugId][0].genericName,
          render: () => (
            <div
              id={`tdmTab_${exDrugId}`}
              style={{
                // height: '700px',
                padding: '10px',
                border: 'solid 1px #d2d2d2',
                overflowY: 'hidden',
                overflowX: 'hidden'
              }}
            >
              <Table compact="very" celled fixed singleLine>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell
                      width="four"
                      title={t('activity.tdmsampling.subjectVisitId')}
                    >
                      {t('activity.tdmsampling.subjectVisitId')}
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      title={t('activity.tdmconcentration.TAD')}
                    >
                      {t('activity.tdmconcentration.TAD')}
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      title={t('activity.tdmconcentration.drugConc')}
                    >
                      {t('activity.tdmconcentration.drugConc')}
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      title={t('activity.tdmconcentration.exdose')}
                    >
                      {t('activity.tdmconcentration.exdose')}
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      title={t('activity.subtbdrug.exdosfrqcodeId')}
                    >
                      {t('activity.subtbdrug.exdosfrqcodeId')}
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      title={t('activity.subtbdrug.exdosfrmcodeId')}
                    >
                      {t('activity.subtbdrug.exdosfrmcodeId')}
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      width="three"
                      title={t('activity.common.exPeriod')}
                    >
                      {t('activity.common.exPeriod')}
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {drugs[exDrugId].map((item, index) => {
                    const visitName = `${helpers.util.getOrdinal(
                      item.visitOrder
                    )} ${item.visitTypeName} ${item.visitDateTime}`;
                    const tad = `${item.tad} hr`;
                    const drugConc = `${item.drugConc} ${item.drugConcUnit}`;
                    const exdose = `${item.exdose} mg`;
                    const exdosfrqName = item.exdosfrqName;
                    const exdosfrmName = item.exdosfrmName;
                    const exPeriod = `${item.exstdtc} ~ ${
                      item.exendtc === null ? '' : item.exendtc
                    }`;

                    return (
                      <Table.Row key={index}>
                        <Table.Cell title={visitName}>
                          <a
                            style={{ cursor: 'pointer' }}
                            onClick={() => {
                              const isFU = item.visitOrder > 1;

                              helpers.history.push(
                                `/project/${projectId}/subject/${subjectId}/activitygroup/${
                                  isFU
                                    ? fuTDM.activityGroupId
                                    : firstTDM.activityGroupId
                                }?projectActivityId=${
                                  isFU
                                    ? fuTDM.projectActivityId
                                    : firstTDM.projectActivityId
                                }&activityId=31`
                              );
                            }}
                          >
                            {visitName}
                          </a>
                        </Table.Cell>
                        <Table.Cell title={tad}>{tad}</Table.Cell>
                        <Table.Cell title={drugConc}>{drugConc}</Table.Cell>
                        <Table.Cell title={exdose}>{exdose}</Table.Cell>
                        <Table.Cell title={exdosfrqName}>
                          {exdosfrqName}
                        </Table.Cell>
                        <Table.Cell title={exdosfrmName}>
                          {exdosfrmName}
                        </Table.Cell>
                        <Table.Cell title={exPeriod}>{exPeriod}</Table.Cell>
                      </Table.Row>
                    );
                  })}
                </Table.Body>
              </Table>
              <CRTDMChartContainer
                subjectId={subjectId}
                exDrugId={exDrugId}
                predictItems={getPredictions({
                  tbdrugId: exDrugId,
                  source: predictItems
                })}
              />
            </div>
          )
        });
      });

      if (newPanes.length > 0 && roles.indexOf(role.TDMReport_Build) > 0) {
        newPanes.push({
          menuItem: 'Report',
          render: () => (
            <CRTDMReport
              projectId={projectId}
              projectSiteId={projectSiteId}
              subjectId={subjectId}
            />
          )
        });
      }

      setPanes(newPanes);
      setIsDone(true);
    },
    [myDrug.items, helpers]
  );

  useEffect(
    () => {
      const priorPaneLength = getPriorPaneLength();

      if (isDone) {
        if (priorPaneLength - 1 === activeIndex) {
          setActiveIndex(panes.length - 1);
        } else {
          setActiveIndex(0);
        }

        setPriorPaneLength(panes.length);
        setIsDone(false);
      }
    },
    [panes, activeIndex, isDone]
  );

  const handleTabChange = (e, { activeIndex }) => {
    setActiveIndex(activeIndex);
  };

  return (
    <div>
      <Message success header="Only up to 48 hours TAD is displayed." />
      <Tab
        activeIndex={activeIndex}
        onTabChange={handleTabChange}
        menu={{ secondary: true, pointing: true }}
        panes={panes}
      />
    </div>
  );
};

export default CRTDMChartWrapper;
