import React, { useRef } from 'react';
import Chart from 'react-apexcharts';
import { useParams } from 'react-router-dom';
import helpers from '../../helpers';

const CRTDMChart = ({ subjectId, series }) => {
  const { projectId } = useParams();
  const chartREf = useRef();

  const getSubjectBySeries = config => {
    const seriesIndex = config.seriesIndex;
    const dataPointIndex = config.dataPointIndex;

    const dataSetObj = series[seriesIndex];
    if (!!dataSetObj.subjects) {
      const subject = series[seriesIndex].subjects[dataPointIndex];
      return subject;
    }
  };

  // 빨간 표준선 앞으로 보여주기 위한
  const setStandardLine = () => {
    if (!!chartREf.current) {
      const chartSvg = chartREf.current.chart.el.getElementsByTagName('svg');
      const domNode = chartSvg[0].getElementsByClassName(
        'apexcharts-inner apexcharts-graphical'
      )[0];
      const populationPrediction = chartSvg[0].getElementsByClassName(
        'apexcharts-line-series'
      )[0];
      if (typeof populationPrediction !== 'undefined') {
        const id = populationPrediction.getAttribute('id');

        const useTag = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'use'
        );
        useTag.setAttributeNS('http://www.w3.org/1999/xlink', 'href', `#${id}`);
        domNode.appendChild(useTag);
      }
    }
  };

  const handleDataPointSelection = (event, chartContext, config) => {
    const subject = getSubjectBySeries(config);

    if (subject.subjectId !== subjectId && subject.subjectNo) {
      helpers.history.push(
        `/project/${projectId}/subject/${subject.subjectId}/casereview`
      );
    }
  };

  const options = {
    stroke: {
      curve: 'smooth',
      width: 2
    },
    chart: {
      height: 350,
      type: 'scatter',
      zoom: {
        enabled: true,
        type: 'xy'
      },
      animations: {
        enabled: false
      },
      events: {
        dataPointSelection: handleDataPointSelection,
        updated: setStandardLine
      }
    },
    xaxis: {
      tickAmount: 5,
      labels: {
        formatter: function(val) {
          return `${parseFloat(val).toFixed(2)} hr`;
        }
      }
    },
    yaxis: {
      tickAmount: 5,
      labels: {
        formatter: function(val) {
          return parseFloat(val).toFixed(2);
        }
      }
    },
    markers: {
      size: 3
    },
    tooltip: {
      custom: ({ seriesIndex, dataPointIndex, w }) => {
        const data = series[seriesIndex].data[dataPointIndex];
        const label = series[seriesIndex].name;
        const x = `${data[0]} hr`;
        const y = `${data[1]}`;

        const dataSetObj = series[seriesIndex];
        if (!!dataSetObj.subjects) {
          const subject = dataSetObj.subjects[dataPointIndex];

          const tdm =
            `<span style="margin-right: 8px;">${subject.exdose}mg</span>` + y;
          if (subject.subjectNo) {
            return (
              '<div class="ui card">' +
              ' <div class="content">' +
              '   <img src="images/' +
              subject.img +
              '" class="ui mini right floated image" />' +
              '   <div class="header">' +
              subject.subjectNo +
              '/' +
              subject.initial +
              '</div>' +
              '   <div class="meta">' +
              subject.siteName +
              '</div>' +
              '   <div class="description">' +
              '<div>' +
              label +
              '</div>' +
              '     <div> ' +
              x +
              ' </div>' +
              '     <div> ' +
              tdm +
              ' ' +
              subject.drugConcUnit +
              '</div>' +
              '   </div>' +
              ' </div>' +
              '</div>'
            );
          } else {
            return (
              '<div class="ui card">' +
              ' <div class="content">' +
              '   <div class="description">' +
              '<div>' +
              label +
              '</div>' +
              '     <div> ' +
              x +
              ' </div>' +
              '     <div> ' +
              tdm +
              ' ' +
              subject.drugConcUnit +
              '</div>' +
              '   </div>' +
              ' </div>' +
              '</div>'
            );
          }
        } else {
          return null;
        }
      }
    }
  };

  return (
    <Chart
      options={options}
      series={series}
      // type="scatter"
      height={350}
      ref={chartREf}
    />
  );
};

export default CRTDMChart;
