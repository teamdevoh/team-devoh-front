import React, { Fragment } from 'react';
import { Dimmer, Icon, Segment } from 'semantic-ui-react';
import Chart from 'react-apexcharts';

const FakeScatter = () => {
  return (
    <Fragment>
      <div style={{ position: 'absolute', top: '50%', left: '50%', zIndex: 1 }}>
        <Icon name="hand point up" size="big" />
      </div>
      <Dimmer.Dimmable
        as={Segment}
        basic
        style={{ height: '350px' }}
        blurring
        dimmed={true}
      >
        <Chart options={{}} series={[]} type="scatter" height={350} />
      </Dimmer.Dimmable>
    </Fragment>
  );
};

export default FakeScatter;
