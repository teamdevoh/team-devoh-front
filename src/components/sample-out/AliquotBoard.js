import React from 'react';
import { Button } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import useUser from '../account-manage/hooks/useUser';
import { DataGrid } from '../data-grid';
import { format } from '../../constants';

const AliquotBoard = ({ items, loading, onRemove, rowRenderer }) => {
  const t = useFormatMessage();
  const [{ profile }] = useUser();

  const columns = [
    {
      key: 'subjectNo',
      name: 'Subject',
      width: 130,
      formatter: ({ value, row }) => {
        const { initial } = row;
        return value + (!!initial ? `/${initial}` : '');
      }
    },
    {
      key: 'aliquotBarcode',
      name: 'Aliquot ID',
      width: 140
    },
    {
      key: 'sample',
      name: 'Sample',
      width: 140
    },
    {
      key: 'sampleMaterial',
      name: 'Material'
    },
    {
      key: 'aliquotAnalyte',
      name: 'Analyte'
    },
    {
      key: 'aliquotVolume',
      name: 'Volume',
      formatter: ({ value, row }) => {
        const { unit } = row;
        return `${value} ${unit}`;
      }
    },
    {
      key: 'aliquotDateTime',
      name: 'Aliquot Time',
      formatter: ({ value, row }) => {
        const localTime = helpers.util.utcToLocal(value);

        return helpers.util.dateformat(localTime, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'preserve',
      name: 'Preserve'
    },
    {
      key: 'currentDateTime',
      name: 'Out Time',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'aliquotMemberName',
      name: 'User',
      width: 150,
      formatter: ({ value, row }) => {
        return profile.name;
      }
    },
    {
      key: 'remove',
      name: 'Remove',
      width: 110,
      formatter: ({ value, row }) => {
        const { sampleAliquotId } = row;
        return (
          <Button
            onClick={onRemove({
              sampleAliquotId
            })}
            size="tiny"
          >
            {t('folder.remove')}
          </Button>
        );
      }
    }
  ];

  return (
    <div>
      <DataGrid
        columns={columns}
        rows={items}
        minHeight={550}
        loading={loading}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        rowKey="sampleAliquotId"
        rowRenderer={rowRenderer}
      />
    </div>
  );
};

export default AliquotBoard;
