import React from 'react';
import { Radio } from 'semantic-ui-react';
import { useCodes } from '../../hooks';
import styled from 'styled-components';
import { code, codeGroup } from '../../constants';

const RadioWrap = styled.div`
  & .ui.radio.checkbox:not(:last-child) {
    margin-right: 10px;
  }
`;

export const SampleAliquotStatus = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.SampleAliquotStatusCode });
  const reasonList = [
    code.RemoveFromBox,
    code.Disposal,
    code.Analysis,
    code.MovingBox
  ];

  const generateField = () => {
    const result = [];

    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (reasonList.includes(item.value)) {
        result.push(
          <Radio
            key={item.value}
            name={name}
            label={item.text}
            value={item.value}
            checked={item.value === value}
            onChange={onChange}
          />
        );
      }
    }

    return result;
  };

  return <RadioWrap>{generateField()}</RadioWrap>;
};
