import helpers from '../../helpers';

const saveSampleBoxTakeOutAliquot = ({ sampleBoxId, model }) => {
  return helpers.Service.put(`api/samples/boxes/${sampleBoxId}`, model);
};

export default {
  saveSampleBoxTakeOutAliquot
};
