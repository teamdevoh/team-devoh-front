import helpers from '../../helpers';

const fetchSubjectTdmStatus = ({
  projectSiteId,
  sampleFromDate,
  sampleToDate,
  tdmTarget,
  reportTarget,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    sampleFromDate,
    sampleToDate,
    tdmTarget,
    reportTarget,
    offset,
    limit
  });

  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/subjects/tdm.status?${params}`
  );
};

export default {
  fetchSubjectTdmStatus
};
