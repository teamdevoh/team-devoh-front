import React from 'react';
import { useFormatMessage } from '../../hooks';
import { DataGrid } from '../data-grid';
import helpers from '../../helpers';
import styled from 'styled-components';
import { format } from '../../constants';

const TdmGridRowWrapper = styled.div`
  ${props => {
    const result = [];
    const { colored } = props;

    if (colored) {
      result.push(`
        & .react-grid-Cell {
          background-color: rgba(208, 25, 25);
        }
      `);
    }

    return result.join('');
  }};
`;

const DifferenceDigWrapper = styled.div`
  & {
    background-color: rgba(242, 113, 28) !important;
  }
`;

const Board = ({ projectId, items, loading, onRowVisibleEnd }) => {
  const t = useFormatMessage();

  const columns = [
    {
      key: 'site',
      name: t('site'),
      width: 145
    },
    {
      key: 'doctor',
      name: 'Doctor'
    },
    {
      key: 'subjectNo',
      name: 'Subject No',
      width: 90,
      formatter: ({ value, row }) => {
        const { subjectId } = row;
        return (
          <a
            style={{ cursor: 'pointer' }}
            onClick={() => {
              helpers.history.push(
                `/project/${projectId}/subject/${subjectId}/activitygroup/5`
              );
            }}
          >
            {value}
          </a>
        );
      }
    },
    {
      key: 'iC_Date',
      name: 'IC Date',
      width: 100
    },
    {
      key: 'iC_Version',
      name: 'IC Version'
    },
    {
      key: 'age',
      name: t('activity.interview.age'),
      width: 60
    },
    {
      key: 'bmi',
      name: 'BMI',
      width: 60
    },
    {
      key: 'smoke',
      name: 'Smoke',
      width: 250
    },
    {
      key: 'gender',
      name: t('activity.interview.sexcodeId')
    },
    {
      key: 'diagnosis',
      name: 'Diagnosis',
      width: 90
    },
    {
      key: 'tB_Site',
      name: 'TB Site',
      width: 180
    },
    {
      key: 'treat_Start',
      name: 'Treat Start',
      width: 100
    },
    {
      key: 'caseon',
      name: 'CASEON',
      width: 100
    },
    {
      key: 'condtc',
      name: 'CONDTC',
      width: 100
    },
    {
      key: 'firsT_DIG',
      name: 'First DIG',
      width: 100,
      formatter: ({ value, row }) => {
        const { diG_FIN } = row;

        if (value !== diG_FIN) {
          return <DifferenceDigWrapper>{value}</DifferenceDigWrapper>;
        } else {
          return <div>{value}</div>;
        }
      }
    },
    {
      key: 'diG_FIN',
      name: 'DIG FIN',
      width: 100,
      formatter: ({ value, row }) => {
        const { firsT_DIG } = row;

        if (value !== firsT_DIG) {
          return <DifferenceDigWrapper>{value}</DifferenceDigWrapper>;
        } else {
          return <div>{value}</div>;
        }
      }
    },
    {
      key: 'comorbid',
      name: 'Comorbidity',
      width: 290
    },
    {
      key: 'adr',
      name: 'ADR',
      width: 290
    },
    {
      key: 'dst',
      name: 'DST',
      width: 290
    },
    {
      key: 'miC_Label',
      name: 'MIC Label',
      width: 130
    },
    {
      key: 'inhriF_RES',
      name: 'INF/RIF RES',
      width: 290
    },
    {
      key: 'solid',
      name: 'SOLID',
      width: 290
    },
    {
      key: 'liquid',
      name: 'LIQUID',
      width: 300
    },
    {
      key: 'firsT_EXCOMBI',
      headerRenderer: header => {
        return (
          <div>
            1st TDM
            <br />
            Drug Combination
          </div>
        );
      },
      width: 200
    },
    {
      key: 'firsT_EXTRT',
      name: 'Brand Name',
      width: 400
    },
    {
      key: 'firsT_ScheduleDateTime',
      name: 'Sampling Scheduled',
      width: 150
    },
    {
      key: 'firsT_QUICK_Report',
      name: 'Quick Report',
      width: 100
    },
    {
      key: 'firsT_Sample_Count',
      name: 'Sampling Count'
    },
    {
      key: 'firsT_SAMPLE_DATE',
      name: 'Sample Date',
      width: 150
    },
    {
      key: 'firsT_SAMPLE_IN',
      name: 'Sample In',
      width: 150
    },
    {
      key: 'firsT_MACROSCOPY',
      name: 'Macroscopy',
      width: 150
    },
    {
      key: 'firsT_TDM_DATE',
      name: 'TDM Date',
      width: 100
    },
    {
      key: 'firsT_REPORT_DATE',
      name: 'Reporting Date',
      width: 100
    },
    {
      key: 'firsT_DATA_IE',
      name: 'DATA IE'
    },
    {
      key: 'firsT_IE_REASON',
      name: 'IE Reason'
    },
    {
      key: 'firsT_Elapsed_Days',
      name: 'Elapsed Days',
      width: 100
    },
    {
      key: 'naT2',
      name: 'NAT2',
      width: 230
    },
    {
      key: 'slcO1B1',
      name: 'SLC01B1',
      width: 290
    },
    {
      key: 'seconD_EXCOMBI',
      headerRenderer: header => {
        return (
          <div>
            2nd TDM
            <br />
            Drug Combination
          </div>
        );
      },
      width: 200
    },
    {
      key: 'seconD_EXTRT',
      name: 'Brand Name',
      width: 400
    },
    {
      key: 'seconD_ScheduleDateTime',
      name: 'Sampling Scheduled',
      width: 150,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'seconD_QUICK_Report',
      name: 'Quick Report',
      width: 100
    },
    {
      key: 'seconD_Sample_Count',
      name: 'Sampling Count'
    },
    {
      key: 'seconD_SAMPLE_DATE',
      name: 'Sample Date',
      width: 150
    },
    {
      key: 'seconD_SAMPLE_IN',
      name: 'Sample In',
      width: 150
    },
    {
      key: 'seconD_MACROSCOPY',
      name: 'Macroscopy',
      width: 150
    },
    {
      key: 'seconD_TDM_DATE',
      name: 'TDM Date',
      width: 100,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'seconD_REPORT_DATE',
      name: 'Reporting Date',
      width: 100,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'seconD_DATA_IE',
      name: 'DATA IE'
    },
    {
      key: 'seconD_IE_REASON',
      name: 'IE Reason'
    },
    {
      key: 'seconD_Elapsed_Days',
      name: 'Elapsed Days',
      width: 100
    },
    {
      key: 'thirD_EXCOMBI',
      headerRenderer: header => {
        return (
          <div>
            3rd TDM
            <br />
            Drug Combination
          </div>
        );
      },
      width: 200
    },
    {
      key: 'thirD_EXTRT',
      name: 'Brand Name',
      width: 400
    },
    {
      key: 'thirD_ScheduleDateTime',
      name: 'Sampling Scheduled',
      width: 150,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'thirD_QUICK_Report',
      name: 'Quick Report',
      width: 100
    },
    {
      key: 'thirD_Sample_Count',
      name: 'Sampling Count'
    },
    {
      key: 'thirD_SAMPLE_DATE',
      name: 'Sample Date',
      width: 150
    },
    {
      key: 'thirD_SAMPLE_IN',
      name: 'Sample In',
      width: 150
    },
    {
      key: 'thirD_MACROSCOPY',
      name: 'Macroscopy',
      width: 150
    },
    {
      key: 'thirD_TDM_DATE',
      name: 'TDM Date',
      width: 100,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'thirD_REPORT_DATE',
      name: 'Reporting Date',
      width: 100,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'thirD_DATA_IE',
      name: 'DATA IE'
    },
    {
      key: 'thirD_IE_REASON',
      name: 'IE Reason'
    },
    {
      key: 'thirD_Elapsed_Days',
      name: 'Elapsed Days',
      width: 100
    },
    {
      key: 'fourtH_EXCOMBI',
      headerRenderer: header => {
        return (
          <div>
            4th TDM
            <br />
            Drug Combination
          </div>
        );
      },
      width: 200
    },
    {
      key: 'fourtH_EXTRT',
      name: 'Brand Name',
      width: 400
    },
    {
      key: 'fourtH_ScheduleDateTime',
      name: 'Sampling Scheduled',
      width: 150,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'fourtH_QUICK_Report',
      name: 'Quick Report',
      width: 100
    },
    {
      key: 'fourtH_Sample_Count',
      name: 'Sampling Count'
    },
    {
      key: 'fourtH_SAMPLE_DATE',
      name: 'Sample Date',
      width: 150
    },
    {
      key: 'fourtH_SAMPLE_IN',
      name: 'Sample In',
      width: 150
    },
    {
      key: 'fourtH_MACROSCOPY',
      name: 'Macroscopy',
      width: 150
    },
    {
      key: 'fourtH_TDM_DATE',
      name: 'TDM Date',
      width: 100,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'fourtH_REPORT_DATE',
      name: 'Reporting Date',
      width: 100,
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'fourtH_DATA_IE',
      name: 'DATA IE'
    },
    {
      key: 'fourtH_IE_REASON',
      name: 'IE Reason'
    },
    {
      key: 'fourtH_Elapsed_Days',
      name: 'Elapsed Days',
      width: 100
    }
  ];

  const rowRenderer = ({ renderBaseRow, ...props }) => {
    return (
      <TdmGridRowWrapper colored={props.row.color !== ''}>
        {renderBaseRow(props)}
      </TdmGridRowWrapper>
    );
  };

  return (
    <DataGrid
      columns={columns}
      rowNumber={{ show: true, key: 'no', name: 'No' }}
      rows={items}
      minHeight={600}
      rowKey="rowNum"
      loading={loading}
      rowRenderer={rowRenderer}
      onRowVisibleEnd={onRowVisibleEnd}
      headerRowHeight={50}
    />
  );
};

export default Board;
