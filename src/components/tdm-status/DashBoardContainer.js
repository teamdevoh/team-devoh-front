import React, { useState } from 'react';
import DashBoard from './DashBoard';

const DashBoardContainer = () => {
  const [filter, setFilter] = useState({
    projectSiteId: 0,
    sampleFromDate: null,
    sampleToDate: null,
    tdmTarget: 0,
    reportTarget: 0
  });

  const handleChange = (event, { name, value }) => {
    setFilter({ ...filter, [name]: value });
  };

  const handleDateRangeChange = (e, { name, value }) => {
    setFilter({
      ...filter,
      sampleFromDate: !value[0] ? null : value[0],
      sampleToDate: !value[1] ? null : value[1]
    });
  };

  const handleRequireTarget = (event, { name, value }) => {
    if (value === '') {
      setFilter({
        ...filter,
        tdmTarget: 0,
        reportTarget: 0
      });
    } else {
      setFilter({
        ...filter,
        tdmTarget: 0,
        reportTarget: 0,
        [value]: 1
      });
    }
  };

  return (
    <DashBoard
      {...filter}
      onChange={handleChange}
      onDateRangeChange={handleDateRangeChange}
      onRequireTarget={handleRequireTarget}
    />
  );
};

export default DashBoardContainer;
