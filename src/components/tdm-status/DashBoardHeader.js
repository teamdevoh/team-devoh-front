import React, { Fragment } from 'react';
import { Grid, Form } from 'semantic-ui-react';
import { ProjectSite } from '../collection/Selector';
import { useFormatMessage } from '../../hooks';
import DateRangePicker from '../datepicker/DateRangePicker';
import { TDMOrReporting } from './Selector';

const DashBoardHeader = ({
  projectId,
  onChange,
  projectSiteId,
  sampleFromDate,
  sampleToDate,
  tdmTarget,
  reportTarget,
  onDateRangeChange,
  onRequireTarget
}) => {
  const t = useFormatMessage();

  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <Form>
            <Form.Group widths="equal">
              <Form.Field>
                <label>{t('site')}</label>
                <ProjectSite
                  name="projectSiteId"
                  value={projectSiteId}
                  onChange={onChange}
                  projectId={projectId}
                />
              </Form.Field>
              <Form.Field>
                <label>Sample Date</label>
                <DateRangePicker
                  name=""
                  value={[sampleFromDate, sampleToDate]}
                  onChange={onDateRangeChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Require TDM / Reporting </label>
                <TDMOrReporting
                  name="requireTarget"
                  value={
                    tdmTarget === 1
                      ? 'tdmTarget'
                      : reportTarget === 1
                        ? 'reportTarget'
                        : 0
                  }
                  onChange={onRequireTarget}
                />
              </Form.Field>
            </Form.Group>
          </Form>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
