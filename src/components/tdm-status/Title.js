import React from 'react';
import { Header } from 'semantic-ui-react';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';
import { useFormatMessage } from '../../hooks';

const Title = () => {
  const t = useFormatMessage();
  return (
    <Header as="h3" dividing>
      {t('tdm.status')}
      <Header.Subheader>Collection Description</Header.Subheader>
      <CopyRedirectUrl />
    </Header>
  );
};

export default Title;
