import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const limit = 20;

const useSubjectTdmStatusList = ({
  projectSiteId,
  sampleFromDate,
  sampleToDate,
  tdmTarget,
  reportTarget
}) => {
  const [total, setTotal] = useState(0);
  const [start, setStart] = useState(0);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async ({ offset, orgItems = items }) => {
    setLoading(true);
    try {
      const res = await api.fetchSubjectTdmStatus({
        projectSiteId,
        sampleFromDate,
        sampleToDate,
        tdmTarget,
        reportTarget,
        offset,
        limit
      });

      if (res && res.data) {
        const newItems = orgItems.concat(res.data.items);
        setItems(newItems);
        setStart(offset + limit);
        setTotal(res.data.paging.total);
      }
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      fetchItems({ offset: 0, orgItems: [] });
    },
    [projectSiteId, sampleFromDate, sampleToDate, tdmTarget, reportTarget]
  );

  return [{ items, loading, start, total, refresh: fetchItems }];
};

export default useSubjectTdmStatusList;
