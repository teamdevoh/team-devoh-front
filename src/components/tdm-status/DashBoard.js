import React from 'react';
import Board from './Board';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useSubjectTdmStatusList } from './hooks';

const DashBoard = ({
  projectSiteId,
  sampleFromDate,
  sampleToDate,
  tdmTarget,
  reportTarget,
  onChange,
  onDateRangeChange,
  onRequireTarget
}) => {
  const { projectId } = useParams();

  const [{ items, loading, start, total, refresh }] = useSubjectTdmStatusList({
    projectSiteId,
    sampleFromDate,
    sampleToDate,
    tdmTarget,
    reportTarget
  });

  const handleRowVisibleEnd = () => {
    if (items.length < total) {
      refresh({ offset: start });
    }
  };

  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onChange={onChange}
            projectSiteId={projectSiteId}
            sampleFromDate={sampleFromDate}
            sampleToDate={sampleToDate}
            tdmTarget={tdmTarget}
            reportTarget={reportTarget}
            onDateRangeChange={onDateRangeChange}
            onRequireTarget={onRequireTarget}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool
            projectSiteId={projectSiteId}
            sampleFromDate={sampleFromDate}
            sampleToDate={sampleToDate}
            tdmTarget={tdmTarget}
            reportTarget={reportTarget}
            total={total}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Board
            projectId={projectId}
            items={items}
            loading={loading}
            onRowVisibleEnd={handleRowVisibleEnd}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
