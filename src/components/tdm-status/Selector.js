import React, { Fragment } from 'react';
import { Radio } from 'semantic-ui-react';
import { RadioWrap } from '../sample-freezer-out/styles';
import { Uncheck } from '../activity/Selector';

export const TDMOrReporting = ({ name, value, onChange }) => {
  const items = [
    {
      text: 'Require TDM',
      value: 'tdmTarget'
    },
    {
      text: 'Require Reporting',
      value: 'reportTarget'
    }
  ];

  return (
    <RadioWrap style={{}}>
      {items.map(item => (
        <Fragment key={item.value}>
          <Radio
            name={name}
            label={item.text}
            value={item.value}
            checked={item.value === value}
            onChange={onChange}
          />
          {item.value === value && (
            <Uncheck name={name} clearValue={''} onClick={onChange} />
          )}
        </Fragment>
      ))}
    </RadioWrap>
  );
};
