import React, { Fragment, useState } from 'react';
import { Label } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import {
  FlexDiv,
  RightButtonWrap,
  StyledDetail
} from '../sample-barcode/styles';
import helpers from '../../helpers';
import { StyledCSVExport } from '../export-tdm/styles';
import api from './api';
import { format, role } from '../../constants';
import { RoleAware } from '../auth';

const DashBoardTool = ({
  projectSiteId,
  sampleFromDate,
  sampleToDate,
  tdmTarget,
  reportTarget,
  total
}) => {
  const t = useFormatMessage();
  const [loading, setLoading] = useState(false);

  const columns = [
    {
      key: 'site',
      name: t('site')
    },
    {
      key: 'doctor',
      name: 'Doctor'
    },
    {
      key: 'subjectNo',
      name: 'Subject No'
    },
    {
      key: 'iC_Date',
      name: 'IC Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'iC_Version',
      name: 'IC Version'
    },
    {
      key: 'age',
      name: t('activity.interview.age')
    },
    {
      key: 'bmi',
      name: 'BMI'
    },
    {
      key: 'smoke',
      name: 'Smoke'
    },
    {
      key: 'gender',
      name: t('activity.interview.sexcodeId')
    },
    {
      key: 'diagnosis',
      name: 'Diagnosis'
    },
    {
      key: 'tB_Site',
      name: 'TB Site'
    },
    {
      key: 'treat_Start',
      name: 'Treat Start'
    },
    {
      key: 'caseon',
      name: 'CASEON'
    },
    {
      key: 'condtc',
      name: 'CONDTC',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'firsT_DIG',
      name: 'First DIG'
    },
    {
      key: 'diG_FIN',
      name: 'DIG FIN'
    },
    {
      key: 'comorbid',
      name: 'Comorbidity'
    },
    {
      key: 'adr',
      name: 'ADR'
    },
    {
      key: 'dst',
      name: 'DST'
    },
    {
      key: 'miC_Label',
      name: 'MIC Label'
    },
    {
      key: 'inhriF_RES',
      name: 'INF/RIF RES'
    },
    {
      key: 'solid',
      name: 'SOLID'
    },
    {
      key: 'liquid',
      name: 'LIQUID'
    },
    {
      key: 'firsT_EXCOMBI',
      name: '1st TDM Drug Combination'
    },
    {
      key: 'firsT_EXTRT',
      name: 'Brand Name'
    },
    {
      key: 'firsT_ScheduleDateTime',
      name: 'Sampling Scheduled',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'firsT_QUICK_Report',
      name: 'Quick Report'
    },
    {
      key: 'firsT_Sample_Count',
      name: 'Sampling Count'
    },
    {
      key: 'firsT_SAMPLE_DATE',
      name: 'Sample Date'
    },
    {
      key: 'firsT_SAMPLE_IN',
      name: 'Sample In'
    },
    {
      key: 'firsT_MACROSCOPY',
      name: 'Macroscopy'
    },
    {
      key: 'firsT_TDM_DATE',
      name: 'TDM Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'firsT_REPORT_DATE',
      name: 'Reporting Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'firsT_DATA_IE',
      name: 'DATA IE'
    },
    {
      key: 'firsT_IE_REASON',
      name: 'IE Reason'
    },
    {
      key: 'firsT_Elapsed_Days',
      name: 'Elapsed Days'
    },
    {
      key: 'naT2',
      name: 'NAT2'
    },
    {
      key: 'slcO1B1',
      name: 'SLC01B1'
    },
    {
      key: 'seconD_EXCOMBI',
      name: '2nd TDM Drug Combination'
    },
    {
      key: 'seconD_EXTRT',
      name: 'Brand Name'
    },
    {
      key: 'seconD_ScheduleDateTime',
      name: 'Sampling Scheduled',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'seconD_QUICK_Report',
      name: 'Quick Report'
    },
    {
      key: 'seconD_Sample_Count',
      name: 'Sampling Count'
    },
    {
      key: 'seconD_SAMPLE_DATE',
      name: 'Sample Date'
    },
    {
      key: 'seconD_SAMPLE_IN',
      name: 'Sample In'
    },
    {
      key: 'seconD_MACROSCOPY',
      name: 'Macroscopy'
    },
    {
      key: 'seconD_TDM_DATE',
      name: 'TDM Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'seconD_REPORT_DATE',
      name: 'Reporting Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'seconD_DATA_IE',
      name: 'DATA IE'
    },
    {
      key: 'seconD_IE_REASON',
      name: 'IE Reason'
    },
    {
      key: 'seconD_Elapsed_Days',
      name: 'Elapsed Days'
    },
    {
      key: 'thirD_EXCOMBI',
      name: '3rd TDM Drug Combination'
    },
    {
      key: 'thirD_EXTRT',
      name: 'Brand Name'
    },
    {
      key: 'thirD_ScheduleDateTime',
      name: 'Sampling Scheduled',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'thirD_QUICK_Report',
      name: 'Quick Report'
    },
    {
      key: 'thirD_Sample_Count',
      name: 'Sampling Count'
    },
    {
      key: 'thirD_SAMPLE_DATE',
      name: 'Sample Date'
    },
    {
      key: 'thirD_SAMPLE_IN',
      name: 'Sample In'
    },
    {
      key: 'thirD_MACROSCOPY',
      name: 'Macroscopy'
    },
    {
      key: 'thirD_TDM_DATE',
      name: 'TDM Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'thirD_REPORT_DATE',
      name: 'Reporting Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'thirD_DATA_IE',
      name: 'DATA IE'
    },
    {
      key: 'thirD_IE_REASON',
      name: 'IE Reason'
    },
    {
      key: 'thirD_Elapsed_Days',
      name: 'Elapsed Days'
    },
    {
      key: 'fourtH_EXCOMBI',
      name: '4th TDM Drug Combination'
    },
    {
      key: 'fourtH_EXTRT',
      name: 'Brand Name'
    },
    {
      key: 'fourtH_ScheduleDateTime',
      name: 'Sampling Scheduled',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'fourtH_QUICK_Report',
      name: 'Quick Report'
    },
    {
      key: 'fourtH_Sample_Count',
      name: 'Sampling Count'
    },
    {
      key: 'fourtH_SAMPLE_DATE',
      name: 'Sample Date'
    },
    {
      key: 'fourtH_SAMPLE_IN',
      name: 'Sample In'
    },
    {
      key: 'fourtH_MACROSCOPY',
      name: 'Macroscopy'
    },
    {
      key: 'fourtH_TDM_DATE',
      name: 'TDM Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'fourtH_REPORT_DATE',
      name: 'Reporting Date',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD);
      }
    },
    {
      key: 'fourtH_DATA_IE',
      name: 'DATA IE'
    },
    {
      key: 'fourtH_IE_REASON',
      name: 'IE Reason'
    },
    {
      key: 'fourtH_Elapsed_Days',
      name: 'Elapsed Days'
    }
  ];

  const handleCSVExportClick = async () => {
    // 전체 데이터
    setLoading(true);
    try {
      const res = await api.fetchSubjectTdmStatus({
        projectSiteId,
        sampleFromDate,
        sampleToDate,
        tdmTarget,
        reportTarget
      });

      const fieldNames = columns.map(item => item.name);
      const fields = columns.map(item => {
        return {
          ...item,
          // label: item.name,
          value: (dataElement, field, data) => {
            return !!dataElement[item.key]
              ? String(dataElement[item.key]).replaceAll(',', '/')
              : '';
          }
        };
      });

      if (res && res.data) {
        helpers.util.exportCSV({
          fileName: 'TDM_Status_List.csv',
          fields,
          fieldNames,
          data: res.data.items
        });
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <Fragment>
      <FlexDiv>
        <Label size="big">
          Counts
          <StyledDetail>{total}</StyledDetail>
        </Label>
        <RightButtonWrap>
          <RoleAware allowedRole={role.TDMStatus_Excel_Read}>
            <StyledCSVExport
              positive
              icon="download"
              loading={loading}
              onClick={handleCSVExportClick}
              content="CSV Export"
            />
          </RoleAware>
        </RightButtonWrap>
      </FlexDiv>
    </Fragment>
  );
};

export default DashBoardTool;
