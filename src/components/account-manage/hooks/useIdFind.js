import { useHistory } from 'react-router-dom';
import api from '../api';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useState } from 'react';

const useIdFind = () => {
  const t = useFormatMessage();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }] = useFormFields({
    memberName: '',
    email: ''
  });

  const edit = async () => {
    setLoading(true);
    const res = await api.findId({
      memberName: model.memberName,
      email: model.email
    });
    if (res && res.data) {
      notification.info({
        title: `${t('verification.info')}<br/>(${t('vaild.10.mimutes')})`,
        confirmButtonName: t('common.ok'),
        onClose: () => {
          history.push('/signin');
        }
      });
    }
    setLoading(false);
  };

  return [{ model, loading, onChange, onEdit: edit }];
};

export default useIdFind;
