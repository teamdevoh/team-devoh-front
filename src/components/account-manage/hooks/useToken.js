import api from '../api';

const useToken = () => {
  const refresh = async projectId => {
    const resfreshed = await api.refreshToken(projectId);
    return resfreshed;
  };

  return { refresh };
};

export default useToken;
