import { useDispatch } from 'react-redux';
import { identity, project, account } from '../../../states';

const useSignOut = () => {
  const dispatch = useDispatch();

  const signOut = () => {
    dispatch(identity.actions.clear());
    dispatch(project.actions.selectedItem({ key: 0 }));
    dispatch(account.actions.signOut());
  };

  return signOut;
};

export default useSignOut;
