import api from '../api';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useState } from 'react';
import helpers from '../../../helpers';

const usePwdChange = () => {
  const t = useFormatMessage();
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }] = useFormFields({
    currentPassword: '',
    password: '',
    verifyPassword: ''
  });

  const edit = async () => {
    setLoading(true);
    const res = await api.changePwd({
      loginName: helpers.Identity.profile.loginName,
      currentPassword: model.currentPassword,
      password: model.password,
      verifyPassword: model.verifyPassword
    });
    if (res && res.data) {
      notification.success({
        title: t('common.alert.changed'),
        onClose: () => {}
      });
    }
    setLoading(false);
  };

  return [{ model, loading, onChange, onEdit: edit }];
};

export default usePwdChange;
