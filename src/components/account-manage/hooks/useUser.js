import { useSelector } from 'react-redux';
import { identity, account } from '../../../states';

const useUser = () => {
  const profile = useSelector(identity.selectors.getProfile);
  const isAuth = useSelector(account.selectors.getIsAuth);

  return [{ profile, isAuth }];
};

export default useUser;
