import { useHistory } from 'react-router-dom';
import api from '../api';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useState } from 'react';

const usePwdReset = ({ authKey, expire, loginName }) => {
  const t = useFormatMessage();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }] = useFormFields({
    password: '',
    verifyPassword: ''
  });

  const edit = async () => {
    setLoading(true);
    const res = await api.resetPwd({
      authKey,
      expire,
      loginName,
      password: model.password,
      verifyPassword: model.verifyPassword
    });
    if (res && res.data) {
      notification.success({
        title: t('common.alert.changed'),
        onClose: () => {
          history.push('/signin');
        }
      });
    }
    setLoading(false);
  };

  return [{ model, loading, onChange, onEdit: edit }];
};

export default usePwdReset;
