import { useState } from 'react';
import api from '../api';

const useIdentityVerify = () => {
  const [loading, setLoading] = useState(false);
  const [isSent, setIsSent] = useState(false);

  const send = async () => {
    setLoading(true);
    setIsSent(false);
    try {
      const res = await api.sendAuthCodeForIdentityVerify();
      if (res && res.data) {
        setIsSent(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  return { send, isSent, loading };
};

export default useIdentityVerify;
