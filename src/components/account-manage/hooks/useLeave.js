import { useEffect } from 'react';
import api from '../api';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields, useBoolean } from '../../../hooks';
import { useState } from 'react';
import helpers from '../../../helpers';
import useSignOut from './useSignOut';

const useLeave = () => {
  const t = useFormatMessage();
  const signOut = useSignOut();
  const showLeaveConfirm = useBoolean(false);
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }] = useFormFields({
    loginName: helpers.Identity.profile.loginName,
    password: ''
  });

  const confirmLeave = () => {
    showLeaveConfirm.setTrue();
  };

  const leave = async () => {
    setLoading(true);
    const res = await api.leave({
      loginName: helpers.Identity.profile.loginName,
      password: model.password
    });
    if (res && res.data) {
      notification.success({
        title: t('user.leaved.message'),
        onClose: () => {
          signOut();
        }
      });
    }
    setLoading(false);
  };

  useEffect(
    () => {
      if (showLeaveConfirm.value) {
        notification.confirm({
          title: t('user.leave.confirm.title'),
          content: t('user.leave.confirm.content'),
          onCancel: showLeaveConfirm.setFalse,
          onOK: () => {
            leave();
            showLeaveConfirm.setFalse();
          }
        });
      }
    },
    [showLeaveConfirm.value]
  );

  return [{ model, loading, onChange, onLeave: confirmLeave }];
};

export default useLeave;
