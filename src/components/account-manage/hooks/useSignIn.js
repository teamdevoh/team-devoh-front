import { useCallback, useState } from 'react';
import { account, identity } from '../../../states';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useFormFields, useFormatMessage } from '../../../hooks';
import api from '../api';
import helpers from '../../../helpers';
import notification from '../../../components/modal/notification';

const useSignIn = () => {
  const t = useFormatMessage();
  const dispatch = useDispatch();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }] = useFormFields({
    userName: '',
    password: ''
  });

  const setIdentity = ({ token, refreshToken, profile }) => {
    dispatch(identity.actions.setUser(profile));
    dispatch(
      account.actions.signIn({
        token,
        refreshToken
      })
    );
    history.replace(`/${history.location.search}`);
  };

  const signIn = useCallback(
    async () => {
      setLoading(true);

      try {
        const res = await api.signIn({
          loginName: model.userName,
          password: model.password
        });
        if (res && res.data) {
          if (res.data.sendEmail) {
            // 코호트 사용자 이메일 보냄
            notification.info({
              title: `${t('cohort.user.verification.info')}<br/>(${t(
                'vaild.10.mimutes'
              )})`,
              confirmButtonName: t('common.ok')
            });
          } else {
            const jwt = res.data.jwt;
            const profile = res.data.profile;
            setIdentity({
              token: jwt.token,
              refreshToken: jwt.refreshToken,
              profile: profile
            });
          }
        }

        setLoading(false);
      } catch (error) {
        setLoading(false);
      }
    },
    [model]
  );

  const signInKaKao = useCallback(() => {
    window.Kakao.Auth.login({
      success: async authObj => {
        setLoading(true);

        const res = await api.externalSignInKakao(authObj.access_token);
        if (res.data === false) {
          helpers.Storage.add(helpers.storagekeys.AccessTokenExternal, {
            token: authObj.access_token,
            provider: 'kakao',
            email: '',
            name: ''
          });
          history.push('/signup');
        } else {
          const jwt = res.data.jwt;
          const profile = res.data.profile;

          setIdentity({
            token: jwt.token,
            refreshToken: jwt.refreshToken,
            profile: profile
          });
        }

        setLoading(false);
      },
      fail: function(err) {
        alert(JSON.stringify(err));
      }
    });
  }, []);

  const signInFacebook = useCallback(async data => {
    setLoading(true);
    const res = await api.externalSignInFacebook(data.accessToken);

    if (res.data === false) {
      helpers.Storage.add(helpers.storagekeys.AccessTokenExternal, {
        token: data.accessToken,
        provider: 'facebook',
        email: '',
        name: res.name
      });
      history.push('/signup');
    } else {
      const jwt = res.data.jwt;
      const profile = res.data.profile;

      setIdentity({
        token: jwt.token,
        refreshToken: jwt.refreshToken,
        profile: profile
      });
    }
  }, []);

  const signInGoogle = useCallback(async data => {
    setLoading(true);

    const res = await api.externalSignInGoogle(data.tokenId);

    if (res.data === false) {
      helpers.Storage.add(helpers.storagekeys.AccessTokenExternal, {
        token: data.tokenId,
        provider: 'google',
        email: data.profileObj.email,
        name: data.profileObj.name.replace('.', '')
      });
      history.push('/signup');
    } else {
      const jwt = res.data.jwt;
      const profile = res.data.profile;

      setIdentity({
        token: jwt.token,
        refreshToken: jwt.refreshToken,
        profile: profile
      });
    }
  }, []);

  return [
    {
      model,
      loading,
      onChange,
      signIn,
      signInKaKao,
      signInFacebook,
      signInGoogle
    }
  ];
};

export default useSignIn;
