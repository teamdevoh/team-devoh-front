import { useHistory } from 'react-router-dom';
import api from '../api';
import { notification } from '../../modal';
import { useFormatMessage, useFormFields } from '../../../hooks';
import { useState } from 'react';

const usePasswordResetVerify = () => {
  const t = useFormatMessage();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }] = useFormFields({
    memberName: '',
    email: '',
    loginName: ''
  });

  const verify = async () => {
    setLoading(true);
    const { memberName, email, loginName } = model;
    const res = await api.resetPwdVerification({
      memberName,
      email,
      loginName
    });
    if (res && res.data) {
      notification.info({
        title: `${t('verification.info')}<br/>(${t('vaild.10.mimutes')})`,
        confirmButtonName: t('common.ok'),
        onClose: () => {
          history.push('/signin');
        }
      });
    }
    setLoading(false);
  };

  return [{ model, loading, onChange, onVerify: verify }];
};

export default usePasswordResetVerify;
