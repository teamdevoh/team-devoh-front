import findIndex from 'lodash/findIndex';
import find from 'lodash/find';
import helpers from '../../../helpers';
import { useCallback, useState, useEffect } from 'react';
import amApi from '../api';
import { project } from '../../../states';
import { useDispatch, useSelector } from 'react-redux';
import useUser from './useUser';

const useRedirect = () => {
  const [show, setShow] = useState(false);
  const [projectId, setProjectKey] = useState(0);
  const [user] = useUser();
  const items = useSelector(project.selectors.getItems);
  const dispatch = useDispatch();

  const isCustomRedirect = () => {
    return helpers.history.location.search.indexOf('redirect') > -1;
  };

  const getLastAccessRoute = () => {
    return localStorage.getItem(helpers.storagekeys.MyLastLocation);
  };

  const findProjectKeyByRoute = useCallback(route => {
    const splitedRoute = route.split('/');
    const projectRouteName = 'project';

    const projectRouteIndex = findIndex(splitedRoute, (stepRoute, i) => {
      if (stepRoute === projectRouteName) {
        return i;
      }
    });

    if (projectRouteIndex > 0) {
      const projectKeyIndex = projectRouteIndex + 1;
      return splitedRoute[projectKeyIndex];
    }

    return 0;
  });

  const changeProjectTarget = projectContext => {
    dispatch(
      project.actions.selectedItem({
        key: projectContext.projectId,
        name: projectContext.localTitle,
        engName: projectContext.title,
        role: projectContext.projectTeam,
        periodFrom: projectContext.periodFrom,
        periodTo: projectContext.periodTo,
        protocolNo: projectContext.protocolNo,
        variable: projectContext.variable
      })
    );
  };

  const findProjectContext = projectKey => {
    let foundProject = null;
    for (let i = 0; i < items.length; i++) {
      const group = items[i];
      foundProject = find(group.projects, {
        projectId: Number(projectKey)
      });
      if (foundProject) {
        return foundProject;
      }
    }
    return null;
  };

  const routeToRedirect = useCallback(
    async (projectKey, route) => {
      if (projectKey > 0) {
        await amApi.refreshToken(projectKey);
        const projectContext = findProjectContext(projectKey);

        if (projectContext != null) {
          changeProjectTarget(projectContext);
          helpers.history.replace(route);
        }
      }
    },
    [user.isAuth, items]
  );

  useEffect(
    () => {
      if (user.isAuth) {
        if (isCustomRedirect()) {
          const params = helpers.qs.parse(helpers.history.location.search);
          const foundProjectKey = findProjectKeyByRoute(params.redirect);
          if (foundProjectKey > 0) {
            setProjectKey(foundProjectKey);
            routeToRedirect(foundProjectKey, params.redirect);
          }
        } else {
          const myLastRoute = getLastAccessRoute();
          if (myLastRoute) {
            const foundProjectKey = findProjectKeyByRoute(myLastRoute);
            if (foundProjectKey > 0) {
              const location = helpers.history.location;
              setShow(`${location.pathname}${location.search}` !== myLastRoute);
              setProjectKey(foundProjectKey);
            }
          }
        }
      }
    },
    [user.isAuth, items]
  );
  return [
    {
      show,
      projectId,
      isCustomRedirect,
      getLastAccessRoute,
      findProjectKeyByRoute,
      changeProjectTarget,
      findProjectContext,
      routeToRedirect
    },
    { setShow, setProjectKey }
  ];
};

export default useRedirect;
