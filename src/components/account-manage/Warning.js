import React from 'react';
const WarningText = () => {
  return (
    <div className="termsScroll warning">
      개인정보 수집 및 이용에 관한 상기 사항의 동의를 거부할 수 있으며, 동의
      후에도 동의 철회가 언제든 가능합니다.
      <br /> You may refuse to accept any of the above in relation to the
      collection and use of personal information, and withdrawal of consent is
      possible at any time after your consent. <br />
      단, 동의거부 및 동의철회 시에는 회원제 서비스 이용에 제한이 있습니다.{' '}
      <br />
      However, when refusing consent or withdrawing consent, the use of
      membership service is limited.
    </div>
  );
};

export default WarningText;
