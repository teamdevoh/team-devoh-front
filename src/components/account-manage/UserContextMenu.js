import React, { memo, useCallback } from 'react';
import history from '../../helpers/history';
import { Dropdown, Responsive } from 'semantic-ui-react';
import { MyFace } from './';
import { useFormatMessage } from '../../hooks';
import { useSignOut, useUser } from './hooks';

const UserContextMenu = memo(() => {
  const t = useFormatMessage();
  const signOut = useSignOut();
  const [user] = useUser();

  const isMobile =
    Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

  const handleGoProfile = useCallback(() => {
    history.push('/user/profile');
  }, []);

  const handleGoSettings = useCallback(() => {
    history.push('/user/settings');
  }, []);

  return (
    <Dropdown
      direction="left"
      trigger={
        <span>
          <MyFace />
          {!isMobile && user.profile.name}
        </span>
      }
      icon={null}
      pointing="top left"
    >
      <Dropdown.Menu>
        <Dropdown.Item text={t('user.privacy')} onClick={handleGoProfile} />
        <Dropdown.Item text={t('common.settings')} onClick={handleGoSettings} />
        <Dropdown.Divider />
        <Dropdown.Item text={t('sign.out')} onClick={signOut} />
      </Dropdown.Menu>
    </Dropdown>
  );
});

export default UserContextMenu;
