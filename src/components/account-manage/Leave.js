import React from 'react';
import { Form, Button, Container, Header, Input } from 'semantic-ui-react';
import { useFormatMessage, useSubmit } from '../../hooks';
import { useLeave } from './hooks';

const Leave = () => {
  const t = useFormatMessage();
  const [{ model, loading, onChange, onLeave }] = useLeave();
  const [{ validator, onValidate }] = useSubmit(onLeave);

  return (
    <Container>
      <Header>{t('user.leave')}</Header>
      <Form onSubmit={onValidate}>
        <Form.Field>
          <label>{t('user.id')}</label>
          <Input
            name="loginName"
            icon="user outline"
            iconPosition="left"
            readOnly
            value={model.loginName}
          />
        </Form.Field>
        <Form.Field required>
          <label>{t('user.password')}</label>
          <Input
            name="password"
            type="password"
            icon="user outline"
            iconPosition="left"
            value={model.password}
            onChange={onChange}
          />
          {validator.message(t('user.password'), model.password, 'required')}
        </Form.Field>
        <Button primary loading={loading}>
          {t('common.ok')}
        </Button>
      </Form>
    </Container>
  );
};

export default Leave;
