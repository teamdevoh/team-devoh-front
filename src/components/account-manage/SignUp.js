import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import {
  Button,
  Divider,
  Container,
  Input,
  TextArea,
  Form
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import SignUpStep from './SignUpStep';
import { SimpleValidator, FaceUpload } from '../modules';
import { MobileInput, EmailInput, PhoneInput } from '../input';
import api from './api';
import helpers from '../../helpers';
import * as loadImage from 'blueimp-load-image';
import Site from './Site';

class SignUp extends Component {
  constructor(props) {
    super(props);

    const externalInfo = {
      name: '',
      email: '',
      token: '',
      provider: '',
      ...helpers.Storage.find(helpers.storagekeys.AccessTokenExternal)
    };

    this.state = {
      profile: {
        memberName: externalInfo.name,
        loginName: '',
        password: '',
        verifyPassword: '',
        email: externalInfo.email,
        avatar: '',
        cellPhone: '',
        officePhone: '',
        department: '',
        position: '',
        description: '',
        provider: externalInfo.provider,
        token: externalInfo.token,
        myFaceFileStorageId: null,
        siteId: ''
      },
      src: ''
    };

    this.validator = SimpleValidator.validator();
    this.validator.purgeFields();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDrop = this.handleDrop.bind(this);
  }

  handleChange(event, { name, value }) {
    if (this.state.profile.hasOwnProperty(name)) {
      this.setState({ profile: { ...this.state.profile, [name]: value } });
    }
  }

  handleDrop(acceptedFiles) {
    if (acceptedFiles && acceptedFiles.length > 0) {
      const file = acceptedFiles[0];
      loadImage(
        file,
        canvas => {
          let image = canvas.toDataURL();
          canvas.toBlob(blob => {
            const orientationFile = new File([blob], file.name, {
              type: file.type
            });
            helpers.util
              .upload({ files: [orientationFile] }, percent => {})
              .then(storageFiles => {
                if (storageFiles && storageFiles.length > 0) {
                  const fileStorageId = storageFiles[0].fileStorageId;
                  this.setState({
                    profile: {
                      ...this.state.profile,
                      myFaceFileStorageId: fileStorageId
                    },
                    src: image
                  });
                }
              })
              .catch(e => {});
          }, file.type);
        },
        { orientation: true }
      );
    }
  }

  handleSubmit() {
    const { validator } = this;
    const { profile } = this.state;
    if (validator.allValid()) {
      const email = profile.email.split('@');
      profile.email1 = email[0];
      profile.email2 = email[1];

      if (profile.provider === 'kakao') {
        api
          .externalSignUpKakao(profile)
          .then(res => {
            if (res && res.data) {
              helpers.Storage.remove(helpers.storagekeys.AccessTokenExternal);
              this.props.history.push('/completed');
            }
          })
          .catch(err => {
            console.error(err.data.message);
          });
      } else if (profile.provider === 'google') {
        api
          .externalSignUpGoogle(profile)
          .then(res => {
            if (res && res.data) {
              helpers.Storage.remove(helpers.storagekeys.AccessTokenExternal);
              this.props.history.push('/completed');
            }
          })
          .catch(err => {
            console.error(err.data.message);
          });
      } else if (profile.provider === 'facebook') {
        api
          .externalSignUpFacebook(profile)
          .then(res => {
            if (res && res.data) {
              helpers.Storage.remove(helpers.storagekeys.AccessTokenExternal);
              this.props.history.push('/completed');
            }
          })
          .catch(err => {
            console.error(err.data.message);
          });
      } else {
        api
          .signUp(profile)
          .then(res => {
            if (res && res.data) {
              this.props.history.push('/completed');
            }
          })
          .catch(err => {
            console.error(err.data.message);
          });
      }
    } else {
      validator.showMessages();
      this.forceUpdate();
    }
  }

  render() {
    const { handleChange, handleSubmit, validator } = this;
    const { intl } = this.props;
    const { profile } = this.state;

    return (
      <div>
        <SignUpStep activeStep={2} />
        <Container text>
          <FaceUpload imageSrc={this.state.src} onDrop={this.handleDrop} />
          <Form
            onSubmit={() => {
              handleSubmit();
            }}
          >
            <Form.Field required>
              <label>{intl.formatMessage({ id: 'user.name' })}</label>
              <Input
                name="memberName"
                icon="user outline"
                iconPosition="left"
                value={profile.memberName}
                onChange={handleChange}
              />
              {validator.message(
                intl.formatMessage({ id: 'user.name' }),
                profile.memberName,
                'required|name|max:20'
              )}
            </Form.Field>
            <Form.Field required>
              <label>{intl.formatMessage({ id: 'user.id' })}</label>
              <Input
                name="loginName"
                icon="address card outline"
                iconPosition="left"
                value={profile.loginName}
                onChange={handleChange}
              />
              {validator.message(
                intl.formatMessage({ id: 'user.id' }),
                profile.loginName,
                'required|alpha_num'
              )}
            </Form.Field>
            {profile.provider === '' && (
              <Form.Field required>
                <label>{intl.formatMessage({ id: 'user.password' })}</label>
                <Input
                  name="password"
                  type="password"
                  icon="lock"
                  iconPosition="left"
                  value={profile.password}
                  onChange={handleChange}
                />
                {validator.message(
                  intl.formatMessage({ id: 'user.password' }),
                  profile.password,
                  'required|password:8'
                )}
              </Form.Field>
            )}
            {profile.provider === '' && (
              <Form.Field required>
                <label>
                  {intl.formatMessage({ id: 'user.confirm.password' })}
                </label>
                <Input
                  name="verifyPassword"
                  type="password"
                  icon="lock"
                  iconPosition="left"
                  value={profile.verifyPassword}
                  onChange={handleChange}
                />
                {validator.message(
                  intl.formatMessage({ id: 'user.confirm.password' }),
                  profile.verifyPassword,
                  `required|passwordMatch:${profile.password}`
                )}
              </Form.Field>
            )}
            <Form.Group widths="equal">
              <Form.Field required>
                <label>{intl.formatMessage({ id: 'user.email' })}</label>
                <EmailInput
                  name="email"
                  placeholder={intl.formatMessage({ id: 'user.email' })}
                  value={profile.email}
                  onChange={handleChange}
                />
                {validator.message(
                  intl.formatMessage({ id: 'user.email' }),
                  profile.email,
                  'required|email'
                )}
              </Form.Field>
              <Form.Field required>
                <label>{intl.formatMessage({ id: 'site' })}</label>
                <Site
                  name="siteId"
                  value={profile.siteId}
                  onChange={handleChange}
                />
                {validator.message(
                  intl.formatMessage({ id: 'site' }),
                  profile.siteId,
                  'required'
                )}
              </Form.Field>
            </Form.Group>
            <Form.Field>
              <label>{intl.formatMessage({ id: 'user.cellphone' })}</label>
              <MobileInput
                name="cellPhone"
                placeholder={intl.formatMessage({ id: 'user.cellphone' })}
                value={profile.cellPhone}
                onChange={handleChange}
              />
            </Form.Field>
            <Form.Field>
              <label>{intl.formatMessage({ id: 'user.officephone' })}</label>
              <PhoneInput
                name="officePhone"
                placeholder={intl.formatMessage({
                  id: 'user.officephone'
                })}
                value={profile.officePhone}
                onChange={handleChange}
              />
            </Form.Field>
            <Form.Group widths="equal">
              <Form.Field>
                <label>{intl.formatMessage({ id: 'user.department' })}</label>
                <Input
                  name="department"
                  icon="building outline"
                  iconPosition="left"
                  value={profile.department}
                  onChange={handleChange}
                />
              </Form.Field>
              <Form.Field>
                <label>{intl.formatMessage({ id: 'user.position' })}</label>
                <Input
                  name="position"
                  icon="id badge"
                  iconPosition="left"
                  value={profile.position}
                  onChange={handleChange}
                />
              </Form.Field>
            </Form.Group>
            <Form.Field>
              <label>{intl.formatMessage({ id: 'user.description' })}</label>
              <TextArea
                name="description"
                value={profile.description}
                onChange={handleChange}
              />
            </Form.Field>
            <Button type="button" primary onClick={handleSubmit}>
              {intl.formatMessage({ id: 'user.create.profile' })}
            </Button>
          </Form>
          <Divider />
          <Link to="/signin">
            {intl.formatMessage({ id: 'sign.in.to.existing' })}
          </Link>
        </Container>
      </div>
    );
  }
}

export default injectIntl(SignUp);
