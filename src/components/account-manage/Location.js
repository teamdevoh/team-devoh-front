import React, { useCallback } from 'react';
import helpers from '../../helpers';
import { Confirm } from '../modal';
import useRedirect from './hooks/useRedirect';

helpers.history.listen((location, action) => {
  if (action === 'PUSH') {
    localStorage.setItem(
      helpers.storagekeys.MyLastLocation,
      `${location.pathname}${location.search}`
    );
  }
});

const Location = () => {
  const [
    { show, getLastAccessRoute, findProjectKeyByRoute, routeToRedirect },
    { setShow }
  ] = useRedirect();

  const handleOKClick = () => {
    setShow(false);

    const myLastRoute = getLastAccessRoute();
    const foundProjectKey = findProjectKeyByRoute(myLastRoute);
    routeToRedirect(foundProjectKey, myLastRoute);
  };

  const handleCancelClick = useCallback(() => {
    setShow(false);
    localStorage.setItem(helpers.storagekeys.MyLastLocation, '');
  }, []);

  return (
    <Confirm
      title={'???'}
      content={'최근 접근 메뉴로 이동하시겠습니까?'}
      open={show}
      onOK={handleOKClick}
      loading={false}
      onCancel={handleCancelClick}
    />
  );
};

export default Location;
