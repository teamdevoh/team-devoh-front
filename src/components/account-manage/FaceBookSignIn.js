import React from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { useSignIn } from './hooks';
import { FacebookIcon } from './styles';

const FaceBookSignIn = () => {
  const [{ signInFacebook }] = useSignIn();

  return (
    <FacebookLogin
      appId="2303828009940160"
      autoLoad={false}
      callback={signInFacebook}
      render={renderProps => <FacebookIcon onClick={renderProps.onClick} />}
    />
  );
};

export default FaceBookSignIn;
