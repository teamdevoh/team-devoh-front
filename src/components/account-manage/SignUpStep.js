import React from 'react';
import { injectIntl } from 'react-intl';
import { Step } from 'semantic-ui-react';
import { onlyUpdateForKeys } from 'recompose';
import PropTypes from 'prop-types';
import { Pencil, Done, TaskComplete } from '../animation';
import map from 'lodash/map';

const propTypes = {
  activeStep: PropTypes.number
};

const defaultProps = {};

const SignUpStep = injectIntl(
  onlyUpdateForKeys([])(({ activeStep, intl }) => {
    const steps = [
      {
        order: '1',
        title: intl.formatMessage({ id: 'sign.up.step.tos.title' }),
        description: intl.formatMessage({ id: 'sign.up.step.tos.description' }),
        component: <Done />
      },
      {
        order: '2',
        title: intl.formatMessage({ id: 'sign.up.step.pi.title' }),
        description: intl.formatMessage({ id: 'sign.up.step.pi.description' }),
        component: <Pencil />
      },
      {
        order: '3',
        title: intl.formatMessage({ id: 'sign.up.step.complete.title' }),
        description: intl.formatMessage({
          id: 'sign.up.step.complete.description'
        }),
        component: <TaskComplete />
      }
    ];
    return (
      <Step.Group fluid ordered className="signupStep">
        {map(steps, (step, index) => {
          const state = { completed: false, active: false };
          if (step.order < activeStep) {
            state['completed'] = true;
          }

          if (step.order === activeStep) {
            state['active'] = true;
          }

          return (
            <Step {...state} key={step.order}>
              <Step.Content>
                <Step.Title>
                  {state.active ? step.component : step.title}
                </Step.Title>
                <Step.Description>{step.description}</Step.Description>
              </Step.Content>
            </Step>
          );
        })}
      </Step.Group>
    );
  })
);

SignUpStep.propTypes = propTypes;
SignUpStep.defaultProps = defaultProps;

export default SignUpStep;
