import React from 'react';
import { useSignIn } from './hooks';
import { KaKaoIcon } from './styles';

const KakaoSignIn = () => {
  const [{ signInKaKao }] = useSignIn();

  return <KaKaoIcon onClick={signInKaKao} />;
};

export default KakaoSignIn;
