import React from 'react';
import { Menu, Header } from 'semantic-ui-react';
import SelectLanguage from './SelectLanguage';
import Logo from '../animation/Logo';
import styled from 'styled-components';

const Text = styled.span`
  vertical-align: super;
`;
const MainTitle = () => {
  return (
    <Header as="h4" inverted>
      <Logo />
      <Text>mart R&amp;D Workstation</Text>
    </Header>
  );
};

const SignInHeader = () => {
  return (
    <Menu inverted fixed="top" className="signHeader">
      <Menu.Menu position="left">
        <MainTitle />
      </Menu.Menu>
      <Menu.Menu position="right">
        <SelectLanguage />
      </Menu.Menu>
    </Menu>
  );
};

export default SignInHeader;
