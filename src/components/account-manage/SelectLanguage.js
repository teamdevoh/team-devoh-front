import React, { useCallback } from 'react';
import { Dropdown } from 'semantic-ui-react';
import { category } from '../../constants';
import helpers from '../../helpers';

const SelectLanguage = () => {
  const handleClick = useCallback((event, { value }) => {
    window.localStorage.setItem(helpers.storagekeys.Locale, value);
    window.location.reload(false);
  }, []);

  const userLanguage = window.localStorage.getItem(helpers.storagekeys.Locale);

  if (!userLanguage) {
    window.localStorage.setItem(helpers.storagekeys.Locale, 'en');
  }

  const selectedItem = category.Language.find(option => {
    return option.value === (userLanguage || 'en');
  });

  return (
    <Dropdown
      className="icon"
      floating
      labeled
      icon="world"
      item
      text={selectedItem.text}
    >
      <Dropdown.Menu>
        {category.Language.map(option => {
          return (
            <Dropdown.Item
              onClick={handleClick}
              key={option.key}
              value={option.value}
              active={option.value === userLanguage}
            >
              {option.text}
            </Dropdown.Item>
          );
        })}
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default SelectLanguage;
