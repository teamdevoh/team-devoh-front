import React, { useCallback, memo, Fragment } from 'react';
import PropTypes from 'prop-types';
import SignInForm from './SignInForm';
//import SignInHeader from './SignInHeader';
import useSignIn from './hooks/useSignIn';

const propTypes = {
  signIn: PropTypes.func
};

const defaultProps = {};

const SignIn = memo(({ history }) => {
  const [{ model, onChange, loading, signIn }] = useSignIn();

  const handleSubmit = useCallback(
    async () => {
      await signIn();
    },
    [model]
  );

  return (
    <Fragment>
      {/*<SignInHeader />*/}
      <SignInForm
        model={model}
        onChange={onChange}
        isLoading={loading}
        onSubmit={handleSubmit}
      />
    </Fragment>
  );
});

SignIn.propTypes = propTypes;
SignIn.defaultProps = defaultProps;

export default SignIn;
