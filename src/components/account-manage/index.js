import { asyncComponent } from '../modules';

const FaceBookSignIn = asyncComponent(() =>
  import('./FaceBookSignIn')
);
const GoogleSignIn = asyncComponent(() =>
  import('./GoogleSignIn')
);
const KakaoSignIn = asyncComponent(() =>
  import('./KakaoSignIn')
);
const SignIn = asyncComponent(() =>
  import('./TermsOfService')
);
const Location = asyncComponent(() =>
  import('./Location')
);
const Site = asyncComponent(() =>
  import('./Site')
);

export {
  FaceBookSignIn,
  GoogleSignIn,
  KakaoSignIn,
  SignIn,
  Location,
  Site
}
export { default as api } from './api';
export { default as MyFace } from './MyFace';
export { default as UserContextMenu } from './UserContextMenu';