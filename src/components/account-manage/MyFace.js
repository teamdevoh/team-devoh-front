import React, { memo } from 'react';
import { useFace } from '../../hooks';
import { LazyImage } from '../modules';
import useUser from './hooks/useUser';

const MyFace = memo(() => {
  const [user] = useUser();
  const [faceUrl] = useFace(user.profile.faceId);

  return <LazyImage avatar={true} src={`${faceUrl}`} />;
});

export default MyFace;
