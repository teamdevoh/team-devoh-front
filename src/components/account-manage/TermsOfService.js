import React, { useCallback, useEffect, useState } from 'react';
import { Divider, Container, Header, Form, Checkbox } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';
import SignUpStep from './SignUpStep';
import MemberContents from './Membership';
import PrivacyContents from './Privacy';
import WarningText from './Warning';
import { LinkButton } from '../button';
import { useFormatMessage } from '../../hooks';

const TermsOfService = () => {
  const t = useFormatMessage();
  const history = useHistory();
  const [isOKAll, setIsOKAll] = useState(false);
  const [isOKMemberShip, setIsOKMemberShip] = useState(false);
  const [isOKPrivacy, setIsOKPrivacy] = useState(false);

  const handleNextClick = (model, reset, invalidate) => {
    if (isOKAll) {
      history.push('/signup');
    }
  };

  const handleAllChange = useCallback((event, data) => {
    setIsOKAll(data.checked);
    setIsOKMemberShip(data.checked);
    setIsOKPrivacy(data.checked);
  }, []);

  const handleIsMembershipChange = useCallback(
    (event, data) => {
      setIsOKMemberShip(data.checked);
    },
    [isOKMemberShip, isOKPrivacy, isOKAll]
  );

  const handleIsPrivacyChange = useCallback(
    (event, data) => {
      setIsOKPrivacy(data.checked);
    },
    [isOKMemberShip, isOKPrivacy, isOKAll]
  );

  const handleLoginClick = useCallback(() => {
    history.push('/signin');
  }, []);

  useEffect(
    () => {
      setIsOKAll(isOKMemberShip && isOKPrivacy);
    },
    [isOKMemberShip, isOKPrivacy]
  );

  return (
    <div>
      <SignUpStep activeStep={1} />
      <Container className="stepFirst">
        <div className="agreeAllDiv">
          <dl>
            <dt>
              이용약관과 개인정보 수집 및 이용에 대한 안내를 모두 동의해 주세요.
            </dt>
            <dd>
              Please agree to all the terms and conditions of collection and use
              of personal information.
            </dd>
          </dl>
          <Form.Field className="chkAll">
            <Checkbox
              name="agreeAll"
              label="전체 동의 ( All Agree )"
              validations="isTrue"
              onChange={handleAllChange}
              checked={isOKAll}
              required
            />
          </Form.Field>
        </div>
        <Divider />
        <Form>
          <div>
            <Form.Field>
              <Header as="h3">회원가입약관</Header>
            </Form.Field>
            <Form.Field>
              <MemberContents />
            </Form.Field>
            <Form.Field>
              <Checkbox
                name="agreedMembership"
                label="회원가입약관의 내용에 동의합니다."
                validations="isTrue"
                checked={isOKMemberShip}
                onChange={handleIsMembershipChange}
                required
              />
            </Form.Field>
          </div>
          <div>
            <Form.Field>
              <Header as="h3">개인정보처리방침안내</Header>
            </Form.Field>
            <Form.Field>
              <PrivacyContents />
            </Form.Field>
            <Form.Field>
              <Checkbox
                name="agreedPrivacy"
                label="개인정보처리방침안내의 내용에 동의합니다."
                validations="isTrue"
                checked={isOKPrivacy}
                onChange={handleIsPrivacyChange}
                required
              />
            </Form.Field>
          </div>
          <Divider />
          <div className="WarningText">
            <WarningText />
          </div>
          <LinkButton
            color="grey"
            icon="left arrow"
            labelPosition="left"
            name={t('sign.up.login')}
            onClick={handleLoginClick}
          />
          <LinkButton
            color="red"
            icon="right arrow"
            className="right floated"
            labelPosition="right"
            name={t('sign.up.personal')}
            onClick={handleNextClick}
          />
        </Form>
      </Container>
    </div>
  );
};

export default TermsOfService;
