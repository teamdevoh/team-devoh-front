import React from 'react';
import {
  Form,
  Input,
  Container,
  Button,
  Grid,
  Header
} from 'semantic-ui-react';
import { useFormatMessage, useSubmit } from '../../hooks';
import usePasswordResetVerify from './hooks/usePasswordResetVerify';
import BackButton from '../button/BackButton';
import { SignInWrapper } from './styles';

const ResetPasswordVerify = () => {
  const t = useFormatMessage();

  const [{ model, loading, onChange, onVerify }] = usePasswordResetVerify();
  const [{ validator, onValidate }] = useSubmit(onVerify);

  return (
    <SignInWrapper>
      <Grid.Row>
        <Grid.Column>
          <Container>
            <Form onSubmit={onValidate}>
              <Form.Field>
                <Header as="h1">{t('reset.password')}</Header>
              </Form.Field>
              <Form.Field>
                <label>{t('user.id')}</label>
                <Input name="loginName" onChange={onChange} />
                {validator.message(t('user.id'), model.memberName, 'required')}
              </Form.Field>
              <Form.Field>
                <label>{t('user.name')}</label>
                <Input name="memberName" onChange={onChange} />
                {validator.message(
                  t('user.name'),
                  model.memberName,
                  'required'
                )}
              </Form.Field>
              <Form.Field>
                <label>{t('user.email')}</label>
                <Input name="email" onChange={onChange} />
                {validator.message(t('user.email'), model.email, 'required')}
              </Form.Field>
              <BackButton name={t('common.back')} />
              <Button primary loading={loading}>
                {t('common.ok')}
              </Button>
            </Form>
          </Container>
        </Grid.Column>
      </Grid.Row>
    </SignInWrapper>
  );
};

export default ResetPasswordVerify;
