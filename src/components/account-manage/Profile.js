import React, { useState, useCallback, useEffect } from 'react';
import {
  Form,
  Button,
  Divider,
  Container,
  Header,
  Input,
  TextArea
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { notification } from '../modal';
import api from './api';
import { FaceUpload } from '../modules';
import { MobileInput, EmailInput, PhoneInput } from '../input';
import helpers from '../../helpers';
import * as loadImage from 'blueimp-load-image';
import { useFormatMessage, useValidator } from '../../hooks';
import { Site } from './';
import { useDispatch } from 'react-redux';
import { identity } from '../../states';

const Profile = () => {
  const dispatch = useDispatch();

  const [model, setModel] = useState({
    memberName: '',
    loginName: '',
    email: '',
    email1: '',
    email2: '',
    cellPhone: '',
    officePhone: '',
    department: '',
    position: '',
    description: '',
    siteId: 0,
    myFaceFileStorageId: null
  });

  const [face, setFace] = useState('');
  const [orienFile, setOrienFile] = useState(null);

  const t = useFormatMessage();
  const { validator, showValidationMessage } = useValidator();
  validator.purgeFields();

  useEffect(() => {
    fetchMyProfile();
  }, []);

  const fetchMyProfile = async () => {
    const res = await api.fetchMyProfile();
    const data = res.data;

    if (data.myFaceFileStorageId) {
      helpers.util.getFace(data.myFaceFileStorageId, 100).then(url => {
        setFace(url);
      });
    }

    setModel({ ...data, email: `${data.email1}@${data.email2}` });
  };

  const handleSubmit = useCallback(
    async () => {
      if (validator.allValid()) {
        const emails = model.email.split('@');

        let fileStorageId = null;

        if (orienFile != null) {
          const storageFiles = await helpers.util.upload(
            { files: [orienFile] },
            percent => {}
          );

          if (storageFiles && storageFiles.length > 0) {
            fileStorageId = storageFiles[0].fileStorageId;
          }
        }

        const myFaceFileStorageId =
          fileStorageId > 0 ? fileStorageId : model.myFaceFileStorageId;
        const res = await api.editMyProfile({
          ...model,
          email1: emails[0],
          email2: emails[1],
          myFaceFileStorageId
        });

        if (res.data) {
          dispatch(
            identity.actions.updateProfile({
              faceId: myFaceFileStorageId,
              name: model.memberName
            })
          );
          notification.success({
            title: t('common.alert.changed'),
            onClose: () => {}
          });
        }
      } else {
        showValidationMessage(true);
      }
    },
    [model, orienFile]
  );

  const handleDrop = useCallback(acceptedFiles => {
    if (acceptedFiles && acceptedFiles.length > 0) {
      const file = acceptedFiles[0];
      loadImage(
        file,
        canvas => {
          let image = canvas.toDataURL();
          canvas.toBlob(blob => {
            const orientationFile = new File([blob], file.name, {
              type: file.type
            });
            setOrienFile(orientationFile);
            setFace(image);
          }, file.type);
        },
        { orientation: true }
      );
    }
  }, []);

  const resetPicture = useCallback(
    () => {
      setModel({ ...model, myFaceFileStorageId: null });
      setFace('images/user.png');
    },
    [model]
  );

  const handleChange = useCallback(
    (event, { name, value }) => {
      setModel({ ...model, [name]: value });
    },
    [model]
  );

  const handleLeave = () => {
    helpers.history.push('/user/leave');
  };

  return (
    <Container>
      <Header>{t('user.edit.profile.title')}</Header>
      <FaceUpload imageSrc={face} onDrop={handleDrop} onReset={resetPicture} />
      <Form onSubmit={handleSubmit}>
        <Form.Group widths="equal">
          <Form.Field required>
            <label>{t('user.name')}</label>
            <Input
              name="memberName"
              icon="user outline"
              iconPosition="left"
              value={model.memberName}
              onChange={handleChange}
            />
            {validator.message(
              t('user.name'),
              model.memberName,
              'required|name|max:20'
            )}
          </Form.Field>
          <Form.Field>
            <label>{t('user.id')}</label>
            <Input
              name="loginName"
              icon="address card outline"
              iconPosition="left"
              value={model.loginName}
              readOnly={true}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field required>
            <label>{t('user.email')}</label>
            <EmailInput
              name="email"
              placeholder={t('user.email')}
              value={model.email}
              onChange={handleChange}
            />
            {validator.message(t('user.email'), model.email, 'required|email')}
          </Form.Field>
          <Form.Field>
            <label>{t('site')}</label>
            <Site name="siteId" value={model.siteId} onChange={handleChange} />
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('user.cellphone')}</label>
            <MobileInput
              name="cellPhone"
              placeholder={t('user.cellphone')}
              value={model.cellPhone}
              onChange={handleChange}
            />
          </Form.Field>
          <Form.Field>
            <label>{t('user.officephone')}</label>
            <PhoneInput
              name="officePhone"
              placeholder={t('user.officephone')}
              value={model.officePhone}
              onChange={handleChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('user.department')}</label>
            <Input
              name="department"
              icon="building outline"
              iconPosition="left"
              value={model.department}
              onChange={handleChange}
            />
          </Form.Field>
          <Form.Field>
            <label>{t('user.position')}</label>
            <Input
              name="position"
              icon="id badge"
              iconPosition="left"
              value={model.position}
              onChange={handleChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('user.description')}</label>
            <TextArea
              name="description"
              value={model.description}
              onChange={handleChange}
            />
          </Form.Field>
        </Form.Group>
        <Button type="button" primary onClick={handleSubmit}>
          {t('user.edit.profile')}
        </Button>
        <Button type="button" negative onClick={handleLeave}>
          {t('user.leave')}
        </Button>
        <Divider />
        <Link to="/">
          <div style={{ marginBottom: '1em' }}>{t('common.to.home')}</div>
        </Link>
      </Form>
    </Container>
  );
};

export default Profile;
