import styled from 'styled-components';
import { Grid, Image } from 'semantic-ui-react';

export const SignInWrapper = styled(Grid).attrs({
  verticalAlign: 'middle',
  centered: true
})`
  height: 100vh;
  padding-top: 0 !important;
  background-color: #f5f8fd;
`;

export const SignInFormBox = styled(Grid.Column)`
  max-width: 450px;
`;

export const FacebookIcon = styled(Image).attrs({
  spaced: 'left',
  size: 'tiny',
  src: './facebook-icoon-retina.png'
})`
  cursor: pointer;
  width: 50px;
  height: 50px;
`;

export const GoogleIcon = styled(Image).attrs({
  spaced: 'left',
  src: './google.svg'
})`
  cursor: pointer;
  width: 55px;
  height: 65px;
`;

export const KaKaoIcon = styled(Image).attrs({
  size: 'tiny',
  spaced: 'left',
  src: '/kakaolink_btn_medium.png'
})`
  &&& {
    cursor: pointer;
    width: 50px;
    height: 50px;
  }
`;

export const FlexDiv = styled.div`
  display: flex;
  margin-top: 10px;
  justify-content: center;
  align-items: center;

  & a {
    width: 100%;
  }
  & a:first-child {
    text-align: right;
  }
  & a:list-child {
    text-align: left;
  }
`;

export const Bar = styled.span`
  width: 1px;
  height: 12px;
  background-color: #d2d2d2;
  margin: 2px 10px;
`;
