import React from 'react';
//import Logo from '../animation/Logo';
import { Form, Input, Button, Grid, Segment, Divider } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { KakaoSignIn, GoogleSignIn } from './';
import { useFormatMessage, useSubmit } from '../../hooks';
import { SignInWrapper, SignInFormBox, FlexDiv, Bar } from './styles';
import SelectLanguage from './SelectLanguage';
import useRedirect from './hooks/useRedirect';
import helpers from '../../helpers';

const SignInForm = ({ isLoading, model, onChange, onSubmit }) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);
  useRedirect();

  const handleGoHomepageClick = () => {
    window.open('https://www.cpmtb.kr/index.html');
  };

  const handleCohortStatusClick = () => {
    helpers.history.push('/cohort/status');
  };

  return (
    <SignInWrapper className="signWrap">
      <Grid.Row>
        <div className="topWrapper">
          <Button basic color="blue" onClick={handleCohortStatusClick}>
            cPMTb Cohort Status
          </Button>
          <Button basic color="blue" onClick={handleGoHomepageClick}>
            cPMTb Home page
          </Button>
          <div className="langDiv">
            <SelectLanguage />
          </div>
        </div>
        <SignInFormBox className="loginFieldBox">
          <div className="logo">
            <img src="images/brandCI.png" alt="" className="brandCI" />
            <br />
            <span>Smart R&amp;D Workstation</span>
          </div>
          <h2 style={{ display: 'none' }}>Log in your account</h2>

          <Form size="large" onSubmit={onValidate}>
            <Segment stacked={true}>
              <Form.Field>
                <Input
                  name="userName"
                  icon="user"
                  iconPosition="left"
                  placeholder={t('user.id')}
                  value={model.userName}
                  onChange={onChange}
                />
                {validator.message(t('user.id'), model.userName, 'required')}
              </Form.Field>
              <Form.Field>
                <Input
                  name="password"
                  type="password"
                  icon="lock"
                  iconPosition="left"
                  value={model.password}
                  onChange={onChange}
                />
                {validator.message(
                  t('user.password'),
                  model.password,
                  'required'
                )}
              </Form.Field>
              <div className="buttonGroup">
                <Button
                  fluid
                  loading={isLoading}
                  disabled={isLoading}
                  size="large"
                  color="twitter"
                  type="submit"
                  className="btn"
                >
                  {t('sign.in')}
                </Button>
                <Button as={Link} size="large" to="/tos" fluid className="btn">
                  {t('sign.up')}
                </Button>
              </div>
              <FlexDiv>
                <Link to="/findId">{t('find.id')}</Link>
                <Bar />
                <Link to="/verify/resetPassword">{t('reset.password')}</Link>
              </FlexDiv>
              <Divider horizontal>Or</Divider>

              <div style={{ textAlign: 'center' }}>
                <KakaoSignIn />
                <GoogleSignIn />
                {/* <FaceBookSignIn /> */}
              </div>
            </Segment>
          </Form>
        </SignInFormBox>
      </Grid.Row>
      <Grid.Row className="visualArea">
        <div className="visual01">
          <img src="images/singin_img_01.png" alt="" />
        </div>
        <div className="visual02">
          <img src="images/singin_img_02.png" alt="" />
        </div>
        <div className="visual03">
          <img src="images/singin_img_03.png" alt="" />
        </div>
        <div className="visual04">
          <img src="images/singin_img_04.png" alt="" />
        </div>
        <div className="visual05">
          <img src="images/singin_img_05.png" alt="" />
        </div>
        <div className="visual06">
          <img src="images/singin_img_06.png" alt="" />
        </div>
        <div className="text">
          Center for Personalised <br />
          Precision Medicine of Tuberculosis
        </div>
      </Grid.Row>
    </SignInWrapper>
  );
};

export default SignInForm;
