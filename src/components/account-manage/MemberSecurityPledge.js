import React, { useState, useEffect } from 'react';
import { Container, Header, Grid, Checkbox, Button } from 'semantic-ui-react';
import useUser from './hooks/useUser';
import styled from 'styled-components';
import api from './api';
import helpers from '../../helpers';
import { notification } from '../modal';
import { useHistory } from 'react-router-dom';
import { identity } from '../../states';
import { useDispatch } from 'react-redux';
import { useFormatMessage } from '../../hooks';
import { format } from '../../constants';

const Root = styled(Grid)`
  background-color: rgb(245, 248, 253);
  height: 100%;
`;

const FlexColumn = styled(Grid.Column)`
  &&&& {
    display: flex;
  }
`;

const RootContainer = styled(Container)`
  &&&& {
    border: 1px solid #d2d2d2;
    display: flex;
    flex-direction: column;
    padding: 53px 5% 40px;
    margin: 2% auto;
    justify-content: center;
    background-color: #fff;
  }
`;

const Contents = styled(Grid)`
  &&& {
    margin-top: 5px;
    font-size: 15px;
  }
`;

const UserInfoItem = styled(Grid.Column)`
  &&&& {
    margin: 5px 0;
  }
`;

const UserText = styled.span`
  font-weight: 600;
`;

const SubContent = styled.div`
  margin-top: 8px;
  font-size: 14px;
`;

const SubText = styled.span`
  color: rgba(0, 0, 0, 0.48);
`;

const ItemWrap = styled(Grid.Row)`
  &&& {
    margin: 0 10%;
  }
`;

const Item = styled(Grid.Column)`
  &&&& {
    margin: 10px 0;
  }
`;

const Highlight = styled.div`
  font-size: 18px;
  margin-top: 10px;
`;

const PledgeRow = styled(Grid.Row)`
  &&& {
    margin-top: 30px;
  }
`;

const MemberSecurityPledge = () => {
  const t = useFormatMessage();
  const [{ profile }] = useUser();
  const history = useHistory();
  const dispatch = useDispatch();
  const [userInfo, setUserInfo] = useState({
    siteName: '',
    position: ''
  });
  const [checked, setChecked] = useState(false);

  const handleOnPledgeChange = (e, { checked }) => {
    setChecked(checked);
  };

  const handleOnSubmit = () => {
    if (checked) {
      api.acceptSecurityPledge(profile.id).then(res => {
        if (res.data) {
          notification.success({
            title: t('common.ok'),
            onClose: () => {
              dispatch(
                identity.actions.updateProfile({ securityPledge: true })
              );
              history.replace('/');
            }
          });
        }
      });
    }
  };

  useEffect(() => {
    api.fetchMyProfile().then(res => {
      const { siteName, position, securityPledge } = res.data;
      if (securityPledge) {
        dispatch(identity.actions.updateProfile({ securityPledge }));
      } else {
        setUserInfo({
          siteName,
          position
        });
      }
    });
  }, []);

  return (
    <Root>
      <Grid.Row>
        <FlexColumn>
          <RootContainer textAlign="center">
            <Header as="h1">정보보안서약서 Information Security Pledge</Header>
            <Contents>
              <Grid.Row columns={1}>
                <UserInfoItem>
                  성명 <SubText>Name</SubText> :{' '}
                  <UserText>{profile.name}</UserText>
                </UserInfoItem>
                <UserInfoItem>
                  소속 <SubText>Site</SubText> :{' '}
                  <UserText>{userInfo.siteName}</UserText>
                </UserInfoItem>
                <UserInfoItem>
                  직위 <SubText>Position</SubText> :{' '}
                  <UserText>{userInfo.position}</UserText>
                </UserInfoItem>
              </Grid.Row>
              <Grid.Row columns={1}>
                <Grid.Column>
                  <div>
                    상기 본인은 결핵 정밀맞춤치료 선도연구센터(Center for
                    Personalized Precision Medicine of Tuberculosis, cPMTb)
                    연구과제 개발 일원으로 참여하면서 다음사항을 준수할 것을
                    서약합니다.
                  </div>
                  <SubContent>
                    <SubText>
                      As a member of the Center for Personalized Precision
                      Medicine of Tuberculosis (cPMTb) research project
                      development, I pledge to comply with the following
                    </SubText>
                  </SubContent>
                </Grid.Column>
              </Grid.Row>
              <ItemWrap columns={1}>
                <Item>
                  <div>
                    1.위 연구과제를 수행하는 과정에서 지득하게 된 제반사항을
                    연구과제를 수행하는 기간 중은 물론이고 위 연구과제를 종료한
                    후에도 연구책임자의 허락없이 일체 외부에 누설하지 않을
                    비밀유지의무를 준수합니다.
                  </div>
                  <SubContent>
                    <SubText>
                      I will comply with the confidentiality obligation not to
                      divulge any research information without permission from
                      the research manager after the period and completion of
                      the research task.
                    </SubText>
                  </SubContent>
                </Item>
                <Item>
                  <div>
                    2. 설사 연구과제를 수행하는 과정에서 지득하게 된 제반사항이
                    연구기관에 의하여 외부에 공표된 경우라도 위 1과 같은
                    비밀유지의무를 준수할 것.
                  </div>
                  <SubContent>
                    <SubText>
                      Even if all the information learned in the course of
                      carrying out the research project is published externally
                      by the research institute, the confidentiality obligations
                      as described in Paragraph 1 shall be observed.
                    </SubText>
                  </SubContent>
                </Item>
                <Item>
                  <div>
                    3.연구과제 종료나 중단시 본인이 연구과제를 수행하는 과정에서
                    수집･보유하게 된 연구자료는 즉시 연구책임자에게 반납할 것.
                  </div>
                  <SubContent>
                    <SubText>
                      I will immediately return the research data collected and
                      retained in the course of carrying out the research
                      project to the research manager when the research project
                      is terminated or stopped.
                    </SubText>
                  </SubContent>
                </Item>
              </ItemWrap>
              <Grid.Row columns={1}>
                <Grid.Column>
                  서약일 <SubText>Date of pledge</SubText> :{' '}
                  {helpers.util.dateformat(new Date(), format.YYYY_MM_DD)}
                </Grid.Column>
                <Grid.Column>
                  <Highlight>
                    연구책임자 (<SubText>Director of Research</SubText>)
                  </Highlight>
                </Grid.Column>
              </Grid.Row>
              <PledgeRow>
                <Grid.Column textAlign="right">
                  <Checkbox
                    name="agreedMemberSecurity"
                    label={
                      <label>
                        동의합니다.{' '}
                        <SubText>
                          I pledge to the Information Security Pledge.
                        </SubText>
                      </label>
                    }
                    validations="isTrue"
                    onChange={handleOnPledgeChange}
                    checked={checked}
                    required
                  />
                </Grid.Column>
              </PledgeRow>
              <Grid.Row>
                <Grid.Column textAlign="left" className="signWrap" width={11}>
                  <div className="logo">
                    <img src="images/brandCI.png" alt="" className="brandCI" />
                  </div>
                </Grid.Column>
                <Grid.Column textAlign="right" width={5}>
                  <Button primary onClick={handleOnSubmit} disabled={!checked}>
                    Start
                  </Button>
                </Grid.Column>
              </Grid.Row>
            </Contents>
          </RootContainer>
        </FlexColumn>
      </Grid.Row>
    </Root>
  );
};

export default MemberSecurityPledge;
