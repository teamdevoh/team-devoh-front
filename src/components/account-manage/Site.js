import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import { useSiteList } from '../../hooks';

const Site = ({ name, value, onChange }) => {
  const [items] = useSiteList();

  return (
    <Dropdown
      name={name}
      options={items}
      value={value}
      search
      selection
      onChange={onChange}
    />
  );
};

export default Site;
