import React from 'react';
import {
  Form,
  Input,
  Container,
  Grid,
  Button,
  Header
} from 'semantic-ui-react';
import { useFormatMessage, useSubmit } from '../../hooks';
import { useLocation, Link } from 'react-router-dom';
import usePwdReset from './hooks/usePwdReset';
import helpers from '../../helpers';
import { SignInWrapper } from './styles';

const ResetPassword = () => {
  const t = useFormatMessage();

  const [{ model, loading, onChange, onEdit }] = usePwdReset(
    helpers.qs.parse(useLocation().search)
  );
  const [{ validator, onValidate }] = useSubmit(onEdit);

  return (
    <SignInWrapper>
      <Grid.Row>
        <Grid.Column>
          <Container>
            <Form onSubmit={onValidate}>
              <Form.Field>
                <Header as="h1">{t('reset.password')}</Header>
              </Form.Field>
              <Form.Field>
                <label>{t('user.new.password')}</label>
                <Input
                  name="password"
                  type="password"
                  icon="lock"
                  iconPosition="left"
                  onChange={onChange}
                />
                {validator.message(
                  t('user.new.password'),
                  model.password,
                  'required|password:8'
                )}
              </Form.Field>
              <Form.Field>
                <label>{t('user.confirm.password')}</label>
                <Input
                  name="verifyPassword"
                  type="password"
                  icon="lock"
                  iconPosition="left"
                  onChange={onChange}
                />
                {validator.message(
                  t('user.confirm.password'),
                  model.verifyPassword,
                  `required|passwordMatch:${model.password}`
                )}
              </Form.Field>
              <Button as={Link} to="/signin">
                {t('sign.in.page')}
              </Button>
              <Button primary loading={loading}>
                {t('user.edit.password')}
              </Button>
            </Form>
          </Container>
        </Grid.Column>
      </Grid.Row>
    </SignInWrapper>
  );
};

export default ResetPassword;
