import React, { Component } from 'react';
import {
  Image,
  Divider,
  Container,
  Grid,
  Icon,
  Header,
  Segment
} from 'semantic-ui-react';
import { Form } from 'semantic-ui-react';
import SignUpStep from './SignUpStep';
import { RedirectButton } from '../button';

class TermsOfService extends Component {
  render() {
    return (
      <div>
        <SignUpStep activeStep={3} />
        <Grid stackable>
          <Grid.Column width={6}>
            <Segment basic>
              <Image src="/slide03_ty1.jpg" />
            </Segment>
          </Grid.Column>
          <Grid.Column width={10}>
            <Segment basic>
              <Container>
                <Form>
                  <Form.Field>
                    <Header as="h1">회원가입 완료</Header>
                  </Form.Field>
                </Form>
                <Divider />
                <RedirectButton
                  color="red"
                  name="서비스 사용을 위하여 로그인 화면으로 이동합니다."
                  to="/signin"
                >
                  <Icon name="arrow right" />
                </RedirectButton>
              </Container>
            </Segment>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default TermsOfService;
