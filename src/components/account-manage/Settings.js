import React, { useState, useCallback } from 'react';
import { Header, Container, Divider, Icon, Accordion } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PasswordChange from './PasswordChange';
import SelectLanguage from './SelectLanguage';
import { useFormatMessage } from '../../hooks';

const propTypes = {};

const defaultProps = {};

const Settings = () => {
  const t = useFormatMessage();
  const [actives, setActives] = useState([true, true]);

  const handleClick = useCallback(
    (e, data) => {
      setActives({ ...actives, [data.index]: !actives[data.index] });
    },
    [actives]
  );

  return (
    <Container>
      <Accordion>
        <Accordion.Title
          as={Header}
          active={actives[0]}
          onClick={handleClick}
          index={0}
        >
          <Icon name="dropdown" />
          {t('user.edit.password.title')}
        </Accordion.Title>
        <Accordion.Content active={actives[0]}>
          <PasswordChange />
        </Accordion.Content>
        <Accordion.Title
          as={Header}
          active={actives[1]}
          onClick={handleClick}
          index={1}
        >
          <Icon name="dropdown" />
          {t('common.defaultset')}
        </Accordion.Title>
        <Accordion.Content active={actives[1]}>
          <SelectLanguage />
        </Accordion.Content>
      </Accordion>
      <Divider />
      <Link to="/">{t('common.to.home')}</Link>
    </Container>
  );
};

Settings.propTypes = propTypes;
Settings.defaultProps = defaultProps;

export default Settings;
