import React from 'react';
import { Form, Button, Container, Input } from 'semantic-ui-react';
import { useFormatMessage, useSubmit } from '../../hooks';
import usePwdChange from './hooks/usePwdChange';

const PasswordChange = () => {
  const t = useFormatMessage();
  const [{ model, loading, onChange, onEdit }] = usePwdChange();
  const [{ validator, onValidate }] = useSubmit(onEdit);

  return (
    <Container>
      <Form onSubmit={onValidate}>
        <Form.Field>
          <label>{t('user.current.password')}</label>
          <Input
            name="currentPassword"
            type="password"
            icon="lock"
            iconPosition="left"
            onChange={onChange}
          />
          {validator.message(
            t('user.current.password'),
            model.currentPassword,
            'required|password:8'
          )}
        </Form.Field>
        <Form.Field>
          <label>{t('user.new.password')}</label>
          <Input
            name="password"
            type="password"
            icon="lock"
            iconPosition="left"
            onChange={onChange}
          />
          {validator.message(
            t('user.new.password'),
            model.password,
            'required|password:8'
          )}
        </Form.Field>
        <Form.Field>
          <label>{t('user.confirm.password')}</label>
          <Input
            name="verifyPassword"
            type="password"
            icon="lock"
            iconPosition="left"
            onChange={onChange}
          />
          {validator.message(
            t('user.confirm.password'),
            model.verifyPassword,
            `required|passwordMatch:${model.password}`
          )}
        </Form.Field>
        <Button primary loading={loading}>
          {t('user.edit.password')}
        </Button>
      </Form>
    </Container>
  );
};

export default PasswordChange;
