import React from 'react';
import { GoogleLogin } from 'react-google-login';
import { useSignIn } from './hooks';
import { GoogleIcon } from './styles';

const GoogleSignIn = () => {
  const [{ signInGoogle }] = useSignIn();

  return (
    <GoogleLogin
      clientId="448548435657-kfgoonmb3edjv9ls7nfa3lnaq5se2f2s.apps.googleusercontent.com"
      buttonText="Google"
      render={renderProps => <GoogleIcon onClick={renderProps.onClick} />}
      onSuccess={signInGoogle}
      onFailure={res => {
        alert('login failed');
      }}
    />
  );
};

export default GoogleSignIn;
