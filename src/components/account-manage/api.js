import helpers from '../../helpers';

const signIn = creds => {
  return helpers.Service.post('api/account/signin', creds);
};

const signUp = member => {
  return helpers.Service.post('api/account/signup', member);
};

const externalSignUpKakao = member => {
  return helpers.Service.post('api/account/external/kakao/signup', member);
};

const externalSignInKakao = token => {
  return helpers.Service.post('api/account/external/kakao/signin', { token });
};

const externalSignUpGoogle = member => {
  return helpers.Service.post('api/account/external/google/signup', member);
};

const externalSignInGoogle = token => {
  return helpers.Service.post('api/account/external/google/signin', { token });
};

const externalSignUpFacebook = member => {
  return helpers.Service.post('api/account/external/facebook/signup', member);
};

const externalSignInFacebook = token => {
  return helpers.Service.post('api/account/external/facebook/signin', {
    token
  });
};

const findId = ({ memberName, email }) => {
  const params = helpers.qs.stringify({
    memberName,
    email
  });
  return helpers.Service.get(`api/account/findid?${params}`);
};

const foundId = ({ authKey, expire, loginName }) => {
  const params = helpers.qs.stringify({
    authKey,
    expire,
    loginName
  });
  return helpers.Service.get(`api/account/foundid?${params}`);
};

const changePwd = model => {
  return helpers.Service.put('api/account/password', model);
};

const resetPwdVerification = ({ memberName, email, loginName }) => {
  const params = helpers.qs.stringify({
    memberName,
    email,
    loginName
  });
  return helpers.Service.get(`api/account/reset/password?${params}`);
};

const resetPwd = model => {
  return helpers.Service.put('api/account/reset/password', model);
};

const fetchMyProfile = () => {
  return helpers.Service.get('api/account/profile');
};

const editMyProfile = model => {
  return helpers.Service.put('api/account/profile', model);
};

const refreshToken = projectId => {
  return helpers.Service.refreshToken(projectId);
};

const fetchMembers = args => {
  const params = helpers.qs.stringify(args);
  return helpers.Service.get(`api/members?${params}`);
};

const fetchMember = memberId => {
  return helpers.Service.get(`api/members/${memberId}`);
};

const editPushToken = ({ memberId, token }) => {
  return helpers.Service.put(`api/members/${memberId}/token`, { token });
};

const leave = model => {
  return helpers.Service.delete(`api/account/leave`, { data: model });
};

const acceptSecurityPledge = memberId => {
  return helpers.Service.put(
    `api/members/${memberId}/pledge/information.security`
  );
};

const sendAuthCodeForIdentityVerify = () => {
  return helpers.Service.post(`api/account/authcode.send`, {});
};

export default {
  signIn,
  findId,
  foundId,
  changePwd,
  resetPwdVerification,
  resetPwd,
  editMyProfile,
  editPushToken,
  fetchMyProfile,
  fetchMembers,
  fetchMember,
  signUp,
  externalSignUpKakao,
  externalSignInKakao,
  externalSignUpGoogle,
  externalSignInGoogle,
  externalSignUpFacebook,
  externalSignInFacebook,
  refreshToken,
  leave,
  acceptSecurityPledge,
  sendAuthCodeForIdentityVerify
};
