import React, { useState, useEffect } from 'react';
import { Grid, Container, Message, Button } from 'semantic-ui-react';
import { useLocation, Link } from 'react-router-dom';
import { useFormatMessage } from '../../hooks';
import api from './api';
import helpers from '../../helpers';
import { SignInWrapper } from './styles';

const FoundId = () => {
  const t = useFormatMessage();
  const [memberInfo, setMemberInfo] = useState('');
  const verification = helpers.qs.parse(useLocation().search);

  useEffect(() => {
    api
      .foundId(verification)
      .then(response => {
        const { memberName, loginName } = response.data;
        setMemberInfo({ memberName, loginName });
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  return (
    <SignInWrapper>
      <Grid.Row>
        <Grid.Column>
          <Container>
            <Message size="huge">
              <Message.Header>
                {t('found.id', {
                  name: memberInfo.memberName,
                  loginName: memberInfo.loginName
                })}
              </Message.Header>
            </Message>
            <Button as={Link} color="twitter" to="/signin">
              {t('sign.in.page')}
            </Button>
            <Button as={Link} to="/verify/resetPassword">
              {t('reset.password')}
            </Button>
          </Container>
        </Grid.Column>
      </Grid.Row>
    </SignInWrapper>
  );
};

export default FoundId;
