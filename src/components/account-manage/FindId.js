import React from 'react';
import {
  Form,
  Input,
  Container,
  Button,
  Grid,
  Header
} from 'semantic-ui-react';
import { useFormatMessage, useSubmit } from '../../hooks';
import useIdFind from './hooks/useIdFind';
import BackButton from '../button/BackButton';
import { SignInWrapper } from './styles';

const FindId = () => {
  const t = useFormatMessage();
  const [{ model, loading, onChange, onEdit }] = useIdFind();
  const [{ validator, onValidate }] = useSubmit(onEdit);

  return (
    <SignInWrapper>
      <Grid.Row>
        <Grid.Column>
          <Container>
            <Form onSubmit={onValidate}>
              <Form.Field>
                <Header as="h1">{t('find.id')}</Header>
              </Form.Field>
              <Form.Field>
                <label>{t('user.name')}</label>
                <Input name="memberName" onChange={onChange} />
                {validator.message(
                  t('user.name'),
                  model.memberName,
                  'required'
                )}
              </Form.Field>
              <Form.Field>
                <label>{t('user.email')}</label>
                <Input name="email" onChange={onChange} />
                {validator.message(t('user.email'), model.email, 'required')}
              </Form.Field>
              <BackButton name={t('common.back')} />
              <Button primary loading={loading}>
                {t('common.ok')}
              </Button>
            </Form>
          </Container>
        </Grid.Column>
      </Grid.Row>
    </SignInWrapper>
  );
};

export default FindId;
