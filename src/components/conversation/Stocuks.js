import React, { memo } from 'react';
import {
  Visibility,
  Card,
  Segment,
  Image,
  Loader,
  Icon,
  Button
} from 'semantic-ui-react';
import moment from 'moment';
import helpers from '../../helpers';
import { useFace, useFormatMessage } from '../../hooks';
import styled from 'styled-components';
import PostAttachList from './PostAttachList';
import api from './api';
import { notification } from '../modal';
import { format } from '../../constants';

const converter = helpers.util.markdownToHtml();

const CardHeaderWrap = styled(Card.Content)`
  display: flex;
`;

const CardHeader = styled(Card.Header)`
  &&&&&& {
    margin-left: 10px;
    display: flex;
    align-items: center;
    font-size: 15px;
  }
`;

const CardMeta = styled(Card.Meta)`
  &&& {
    margin-left: 10px;
    display: flex !important;
    align-items: center;
  }
`;

const OptionButton = styled(Button)`
  &&& {
    padding: 6px;
    background-color: transparent;
    margin-left: auto;
  }
`;

const AvatarWrapper = memo(({ myFaceId }) => {
  const [faceUrl] = useFace(myFaceId);
  return <Image src={faceUrl} circular />;
});

const Stucks = ({ items, projectId, loading, onShowImg, refresh }) => {
  const t = useFormatMessage();

  const handleUnstuck = postId => () => {
    notification.confirm({
      title: t('remove.from.fixed.list'),
      onOK: async () => {
        const res = await api.updatePostStuck({
          projectId,
          postId,
          model: { isStuck: false }
        });

        if (res) {
          refresh({ refreshPost: true });
        }
      }
    });
  };

  return (
    <Visibility
      style={{ display: 'flex', flexGrow: 1, height: '100vh' }}
      // continuous={hasMore} onBottomVisible={handleVisibleBottom}
    >
      <Segment
        style={{
          background: '#e1e5ed',
          flexGrow: 1,
          overflow: 'auto',
          position: 'relative',
          height: '77%'
        }}
      >
        <Loader active={loading} inline="centered" />
        <Card.Group itemsPerRow={1}>
          {items.map(item => {
            const { postId, contents, myFaceId, author, modified } = item;
            const attachs = helpers.util.generatePostFileInfo(item.attachs);

            return (
              <Card key={postId}>
                <CardHeaderWrap>
                  <AvatarWrapper myFaceId={myFaceId} />
                  <CardHeader>{author}</CardHeader>
                  <CardMeta>
                    <span>
                      {moment(modified, format.YYYYMMDDHHMMSS).fromNow()}
                    </span>
                  </CardMeta>
                  <OptionButton icon circular onClick={handleUnstuck(postId)}>
                    <Icon name="pin" />
                  </OptionButton>
                </CardHeaderWrap>
                <Card.Content className="contents">
                  <Card.Description
                    onClick={onShowImg}
                    dangerouslySetInnerHTML={{
                      __html: converter.makeHtml(contents)
                    }}
                  />
                  {/* 파일 */}
                  <PostAttachList
                    postId={postId}
                    projectId={projectId}
                    attachs={attachs}
                    // loading={attachDown.loading}
                  />
                </Card.Content>
              </Card>
            );
          })}
        </Card.Group>
      </Segment>
    </Visibility>
  );
};

export default Stucks;
