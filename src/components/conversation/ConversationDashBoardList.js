import React, { Fragment } from 'react';
import { Comment } from 'semantic-ui-react';
import styled from 'styled-components';
import helpers from '../../helpers';
import Post from './Post';

const CommentGroup = styled(Comment.Group)`
  &&&& {
    max-width: 100%;
    padding: 15px;
    background-color: rgb(245, 248, 253);
    margin: 0;

    .isReply& {
      background-color: transparent;
    }
  }
`;

const generateList = ({ list, isReply, ...rest }) => {
  const result = [];

  for (let i = list.length - 1; i >= 0; i--) {
    const { postId, attachs, modified } = list[i];

    result.push(
      <Post
        key={postId}
        data={{
          ...list[i],
          attachs: helpers.util.generatePostFileInfo(attachs),
          postDate: modified
        }}
        isReply={isReply}
        {...rest}
      />
    );
  }

  return result;
};

const ConversationDashBoardList = ({ items, isReply, ...rest }) => {
  return (
    <Fragment>
      <CommentGroup className={`${isReply ? 'isReply' : ''}`}>
        {generateList({ list: items, isReply, ...rest })}
      </CommentGroup>
    </Fragment>
  );
};

export default ConversationDashBoardList;
