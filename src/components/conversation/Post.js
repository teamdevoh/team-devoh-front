import React, { memo, useState, Fragment } from 'react';
import { Comment, Button, Icon, Image } from 'semantic-ui-react';
import styled from 'styled-components';
import moment from 'moment';
// import { LazyImage } from '../modules';
import { useFace, useFormatMessage } from '../../hooks';
import api from './api';
import Replies from './Replies';
import helpers from '../../helpers';
import ConversationEditor from './ConversationEditor';
import PostAttachList from './PostAttachList';
import { notification } from '../modal';
import EditorLoader from './EditorLoader';
import { format } from '../../constants';

const converter = helpers.util.markdownToHtml();

const Avatar = styled.div`
  display: block;
  width: 2.5em;
  height: auto;
  float: left;
  margin: 0.2em 0 0;
`;

const ContentsWrap = styled(Comment.Content)`
  &&& {
    margin-left: 35px;
    background-color: #fff;
    padding: 13px;

    .isReply& {
      background-color: transparent;
    }
  }
`;

const CommentHeader = styled.div`
  display: flex;
`;

const ButtonWrap = styled.div`
  margin-left: auto;
`;

const OptionButton = styled(Button)`
  &&& {
    padding: 6px;
    background-color: transparent;
  }
`;

const ReplyButton = styled.a`
  &&&&& {
    color: #192254;
    margin-top: 5px;
  }
`;

const EditorWrap = styled.div`
  position: relative;
`;

const AvatarWrapper = memo(({ myFaceId, isRemove = false }) => {
  const [faceUrl] = useFace(myFaceId);
  return (
    <Avatar>
      {isRemove ? (
        <Icon name="trash" />
      ) : (
        // <LazyImage avatar src={faceUrl} />
        <Image src={faceUrl} circular />
      )}
    </Avatar>
  );
});

const Post = ({
  data,
  isReply = false,
  onShowImg,
  onStuckRefresh,
  onUpdatePost
}) => {
  const t = useFormatMessage();
  const { id } = helpers.Identity.profile;
  const {
    postId,
    projectId,
    parentPostId,
    myFaceId,
    author,
    authorId,
    // modified,
    postDate,
    contents,
    replies,
    signedUrlList,
    attachs: propAttachs = [],
    isStuck
  } = data;
  const [isRemove, setIsRemove] = useState(false);
  const [showReplyInput, setShowReplyInput] = useState(false);
  const [showEditor, setShowEditor] = useState(false);
  const [newReply, setNewReply] = useState(null);
  const [attachs, setAttachs] = useState(propAttachs);
  const [replyAttachs, setReplyAttachs] = useState([]);
  const [onSave, setOnSave] = useState(false);

  /**
   * 수정, 취소 버튼 클릭
   */
  const handleModifyBtnClick = () => {
    setShowEditor(!showEditor);
  };

  const handleUndo = () => {
    if (contents.trim() === '' && attachs.length === 0) {
      notification.warning({
        title: t('post.save.confirmation')
      });
    } else {
      setShowEditor(!showEditor);
    }
  };

  /**
   * post, reply 수정
   */
  const handleModifySaveClick = async (value, newFileStorageIds, isStuck) => {
    setOnSave(true);
    let res = null;

    if (isReply) {
      res = await api.updateReplies({
        projectId,
        parentPostId,
        postId,
        model: { contents: value, fileStorageIds: newFileStorageIds }
      });
    } else {
      res = await api.updatePosts({
        projectId,
        postId,
        model: { contents: value, fileStorageIds: newFileStorageIds, isStuck }
      });
    }

    if (res) {
      setOnSave(false);
      const { attachs, isStuck } = res.data;
      setAttachs(helpers.util.generatePostFileInfo(attachs));
      setShowEditor(false);
      onUpdatePost(postId, res.data);

      if (!isReply && isStuck) {
        onStuckRefresh();
      }
    }
  };

  /**
   * 새 댓글 저장
   */
  const handleSaveClick = async (value, newFileStorageIds) => {
    const res = await api.addReplies({
      projectId,
      parentPostId: postId,
      model: {
        contents: value,
        fileStorageIds: newFileStorageIds
      }
    });

    if (res.data) {
      setNewReply(res.data);
      setShowReplyInput(!showReplyInput);
      setReplyAttachs([]);
    }
  };

  const handleDelete = () => {
    notification.confirm({
      title: t('delete.message'),
      content: '',
      onOK: async () => {
        let result = null;

        if (isReply) {
          const res = await api.removeReplies({
            projectId,
            parentPostId,
            postId
          });
          result = res.data;
        } else {
          const res = await api.removePosts({
            projectId,
            postId
          });
          result = res.data;
        }

        if (result) {
          setIsRemove(true);
        }
      }
    });
  };

  const handleAddAttachs = newItems => {
    setAttachs([...attachs, ...newItems]);
  };

  const handleAddNewReplyAttachs = newItems => {
    setReplyAttachs([...replyAttachs, ...newItems]);
  };

  const handleAttachDeleteClick = postAttachmentId => {
    setAttachs(
      attachs.filter(file => {
        return file.key !== postAttachmentId;
      })
    );
  };

  const handleNewReplyAttachDeleteClick = postAttachmentId => {
    setReplyAttachs(
      replyAttachs.filter(file => {
        return file.key !== postAttachmentId;
      })
    );
  };

  return (
    <Comment>
      <AvatarWrapper myFaceId={myFaceId} isRemove={isRemove} />
      <ContentsWrap className={`${isReply ? 'isReply' : ''}`}>
        {isRemove ? (
          <div>{t('deleted.message')}</div>
        ) : (
          <Fragment>
            <CommentHeader style={{ marginBottom: '10px' }}>
              <Comment.Author as="a">{author}</Comment.Author>
              <Comment.Metadata>
                <span>{moment(postDate, format.YYYYMMDDHHMMSS).fromNow()}</span>
                {isStuck ? <Icon name="pin" color="black" /> : null}
              </Comment.Metadata>
              {id === authorId &&
                !showEditor && (
                  <ButtonWrap>
                    <OptionButton icon circular onClick={handleModifyBtnClick}>
                      <Icon name="pencil alternate" />
                    </OptionButton>
                    <OptionButton icon circular onClick={handleDelete}>
                      <Icon name="trash" />
                    </OptionButton>
                  </ButtonWrap>
                )}
            </CommentHeader>
            {showEditor ? (
              <EditorWrap>
                {onSave && <EditorLoader />}
                <ConversationEditor
                  postId={postId}
                  projectId={projectId}
                  onSubmit={handleModifySaveClick}
                  value={contents}
                  signedUrlList={signedUrlList}
                  attachs={attachs}
                  saveIconName="check"
                  extraCustomLastButtons={[
                    {
                      name: 'undo',
                      execute: (state, api) => {
                        handleUndo();
                      },
                      buttonProps: { 'aria-label': 'Undo edit' },
                      keyCommand: 'undo',
                      iconName: 'cancel'
                    }
                  ]}
                  onAttachDeleteClick={handleAttachDeleteClick}
                  onAddAttachCb={handleAddAttachs}
                  isStuck={isStuck}
                  showStuck={isReply ? false : true}
                />
              </EditorWrap>
            ) : (
              <Fragment>
                <Comment.Text
                  onClick={onShowImg}
                  className="conversation-contents"
                  dangerouslySetInnerHTML={{
                    __html: converter.makeHtml(contents)
                  }}
                />
                {/* 파일 */}
                <PostAttachList
                  postId={postId}
                  projectId={projectId}
                  attachs={attachs}
                  // loading={attachDown.loading}
                  onDeleteClick={handleAttachDeleteClick}
                />
              </Fragment>
            )}
            {/* 댓글 있을때만 */}
            {((!!replies && replies.length !== 0) || !!newReply) && (
              <Replies
                items={replies}
                projectId={projectId}
                parentPostId={postId}
                newItem={newReply}
                onShowImg={onShowImg}
              />
            )}
            {/* 댓글 아닐떄만 */}
            {!isReply && (
              <Comment.Actions>
                <ReplyButton
                  onClick={() => {
                    setShowReplyInput(!showReplyInput);
                  }}
                >
                  <Icon name="reply" />
                  Reply
                </ReplyButton>
              </Comment.Actions>
            )}
            {showReplyInput && (
              <ConversationEditor
                projectId={projectId}
                onSubmit={handleSaveClick}
                hasSaveButton
                signedUrlList={signedUrlList}
                attachs={replyAttachs}
                onAttachDeleteClick={handleNewReplyAttachDeleteClick}
                onAddAttachCb={handleAddNewReplyAttachs}
                showStuck={false}
              />
            )}
          </Fragment>
        )}
      </ContentsWrap>
    </Comment>
  );
};

export default Post;
