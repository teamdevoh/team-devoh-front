import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import api from '../api';

const limit = 10;

const useConversationList = ({ filter }) => {
  const [total, setTotal] = useState(0);
  const [start, setStart] = useState(0);
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [cb, setCb] = useState(null);
  const projectId = useSelector(state => state.project.selected.key);

  const fetchItems = async ({ offset, orgItems = items, callBack = cb }) => {
    setLoading(true);
    try {
      const res = await api.fetchPosts({
        projectId,
        offset,
        limit
      });

      const newItems = orgItems.concat(res.data.items);

      setTotal(res.data.paging.total);
      setItems(newItems);
      setLoading(false);
      setStart(offset + limit);

      if (!!callBack) {
        callBack();
      }
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems({ offset: 0, orgItems: [] });
    },
    [filter]
  );

  return [
    { items, loading, start, total, refresh: fetchItems },
    { setCb, setItems }
  ];
};

export default useConversationList;
