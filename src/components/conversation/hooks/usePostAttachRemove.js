import { useState } from 'react';
import api from '../api';

const usePostAttachRemove = ({ projectId, postId }) => {
  const [loading, setLoading] = useState(false);

  const remove = async postAttachmentId => {
    setLoading(true);
    const res = await api.removePostAttach({
      projectId,
      postId,
      postAttachmentId
    });
    setLoading(true);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  return { remove, loading };
};

export default usePostAttachRemove;
