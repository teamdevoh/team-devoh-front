import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import api from '../api';

const limit = 50;

const useStuckList = ({ filter }) => {
  const [total, setTotal] = useState(0);
  const [start, setStart] = useState(0);
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);
  const projectId = useSelector(state => state.project.selected.key);

  const fetchItems = async (
    { offset, orgItems = items } = { offset: 0, orgItems: [] }
  ) => {
    setLoading(true);
    const res = await api.fetchStucks({
      projectId
      // offset,
      // limit
    });

    const newItems = orgItems.concat(res.data.items);

    setTotal(res.data.paging.total);
    setItems(newItems);
    setLoading(false);
    setStart(offset + limit);
  };

  useEffect(
    () => {
      fetchItems({ offset: 0, orgItems: [] });
    },
    [filter]
  );

  return [{ items, loading, start, total, refresh: fetchItems }];
};

export default useStuckList;
