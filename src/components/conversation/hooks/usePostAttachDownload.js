import { useState, useCallback } from 'react';
import helpers from '../../../helpers';

const usePostAttachDownload = ({ projectId, postId }) => {
  const [loading, setLoading] = useState(false);

  const download = useCallback(async postAttachmentId => {
    setLoading(true);
    helpers.util
      .download(
        `api/projects/${projectId}/posts/${postId}/attach/${postAttachmentId}`,
        percent => {
          if (percent === 100) {
            setLoading(false);
          }
        }
      )
      .catch(err => {
        setLoading(false);
      });
  }, []);

  return { download, loading };
};

export default usePostAttachDownload;
