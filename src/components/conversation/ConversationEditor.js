import React, { Fragment, useState, useEffect, useMemo } from 'react';
import { Icon, Popup } from 'semantic-ui-react';
import EditorForm from '../editor/EditorForm';
import api from './api';
import PostAttachList from './PostAttachList';
import helpers from '../../helpers';
import { useUser } from '../account-manage/hooks';
import { FakeFileList } from '../modules';
import { useUpload, useFormatMessage } from '../../hooks';
import { notification } from '../modal';

const ConversationEditor = ({
  projectId,
  postId = 0,
  attachs = [],
  onAttachDeleteClick,
  onSubmit,
  onAddAttachCb = f => f,
  loading: propLoading = false,
  isStuck: propIsStuck = false,
  showStuck = true,
  ...rest
}) => {
  const t = useFormatMessage();
  const [newFileStorageIds, setNewFileStorageIds] = useState(new Set());
  const [isStuck, setIsStuck] = useState(propIsStuck);
  const [my] = useUser();
  const { upload, fakes, loading /*, isError*/ } = useUpload();
  const extraCustomButtons = useMemo(
    () => {
      return showStuck
        ? [
            {
              name: 'pin',
              execute: (state, api) => {
                handleSetStuck();
              },
              buttonProps: { 'aria-label': 'pin' },
              keyCommand: 'pin',
              icon: () => (
                <Popup
                  trigger={
                    <Icon
                      name="pin"
                      color={isStuck ? 'black' : 'grey'}
                      style={{ opacity: isStuck ? '1' : '0.6' }}
                    />
                  }
                  content="Post Fixing"
                  inverted
                  basic
                />
              )
            }
          ]
        : [];
    },
    [isStuck]
  );

  const handleSetStuck = () => {
    setIsStuck(!isStuck);
  };

  const uploadFilesCompleted = async (ids, acceptedFiles) => {
    // let attachInfo = {};
    if (postId === 0) {
      // 새글 작성일 때
      const newItems = new Set([...newFileStorageIds]);
      for (let i = 0; i < ids.length; i++) {
        newItems.add(ids[i]);
      }
      setNewFileStorageIds(newItems);
    } else {
      // const res =
      await api.addPostAttach({
        projectId,
        postId,
        model: {
          fileStorageIds: ids
        }
      });
      // attachInfo = res.data;
    }

    const newItems = [];

    for (let i = 0; i < ids.length; i++) {
      const id = ids[i];
      const acceptedFile = acceptedFiles[i];
      const { type, name, size } = acceptedFile;

      // const thumbnailApiUrl = helpers.util.isImage(type) ?
      // `api/tasks/${taskItemId}/attach.thumbnail/${newTaskAttachmentId}` : undefined;

      const fileObj = await helpers.file.setFileInfo({
        id,
        extension: type,
        originalName: name,
        size,
        // created: attachInfo.created,
        creator: my.profile.name
        // thumbnailApiUrl,
      });

      newItems.push(fileObj);
    }
    onAddAttachCb(newItems);
  };

  const handleUploadFile = files => {
    upload(files, uploadFilesCompleted);
  };

  const handleSubmit = value => {
    value = value.trim();
    if (value === '' && attachs.length === 0) {
      notification.warning({
        title: t('post.save.confirmation')
      });
    } else {
      onSubmit(value, [...newFileStorageIds], isStuck);
      setNewFileStorageIds(new Set());
      setIsStuck(false);
    }
  };

  useEffect(
    () => {
      const newItem = [];
      for (let i = 0; i < attachs.length; i++) {
        const { key } = attachs[i];
        if (newFileStorageIds.has(key)) {
          newItem.push(key);
        }
      }

      setNewFileStorageIds(new Set([...newItem]));
    },
    [attachs]
  );

  return (
    <Fragment>
      <EditorForm
        onUploadFile={handleUploadFile}
        onSubmit={handleSubmit}
        attachs={attachs}
        loading={loading || propLoading}
        extraCustomButtons={extraCustomButtons}
        {...rest}
      />
      {/* 파일 */}
      <PostAttachList
        postId={postId}
        projectId={projectId}
        attachs={attachs}
        onDeleteClick={onAttachDeleteClick}
        isEdit
      />
      <FakeFileList items={fakes} />
    </Fragment>
  );
};

export default ConversationEditor;
