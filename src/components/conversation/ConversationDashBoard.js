import React from 'react';
import ConversationDashBoardBody from './ConversationDashBoardBody';

const ConversationDashBoard = () => {
  return (
    <React.Fragment>
      <ConversationDashBoardBody />
    </React.Fragment>
  );
};
export default ConversationDashBoard;
