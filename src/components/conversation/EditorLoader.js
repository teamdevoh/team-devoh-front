import React from 'react';
import { Loader } from 'semantic-ui-react';
import styled from 'styled-components';

const LoadingWrap = styled.div`
  background: rgb(255, 255, 255);
  height: 100%;
  width: 100%;
  position: absolute;
  z-index: 100;
  opacity: 0.6;
  display: flex;
  align-items: center;
`;

const EditorLoader = () => {
  return (
    <LoadingWrap>
      <Loader active inline="centered" />
    </LoadingWrap>
  );
};

export default EditorLoader;
