import React, { Fragment, useState, useEffect } from 'react';
import { Button } from 'semantic-ui-react';
import styled from 'styled-components';
import api from './api';
import ConversationDashBoardList from './ConversationDashBoardList';
import { useFormatMessage } from '../../hooks';

const Root = styled.div`
  background-color: #faf9f8;
`;

const MoreButton = styled(Button)`
  &&& {
    width: 100%;
    padding: 5px 15px;
    text-align: left;
  }
`;

const Replies = ({ items = [], projectId, parentPostId, newItem, ...rest }) => {
  const t = useFormatMessage();
  const [replies, setReplies] = useState(items);
  const [fold, setFold] = useState(true);
  const [total, setTotal] = useState(items.length);
  const [showMoreButton, setShowMoreButton] = useState(true);

  const handleClick = () => {
    const newFoldState = !fold;
    setFold(newFoldState);

    if (newFoldState) {
      // 접기
      setReplies([{ ...replies[0] }]);
    } else {
      // 열기
      fetch();
    }
  };

  const fetch = () => {
    api.fetchReplies({ projectId, parentPostId }).then(res => {
      const { items: newItems, paging } = res.data;

      const newList = [...newItems];
      setReplies(newList);
      setTotal(paging.total);
      if (paging.total === 1) {
        setShowMoreButton(false);
      }
    });
  };

  const handleUpdatePost = (updatePostId, newData) => {
    const newList = [];
    let isUpdateList = false;

    for (let i = 0; i < replies.length; i++) {
      const curData = replies[i];
      const { postId } = curData;
      if (postId === updatePostId) {
        isUpdateList = true;
      } else {
        newList.push(curData);
      }
    }

    if (isUpdateList) {
      newList.unshift(newData);
    }

    setReplies(newList);
  };

  useEffect(
    () => {
      if (!!newItem) {
        const newReplies = [...replies];
        newReplies.unshift(newItem);
        setReplies(newReplies);
        setTotal(total + 1);
      }
    },
    [newItem]
  );

  return (
    <Root>
      {total !== 0 ? (
        <Fragment>
          {showMoreButton && (
            <MoreButton onClick={handleClick}>
              {fold ? t('task.audit.more') : t('common.collapse')}
            </MoreButton>
          )}
          <ConversationDashBoardList
            items={replies}
            isReply
            onUpdatePost={handleUpdatePost}
            {...rest}
          />
        </Fragment>
      ) : null}
    </Root>
  );
};

export default Replies;
