import React, { Fragment, useCallback } from 'react';
import { Segment, Dimmer, Loader, Button } from 'semantic-ui-react';
import { FileList } from '../modules';
import usePostAttachDownload from './hooks/usePostAttachDownload';
import usePostAttachRemove from './hooks/usePostAttachRemove';
import styled from 'styled-components';
import MultiDownloadButton from '../modules/MultiDownloadButton';

FileList.preload();

const OptionButton = styled(Button)`
  &&& {
    padding: 6px;
    background-color: transparent;
  }
`;

const FileDeleteButton = ({ onDeleteClick }) => (
  <OptionButton icon="delete" onClick={onDeleteClick} />
);

const FileDownloadButton = ({ onDownloadClick }) => (
  <OptionButton icon="cloud download" onClick={onDownloadClick} />
);

const PostAttachList = ({
  loading,
  attachs,
  postId = 0,
  projectId,
  onDeleteClick = f => f,
  isEdit = false
}) => {
  const attachDown = usePostAttachDownload({
    postId,
    projectId
  });
  const attachRemove = usePostAttachRemove({
    postId,
    projectId
  });

  const handleRemoveAttach = useCallback(
    async postAttachmentId => {
      let onDelete = postId === 0;
      if (postId !== 0) {
        onDelete = await attachRemove.remove(postAttachmentId);
      }
      if (onDelete) {
        onDeleteClick(postAttachmentId);
      }
    },
    [attachs]
  );

  const getDownloadApi = useCallback(
    id => {
      return `api/projects/${projectId}/posts/${postId}/attach/${id}`;
    },
    [projectId, postId]
  );

  return (
    <Fragment>
      {attachs.length === 0 ? null : (
        <Segment>
          <Dimmer active={attachDown.loading} inverted>
            <Loader size="small">Loading</Loader>
          </Dimmer>
          <FileList
            files={attachs}
            // 수정중일떈 삭제만 가능하도록
            onDownloadClick={isEdit ? undefined : attachDown.download}
            onDeleteClick={isEdit ? handleRemoveAttach : undefined}
            deleteButton={isEdit ? FileDeleteButton : undefined}
            downloadButton={isEdit ? undefined : FileDownloadButton}
            lazyThumbnail={false}
            hasContextMenu={false}
            MultiDownloadButton={
              isEdit || attachs.length < 2
                ? undefined
                : MultiDownloadButton({ getDownloadApi })
            }
          />
        </Segment>
      )}
    </Fragment>
  );
};

export default PostAttachList;
