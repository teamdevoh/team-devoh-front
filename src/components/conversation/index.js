import { asyncComponent } from '../modules';

const ConversationDashBoard = asyncComponent(() =>
  import('./ConversationDashBoard')
);

export { ConversationDashBoard };
