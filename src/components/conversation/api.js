import helpers from '../../helpers';

const fetchPosts = ({ projectId, offset, limit, search }) => {
  const params = helpers.qs.stringify({
    offset,
    limit,
    search
  });
  return helpers.Service.get(`api/projects/${projectId}/posts?${params}`);
};

const fetchStucks = ({ projectId, offset, limit, search }) => {
  const params = helpers.qs.stringify({
    offset,
    limit,
    search
  });
  return helpers.Service.get(`api/projects/${projectId}/stucks?${params}`);
};

const addPosts = ({ projectId, model }) => {
  return helpers.Service.post(`api/projects/${projectId}/posts`, model);
};

const addReplies = ({ projectId, parentPostId, model }) => {
  return helpers.Service.post(
    `api/projects/${projectId}/posts/${parentPostId}/replies`,
    model
  );
};

const updatePosts = ({ projectId, postId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/posts/${postId}`,
    model
  );
};

const updatePostStuck = ({ projectId, postId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/posts/${postId}/stuck`,
    model
  );
};

const updateReplies = ({ projectId, parentPostId, postId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/posts/${parentPostId}/replies/${postId}`,
    model
  );
};

const fetchReplies = ({
  projectId,
  parentPostId
  // offset, limit, search
}) => {
  return helpers.Service.get(
    `api/projects/${projectId}/posts/${parentPostId}/replies`
  );
};

const removePosts = ({ projectId, postId }) => {
  return helpers.Service.delete(`api/projects/${projectId}/posts/${postId}`);
};

const removeReplies = ({ projectId, parentPostId, postId }) => {
  return helpers.Service.delete(
    `api/projects/${projectId}/posts/${parentPostId}/replies/${postId}`
  );
};

const addPostAttach = ({ projectId, postId, model }) => {
  return helpers.Service.post(
    `api/projects/${projectId}/posts/${postId}/attach`,
    model
  );
};

const removePostAttach = ({ projectId, postId, postAttachmentId }) => {
  return helpers.Service.delete(
    `api/projects/${projectId}/posts/${postId}/attach/${postAttachmentId}`
  );
};

export default {
  fetchPosts,
  fetchStucks,
  addPosts,
  addReplies,
  fetchReplies,
  updatePosts,
  updatePostStuck,
  updateReplies,
  removePosts,
  removeReplies,
  addPostAttach,
  removePostAttach
};
