import React, { memo, useState, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { useInView } from 'react-intersection-observer';
import { Loader, Container, Responsive, Image } from 'semantic-ui-react';
import { useConversationList, useStuckList } from './hooks';
import ConversationDashBoardList from './ConversationDashBoardList';
import api from './api';
import ConversationEditor from './ConversationEditor';
import styled from 'styled-components';
import Stucks from './Stocuks';
import { Modal } from '../modal';
import EditorLoader from './EditorLoader';

const Root = styled.div`
  & .conversation-contents.text {
    overflow-x: auto;
  }

  & .conversation-contents img,
  & .conversation-stuck .contents img,
  & .mde-preview-content img {
    height: 13rem;
    cursor: pointer;
  }
`;

const ConversationRoot = styled.div`
  height: 100vh;
  width: 65%;
  display: flex;
  flex-direction: column;

  /* Mobile Device */
  @media all and (max-width: 960px) {
    width: 100%;
    margin: 0 10px;
  }

  @media only screen and (max-width: 991px) and (min-width: 768px) {
    & .ui.container {
      width: 100%;
    }
  }
`;

const EditorWrap = styled(Container)`
  position: relative;
`;

const ConversationDashBoardBody = () => {
  const projectId = useSelector(state => state.project.selected.key);
  const [ref, inView, entry] = useInView({
    root: document.querySelector('#conversationWrapper'),
    rootMargin: '500px',
    threshold: 0
  });
  const listScroll = useRef();
  const scrollH = useRef(null);
  const [filter, setFilter] = useState({});
  const [stuckFilter, setStuckFilter] = useState({});
  const [newAttachs, setNewAttachs] = useState([]);
  const [onSave, setOnSave] = useState(false);
  const [showImg, setShowImg] = useState({
    open: false,
    src: null,
    alt: null
  });
  const [
    { items, loading, total, start, refresh },
    { setCb, setItems }
  ] = useConversationList({
    filter
  });
  const [
    {
      items: stuckItems,
      loading: stuckLoading
      // total: stuckTotal,
      // start: stuckStart,
      // refresh: stuckRefresh
    }
  ] = useStuckList({ filter: stuckFilter });

  const refreshPostList = () => {
    // 글 작성 후 맨아래로 스크롤 내리기 위한 콜백 함수
    setCb(() => () => {
      listScroll.current.scrollTo(null, listScroll.current.scrollHeight);
      setScrollH(listScroll.current.scrollHeight);
    });
    setFilter({ ...filter });
    setCb(null);
  };

  const handleClose = () => {
    setShowImg({
      open: false,
      src: null,
      alt: null
    });
  };

  const handleOnImgClick = e => {
    if (e.target.tagName === 'IMG') {
      const { src, alt } = e.target;
      const open = true;
      setShowImg({
        open,
        src,
        alt
      });
    }
  };

  const setScrollH = height => {
    scrollH.current = height;
  };

  const handleRefeshStucks = ({ refreshPost = false } = {}) => {
    setStuckFilter({});

    if (refreshPost) {
      refreshPostList();
    }
  };

  const handleUpdatePost = (updatePostId, newData) => {
    setItems(
      items.map(data => {
        const { postId } = data;
        if (postId === updatePostId) {
          return newData;
        }
        return data;
      })
    );
  };

  /**
   * 확인버튼
   */
  const handleSubmit = async (value, newFileStorageIds, isStuck = false) => {
    setOnSave(true);
    if (!loading) {
      const res = await api.addPosts({
        projectId,
        model: {
          projectId,
          contents: value,
          parentPostId: 0,
          fileStorageIds: newFileStorageIds,
          isStuck
        }
      });
      const { data } = res;

      if (data) {
        setOnSave(false);
        refreshPostList();
        setNewAttachs([]);

        if (data.isStuck) {
          handleRefeshStucks();
        }
      }
    }
  };

  const handleAddAttachs = newItems => {
    setNewAttachs([...newAttachs, ...newItems]);
  };

  const handleAttachDelete = postAttachmentId => {
    setNewAttachs(
      newAttachs.filter(file => {
        return file.key !== postAttachmentId;
      })
    );
  };

  useEffect(
    () => {
      if (inView && items.length < total && entry.isIntersecting) {
        refresh({
          offset: start,
          callBack: () => {
            setScrollH(listScroll.current.scrollHeight - scrollH.current);
            listScroll.current.scrollTo(null, scrollH.current);
          }
        });
      }
    },
    [inView]
  );

  useEffect(
    () => {
      // 첫 리스트 받아오고 나서 맨아래로 스크롤
      if (!loading && start === 0 && items.length > 0) {
        const { scrollHeight } = listScroll.current;
        listScroll.current.scrollTo(null, scrollHeight);
        setScrollH(scrollHeight);
      }
    },
    [loading]
  );

  return (
    <Root style={{ display: 'flex' }}>
      <ConversationRoot className="conversation">
        <Container style={{ height: '77%' }} className="margin-0">
          <div
            id="conversationWrapper"
            style={{
              position: 'relative',
              height: '100%',
              overflowY: 'auto',
              border: '1px solid rgba(34, 36, 38, 0.1)'
            }}
            ref={listScroll}
          >
            <div style={{ position: 'absolute', width: '100%' }}>
              <div ref={ref}>
                <Loader active={loading} inline="centered" />
              </div>
              <ConversationDashBoardList
                items={items}
                onStuckRefresh={handleRefeshStucks}
                onShowImg={handleOnImgClick}
                onUpdatePost={handleUpdatePost}
              />
            </div>
          </div>
        </Container>
        <EditorWrap className="margin-0">
          {onSave && <EditorLoader />}
          <ConversationEditor
            projectId={projectId}
            hasSaveButton
            onSubmit={handleSubmit}
            loading={loading}
            attachs={newAttachs}
            onAddAttachCb={handleAddAttachs}
            onAttachDeleteClick={handleAttachDelete}
          />
        </EditorWrap>
      </ConversationRoot>
      <Responsive
        minWidth={1024}
        style={{ width: '35%' }}
        className="conversation-stuck"
      >
        <Stucks
          items={stuckItems}
          projectId={projectId}
          loading={stuckLoading}
          onShowImg={handleOnImgClick}
          refresh={handleRefeshStucks}
        />
      </Responsive>
      <Modal
        open={showImg.open}
        onOK={handleClose}
        title=""
        content={<Image src={showImg.src} alt={showImg.alt} fluid />}
      />
    </Root>
  );
};

export default memo(ConversationDashBoardBody);
