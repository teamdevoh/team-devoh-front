import React, { useCallback, memo } from 'react';
import { useFormatMessage } from '../../hooks';
import { Container, Label, Responsive } from 'semantic-ui-react';
import { DataGrid } from '../data-grid';
import { code } from '../../constants';
import { StyledCSVExport } from '../export-tdm/styles';
import helpers from '../../helpers';
import {
  FlexDiv,
  StyledDetail,
  RightButtonWrap
} from '../sample-barcode/styles';

const QueryList = ({
  loading,
  items,
  setFilteredQueries,
  onActivityClick,
  onRowClick
}) => {
  const isMobile =
    Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;
  const t = useFormatMessage();

  const handleActivityClick = useCallback(row => {
    onActivityClick(row);
  }, []);

  const columns = [
    {
      key: 'siteName',
      name: t('site'),
      width: 150
    },
    {
      key: 'subjectNo',
      name: 'Subject No'
    },
    {
      key: 'initial',
      name: t('collection.subject.initial')
    },
    {
      key: 'status',
      name: t('status')
    },
    {
      key: 'activityName',
      name: t('activity'),
      formatter: ({ value, isScrolling, row }) => (
        <a
          style={{ cursor: 'pointer' }}
          onClick={() => {
            handleActivityClick(row);
          }}
        >
          {value}
        </a>
      )
    },
    {
      key: 'openAuthor',
      name: t('user')
    },
    {
      key: 'queryDate',
      name: 'Query Date'
    },
    {
      key: 'queryStatusName',
      name: t('query.status'),
      formatter: ({ value, isScrolling, row }) => {
        const statusValue = row.queryStatusCodeId;
        const color =
          statusValue === code.QueryOpen
            ? 'blue'
            : statusValue === code.QueryAnswer
              ? 'red'
              : 'green';
        return <Label color={color}>{value}</Label>;
      }
    }
  ];

  const handleExport = async () => {
    try {
      const data = items.map((item, index) => {
        const { openAuthor } = item;

        return {
          ...item,
          no: index + 1,
          openAuthor: openAuthor.replaceAll(',', ' ')
        };
      });

      const noColumns = [
        {
          key: 'no',
          name: 'No'
        }
      ];
      const csvColumns = noColumns.concat(columns);

      const fields = csvColumns.map(item => ({ key: item.key }));
      const fieldNames = csvColumns.map(item => item.name);

      helpers.util.exportCSV({
        fileName: `Queries List.csv`,
        data,
        fields,
        fieldNames
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Container>
      <FlexDiv>
        <Label size="big">
          Count
          <StyledDetail>{items.length}</StyledDetail>
        </Label>
        <RightButtonWrap>
          <StyledCSVExport
            positive
            icon="download"
            onClick={handleExport}
            content="CSV Export"
          />
        </RightButtonWrap>
      </FlexDiv>
      <DataGrid
        columns={columns}
        rows={items}
        loading={loading}
        minHeight={isMobile ? 200 : 600}
        onRowClick={onRowClick}
        rowKey="activityQueryId"
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        onGridSorted={orderedItems => setFilteredQueries(orderedItems)}
      />
    </Container>
  );
};

export default memo(QueryList);
