import React, { Fragment, useCallback } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Icon, Popup } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { SortButton } from '../button';

const DashBoardTool = ({ onRequestSort, order }) => {
  const t = useFormatMessage();
  const history = useHistory();
  const { projectId } = useParams();

  const handleSubjectClick = useCallback(() => {
    history.push(`/project/${projectId}/subject/0/step/0`);
  }, []);

  // const handleChartClick = useCallback(() => {
  //   history.push(`/project/${projectId}/collection/chart`);
  // }, []);

  const handleCalendarClick = useCallback(() => {
    history.push(`/project/${projectId}/collection/calendar`);
  }, []);

  const handleQuestionClick = useCallback(() => {
    history.push(`/project/${projectId}/collection/cohort/query`);
  }, []);

  const separator = ' | ';

  return (
    <Fragment>
      <SortButton onClick={onRequestSort} curOrder={order} order="asc" />
      {separator}
      <SortButton onClick={onRequestSort} curOrder={order} order="desc" />
      {separator}
      <Popup
        trigger={
          <Icon
            name="user plus"
            size="large"
            className="link"
            onClick={handleSubjectClick}
          />
        }
        content={t('add.new.subject')}
        inverted
      />
      {separator}
      {/* <Icon
        name="chart bar outline"
        size="large"
        className="link"
        onClick={handleChartClick}
      />
      {separator} */}
      <Popup
        trigger={
          <Icon
            name="calendar alternate outline"
            size="large"
            className="link"
            onClick={handleCalendarClick}
          />
        }
        content={t('subject.visit.calendar')}
        inverted
      />
      {separator}
      <Popup
        trigger={
          <Icon
            name="question circle outline"
            size="large"
            className="link"
            onClick={handleQuestionClick}
          />
        }
        content={t('query.view')}
        inverted
      />
    </Fragment>
  );
};

export default DashBoardTool;
