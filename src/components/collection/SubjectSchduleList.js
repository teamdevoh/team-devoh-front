import React from 'react';
import { List, Icon } from 'semantic-ui-react';
import styled from 'styled-components';

const SchduleItem = styled(List.Item)`
  &&&&& {
    padding: 12px;
    cursor: pointer;

    ${({ eventcolor }) => {
      return `background-color: ${eventcolor}`;
    }};

    &:hover {
      background-color: #f5f5f5;
    }
  }
`;

const ColorIcon = styled(Icon)`
  &&&&& {
    margin-right: 5px;
    ${({ eventcolor }) => {
      return `color: ${eventcolor}`;
    }};
  }
`;

const SubjectSchduleList = ({ list, onRowEventClick }) => {
  const handleItemClick = itemData => () => {
    onRowEventClick(itemData);
  };

  return (
    <List celled>
      {list.map(item => {
        const { title, subjectVisitId, color, itemData } = item;
        return (
          <SchduleItem
            key={subjectVisitId}
            eventcolor={color}
            onClick={handleItemClick(itemData)}
          >
            <ColorIcon name="circle" eventcolor={color} />
            {title}
          </SchduleItem>
        );
      })}
    </List>
  );
};

export default SubjectSchduleList;
