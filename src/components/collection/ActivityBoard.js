import React, { Fragment } from 'react';
import helpers from '../../helpers';
import { Icon } from 'semantic-ui-react';
import { SummaryWrapper } from './';
import { useFormatMessage } from '../../hooks';
import { DataGrid } from '../data-grid';
import { code, role } from '../../constants';

const ActivityBoard = ({ projectId, items, loading, activityCount }) => {
  const t = useFormatMessage();

  const hasRoleCaseReview = helpers.Identity.hasRole(role.CaseReview_Read);

  const columns = [
    {
      key: 'subjectNo',
      name: t('collection.subject.no'),
      width: hasRoleCaseReview ? 120 : 90,
      formatter: ({ value, isScrolling, row }) => (
        <Fragment>
          <a
            style={{ cursor: 'pointer' }}
            onClick={() => {
              helpers.history.push(
                `/project/${projectId}/subject/${row.subjectId}/step/0`
              );
            }}
          >
            {value}
          </a>
          {hasRoleCaseReview && (
            <Icon
              name="address card"
              size="big"
              className="link"
              onClick={() => {
                helpers.history.push(
                  `/project/${projectId}/subject/${row.subjectId}/casereview`
                );
              }}
            />
          )}
        </Fragment>
      )
    },
    { key: 'initial', name: t('collection.subject.initial'), width: 90 },
    { key: 'diagnosis', name: t('activity.diagnosis.name') },
    {
      key: 'finalDiagnosis',
      name: t('activity.caseConclusion.digFinCodeId')
    },
    {
      key: 'subjectStatusName',
      name: t('status'),
      width: 150,
      formatter: ({ value, isScrolling, row }) => (
        <div>
          {row.subjectStatusCode === code.SubjectCompletion && (
            <Icon name="check circle outline" color="green" />
          )}
          {row.subjectStatusCode === code.SubjectDropout && (
            <Icon name="times circle outline" color="red" />
          )}
          {row.subjectStatusCode === code.SubjectOngoing && (
            <Icon loading name="circle notch" />
          )}
          {value}
        </div>
      )
    },
    {
      key: 'interview',
      formatter: SummaryWrapper,
      hidden: activityCount.interview > 0 ? false : true,
      width: activityCount.interview > 0 ? 140 : -1,
      headerRenderer: header => {
        return (
          <div>
            Basic Patient
            <br />
            Information ({activityCount.interview})
          </div>
        );
      }
    },
    {
      key: 'medication',
      formatter: SummaryWrapper,
      hidden: activityCount.medication > 0 ? false : true,
      width: activityCount.medication > 0 ? 140 : -1,
      headerRenderer: header => {
        return (
          <div>
            Medication
            <br />
            History ({activityCount.medication})
          </div>
        );
      }
    },
    {
      key: 'test',
      formatter: SummaryWrapper,
      hidden: activityCount.test > 0 ? false : true,
      width: activityCount.test > 0 ? 140 : -1,
      headerRenderer: header => {
        return (
          <div>
            Microbiological
            <br />
            Exam ({activityCount.test})
          </div>
        );
      }
    },
    {
      key: 'lab',
      formatter: SummaryWrapper,
      hidden: activityCount.lab > 0 ? false : true,
      width: activityCount.lab > 0 ? 140 : -1,
      headerRenderer: header => {
        return (
          <div>
            Lab & Radiologic
            <br />
            Findings ({activityCount.lab})
          </div>
        );
      }
    },
    {
      key: 'tdm',
      formatter: SummaryWrapper,
      hidden: activityCount.tdm > 0 ? false : true,
      width: activityCount.tdm > 0 ? 140 : -1,
      headerRenderer: header => {
        return <div>1st TDM ({activityCount.tdm})</div>;
      }
    },
    {
      key: 'futdm',
      formatter: SummaryWrapper,
      hidden: activityCount.futdm > 0 ? false : true,
      width: activityCount.futdm > 0 ? 140 : -1,
      headerRenderer: header => {
        return <div>F/U TDM ({activityCount.futdm})</div>;
      }
    },
    {
      key: 'adr',
      formatter: SummaryWrapper,
      hidden: activityCount.adr > 0 ? false : true,
      width: activityCount.adr > 0 ? 140 : -1,
      headerRenderer: header => {
        return <div>ADR ({activityCount.adr})</div>;
      }
    },
    {
      key: 'caseConclusion',
      formatter: SummaryWrapper,
      hidden: activityCount.caseConclusion > 0 ? false : true,
      width: activityCount.caseConclusion > 0 ? 140 : -1,
      headerRenderer: header => {
        return (
          <div>
            Monitoring & <br />
            Evaluation ({activityCount.caseConclusion})
          </div>
        );
      }
    }
  ];

  return (
    <DataGrid
      rowKey="subjectId"
      columns={columns}
      rows={items}
      minHeight={580}
      headerRowHeight={50}
      loading={loading}
    />
  );
};

export default ActivityBoard;
