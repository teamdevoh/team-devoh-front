import React from 'react';
import ActivitySummary from './ActivitySummary';
import helpers from '../../helpers';
import { useParams } from 'react-router-dom';

const SummaryWrapper = props => {
  const { row, value } = props;
  const { projectId } = useParams();

  const handleStatusClick = () => {
    helpers.history.push(
      `/project/${projectId}/subject/${row.subjectId}/activitygroup/${
        value.activityGroupId
      }`
    );
  };

  const handleModalStatusClick = item => () => {
    helpers.history.push(
      `/project/${projectId}/subject/${row.subjectId}/activitygroup/${
        value.activityGroupId
      }?projectActivityId=${item.projectActivityId}&activityId=${
        item.activityId
      }`
    );
  };

  return (
    <ActivitySummary
      {...props}
      rowMasterKey="subjectId"
      modalTitle={`${row.subjectNo}/${row.initial}`}
      onStatusClick={handleStatusClick}
      onModalStatusClick={handleModalStatusClick}
    />
  );
};
export default SummaryWrapper;
