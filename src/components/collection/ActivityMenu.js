import React, { useCallback, useEffect } from 'react';
import { Grid, Menu, Select, Icon, Loader, Button } from 'semantic-ui-react';
import { useParams, useLocation } from 'react-router-dom';
import { FormContainer } from '../activity';
import helpers from '../../helpers';
import { useActivityGroupList, useActivitiesOfGroupList } from './hooks';
import StatusWrapper from './StatusWrapper';
import { RoleAware } from '../auth';
import { role } from '../../constants';

const ActivityMenu = ({
  projectActivityId,
  activityGroupId,
  subjectId,
  projectSiteId
}) => {
  const location = useLocation();
  const { projectId } = useParams();
  const [activityGroups] = useActivityGroupList({
    projectId,
    subjectId,
    projectSiteId
  });
  const query = helpers.qs.parse(location.search);
  const [{ items, clearGroups, fetch, loading }] = useActivitiesOfGroupList({
    activityGroupId,
    subjectId
  });

  const goActivity = ({ projectActivityId, activityId }) => {
    helpers.history.push(
      `/project/${projectId}/subject/${subjectId}/activitygroup/${activityGroupId}?projectActivityId=${projectActivityId}&activityId=${activityId}`
    );
  };

  const handleItemClick = useCallback(
    (event, data) => {
      const item = data.item;
      goActivity({
        projectActivityId: item.projectActivityId,
        activityId: item.activityId
      });
    },
    [activityGroupId, subjectId]
  );

  const handleClickBackToList = useCallback(
    () => {
      helpers.history.push(`/project/${projectId}/collection/cohort`);
    },
    [projectId]
  );

  useEffect(() => {
    if (items.length > 0 && subjectId > 0) {
      // For default select item
      if (projectActivityId === void 0) {
        const firstItem = items[0];
        goActivity({
          projectActivityId: firstItem.projectActivityId,
          activityId: firstItem.activityId
        });
      }
    }
  });

  const handleGroupChange = useCallback(
    (event, data) => {
      clearGroups();
      helpers.history.push(
        `/project/${projectId}/subject/${subjectId}/activitygroup/${data.value}`
      );
    },
    [projectId, subjectId]
  );

  const handleCaseReviewClick = useCallback(
    () => {
      helpers.history.push(
        `/project/${projectId}/subject/${subjectId}/casereview`
      );
    },
    [projectId, subjectId]
  );

  const handleSubjectInfoClick = useCallback(
    () => {
      helpers.history.push(`/project/${projectId}/subject/${subjectId}/step/0`);
    },
    [projectId, subjectId]
  );

  const handleVisitScheduleClick = useCallback(
    () => {
      helpers.history.push(`/project/${projectId}/subject/${subjectId}/step/1`);
    },
    [projectId, subjectId]
  );

  useEffect(
    () => {
      return clearGroups;
    },
    [projectId]
  );

  return (
    <Grid stackable>
      <Grid.Column width={4}>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Select
                fluid
                options={activityGroups}
                value={Number(activityGroupId)}
                onChange={handleGroupChange}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              {loading && <Loader active />}
              <Menu fluid vertical tabular stackable>
                {items.map(item => {
                  return (
                    <Menu.Item
                      key={item.projectActivityId}
                      active={
                        Number(projectActivityId) === item.projectActivityId
                      }
                      item={item}
                      onClick={handleItemClick}
                    >
                      <Grid>
                        <Grid.Column width="ten">{item.name}</Grid.Column>
                        <Grid.Column width="six">
                          <StatusWrapper
                            activityStatusCodeId={item.activityStatusCodeId}
                            queryStatusCodeId={item.queryStatusCodeId}
                            hasNote={item.hasNote}
                            onClick={() => {}}
                          />
                        </Grid.Column>
                      </Grid>
                    </Menu.Item>
                  );
                })}
              </Menu>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Column>

      <Grid.Column width={12}>
        <Button icon labelPosition="left" onClick={handleClickBackToList}>
          <Icon name="arrow left" size="large" />
          Go back to Collection List
        </Button>
        <Button icon labelPosition="left" onClick={handleSubjectInfoClick}>
          <Icon name="user" size="large" />
          Subject Information
        </Button>
        <Button icon labelPosition="left" onClick={handleVisitScheduleClick}>
          <Icon name="calendar alternate outline" size="large" />
          Visit Schedule
        </Button>
        <RoleAware allowedRole={role.CaseReview_Read}>
          <Button
            icon
            labelPosition="right"
            color="teal"
            onClick={handleCaseReviewClick}
          >
            Case Review
            <Icon name="address card" size="large" />
          </Button>
        </RoleAware>
        {Number(query.activityId) > 0 && (
          <FormContainer
            activityId={Number(query.activityId)}
            projectSiteId={projectSiteId}
            fetchActivityMenu={fetch}
          />
        )}
      </Grid.Column>
    </Grid>
  );
};

export default ActivityMenu;
