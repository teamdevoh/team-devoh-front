import React, { Fragment, useState } from 'react';
import { Button, Tab } from 'semantic-ui-react';
import { useRouteMatch } from 'react-router-dom';
import ChartTotalTab from './ChartTotalTab';

const CollectionChartTab = () => {
  const match = useRouteMatch();
  const { projectId } = match.params;
  const [selectedTab, setSelectedTab] = useState('1');

  const handleOnClick = (e, { value }) => {
    setSelectedTab(value);
  };

  return (
    <Fragment>
      <Button.Group>
        <Button value="1" primary={selectedTab === '1'} onClick={handleOnClick}>
          Total
        </Button>
        <Button value="2" primary={selectedTab === '2'} onClick={handleOnClick}>
          2
        </Button>
        <Button value="3" primary={selectedTab === '3'} onClick={handleOnClick}>
          3
        </Button>
      </Button.Group>
      {selectedTab === '1' && (
        <Tab.Pane>
          <ChartTotalTab projectId={projectId} />
        </Tab.Pane>
      )}
    </Fragment>
  );
};

export default CollectionChartTab;
