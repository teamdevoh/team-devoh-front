import React, { Fragment, useCallback, useEffect, useState } from 'react';
import {
  Popup,
  Button,
  List,
  Divider,
  Checkbox,
  Icon
} from 'semantic-ui-react';
import { useDispatch, useSelector } from 'react-redux';
import { subject } from '../../states';
import { code } from '../../constants';
import { useFormatMessage } from '../../hooks';
import { useLocation } from 'react-router-dom';
import helpers from '../../helpers';

const ongoingMonth = ['3', '6', '9', '12', '15', '18', '21', '24'];
const caseConclusionMonth = ['6', '12', '24'];

const MonthFilter = () => {
  const t = useFormatMessage();
  const dispatch = useDispatch();
  const location = useLocation();
  const { ongoingMonths, caseConMonths } = useSelector(
    state => state.subject.filter
  );
  const [ongoing, setOngoing] = useState(new Set([...ongoingMonths]));
  const [caseConclusion, setCaseConclusion] = useState(
    new Set([...caseConMonths])
  );

  const handleOngoingAllCheckChange = (e, { value, checked }) => {
    if (checked) {
      setOngoing(new Set([...ongoingMonth]));
    } else {
      setOngoing(new Set());
    }
  };

  const handleCaseConclusionAllCheckChange = (e, { value, checked }) => {
    if (checked) {
      setCaseConclusion(new Set([...caseConclusionMonth]));
    } else {
      setCaseConclusion(new Set());
    }
  };

  const handleOngoingCheckChange = (e, { value, checked }) => {
    const newOngoing = new Set([...ongoing]);

    if (checked) {
      newOngoing.add(value);
    } else {
      newOngoing.delete(value);
    }
    setOngoing(newOngoing);
  };

  const handleCaseConclusionCheckChange = (e, { value, checked }) => {
    const newCaseConclusion = new Set([...caseConclusion]);

    if (checked) {
      newCaseConclusion.add(value);
    } else {
      newCaseConclusion.delete(value);
    }
    setCaseConclusion(newCaseConclusion);
  };

  const handleOngoingFilterClose = () => {
    const sortedOngoing = [...ongoing].sort((a, b) => a - b);

    if (ongoingMonths.join(',') !== sortedOngoing.join(',')) {
      dispatch(
        subject.actions.filter({
          subjectStatusCode: code.SubjectOngoing,
          ongoingMonths: [...sortedOngoing],
          caseConMonths: [],
          queryStatusQuery: ''
        })
      );
    }
  };

  const handleCaseConclusionFilterClose = () => {
    const sortedCaseConclusion = [...caseConclusion].sort((a, b) => a - b);

    if (caseConMonths.join(',') !== sortedCaseConclusion.join(',')) {
      dispatch(
        subject.actions.filter({
          subjectStatusCode: code.SubjectCompletion,
          ongoingMonths: [],
          caseConMonths: [...sortedCaseConclusion],
          queryStatusQuery: ''
        })
      );
    }
  };

  useEffect(
    () => {
      setOngoing(new Set([...ongoingMonths]));
    },
    [ongoingMonths]
  );

  useEffect(
    () => {
      setCaseConclusion(new Set([...caseConMonths]));
    },
    [caseConMonths]
  );

  useEffect(
    () => {
      const query = helpers.qs.parse(location.search);
      const { ongoingmonths, caseconmonths } = query;

      const monthFilter = {
        ongoingMonths: [],
        caseConMonths: []
      };
      let subjectStatusCode = 0;

      if (!!ongoingmonths) {
        if (ongoingmonths === 'all') {
          monthFilter['ongoingMonths'] = [...ongoingMonth];
        } else {
          monthFilter['ongoingMonths'] = [...ongoingmonths.split(',')];
        }
        subjectStatusCode = code.SubjectOngoing;
      } else if (!!caseconmonths) {
        if (caseconmonths === 'all') {
          monthFilter['caseConMonths'] = [...caseConclusionMonth];
        } else {
          monthFilter['caseConMonths'] = [...caseconmonths.split(',')];
        }
        subjectStatusCode = code.SubjectCompletion;
      }

      dispatch(
        subject.actions.filter({
          subjectStatusCode,
          ...monthFilter,
          queryStatusQuery: ''
        })
      );
    },
    [location]
  );

  const generateSelectedMonths = months => {
    months = [...months].sort((a, b) => a - b);
    let monthsStr = ' - ';
    let ellipsis = null;

    if (months.length > 0) {
      if (months.length > 3) {
        monthsStr = months.slice(0, 3).join(', ');
        ellipsis = (
          <Popup
            trigger={
              <Icon
                style={{ marginRight: 0, marginLeft: '8px' }}
                name="ellipsis horizontal"
                size="small"
                circular
                inverted
              />
            }
            content={months.slice(3).join(', ')}
            position="top left"
          />
        );
      } else {
        monthsStr = months.join(', ');
      }
    }

    return (
      <Fragment>
        {monthsStr}
        {ellipsis}
      </Fragment>
    );
  };

  const generateMonthFilterItems = useCallback(
    (months, seletedMonths, onChange) => {
      return months.map(month => {
        return (
          <List.Item key={month}>
            <Checkbox
              label={`${month} ${t('months')}`}
              value={month}
              onChange={onChange}
              checked={seletedMonths.has(month.toString())}
            />
          </List.Item>
        );
      });
    },
    []
  );

  return (
    <Fragment>
      <Popup
        trigger={
          <Button basic={ongoing.size === 0} style={{ minHeight: '43px' }}>
            {t('ongoing.months')} ({generateSelectedMonths([...ongoing])})
          </Button>
        }
        position="bottom left"
        on="click"
        basic
        onClose={handleOngoingFilterClose}
      >
        <List>
          <List.Item>
            <Checkbox
              label={t('all')}
              value="all"
              indeterminate={
                ongoing.size !== 0 && ongoing.size !== ongoingMonth.length
              }
              checked={
                ongoing.size !== 0 && ongoing.size === ongoingMonth.length
              }
              onChange={handleOngoingAllCheckChange}
            />
          </List.Item>
          <Divider />
          {generateMonthFilterItems(
            ongoingMonth,
            ongoing,
            handleOngoingCheckChange
          )}
        </List>
      </Popup>
      <Popup
        trigger={
          <Button
            basic={caseConclusion.size === 0}
            style={{ minHeight: '43px' }}
          >
            {t('caseConclusion.months')} (
            {generateSelectedMonths([...caseConclusion])})
          </Button>
        }
        position="bottom left"
        on="click"
        basic
        onClose={handleCaseConclusionFilterClose}
      >
        <List>
          <List.Item>
            <Checkbox
              label={t('all')}
              value="all"
              indeterminate={
                caseConclusion.size !== 0 &&
                caseConclusion.size !== caseConclusionMonth.length
              }
              checked={
                caseConclusion.size !== 0 &&
                caseConclusion.size === caseConclusionMonth.length
              }
              onChange={handleCaseConclusionAllCheckChange}
            />
          </List.Item>
          <Divider />
          {generateMonthFilterItems(
            caseConclusionMonth,
            caseConclusion,
            handleCaseConclusionCheckChange
          )}
        </List>
      </Popup>
    </Fragment>
  );
};

export default MonthFilter;
