import React, { useEffect, useState } from 'react';
import { Container, Grid, Dropdown } from 'semantic-ui-react';
import CollectionChart from './CollectionChart';
import { useFormatMessage } from '../../hooks';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';

const ChartTotalTab = ({ projectId }) => {
  const [item] = useMyProjectSiteList({ projectId });
  const t = useFormatMessage();
  const [projectSiteId, setProjectSiteId] = useState(0);

  const handleChange = (e, { value }) => {
    setProjectSiteId(value);
  };

  useEffect(
    () => {
      item.unshift({
        text: t('common.all'),
        value: 0
      });
    },
    [item]
  );

  return (
    <Container fluid>
      <Dropdown
        options={item}
        selection
        value={projectSiteId}
        onChange={handleChange}
        placeholder={t('common.all')}
      />
      <Grid stackable>
        <Grid.Row>
          <Grid.Column widescreen={8} mobile={16}>
            <CollectionChart
              projectId={projectId}
              projectSiteId={projectSiteId}
            />
          </Grid.Column>
          <Grid.Column widescreen={8} mobile={16}>
            <CollectionChart
              projectId={projectId}
              projectSiteId={projectSiteId}
              type="month"
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column widescreen={8} mobile={16}>
            <CollectionChart
              projectId={projectId}
              projectSiteId={projectSiteId}
              type="week"
            />
          </Grid.Column>
          <Grid.Column widescreen={8} mobile={16} />
        </Grid.Row>
      </Grid>
    </Container>
  );
};

export default ChartTotalTab;
