import React, { useCallback } from 'react';
import { List, Icon } from 'semantic-ui-react';
import { useHistory, useParams } from 'react-router-dom';

const SubjectListItem = ({ item }) => {
  const history = useHistory();
  const { projectId } = useParams();

  const handleClick = useCallback(() => {
    history.push(
      `/project/${projectId}/collection/subject/${item.subjectId}/step/0`
    );
  }, []);

  return (
    <List.Item>
      <List.Content floated="right">
        <Icon size="big" name="question circle outline" />
      </List.Content>
      <List.Content>
        <List.Header as="a" onClick={handleClick}>
          {item.subjectPrefix}
          {item.subjectNo}
        </List.Header>
        <List.Description>
          <a>{item.initial}</a> status <a>{item.subjectStatusName}</a>
        </List.Description>
      </List.Content>
    </List.Item>
  );
};

export default SubjectListItem;
