import helpers from '../../helpers';

const fetchSubjectActivitiesBySort = ({
  projectId,
  projectSiteId,
  subjectStatusCode,
  search,
  order,
  queryStatusQuery,
  ongoingMonths,
  caseConMonths
}) => {
  const params = helpers.qs.stringify({
    subjectStatusCode,
    search,
    ongoingMonths,
    caseConMonths
  });

  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/subjects.status.sort?${params}&${queryStatusQuery}&orderBy=${order}`
  );
};

const fetchActivitiesOfGroup = ({ activityGroupId, subjectId }) => {
  const params = helpers.qs.stringify({
    subjectId
  });

  return helpers.Service.get(
    `api/activitygroups/${activityGroupId}/activities?${params}`
  );
};

const fetchActivitiesOfProject = ({ projectId }) => {
  return helpers.Service.get(`api/projects/${projectId}/activitygroups`);
};

const fetchActivityStatusOfSubject = ({
  projectId,
  subjectId,
  projectSiteId
}) => {
  return helpers.Service.get(
    `api/subjects/${subjectId}/activities.status?projectSiteId=${projectSiteId}&$select=subjectid&
    $expand=Interview,Medication,Test,Lab,TDM,ADR,CaseConclusion,FUTDM`
  );
};

const fetchSubjectVisits = ({ subjectId }) => {
  return helpers.Service.get(`api/subjects/${subjectId}/visits`);
};

const addSubject = ({ model }) => {
  return helpers.Service.post(`api/subjects`, model);
};

const updateSubject = ({ subjectId, model }) => {
  return helpers.Service.put(`api/subjects/${subjectId}`, model);
};

const addSubjectVisit = ({ subjectId, model }) => {
  return helpers.Service.post(`api/subjects/${subjectId}/visits`, model);
};

const updateSubjectVisit = ({ subjectId, model }) => {
  return helpers.Service.put(`api/subjects/${subjectId}/visits`, model);
};

const removeSubjectVisit = ({ subjectId, subjectVisitId }) => {
  return helpers.Service.delete(
    `api/subjects/${subjectId}/visits/${subjectVisitId}`
  );
};

const fetchSubjects = ({ projectSiteId, subjectNo, offset, limit }) => {
  const params = helpers.qs.stringify({
    subjectNo: subjectNo === void 0 ? '' : subjectNo,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/subjects?${params}`
  );
};

const fetchSubject = ({ subjectId }) => {
  return helpers.Service.get(`api/subjects/${subjectId}`);
};

const fetchSubjectProjectSiteId = ({ subjectId }) => {
  return helpers.Service.get(`api/subjects/${subjectId}/projectSiteId`);
};

const fetchCntOfSubjectsPerSite = ({ projectId, projectSiteId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/site/${projectSiteId}/subjects/count`
  );
};

const fetchCntOfSubjectsPerSiteByMonth = ({ projectId, projectSiteId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/site/${projectSiteId}/month/subjects/count`
  );
};

const fetchCntOfSubjectsPerSiteByWeek = ({ projectId, projectSiteId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/site/${projectSiteId}/week/subjects/count`
  );
};

const fetchTotalCntOfSubjectsScheduleByMonth = ({ projectId, start, end }) => {
  const params = helpers.qs.stringify({
    start,
    end
  });

  return helpers.Service.get(
    `api/projects/${projectId}/site/subjects/schedule/total?${params}`
  );
};

const fetchSubjectsScheduleByWeek = ({ projectId, start, end }) => {
  const params = helpers.qs.stringify({
    start,
    end
  });

  return helpers.Service.get(
    `api/projects/${projectId}/site/subjects/schedule/week?${params}`
  );
};

const fetchSubjectsScheduleByDate = ({ projectId, date }) => {
  const params = helpers.qs.stringify({
    date
  });

  return helpers.Service.get(
    `api/projects/${projectId}/site/subjects/schedule/date?${params}`
  );
};

const fetchQueries = ({ projectId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/queries?$orderBy=subjectNo,projectActivityId asc`
  );
};

const fetchQueryHistories = ({ projectId, activityQueryId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/queries/${activityQueryId}/histories`
  );
};

export default {
  fetchSubjectActivitiesBySort,
  fetchActivitiesOfGroup,
  fetchActivitiesOfProject,
  fetchActivityStatusOfSubject,
  addSubject,
  updateSubject,
  addSubjectVisit,
  updateSubjectVisit,
  removeSubjectVisit,
  fetchSubjects,
  fetchSubject,
  fetchSubjectProjectSiteId,
  fetchSubjectVisits,
  fetchCntOfSubjectsPerSite,
  fetchCntOfSubjectsPerSiteByMonth,
  fetchCntOfSubjectsPerSiteByWeek,
  fetchTotalCntOfSubjectsScheduleByMonth,
  fetchSubjectsScheduleByWeek,
  fetchSubjectsScheduleByDate,
  fetchQueries,
  fetchQueryHistories
};
