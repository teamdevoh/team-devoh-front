import React, { Fragment } from 'react';
import { useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import styled from 'styled-components';
import { format } from '../../constants';

const AuthorWrapper = styled.div`
  font-size: 0.8em;
  color: rgba(0, 0, 0, 0.5);
`;

const AuthorInfo = ({ date, name }) => {
  const t = useFormatMessage();
  if (helpers.util.verifyDate(date)) {
    return (
      <AuthorWrapper>
        {t('activity.author.date', {
          author: name,
          date: helpers.util.dateformat(date, format.YYYY_MM_DD_HHMM)
        })}
      </AuthorWrapper>
    );
  } else {
    return <Fragment />;
  }
};

export default AuthorInfo;
