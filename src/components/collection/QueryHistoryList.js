import React, { memo, Fragment } from 'react';
import { Item, Label, Divider, Responsive, Table } from 'semantic-ui-react';
import { useFace, useFormatMessage } from '../../hooks';
import { code, format } from '../../constants';
import { TableWrap, StyledTh } from '../case-review/styles';
import { EmptyRowsViewForSemantic } from '../modules';
import helpers from '../../helpers';

const getQueryColor = queryStatusCodeId => {
  return queryStatusCodeId === code.QueryOpen
    ? 'blue'
    : queryStatusCodeId === code.QueryAnswer
      ? 'red'
      : 'green';
};

const Cell = ({ row, idx }) => {
  const {
    author,
    myFaceFileStorageId,
    role,
    status,
    queryDateTime,
    queryStatusCodeId,
    query
  } = row;
  const [faceUrl] = useFace(myFaceFileStorageId);

  const color = getQueryColor(queryStatusCodeId);

  return (
    <Fragment>
      <Item.Group style={{ paddingTop: '10px', paddingLeft: '10px' }}>
        <Item>
          <Item.Image size="mini" avatar src={faceUrl} />
          <Item.Content>
            <Item.Header
              style={{ fontWeight: 'normal', fontSize: '13px' }}
              as="h6"
            >
              {author} <Label size="mini">{role}</Label>
              <Label size="mini" color={color}>
                {status}
              </Label>
            </Item.Header>
            <Item.Meta>
              <span className="cinema">
                {helpers.util.dateformat(queryDateTime, format.YYYY_MM_DD_HHMM)}
              </span>
            </Item.Meta>
            <Item.Description>{query}</Item.Description>
          </Item.Content>
        </Item>
      </Item.Group>
      <Divider section style={{ marginTop: '-15px', marginBottom: '-15px' }} />
    </Fragment>
  );
};

const QueryHistoryList = ({ items, loading }) => {
  const isMobile =
    Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;
  const t = useFormatMessage();

  return (
    <TableWrap maxHeight={isMobile ? `200px` : `600px`}>
      <Table celled padded>
        <Table.Header>
          <Table.Row>
            <StyledTh>{t('user')}</StyledTh>
            <StyledTh>{t('role')}</StyledTh>
            <StyledTh>{t('status')}</StyledTh>
            <StyledTh>{t('common.date')}</StyledTh>
            <StyledTh>{t('common.query')}</StyledTh>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {items.map(item => (
            <Table.Row key={item.activityQueryHistoryId}>
              <Table.Cell colSpan={5}>
                <Cell row={item} />
              </Table.Cell>
            </Table.Row>
          ))}
          {items.length === 0 && (
            <EmptyRowsViewForSemantic loading={loading} columnLength={5} />
          )}
        </Table.Body>
      </Table>
    </TableWrap>
  );
};

export default memo(QueryHistoryList);
