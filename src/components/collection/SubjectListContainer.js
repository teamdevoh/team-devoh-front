import React, { useEffect, memo, Fragment } from 'react';
import { useInView } from 'react-intersection-observer';
import { Loader } from 'semantic-ui-react';
import { useSubjectList } from './hooks';
import SubjectList from './SubjectList';

const SubjectListContainer = memo(() => {
  const [ref, inView, entry] = useInView({
    root: document.querySelector('#subjectWrapper'),
    rootMargin: '200px',
    threshold: 0
  });
  const [{ items, loading, total, start, refresh }] = useSubjectList();

  useEffect(
    () => {
      if (inView && items.length < total && entry.isIntersecting) {
        refresh({ offset: start });
      }
    },
    [inView]
  );

  return (
    <Fragment>
      <SubjectList items={items} />
      <div ref={ref}>
        <Loader active={loading} inline="centered" size="mini" />
      </div>
    </Fragment>
  );
});

export default SubjectListContainer;
