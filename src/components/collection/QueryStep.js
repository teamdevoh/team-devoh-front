import React, { useCallback, useState, useEffect } from 'react';
import StepGroup from '../element/StepGroup';
import { useFormatMessage } from '../../hooks';
import { code } from '../../constants';
import { useSelector } from 'react-redux';

const QueryStep = ({ queryCountSet, onClick }) => {
  const t = useFormatMessage();
  const [items, setItems] = useState([]);
  const [activeValue, setActiveValue] = useState(0);
  const { subjectStatusCode, ongoingMonths, caseConMonths } = useSelector(
    state => state.subject.filter
  );

  const handleClick = useCallback(
    (event, data) => {
      items.forEach(item => {
        item.active = false;
        if (data.value === item.value) {
          item.active = true;
        }
      });

      setItems(items);
      setActiveValue(data.value);
      onClick(data.value);
    },
    [items, onClick]
  );

  useEffect(
    () => {
      fetchData();
    },
    [queryCountSet]
  );

  useEffect(
    () => {
      setActiveValue(0);
    },
    [subjectStatusCode, ongoingMonths, caseConMonths]
  );

  const fetchData = useCallback(
    () => {
      const sum = queryCountSet['sum'];
      const open = queryCountSet[code.QueryOpen];
      const answer = queryCountSet[code.QueryAnswer];
      const close = queryCountSet[code.QueryClose];

      setItems([
        { value: 0, name: 'None', active: activeValue === 0 },
        { value: 1, name: t('all'), active: activeValue === 1, length: sum },
        {
          value: code.QueryOpen,
          name: t('collection.query.status.open'),
          active: activeValue === code.QueryOpen,
          length: open
        },
        {
          value: code.QueryAnswer,
          name: t('collection.query.status.answer'),
          active: activeValue === code.QueryAnswer,
          length: answer
        },
        {
          value: code.QueryClose,
          name: t('collection.query.status.close'),
          active: activeValue === code.QueryClose,
          length: close
        }
      ]);
    },
    [queryCountSet, activeValue]
  );

  return <StepGroup items={items} onClick={handleClick} />;
};

export default QueryStep;
