import React from 'react';
import { List } from 'semantic-ui-react';
import map from 'lodash/map';
import SubjectListItem from './SubjectListItem';

const SubjectList = ({ items }) => {
  return (
    <List divided relaxed size="mini">
      {map(items, item => {
        return <SubjectListItem key={item.subjectId} item={item} />;
      })}
    </List>
  );
};

export default SubjectList;
