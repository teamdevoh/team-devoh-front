import { activity } from '../../../states';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import findIndex from 'lodash/findIndex';
import helpers from '../../../helpers';

const useMoveToNextStep = ({ projectActivityId }) => {
  const location = useLocation();
  const activities = useSelector(activity.selectors.activitiesOfGroup);

  // if next item exists,
  // It will be changes history to next location history
  const goToNext = () => {
    if (activities.length > 0) {
      const currentIndex = findIndex(activities, {
        projectActivityId: Number(projectActivityId)
      });

      const hasNextStep = activities.length - 1 === currentIndex ? false : true;

      // whether last step
      if (hasNextStep) {
        const next = activities[currentIndex + 1];
        const nextLocation = `${location.pathname}?projectActivityId=${
          next.projectActivityId
        }&activityId=${next.activityId}`;

        helpers.history.push(nextLocation);
      }
    }
  };

  return {
    goToNext
  };
};

export default useMoveToNextStep;
