import { useState } from 'react';
import { useCallback } from 'react';
import { useEffect } from 'react';
import api from '../api';

const useSubjectInfo = subjectId => {
  const [projectSiteId, setProjectSiteId] = useState(0);

  const fetchItem = useCallback(
    async () => {
      const res = await api.fetchSubjectProjectSiteId({ subjectId });
      if (res && res.data) {
        setProjectSiteId(res.data);
      }
    },
    [subjectId]
  );

  useEffect(
    () => {
      if (subjectId > 0) {
        fetchItem();
      }
    },
    [subjectId]
  );

  return { projectSiteId };
};

export default useSubjectInfo;
