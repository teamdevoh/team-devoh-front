import api from '../api';
import { useState, useEffect } from 'react';

const useGetProjectActivityId = ({
  activityGroupId,
  subjectId,
  activityId
}) => {
  const [id, setId] = useState(0);

  const fetchItem = async () => {
    const res = await api.fetchActivitiesOfGroup({
      activityGroupId,
      subjectId
    });
    if (res && res.data) {
      const item = _.find(res.data, { activityId });
      setId(item.projectActivityId);
    }
  };

  useEffect(
    () => {
      fetchItem();
    },
    [activityGroupId, subjectId, activityId]
  );

  return { id };
};

export default useGetProjectActivityId;
