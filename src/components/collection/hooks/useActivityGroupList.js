import React, { useState } from 'react';
import { useCallback } from 'react';
import api from '../api';
import { useEffect } from 'react';
import StatusWrapper from '../StatusWrapper';
import { Grid } from 'semantic-ui-react';

const useActivityGroupList = ({ projectId, subjectId, projectSiteId }) => {
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(
    async () => {
      try {
        const res = await api.fetchActivitiesOfProject({ projectId });
        if (res && res.data) {
          let statusData = {};
          let options = [];
          if (subjectId) {
            const resStatus = await api.fetchActivityStatusOfSubject({
              projectId,
              subjectId,
              projectSiteId
            });
            statusData = resStatus.data;
          }

          res.data.forEach(item => {
            const statusItem = _.find(statusData, {
              activityGroupId: item.activityGroupId
            });

            options.push({
              key: item.activityGroupId,
              text: `${item.name} (${item.activityCount})`,
              value: item.activityGroupId,
              content: (
                <Grid>
                  <Grid.Column width="ten">
                    {item.name} ({item.activityCount})
                  </Grid.Column>
                  <Grid.Column width="six">
                    {statusItem && (
                      <StatusWrapper
                        activityStatusCodeId={
                          statusItem.minActivityStatusCodeId
                        }
                        queryStatusCodeId={statusItem.lastQueryStatusCodeId}
                        hasNote={statusItem.hasNote}
                        onClick={() => {}}
                      />
                    )}
                  </Grid.Column>
                </Grid>
              )
            });
          });

          setItems(options);
        }
      } catch (error) {}
    },
    [projectId, subjectId]
  );

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, subjectId]
  );

  return [items];
};

export default useActivityGroupList;
