import { useState, useCallback, useEffect } from 'react';
import api from '../api';
import { useDispatch, useSelector } from 'react-redux';
import activity from '../../../states/activity';
import { useNoteStatusWithAll } from '../../activity-note/hooks';
import { useQueryStatusWithAll } from '../../activity-query/hooks';

const useActivitiesOfGroupList = ({ activityGroupId, subjectId }) => {
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();
  const items = useSelector(activity.selectors.activitiesOfGroup);
  const { getNoteStatus } = useNoteStatusWithAll({ subjectId });
  const { getQueryStatus } = useQueryStatusWithAll({ subjectId });

  const fetchItems = useCallback(() => {
    try {
      api
        .fetchActivitiesOfGroup({
          activityGroupId,
          subjectId
        })
        .then(async res => {
          res.data.forEach(item => {
            item.activityKeyId = 0;
          });

          await getNoteStatus(res.data);
          await getQueryStatus(res.data);

          dispatch(activity.actions.fetchActivitiesOfGroup(res.data));
        });
    } catch (error) {
    } finally {
      setLoading(false);
    }
  });

  const clearGroups = () => {
    dispatch(activity.actions.fetchActivitiesOfGroup([]));
  };

  useEffect(
    () => {
      fetchItems();
    },
    [activityGroupId, subjectId]
  );

  return [{ items, clearGroups, fetch: fetchItems, loading }];
};

export default useActivitiesOfGroupList;
