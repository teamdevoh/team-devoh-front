import { useEffect, useState } from 'react';
import api from '../api';

const useVisitListOfSubject = subjectId => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    setLoading(true);
    const res = await api.fetchSubjectVisits({ subjectId });
    if (res && res.data) {
      setItems(res.data);
    }

    setLoading(false);
  };

  useEffect(
    () => {
      if (subjectId > 0) {
        fetchItems();
      }
    },
    [subjectId]
  );

  return [{ items, loading }, setItems];
};

export default useVisitListOfSubject;
