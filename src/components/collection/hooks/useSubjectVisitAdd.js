import api from '../api';
import { useState } from 'react';

const useSubjectVisitAdd = subjectId => {
  const [loading, setLoading] = useState(false);

  const add = async model => {
    setLoading(true);
    const res = await api.addSubjectVisit({ subjectId, model });
    setLoading(false);
    if (res && res.data) {
      return res.data;
    }

    return 0;
  };

  return [{ onAdd: add, loading }];
};

export default useSubjectVisitAdd;
