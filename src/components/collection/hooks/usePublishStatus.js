import { activity } from '../../../states';
import { useDispatch } from 'react-redux';
import minBy from 'lodash/minBy';
import findIndex from 'lodash/findIndex';

const usePublishStatus = ({ projectActivityId }) => {
  const dispatch = useDispatch();

  const pickActivityValueBy = repeatItems => {
    const minItem = minBy(repeatItems, 'activityStatusCodeId');
    return minItem.activityStatusCodeId;
  };

  const pickQueryValueBy = repeatItems => {
    if (repeatItems && repeatItems.length > 0) {
      const minQueryStatusCodeId = minBy(repeatItems, 'queryStatusCodeId')
        .queryStatusCodeId;
      return minQueryStatusCodeId;
    }
    return 0;
  };

  const pickNoteValueBy = repeatItems => {
    const found = findIndex(repeatItems, { hasNote: true });
    return found > -1;
  };

  const pickFileValueBy = repeatItems => {
    const found = findIndex(repeatItems, { hasFile: true });
    return found > -1;
  };

  const activityStatus = activityStatusCodeId => {
    dispatch(
      activity.actions.updateStatusActivitiesOfGroup(
        Number(projectActivityId),
        activityStatusCodeId
      )
    );
  };

  const noteStatus = hasNote => {
    dispatch(
      activity.actions.updateNoteStatusActivitiesOfGroup(
        Number(projectActivityId),
        hasNote
      )
    );
  };

  const queryStatus = queryStatusCodeId => {
    dispatch(
      activity.actions.updateQueryStatusActivitiesOfGroup(
        Number(projectActivityId),
        queryStatusCodeId
      )
    );
  };

  return {
    pickActivityValueBy,
    pickQueryValueBy,
    pickNoteValueBy,
    pickFileValueBy,
    activityStatus,
    noteStatus,
    queryStatus
  };
};

export default usePublishStatus;
