import { useState } from 'react';
import api from '../api';

const useSubjectVisitEdit = subjectId => {
  const [loading, setLoading] = useState(false);

  const edit = async model => {
    setLoading(true);
    const res = await api.updateSubjectVisit({ subjectId, model });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  return [{ onEdit: edit, loading }];
};

export default useSubjectVisitEdit;
