import { useCallback, useEffect, useState } from 'react';
import { code } from '../../../constants';
import api from '../api';

const useSubjectActivityStatus = ({
  projectId,
  projectSiteId,
  subjectStatusCode,
  search,
  order,
  queryStatusQuery,
  ongoingMonths,
  caseConMonths
}) => {
  const [isRendered, setIsRendered] = useState(false);
  const [items, setItems] = useState([]);
  const [statusCount, setStatusCount] = useState({
    complete: 0,
    dropout: 0,
    ongoing: 0,
    register: 0
  });
  const [loading, setLoading] = useState(false);
  const [activityCount, setActivityCount] = useState({
    interview: 0,
    medication: 0,
    test: 0,
    lab: 0,
    tdm: 0,
    adr: 0,
    caseConclusion: 0,
    futdm: 0
  });

  const fetchItems = useCallback(async () => {
    setLoading(true);
    const param = {
      projectId,
      projectSiteId,
      subjectStatusCode,
      search,
      order,
      // queryStatusQuery,
      ongoingMonths: ongoingMonths.join(','),
      caseConMonths: caseConMonths.join(',')
    };

    try {
      const res = await api.fetchSubjectActivitiesBySort(param);

      if (res && res.data) {
        if (res.data.activityStatusBySubjects.length > 0) {
          const firstRow = res.data.activityStatusBySubjects[0];
          setActivityCount({
            interview: firstRow.interview.count,
            medication: firstRow.medication.count,
            test: firstRow.test.count,
            lab: firstRow.lab.count,
            tdm: firstRow.tdm.count,
            adr: firstRow.adr.count,
            caseConclusion: firstRow.caseConclusion.count,
            futdm: firstRow.futdm.count
          });
        }

        if (res.data.countOfSubjectStatus) {
          setStatusCount(res.data.countOfSubjectStatus);
        }

        if (queryStatusQuery) {
          const filtered = _.filter(res.data.activityStatusBySubjects, item => {
            if (queryStatusQuery === 1) {
              return (
                item.interview.lastQueryStatusCodeId > 0 ||
                item.medication.lastQueryStatusCodeId > 0 ||
                item.test.lastQueryStatusCodeId > 0 ||
                item.lab.lastQueryStatusCodeId > 0 ||
                item.tdm.lastQueryStatusCodeId > 0 ||
                item.adr.lastQueryStatusCodeId > 0 ||
                item.caseConclusion.lastQueryStatusCodeId > 0
              );
            }
            if (queryStatusQuery === code.QueryOpen) {
              return (
                item.interview.lastQueryStatusCodeId === code.QueryOpen ||
                item.medication.lastQueryStatusCodeId === code.QueryOpen ||
                item.test.lastQueryStatusCodeId === code.QueryOpen ||
                item.lab.lastQueryStatusCodeId === code.QueryOpen ||
                item.tdm.lastQueryStatusCodeId === code.QueryOpen ||
                item.adr.lastQueryStatusCodeId === code.QueryOpen ||
                item.caseConclusion.lastQueryStatusCodeId === code.QueryOpen
              );
            }
            if (queryStatusQuery === code.QueryAnswer) {
              return (
                item.interview.lastQueryStatusCodeId === code.QueryAnswer ||
                item.medication.lastQueryStatusCodeId === code.QueryAnswer ||
                item.test.lastQueryStatusCodeId === code.QueryAnswer ||
                item.lab.lastQueryStatusCodeId === code.QueryAnswer ||
                item.tdm.lastQueryStatusCodeId === code.QueryAnswer ||
                item.adr.lastQueryStatusCodeId === code.QueryAnswer ||
                item.caseConclusion.lastQueryStatusCodeId === code.QueryAnswer
              );
            }
            if (queryStatusQuery === code.QueryClose) {
              return (
                item.interview.lastQueryStatusCodeId === code.QueryClose ||
                item.medication.lastQueryStatusCodeId === code.QueryClose ||
                item.test.lastQueryStatusCodeId === code.QueryClose ||
                item.lab.lastQueryStatusCodeId === code.QueryClose ||
                item.tdm.lastQueryStatusCodeId === code.QueryClose ||
                item.adr.lastQueryStatusCodeId === code.QueryClose ||
                item.caseConclusion.lastQueryStatusCodeId === code.QueryClose
              );
            }
          });

          setItems(filtered);
        } else {
          setItems(res.data.activityStatusBySubjects);
        }
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  });

  useEffect(
    () => {
      if (isRendered) {
        fetchItems();
      } else {
        setIsRendered(true);
      }
    },
    [
      projectId,
      projectSiteId,
      subjectStatusCode,
      queryStatusQuery,
      search,
      order,
      ongoingMonths,
      caseConMonths
    ]
  );

  return [{ statusCount, items, loading, activityCount }];
};

export default useSubjectActivityStatus;
