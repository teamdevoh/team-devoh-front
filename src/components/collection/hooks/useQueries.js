import api from '../api';
import { useEffect } from 'react';
import { useState } from 'react';

const useQueries = projectId => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchQueries({ projectId });
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId]
  );

  return { items, loading };
};

export default useQueries;
