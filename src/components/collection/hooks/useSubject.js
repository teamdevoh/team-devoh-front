import { useState, useEffect } from 'react';
import { code } from '../../../constants';
import { useFormFields } from '../../../hooks';
import api from '../api';

const initial = {
  projectSiteId: '',
  subjectNo: '',
  name: '',
  initial: '',
  hospitalNo: '',
  contactNo: '',
  subjectStatusCode: '',
  dropoutDate: null,
  dropoutReasonCode: '',
  dropoutDetail: null,
  remark: '',
  address: ''
};

const useSubject = subjectId => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields(initial);

  const fetchItem = async () => {
    setLoading(true);
    const res = await api.fetchSubject({ subjectId });
    setLoading(false);
    if (res && res.data) {
      setModel(res.data);
    }
  };

  const add = async () => {
    try {
      setLoading(true);
      const res = await api.addSubject({ model });

      if (res && res.data.key > 0) {
        return {
          newId: res.data.key,
          subjectNo: res.data.value
        };
      } else {
        return {
          newId: 0,
          subjectNo: ''
        };
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const edit = async () => {
    try {
      setLoading(true);
      const res = await api.updateSubject({
        subjectId: model.subjectId,
        model
      });

      if (res && res.data) {
        return true;
      }
      return false;
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      if (subjectId > 0) {
        fetchItem();
      } else {
        setModel(initial);
      }
    },
    [subjectId]
  );

  useEffect(
    () => {
      if (model.subjectStatusCode !== code.SubjectDropout) {
        setModel({
          ...model,
          dropoutDate: null,
          dropoutReasonCode: ''
        });
      }
    },
    [model.subjectStatusCode]
  );

  return [{ model, loading, onChange, onAdd: add, onEdit: edit }];
};

export default useSubject;
