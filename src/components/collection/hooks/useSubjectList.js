import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useSubjectList = ({ projectSiteId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(
    async ({ offset }) => {
      setLoading(true);
      const res = await api.fetchSubjects({
        projectSiteId
      });
      if (res && res.data) {
        let options = [];
        res.data.items.forEach(item => {
          options.push({
            text: `${item.subjectPrefix}${item.subjectNo}/${item.initial}/${
              item.age
            }/${item.sex.substr(0, 1)}`,
            value: item.subjectId
          });
        });

        setLoading(false);
        setItems(options);
      }
    },
    [projectSiteId]
  );

  useEffect(
    () => {
      if (projectSiteId > 0) {
        fetchItems({ offset: 0 });
      }
    },
    [projectSiteId]
  );

  return [{ items, loading }, setItems];
};

export default useSubjectList;
