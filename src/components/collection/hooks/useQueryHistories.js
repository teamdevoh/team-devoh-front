import { useEffect, useState } from 'react';
import api from '../api';

const useQueryHistories = (projectId, queryRow) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchQueryHistories({
        projectId,
        activityQueryId: queryRow.activityQueryId
      });
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, queryRow]
  );

  return { items, loading };
};

export default useQueryHistories;
