import find from 'lodash/find';
import helpers from '../../../helpers';
import { useCodes, useFormatMessage } from '../../../hooks';
import { useVisitListOfSubject } from './';
import { useEffect } from 'react';
import { useState } from 'react';
import { codeGroup, format } from '../../../constants';

const useVisitListOfSubjectWithDate = ({
  subjectId,
  visitTypeCode,
  showShortDate = false
}) => {
  const t = useFormatMessage();
  const [visitItems, setVisitItems] = useState([]);
  const [{ items, loading }] = useVisitListOfSubject(subjectId);
  const [visits] = useCodes({ codeGroupId: codeGroup.VisitTypeCode });

  useEffect(
    () => {
      if (items.length > 0) {
        let options = [];
        const filteredItems = _.filter(items, item => {
          if (visitTypeCode) {
            return item.visitTypeCode === visitTypeCode;
          } else {
            return item;
          }
        });

        filteredItems.forEach(item => {
          const foundItem = find(visits, { value: item.visitTypeCode });
          if (foundItem) {
            const orderName = helpers.util.getOrdinal(item.visitOrder);

            const hasVisitDate = item.visitDateTime ? true : false;
            const dateformat = showShortDate ? format.YYYY_MM_DD : null;
            const date = hasVisitDate
              ? `${
                  showShortDate ? '' : `(${t('activity.visit')})`
                }${helpers.util.dateformat(item.visitDateTime, dateformat)}`
              : `${
                  showShortDate ? '' : `(${t('schedule.date')})`
                }${helpers.util.dateformat(item.scheduleDateTime, dateformat)}`;

            options.push({
              text: `${(item.visitTypeCode === 21 ? orderName + ' ' : '') +
                foundItem.text}/${date}`,
              value: item.subjectVisitId,
              data: { ...item }
            });

            setVisitItems(options);
          }
        });
      }
    },
    [items]
  );

  return [{ items: visitItems, loading }, setVisitItems];
};

export default useVisitListOfSubjectWithDate;
