import React, { useEffect, useState, Fragment } from 'react';
import { Bar } from 'react-chartjs-2';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import api from './api';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';

const color = [
  '#008299',
  '#FF6B5E',
  '#92C4FF',
  '#BCCC60',
  '#62CCAE',
  '#A693FF',
  '#FFA661'
];

const generateStackChartData = ({
  labels: orgLabels = [],
  data: primitiveData,
  labelType
}) => {
  const labelMap = new Map();
  const labels = [];
  const datasets = [];
  const divisionMap = new Map();

  for (let i = 0; i < orgLabels.length; i++) {
    const { siteId, siteName, count } = orgLabels[i];
    labelMap.set(siteId, { index: labels.length, totalCnt: count, hasCnt: 0 });
    labels.push(siteName);
  }

  for (let i = 0; i < primitiveData.length; i++) {
    const curData = primitiveData[i];
    const { siteId, projectSiteId, count } = curData;

    // if (!labelMap.has(siteId)) {
    //   labelMap.set(siteId, { index: labels.length, hasCnt: 0 });
    //   labels.push(siteName);
    // }

    const curLabel = labelMap.get(siteId);
    const labelHasCnt = curLabel.hasCnt + 1;
    labelMap.set(siteId, { ...curLabel, hasCnt: labelHasCnt });

    const divisionKey = `${projectSiteId}-${curData[labelType]}`;
    const labelIndex = curLabel.index;
    let data = [];
    let datasetsCurIndex = null;

    if (!divisionMap.has(divisionKey)) {
      datasetsCurIndex = datasets.length;
      divisionMap.set(divisionKey, { index: datasetsCurIndex });
      const itemData = {
        label: curData[labelType],
        data: [],
        stack: 1,
        backgroundColor: color[(labelHasCnt % color.length) - 1]
      };
      datasets.push(itemData);
    } else {
      data = datasets[divisionMap.get(divisionKey).index];
      datasetsCurIndex = divisionMap.get(divisionKey).index;
    }

    data[labelIndex] = count;
    datasets[datasetsCurIndex] = {
      ...datasets[datasetsCurIndex],
      data
    };
  }

  return {
    labels,
    datasets
  };
};

const repeatColor = dataLength => {
  let repeatCount = Math.ceil(dataLength / color.length);

  const resultColor = [];
  for (let i = 0; i < repeatCount; i++) {
    resultColor.push(...color);
  }

  return resultColor;
};

const generateNormallyChartData = ({ data: primitiveData, labelType }) => {
  const labelMap = new Map();
  const labels = [];
  const data = [];

  for (let i = 0; i < primitiveData.length; i++) {
    const curData = primitiveData[i];
    const { count } = curData;
    const labelMapId = curData[labelType];

    if (!labelMap.has(labelMapId)) {
      labelMap.set(labelMapId, { index: labels.length });
      labels.push(curData[labelType]);
    }

    const curLabel = labelMap.get(labelMapId);
    const labelIndex = curLabel.index;

    data[labelIndex] = count;
  }

  const backgroundColor =
    data.length > color.length ? repeatColor(data.length) : color;

  return {
    labels,
    datasets: [
      {
        data,
        backgroundColor
      }
    ]
  };
};

const CollectionChart = ({ projectId, projectSiteId = 0, type = 'all' }) => {
  const [data, setData] = useState(null);
  const t = useFormatMessage();

  useEffect(
    () => {
      setData(null);

      function settingData({ labels, data, labelType }) {
        // const { labels, data } = res.data;
        const temp =
          projectSiteId === 0
            ? generateStackChartData({ labels, data, labelType })
            : generateNormallyChartData({ data, labelType });
        setData({
          ...temp
        });
      }

      function fetchAllCount() {
        api
          .fetchCntOfSubjectsPerSite({
            projectId,
            projectSiteId
          })
          .then(res => {
            settingData({ ...res.data, labelType: 'docName' });
          });
      }

      function fetchMonthlyCount() {
        api
          .fetchCntOfSubjectsPerSiteByMonth({
            projectId,
            projectSiteId
          })
          .then(res => {
            settingData({ ...res.data, labelType: 'month' });
          });
      }

      function fetchWeeklyCount() {
        api
          .fetchCntOfSubjectsPerSiteByWeek({
            projectId,
            projectSiteId
          })
          .then(res => {
            settingData({ ...res.data, labelType: 'week' });
          });
      }

      if (type === 'all') {
        fetchAllCount();
      } else if (type === 'month') {
        fetchMonthlyCount();
      } else if (type === 'week') {
        fetchWeeklyCount();
      }
    },
    [projectSiteId]
  );

  const isMobile = helpers.util.isMobile();
  return (
    <Fragment>
      {data && (
        <Bar
          data={data}
          width={100}
          height={50}
          options={{
            plugins: {
              datalabels: {
                color: '#000',
                formatter: (value, context) => {
                  if (typeof value === 'undefined') {
                    return null;
                  }
                  const { label } = context.dataset;
                  const title =
                    typeof label === 'undefined'
                      ? context.chart.data.labels[context.dataIndex]
                      : label;
                  return `${title} : ${value}`;
                }
              }
            },
            title: {
              display: true,
              fontSize: 14,
              fontStyle: 'bold',
              text: t(`common.${type}`)
            },
            legend: { display: false },
            tooltips: {
              mode: 'label',
              intersect: false,
              filter: data => {
                return !isNaN(data.y);
              }
            },
            scales: {
              xAxes: [
                {
                  ticks: {
                    autoSkip: false
                  }
                }
              ],
              yAxes: [
                {
                  ticks: {
                    stepSize: isMobile ? null : 10
                  }
                }
              ]
            }
          }}
          plugins={[ChartDataLabels]}
        />
      )}
    </Fragment>
  );
};

export default CollectionChart;
