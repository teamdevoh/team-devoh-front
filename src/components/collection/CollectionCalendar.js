import React, { useRef, useCallback, useState } from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction'; // needed for dayClick
// must manually import the stylesheets for each plugin
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/list/main.css';
import { Container, Placeholder } from 'semantic-ui-react';
import { useRouteMatch, useHistory } from 'react-router-dom';
import moment from 'moment';

import api from './api';
import { Modal } from '../modal';
import SubjectSchduleList from './SubjectSchduleList';
import ConfirmSubjectVisit from './ConfirmSubjectVisit';
import { useFormatMessage } from '../../hooks';
import { useActivityGroupList } from './hooks';
import styled from 'styled-components';
import helpers from '../../helpers';
import { format } from '../../constants';

const color = {
  visit: ['#fff', '#000'],
  unvisited: ['#FDE3E5', '#000']
};

const ContainerRoot = styled(Container)`
  &&& {
    position: relative;
    & .fc-center div {
      display: flex;
      align-items: center;
    }

    & .fc-toolbar h2 {
      margin: 0 5px;
    }

    & .fc-right {
      .fc-showMonth-button,
      .fc-showWeekly-button {
        opacity: 0.5;
      }

      ${({ selected_tab }) => {
        const tab =
          selected_tab.charAt(0).toUpperCase() + selected_tab.slice(1);
        return `.fc-show${tab}-button {
          opacity: 1;
        }`;
      }};
    }

    & .fc-scroller {
      ${({ is_mobile }) => {
        if (is_mobile === 'true') {
          return `max-height: calc(100vh - 40vh);
           height: auto !important;
          `;
        }
      }};
    }

    & .fc-list-item {
      cursor: pointer;
    }

    & .fc-more {
      color: #192254;
    }
  }
`;

const CalendarWrap = styled.div`
  & .fc-title {
    white-space: normal;
  }
  & .fc-event-container {
    cursor: pointer;
  }
`;

const StyledLoading = styled(Placeholder)`
  &&& {
    position: absolute;
    z-index: 100;
    width: 100%;
    height: 100%;
    opacity: 0.3;
    max-width: unset;
  }
`;

const handleListEventRender = info => {
  const { backgroundColor = '' } = info.event;
  if (backgroundColor !== '') {
    info.el.style.backgroundColor = backgroundColor;
  }
};

const generateDetailList = ({ list, titleHasTime = true }) => {
  return list.map(item => {
    const {
      // siteId,
      // projectSiteId,
      siteName,
      // subjectId,
      subjectNo,
      initial,
      gender,
      // subjectName,
      age,
      visitType,
      subjectVisitId,
      scheduleDateTime,
      visitDateTime
    } = item;

    const visit = visitDateTime !== null;
    const start = visitDateTime === null ? scheduleDateTime : visitDateTime;
    const visitTime = titleHasTime
      ? ` ${visitType} ${moment(start).format('LT')}`
      : '';
    return {
      title: `${siteName} [${subjectNo}/${initial}/${!gender ? ' ' : gender}/${
        !age ? ' ' : age
      }]${visitTime}`,
      start,
      color: color[visit ? 'visit' : 'unvisited'][0],
      textColor: color[visit ? 'visit' : 'unvisited'][1],
      subjectVisitId,
      itemData: {
        ...item
      }
    };
  });
};

const CollectionCalendar = () => {
  const isMobile = helpers.util.isMobile();
  const t = useFormatMessage();
  const match = useRouteMatch();
  const { projectId } = match.params;
  const [activityGroupList] = useActivityGroupList({ projectId });
  const history = useHistory();
  const monthComponentRef = useRef();
  const weekComponentRef = useRef();
  const [detailModal, setDetailModal] = useState({
    open: false,
    title: '',
    list: []
  });
  const [loading, setLoading] = useState(false);
  const [selectedTab, setSelectedTab] = useState('month');
  const curDate = useRef(null);

  const setCurDate = ({ start, end }) => {
    const diffDate = (end.getTime() - start.getTime()) / 1000 / 60 / 60 / 24;
    const newDate = start.getDate() + Math.round(diffDate / 2);

    curDate.current = new Date(start.setDate(newDate));
  };

  const getCurDate = ({ isWeekly = false } = {}) => {
    return isWeekly ? curDate.current.setDate(1) : curDate.current;
  };

  const handleSelectTab = tab => {
    if (tab === selectedTab) {
      return false;
    }

    let paramDate = new Date();
    if (tab === 'month') {
      paramDate = getCurDate();
    } else if (tab === 'weekly') {
      paramDate = getCurDate({ isWeekly: true });
    }

    setSelectedTab(tab);
    monthComponentRef.current.getApi().gotoDate(paramDate);
  };

  const goActivityGroup = subjectId => {
    history.push(
      `/project/${projectId}/subject/${subjectId}/activitygroup/${
        activityGroupList[0].value
      }`
    );
  };

  const getCalendarData = useCallback(async (fetchInfo, successCallback) => {
    try {
      const { start, end } = fetchInfo;
      const response = await api.fetchTotalCntOfSubjectsScheduleByMonth({
        projectId,
        start: helpers.util.dateformat(start, format.YYYY_MM_DD),
        end: helpers.util.dateformat(end, format.YYYY_MM_DD)
      });

      setCurDate({ start, end });

      successCallback(
        response.data.map(event => {
          const { siteName, count, scheduledDate } = event;
          return {
            title: `${siteName}: ${count}`,
            start: new Date(scheduledDate),
            allDay: true
          };
        })
      );
    } catch (error) {
      console.log(error);
    }
  }, []);

  const getWeekListData = useCallback(async (fetchInfo, successCallback) => {
    try {
      const { start, end } = fetchInfo;
      const response = await api.fetchSubjectsScheduleByWeek({
        projectId,
        start: helpers.util.dateformat(start, format.YYYY_MM_DD),
        end: helpers.util.dateformat(end, format.YYYY_MM_DD)
      });

      setCurDate({ start, end });

      successCallback(
        generateDetailList({ list: response.data, titleHasTime: false })
      );
    } catch (error) {
      console.log(error);
    }
  }, []);

  const handleEventClick = async ({ event }) => {
    const date = event.start;

    const res = await api.fetchSubjectsScheduleByDate({
      projectId,
      date: helpers.util.dateformat(date, format.YYYY_MM_DD)
    });

    const titleDate = moment(date);
    const detailObj = {
      open: true,
      title: `${titleDate.format(format.YYYY_MM_DD)}(${titleDate.format(
        'ddd'
      )}) ${t('subject.visit.list')}`,
      list: generateDetailList({ list: res.data })
    };
    setDetailModal(detailObj);
  };

  const handleListEventClick = ({ event }) => {
    const { itemData } = event.extendedProps;
    const { visitDateTime, subjectId } = itemData;

    if (visitDateTime === null) {
      setDetailModal({
        open: true,
        title: t('confirm.subject.visit.title'),
        content: (
          <ConfirmSubjectVisit
            {...itemData}
            goActivityGroup={goActivityGroup}
          />
        )
      });
    } else {
      goActivityGroup(subjectId);
    }
  };

  const handleCloseModal = () => {
    setDetailModal({
      open: false,
      title: '',
      list: []
    });
  };

  const customButtons = {
    showMonth: {
      text: 'month',
      click: function() {
        handleSelectTab('month');
      }
    },
    showWeekly: {
      text: 'weekly',
      click: function() {
        handleSelectTab('weekly');
      }
    }
  };

  const handleRowEventClick = data => {
    const { visitDateTime, subjectId } = data;
    if (visitDateTime === null) {
      setDetailModal({
        ...detailModal,
        title: t('confirm.subject.visit.title'),
        content: (
          <ConfirmSubjectVisit {...data} goActivityGroup={goActivityGroup} />
        )
      });
    } else {
      goActivityGroup(subjectId);
    }
  };

  const handleCalendarLoading = isLoading => {
    setLoading(isLoading);
  };

  return (
    <ContainerRoot
      className={`${selectedTab} margin-0`}
      selected_tab={selectedTab}
      is_mobile={isMobile.toString()}
    >
      {loading && (
        <StyledLoading>
          <Placeholder.Image />
        </StyledLoading>
      )}
      <CalendarWrap
        style={{ display: selectedTab === 'month' ? 'block' : 'none' }}
      >
        <FullCalendar
          defaultView="dayGridMonth"
          header={{
            left: 'today',
            center: 'prev,title,next',
            right: 'showMonth,showWeekly'
          }}
          plugins={[dayGridPlugin, interactionPlugin]}
          ref={monthComponentRef}
          events={getCalendarData}
          eventClick={handleEventClick}
          customButtons={customButtons}
          eventLimit
          views={{
            dayGridMonth: {
              eventLimit: 3
            }
          }}
          loading={handleCalendarLoading}
        />
      </CalendarWrap>
      <div style={{ display: selectedTab === 'weekly' ? 'block' : 'none' }}>
        <FullCalendar
          defaultView="listWeek"
          header={{
            left: 'today',
            center: 'prev,title,next',
            right: 'showMonth,showWeekly'
          }}
          plugins={[listPlugin, interactionPlugin]}
          ref={weekComponentRef}
          events={getWeekListData}
          eventClick={handleListEventClick}
          customButtons={customButtons}
          eventRender={handleListEventRender}
          loading={handleCalendarLoading}
        />
      </div>
      <Modal
        size={detailModal.content ? 'tiny' : undefined}
        open={detailModal.open}
        onOK={handleCloseModal}
        title={detailModal.title}
        content={
          !detailModal.content ? (
            <SubjectSchduleList
              list={detailModal.list}
              onRowEventClick={handleRowEventClick}
            />
          ) : (
            detailModal.content
          )
        }
      />
    </ContainerRoot>
  );
};

export default CollectionCalendar;
