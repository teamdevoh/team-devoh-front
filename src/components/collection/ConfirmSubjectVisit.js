import React, { useState } from 'react';
import { Button, Grid, Header, Container, Form } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import DateTimePicker from '../datepicker/DateTimePicker';
import styled from 'styled-components';
import helpers from '../../helpers';
import { useSubjectVisitEdit } from './hooks';
import { notification } from '../modal';
import { FormattedMessage } from 'react-intl';

const VisitValue = styled.strong`
  color: #0986ff;
  font-weight: 700;
`;

const ConfirmSubjectVisit = ({
  siteName,
  subjectNo,
  initial,
  gender,
  age,
  subjectId,
  goActivityGroup,
  visitType,
  visitTypeCode,
  subjectVisitId,
  scheduleDateTime,
  isAlarm
}) => {
  const t = useFormatMessage();
  const isMobile = helpers.util.isMobile();
  const [dateTime, setDateTime] = useState('');
  const [{ onEdit, loading }] = useSubjectVisitEdit(subjectId);

  const handleDateTiemChange = (e, { value }) => {
    setDateTime(value);
  };

  const handleOkClick = async () => {
    if (dateTime === '') {
      notification.warning({
        title: t('check.visit.date')
      });
    } else {
      const res = await onEdit({
        subjectVisitId,
        subjectId,
        visitTypeCode,
        scheduleDateTime: helpers.util.dateformat(scheduleDateTime),
        visitDateTime: dateTime,
        isAlarm
      });

      if (res === true) {
        notification.success({
          title: t('common.alert.saved')
        });
        goActivityGroup(subjectId);
      }
    }
  };

  const handleDataCheckClick = () => {
    goActivityGroup(subjectId);
  };

  return (
    <Container>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as="h3">{siteName}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            [{subjectNo}/{initial}/{!gender ? ' ' : gender}/{!age ? ' ' : age}]{' '}
            <FormattedMessage
              id="confirm.subject.visit.content"
              values={{
                visit: <VisitValue>{visitType}</VisitValue>
              }}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {t('schedule.date')} : {helpers.util.dateformat(scheduleDateTime)}
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <Form>
        <Form.Field width="16">
          <DateTimePicker
            name="visitDate"
            value={dateTime}
            onChange={handleDateTiemChange}
            loading={loading}
          />
        </Form.Field>
        <Form.Group widths="equal">
          <Form.Field>
            <Button
              fluid
              primary
              className="visitDateOK"
              onClick={handleOkClick}
              is_mobile={isMobile.toString()}
            >
              {t('save.visit.n.move')}
            </Button>
          </Form.Field>
          <Form.Field>
            <Button
              fluid
              secondary
              className="unvisitedButton"
              onClick={handleDataCheckClick}
              is_mobile={isMobile.toString()}
              loading={loading}
            >
              {t('unvisited.data.check')}
            </Button>
          </Form.Field>
        </Form.Group>
      </Form>
    </Container>
  );
};

export default ConfirmSubjectVisit;
