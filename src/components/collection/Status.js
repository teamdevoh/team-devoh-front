import React, { Fragment } from 'react';
import { Icon, Label, Rating } from 'semantic-ui-react';
import MetaPopup from './MetaPopup';
import { useFormatMessage } from '../../hooks';
import styled from 'styled-components';

const IconNone = ({ onClick, requiredRegCnt }) => {
  return (
    <Icon
      name="plus circle"
      link
      size="large"
      onClick={onClick}
      color={requiredRegCnt > 0 ? 'green' : null}
    />
  );
};

const IconTemp = ({ onClick }) => {
  return (
    <Label circular onClick={onClick} style={{ cursor: 'pointer' }}>
      T
    </Label>
  );
};

const IconDone = ({ onClick }) => {
  return (
    <Label
      circular
      color="green"
      onClick={onClick}
      style={{ cursor: 'pointer' }}
    >
      S
    </Label>
  );
};

const IconNote = ({ color, onClick }) => {
  return (
    <Label
      circular
      color={color || 'blue'}
      style={{ cursor: 'pointer' }}
      onClick={onClick}
    >
      N
    </Label>
  );
};

const IconFile = ({ color, onClick }) => {
  return (
    <Label
      circular
      color={color || 'blue'}
      style={{ cursor: 'pointer' }}
      onClick={onClick}
    >
      F
    </Label>
  );
};

const IconAudit = ({ color, onClick }) => {
  return (
    <Label
      circular
      color={color || 'blue'}
      style={{ cursor: 'pointer' }}
      onClick={onClick}
    >
      AT
    </Label>
  );
};

const IconQueryEmpty = ({ onClick }) => {
  return (
    <Label
      circular
      color="grey"
      onClick={onClick}
      style={{ cursor: 'pointer' }}
    >
      Q
    </Label>
  );
};

const IconQueryOpen = ({ onClick }) => {
  return (
    <Label circular color="red" onClick={onClick} style={{ cursor: 'pointer' }}>
      O
    </Label>
  );
};

const IconQueryAnswer = ({ onClick }) => {
  return (
    <Label circular color="red" onClick={onClick} style={{ cursor: 'pointer' }}>
      A
    </Label>
  );
};

const IconQueryClose = ({ onClick }) => {
  return (
    <Label circular color="red" onClick={onClick} style={{ cursor: 'pointer' }}>
      C
    </Label>
  );
};

const IconReview = ({ number, onClick }) => {
  const StyledReview = styled(Label)`
    &&& {
      background-color: #ffe623;
      border-color: #ffe623;
      color: black;
      cursor: pointer;
    }
  `;

  return (
    <StyledReview circular onClick={onClick}>
      <ReviewName number={number} />
    </StyledReview>
  );
};

const ReviewName = ({ number }) => {
  return (
    <Fragment>
      {number === 1 && (
        <span>
          1<sup>st</sup>
        </span>
      )}
      {number === 2 && (
        <span>
          2<sup>nd</sup>
        </span>
      )}
      {number === 3 && (
        <span>
          3<sup>rd</sup>
        </span>
      )}
    </Fragment>
  );
};

const Review = ({ number, onClick }) => {
  const t = useFormatMessage();
  if (number && number > 0) {
    return (
      <MetaPopup
        trigger={
          <Rating
            icon="star"
            disabled
            defaultRating={number}
            maxRating={3}
            onClick={onClick}
          />
        }
      >
        <ReviewName number={number} /> {t('collection.review.completed')}
      </MetaPopup>
    );
  } else {
    return null;
  }
};

export {
  IconNone,
  IconTemp,
  IconDone,
  IconNote,
  IconFile,
  IconAudit,
  IconQueryEmpty,
  IconQueryOpen,
  IconQueryAnswer,
  IconQueryClose,
  IconReview,
  Review
};
