import React, { useCallback, useEffect } from 'react';
import { Grid, Dropdown } from 'semantic-ui-react';
import { Subject as SubjectInput } from './Selector';
import { useDispatch, useSelector } from 'react-redux';
import { subject } from '../../states';
import ActivityStep from './ActivityStep';
import QueryStep from './QueryStep';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import { useParams } from 'react-router-dom';
import { code } from '../../constants';
import MonthFilter from './MonthFilter';

const DashBoardHeader = ({ loading, statusCount, queryCountSet }) => {
  const dispatch = useDispatch();
  const {
    projectSiteId,
    subjectNo,
    ongoingMonths,
    caseConMonths
  } = useSelector(state => state.subject.filter);
  const { projectId } = useParams();
  const [items] = useMyProjectSiteList({ projectId, showSubjectPrefix: true });

  const handleSiteChange = useCallback((event, { name, value }) => {
    dispatch(
      subject.actions.filter({ projectSiteId: value, queryStatusQuery: '' })
    );
  });

  const handleSubjectNumberClick = useCallback(value => {
    dispatch(
      subject.actions.filter({ subjectNo: value, queryStatusQuery: '' })
    );
  }, []);

  const handleStatusClick = useCallback(
    value => {
      const monthsFiletr = {
        ongoingMonths: [],
        caseConMonths: []
      };

      if (value === code.SubjectOngoing) {
        monthsFiletr['ongoingMonths'] = ongoingMonths;
      } else if (value === code.SubjectCompletion) {
        monthsFiletr['caseConMonths'] = caseConMonths;
      }

      dispatch(
        subject.actions.filter({
          subjectStatusCode: value,
          queryStatusQuery: '',
          ...monthsFiletr
        })
      );
    },
    [ongoingMonths, caseConMonths]
  );

  const handleQueryClick = useCallback(value => {
    const query = value;
    dispatch(subject.actions.filter({ queryStatusQuery: query }));
  }, []);

  useEffect(
    () => {
      if (items.length > 0) {
        dispatch(
          subject.actions.filter({
            projectSiteId: projectSiteId,
            queryStatusQuery: ''
          })
        );
      }
    },
    [items]
  );

  return (
    <Grid stackable>
      <Grid.Column width="five">
        <Dropdown
          fluid
          selection
          value={projectSiteId}
          onChange={handleSiteChange}
          options={[{ key: 0, value: 0, text: 'All' }, ...items]}
        />
      </Grid.Column>
      <Grid.Column width="five">
        <SubjectInput value={subjectNo} onClick={handleSubjectNumberClick} />
      </Grid.Column>
      <Grid.Column
        width="6"
        style={{
          display: 'flex',
          alignItems: 'center',
          flexWrap: 'wrap'
        }}
      >
        <MonthFilter />
      </Grid.Column>
      <Grid.Column width="eight">
        <ActivityStep statusCount={statusCount} onClick={handleStatusClick} />
      </Grid.Column>
      <Grid.Column width="eight">
        <QueryStep queryCountSet={queryCountSet} onClick={handleQueryClick} />
      </Grid.Column>
    </Grid>
  );
};

export default DashBoardHeader;
