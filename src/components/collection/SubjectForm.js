import React, { useEffect, useState, Fragment } from 'react';
import {
  Divider,
  Dropdown,
  Form,
  Input,
  TextArea,
  Container
} from 'semantic-ui-react';
import { BackButton, SaveButton } from '../button';
import { useFormatMessage, useCodes, useSubmit } from '../../hooks';
import { useHistory, useParams } from 'react-router-dom';
import MobileInput from '../input/MobileInput';
import { notification } from '../modal';
import { useSubject } from './hooks';
import find from 'lodash/find';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import { SubjectStatus } from './Selector';
import { code, codeGroup, role } from '../../constants';
import { DatePicker } from '../datepicker';

DatePicker.preload();

const DropoutReasonSelector = ({ name, onChange, value }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.DropoutReason });
  return (
    <Dropdown
      name={name}
      options={items}
      value={value}
      search
      selection
      onChange={onChange}
    />
  );
};

const SubjectForm = () => {
  const t = useFormatMessage();
  const [subjectPrefix, setSubjectPrefix] = useState('');
  const { projectId, subjectId } = useParams();
  const history = useHistory();
  const [{ model, loading, onChange, onAdd, onEdit }] = useSubject(
    Number(subjectId)
  );
  const [myProjectSites] = useMyProjectSiteList({ projectId });

  const handleSubmit = async () => {
    if (Number(subjectId) > 0) {
      const is = await onEdit();
      if (is) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {}
        });
      }
    } else {
      const { newId, subjectNo } = await onAdd();
      if (newId > 0) {
        notification.info({
          title: t('collection.subject.no'),
          text: `${subjectPrefix}${subjectNo}`,
          confirmButtonName: t('common.ok'),
          onClose: () => {
            history.push(`/project/${projectId}/subject/${newId}/step/1`);
          }
        });
      }
    }
  };

  const [{ validator, onValidate }] = useSubmit(handleSubmit);

  const getSubjectPrefix = projectSiteId => {
    const selectedOption = find(myProjectSites, { value: projectSiteId });
    if (selectedOption) {
      setSubjectPrefix(selectedOption.subjectprefix);
    }
  };

  useEffect(
    () => {
      if (model.projectSiteId > 0) {
        getSubjectPrefix(model.projectSiteId);
      } else {
        setSubjectPrefix('');
      }
    },
    [myProjectSites, model.projectSiteId]
  );

  return (
    <Container>
      <Form onSubmit={onValidate}>
        <Form.Group widths="equal">
          <Form.Field required>
            <label>{t('site')}</label>
            <Dropdown
              name={'projectSiteId'}
              options={myProjectSites}
              value={model.projectSiteId}
              search
              selection
              onChange={onChange}
            />
            {validator.message(t('site'), model.projectSiteId, 'required')}
          </Form.Field>
          <Form.Field required>
            <label>{t('collection.subject.no')}</label>
            <div>
              {subjectPrefix}
              {model.subjectNo || '####'}
            </div>
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field required>
            <label>{t('collection.subject.name')}</label>
            <Input name="name" value={model.name} onChange={onChange} />
            {validator.message(
              t('collection.subject.name'),
              model.name,
              'required'
            )}
          </Form.Field>
          <Form.Field required>
            <label>{t('collection.subject.initial')}</label>
            <Input name="initial" value={model.initial} onChange={onChange} />
            {validator.message(
              t('collection.subject.initial'),
              model.initial,
              'required'
            )}
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('collection.hospital.no')}</label>
            <Input
              name="hospitalNo"
              value={model.hospitalNo}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('collection.subject.contact')}</label>
            <MobileInput
              name="contactNo"
              value={model.contactNo}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <label>{t('collection.subject.address')}</label>
            <Input
              name="address"
              value={model.address || ''}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field required>
            <label>{t('status')}</label>
            <SubjectStatus
              name="subjectStatusCode"
              value={model.subjectStatusCode}
              onChange={onChange}
            />
            {validator.message(
              t('status'),
              model.subjectStatusCode,
              'required'
            )}
          </Form.Field>
          {model.subjectStatusCode === code.SubjectDropout && (
            <Fragment>
              <Form.Field required>
                <label>{t('collection.subject.dropout.date')}</label>
                <DatePicker
                  name="dropoutDate"
                  value={model.dropoutDate}
                  onChange={onChange}
                />
                {validator.message(
                  t('collection.subject.dropout.date'),
                  model.dropoutDate,
                  'required'
                )}
              </Form.Field>
              <Form.Field required>
                <label>{t('collection.subject.dropout.reason')}</label>
                <DropoutReasonSelector
                  name="dropoutReasonCode"
                  value={model.dropoutReasonCode}
                  onChange={onChange}
                />
                {validator.message(
                  t('collection.subject.dropout.reason'),
                  model.dropoutReasonCode,
                  'required'
                )}
              </Form.Field>
            </Fragment>
          )}
        </Form.Group>
        {model.subjectStatusCode === code.SubjectDropout && (
          <Form.Group widths="equal">
            <Form.Field>
              <label>{t('dropout.detail')}</label>
              <TextArea
                name="dropoutDetail"
                value={model.dropoutDetail || ''}
                onChange={onChange}
              />
            </Form.Field>
          </Form.Group>
        )}
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('remark')}</label>
            <TextArea name="remark" value={model.remark} onChange={onChange} />
          </Form.Field>
        </Form.Group>
        <Divider />
        <BackButton name={t('common.back')} />
        <SaveButton
          allowedRole={role.Subject_Edit}
          name={t('common.save')}
          loading={loading}
        />
      </Form>
    </Container>
  );
};

export default SubjectForm;
