import React, { Fragment } from 'react';
import { useParams, useLocation } from 'react-router-dom';
import ActivityMenu from './ActivityMenu';
import helpers from '../../helpers';
import ActivityHeader from './ActivityHeader';
import useSubjectInfo from './hooks/useSubjectInfo';

const ActivityContainer = () => {
  const { subjectId, activityGroupId, projectId } = useParams();
  const search = useLocation().search;
  const qs = helpers.qs.parse(search);
  const subject = useSubjectInfo(subjectId);

  return (
    <Fragment>
      {subject.projectSiteId > 0 && (
        <Fragment>
          <ActivityHeader
            projectId={projectId}
            subjectId={Number(subjectId)}
            projectSiteId={subject.projectSiteId}
            onSubjectChange={keys => {
              helpers.history.push(
                `/project/${projectId}/subject/${
                  keys.subjectId
                }/activitygroup/${keys.activityGroupId}`
              );
            }}
          />
          <ActivityMenu
            projectActivityId={qs.projectActivityId}
            activityGroupId={activityGroupId}
            subjectId={Number(subjectId)}
            projectSiteId={subject.projectSiteId}
          />
        </Fragment>
      )}
    </Fragment>
  );
};

export default ActivityContainer;
