import React from 'react';
import styled from 'styled-components';
import { Popup } from 'semantic-ui-react';

const StyledPopup = styled(Popup)`
  border-radius: 0;
  opacity: 0.7;
  padding: 2em;
`;

const MetaPopup = ({ trigger, children }) => {
  return (
    <StyledPopup on="hover" inverted trigger={trigger}>
      {children}
    </StyledPopup>
  );
};

export default MetaPopup;
