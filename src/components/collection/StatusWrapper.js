import React, { Fragment } from 'react';
import {
  IconNone,
  IconTemp,
  IconDone,
  IconReview,
  IconQueryOpen,
  IconQueryAnswer,
  IconQueryClose,
  IconNote
} from './Status';
import { code } from '../../constants';

const StatusWrapper = ({
  activityStatusCodeId,
  queryStatusCodeId,
  hasNote,
  onClick,
  requiredRegCnt
}) => {
  return (
    <Fragment>
      {activityStatusCodeId === 0 && (
        <IconNone requiredRegCnt={requiredRegCnt} />
      )}
      {activityStatusCodeId === code.None && (
        <IconNone onClick={onClick} requiredRegCnt={requiredRegCnt} />
      )}
      {activityStatusCodeId === code.Temp && <IconTemp onClick={onClick} />}
      {activityStatusCodeId === code.Done && <IconDone onClick={onClick} />}
      {activityStatusCodeId === code.FirstSign && (
        <IconReview number={1} onClick={onClick} />
      )}
      {activityStatusCodeId === code.SecondSign && (
        <IconReview number={2} onClick={onClick} />
      )}
      {activityStatusCodeId === code.ThirdSign && (
        <IconReview number={3} onClick={onClick} />
      )}
      {queryStatusCodeId === code.QueryOpen && (
        <IconQueryOpen onClick={onClick} />
      )}
      {queryStatusCodeId === code.QueryAnswer && (
        <IconQueryAnswer onClick={onClick} />
      )}
      {queryStatusCodeId === code.QueryClose && (
        <IconQueryClose onClick={onClick} />
      )}
      {hasNote === true && <IconNote onClick={onClick} />}
    </Fragment>
  );
};

export default StatusWrapper;
