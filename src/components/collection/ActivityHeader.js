import React, { memo, Fragment, useCallback, useState } from 'react';
import { Table, Dropdown } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import useVisitListOfSubject from './hooks/useVisitListOfSubject';
import { useCodes } from '../../hooks';
import helpers from '../../helpers';
import { Subjects } from './Selector';
import { codeGroup, code } from '../../constants';

const Site = memo(({ projectId, value, onChange }) => {
  const [sites] = useMyProjectSiteList({ projectId, showSubjectPrefix: true });
  return (
    <Dropdown
      style={{ minWidth: '300px' }}
      search
      selection
      options={sites}
      value={value}
      onChange={onChange}
    />
  );
});

const VisitHeaderCells = memo(({ visitCodes }) => {
  return (
    <Fragment>
      {visitCodes.map(item => {
        return (
          <Table.HeaderCell key={item.value}>{item.text}</Table.HeaderCell>
        );
      })}
    </Fragment>
  );
});

const ActivityHeader = ({
  projectId,
  projectSiteId,
  subjectId,
  onSubjectChange
}) => {
  const [pSiteId, setPSiteId] = useState(projectSiteId);
  const [sbjId, setSbjId] = useState(subjectId);
  const [visitCodes] = useCodes({ codeGroupId: codeGroup.VisitTypeCode });
  const [subjectVisit, setSubjectVisits] = useVisitListOfSubject(subjectId);
  const { activityGroupId } = useParams();

  const handleSiteChange = useCallback(
    (event, data) => {
      setPSiteId(data.value);
      setSbjId(0);
      onSubjectChange({ subjectId: 0, activityGroupId });
      setSubjectVisits([]);
    },
    [pSiteId, sbjId, activityGroupId]
  );

  const handleSubjectChange = useCallback(
    (event, data) => {
      setSbjId(data.value);
      onSubjectChange({ subjectId: data.value, activityGroupId });
    },
    [pSiteId, sbjId, activityGroupId]
  );

  const getVisitDates = useCallback(
    value => {
      let dates = [];

      subjectVisit.items.forEach(visit => {
        if (visit.visitTypeCode === value) {
          if (value === code.TDMVisit) {
            const { scheduleDateTime, visitDateTime } = visit;

            const prefix = visitDateTime ? '(V)' : '(S)';
            dates.push(
              `${prefix} ${helpers.util.dateformat(
                visitDateTime || scheduleDateTime
              )}`
            );
          } else {
            dates.push(helpers.util.dateformat(visit.visitDateTime));
          }
        }
      });

      return dates.join('/');
    },
    [subjectVisit.items]
  );

  return (
    <Table celled compact="very" size="small">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell width={3}>
            <Site
              projectId={projectId}
              value={pSiteId}
              onChange={handleSiteChange}
            />
          </Table.HeaderCell>
          <VisitHeaderCells visitCodes={visitCodes} />
        </Table.Row>
        <Table.Row>
          <Table.Cell>
            <Subjects
              style={{ minWidth: '300px' }}
              projectSiteId={pSiteId}
              value={subjectId}
              onChange={handleSubjectChange}
            />
          </Table.Cell>
          {visitCodes.map(visit => {
            return (
              <Table.Cell key={visit.value}>
                {getVisitDates(visit.value)}
              </Table.Cell>
            );
          })}
        </Table.Row>
      </Table.Header>
    </Table>
  );
};

export default ActivityHeader;
