import React, { useState } from 'react';
import ActivityBoard from './ActivityBoard';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useSubjectActivityStatus } from './hooks';
import { useEffect } from 'react';
import { code } from '../../constants';

const DashBoard = () => {
  const [order, setOrder] = useState('asc');
  const initQueryCount = {
    [code.QueryOpen]: 0,
    [code.QueryAnswer]: 0,
    [code.QueryClose]: 0,
    sum: 0
  };
  const [queryCount, setQueryCount] = useState(initQueryCount);
  const { projectId } = useParams();
  const {
    projectSiteId,
    subjectStatusCode,
    subjectNo,
    queryStatusQuery,
    ongoingMonths,
    caseConMonths
  } = useSelector(state => state.subject.filter);

  const [
    { items, statusCount, loading, activityCount }
  ] = useSubjectActivityStatus({
    projectId,
    projectSiteId,
    subjectStatusCode,
    search: subjectNo,
    order,
    queryStatusQuery: queryStatusQuery,
    ongoingMonths,
    caseConMonths
  });

  const handleRequestSort = (event, order) => {
    setOrder(order === 'desc' ? 'desc' : 'asc');
  };

  useEffect(
    () => {
      setQueryCount(initQueryCount);
    },
    [projectSiteId, subjectStatusCode, subjectNo, ongoingMonths, caseConMonths]
  );

  useEffect(
    () => {
      if (queryCount.sum === 0) {
        buildQueryCountSet();
      }
    },
    [items]
  );

  const buildQueryCountSet = () => {
    const activities = [
      'interview',
      'medication',
      'test',
      'lab',
      'tdm',
      'adr',
      'caseConclusion'
    ];

    let sumOpen = 0;
    let sumAnswer = 0;
    let sumClose = 0;
    let sumQuery = 0;
    _.forEach(items, item => {
      let hasOpenQuery = false;
      let hasAnswerQuery = false;
      let hasCloseQuery = false;
      let hasQuery = false;

      activities.forEach(activity => {
        const queryStatusCodeId = item[activity].lastQueryStatusCodeId;

        switch (queryStatusCodeId) {
          case code.QueryOpen:
            hasOpenQuery = true;
            hasQuery = true;
            break;
          case code.QueryAnswer:
            hasAnswerQuery = true;
            hasQuery = true;
            break;
          case code.QueryClose:
            hasCloseQuery = true;
            hasQuery = true;
            break;
          default:
            break;
        }
      });

      sumOpen += hasOpenQuery ? 1 : 0;
      sumAnswer += hasAnswerQuery ? 1 : 0;
      sumClose += hasCloseQuery ? 1 : 0;
      sumQuery += hasQuery ? 1 : 0;
    });

    setQueryCount({
      [code.QueryOpen]: sumOpen,
      [code.QueryAnswer]: sumAnswer,
      [code.QueryClose]: sumClose,
      sum: sumQuery
    });
  };

  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            statusCount={statusCount}
            queryCountSet={queryCount}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool onRequestSort={handleRequestSort} order={order} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <ActivityBoard
            projectId={projectId}
            loading={loading}
            items={items}
            activityCount={activityCount}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
