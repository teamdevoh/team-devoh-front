import React from 'react';
import { Container } from 'semantic-ui-react';
import SubjectForm from './SubjectForm';
import SubjectVisitForm from './SubjectVisitForm';
import { useParams, useHistory } from 'react-router-dom';
import Anchor from '../button/Anchor';
import SubjectStep from './SubjectStep';

const SubjectContainer = () => {
  const { index } = useParams();
  const history = useHistory();

  const handleBackClick = () => {
    history.goBack();
  };

  return (
    <Container>
      <p>
        <Anchor text="Go Back" onClick={handleBackClick} />
      </p>
      <SubjectStep />
      {index === '0' && <SubjectForm />}
      {index === '1' && <SubjectVisitForm />}
    </Container>
  );
};

export default SubjectContainer;
