import React, { useCallback } from 'react';
import { Step, Icon } from 'semantic-ui-react';
import { useParams, useHistory } from 'react-router-dom';

const SubjectStep = () => {
  const { index, projectId, subjectId } = useParams();
  const history = useHistory();

  const handleSubjectClick = useCallback(
    () => {
      history.push(`/project/${projectId}/subject/${subjectId}/step/0`);
    },
    [subjectId]
  );

  const handleVisitClick = useCallback(
    () => {
      history.push(`/project/${projectId}/subject/${subjectId}/step/1`);
    },
    [subjectId]
  );

  return (
    <Step.Group>
      <Step active={index === '0'} onClick={handleSubjectClick}>
        <Icon name="user" />
        <Step.Content>
          <Step.Title>Subject</Step.Title>
          <Step.Description>Enter subject information</Step.Description>
        </Step.Content>
      </Step>
      <Step
        active={index === '1'}
        disabled={subjectId === '0'}
        onClick={handleVisitClick}
      >
        <Icon name="calendar alternate outline" />
        <Step.Content>
          <Step.Title>Visit</Step.Title>
          <Step.Description>Choose visit schedule</Step.Description>
        </Step.Content>
      </Step>
    </Step.Group>
  );
};

export default SubjectStep;
