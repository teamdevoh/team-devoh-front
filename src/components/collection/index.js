import { asyncComponent } from '../modules';

const DashBoard = asyncComponent(() => import('./DashBoard'));
const ActivityStatusPanel = asyncComponent(() =>
  import('./ActivityStatusPanel')
);
const ActivitySummary = asyncComponent(() => import('./ActivitySummary'));
const SummaryWrapper = asyncComponent(() => import('./SummaryWrapper'));

const AuthorInfo = asyncComponent(() => import('./AuthorInfo'));

export {
  DashBoard,
  ActivityStatusPanel,
  ActivitySummary,
  SummaryWrapper,
  NoteContainer,
  AuthorInfo
};
