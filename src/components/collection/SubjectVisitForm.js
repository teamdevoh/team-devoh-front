import React, { Fragment, useCallback, useEffect, useRef } from 'react';
import {
  Message,
  Form,
  Checkbox,
  Select,
  Icon,
  Placeholder,
  Segment
} from 'semantic-ui-react';
import DateTimePicker from '../datepicker/DateTimePicker';
import { SaveButton } from '../button';
import { useFormatMessage, useCodes } from '../../hooks';
import {
  useSubjectVisitEdit,
  useSubjectVisitAdd,
  useSubject,
  useActivityGroupList
} from './hooks';
import { useParams, useHistory } from 'react-router-dom';
import helpers from '../../helpers';
import filter from 'lodash/filter';
import { notification } from '../modal';
import update from 'react-addons-update';
import useVisitListOfSubject from './hooks/useVisitListOfSubject';
import styled from 'styled-components';
import { code, codeGroup, role } from '../../constants';

const VisitWrapper = styled(Segment)`
  &&& {
    ${props => {
      return {
        backgroundColor: props.isedit === 'true' ? '#e0e1e2' : ''
      };
    }};
  }
`;

const VisitInfo = ({ subjectId }) => {
  const t = useFormatMessage();
  const [subject] = useSubject(subjectId);
  return (
    <Fragment>
      {!subject.loading && (
        <Message info>
          <Message.Header>
            {subject.model.subjectPrefix}
            {subject.model.subjectNo}[{subject.model.initial}]{' '}
            {t('subject.visit.schedule')}
          </Message.Header>
          <p>{t('subject.visit.schedule.info')}</p>
        </Message>
      )}
      {subject.loading && (
        <Placeholder fluid>
          <Placeholder.Line length="very short" />
          <Placeholder.Line length="short" />
          <Placeholder.Line length="long" />
        </Placeholder>
      )}
    </Fragment>
  );
};

const AddVisit = ({ onClick, order }) => {
  const t = useFormatMessage();
  return (
    <div>
      <Icon name="arrow right" />
      <a onClick={onClick} style={{ cursor: 'pointer' }}>
        {t('reg.next.visit.date.info', { order })}
      </a>
    </div>
  );
};

const SubjectVisitForm = () => {
  const { subjectId, projectId } = useParams();
  const [{ onAdd }] = useSubjectVisitAdd(subjectId);
  const [{ onEdit }] = useSubjectVisitEdit(subjectId);
  const t = useFormatMessage();
  const [codes] = useCodes({ codeGroupId: codeGroup.VisitTypeCode });
  const [{ items, loading }, setItems] = useVisitListOfSubject(subjectId);
  const isNext = useRef(false);
  const history = useHistory();
  const [activityGroupList] = useActivityGroupList({ projectId });

  const getIsNext = () => {
    return isNext.current;
  };

  const setIsNext = next => {
    isNext.current = next;
  };

  useEffect(
    () => {
      if (items.length === 0 && !loading) {
        setItems([
          {
            subjectVisitId: 0,
            subjectId,
            visitTypeCode: code.SubjectAgree,
            scheduleDateTime: null,
            visitDateTime: null,
            isAlarm: false
          }
        ]);
      }
    },
    [items, loading]
  );

  useEffect(() => {
    if (subjectId === '0') {
      throw new Error('Require is Subject ID');
    }
  });

  const handleAddVisitClick = useCallback(
    () => {
      const merged = items.concat({
        subjectVisitId: 0,
        subjectId,
        visitTypeCode: 0,
        scheduleDateTime: null,
        visitDateTime: '',
        isAlarm: true,
        changed: true
      });

      setItems(merged);
    },
    [items]
  );

  const handleChange = (event, { name, value, index }) => {
    setItems(
      update(items, {
        [index]: { [name]: { $set: value }, changed: { $set: true } }
      })
    );
  };

  const handleSubmit = useCallback(
    async index => {
      const model = items[index];

      const isMove = move => {
        if (move) {
          const { visitTypeCode, visitOrder } = model;

          let to = `/project/${projectId}/subject/${subjectId}/activitygroup`;

          switch (visitTypeCode) {
            case code.SubjectAgree:
              to += `/${_.find(activityGroupList, { key: 1 }).value}`;
              break;
            case code.TDMVisit:
              const otherTdmVisit = items.filter(item => {
                const { subjectVisitId, visitTypeCode } = item;

                return (
                  subjectVisitId !== model.subjectVisitId &&
                  visitTypeCode === code.TDMVisit
                );
              });

              const isFirstTdm = otherTdmVisit.length === 0 || visitOrder === 1;

              to += `/${
                isFirstTdm
                  ? _.find(activityGroupList, { key: 5 }).value
                  : _.find(activityGroupList, { key: 15 }).value
              }`;
              break;
            case code.UnscheduledVisit:
              to += `/${_.find(activityGroupList, { key: 6 }).value}`;
              break;
            case code.CaseConclusion:
              to += `/${_.find(activityGroupList, { key: 7 }).value}`;
              break;
            default:
              to += `/${_.find(activityGroupList, { key: 1 }).value}`;
              break;
          }

          history.push(to);
        }
      };

      if (model.subjectVisitId > 0) {
        const is = await onEdit(model);
        if (is) {
          notification.success({
            title: t('common.alert.changed'),
            onClose: () => {
              setItems(
                update(items, {
                  [index]: { changed: { $set: false } }
                })
              );
              isMove(getIsNext());
            }
          });
        }
      } else {
        const newId = await onAdd(model);
        if (newId > 0) {
          notification.success({
            title: t('common.alert.added'),
            onClose: () => {
              setItems(
                update(items, {
                  [index]: {
                    subjectVisitId: { $set: newId },
                    changed: { $set: false }
                  }
                })
              );
              isMove(getIsNext());
            }
          });
        }
      }
    },
    [items, activityGroupList]
  );

  const isFirstVisit = visitTypeCode => {
    return visitTypeCode === code.SubjectAgree;
  };

  const getVisitOptions = visitTypeCode => {
    const isFirst = isFirstVisit(visitTypeCode);
    let visitOptions = [];
    if (isFirst) {
      visitOptions = filter(codes, { value: code.SubjectAgree });
    } else {
      visitOptions = filter(
        codes,
        option => option.value !== code.SubjectAgree
      );
    }
    return visitOptions;
  };

  const handleSaveAndMoveClick = () => {
    setIsNext(true);
  };

  return (
    <Fragment>
      <VisitInfo subjectId={subjectId} />
      {items.map((item, i) => {
        const isFirst = isFirstVisit(item.visitTypeCode);
        let visitOptions = getVisitOptions(item.visitTypeCode);

        return (
          <VisitWrapper key={i} isedit={(!!item.changed).toString()}>
            <Form
              onSubmit={async () => {
                await handleSubmit(i);
              }}
            >
              <Form.Group widths="equal">
                <Form.Field>
                  <label>{t('visit.type')}</label>
                  <Select
                    fluid
                    name="visitTypeCode"
                    options={visitOptions}
                    value={item.visitTypeCode}
                    onChange={(event, { name, value }) => {
                      handleChange(event, { name, value, index: i });
                    }}
                  />
                </Form.Field>
                {!isFirst && (
                  <Form.Field>
                    <label>{t('scheduled.visit.datetime')}</label>
                    <DateTimePicker
                      name="scheduleDateTime"
                      value={item.scheduleDateTime}
                      onChange={(event, { name, value }) => {
                        handleChange(event, {
                          name,
                          value: helpers.util.dateformat(value),
                          index: i
                        });
                      }}
                    />
                  </Form.Field>
                )}

                <Form.Field>
                  <label>{t('visit.datetime')}</label>
                  <DateTimePicker
                    name="visitDateTime"
                    value={item.visitDateTime}
                    onChange={(event, { name, value }) => {
                      handleChange(event, {
                        name,
                        value: helpers.util.dateformat(value),
                        index: i
                      });
                    }}
                  />
                </Form.Field>
              </Form.Group>
              {!isFirst && (
                <Form.Group>
                  <Form.Field>
                    <Checkbox
                      label={t('noti.day.before.visit')}
                      name="isAlarm"
                      checked={item.isAlarm}
                      onChange={(event, data) => {
                        handleChange(event, {
                          name: data.name,
                          value: data.checked,
                          index: i
                        });
                      }}
                    />
                  </Form.Field>
                </Form.Group>
              )}
              <SaveButton
                size="tiny"
                allowedRole={role.Subject_Edit}
                name={t('common.save')}
                loading={false}
              />
              <SaveButton
                size="tiny"
                allowedRole={role.Subject_Edit}
                name={t('common.save.move')}
                loading={false}
                onClick={handleSaveAndMoveClick}
              />
            </Form>
          </VisitWrapper>
        );
      })}

      {items.length > 0 &&
        items[items.length - 1].subjectVisitId > 0 && (
          <AddVisit order={items.length + 1} onClick={handleAddVisitClick} />
        )}
    </Fragment>
  );
};

export default SubjectVisitForm;
