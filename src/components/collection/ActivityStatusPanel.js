import React from 'react';
import { Table } from 'semantic-ui-react';
import {
  IconNone,
  IconTemp,
  IconDone,
  Review,
  IconQueryOpen,
  IconQueryAnswer,
  IconQueryClose,
  IconNote
} from './Status';
import { code } from '../../constants';

const ActivityStatusPanel = ({ items, onModalStatusClick }) => {
  return (
    <Table singleLine>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Activity</Table.HeaderCell>
          <Table.HeaderCell>Status</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map(item => {
          return (
            <Table.Row key={item.projectActivityId}>
              <Table.Cell>{item.name}</Table.Cell>
              <Table.Cell
                style={{ cursor: 'pointer' }}
                onClick={onModalStatusClick(item)}
              >
                {item.activityStatusCodeId === code.Temp && <IconTemp />}
                {item.activityStatusCodeId === code.Done && <IconDone />}
                {item.activityStatusCodeId === 0 && <IconNone />}
                {item.activityStatusCodeId === code.FirstSign && (
                  <Review number={1} />
                )}
                {item.activityStatusCodeId === code.SecondSign && (
                  <Review number={2} />
                )}
                {item.activityStatusCodeId === code.ThirdSign && (
                  <Review number={3} />
                )}

                {item.queryStatusCodeId === code.QueryOpen && <IconQueryOpen />}
                {item.queryStatusCodeId === code.QueryAnswer && (
                  <IconQueryAnswer />
                )}
                {item.queryStatusCodeId === code.QueryClose && (
                  <IconQueryClose />
                )}
                {item.hasNote && <IconNote />}
              </Table.Cell>
            </Table.Row>
          );
        })}
      </Table.Body>
    </Table>
  );
};

export default ActivityStatusPanel;
