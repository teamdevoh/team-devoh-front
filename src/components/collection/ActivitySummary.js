import React, { useCallback, useState, Fragment } from 'react';
import api from './api';
import { Modal } from '../modal';
import { Icon } from 'semantic-ui-react';
import { useBoolean } from '../../hooks';
import { useNoteStatusWithAll } from '../activity-note/hooks';
import { useQueryStatusWithAll } from '../activity-query/hooks';
import { ActivityStatusPanel } from './';
import StatusWrapper from './StatusWrapper';
import { code } from '../../constants';

const ActivitySummary = ({
  value,
  isScrolling,
  row,
  rowMasterKey,
  modalTitle,
  onStatusClick,
  onModalStatusClick
}) => {
  const visible = useBoolean(false);
  const [items, setItems] = useState([]);
  const [selectedIds, setSelectedIds] = useState({
    activityGroupId: 0,
    masterKey: row[rowMasterKey]
  });

  const { getNoteStatus } = useNoteStatusWithAll({
    subjectId: selectedIds.masterKey
  });

  const { getQueryStatus } = useQueryStatusWithAll({
    subjectId: selectedIds.masterKey
  });

  const fetchItems = async ({ activityGroupId, subjectId }) => {
    const res = await api.fetchActivitiesOfGroup({
      activityGroupId,
      subjectId
    });

    res.data.forEach(item => {
      item.activityKeyId = 0;
    });
    await getNoteStatus(res.data);
    await getQueryStatus(res.data);

    setItems(res.data);
  };

  if (value.minActivityStatusCodeId === 0) {
    value.minActivityStatusCodeId = code.None;
  }

  const handleStatusClick = useCallback(
    () => {
      onStatusClick();
    },
    [row]
  );

  return (
    <div>
      <StatusWrapper
        activityStatusCodeId={value.minActivityStatusCodeId}
        queryStatusCodeId={value.lastQueryStatusCodeId}
        hasNote={value.hasNote}
        onClick={handleStatusClick}
        requiredRegCnt={value.requiredRegCnt}
      />
      {value.count > 1 && (
        <Fragment>
          <Icon
            name="search plus"
            size="large"
            link
            onClick={() => {
              visible.setTrue();
              setSelectedIds({
                activityGroupId: value.activityGroupId,
                masterKey: row[rowMasterKey]
              });
              fetchItems({
                activityGroupId: value.activityGroupId,
                subjectId: row[rowMasterKey]
              });
            }}
          />
          <Modal
            open={visible.value}
            onOK={visible.setFalse}
            title={modalTitle}
            content={
              <ActivityStatusPanel
                items={items}
                activityGroupId={selectedIds.activityGroupId}
                subjectId={selectedIds.masterKey}
                onModalStatusClick={onModalStatusClick}
              />
            }
          />
        </Fragment>
      )}
    </div>
  );
};

export default ActivitySummary;
