import React, { useCallback, useEffect, useState } from 'react';
import update from 'react-addons-update';
import { useSelector } from 'react-redux';
import StepGroup from '../element/StepGroup';
import { code } from '../../constants';
import { useFormatMessage } from '../../hooks';

const ActivityStep = ({ statusCount, onClick }) => {
  const { subjectStatusCode } = useSelector(state => state.subject.filter);
  const t = useFormatMessage();
  const [items, setItems] = useState([]);

  useEffect(
    () => {
      const status = [
        {
          name: t('collection.subject.register'),
          value: 0,
          length: statusCount.register,
          active: 0 === subjectStatusCode
        },
        {
          name: t('collection.subject.dropout'),
          value: code.SubjectDropout,
          length: statusCount.dropout,
          active: code.SubjectDropout === subjectStatusCode
        },
        {
          name: t('collection.subject.complete'),
          value: code.SubjectCompletion,
          length: statusCount.complete,
          active: code.SubjectCompletion === subjectStatusCode
        },
        {
          name: t('collection.subject.ongoing'),
          value: code.SubjectOngoing,
          length: statusCount.ongoing,
          active: code.SubjectOngoing === subjectStatusCode
        }
      ];

      setItems(status);
    },
    [statusCount]
  );

  const handleClick = useCallback(
    (event, data) => {
      items.forEach(item => {
        item.active = false;
      });
      const updated = update(items, {
        [data.index]: { active: { $set: true } }
      });

      setItems(updated);
      onClick(data.value);
    },
    [onClick]
  );

  return <StepGroup items={items} onClick={!!onClick ? handleClick : null} />;
};

export default ActivityStep;
