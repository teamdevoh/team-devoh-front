import React, { useCallback, useEffect, useState } from 'react';
import useQueries from './hooks/useQueries';
import useQueryHistories from './hooks/useQueryHistories';
import { Grid, Dropdown, Header } from 'semantic-ui-react';
import QueryList from './QueryList';
import QueryHistoryList from './QueryHistoryList';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import { Subjects } from './Selector';
import StepGroup from '../element/StepGroup';
import { useFormatMessage, useProjectMemberKeyPair } from '../../hooks';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';
import helpers from '../../helpers';
import { useParams } from 'react-router-dom';
import { code } from '../../constants';

const QueryContainer = () => {
  const { projectId } = useParams();
  const t = useFormatMessage();
  const [selectedQueryRow, setSelectedQueryRow] = useState({
    activityQueryId: 0
  });
  const [selectedProjectSiteId, setSelectedProjectSiteId] = useState(0);
  const [selectedSubjectId, setSelectedSubjectId] = useState(0);
  const [projectMemberId, setProjectMemberId] = useState(0);
  const [memberId, setMemberId] = useState(0);
  const [selectedQuery, setSelectedQuery] = useState(0);
  const [queryCount, setQueryCount] = useState([]);
  const query = useQueries(projectId);
  const [filteredQueries, setFilteredQueries] = useState([]);
  const queryHistory = useQueryHistories(projectId, selectedQueryRow);
  const [sites] = useMyProjectSiteList({ projectId, showSubjectPrefix: true });
  const member = useProjectMemberKeyPair(
    {
      projectId,
      projectSiteId: selectedProjectSiteId,
      holder: t('common.all')
    },
    selectedProjectSiteId
  );

  const handleChangeProjectSite = useCallback((event, data) => {
    setSelectedSubjectId(0);
    setSelectedProjectSiteId(data.value);
  }, []);

  const handleChangeSubject = useCallback((event, data) => {
    setSelectedSubjectId(data.value);
  }, []);

  const handleChangeProjectMember = useCallback((event, data) => {
    const selected = data.options.filter(item => item.value === data.value);

    setProjectMemberId(data.value);
    setMemberId(!!selected[0].data ? selected[0].data.memberId : 0);
  }, []);

  const handleChangeQuery = useCallback(
    (event, data) => {
      _.forEach(queryCount, item => {
        item.active = false;
        if (item.value === data.value) {
          item.active = true;
        }
      });

      setQueryCount(queryCount);
      setSelectedQuery(data.value);
    },
    [queryCount]
  );

  useEffect(
    () => {
      const filteredItems = _.filter(
        query.items,
        item =>
          (selectedProjectSiteId === 0 ||
            item.projectSiteId === selectedProjectSiteId) &&
          (selectedSubjectId === 0 || item.subjectId === selectedSubjectId) &&
          (memberId === 0 || item.memberId === memberId)
      );

      setFilteredQueries(filteredItems);
    },
    [query.items, selectedProjectSiteId, selectedSubjectId, memberId]
  );

  useEffect(
    () => {
      setProjectMemberId(0);
      setMemberId(0);
    },
    [selectedProjectSiteId]
  );

  const calcQuery = useCallback(
    () => {
      let open = _.countBy(filteredQueries, {
        queryStatusCodeId: code.QueryOpen
      }).true;
      let answer = _.countBy(filteredQueries, {
        queryStatusCodeId: code.QueryAnswer
      }).true;
      let close = _.countBy(filteredQueries, {
        queryStatusCodeId: code.QueryClose
      }).true;

      open = open === void 0 ? 0 : open;
      answer = answer === void 0 ? 0 : answer;
      close = close === void 0 ? 0 : close;

      return { open, answer, close };
    },
    [filteredQueries]
  );

  const buildQueryCountSet = useCallback(
    ({ open, answer, close }) => {
      setQueryCount([
        {
          value: 0,
          name: t('all'),
          active: selectedQuery === 0 ? true : false,
          length: open + answer + close
        },
        {
          value: code.QueryOpen,
          name: t('collection.query.status.open'),
          active: selectedQuery === code.QueryOpen ? true : false,
          length: open
        },
        {
          value: code.QueryAnswer,
          name: t('collection.query.status.answer'),
          active: selectedQuery === code.QueryAnswer ? true : false,
          length: answer
        },
        {
          value: code.QueryClose,
          name: t('collection.query.status.close'),
          active: selectedQuery === code.QueryClose ? true : false,
          length: close
        }
      ]);
    },
    [selectedQuery]
  );

  useEffect(
    () => {
      const { open, answer, close } = calcQuery();
      buildQueryCountSet({ open, answer, close });
    },
    [filteredQueries]
  );

  const handleActivityClick = useCallback(row => {
    helpers.history.push(
      `/project/${projectId}/subject/${row.subjectId}/activitygroup/${
        row.activityGroupId
      }?projectActivityId=${row.projectActivityId}&activityId=${row.activityId}`
    );
  }, []);

  const handleRowClick = useCallback((rowIdx, row) => {
    setSelectedQueryRow(row);
  }, []);

  return (
    <div>
      <Grid stackable>
        <Grid.Row>
          <Grid.Column>
            <Header as="h3" dividing>
              Query
              <Header.Subheader>
                You can see all queries of a Project
              </Header.Subheader>
              <CopyRedirectUrl />
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column computer="four" mobile="eight">
            <Dropdown
              search
              fluid
              selection
              options={[{ key: 0, text: '-', value: 0 }].concat(sites)}
              value={selectedProjectSiteId}
              onChange={handleChangeProjectSite}
            />
          </Grid.Column>
          <Grid.Column computer="three" mobile="eight">
            <Subjects
              projectSiteId={selectedProjectSiteId}
              value={selectedSubjectId}
              onChange={handleChangeSubject}
              hasAll={true}
              fluid={true}
            />
          </Grid.Column>
          <Grid.Column computer="three" mobile="eight">
            <Dropdown
              placeholder=""
              selection
              search
              fluid
              options={member.items}
              onChange={handleChangeProjectMember}
              value={projectMemberId}
              loading={member.loading}
            />
          </Grid.Column>
          <Grid.Column computer="six" mobile="sixteen">
            <StepGroup items={queryCount} onClick={handleChangeQuery} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width="nine">
            <QueryList
              items={_.filter(filteredQueries, item => {
                return (
                  selectedQuery === 0 ||
                  item.queryStatusCodeId === selectedQuery
                );
              })}
              setFilteredQueries={setFilteredQueries}
              loading={query.loading}
              onActivityClick={handleActivityClick}
              onRowClick={handleRowClick}
            />
          </Grid.Column>
          <Grid.Column width="one" />
          <Grid.Column width="five">
            <QueryHistoryList
              items={queryHistory.items}
              loading={queryHistory.loading}
            />
          </Grid.Column>
          <Grid.Column width="one" />
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default QueryContainer;
