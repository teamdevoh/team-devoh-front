import React, { memo } from 'react';
import { Dropdown } from 'semantic-ui-react';
import SearchInput from '../input/SearchInput';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import { useCodes, useFormatMessage } from '../../hooks';
import { useSubjectList } from './hooks';
import { codeGroup } from '../../constants';

const ProjectSite = ({
  name,
  value,
  onChange,
  projectId,
  isSelect = false,
  ...rest
}) => {
  const t = useFormatMessage();
  const [items] = useMyProjectSiteList({ projectId });

  let options = [
    {
      text: t('common.all'),
      value: 0,
      subjectprefix: ''
    }
  ];

  return (
    <Dropdown
      fluid
      selection
      name={name}
      defaultValue={value}
      onChange={onChange}
      options={options.concat(items)}
      {...rest}
    />
  );
};

const ProjectSiteForChart = ({
  name,
  value,
  onChange,
  projectId,
  isSelect = false,
  ...rest
}) => {
  const t = useFormatMessage();
  const [items] = useMyProjectSiteList({ projectId });

  let options = [
    {
      text: t('common.all'),
      value: 0,
      subjectprefix: ''
    }
  ];

  return (
    <Dropdown
      fluid
      selection
      name={name}
      value={value}
      onChange={onChange}
      options={options.concat(items)}
      {...rest}
    />
  );
};

const Subjects = memo(
  ({
    projectSiteId,
    value,
    onChange,
    reset = false,
    hasAll = false,
    ...rest
  }) => {
    const [subject] = useSubjectList({ projectSiteId: projectSiteId });

    const options = hasAll
      ? [
          {
            text: '-',
            value: 0
          },
          ...subject.items
        ]
      : subject.items;

    return (
      <Dropdown
        search
        selection
        options={reset ? [] : options}
        value={value}
        onChange={onChange}
        {...rest}
      />
    );
  }
);

const Activity = ({ projectId, onChange }) => {
  const options = [
    {
      text: '-----------------------------------------',
      value: '-'
    }
  ];

  return (
    <Dropdown item defaultValue={'-'} onChange={onChange} options={options} />
  );
};

const Subject = ({ value, onClick }) => {
  return <SearchInput value={value} onClick={onClick} />;
};

const Visits = ({ onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.VisitTypeCode });
  return (
    <Dropdown
      item
      defaultValue={0}
      onChange={onChange}
      options={[
        { text: '------------------------------------', value: 0 }
      ].concat(items)}
    />
  );
};

const SubjectStatus = ({ name, onChange, value, useAll = false }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.SubjectStatus });
  return (
    <Dropdown
      name={name}
      options={useAll ? [{ key: 0, text: '-', value: 0 }, ...items] : items}
      value={value}
      search
      selection
      onChange={onChange}
    />
  );
};

export {
  ProjectSite,
  ProjectSiteForChart,
  Subjects,
  SubjectStatus,
  Activity,
  Subject,
  Visits
};
