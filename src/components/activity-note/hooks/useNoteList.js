import { useCallback, useEffect } from 'react';
import api from '../api';
import { useBoolean } from '../../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import { activity } from '../../../states';

const useNoteList = ({ subjectId, activityKeyId, projectActivityId }) => {
  const isLoading = useBoolean(false);
  const dispatch = useDispatch();
  const notes = useSelector(activity.selectors.notes);

  const fetchItems = useCallback(
    async () => {
      isLoading.setTrue();
      const res = await api.fetchNotes({
        subjectId,
        activityKeyId,
        projectActivityId
      });
      if (res && res.data) {
        dispatch(activity.actions.fetchNotes(res.data));
      }
      isLoading.setFalse();
    },
    [subjectId, activityKeyId]
  );

  useEffect(
    () => {
      fetchItems();
    },
    [subjectId, activityKeyId]
  );

  return [{ items: notes, loading: isLoading.value }];
};

export default useNoteList;
