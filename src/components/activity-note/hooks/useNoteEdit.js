import { useCallback } from 'react';
import api from '../api';
import { useDispatch, useSelector } from 'react-redux';
import { activity, project } from '../../../states';
import { useBoolean } from '../../../hooks';
import helpers from '../../../helpers';

const useNoteEdit = ({ subjectId, activityKeyId, activityNoteId }) => {
  const loading = useBoolean(false);
  const dispatch = useDispatch();
  const selectedProject = useSelector(project.selectors.getSelectedItem);

  const add = useCallback(
    async model => {
      loading.setTrue();
      const res = await api.addNote({ subjectId, activityKeyId, model });
      if (res && res.data) {
        dispatch(
          activity.actions.addNote({
            activityNoteId: res.data,
            note: model.note,
            memberId: helpers.Identity.profile.id,
            myFaceFileStorageId: helpers.Identity.profile.faceId,
            author: helpers.Identity.profile.name,
            team: selectedProject.role,
            created: Date.now()
          })
        );
      }
      loading.setFalse();
      return res.data > 0 ? true : false;
    },
    [subjectId, activityKeyId]
  );

  const edit = useCallback(
    async model => {
      loading.setTrue();
      const res = await api.updateNote({
        subjectId,
        activityKeyId,
        activityNoteId,
        model
      });
      if (res && res.data) {
        dispatch(activity.actions.editNote({ ...model, created: Date.now() }));
      }
      loading.setFalse();
      return res.data ? true : false;
    },
    [subjectId, activityKeyId, activityNoteId]
  );

  const remove = useCallback(
    async () => {
      loading.setTrue();
      const res = await api.removeNote({
        subjectId,
        activityKeyId,
        activityNoteId
      });
      if (res && res.data) {
        dispatch(activity.actions.removeNote(activityNoteId));
      }
      loading.setFalse();
      return res.data ? true : false;
    },
    [subjectId, activityKeyId, activityNoteId]
  );

  return { add, edit: edit, remove, loading: loading.value };
};

export default useNoteEdit;
