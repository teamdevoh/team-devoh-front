import { useEffect } from 'react';
import api from '../api';
import { useBoolean } from '../../../hooks';

const useNoteStatus = ({ subjectId, activityKeyId, projectActivityId }) => {
  const hasNote = useBoolean(false);
  const isLoading = useBoolean(true);

  const fetchItem = async () => {
    const res = await api.fetchActivityNoteStatus({
      subjectId,
      activityKeyId,
      projectActivityId
    });
    if (res) {
      hasNote.setValue(res.data);
    }
    isLoading.setFalse();
  };

  useEffect(
    () => {
      if (subjectId > 0 && projectActivityId > 0 && activityKeyId !== void 0) {
        fetchItem();
      }
    },
    [subjectId, activityKeyId, projectActivityId]
  );

  return [
    { hasNote: hasNote.value, loading: isLoading.value },
    hasNote.setValue
  ];
};

export default useNoteStatus;
