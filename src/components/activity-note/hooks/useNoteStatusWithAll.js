import api from '../api';

const useNoteStatusWithAll = ({ subjectId }) => {
  const getNoteStatus = items => {
    return Promise.all(
      items.map(async item => {
        const res = await api.fetchActivityNoteStatus({
          subjectId,
          activityKeyId: item.activityKeyId,
          projectActivityId: item.projectActivityId
        });

        item['hasNote'] = false;
        if (res && res.data) {
          item['hasNote'] = res.data;
        }

        return item;
      })
    );
  };

  return { getNoteStatus };
};

export default useNoteStatusWithAll;
