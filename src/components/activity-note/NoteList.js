import React, { Fragment } from 'react';
import { Feed, Divider } from 'semantic-ui-react';
import { useNoteList } from './hooks';
import NoteListItem from './NoteListItem';

const NoteList = props => {
  const [note] = useNoteList({
    subjectId: props.subjectId,
    activityKeyId: props.activityKeyId,
    projectActivityId: props.projectActivityId
  });

  return (
    <Feed>
      {note.items.map((item, index) => {
        return (
          <Fragment>
            <NoteListItem
              key={index}
              index={index}
              {...item}
              {...props}
              onDeleteItemClick={() => {}}
            />
            <Divider />
          </Fragment>
        );
      })}
    </Feed>
  );
};

export default NoteList;
