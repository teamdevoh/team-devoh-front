import React, { useEffect, useRef } from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import { SaveButton } from '../button';
import { useFormatMessage } from '../../hooks';
import { role } from '../../constants';

const NoteForm = ({ loading, model, onChange, onSubmit }) => {
  const t = useFormatMessage();
  const textareaRef = useRef();

  useEffect(() => {
    textareaRef.current.focus();
  }, []);

  return (
    <Form onSubmit={onSubmit}>
      <Form.Field width="sixteen">
        <TextArea
          name="note"
          placeholder=""
          value={model.note}
          onChange={onChange}
          rows="2"
          ref={textareaRef}
        />
      </Form.Field>
      <Form.Field>
        <SaveButton
          size="tiny"
          allowedRole={role.Activity_Read}
          name={t('common.save')}
          loading={loading}
        />
      </Form.Field>
    </Form>
  );
};

export default NoteForm;
