import React from 'react';
import { NoteFormContainer, NoteList } from './';
import { useSelector } from 'react-redux';
import { project } from '../../states';
import helpers from '../../helpers';
import { MyFace } from '../account-manage';
import { Grid, Segment } from 'semantic-ui-react';

const NoteContainer = ({ subjectId, activityKeyId, projectActivityId }) => {
  const selectedProject = useSelector(project.selectors.getSelectedItem);

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column
          width="sixteen"
          style={{ height: '250px', overflowY: 'auto' }}
        >
          <NoteList
            projectId={selectedProject.key}
            subjectId={subjectId}
            activityKeyId={activityKeyId}
            projectActivityId={projectActivityId}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="sixteen">
          <Segment style={{ backgroundColor: '#FBFAFA' }}>
            <div style={{ marginBottom: '0.5em' }}>
              <MyFace />
              {helpers.Identity.profile.name} {selectedProject.role}
            </div>
            <div>
              <NoteFormContainer
                activityNoteId={0}
                projectId={selectedProject.key}
                subjectId={subjectId}
                activityKeyId={activityKeyId}
                projectActivityId={projectActivityId}
                value={''}
              />
            </div>
          </Segment>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default NoteContainer;
