import helpers from '../../helpers';

const fetchActivityNoteStatus = ({
  subjectId,
  activityKeyId,
  projectActivityId
}) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });

  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/note.status?${params}`
  );
};

const fetchNotes = ({ subjectId, activityKeyId, projectActivityId }) => {
  const params = helpers.qs.stringify({
    projectActivityId
  });
  return helpers.Service.get(
    `api/subjects/${subjectId}/activities/${activityKeyId}/notes?${params}`
  );
};

const addNote = ({ subjectId, activityKeyId, model }) => {
  return helpers.Service.post(
    `api/subjects/${subjectId}/activities/${activityKeyId}/notes`,
    model
  );
};

const updateNote = ({ subjectId, activityKeyId, activityNoteId, model }) => {
  return helpers.Service.put(
    `api/subjects/${subjectId}/activities/${activityKeyId}/notes/${activityNoteId}`,
    model
  );
};

const removeNote = ({ subjectId, activityKeyId, activityNoteId }) => {
  return helpers.Service.delete(
    `api/subjects/${subjectId}/activities/${activityKeyId}/notes/${activityNoteId}`
  );
};

export default {
  fetchActivityNoteStatus,
  fetchNotes,
  addNote,
  updateNote,
  removeNote
};
