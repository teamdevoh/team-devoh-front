import React, { useCallback } from 'react';
import { NoteForm } from './';
import { useNoteEdit } from './hooks';
import { useFormFields, useFormatMessage } from '../../hooks';
import { notification } from '../modal';

const NoteFormContainer = ({
  activityNoteId,
  projectId,
  subjectId,
  activityKeyId,
  projectActivityId,
  cbSubmit,
  value
}) => {
  const t = useFormatMessage();
  const { add, edit, loading } = useNoteEdit({
    subjectId,
    activityKeyId,
    activityNoteId
  });
  const [{ model, onChange }, setModel] = useFormFields({
    subjectId: subjectId,
    activityKeyId: activityKeyId,
    projectActivityId: projectActivityId,
    activityNoteId: activityNoteId,
    note: value
  });

  const handleSubmit = useCallback(
    async () => {
      if (activityNoteId > 0) {
        if (await edit(model)) {
          notification.success({
            title: t('common.alert.changed'),
            onClose: () => {
              if (cbSubmit) {
                cbSubmit();
              }
            }
          });
        }
      } else {
        if (await add(model)) {
          notification.success({
            title: t('common.alert.added'),
            onClose: () => {
              setModel({ ...model, note: value });
            }
          });
        }
      }
    },
    [model]
  );

  return (
    <NoteForm
      loading={loading}
      model={model}
      onChange={onChange}
      onSubmit={handleSubmit}
    />
  );
};

export default NoteFormContainer;
