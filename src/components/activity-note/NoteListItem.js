import React, { useCallback, Fragment } from 'react';
import helpers from '../../helpers';
import { Dropdown, Feed, Image, Icon } from 'semantic-ui-react';
import { useFace, useBoolean, useFormatMessage } from '../../hooks';
import { NoteFormContainer } from './';
import { useNoteEdit } from './hooks';
import { notification, Confirm } from '../modal';
import { format } from '../../constants';

const ContextDeleteItem = ({ subjectId, activityKeyId, activityNoteId }) => {
  const t = useFormatMessage();
  const showRemoveConfirm = useBoolean(false);
  const { remove, loading } = useNoteEdit({
    subjectId,
    activityKeyId,
    activityNoteId
  });

  const handleDeleteClick = useCallback(() => {
    showRemoveConfirm.setTrue();
  }, []);

  const handleOKClick = useCallback(async () => {
    if (await remove()) {
      notification.success({
        title: t('common.alert.deleted'),
        onClose: () => {}
      });
    }
    showRemoveConfirm.setFalse();
  }, []);

  const handleCancelClick = useCallback(() => {
    showRemoveConfirm.setFalse();
  }, []);

  return (
    <Fragment>
      <Dropdown.Item text={t('common.delete')} onClick={handleDeleteClick} />
      <Confirm
        title={t('activity.note.remove.title')}
        content={t('activity.note.remove.description')}
        open={showRemoveConfirm.value}
        onOK={handleOKClick}
        loading={loading}
        onCancel={handleCancelClick}
      />
    </Fragment>
  );
};

const Note = ({ text }) => {
  return (
    <div>
      {text.split('\n').map((singleText, index) => {
        return (
          <span key={index}>
            {singleText}
            <br />
          </span>
        );
      })}
    </div>
  );
};

const NoteListItem = ({
  activityNoteId,
  author,
  myFaceFileStorageId,
  memberId,
  note,
  created,
  team,
  projectId,
  subjectId,
  activityKeyId,
  projectActivityId,
  onDeleteItemClick
}) => {
  const [faceUrl] = useFace(myFaceFileStorageId);
  const showContext = useBoolean(false);
  const t = useFormatMessage();

  const handleClick = useCallback(
    () => {
      helpers.history.push(`/project/${projectId}/member/edit/${memberId}`);
    },
    [memberId]
  );

  const handleEditClick = useCallback(
    () => {
      showContext.setTrue();
    },
    [showContext.value]
  );

  const handleUndoClick = useCallback(
    () => {
      showContext.setFalse();
    },
    [showContext.value]
  );

  return (
    <Feed.Event>
      <Image src={faceUrl} avatar={true} />
      <Feed.Content>
        <Feed.Summary>
          <Feed.User onClick={handleClick}>{author}</Feed.User> {team}
          <Feed.Date>
            {helpers.util.dateformat(created, format.YYYY_MM_DD_HHMM)}
          </Feed.Date>
          {helpers.Identity.profile.id === memberId && (
            <div style={{ float: 'right' }}>
              <Dropdown
                direction="left"
                icon="ellipsis horizontal"
                style={{ opacity: 0.7, zIndex: 2 }}
              >
                <Dropdown.Menu>
                  <Dropdown.Item
                    text={t('common.edit')}
                    onClick={handleEditClick}
                  />
                  <ContextDeleteItem
                    subjectId={subjectId}
                    activityKeyId={activityNoteId}
                    activityNoteId={activityNoteId}
                  />
                </Dropdown.Menu>
              </Dropdown>
            </div>
          )}
        </Feed.Summary>
        <Feed.Extra style={{ width: '100%' }} text>
          <Note text={note} />
          {showContext.value && (
            <Fragment>
              <Icon
                name="undo"
                style={{ cursor: 'pointer' }}
                onClick={handleUndoClick}
              />
              <NoteFormContainer
                activityNoteId={activityNoteId}
                projectId={projectId}
                subjectId={subjectId}
                activityKeyId={activityKeyId}
                projectActivityId={projectActivityId}
                value={note}
                cbSubmit={handleUndoClick}
              />
            </Fragment>
          )}
        </Feed.Extra>
      </Feed.Content>
    </Feed.Event>
  );
};

export default NoteListItem;
