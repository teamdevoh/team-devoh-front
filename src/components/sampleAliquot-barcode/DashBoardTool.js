import React, { Fragment, useState } from 'react';
import { Label, Button } from 'semantic-ui-react';
import { notification } from '../modal';
import { useFormatMessage, useBoolean } from '../../hooks';
import api from './api';
import helpers from '../../helpers';
import { useAliquotBarcodePrint } from './hooks';
import BarcodePrintInfoModal from '../sample-barcode/BarcodePrintInfoModal';
import {
  StyledDetail,
  RightButtonWrap,
  FlexDiv
} from '../sample-barcode/styles';
import { format } from '../../constants';
import { StyledCSVExport } from '../export-tdm/styles';

const DashBoardTool = ({
  projectId,
  items,
  setIsDo,
  selectedIndexes,
  exportCSV
}) => {
  const t = useFormatMessage();
  const showBarcodePrintinfo = useBoolean(false);
  const [printableCount, setPrintableCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [{ loading: printLoading, onPrintBarcode }] = useAliquotBarcodePrint({
    projectId
  });
  const [csvLoading, setCsvLoading] = useState(false);

  const handleCreateSampleIdClick = async () => {
    setLoading(true);
    const creatableItems = items.filter((item, index) => {
      const { barcode, aliquotBarcode } = item;
      return selectedIndexes.includes(index) && barcode && !aliquotBarcode;
    });

    if (creatableItems.length === 0) {
      notification.info({
        title: t('sample.aliquot.barcode.cannot.generate'),
        confirmButtonName: t('common.ok'),
        onClose: () => {}
      });
    } else {
      const model = creatableItems.map(item => {
        const {
          projectId,
          subjectId,
          sampleId,
          projectSampleAliquotId,
          aliquotCount
        } = item;

        return {
          projectId,
          subjectId,
          sampleId,
          projectSampleAliquotId,
          aliquotCount
        };
      });

      const res = await api.addSampleAliquotBarcodes({ projectId, model });

      if (res && res.data) {
        notification.success({
          title: t('project.task.completed'),
          onClose: () => {}
        });
        setIsDo(true);
      }
    }
    setLoading(false);
  };

  const handlePrintBarcodeClick = async () => {
    const printBarcodes = items.filter((item, index) => {
      const { aliquotBarcode } = item;
      return selectedIndexes.includes(index) && aliquotBarcode;
    });

    if (printBarcodes.length === 0) {
      notification.info({
        title: t('aliquot.barcode.none'),
        confirmButtonName: t('common.ok'),
        onClose: () => {}
      });
    } else {
      const model = printBarcodes.map(item => {
        const {
          // irbNo,
          site,
          subjectNo,
          initial,
          visit,
          aliquotAnalyte,
          material,
          sample,
          scheduledDateTime,
          scheduledDate,
          scheduledTime,
          samplingDateTime,
          samplingDate,
          samplingTime,
          aliquotBarcode,
          brthdtc
        } = item;

        return {
          irbNo: site,
          subjectNo,
          initial,
          visitName: visit,
          analyte: aliquotAnalyte,
          material,
          barcodeName: sample,
          scheduledDateTime: samplingDateTime || scheduledDateTime,
          scheduledDate: samplingDate || scheduledDate,
          scheduledTime: samplingTime || scheduledTime,
          actualDateTime: samplingDateTime,
          actualDate: samplingDate,
          actualTime: samplingTime,
          barcode: aliquotBarcode,
          dob: helpers.util.dateformat(brthdtc, format.YYYY_MM_DD)
        };
      });

      const data = await onPrintBarcode(model);

      if (data > 0) {
        showBarcodePrintinfo.setTrue();
        setPrintableCount(data);
      }
    }
  };

  const handleRemoveClick = async () => {
    const removeBarcodes = items.filter((item, index) => {
      const { aliquotBarcode, aliquotDateTime } = item;
      return (
        selectedIndexes.includes(index) && aliquotBarcode && !aliquotDateTime
      );
    });

    if (removeBarcodes.length === 0) {
      notification.info({
        title: t('aliquot.barcode.remove.disabled.info'),
        confirmButtonName: t('common.ok'),
        onClose: () => {}
      });
    } else {
      /*
        {
          sampleId: 1,
          aliquotAnalyte: PL,
          sampleAliquotIds: [1, 2, 3]
        }
      */
      const model = [];
      const josnModel = {};
      for (let i = 0; i < removeBarcodes.length; i++) {
        const {
          sampleId,
          aliquotAnalyte,
          sampleAliquotId,
          projectAliquotCount,
          projectSampleAliquotId
        } = removeBarcodes[i];
        const key = `${sampleId}-${aliquotAnalyte}`;

        if (typeof josnModel[key] === 'undefined') {
          josnModel[key] = {
            sampleId,
            aliquotAnalyte,
            projectSampleAliquotId,
            sampleAliquotIds: [sampleAliquotId],
            projectAliquotCount
          };
        } else {
          josnModel[key].sampleAliquotIds.push(sampleAliquotId);
        }
      }

      for (let key in josnModel) {
        if (josnModel.hasOwnProperty(key)) {
          const {
            sampleId,
            aliquotAnalyte,
            sampleAliquotIds,
            projectAliquotCount,
            projectSampleAliquotId
          } = josnModel[key];
          if (sampleAliquotIds.length === projectAliquotCount) {
            // aliquot id 삭제할때 해당 sample 의 같은 analyte 모두 선택해서 삭제해야함
            model.push({
              sampleId,
              aliquotAnalyte,
              sampleAliquotIds,
              projectSampleAliquotId
            });
          }
        }
      }

      if (model.length === 0) {
        notification.info({
          title: t('aliquot.barcode.remove.info'),
          confirmButtonName: t('common.ok'),
          onClose: () => {}
        });
      } else {
        notification.confirm({
          title: t('delete.message'),
          content: '',
          onOK: async () => {
            const res = await api.removeSampleAliquotBarcodes({
              projectId,
              model
            });

            if (res && res.data) {
              notification.success({
                title: t('project.task.completed'),
                onClose: () => {}
              });

              setIsDo(true);
            }
          }
        });
      }
    }
  };

  const handleExport = async () => {
    setCsvLoading(true);
    try {
      await exportCSV('Aliquot List');
    } catch (err) {}
    setCsvLoading(false);
  };

  return (
    <Fragment>
      <FlexDiv>
        <Label size="big">
          Aliquot Count
          <StyledDetail>{items.length}</StyledDetail>
        </Label>
        <RightButtonWrap>
          <Button
            disabled={selectedIndexes.length === 0}
            onClick={handleCreateSampleIdClick}
            loading={loading || printLoading}
          >
            Create Aliquot ID
          </Button>
          <Button
            disabled={selectedIndexes.length === 0}
            onClick={handlePrintBarcodeClick}
            loading={loading || printLoading}
          >
            Print Barcode
          </Button>
          <Button
            disabled={selectedIndexes.length === 0}
            onClick={handleRemoveClick}
            loading={loading || printLoading}
          >
            Remove Aliquot ID
          </Button>
          <StyledCSVExport
            positive
            icon="download"
            loading={csvLoading}
            onClick={handleExport}
            content="CSV Export"
          />
        </RightButtonWrap>
      </FlexDiv>
      <BarcodePrintInfoModal
        open={showBarcodePrintinfo.value}
        onOK={showBarcodePrintinfo.setFalse}
        title={'Print Aliquot Barcode'}
        printableCount={printableCount}
        targetUrl={`http://oz.econsert.com/ozrviewer/ozreport_c.html?api=SampleManager&action=GetStudyLabelPrintByLocalIP&doc=SampleManagement/AliquotLabel&param=studyID:0`}
      />
    </Fragment>
  );
};

export default DashBoardTool;
