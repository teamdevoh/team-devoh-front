import React from 'react';
import { Select } from 'semantic-ui-react';
import { useProjectSampleAliquotList } from '../sample-setting/hooks';
import { useFormatMessage } from '../../hooks';

export const ProjectSampleAliquots = ({
  name,
  projectId,
  projectSampleId,
  value,
  onChange
}) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useProjectSampleAliquotList({
    projectId,
    projectSampleId
  });

  const options = items.map(item => {
    const { aliquotAnalyte, projectSampleAliquotId } = item;

    return {
      text: aliquotAnalyte,
      value: projectSampleAliquotId
    };
  });

  return (
    <Select
      name={name}
      item
      loading={loading}
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('all'),
          value: 0
        },
        ...options
      ]}
    />
  );
};
