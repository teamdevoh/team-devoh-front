import React from 'react';
import { DataGrid } from '../data-grid';

const ActivityBoard = ({
  columns,
  items,
  loading,
  selectedIndexes,
  onRowsSelected,
  onRowsDeselected
}) => {
  return (
    <DataGrid
      columns={columns}
      rows={items}
      minHeight={550}
      loading={loading}
      rowKey="rowNum"
      rowNumber={{ show: true, key: 'no', name: 'No' }}
      rowSelection={{
        showCheckbox: true,
        enableShiftSelect: true,
        onRowsSelected,
        onRowsDeselected,
        selectBy: {
          indexes: selectedIndexes
        }
      }}
    />
  );
};

export default ActivityBoard;
