import helpers from '../../helpers';

const fetchSampleAliquotBarcodes = ({
  curProjectId,
  projectId,
  projectSiteId,
  subjectIds,
  visitOrder,
  projectSampleId,
  projectsamplealiquotid
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    subjectIds,
    visitOrder,
    projectSampleId,
    projectsamplealiquotid
  });

  return helpers.Service.get(
    `api/projects/${curProjectId}/samples/aliquot/barcodes?${params}`
  );
};

const addSampleAliquotBarcodes = ({ projectId, model }) => {
  return helpers.Service.post(
    `api/project/${projectId}/samples/aliquot/barcodes`,
    model
  );
};

const removeSampleAliquotBarcodes = ({ projectId, model }) => {
  return helpers.Service.delete(
    `api/project/${projectId}/samples/aliquot/barcodes`,
    { data: model }
  );
};

const addSampleAliquotBarcodesPrint = ({ projectId, model }) => {
  return helpers.Service.post(
    `api/project/${projectId}/samples/aliquot/barcodes/print`,
    model
  );
};

export default {
  fetchSampleAliquotBarcodes,
  addSampleAliquotBarcodes,
  removeSampleAliquotBarcodes,
  addSampleAliquotBarcodesPrint
};
