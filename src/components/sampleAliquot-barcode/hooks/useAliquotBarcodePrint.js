import { useState } from 'react';
import api from '../api';

const useAliquotBarcodePrint = ({ projectId }) => {
  const [loading, setLoading] = useState(false);

  const printBarcode = async model => {
    setLoading(true);
    const res = await api.addSampleAliquotBarcodesPrint({
      projectId,
      model
    });

    setLoading(false);
    return res.data;
  };

  return [{ loading, onPrintBarcode: printBarcode }];
};

export default useAliquotBarcodePrint;
