import React from 'react';
import styled from 'styled-components';
import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';

const AlignRightWrap = styled.div`
  text-align: right;
`;

const useSampleAliquotBarcodeList = ({
  curProjectId,
  projectId,
  projectSiteId,
  subjectIds,
  visitOrder,
  projectSampleId,
  projectsamplealiquotid
}) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isDo, setIsDo] = useState();

  const fetchItems = useCallback(async () => {
    setLoading(true);
    const res = await api.fetchSampleAliquotBarcodes({
      curProjectId,
      projectId,
      projectSiteId,
      subjectIds: subjectIds && subjectIds.join(','),
      visitOrder,
      projectSampleId,
      projectsamplealiquotid
    });

    setLoading(false);
    setIsDo(false);
    if (res && res.data) {
      return res.data;
    } else {
      return [];
    }
  });

  useEffect(
    () => {
      if (!!projectId) {
        // project 기본 선택되어 있어야함

        const fetch = async () => {
          const item = await fetchItems();

          setItems(item);
        };
        fetch();
      }
    },
    [
      curProjectId,
      projectId,
      projectSiteId,
      subjectIds,
      visitOrder,
      projectSampleId,
      projectsamplealiquotid
    ]
  );

  useEffect(
    () => {
      if (isDo) {
        const fetch = async () => {
          const item = await fetchItems();

          setItems(item);
        };
        fetch();
      }
    },
    [isDo]
  );

  const columns = [
    {
      key: 'site',
      name: 'Site'
    },
    {
      key: 'subjectNo',
      name: 'Subject',
      width: 130,
      formatter: ({ value, row }) => {
        const { initial } = row;
        return value + (!!initial ? `/${initial}` : '');
      }
    },
    {
      key: 'visit',
      name: 'Visit',
      width: 130
    },
    {
      key: 'sample',
      name: 'Sample',
      width: 140
    },
    {
      key: 'barcode',
      name: 'Sample ID',
      width: 100
    },
    {
      key: 'samplingDateTime',
      name: 'Sampling',
      width: 150
    },
    {
      key: 'aliquotAnalyte',
      name: 'Analyte',
      width: 160
    },
    {
      key: 'aliquotCount',
      name: 'Count',
      width: 70
    },
    {
      key: 'aliquotBarcode',
      name: 'Aliquot ID',
      width: 100
    },
    {
      key: 'aliquotVolume',
      name: 'Volume',
      formatter: ({ value, row }) => {
        const { unit } = row;
        return <AlignRightWrap>{`${value} ${unit}`}</AlignRightWrap>;
      },
      width: 100
    },
    {
      key: 'memberName',
      name: 'User',
      width: 150
    }
  ];

  const exportCSV = useCallback(
    async fileName => {
      const items = await fetchItems();
      if (items) {
        items.forEach(item => {
          const { aliquotVolume, unit, memberName } = item;
          item.aliquotVolume = `${aliquotVolume} ${unit}`;
          item.memberName = !!memberName ? memberName.replaceAll(',', ' ') : '';
        });

        const fields = columns.map(item => ({ key: item.key }));
        const fieldNames = columns.map(item => item.name);

        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          data: items,
          fields,
          fieldNames
        });

        return items;
      }
      return [];
    },
    [
      curProjectId,
      projectId,
      projectSiteId,
      subjectIds,
      visitOrder,
      projectSampleId,
      projectsamplealiquotid
    ]
  );

  return [{ items, loading, isDo, columns, exportCSV }, { setIsDo }];
};

export default useSampleAliquotBarcodeList;
