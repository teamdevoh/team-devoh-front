import React, { useState, useEffect, useCallback } from 'react';
import Board from './Board';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useSampleAliquotBarcodeList } from './hooks';

const DashBoard = () => {
  const { projectId } = useParams();
  const [filter, setFilter] = useState({
    projectId: null,
    siteId: 0,
    projectSiteId: 0,
    subjectIds: null,
    visitOrder: 0,
    projectSampleId: 0,
    projectsamplealiquotid: 0
  });

  const [
    { items, loading, isDo, columns, exportCSV },
    { setIsDo }
  ] = useSampleAliquotBarcodeList({
    curProjectId: projectId,
    ...filter
  });

  const [selectedIndexes, setSelectedIndexes] = useState([]);

  useEffect(
    () => {
      if (isDo) {
        setSelectedIndexes([]);
      }
    },
    [isDo]
  );

  const handleFilterChange = useCallback(
    (event, { name, value }) => {
      let newFilter = { ...filter, [name]: value };

      if (name === 'projectId') {
        newFilter = {
          ...newFilter,
          projectSiteId: 0,
          subjectIds: null,
          projectSampleId: 0,
          projectsamplealiquotid: 0
        };
      }

      if (name === 'projectSiteId') {
        newFilter = {
          ...newFilter,
          subjectIds: null
        };
      }

      if (name === 'projectSampleId') {
        newFilter = {
          ...newFilter,
          projectsamplealiquotid: 0
        };
      }

      setFilter({ ...newFilter });
    },
    [filter]
  );

  const handleRowsSelected = rows => {
    setSelectedIndexes(selectedIndexes.concat(rows.map(r => r.rowIdx)));
  };

  const handleRowsDeselected = rows => {
    let rowIndexes = rows.map(r => r.rowIdx);
    setSelectedIndexes(
      selectedIndexes.filter(i => rowIndexes.indexOf(i) === -1)
    );
  };

  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onFilterChange={handleFilterChange}
            filter={filter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool
            projectId={projectId}
            items={items}
            setIsDo={setIsDo}
            loading={loading}
            selectedIndexes={selectedIndexes}
            exportCSV={exportCSV}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Board
            columns={columns}
            items={items}
            loading={loading}
            onRowsSelected={handleRowsSelected}
            onRowsDeselected={handleRowsDeselected}
            selectedIndexes={selectedIndexes}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
