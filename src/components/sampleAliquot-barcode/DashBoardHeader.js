import React, { Fragment } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { ProjectsOfSamples, ProjectSite } from '../sample-status/Selector';
import { Subjects } from '../collection/Selector';
import { StyledForm } from '../sample-status/styles';
import { TdmVisitOrder, ProjectSamples } from '../sample-barcode/Selector';
import { ProjectSampleAliquots } from './Selector';

const DashBoardHeader = ({ projectId, filter, onFilterChange }) => {
  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <StyledForm>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Project</label>
                <ProjectsOfSamples
                  name="projectId"
                  value={filter.projectId}
                  onChange={onFilterChange}
                  defaultFirstValue
                />
              </Form.Field>
              <Form.Field>
                <label>Site</label>
                <ProjectSite
                  projectId={filter.projectId}
                  name="projectSiteId"
                  value={filter.projectSiteId}
                  onChange={onFilterChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Subject</label>
                <Subjects
                  projectSiteId={filter.projectSiteId}
                  name="subjectIds"
                  value={!!!filter.subjectIds ? [] : filter.subjectIds}
                  multiple
                  onChange={onFilterChange}
                  reset={filter.projectSiteId === 0}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Visit</label>
                <TdmVisitOrder
                  name="visitOrder"
                  value={filter.visitOrder}
                  onChange={onFilterChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Sample</label>
                <ProjectSamples
                  name="projectSampleId"
                  value={filter.projectSampleId}
                  onChange={onFilterChange}
                  projectId={filter.projectId}
                />
              </Form.Field>
              <Form.Field>
                <label>Analyte</label>
                <ProjectSampleAliquots
                  name="projectsamplealiquotid"
                  value={filter.projectsamplealiquotid}
                  onChange={onFilterChange}
                  projectId={filter.projectId}
                  projectSampleId={filter.projectSampleId}
                />
              </Form.Field>
            </Form.Group>
          </StyledForm>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
