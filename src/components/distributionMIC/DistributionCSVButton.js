import React, { useCallback } from 'react';
import { Button } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { useMICSubject } from './hooks';
import { role } from '../../constants';
import { RoleAware } from '../auth';

const DistributionCSVButton = ({ exDrugId, items }) => {
  const t = useFormatMessage();
  const micSubject = useMICSubject({ exDrugId, items });

  const handleClick = useCallback(
    () => {
      micSubject.exec();
    },
    [exDrugId, items]
  );

  return (
    <RoleAware allowedRole={role.MICDistribution_Excel_Read}>
      <Button
        positive
        icon="download"
        content={t('common.export')}
        loading={micSubject.loading}
        disabled={micSubject.loading}
        onClick={handleClick}
      />
    </RoleAware>
  );
};

export default DistributionCSVButton;
