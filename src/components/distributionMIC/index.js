import { asyncComponent } from '../modules';

export const MICDisContainer = asyncComponent(() => import('./MICDisContainer'));
export const CasesContainer = asyncComponent(() => import('./CasesContainer'));
export const CasesList = asyncComponent(() => import('./CasesList'));
export const DistributionContainer = asyncComponent(() => import('./DistributionContainer'));
export const DistributionList = asyncComponent(() => import('./DistributionList'));
export const DistributionAccordion = asyncComponent(() => import('./DistributionAccordion'));
export const DistributionChart = asyncComponent(() => import('./DistributionChart'));
export const DistributionCSVButton = asyncComponent(() => import('./DistributionCSVButton'));