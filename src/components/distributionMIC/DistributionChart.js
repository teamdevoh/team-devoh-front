import React, { useEffect, useState } from 'react';
import Chart from 'react-apexcharts';

const DistributionChart = ({
  diagnoName,
  items = [],
  testmicKeys = [],
  cc
}) => {
  const [series, setSeries] = useState([]);

  useEffect(
    () => {
      let data = [];

      items.forEach(item => {
        let testmicCountSet = [];
        testmicKeys.forEach(testmic => {
          const count = item[testmic];

          testmicCountSet.push(count === void 0 ? 0 : count);
        });

        data.push({
          name: item.diagnoName,
          data: testmicCountSet
        });
      });

      setSeries(data);
    },
    [items]
  );

  const options = {
    annotations: {
      xaxis: [
        {
          x: cc,
          borderColor: '#33B2FF',
          label: {
            borderColor: '#black',
            style: {
              color: '#fff',
              background: '#black'
            },
            orientation: 'vertical',
            text: 'Critical concentration for MTB'
          }
        }
      ]
    },
    chart: {
      type: 'bar',
      height: 350,
      animations: {
        enabled: false
      }
    },
    colors: [
      '#F3B415',
      '#F27036',
      '#663F59',
      '#6A6E94',
      '#4E88B4',
      '#00A7C6',
      '#18D8D8',
      '#A9D794',
      '#46AF78',
      '#A93F55',
      '#8C5E58',
      '#2176FF',
      '#33A1FD',
      '#7A918D',
      '#BAFF29'
    ],
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '55%',
        endingShape: 'rounded'
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    xaxis: {
      categories: _.orderBy([...testmicKeys, cc === '0' ? '' : cc], value =>
        // eslint-disable-next-line no-useless-escape
        parseFloat(value.match(/[\d\.]+/))
      )
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    title: {
      text: diagnoName
    },
    fill: {
      opacity: 1
    }
  };

  return <Chart options={options} series={series} type="bar" height={350} />;
};

export default DistributionChart;
