import { useEffect, useState } from 'react';

const useDistributionList = ({ exDrugId, micItems }) => {
  const [items, setItems] = useState([]);
  const [headerKeys, setHeaderKeys] = useState([]);

  const fetchItems = async () => {
    let data = [];
    let collectingKeys = [];
    let characterKeys = [];

    micItems.forEach(item => {
      let distItem = { diagnoName: item.diagnoName };

      const drug = _.find(item.drugs, { exDrugId: exDrugId });

      if (drug) {
        const testmics = drug.testmics;
        const groups = _.groupBy(testmics);
        Object.keys(groups).forEach(groupKey => {
          distItem = { ...distItem, [groupKey]: groups[groupKey].length };
          // eslint-disable-next-line no-useless-escape
          const headerKey = parseFloat(groupKey.match(/[\d\.]+/));
          if (headerKey) {
            collectingKeys = [
              ...collectingKeys,
              { key: headerKey, name: groupKey }
            ];
          } else {
            characterKeys = [
              ...characterKeys,
              { key: groupKey, name: groupKey }
            ];
          }
        });

        data.push(distItem);
      }
    });

    const headers = [];
    let alltestMIC = [...collectingKeys, ...characterKeys];
    alltestMIC = _.toArray(_.orderBy(alltestMIC, 'key'));

    const duplicateCheck = {};

    alltestMIC.forEach(testMIC => {
      if (
        testMIC.name !== '' &&
        typeof duplicateCheck[testMIC.name] === 'undefined'
      ) {
        duplicateCheck[testMIC.name] = true;
        headers.push(testMIC.name);
      }
    });

    setHeaderKeys(headers);
    setItems(data);
  };

  useEffect(
    () => {
      if (micItems.length > 0) {
        fetchItems();
      } else {
        setHeaderKeys([]);
        setItems([]);
      }
    },
    [exDrugId, micItems]
  );

  return { items, testmicKeys: headerKeys };
};

export default useDistributionList;
