import { useEffect, useState } from 'react';

const useCases = props => {
  const { micItems } = props;
  const [loading, setLoading] = useState(props.loading);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    try {
      setLoading(true);
      let crossItems = [];
      let totalItem = { diagnoName: 'Total', totalCase: 0 };

      micItems.forEach(item => {
        let crossItem = {
          diagnoName: item.diagnoName,
          totalCase: item.totalCase
        };
        totalItem['totalCase'] += item.totalCase;

        item.drugs.forEach(drug => {
          crossItem[drug.genericName] = drug.count;
          const totalDrug = totalItem[drug.genericName];
          totalItem[drug.genericName] = totalDrug
            ? totalDrug + drug.count
            : drug.count;
        });

        crossItems.push(crossItem);
      });

      crossItems.push(totalItem);

      setItems(crossItems);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      if (micItems.length > 0) {
        fetchItems();
      } else {
        setItems([]);
      }
    },
    [micItems]
  );

  useEffect(
    () => {
      setLoading(props.loading);
    },
    [props.loading]
  );

  return { items, loading };
};

export default useCases;
