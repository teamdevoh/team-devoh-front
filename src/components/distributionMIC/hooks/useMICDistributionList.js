import { useEffect, useState } from 'react';
import api from '../api';

const useMICDistributionList = projectSiteId => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [micDrugCC, setMicDrugCC] = useState([]);

  const fetchItems = async () => {
    setLoading(true);
    try {
      const res = await api.fetchMICDistribution(projectSiteId);
      if (res && res.data) {
        setMicDrugCC(res.data.micDrugCC);

        setItems(
          _.filter(res.data.micDrugMetaWithDiagnosis, item => {
            return (
              item.totalCase > 0 && item.diagnoName !== '잠복결핵 (Latent TB)'
            );
          })
        );
      }
    } catch (err) {}
    setLoading(false);
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectSiteId]
  );

  return { items, micDrugCC, loading };
};

export default useMICDistributionList;
