import { useCallback } from 'react';
import helpers from '../../../helpers';
import { useFormatMessage, useBoolean } from '../../../hooks';

const useMICSubject = ({ exDrugId, items }) => {
  const t = useFormatMessage();
  const loading = useBoolean(false);

  const exec = useCallback(
    () => {
      loading.setTrue();

      let data = [];
      let drugName = '';

      items.forEach(item => {
        const drug = _.find(item.drugs, { exDrugId });
        drugName = drug.genericName;

        drug.subjectNos.forEach((subjectNo, index) => {
          data.push({
            subjectNo,
            diagnoName: item.diagnoName,
            drugName: drugName,
            testmic: drug.testmics[index]
          });
        });
      });

      const orderedData = _.orderBy(data, 'subjectNo', 'asc');

      helpers.util.exportCSV({
        fileName: `MIC Distribution ${drugName}.csv`,
        data: orderedData,
        fields: [
          { key: 'subjectNo' },
          { key: 'diagnoName' },
          { key: 'drugName' },
          { key: 'testmic' }
        ],
        fieldNames: [
          t('collection.subject.no'),
          t('activity.diagnosis.name'),
          t('activity.micresult.exdrugId'),
          t('activity.micresult.testmic')
        ]
      });
      loading.setFalse();
    },
    [exDrugId, items]
  );

  return { exec, loading: loading.value };
};

export default useMICSubject;
