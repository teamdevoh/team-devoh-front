import { useEffect, useState } from 'react';

const useMICDrugs = props => {
  const { micItems = [] } = props;
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(props.loading);

  useEffect(
    () => {
      if (micItems.length > 0) {
        let data = [];

        micItems.forEach(item => {
          item.drugs.forEach(drug => {
            const foundItem = _.find(data, { key: drug.exDrugId });
            if (foundItem) {
              foundItem.count += drug.count;
            } else {
              data.push({
                key: drug.exDrugId,
                name: drug.genericName,
                count: drug.count
              });
            }
          });
        });

        data = data.sort((a, b) => b.count - a.count);

        setItems(data);
      } else {
        setItems([]);
      }
    },
    [micItems]
  );

  useEffect(
    () => {
      setLoading(props.loading);
    },
    [props.loading]
  );

  return { items, loading };
};

export default useMICDrugs;
