import React, { useEffect, useState } from 'react';
import { DataGrid } from '../data-grid';

const DistributionList = ({
  diagnoName,
  items,
  testmicKeys,
  onCheckedDiagnosis
}) => {
  const [columns, setColumns] = useState([]);
  const [selectedIndexes, setSelectedIndexes] = useState([]);

  useEffect(
    () => {
      let cols = [
        { key: 'diagnoName', name: diagnoName, width: 200, frozen: true }
      ];

      testmicKeys.forEach(testmicKey => {
        cols.push({ key: testmicKey, name: testmicKey });
      });

      setColumns(cols);

      items.forEach((item, i) => {
        selectedIndexes.push(i);
      });
      setSelectedIndexes(selectedIndexes);
    },
    [items]
  );

  const onRowsSelected = rows => {
    let selectedRowIndexs = [];
    selectedRowIndexs = selectedIndexes.concat(rows.map(r => r.rowIdx));

    let names = [];
    items.forEach((item, index) => {
      if (selectedRowIndexs.indexOf(index) > -1) {
        names.push(item.diagnoName);
      }
    });

    setSelectedIndexes(selectedRowIndexs);
    onCheckedDiagnosis(names);
  };

  const onRowsDeselected = rows => {
    let rowIndexes = rows.map(r => r.rowIdx);
    let selectIndexes = selectedIndexes.filter(
      i => rowIndexes.indexOf(i) === -1
    );

    setSelectedIndexes(selectIndexes);

    let names = [];
    items.forEach((item, index) => {
      if (selectIndexes.indexOf(index) > -1) {
        names.push(item.diagnoName);
      }
    });

    onCheckedDiagnosis(names);
  };

  return (
    <DataGrid
      rowKey="diagnoName"
      minHeight={280}
      columns={columns}
      rows={items}
      loading={false}
      rowNumber={{ show: false, key: 'no', name: 'No' }}
      rowSelection={{
        showCheckbox: true,
        enableShiftSelect: true,
        onRowsSelected: onRowsSelected,
        onRowsDeselected: onRowsDeselected,
        selectBy: {
          indexes: selectedIndexes
        }
      }}
    />
  );
};

export default DistributionList;
