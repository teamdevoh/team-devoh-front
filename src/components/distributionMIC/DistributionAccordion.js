import React, { Fragment, useCallback, useState } from 'react';
import { useEffect } from 'react';
import { Accordion, Grid, Icon, Loader } from 'semantic-ui-react';
import { RootWrap } from '../modules/EmptyRowsViewForSemantic';
import { StyledDimmer } from '../modules/styles';
import { DistributionList, DistributionChart, DistributionCSVButton } from './';
import { useDistributionList, useMICDrugs } from './hooks';

const DistributionAccordion = ({ items, micDrugCC, loading }) => {
  const [activeIndex, setActiveIndex] = useState(-1);
  const [selectedExDrugId, setSelectedExDrugId] = useState(0);
  const [cc, setCC] = useState('0');
  const drug = useMICDrugs({ micItems: items, loading });

  const dist = useDistributionList({
    exDrugId: selectedExDrugId,
    micItems: items
  });
  const [selectedDianosisNames, setSelectedDianosisNames] = useState([]);

  const handleClick = useCallback(
    (event, data) => {
      const selectedDrug = drug.items[data.index];
      setSelectedExDrugId(selectedDrug.key);
      setActiveIndex(data.active ? -1 : data.index);
      const foundCC = _.find(micDrugCC, { tbdrugId: selectedDrug.key });
      setCC(foundCC ? foundCC.cc : '0');
    },
    [drug.items]
  );

  useEffect(
    () => {
      let names = [];
      items.forEach(item => {
        names.push(item.diagnoName);
      });

      setSelectedDianosisNames(names);
    },
    [selectedExDrugId, items]
  );

  const handleDiagnosisClick = useCallback(
    names => {
      setSelectedDianosisNames(names);
    },
    [selectedDianosisNames]
  );

  return (
    <Fragment>
      <Accordion>
        {drug.items.map((item, index) => {
          return (
            <Fragment key={item.key}>
              <Accordion.Title
                active={activeIndex === index}
                index={index}
                onClick={handleClick}
              >
                <Icon name="dropdown" />
                {item.name} <span style={{ color: 'red' }}>{item.count}</span>{' '}
                cases
              </Accordion.Title>
              <Accordion.Content active={activeIndex === index}>
                {activeIndex === index && (
                  <Grid stackable>
                    <Grid.Column width="sixteen">
                      <DistributionCSVButton
                        exDrugId={item.key}
                        items={items}
                      />
                    </Grid.Column>
                    <Grid.Column width="sixteen">
                      <DistributionList
                        diagnoName={item.name}
                        {...dist}
                        selectedDianosisNames={selectedDianosisNames}
                        onCheckedDiagnosis={handleDiagnosisClick}
                      />
                    </Grid.Column>
                    <Grid.Column width="sixteen">
                      <DistributionChart
                        diagnoName={item.name}
                        items={dist.items.filter(
                          item =>
                            selectedDianosisNames.indexOf(item.diagnoName) > -1
                        )}
                        testmicKeys={dist.testmicKeys}
                        cc={cc}
                      />
                    </Grid.Column>
                  </Grid>
                )}
              </Accordion.Content>
            </Fragment>
          );
        })}
      </Accordion>
      {drug.items.length === 0 && (
        <RootWrap>
          {drug.loading ? (
            <StyledDimmer active inverted>
              <Loader size="big">Loading</Loader>
            </StyledDimmer>
          ) : (
            <Fragment>
              <Icon name="window close outline" size="big" />
              <h3>{'No Data'}</h3>
            </Fragment>
          )}
        </RootWrap>
      )}
    </Fragment>
  );
};

export default DistributionAccordion;
