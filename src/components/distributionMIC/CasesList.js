import React, { useEffect, useState } from 'react';
import { DataGrid } from '../data-grid';

const CasesList = ({ items, loading }) => {
  const [columns, setColumns] = useState([]);

  useEffect(
    () => {
      if (items.length > 0) {
        const colKeys = Object.keys(items[0]);
        let cols = [];

        colKeys.forEach(colKey => {
          let name = '';
          let width = null;
          let color = null;
          let frozen = false;

          if (colKey === 'diagnoName') {
            name = 'TB Classification';
            width = 200;
            color = 'green';
            frozen = true;
          } else if (colKey === 'totalCase') {
            name = 'Total Cases';
            width = 80;
            color = 'red';
            frozen = true;
          } else {
            name = colKey;
          }
          const col = {
            key: colKey,
            name: name,
            width: width,
            frozen: frozen,
            headerRenderer: header => (
              <div style={{ color: color }}>{header.column.name}</div>
            )
          };
          cols.push(col);
        });

        setColumns(cols);
      }
    },
    [items]
  );

  return (
    <DataGrid
      rowKey="diagnoName"
      minHeight={600}
      columns={columns}
      rows={items}
      loading={loading}
      rowNumber={{ show: false, key: 'no', name: 'No' }}
    />
  );
};

export default CasesList;
