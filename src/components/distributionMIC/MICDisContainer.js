import React, { useCallback, useState } from 'react';
import { useParams } from 'react-router-dom';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import { Dropdown, Grid, Header, Tab } from 'semantic-ui-react';
import { CasesContainer, DistributionContainer } from './';
import { useFormatMessage } from '../../hooks';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';
import { useMICDistributionList } from './hooks';

const MICDisContainer = () => {
  const { projectId } = useParams();
  const t = useFormatMessage();
  const [myProjectSites] = useMyProjectSiteList({ projectId });
  const [selectedProjectSiteId, setSelectedProjectSiteId] = useState(0);
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const mic = useMICDistributionList(selectedProjectSiteId);

  const handleProjectSiteChange = useCallback((event, data) => {
    setSelectedProjectSiteId(data.value);
  }, []);

  const handleSelectedTabChange = useCallback((event, data) => {
    setSelectedTabIndex(data.activeIndex);
  }, []);

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column>
          <Header as="h3" dividing>
            MIC Distribution
            <Header.Subheader>MIC Distribution</Header.Subheader>
            <CopyRedirectUrl />
          </Header>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width="sixteen">
          <Dropdown
            fluid
            options={[{ text: 'All', value: 0 }, ...myProjectSites]}
            search
            selection
            value={selectedProjectSiteId}
            onChange={handleProjectSiteChange}
            placeholder={t('common.all')}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Tab
            menu={{ secondary: true, pointing: true, borderless: true }}
            activeIndex={selectedTabIndex}
            onTabChange={handleSelectedTabChange}
            panes={[
              {
                menuItem: 'Cases',
                render: () => (
                  <Tab.Pane attached={false} basic>
                    <CasesContainer
                      projectSiteId={selectedProjectSiteId}
                      items={mic.items}
                      loading={mic.loading}
                    />
                  </Tab.Pane>
                )
              },
              {
                menuItem: 'Distribution',
                render: () => (
                  <Tab.Pane attached={false} basic>
                    <DistributionContainer
                      projectSiteId={selectedProjectSiteId}
                      items={mic.items}
                      micDrugCC={mic.micDrugCC}
                      loading={mic.loading}
                    />
                  </Tab.Pane>
                )
              }
            ]}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default MICDisContainer;
