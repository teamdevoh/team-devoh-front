import React from 'react';
import { DistributionAccordion } from './';

const DistributionContainer = ({ items, micDrugCC, loading = false }) => {
  return (
    <DistributionAccordion
      items={items}
      micDrugCC={micDrugCC}
      loading={loading}
    />
  );
};

export default DistributionContainer;
