import helpers from '../../helpers';

const fetchMICDistribution = projectSiteId => {
  return helpers.Service.get(
    `api/projectsites/${projectSiteId}/subjects/dist.mic`
  );
};

export default {
  fetchMICDistribution
};
