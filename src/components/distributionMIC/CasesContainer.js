import React from 'react';
import { CasesList } from './';
import { useCases } from './hooks';

const CasesContainer = ({ items, loading = false }) => {
  const cases = useCases({ micItems: items, loading });

  return <CasesList items={cases.items} loading={cases.loading} />;
};

export default CasesContainer;
