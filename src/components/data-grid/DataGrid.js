import React, { useCallback, useRef, useState, Fragment } from 'react';
import './datagrid.css';
import ReactDataGrid from 'react-data-grid';
import { EmptyRowsView } from '../modules';
import helpers from '../../helpers';
import styled from 'styled-components';

const RowActiveBackGround = styled.div`
  & .react-grid-Cell {
    background-color: yellow !important;
  }
`;

const DataGrid = ({
  columns = [],
  headerRowHeight,
  onRowVisibleStart = scrollState => {},
  onRowVisibleEnd = scrollState => {},
  loading,
  minHeight,
  onGridSorted,
  onRowClick,
  onRowDoubleClick,
  rowGetter,
  rowHeight,
  rowKey,
  rowNumber = {
    show: false,
    key: 'no',
    name: 'No',
    width: 50
  },
  rowRenderer,
  rows = [],
  rowSelection,
  selectRowKey,
  emptyMessage = ''
}) => {
  const gridProps = {
    columns,
    headerRowHeight,
    onRowVisibleStart,
    onRowVisibleEnd,
    loading,
    minHeight,
    onGridSorted,
    onRowClick,
    onRowDoubleClick,
    rowGetter,
    rowHeight,
    rowKey,
    rowNumber,
    rowRenderer,
    rows,
    rowSelection,
    selectRowKey,
    emptyMessage
  };
  const util = helpers.util;
  const refGrid = useRef();
  const [activeRowKey, setActiveRowKey] = useState(gridProps.selectRowKey);

  // Unique key property name of items
  const rowKeyProperty = rowKey;

  const defaultColumnProperties = {
    resizable: true,
    sortable: gridProps.onGridSorted ? true : false
  };

  const handleRowClick = useCallback(
    (rowIdx, row) => {
      if (rowIdx > -1 && rowKeyProperty) {
        const rowKey = row[rowKeyProperty];
        setActiveRowKey(rowKey);

        if (gridProps.onRowClick) {
          gridProps.onRowClick(rowIdx, row);
        }
      }
    },
    [gridProps]
  );

  const Wrapper = ({ children, rowkey }) => {
    if (rowkey === activeRowKey) {
      return <RowActiveBackGround>{children}</RowActiveBackGround>;
    }

    return <div>{children}</div>;
  };

  const RowRenderer = useCallback(
    renderer => {
      const { renderBaseRow, ...props } = renderer;

      if (gridProps.rowRenderer) {
        return (
          <Wrapper rowkey={props.row[rowKeyProperty]}>
            {gridProps.rowRenderer({ renderBaseRow, ...props })}
          </Wrapper>
        );
      } else {
        return (
          <Wrapper rowkey={props.row[rowKeyProperty]}>
            {renderBaseRow(props)}
          </Wrapper>
        );
      }
    },
    [activeRowKey]
  );

  const baseRowGetter = rowIdx => {
    const currentRow = {
      ...rows[rowIdx],
      [gridProps.rowNumber.key]: rowIdx + 1
    };

    if (gridProps.rowGetter) {
      let row = gridProps.rowGetter(rowIdx);
      return { ...currentRow, ...row };
    }

    return currentRow;
  };

  const handleGridSort = (sortColumn, sortDirection) => {
    const reversed = util.sort(gridProps.rows, sortColumn, sortDirection);
    if (gridProps.onGridSorted) {
      gridProps.onGridSorted(reversed);
    } else {
      throw new Error('onGridSorted is not defined');
    }
  };

  const cols = gridProps.columns.map(c => ({
    ...c,
    ...defaultColumnProperties
  }));

  const isStartVisible = state => {
    return (
      state.rowOverscanStartIdx === state.rowVisibleStartIdx &&
      state.scrollDirection === 'upwards'
    );
  };
  const isEndVisible = state => {
    return (
      state.rowOverscanEndIdx === state.rowVisibleEndIdx &&
      state.scrollDirection === 'downwards'
    );
  };

  const handleOnScroll = state => {
    if (isStartVisible(state)) {
      gridProps.onRowVisibleStart(state);
    }

    if (isEndVisible(state)) {
      gridProps.onRowVisibleEnd(state);
    }
  };

  return (
    <Fragment>
      {/* {gridProps.loading && (
        <Root>
          <StyledSegment>
            <Dimmer active inverted>
              <Loader size="big">Loading</Loader>
            </Dimmer>
          </StyledSegment>
        </Root>
      )} */}
      <ReactDataGrid
        ref={refGrid}
        {...gridProps}
        columns={
          gridProps.rowNumber.show
            ? [
                {
                  key: gridProps.rowNumber.key,
                  name: gridProps.rowNumber.name,
                  width: gridProps.rowNumber.width || 50,
                  sortable: false
                }
              ].concat(cols)
            : cols
        }
        onRowClick={handleRowClick}
        rowRenderer={RowRenderer}
        rowGetter={baseRowGetter}
        rowsCount={gridProps.rows.length}
        emptyRowsView={() => (
          <EmptyRowsView
            loading={gridProps.loading}
            message={gridProps.emptyMessage}
          />
        )}
        onGridSort={handleGridSort}
        onScroll={handleOnScroll}
      />
    </Fragment>
  );
};

export default DataGrid;
