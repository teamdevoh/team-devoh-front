import React from 'react';
import { Select } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import { useCodes, useFormatMessage } from '../../hooks';

export const FreezerLocation = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const [items] = useCodes({ codeGroupId: codeGroup.FreezerLocationCode });

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('all'),
          value: 0
        },
        ...items
      ]}
    />
  );
};
