import helpers from '../../helpers';

const fetchSampleBoxStatus = ({
  projectId,
  projectSiteId,
  boxTypeCodeId,
  freezerMachineCodeId,
  freezerLocationCodeId,
  searchKeyword
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    boxTypeCodeId,
    freezerMachineCodeId,
    freezerLocationCodeId,
    searchKeyword
  });

  return helpers.Service.get(`api/samples/boxes.status?${params}`);
};

export default {
  fetchSampleBoxStatus
};
