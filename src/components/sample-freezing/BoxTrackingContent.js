import React from 'react';
import { Container, Form, Divider, Label } from 'semantic-ui-react';
import { StyledForm } from '../sample-status/styles';
import { DataGrid } from '../data-grid';
import { useFormatMessage } from '../../hooks';
import { FlexDiv } from '../sample-barcode/styles';
import { useSampleBoxTrackingList } from '../sample-status/hooks';
import { useSampleBoxAliquotList } from '../sample-boxes/hooks';
import helpers from '../../helpers';
import { format } from '../../constants';

const BoxTrackingContent = ({ info }) => {
  const t = useFormatMessage();
  const {
    aliquotAnalyte,
    boxBarcode,
    boxNote,
    project,
    sampleMaterial,
    site,
    visit,
    projectId,
    sampleBoxId
  } = info;

  const [{ items, loading }] = useSampleBoxTrackingList({
    projectId,
    sampleBoxId
  });

  const [
    { items: aliquotItems, loading: aliquotItemsLoading }
  ] = useSampleBoxAliquotList({ sampleBoxId });

  const columns = [
    {
      key: 'boxBarcode',
      name: 'Box ID'
    },
    {
      key: 'status',
      name: 'Status'
    },
    {
      key: 'freezer',
      name: 'Freezer'
    },
    { key: 'location', name: 'Location' },
    {
      key: 'dateTime',
      name: t('common.date')
    },
    {
      key: 'memberName',
      name: 'User'
    },
    {
      key: 'freezerOutReason',
      name: 'Out Reason'
    }
  ];

  const aliquotColumns = [
    {
      key: 'subject',
      name: 'Subject',
      width: 100
    },
    {
      key: 'aliquotBarcode',
      name: 'Ailquot ID',
      width: 100
    },
    {
      key: 'sample',
      name: 'Sample'
    },
    {
      key: 'material',
      name: 'Material',
      width: 80
    },
    {
      key: 'aliquotAnalyte',
      name: 'Analyte',
      width: 80
    },
    {
      key: 'aliquotVolume',
      name: 'Volume',
      formatter: ({ value, row }) => {
        const { unit } = row;

        return value ? `${value} ${unit}` : '';
      },
      width: 100
    },
    {
      key: 'boxingDateTime',
      name: 'Boxing',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      },
      width: 140
    },
    {
      key: 'memberName',
      name: 'User',
      width: 150
    }
  ];

  return (
    <Container>
      <StyledForm>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Project</label>
            {project}
          </Form.Field>
          <Form.Field>
            <label>Site</label>
            {site}
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Material</label>
            {sampleMaterial}
          </Form.Field>
          <Form.Field>
            <label>Aliquot</label>
            {aliquotAnalyte}
          </Form.Field>
          <Form.Field>
            <label>Visit</label>
            {visit}
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Box Id</label>
            {boxBarcode}
          </Form.Field>
          <Form.Field>
            <label>{t('project.sitenote')}</label>
            {boxNote}
          </Form.Field>
          <Form.Field />
        </Form.Group>
      </StyledForm>
      <Divider />
      <FlexDiv>
        <Label size="big">Box List</Label>
      </FlexDiv>
      <DataGrid
        columns={columns}
        rows={items}
        minHeight={350}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        loading={loading}
        rowKey="rowIndex"
      />
      <FlexDiv style={{ marginTop: '20px' }}>
        <Label size="big">Aliquot List</Label>
      </FlexDiv>
      <DataGrid
        columns={aliquotColumns}
        rows={aliquotItems}
        minHeight={350}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        loading={aliquotItemsLoading}
        rowKey="sampleBoxAliquotId"
      />
    </Container>
  );
};

export default BoxTrackingContent;
