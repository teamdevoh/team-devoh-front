import React, { useState, useCallback } from 'react';
import Board from './Board';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useSampleBoxStatusList } from './hooks';

const DashBoard = () => {
  const [filter, setFilter] = useState({
    projectId: 0,
    projectSiteId: 0,
    boxTypeCodeId: 0,
    freezerMachineCodeId: 0,
    freezerLocationCodeId: 0,
    searchKeyword: ''
  });

  const [{ items, loading, exportCSV }] = useSampleBoxStatusList({
    ...filter
  });

  const handleFilterChange = useCallback(
    (event, { name, value }) => {
      let newFilter = { ...filter, [name]: value };

      if (name === 'projectId') {
        newFilter = {
          ...newFilter,
          projectSiteId: 0
        };
      }

      setFilter({ ...newFilter });
    },
    [filter]
  );
  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader onChange={handleFilterChange} filter={filter} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool items={items} exportCSV={exportCSV} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Board items={items} loading={loading} />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
