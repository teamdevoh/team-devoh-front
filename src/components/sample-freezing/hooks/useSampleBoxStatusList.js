import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';
import { format } from '../../../constants';
import { useFormatMessage } from '../../../hooks';

const useSampleBoxStatusList = ({
  projectId,
  projectSiteId,
  boxTypeCodeId,
  freezerMachineCodeId,
  freezerLocationCodeId,
  searchKeyword
}) => {
  const t = useFormatMessage();
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    const res = await api.fetchSampleBoxStatus({
      projectId,
      projectSiteId,
      boxTypeCodeId,
      freezerMachineCodeId,
      freezerLocationCodeId,
      searchKeyword
    });

    setLoading(false);
    if (res && res.data) {
      return res.data;
    } else {
      return [];
    }
  });

  useEffect(
    () => {
      const fetch = async () => {
        const item = await fetchItems();

        setItems(item);
      };
      fetch();
    },
    [
      projectId,
      projectSiteId,
      boxTypeCodeId,
      freezerMachineCodeId,
      freezerLocationCodeId,
      searchKeyword
    ]
  );

  const exportCSV = useCallback(
    async fileName => {
      const items = await fetchItems();
      if (items) {
        items.forEach(item => {
          const {
            freezerMachine,
            freezerLocation,
            boxStatusDateTime,
            memberName
          } = item;

          item.freezerMachine =
            (freezerMachine || freezerLocation) &&
            `${freezerMachine} | ${freezerLocation}`;
          item.boxStatusDateTime = helpers.util.dateformat(
            boxStatusDateTime,
            format.YYYY_MM_DD_HHMM
          );
          item.memberName = !!memberName ? memberName.replaceAll(',', ' ') : '';
        });

        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          data: items,
          fields: [
            { key: 'project' },
            { key: 'site' },
            { key: 'boxBarcode' },
            { key: 'boxType' },
            { key: 'visit' },
            { key: 'aliquotAnalyte' },
            { key: 'boxNote' },
            { key: 'aliquotCnt' },
            { key: 'freezerMachine' },
            { key: 'boxStatusDateTime' },
            { key: 'memberName' }
          ],
          fieldNames: [
            t('project'),
            t('project.site'),
            'Box ID',
            'Type',
            'Visit',
            'Analyte',
            t('project.sitenote'),
            'Count',
            'Freezer',
            'Freezer In',
            'User'
          ]
        });

        return items;
      }
      return [];
    },
    [
      projectId,
      projectSiteId,
      boxTypeCodeId,
      freezerMachineCodeId,
      freezerLocationCodeId,
      searchKeyword
    ]
  );

  return [{ items, loading, exportCSV }];
};

export default useSampleBoxStatusList;
