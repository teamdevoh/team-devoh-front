import { asyncComponent } from '../modules';

const DashBoardContainer = asyncComponent(() => import('./DashBoardContainer'));

export default DashBoardContainer;
