import React, { Fragment, useState } from 'react';
import { Button, Label } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import { useBoolean, useCodes, useFormatMessage } from '../../hooks';
import BarcodePrintInfoModal from '../sample-barcode/BarcodePrintInfoModal';
import {
  FlexDiv,
  RightButtonWrap,
  StyledDetail
} from '../sample-barcode/styles';
import { StyledCSVExport } from '../export-tdm/styles';

const DashBoardTool = ({ items, exportCSV }) => {
  const t = useFormatMessage();
  const showBarcodePrintinfo = useBoolean(false);
  const [targetUrl, setTargetUrl] = useState();
  const [freezerMachineCodes] = useCodes({
    codeGroupId: codeGroup.FreezerMachineCode
  });
  const [csvLoading, setCsvLoading] = useState(false);

  const handlePrintFreezerLabel = () => {
    const machineIds = freezerMachineCodes.map(item => item.tag2);
    setTargetUrl(
      `http://oz.econsert.com/ozrviewer/ozreport_c.html?api=SampleManager&action=GetDeviceMachineManyByDeviceMachineCodes&doc=SampleManagement/DeviceMachineLabel&param=deviceMachineList:[${machineIds.join(
        ','
      )}]`
    );
    showBarcodePrintinfo.setTrue();
  };

  const handlePrintFreezerLocationLabel = () => {
    setTargetUrl(
      `http://oz.econsert.com/ozrviewer/ozreport_c.html?api=SampleManager&action=GetFreezerLocationLabel&doc=SampleManagement/FreezerLocationLabel&param=deviceTypeCode:1970|deviceMachineID:0`
    );
    showBarcodePrintinfo.setTrue();
  };

  const handleExport = async () => {
    setCsvLoading(true);
    try {
      await exportCSV('Freezing List');
    } catch (err) {}
    setCsvLoading(false);
  };

  return (
    <Fragment>
      <FlexDiv>
        <Label size="big">
          Box
          <StyledDetail>{items.length}</StyledDetail>
        </Label>
        <RightButtonWrap>
          <Button onClick={handlePrintFreezerLabel}>
            {t('print.freezer.label')}
          </Button>
          <Button onClick={handlePrintFreezerLocationLabel}>
            {t('print.freezer.location.label')}
          </Button>
          <StyledCSVExport
            positive
            icon="download"
            loading={csvLoading}
            onClick={handleExport}
            content="CSV Export"
            disabled={items.length === 0}
          />
        </RightButtonWrap>
      </FlexDiv>
      <BarcodePrintInfoModal
        open={showBarcodePrintinfo.value}
        onOK={showBarcodePrintinfo.setFalse}
        title={'Print Aliquot Barcode'}
        targetUrl={targetUrl}
      />
    </Fragment>
  );
};

export default DashBoardTool;
