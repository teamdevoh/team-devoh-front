import React, { Fragment, useRef } from 'react';
import { Form, Grid, Input, Icon } from 'semantic-ui-react';
import { ProjectsOfSamples, ProjectSite } from '../sample-status/Selector';
import { StyledForm } from '../sample-status/styles';
import { BoxType } from '../sample-boxes/Selector';
import { FreezerMachine } from '../sample-status/Selector';
import { FreezerLocation } from './Selector';

const DashBoardHeader = ({ filter, onChange }) => {
  const search = useRef('');

  const setSearch = value => {
    search.current = value;
  };

  const getSearch = () => {
    return search.current;
  };

  const handleSearchKeyUp = event => {
    if (event.keyCode === 13 || event.key.toLowerCase() === 'enter') {
      hdnaleSearchClick();
    }
  };

  const handleSearchChange = (e, { value }) => {
    setSearch(value);
  };

  const hdnaleSearchClick = () => {
    const value = getSearch().trim();

    onChange(null, { name: 'searchKeyword', value });
  };

  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <StyledForm>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Project</label>
                <ProjectsOfSamples
                  name="projectId"
                  value={filter.projectId}
                  onChange={onChange}
                  hasAll
                />
              </Form.Field>
              <Form.Field>
                <label>Site</label>
                <ProjectSite
                  projectId={filter.projectId}
                  name="projectSiteId"
                  value={filter.projectSiteId}
                  onChange={onChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Type</label>
                <BoxType
                  name="boxTypeCodeId"
                  value={filter.boxTypeCodeId}
                  onChange={onChange}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label>Freezer</label>
                <FreezerMachine
                  name="freezerMachineCodeId"
                  value={filter.freezerMachineCodeId}
                  onChange={onChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Location</label>
                <FreezerLocation
                  name="freezerLocationCodeId"
                  value={filter.freezerLocationCodeId}
                  onChange={onChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Search</label>
                <Input
                  placeholder="Box ID, Aliquot ID"
                  icon={
                    <Icon
                      name="search"
                      inverted
                      circular
                      link
                      onClick={hdnaleSearchClick}
                    />
                  }
                  onChange={handleSearchChange}
                  onKeyUp={handleSearchKeyUp}
                  name="searchKeyword"
                  fluid
                />
              </Form.Field>
            </Form.Group>
          </StyledForm>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
