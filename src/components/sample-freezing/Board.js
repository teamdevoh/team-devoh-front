import React, { Fragment, useState } from 'react';
import { DataGrid } from '../data-grid';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { useBoolean } from '../../hooks';
import { Modal } from '../modal';
import BoxTrackingContent from './BoxTrackingContent';
import { format } from '../../constants';

const Board = ({ items, loading }) => {
  const t = useFormatMessage();
  const showBoxTrackingModal = useBoolean(false);
  const [boxInfo, setBoxInfo] = useState(null);

  const columns = [
    {
      key: 'project',
      name: t('project'),
      width: 300
    },
    {
      key: 'site',
      name: t('project.site'),
      width: 170
    },
    {
      key: 'boxBarcode',
      name: 'Box ID',
      width: 100,
      formatter: ({ value, row }) => (
        <a
          style={{ cursor: 'pointer' }}
          onClick={() => {
            showBoxTrackingModal.setTrue();
            setBoxInfo(row);
          }}
        >
          {value}
        </a>
      )
    },
    {
      key: 'boxType',
      name: 'Type',
      width: 80
    },
    {
      key: 'visit',
      name: 'Visit',
      width: 110
    },
    {
      key: 'aliquotAnalyte',
      name: 'Analyte',
      width: 80
    },
    {
      key: 'boxNote',
      name: t('project.sitenote')
    },
    {
      key: 'aliquotCnt',
      name: 'Count',
      width: 70
    },
    {
      key: 'freezerMachine',
      name: 'Freezer',
      width: 120,
      formatter: ({ value, row }) => {
        const { freezerLocation } = row;

        return (value || freezerLocation) && `${value}, ${freezerLocation}`;
      }
    },
    {
      key: 'boxStatusDateTime',
      name: 'Freezer In',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      },
      width: 140
    },
    {
      key: 'memberName',
      name: 'User',
      width: 150
    }
  ];

  return (
    <Fragment>
      <DataGrid
        columns={columns}
        rows={items}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        rowKey="sampleBoxId"
        minHeight={550}
        loading={loading}
      />
      <Modal
        size="large"
        open={showBoxTrackingModal.value}
        onOK={showBoxTrackingModal.setFalse}
        title={'Box Tracking'}
        content={<BoxTrackingContent info={boxInfo} />}
      />
    </Fragment>
  );
};

export default Board;
