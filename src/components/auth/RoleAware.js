import { Component } from 'react';
import helpers from '../../helpers';
import PropTypes from 'prop-types';
import some from 'lodash/some';

const propTypes = {
  allowedRole: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
};

const defaultProps = {};

class RoleAware extends Component {
  hasRole() {
    if (helpers.Identity.profile) {
      const roles = helpers.Identity.profile.roles;
      const allowedRoles = this.props.allowedRole.split(',');

      return some(allowedRoles, allowedRole => {
        return roles.indexOf(allowedRole) > -1;
      });
    } else {
      return false;
    }
  }

  render() {
    const { children } = this.props;

    if (this.hasRole()) {
      return children;
    } else {
      return null;
    }
  }
}

RoleAware.propTypes = propTypes;
RoleAware.defaultProps = defaultProps;

export default RoleAware;
