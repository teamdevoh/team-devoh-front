import React, { useEffect, Fragment } from 'react';
import { useLocation } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

const Verification = () => {
  const query = new URLSearchParams(useLocation().search);
  const redirectUrl = query.get('cb');
  const history = useHistory();

  useEffect(() => {
    history.replace(
      `/${redirectUrl}?authKey=${query.get('auth')}&expire=${query.get(
        'expire'
      )}&loginName=${query.get('user')}`
    );
  }, []);

  return <Fragment />;
};

export default Verification;
