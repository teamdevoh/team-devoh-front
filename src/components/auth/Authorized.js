import React, { Component } from 'react';
import helpers from '../../helpers';
import { Redirect } from 'react-router-dom';
import { Grid } from 'semantic-ui-react';
import some from 'lodash/some';

function Authorized(WrappedComponent, allowedRole) {
  class WithAuthorized extends Component {
    constructor(props) {
      super(props);

      this.profile = helpers.Identity.profile;
      this.hasPermission = this.hasPermission.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
      return false;
    }

    hasPermission() {
      if (allowedRole) {
        const roles = this.profile.roles;
        const allowedRoles = allowedRole.split(',');

        return some(allowedRoles, role => {
          return roles.indexOf(role) > -1;
        });
      } else {
        return helpers.Auth ? true : false;
      }
    }

    render() {
      const { profile } = this;
      if (profile !== null) {
        if (!profile.securityPledge) {
          return <Redirect to={`/securitysign`} />;
        } else if (this.hasPermission()) {
          return <WrappedComponent {...this.props} />;
        } else {
          return (
            <Grid verticalAlign="middle" centered columns="3">
              <Grid.Row>
                <Grid.Column>
                  <h1>No page for you!</h1>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          );
        }
      } else {
        return <Redirect to={`/signin${helpers.history.location.search}`} />;
      }
    }
  }

  return WithAuthorized;
}

export default Authorized;
