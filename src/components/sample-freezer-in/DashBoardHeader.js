import React, { useState, useEffect } from 'react';
import { Form, Header, Responsive } from 'semantic-ui-react';
import { StyledForm } from '../sample-status/styles';
import styled from 'styled-components';
import notification from '../modal/notification';
import { useFormatMessage, useCodes } from '../../hooks';
import { useSampleBoxInfoToBoxingByBarcode } from '../sample-boxing/hooks';
import { StyledInput } from '../sample-aliquot/styles';
import { code, codeGroup } from '../../constants';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const StyledDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  & > * {
    flex: 1;
  }
`;

const StyledHeader = styled(Header)`
  &&& {
    margin: ${isMobile ? `0` : `0 0 0 10px`};
  }
`;

const BarcodeInput = styled(StyledInput)`
  flex-grow: 0;
`;

const DashBoardHeader = ({
  freezerInfo,
  setFreezerInfo,
  sampleBoxInfo,
  setSampleBoxInfo,
  locationInfo,
  setLocationInfo,
  boxItems,
  freezerIdEl,
  boxIdEl,
  locationIdEl
}) => {
  const t = useFormatMessage();
  const [freezerBarcode, setFreezerBarcode] = useState('');
  const [locationBarcode, setLocationBarcode] = useState('');
  const [sampleBoxBarcode, setSampleBoxBarcode] = useState('');

  const [freezerItems] = useCodes({
    codeGroupId: codeGroup.FreezerMachineCode
  });

  const [locationItems] = useCodes({
    codeGroupId: codeGroup.FreezerLocationCode
  });

  const [
    { loading: sampleBoxInfoLoading, fetch: fetchSampleBoxInfo }
  ] = useSampleBoxInfoToBoxingByBarcode();

  useEffect(() => {
    freezerIdEl.current.focus();
  }, []);

  useEffect(
    () => {
      setSampleBoxBarcode('');
      boxIdEl.current.focus();
    },
    [sampleBoxInfo.sampleBoxId]
  );

  useEffect(
    () => {
      if (!freezerInfo.freezerMachineCodeId) {
        setFreezerBarcode('');
        freezerIdEl.current.focus();
      }
      setLocationInfo({});
      setLocationBarcode('');
      setSampleBoxInfo({});
      setSampleBoxBarcode('');
    },
    [freezerInfo.freezerMachineCodeId]
  );

  const handleFreezerIdKeyUp = async e => {
    const value = e.target.value.trim();

    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      if (value !== '') {
        const freezersInfo = freezerItems.filter(item => item.tag1 === value);
        if (freezersInfo.length > 0) {
          const freezerInfo = {
            ...freezersInfo[0],
            freezerMachineCodeId: freezersInfo[0].value
          };
          setFreezerInfo(freezerInfo);
          setFreezerBarcode(value);
          locationIdEl.current.focus();
        } else {
          // 존재하지 않는 freezer id
          setFreezerInfo({});
          setFreezerBarcode('');
          freezerIdEl.current.focus();
          notification.warning({
            title: t('does.not.exist', { name: 'Freezer ID' })
          });
        }
      } else {
        notification.warning({
          title: t('empty.info', { name: 'Freezer ID' })
        });
      }
    }
  };

  const handleLocationIdKeyUp = async e => {
    const value = e.target.value.trim();

    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      if (value !== '') {
        const locationsInfo = locationItems.filter(item => item.tag1 === value);
        if (locationsInfo.length > 0) {
          const locationInfo = {
            ...locationsInfo[0],
            freezerLocationCodeId: locationsInfo[0].value
          };
          setLocationInfo(locationInfo);
          setLocationBarcode(value);
          boxIdEl.current.focus();
        } else {
          // 존재하지 않는 location id
          setLocationInfo({});
          locationIdEl.current.focus();
          setLocationBarcode('');
          notification.warning({
            title: t('does.not.exist', { name: 'Location ID' })
          });
        }
      } else {
        notification.warning({
          title: t('empty.info', { name: 'Location ID' })
        });
      }
    }
  };

  const handleBoxIdKeyUp = async e => {
    const value = e.target.value.trim();

    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      if (value !== '') {
        const addedBox = boxItems.filter(item => item.boxBarcode === value);

        if (addedBox.length > 0) {
          notification.warning({
            title: t('added.item', { name: 'Box' })
          });
          setSampleBoxBarcode('');
          boxIdEl.current.focus();
        } else {
          const boxInfo = await fetchSampleBoxInfo(value);
          if (boxInfo) {
            if (boxInfo.sampleBoxId > 0) {
              if (boxInfo.boxStatusCodeId === code.Warehousing) {
                notification.warning({
                  title: t('disabled.freezer.in')
                });
                setSampleBoxBarcode('');
                boxIdEl.current.focus();
              } else if (
                boxInfo.boxStatusCodeId === code.OutOfStock ||
                boxInfo.boxStatusCodeId === null
              ) {
                setSampleBoxInfo(boxInfo);
                setSampleBoxBarcode('');
                boxIdEl.current.focus();
              }
            } else {
              // 존재하지 않는 box id
              setSampleBoxInfo({});
              boxIdEl.current.focus();
              setSampleBoxBarcode('');
              notification.warning({
                title: t('does.not.exist', { name: 'Box ID' })
              });
            }
          }
        }
      } else {
        notification.warning({
          title: t('empty.info', { name: 'Box ID' })
        });
      }
    }
  };

  const handleBarcodeChange = (event, { value, name }) => {
    if (name === 'boxId') {
      setSampleBoxBarcode(value);
    } else if (name === 'locationId') {
      setLocationBarcode(value);
    } else if (name === 'freezerId') {
      setFreezerBarcode(value);
    }
  };

  return (
    <StyledForm>
      <Form.Field required>
        <label>Freezer ID</label>
        <StyledDiv>
          <BarcodeInput
            size="massive"
            name="freezerId"
            ref={freezerIdEl}
            onKeyUp={handleFreezerIdKeyUp}
            onChange={handleBarcodeChange}
            value={freezerBarcode}
          />
          <StyledHeader as="h2">
            {freezerInfo.tag1 && `${freezerInfo.tag1}, ${freezerInfo.note}`}
          </StyledHeader>
        </StyledDiv>
      </Form.Field>
      <Form.Field required>
        <label>Location ID</label>
        <StyledDiv>
          <BarcodeInput
            size="massive"
            name="locationId"
            ref={locationIdEl}
            onKeyUp={handleLocationIdKeyUp}
            onChange={handleBarcodeChange}
            value={locationBarcode}
          />
          <StyledHeader as="h2">
            {locationInfo.text && locationInfo.text}
          </StyledHeader>
        </StyledDiv>
      </Form.Field>
      <Form.Field required>
        <label>Box ID</label>
        <StyledDiv>
          <BarcodeInput
            size="massive"
            name="boxId"
            ref={boxIdEl}
            onKeyUp={handleBoxIdKeyUp}
            onChange={handleBarcodeChange}
            value={sampleBoxBarcode}
            loading={sampleBoxInfoLoading}
          />
        </StyledDiv>
      </Form.Field>
    </StyledForm>
  );
};

export default DashBoardHeader;
