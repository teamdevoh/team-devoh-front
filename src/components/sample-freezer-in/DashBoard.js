import React, { useState, useEffect, useRef } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import BoxBoard from './BoxBoard';
import DashBoardTool from './DashBoardTool';
import DashBoardHeader from './DashBoardHeader';
import notification from '../modal/notification';
import { useFormatMessage } from '../../hooks';
import api from './api';

const DashBoard = () => {
  const t = useFormatMessage();
  const freezerIdEl = useRef(null);
  const locationIdEl = useRef(null);
  const boxIdEl = useRef(null);
  const [freezerInfo, setFreezerInfo] = useState({});
  const [sampleBoxInfo, setSampleBoxInfo] = useState({});
  const [locationInfo, setLocationInfo] = useState({});
  const [boxItems, setBoxItems] = useState([]);
  const [removeBoxId, setRemoveBoxId] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(
    () => {
      if (sampleBoxInfo.boxBarcode) {
        const addedBox = boxItems.filter(
          item => item.boxBarcode === sampleBoxInfo.boxBarcode
        );

        if (addedBox.length === 0) {
          setBoxItems(boxItems.concat([{ ...sampleBoxInfo }]));
        }
      }
    },
    [sampleBoxInfo.boxBarcode]
  );

  useEffect(
    () => {
      if (removeBoxId) {
        const newItems = boxItems.filter(
          item => removeBoxId !== item.sampleBoxId
        );

        setBoxItems(newItems);
        setSampleBoxInfo({});
        setRemoveBoxId();
        boxIdEl.current.focus();
      }
    },
    [removeBoxId]
  );

  useEffect(
    () => {
      if (!freezerInfo.freezerMachineCodeId) {
        setBoxItems([]);
      }
    },
    [freezerInfo.freezerMachineCodeId]
  );

  const handleBoxRemove = ({ sampleBoxId }) => () => {
    setRemoveBoxId(sampleBoxId);
  };

  const handleSaveBoxClick = async () => {
    freezerIdEl.current.focus();
    setLoading(true);
    const model = boxItems.map(item => {
      const { sampleBoxId, currentDateTime } = item;
      return {
        sampleBoxId,
        BoxStatusDateTime: currentDateTime,
        freezerMachineCodeId: freezerInfo.freezerMachineCodeId,
        freezerLocationCodeId: locationInfo.freezerLocationCodeId
      };
    });
    try {
      const res = await api.updateSampleBoxesFreezerIn({
        model
      });

      if (res && res.data) {
        notification.success({
          title: t('common.ok'),
          onClose: () => {}
        });
        setFreezerInfo({});
      } else {
        notification.error({});
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Segment basic>
      <Grid.Row>
        <Grid.Column width={16}>
          <DashBoardHeader
            freezerInfo={freezerInfo}
            setFreezerInfo={setFreezerInfo}
            sampleBoxInfo={sampleBoxInfo}
            locationInfo={locationInfo}
            setSampleBoxInfo={setSampleBoxInfo}
            setLocationInfo={setLocationInfo}
            boxItems={boxItems}
            freezerIdEl={freezerIdEl}
            boxIdEl={boxIdEl}
            locationIdEl={locationIdEl}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool
            items={boxItems}
            onSave={handleSaveBoxClick}
            loading={loading}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <BoxBoard items={boxItems} onRemove={handleBoxRemove} />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
