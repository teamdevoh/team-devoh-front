import helpers from '../../helpers';

const updateSampleBoxesFreezerIn = ({ model }) => {
  return helpers.Service.put(`api/samples/boxes.in`, model);
};

export default {
  updateSampleBoxesFreezerIn
};
