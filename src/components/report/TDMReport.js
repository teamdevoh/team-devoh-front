import React, { useState } from 'react';

const TDMReport = () => {
  const [height, setHeight] = useState('0px');

  return (
    <iframe
      style={{
        width: '100%',
        height: height,
        overflowY: 'hidden'
      }}
      onLoad={() => {
        setHeight(window.document.body.scrollHeight + 'px');
      }}
      src="http://bioinformatics.vn:3838/pgrc/cPMTb-data-review/"
      // scrolling="no"
      frameBorder="0"
      title="cPMTb-data-review"
    />
  );
};

export default TDMReport;
