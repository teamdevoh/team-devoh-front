import React from 'react';
import { asyncComponent } from '../modules';

const TDMReport = asyncComponent(() => import('./TDMReport'));

export { TDMReport };
