import React, { useState, useEffect, useRef } from 'react';
import ReactMde from 'react-mde';
import 'react-mde/lib/styles/css/react-mde-all.css';
import { Icon, Loader, Image, Popup } from 'semantic-ui-react';
import helpers from '../../helpers';
import './Editor.css';
import { Modal } from '../modal';

const converter = helpers.util.markdownToHtml();

const generateIcon = iconObj => commandName => {
  if (commandName === 'loading') {
    return <Loader active inline size="mini" />;
  } else {
    const { title, name } = iconObj[commandName];

    if (!!title) {
      return (
        <Popup trigger={<Icon name={name} />} content={title} inverted basic />
      );
    } else {
      return <Icon name={name} />;
    }
  }
};

const MarkdownEditor = ({
  value: propValue,
  onSave,
  getValue,
  attachValue = '',
  iconObj,
  minEditorHeight: propMinEditorHeight = 50,
  ...rest
}) => {
  const textareaRef = useRef();
  const [value, setValue] = useState(propValue);
  // <"write" | "preview">
  const [selectedTab, setSelectedTab] = useState('write');
  const [textAreaStyle, setTextAreaStyle] = useState({
    height: propMinEditorHeight
  });
  const [showImg, setShowImg] = useState({
    open: false,
    src: null,
    alt: null
  });

  useEffect(
    () => {
      if (onSave) {
        getValue(value);
        setSelectedTab('write');
        setValue('');
      }
    },
    [onSave]
  );

  useEffect(
    () => {
      setValue(propValue);
    },
    [propValue]
  );

  useEffect(
    () => {
      setValue(value + attachValue);
    },
    [attachValue]
  );

  useEffect(
    () => {
      const { textAreaRef } = textareaRef.current;
      textAreaRef.style.height = 'auto';
      textAreaRef.style.height = `${textAreaRef.scrollHeight}px`;
      setTextAreaStyle({
        height: textAreaRef.scrollHeight
      });
    },
    [value]
  );

  const handleChange = newValue => {
    setValue(newValue);
  };

  const handleClose = () => {
    setShowImg({
      open: false,
      src: null,
      alt: null
    });
  };

  const handleOnImgClick = e => {
    if (e.target.tagName === 'IMG') {
      const { src, alt } = e.target;
      const open = true;
      setShowImg({
        open,
        src,
        alt
      });
    }
  };

  return (
    <div className="markdown-editor" onClick={handleOnImgClick}>
      <ReactMde
        value={value}
        onChange={handleChange}
        selectedTab={selectedTab}
        onTabChange={setSelectedTab}
        generateMarkdownPreview={markdown =>
          Promise.resolve(converter.makeHtml(markdown))
        }
        getIcon={
          typeof iconObj !== 'undefined' ? generateIcon(iconObj) : undefined
        }
        textAreaProps={{
          style: textAreaStyle
        }}
        ref={textareaRef}
        {...rest}
      />
      <Modal
        open={showImg.open}
        onOK={handleClose}
        title=""
        content={<Image src={showImg.src} alt={showImg.alt} fluid />}
      />
    </div>
  );
};

export default MarkdownEditor;
