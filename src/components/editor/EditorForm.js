import React, { Fragment, useState, useEffect } from 'react';
import { Form } from 'semantic-ui-react';
import styled from 'styled-components';
import MarkdownEditor from './MarkdownEditor';
import { commands } from 'react-mde';
import { UploadLayer } from '../modules';
import { useFormatMessage } from '../../hooks';
import helpers from '../../helpers';

const MarginForm = styled(Form)`
  &&& {
    // margin-top: 10px;
    margin-bottom: 10px;
  }
`;

const generateIconObj = (extraButtons, saveIconName = '', t) => {
  const iconObj = {
    header: { name: 'heading' },
    bold: { name: 'bold', title: 'Bold' },
    italic: { name: 'italic' },
    strikethrough: { name: 'strikethrough' },
    link: { name: 'linkify' },
    quote: { name: 'quote right' },
    code: { name: 'code' },
    image: { name: 'image', title: t('image.attach') },
    'unordered-list': { name: 'list' },
    'ordered-list': { name: 'ordered list' },
    'checked-list': { name: 'check square' },
    file: { name: 'cloud upload', title: t('file.attach') },
    save: {
      name: saveIconName !== '' ? saveIconName : 'send',
      title: t('common.save')
    }
  };

  for (let i = 0; i < extraButtons.length; i++) {
    const { name, iconName = '', icon } = extraButtons[i];

    if (!iconName && !!icon) {
      // iconObj[name] = icon;
    } else {
      iconObj[name] = iconName;
    }
  }

  return iconObj;
};

const generateCommandList = (
  handlers,
  extraCustomButtons,
  extraCustomLastButtons,
  loading = false
) => {
  const listCommands = [
    {
      commands: [
        commands.boldCommand,
        {
          name: 'image',
          execute: (state, api) => {
            handlers['image']();
          },
          buttonProps: { 'aria-label': 'Add image' },
          keyCommand: 'image'
        },
        {
          name: loading ? 'loading' : 'file',
          execute: (state, api) => {
            if (loading === false) {
              handlers['file']();
            }
          },
          buttonProps: { 'aria-label': 'Add file' },
          keyCommand: 'file keyCommand'
        },
        ...extraCustomButtons
      ]
    },
    {
      commands: [
        ...extraCustomLastButtons,
        {
          name: loading ? 'loading' : 'save',
          execute: (state, api) => {
            if (loading === false) {
              handlers['save']();
            }
          },
          buttonProps: { 'aria-label': 'Add file' },
          keyCommand: 'save keyCommand'
        }
      ]
    }
  ];

  return listCommands;
};

const generateContents = (value, signedUrlList) => {
  for (let url in signedUrlList) {
    if (signedUrlList.hasOwnProperty(url)) {
      const storageFileName = signedUrlList[url];
      value = value.split(url).join(`storageFileName:[${storageFileName}]`);
    }
  }

  return value;
};

const EditorForm = ({
  value = '',
  hasSaveButton = false,
  saveIconName = '',
  extraCustomButtons = [],
  extraCustomLastButtons = [],
  onSubmit = f => f,
  signedUrlList: propSignedUrlList = {},
  onUploadFile = f => f,
  attachs = [],
  loading = false,
  ...rest
}) => {
  const t = useFormatMessage();
  const [onSave, setOnSave] = useState(false);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [listCommands, setListCommands] = useState();
  const [attachValue, setAttachValue] = useState('');
  const [uploadType, setUploadType] = useState(null);
  const [signedUrlList, setSignedUrlList] = useState(propSignedUrlList);
  const [iconObj, setIconObj] = useState();

  useEffect(() => {
    setIconObj(
      generateIconObj(
        [...extraCustomButtons, ...extraCustomLastButtons],
        saveIconName,
        t
      )
    );
  }, []);

  useEffect(
    () => {
      setListCommands(
        generateCommandList(
          {
            image: toggleUploadModal('image'),
            file: toggleUploadModal('file'),
            save: handleSave
          },
          extraCustomButtons,
          extraCustomLastButtons,
          loading
        )
      );
    },
    [loading, attachs, extraCustomButtons]
  );

  const toggleUploadModal = type => () => {
    setUploadType(type);
    setShowUploadModal(!showUploadModal);
  };

  const handleSave = () => {
    setOnSave(true);
  };

  const handleSubmit = value => {
    const valueToStore = generateContents(value, signedUrlList);
    onSubmit(valueToStore);
    setOnSave(false);
  };

  const handleGetValue = value => {
    handleSubmit(value);
  };

  const handleDropFile = async files => {
    setShowUploadModal(false);

    const isImg = uploadType === 'image';
    if (isImg) {
      const imgSeparator = uploadType === 'image' ? '!' : '';
      const newSignedUrlList = {
        ...signedUrlList
      };
      let newAttachValue = '';
      const data = await helpers.util.upload({ files });

      for (let i = 0; i < data.length; i++) {
        const { name, originalName } = data[i];

        const signedUrl = await helpers.util.getSignedUrlByName(name);
        // const signedUrl = `https://example.com/your-file/${name}`;
        newSignedUrlList[signedUrl] = name;
        newAttachValue += `\n\n${imgSeparator}[${originalName}](${signedUrl})`;
      }
      ////////////////////////////////////////////////////////////////////////
      // const imgSeparator = uploadType === 'image' ? '!' : '';
      // const newSignedUrlList = {
      //   ...signedUrlList
      // };
      // let newAttachValue = '';

      // for (let i = 0; i < files.length; i++) {
      //   const { name } = files[i];

      //   // const signedUrl = await helpers.util.getSignedUrlByName(name);
      //   const signedUrl = `https://example.com/your-file/${name}`;
      //   newSignedUrlList[signedUrl] = name;
      //   newAttachValue += `\n\n${imgSeparator}[${name}](${signedUrl})`;
      // }
      // console.log(files);
      ////////////////////////////////////////////////////////////////////////

      setSignedUrlList(newSignedUrlList);
      setAttachValue(newAttachValue);
    } else {
      onUploadFile(files);
    }
  };

  const handleModalClose = () => {
    setShowUploadModal(false);
  };

  const isImg = uploadType === 'image';

  return (
    <Fragment>
      <MarginForm {...rest}>
        <Form.Group>
          <Form.Field required width="16">
            <MarkdownEditor
              onSave={onSave}
              getValue={handleGetValue}
              signedUrlList={signedUrlList}
              value={value}
              commands={listCommands}
              attachValue={attachValue}
              iconObj={iconObj}
            />
          </Form.Field>
        </Form.Group>
      </MarginForm>
      <UploadLayer
        open={showUploadModal}
        title={
          isImg
            ? t('common.attachment.upload.img')
            : t('common.attachment.upload')
        }
        onDrop={handleDropFile}
        onClose={handleModalClose}
        accept={isImg ? helpers.util.imageTypes : undefined}
      />
    </Fragment>
  );
};

export default EditorForm;
