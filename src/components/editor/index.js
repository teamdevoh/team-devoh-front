import MarkdownEditor from './MarkdownEditor';
import EditorForm from './EditorForm';

export default { MarkdownEditor, EditorForm };
