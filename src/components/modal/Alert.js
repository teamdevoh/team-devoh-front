import React, { memo } from 'react';
import { Button, Modal } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { AlertWrapper } from './styles';

const Alert = memo(
  ({ closeOnDimmerClick, content, dimmer, intl, open, onOK, title }) => {
    const t = useFormatMessage();
    return (
      <AlertWrapper
        open={open}
        dimmer={dimmer}
        closeOnDimmerClick={closeOnDimmerClick}
      >
        <Modal.Header>{title ? title : 'Warning'}</Modal.Header>
        <Modal.Content>{content}</Modal.Content>
        <Modal.Actions>
          <Button
            positive
            icon="checkmark"
            labelPosition="right"
            content={t('common.ok')}
            onClick={onOK}
          />
        </Modal.Actions>
      </AlertWrapper>
    );
  }
);

export default Alert;
