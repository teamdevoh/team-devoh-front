import styled from 'styled-components';
import { Modal, Responsive } from 'semantic-ui-react';

const ModalContainer = styled(Modal)`
  &&& {
    position: relative;
    bottom: 1em;
  }
`;

export const ScrollingWrapper = styled(ModalContainer)`
  &&& {
    position: relative;
    bottom: 1em;
    width: ${() => {
      if (Responsive.onlyLargeScreen.minWidth >= window.innerWidth) {
        return '100%';
      } else {
        return {};
      }
    }};
  }
`;

export const ConfirmWrapper = styled(ModalContainer).attrs({ size: 'tiny' })``;
export const AlertWrapper = styled(ModalContainer).attrs({ size: 'tiny' })``;
