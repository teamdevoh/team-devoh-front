import React from 'react';
import { Prompt } from 'react-router-dom';
import { useFormatMessage } from '../../hooks';

const LeavingGuard = ({ shouldBlock }) => {
  const t = useFormatMessage();
  return <Prompt when={shouldBlock} message={t('activity.leaving.guard')} />;
};

export default LeavingGuard;
