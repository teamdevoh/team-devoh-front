import React, { memo } from 'react';
import { Button, Modal } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { ConfirmWrapper } from './styles';

const Confirm = memo(
  ({
    closeOnDimmerClick,
    content,
    dimmer,
    disabledCancel,
    disabledOK,
    open,
    onCancel,
    onOK,
    title,
    loading
  }) => {
    const t = useFormatMessage();
    return (
      <ConfirmWrapper
        open={open}
        dimmer={dimmer}
        closeOnDimmerClick={closeOnDimmerClick}
      >
        <Modal.Header>{title ? title : 'Confirm'}</Modal.Header>
        <Modal.Content>{content ? content : 'Are you sure?'}</Modal.Content>
        <Modal.Actions>
          <Button negative onClick={onCancel} disabled={disabledCancel}>
            {t('common.cancel')}
          </Button>
          <Button
            positive
            icon="checkmark"
            labelPosition="right"
            content={t('common.ok')}
            onClick={onOK}
            loading={loading}
            disabled={disabledOK || loading}
          />
        </Modal.Actions>
      </ConfirmWrapper>
    );
  }
);

export default Confirm;
