import React from 'react';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { DoneCircle, Error, Warning, Information } from '../animation';

const MySwal = withReactContent(Swal);

const success = ({ title, text, onClose, timer }) => {
  MySwal.fire({
    allowOutsideClick: false,
    onClose,
    showConfirmButton: false,
    timer: timer || 1000,
    title: <DoneCircle />,
    html: `<strong>${title}</strong>`
  });
};

const error = ({ title, text = '' }) => {
  MySwal.fire({
    allowOutsideClick: false,
    confirmButtonText: 'OK',
    focusConfirm: false,
    html: title
      ? `<div><strong>${title}</strong><p>${text}</p></div>`
      : `Something went wrong!`,
    onClose: () => {},
    showCloseButton: false,
    title: <Error />
  });
};

const warning = ({ title, text, onClose }) => {
  MySwal.fire({
    allowOutsideClick: false,
    html: title,
    onClose,
    showConfirmButton: false,
    timer: 3000,
    title: <Warning />
  });
};

const info = ({ title, text = '', confirmButtonName, onClose }) => {
  MySwal.fire({
    allowOutsideClick: false,
    html: `<div><strong>${title}</strong><p>${text}</p></div>`,
    onClose,
    showConfirmButton: true,
    title: <Information />,
    confirmButtonText: confirmButtonName
  });
};

const confirm = ({
  title,
  content,
  cancelButtonName,
  confirmButtonName,
  onOK,
  onCancel = f => f
}) => {
  MySwal.fire({
    allowOutsideClick: false,
    html: content,
    showConfirmButton: true,
    showCancelButton: true,
    title: <strong>{title}</strong>,
    buttonsStyling: false,
    confirmButtonClass: 'ui positive button',
    cancelButtonClass: 'ui negative button',
    confirmButtonText: confirmButtonName || 'OK',
    cancelButtonText: cancelButtonName || 'Cancel',
    reverseButtons: true
  }).then(result => {
    if (result.value) {
      onOK();
    } else {
      onCancel();
    }
  });
};

const Any = ({ title, text, onClose, timer }) => {
  MySwal.fire({
    allowOutsideClick: false,
    onClose,
    showConfirmButton: false,
    timer: timer || 2500,
    title: title,
    html: `<strong>${text}</strong>`
  });
};

export default {
  error,
  success,
  warning,
  info,
  confirm,
  Any
};
