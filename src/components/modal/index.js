import { asyncComponent } from '../modules';

const Alert = asyncComponent(() => import('./Alert'));
const LeavingGuard = asyncComponent(() => import('./LeavingGuard'));

export { Alert, LeavingGuard };
export { default as notification } from './notification';
export { default as Modal } from './Modal';
export { default as Confirm } from './Confirm';
