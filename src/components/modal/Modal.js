import React, { memo } from 'react';
import { Modal as SMModal } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Close } from '../animation';
import { ScrollingWrapper } from './styles';

const Modal = memo(
  ({ closeOnDimmerClick, content, open, onOK, size, title, actions }) => {
    return (
      <ScrollingWrapper
        {...{ size: size } || {}}
        open={open}
        closeOnDimmerClick={closeOnDimmerClick}
      >
        <SMModal.Header>
          {title}
          <Close onClick={onOK} />
        </SMModal.Header>
        <SMModal.Content className="modal-content" scrolling>
          {content}
        </SMModal.Content>
        <SMModal.Actions>{actions}</SMModal.Actions>
      </ScrollingWrapper>
    );
  }
);

Modal.propTypes = {
  /**
   * Whether or not the Modal should close when the dimmer is clicked.
   */
  closeOnDimmerClick: PropTypes.bool,
  /**
   * Shorthand for primary content.
   */
  content: PropTypes.element.isRequired,
  /**
   * A Modal can appear in a dimmer.
   */
  dimmer: PropTypes.string,
  /**
   * Controls whether or not the Modal is displayed.
   */
  open: PropTypes.bool,
  /**
   * Called when a OK event happens.
   */
  onOK: PropTypes.func,
  /**
   * Header title
   */
  title: PropTypes.string.isRequired
};

export default Modal;
