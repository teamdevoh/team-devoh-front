import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import styled from 'styled-components';

const StyledButton = styled(Button)`
  &&& {
    margin-top: 0.5em;
  }
`;

const propTypes = {
  /**
   * Additional classes.
   */
  className: PropTypes.string,
  /**
   * A button can have different colors
   */
  color: PropTypes.oneOf([
    'red',
    'orange',
    'yellow',
    'olive',
    'green',
    'teal',
    'blue',
    'violet',
    'purple',
    'pink',
    'brown',
    'grey',
    'black',
    'facebook',
    'google plus',
    'instagram',
    'linkedin',
    'twitter',
    'vk',
    'youtube'
  ]),
  /**
   * Shorthand for primary content.
   */
  name: PropTypes.string.isRequired,
  /**
   * A button can take the width of its container.
   */
  fluid: PropTypes.bool,
  /**
   * Add an Icon by name, props object, or pass an <Icon />.
   */
  icon: PropTypes.string,
  /**
   * A labeled button can format a Label or Icon to appear on the left or right.
   */
  labelPosition: PropTypes.oneOf(['right', 'left']),
  /**
   * Called after user's click.
   *
   * @param {SyntheticEvent} event React's original SyntheticEvent.
   * @param {object} data All props
   */
  onClick: PropTypes.func
};

const defaultProps = {
  color: 'blue',
  fluid: false,
  icon: 'right arrow',
  labelPosition: 'right'
};

const LinkButton = ({
  color,
  fluid,
  icon,
  labelPosition,
  name,
  onClick,
  className,
  ...rest
}) => {
  return (
    <StyledButton
      fluid={fluid}
      color={color}
      icon={icon}
      className={className}
      labelPosition={labelPosition}
      content={name}
      onClick={onClick}
      {...rest}
    />
  );
};

LinkButton.propTypes = propTypes;
LinkButton.defaultProps = defaultProps;

export default LinkButton;
