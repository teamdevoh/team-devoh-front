import { asyncComponent } from '../modules';
import LinkButton from './LinkButton';

const AddLinkButton = asyncComponent(() => import('./AddLinkButton'));
const BackButton = asyncComponent(() => import('./BackButton'));
const CancelButton = asyncComponent(() => import('./CancelButton'));
const RedirectButton = asyncComponent(() => import('./RedirectButton'));
const RemoveButton = asyncComponent(() => import('./RemoveButton'));
const SaveButton = asyncComponent(() => import('./SaveButton'));
const AnchorBack = asyncComponent(() => import('./AnchorBack'));
const Anchor = asyncComponent(() => import('./Anchor'));
const SortButton = asyncComponent(() => import('./SortButton'));

export {
  AddLinkButton,
  BackButton,
  CancelButton,
  LinkButton,
  RedirectButton,
  RemoveButton,
  RemoveLinkButton,
  SaveButton,
  AnchorBack,
  Anchor,
  SortButton
};
