import React, { useCallback, Fragment } from 'react';
import { Icon } from 'semantic-ui-react';
import history from '../../helpers/history';
import { useFormatMessage } from '../../hooks';
import { Link } from './styles';

const AnchorBack = () => {
  const t = useFormatMessage();

  const handleClick = useCallback(() => {
    history.goBack();
  }, []);

  return (
    <Fragment>
      <Icon name="arrow left" />
      <Link onClick={handleClick}>{t('common.back')}</Link>
    </Fragment>
  );
};

export default AnchorBack;
