import React from 'react';
import { Link } from './styles';

const Anchor = ({ text, onClick }) => {
  return <Link onClick={onClick}>{text}</Link>;
};

export default Anchor;
