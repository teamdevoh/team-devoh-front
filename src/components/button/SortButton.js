import React from 'react';
import { Icon, Popup } from 'semantic-ui-react';
import styled from 'styled-components';
import { useFormatMessage } from '../../hooks';

const ClickedIcon = styled(Icon)`
  &&& {
    & {
      opacity: 0.5;
    }

    &.selected {
      color: #192254;
      opacity: 1;
    }
  }
`;

const SortButton = ({ curOrder, order, onClick }) => {
  const t = useFormatMessage();
  const handleClick = () => {
    onClick(null, order);
  };

  return (
    <Popup
      trigger={
        <div style={{ display: 'inline-block' }}>
          <ClickedIcon
            name={`sort numeric ${
              order === 'asc' ? 'ascending' : 'descending'
            }`}
            size="large"
            className={`link${curOrder === order ? ' selected' : ''}`}
            onClick={handleClick}
          />
        </div>
      }
      content={order === 'asc' ? t('ascending') : t('descending')}
      inverted
    />
  );
};

export default SortButton;
