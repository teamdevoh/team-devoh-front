import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import history from '../../helpers/history';
import { Button } from 'semantic-ui-react';

const propTypes = {
  /**
   * A button can have different colors
   */
  color: PropTypes.oneOf([
    'red',
    'orange',
    'yellow',
    'olive',
    'green',
    'teal',
    'blue',
    'violet',
    'purple',
    'pink',
    'brown',
    'grey',
    'black',
    'facebook',
    'google plus',
    'instagram',
    'linkedin',
    'twitter',
    'vk',
    'youtube'
  ]),
  /**
   * Additional classes.
   */
  className: PropTypes.string,
  /**
   * Shorthand for primary content.
   */
  name: PropTypes.string.isRequired
};

const defaultProps = {
  className: ''
};

const BackButton = ({ color, name, className }) => {
  const handleClick = useCallback(() => {
    history.goBack();
  }, []);

  return (
    <Button
      type="button"
      style={{ marginTop: '0.5em' }}
      color={color}
      content={name}
      className={className}
      onClick={handleClick}
    />
  );
};

BackButton.propTypes = propTypes;
BackButton.defaultProps = defaultProps;

export default BackButton;
