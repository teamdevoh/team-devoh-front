import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { RoleAware } from '../auth';
import styled from 'styled-components';

const StyledButton = styled(Button)`
  &&& {
    margin-top: 0.5em;
  }
`;

const propTypes = {
  /**
   * Allowed role name
   */
  allowedRole: PropTypes.string.isRequired,
  /**
   * Additional classes.
   */
  className: PropTypes.string,
  /**
   * A button can have different colors
   */
  color: PropTypes.oneOf([
    'red',
    'orange',
    'yellow',
    'olive',
    'green',
    'teal',
    'blue',
    'violet',
    'purple',
    'pink',
    'brown',
    'grey',
    'black',
    'facebook',
    'google plus',
    'instagram',
    'linkedin',
    'twitter',
    'vk',
    'youtube'
  ]),
  /**
   * A button can show it is currently unable to be interacted with.
   */
  disabled: PropTypes.bool,
  /**
   * A button can show a loading indicator.
   */
  loading: PropTypes.bool,
  /**
   * Shorthand for primary content.
   */
  name: PropTypes.string.isRequired,
  /**
   * Called after user's click.
   *
   * @param {SyntheticEvent} event React's original SyntheticEvent.
   * @param {object} data All props
   */
  onClick: PropTypes.func
};

const defaultProps = {
  color: 'blue',
  disabled: false,
  loading: false
};

const SaveButton = ({
  allowedRole,
  color,
  loading,
  className,
  disabled,
  name,
  onClick,
  size
}) => {
  return (
    <RoleAware allowedRole={allowedRole}>
      <StyledButton
        className={className}
        color={color}
        content={name}
        onClick={onClick}
        disabled={disabled || loading}
        size={size}
        loading={loading || false}
      />
    </RoleAware>
  );
};

SaveButton.propTypes = propTypes;
SaveButton.defaultProps = defaultProps;

export default SaveButton;
