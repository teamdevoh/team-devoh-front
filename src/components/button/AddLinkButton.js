import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import history from '../../helpers/history';

const propTypes = {
  /**
   * Additional classes.
   */
  className: PropTypes.string,
  /**
   * A button can have different colors
   */
  color: PropTypes.oneOf([
    'red',
    'orange',
    'yellow',
    'olive',
    'green',
    'teal',
    'blue',
    'violet',
    'purple',
    'pink',
    'brown',
    'grey',
    'black',
    'facebook',
    'google plus',
    'instagram',
    'linkedin',
    'twitter',
    'vk',
    'youtube'
  ]),
  /**
   * Shorthand for primary content.
   */
  name: PropTypes.string.isRequired,
  /**
   * new entry onto the history stack
   */
  to: PropTypes.string.isRequired
};

const defaultProps = {};

const AddLinkButton = ({ name, to, color, className }) => {
  const handleClick = useCallback(() => {
    history.push(to);
  }, []);

  return (
    <Button
      type="button"
      style={{ marginTop: '0.5em' }}
      color={color}
      content={name}
      className={className}
      onClick={handleClick}
      icon="plus"
    />
  );
};

AddLinkButton.propTypes = propTypes;
AddLinkButton.defaultProps = defaultProps;

export default AddLinkButton;
