import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { RoleAware } from '../auth';

const propTypes = {
  /**
   * Allowed role name
   */
  allowedRole: PropTypes.string.isRequired,
  /**
   * Additional classes.
   */
  className: PropTypes.string,
  /**
   * A button can have different colors
   */
  color: PropTypes.oneOf([
    'red',
    'orange',
    'yellow',
    'olive',
    'green',
    'teal',
    'blue',
    'violet',
    'purple',
    'pink',
    'brown',
    'grey',
    'black',
    'facebook',
    'google plus',
    'instagram',
    'linkedin',
    'twitter',
    'vk',
    'youtube'
  ]),
  /**
   * A button can show it is currently unable to be interacted with.
   */
  disabled: PropTypes.bool,
  /**
   * A button can show a loading indicator.
   */
  loading: PropTypes.bool,
  /**
   * Shorthand for primary content.
   */
  name: PropTypes.string.isRequired,
  /**
   * Called after user's click.
   *
   * @param {SyntheticEvent} event React's original SyntheticEvent.
   * @param {object} data All props
   */
  onClick: PropTypes.func
};

const defaultProps = {
  className: '',
  color: 'red',
  disabled: false,
  loading: false
};

const RemoveButton = ({
  allowedRole,
  className,
  color,
  disabled,
  loading,
  name,
  onClick
}) => {
  return (
    <RoleAware allowedRole={allowedRole}>
      <Button
        style={{ marginTop: '0.5em' }}
        type="button"
        color={color}
        content={name}
        className={className}
        onClick={onClick}
        disabled={disabled || loading}
        loading={loading || false}
      />
    </RoleAware>
  );
};

RemoveButton.propTypes = propTypes;
RemoveButton.defaultProps = defaultProps;

export default RemoveButton;
