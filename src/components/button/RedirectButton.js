import React from 'react';
import { Link } from 'react-router-dom';
import { onlyUpdateForKeys } from 'recompose';
import { Button } from 'semantic-ui-react';

const Redirect = onlyUpdateForKeys(['color', 'name', 'to', 'className'])(
  ({ children, color, name, to, className }) => {
    return (
      <Button
        style={{ marginTop: '0.5em' }}
        color={color}
        className={className}
        as={Link}
        to={to}
      >
        {children} {name}
      </Button>
    );
  }
);

export default Redirect;
