import React, { Fragment, useState, useEffect } from 'react';
import { Dimmer, Loader, Dropdown } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import type from './itemType';
import { useTBDrugSampleAliquotForChart } from './hooks/useTBDrugSampleAliquot';
import { generateChartDataForGroupByGender } from './ChartContainer';
import useProjectSampleAliquotList from '../../sample-setting/hooks/useProjectSampleAliquotList';

const TBDrugSampleChart = props => {
  const { projectId, targetCount, projectSiteId, chartOptions } = props;
  const [{ items: sampleAliquotItems }] = useProjectSampleAliquotList({
    projectId
  });
  const [data, setData] = useState();
  const [options, setOptions] = useState([]);
  const [projectSampleAliquotId, setProjectSampleAliquotId] = useState(null);
  const { data: tbDrugSampleAliquot, loading } = useTBDrugSampleAliquotForChart(
    {
      projectId,
      projectSiteId,
      parentItemCount: targetCount,
      projectSampleAliquotId:
        projectSampleAliquotId ||
        (sampleAliquotItems[0] &&
          sampleAliquotItems[0].projectSampleAliquotId) ||
        0
    }
  );

  useEffect(
    () => {
      const items = [];
      for (let i = 0; i < sampleAliquotItems.length; i++) {
        const { aliquotAnalyte, projectSampleAliquotId } = sampleAliquotItems[
          i
        ];

        items.push({
          text: aliquotAnalyte,
          value: projectSampleAliquotId
        });
      }

      setOptions(items);
    },
    [sampleAliquotItems]
  );

  useEffect(
    () => {
      setData(
        generateChartDataForGroupByGender({
          items: tbDrugSampleAliquot,
          doSort: true
        })
      );
    },
    [tbDrugSampleAliquot]
  );

  const handleAliquotChange = (event, { value }) => {
    setProjectSampleAliquotId(value);
  };

  return (
    <Fragment>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <div style={{ marginBottom: '20px' }}>
        {options.length !== 0 && (
          <Dropdown
            name="projectSampleAliquotId"
            search
            fluid
            selection
            onChange={handleAliquotChange}
            options={options}
            defaultValue={options[0].value}
          />
        )}
      </div>
      <CohortChart
        data={data}
        legend
        options={chartOptions[type.TBDrugSample]}
      />
    </Fragment>
  );
};

export default TBDrugSampleChart;
