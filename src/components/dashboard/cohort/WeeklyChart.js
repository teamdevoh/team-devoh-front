import React, { Fragment, useState, useEffect } from 'react';
import { Grid, Dimmer, Loader, Responsive } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import { ProjectSiteForChart } from '../../collection/Selector';
import { generateChartData } from './ChartContainer';
import useStatisticsForChart from './hooks/useStatisticsForChart';
import type from './itemType';
import DateRangePicker from '../../datepicker/DateRangePicker';
import helpers from '../../../helpers';
import styled from 'styled-components';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const DateRange = styled(DateRangePicker)`
  &&& {
    min-width: 220px;
  }
`;

const WeeklyChart = props => {
  const {
    projectId,
    id,
    targetCount,
    projectSiteId,
    period,
    chartOptions,
    itemType
  } = props;
  const [data, setData] = useState();
  const [filter, setFilter] = useState({
    projectSiteId,
    startDate: period.split(' ~ ')[0],
    endDate: period.split(' ~ ')[1]
  });
  const [loading, setLoading] = useState(false);
  const { fetchItems } = useStatisticsForChart(projectId);

  const fetch = async () => {
    setLoading(true);
    const param = {
      type: type.Week,
      projectSiteId: filter.projectSiteId,
      key: id,
      parentItemCount: targetCount,
      startDate: filter.startDate,
      endDate: filter.endDate
    };

    const newData = await fetchItems(param);

    const { data } = generateChartData({
      items: newData,
      doSort: false
    });

    setData(data);
    setLoading(false);
  };

  useEffect(
    () => {
      fetch();
    },
    [filter]
  );

  const handleProejctSiteChange = (e, { name, value }) => {
    setFilter({
      ...filter,
      projectSiteId: value
    });
  };

  const handleDateRangeChange = (e, { value }) => {
    const newFilter = {
      ...filter
    };
    if (value.length === 1) {
      const newDate = helpers.util.dateformat(value[0], 'YYYY-MM-DD');

      const sDiff = Math.abs(
        new Date(value[0]) - new Date(newFilter.startDate)
      );
      const eDiff = Math.abs(new Date(value[0]) - new Date(newFilter.endDate));

      if (sDiff < eDiff) {
        newFilter.startDate = newDate;
      } else {
        newFilter.endDate = newDate;
      }
    } else if (value.length === 2) {
      newFilter.startDate = helpers.util.dateformat(value[0], 'YYYY-MM-DD');
      newFilter.endDate = helpers.util.dateformat(value[1], 'YYYY-MM-DD');
    }
    setFilter({
      ...newFilter
    });
  };

  useEffect(
    () => {
      setFilter({
        ...filter,
        projectSiteId: props.projectSiteId
      });
    },
    [props.projectSiteId]
  );

  return (
    <Fragment>
      <div style={{ marginBottom: '20px' }}>
        <Grid columns={isMobile ? null : 'equal'}>
          <Grid.Column width={isMobile ? '16' : '8'}>
            <ProjectSiteForChart
              value={filter.projectSiteId}
              onChange={handleProejctSiteChange}
              projectId={projectId}
            />
          </Grid.Column>
          <Grid.Column>
            <DateRange
              name=""
              value={[filter.startDate, filter.endDate]}
              onChange={handleDateRangeChange}
              options={{
                maxDate: 'today'
              }}
            />
          </Grid.Column>
        </Grid>
      </div>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <CohortChart
        data={data}
        title={`${filter.startDate} ~ ${filter.endDate}`}
        options={chartOptions[itemType]}
      />
    </Fragment>
  );
};

export default WeeklyChart;
