import React, { useCallback } from 'react';
import { StyledBreadcrumbs, StyledName } from './styles';
import { Icon } from 'semantic-ui-react';

const Name = ({
  indent,
  name,
  onClick,
  hasChildren,
  parentName,
  showChildren
}) => {
  const handleClick = useCallback(
    () => {
      onClick();
    },
    [onClick]
  );

  return (
    <StyledName indent={indent} onClick={handleClick}>
      <StyledBreadcrumbs>{parentName}</StyledBreadcrumbs>
      {hasChildren && (
        <Icon name={`caret ${showChildren ? 'down' : 'right'}`} />
      )}
      <span
        style={{
          marginLeft: hasChildren === false ? '1.5em' : '',
          fontWeight: 'bold'
        }}
      >
        {name}
      </span>
    </StyledName>
  );
};

export default Name;
