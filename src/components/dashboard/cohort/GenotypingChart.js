import React, { Fragment, useState, useEffect } from 'react';
import { Dimmer, Loader, Dropdown } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import type from './itemType';
import { useGenotypingFieldForChart } from './hooks/useGenotypingField';
import { genotypingFields, genotypingFiledInfo } from './hooks/useGenotyping';
import { generateChartDataForGroupByGender } from './ChartContainer';

const GenotypingChart = props => {
  const { projectId, targetCount, projectSiteId, chartOptions } = props;
  const [data, setData] = useState();
  const [options, setOptions] = useState([]);
  const [codeGroupId, setCodeGroupId] = useState(genotypingFields[0]);
  const { data: genotyping, loading } = useGenotypingFieldForChart({
    projectId,
    projectSiteId,
    parentItemCount: targetCount,
    codeGroupId
  });

  useEffect(() => {
    const items = [];
    for (let i = 0; i < genotypingFields.length; i++) {
      const curCodeGroupId = genotypingFields[i];

      items.push({
        text: genotypingFiledInfo[curCodeGroupId].name,
        value: curCodeGroupId
      });
    }

    setOptions(items);
  }, []);

  useEffect(
    () => {
      setData(
        generateChartDataForGroupByGender({
          items: genotyping,
          doSort: true
        })
      );
    },
    [genotyping]
  );

  const handleCodeGroupChange = (event, { value }) => {
    setCodeGroupId(value);
  };

  return (
    <Fragment>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <div style={{ marginBottom: '20px' }}>
        <Dropdown
          name="codeGroupId"
          search
          fluid
          selection
          onChange={handleCodeGroupChange}
          options={options}
          value={codeGroupId}
        />
      </div>
      <CohortChart data={data} options={chartOptions[type.Genotyping]} />
    </Fragment>
  );
};

export default GenotypingChart;
