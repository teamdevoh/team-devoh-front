import helpers from '../../../helpers';

const fetchCohortStatistics = ({ projectSiteId, odataQuery = '' }) => {
  const params = helpers.qs.stringify({ projectSiteId });
  return helpers.Service.get(
    `api/activities/cohort/statistics?${params}${odataQuery}`
  );
};

const fetchStatisticsBySites = ({
  projectId,
  projectSiteId,
  parentItemCount
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/sites?${params}`
  );
};

const fetchSiteStatisticsForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  start,
  end
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount,
    start,
    end
  });

  return helpers.Service.get(
    `api/activities/cohort/statistics/sites/chart?${params}`
  );
};

const fetchStatisticsSubjectStatus = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subject.status?${params}${odataQuery}`
  );
};

const fetchStatisticsTreatmentOutcome = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/treatment.outcome?${params}${odataQuery}`
  );
};

const fetchInvestigatorsBySite = ({
  projectId,
  projectSiteId,
  parentItemCount
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(`api/activities/cohort/statistics/inve?${params}`);
};

const fetchSiteInvestigatorsStatisticsForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  start,
  end
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount,
    start,
    end
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/inve/chart?${params}`
  );
};

const fetchStatisticsInterview = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/interview?${params}${odataQuery}`
  );
};

const fetchStatisticsInterviewForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/interview/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsWeeks = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = '',
  targetDate
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount,
    targetDate
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/weeks?${params}${odataQuery}`
  );
};

const fetchStatisticsWeeksForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = '',
  startDate,
  endDate
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount,
    startDate,
    endDate
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/weeks/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsBody = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/body?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsBodyForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/body/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsDiagnosis = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/diagnosis?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsDiagnosisForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/diagnosis/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsInitOrReTreatment = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/treatment?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsInitOrReTreatmentForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/treatment/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsCaseConclusion = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/conclusion?${params}${odataQuery}`
  );
};

const fetchStatisticsCaseConclusionForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/conclusion/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsComorbid = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/comorbid?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsComorbidForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/comorbid/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsTdmTbDrug = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/tdm/tbdrug?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsTdmTbDrugForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/tdm/tbdrug/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsRegimen = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/regimen?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsRegimenForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/regimen/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsGenotyping = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/genotyping?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsGenotypingForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/genotyping/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsTBDrugSample = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/tbdrugsample?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsTBDrugSampleForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/tbdrugsample/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsADR = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/adr?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsADRForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/adr/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsMICResult = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/micresult?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsMICResultForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/micresult/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsLbch = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/lbch?${params}${odataQuery}`
  );
};

const fetchStatisticsSubjectsLbhem = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/lbhem?${params}${odataQuery}`
  );
};

const fetchStatisticsBySampleAliquotAnalyte = ({
  projectId,
  projectSiteId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/activities/cohort/statistics/subjects/sample?${params}${odataQuery}`
  );
};

export default {
  fetchCohortStatistics,
  fetchStatisticsBySites,
  fetchSiteStatisticsForChart,
  fetchStatisticsSubjectStatus,
  fetchStatisticsTreatmentOutcome,
  fetchInvestigatorsBySite,
  fetchSiteInvestigatorsStatisticsForChart,
  fetchStatisticsInterview,
  fetchStatisticsWeeks,
  fetchStatisticsWeeksForChart,
  fetchStatisticsInterviewForChart,
  fetchStatisticsSubjectsBody,
  fetchStatisticsSubjectsBodyForChart,
  fetchStatisticsSubjectsDiagnosis,
  fetchStatisticsSubjectsDiagnosisForChart,
  fetchStatisticsSubjectsInitOrReTreatment,
  fetchStatisticsSubjectsInitOrReTreatmentForChart,
  fetchStatisticsCaseConclusion,
  fetchStatisticsCaseConclusionForChart,
  fetchStatisticsSubjectsComorbid,
  fetchStatisticsSubjectsComorbidForChart,
  fetchStatisticsSubjectsTdmTbDrug,
  fetchStatisticsSubjectsTdmTbDrugForChart,
  fetchStatisticsSubjectsRegimen,
  fetchStatisticsSubjectsRegimenForChart,
  fetchStatisticsSubjectsGenotyping,
  fetchStatisticsSubjectsGenotypingForChart,
  fetchStatisticsSubjectsTBDrugSample,
  fetchStatisticsSubjectsTBDrugSampleForChart,
  fetchStatisticsSubjectsADR,
  fetchStatisticsSubjectsADRForChart,
  fetchStatisticsSubjectsMICResult,
  fetchStatisticsSubjectsMICResultForChart,
  fetchStatisticsSubjectsLbch,
  fetchStatisticsSubjectsLbhem,
  fetchStatisticsBySampleAliquotAnalyte
};
