import React, { Fragment, useState, useEffect } from 'react';
import { Dimmer, Loader, Tab } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import { generateChartData } from './ChartContainer';
import type from './itemType';
import {
  useWBC,
  useHemoglobin,
  usePlatelet,
  useEosinophil,
  useANC
} from './hooks';

const LbhemChart = props => {
  const { projectId, id, targetCount, projectSiteId, chartOptions } = props;
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [itemType, setItemType] = useState(type.WBC);
  const wbc = useWBC(projectId);
  const hemoglobin = useHemoglobin(projectId);
  const platelet = usePlatelet(projectId);
  const eosinophil = useEosinophil(projectId);
  const anc = useANC(projectId);

  const fetch = async () => {
    setLoading(true);
    let newData = [];
    const param = {
      type: itemType,
      projectSiteId,
      key: id,
      parentItemCount: targetCount
    };

    if (itemType === type.WBC) {
      newData = await wbc.build(param);
    }

    if (itemType === type.Hemoglobin) {
      newData = await hemoglobin.build(param);
    }

    if (itemType === type.Platelet) {
      newData = await platelet.build(param);
    }

    if (itemType === type.Eosinophil) {
      newData = await eosinophil.build(param);
    }

    if (itemType === type.ANC) {
      newData = await anc.build(param);
    }

    const { data } = generateChartData({ items: newData });
    setData(data);
    setLoading(false);
  };

  useEffect(
    () => {
      fetch();
    },
    [projectSiteId, itemType]
  );

  const handleTabChange = (e, { activeIndex, panes }) => {
    setItemType(panes[activeIndex].type);
  };

  const panes = [
    {
      menuItem: 'WBC',
      type: type.WBC,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.WBC]} />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'Hemoglobin',
      type: type.Hemoglobin,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.Hemoglobin]} />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'Platelet',
      type: type.Platelet,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.Platelet]} />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'Eosinophil(WBC x eosinophil x 10)',
      type: type.Eosinophil,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.Eosinophil]} />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'Absolute neutrophil count (ANC)',
      type: type.ANC,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.ANC]} />
        </Tab.Pane>
      )
    }
  ];

  return (
    <Fragment>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <Tab panes={panes} onTabChange={handleTabChange} />
    </Fragment>
  );
};

export default LbhemChart;
