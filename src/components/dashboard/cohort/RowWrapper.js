import React, { Fragment, useState } from 'react';
import Row from './Row';
import { useBoolean } from '../../../hooks';
import { Modal } from '../../modal';
import ChartContainer from './ChartContainer';
import useStatisticsForChart from './hooks/useStatisticsForChart';
import { useParams } from 'react-router-dom';
import useStatistics from './hooks/useStatistics';
import CPMTbSubjectChart from './CPMTbSubjectChart';
import TreatmentOutcomeChart from './TreatmentOutcomeChart';
import MonthlyChart from './MonthlyChart';
import GenotypingChart from './GenotypingChart';
import TBDrugSampleChart from './TBDrugSampleChart';
import type from './itemType';
import WeeklyChart from './WeeklyChart';
import LbchChart from './LbchChart';
import LbhemChart from './LbhemChart';
import { Tab } from 'semantic-ui-react';
import AdrChart from './AdrChart';
import AdrList from './AdrList';

export const chartOptionByItemType = {
  [type.Inve]: {
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          const { index, yLabel } = tooltipItem;
          const { metaData } = data.datasets[0];
          let label =
            `${data.labels[index]}(${metaData[index].siteName})` || '';

          if (label) {
            label += ': ';
          }
          label += yLabel;
          return label;
        }
      }
    }
  },
  [type.Week]: {
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          const { index, yLabel } = tooltipItem;
          const { metaData } = data.datasets[0];
          let label = `${data.labels[index]}(${metaData[index].period})` || '';

          if (label) {
            label += ': ';
          }
          label += yLabel;
          return label;
        }
      }
    }
  },
  [type.Gend]: {
    scales: {
      yAxes: [
        {
          ticks: {
            min: 0,
            stepSize: 100
          }
        }
      ]
    }
  }
};

const RowWrapper = ({
  indent,
  projectSiteId,
  parentName,
  items,
  activeRowKey,
  onRowClick
}) => {
  const { projectId } = useParams();
  const showChartModal = useBoolean(false);
  const [rowData, setRowData] = useState({});
  const { fetchItems } = useStatisticsForChart(projectId);
  const statistics = useStatistics(projectId);

  const handleOpenChartModal = rowData => () => {
    setRowData(rowData);
    showChartModal.setTrue();
  };

  const getChartContent = () => {
    const name = rowData.name;
    let result;

    switch (name) {
      case 'cPMTb Subject':
        result = (
          <CPMTbSubjectChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
      case 'Treatment Outcome':
        result = (
          <TreatmentOutcomeChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
      case 'Weekly':
        result = (
          <WeeklyChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
      case 'Monthly':
        result = (
          <MonthlyChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
      case 'Genotyping':
        result = (
          <GenotypingChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
      case 'TB Drug Sample':
        result = (
          <TBDrugSampleChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
      case 'LBCH':
        result = (
          <LbchChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
      case 'LBHEM':
        result = (
          <LbhemChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
      case 'ADR':
        const panes = [
          {
            menuItem: 'Chart',
            type: type.AdrSoc,
            render: () => (
              <Tab.Pane>
                <AdrChart
                  projectId={projectId}
                  projectSiteId={projectSiteId}
                  chartOptions={chartOptionByItemType}
                  {...rowData}
                />
              </Tab.Pane>
            )
          },
          {
            menuItem: 'ADR List',
            type: type.Adr,
            render: () => (
              <Tab.Pane>
                <AdrList projectId={projectId} projectSiteId={projectSiteId} />
              </Tab.Pane>
            )
          }
        ];

        result = <Tab panes={panes} />;
        break;
      default:
        result = (
          <ChartContainer
            projectSiteId={projectSiteId}
            fetchItems={fetchItems}
            chartOptions={chartOptionByItemType}
            {...rowData}
          />
        );
        break;
    }

    return result;
  };

  if (items != null && items.length > 0) {
    return (
      <Fragment>
        {items.map((item, index) => {
          return (
            <Row
              key={index + item.name}
              index={index}
              id={item.id}
              projectSiteId={projectSiteId}
              parentName={parentName}
              indent={indent}
              statistics={statistics}
              activeRowKey={activeRowKey}
              onRowClick={onRowClick}
              onChartClick={handleOpenChartModal({
                ...item
              })}
              {...item}
            />
          );
        })}
        <Modal
          size="large"
          open={showChartModal.value}
          onOK={showChartModal.setFalse}
          title={Object.keys(rowData).length !== 0 ? rowData.name : ''}
          content={getChartContent()}
        />
      </Fragment>
    );
  }

  return <Fragment />;
};

export default RowWrapper;
