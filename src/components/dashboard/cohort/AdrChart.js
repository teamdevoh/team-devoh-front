import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { Dimmer, Loader, Dropdown, Form } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import useStatisticsForChart from './hooks/useStatisticsForChart';
import type from './itemType';
import { generateChartData } from './ChartContainer';

import { adrGroup } from './hooks/useADR';
import { ProjectSiteForChart } from '../../collection/Selector';

const AdrChart = props => {
  const {
    projectId,
    id,
    targetCount,
    // projectSiteId,
    chartOptions,
    itemType: initType
  } = props;
  const { fetchItems } = useStatisticsForChart(projectId);
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [itemType, setItemType] = useState(initType);
  const [projectSiteId, setProjectSiteId] = useState(props.projectSiteId);
  const options = useMemo(
    () => {
      const newOptions = [];
      for (let i = 0; i < adrGroup.length; i++) {
        const { name, type } = adrGroup[i];

        newOptions.push({
          text: name,
          value: type
        });
      }

      return newOptions;
    },
    [adrGroup]
  );

  const handleItemTypeChange = (event, { value }) => {
    setItemType(value);
  };

  const fetch = async ({ id, targetCount, projectSiteId, itemType }) => {
    setLoading(true);
    const param = {
      type: itemType,
      projectSiteId: projectSiteId,
      key: id,
      parentItemCount: targetCount
    };

    const newData = await fetchItems(param);

    const { data } = generateChartData({ items: newData, doSolrt: true });

    setData(data);
    setLoading(false);
  };

  const handleProejctSiteChange = (e, { name, value }) => {
    setProjectSiteId(value);
  };

  useEffect(
    () => {
      fetch({
        id,
        targetCount,
        projectSiteId,
        itemType
      });
    },
    [id, targetCount, projectSiteId, itemType]
  );

  useEffect(
    () => {
      setProjectSiteId(props.projectSiteId);
    },
    [props.projectSiteId]
  );

  return (
    <Fragment>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <div style={{ marginBottom: '20px' }}>
        <Form>
          <Form.Group widths="equal">
            <Form.Field>
              <ProjectSiteForChart
                value={projectSiteId}
                onChange={handleProejctSiteChange}
                projectId={projectId}
              />
            </Form.Field>
            <Form.Field>
              <Dropdown
                name="itemType"
                search
                fluid
                selection
                onChange={handleItemTypeChange}
                options={options}
                defaultValue={options[0].value}
              />
            </Form.Field>
          </Form.Group>
        </Form>
      </div>
      <CohortChart data={data} options={chartOptions[type.Adr]} />
    </Fragment>
  );
};

export default AdrChart;
