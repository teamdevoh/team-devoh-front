import React from 'react';
import { Tab, Grid, Header } from 'semantic-ui-react';
import CohortDashboard from './CohortDashboard';
import CohortChartDashboard from './CohortChartDashboard';
import CopyRedirectUrl from '../../modules/CopyRedirectUrl';

const CohortTab = () => {
  const panes = [
    {
      menuItem: 'Chart',
      render: () => {
        return (
          <Tab.Pane>
            <CohortChartDashboard />
          </Tab.Pane>
        );
      }
    },
    {
      menuItem: 'List',
      render: () => {
        return (
          <Tab.Pane>
            <CohortDashboard />
          </Tab.Pane>
        );
      }
    }
  ];

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column>
          <Header as="h3" dividing>
            Dashboard
            <Header.Subheader>Cohort</Header.Subheader>
            <CopyRedirectUrl />
          </Header>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Tab panes={panes} />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default CohortTab;
