import api from '../api';
import helpers from '../../../../helpers';

const usePlatelet = projectId => {
  const standard = [140, 440];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];

    let decreasedCnt = 0;
    let normalCnt = 0;
    let increasedCnt = 0;

    const res = await api.fetchStatisticsSubjectsLbhem({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,plt&$filter=${odata.notEqual(
        'plt',
        null
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let minPlt = null;

          curSubjectItem.forEach(lbhem => {
            const { plt = null } = lbhem;
            if (plt !== null && plt.trim() !== '' && !isNaN(plt)) {
              if (minPlt === null || minPlt > Number(plt)) {
                minPlt = Number(plt);
              }
            }
          });

          if (minPlt !== null) {
            // PLT 값이 1000 보다 크면 PLT / 1000 해야됨
            if (minPlt > 1000) {
              minPlt /= 1000;
            }
            if (minPlt < standard[0]) {
              decreasedCnt++;
            } else if (minPlt >= standard[0] && minPlt <= standard[1]) {
              normalCnt++;
            } else if (minPlt > standard[1]) {
              increasedCnt++;
            }
          }
        }
      }

      data = [
        {
          name: 'Platelet count decreased(<140x10^9/L)',
          itemCount: decreasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Normal(>=140, <=440x10^9/L)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Platelet count increased(>440 x10^9/L)',
          itemCount: increasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default usePlatelet;
