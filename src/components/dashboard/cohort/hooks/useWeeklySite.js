import api from '../api';
import helpers from '../../../../helpers';

const useWeeklySite = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const generateData = ({ data, parentItemCount }) => {
    const g = _.groupBy(data, 'siteName');
    const newData = [];

    Object.keys(g).forEach(key => {
      newData.push({
        name: key,
        itemCount: g[key].length,
        targetCount: parentItemCount,
        rate: util.calcPercentage(g[key].length, parentItemCount),
        hasChildren: false
      });
    });

    return newData;
  };

  const build = async ({ projectSiteId, parentItemCount, parentPeriod }) => {
    const date = parentPeriod.split(' ~ ');
    const res = await api.fetchStatisticsInterview({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=SiteName&$filter=${odata.between(
        'Consdtc',
        date[0],
        date[1]
      )}`
    });

    if (res && res.data) {
      return generateData({ data: res.data, parentItemCount });
    }
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    parentPeriod
  }) => {
    const date = parentPeriod.split(' ~ ');
    const res = await api.fetchStatisticsInterviewForChart({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$filter=${odata.between('Consdtc', date[0], date[1])}`
    });

    if (res && res.data) {
      return generateData({ data: res.data, parentItemCount });
    }
  };

  return { build, buildForChart };
};

export default useWeeklySite;
