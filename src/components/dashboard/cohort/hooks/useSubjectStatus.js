import api from '../api';
import helpers from '../../../../helpers';
import { useCodes } from '../../../../hooks';
import { code, codeGroup } from '../../../../constants';
import type from '../itemType';

const useSubjectStatus = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;
  const [items] = useCodes({ codeGroupId: codeGroup.SubjectStatus });
  const statusCodes = [
    code.SubjectOngoing,
    code.SubjectDropout,
    code.SubjectCompletion
  ];

  const generateData = async ({ promises, parentItemCount }) => {
    const resAll = await Promise.all(promises);
    const data = [];

    resAll.forEach((res, index) => {
      if (res && res.data) {
        data.push({
          name: _.find(items, { key: statusCodes[index] }).text,
          itemCount: res.data.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(res.data.length, parentItemCount),
          hasChildren: true,
          itemType: type.SubjectStatusSite,
          metaData: { statusCode: statusCodes[index] }
        });
      }
    });

    return data;
  };

  const build = ({ projectSiteId, parentItemCount, query = '' }) => {
    let promises = [];

    statusCodes.forEach(statusCode => {
      promises.push(
        api.fetchStatisticsSubjectStatus({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=SubjectStatusCode&$filter=${odata.equal(
            'SubjectStatusCode',
            statusCode
          )} ${query}`
        })
      );
    });

    return generateData({ promises, parentItemCount });
  };

  return { build };
};

export default useSubjectStatus;
