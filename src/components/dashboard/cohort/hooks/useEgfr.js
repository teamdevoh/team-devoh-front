import api from '../api';
import helpers from '../../../../helpers';
import { code } from '../../../../constants';

const useEgfr = projectId => {
  const standard = [90, 60, 30, 15];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];

    let normalCnt = 0;
    let mildCnt = 0;
    let moderateCnt = 0;
    let severeCnt = 0;
    let esrd = 0;

    const res = await api.fetchStatisticsSubjectsLbch({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,cr,age,sexcodeId,ethniccodeId&$filter=${odata.notEqual(
        'cr',
        null
      )} and ${odata.notEqual('age', null)} and ${odata.notEqual(
        'sexcodeId',
        null
      )} and ${odata.notEqual('ethniccodeId', null)}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let maxCr = null;

          curSubjectItem.forEach(lbch => {
            const { cr = null, age, sexcodeId, ethniccodeId } = lbch;

            if (
              age !== null &&
              sexcodeId !== null &&
              ethniccodeId !== null &&
              cr !== null &&
              cr.trim() !== '' &&
              !isNaN(cr)
            ) {
              if (maxCr === null || maxCr < Number(cr)) {
                maxCr = Number(cr);
              }
            }
          });

          if (maxCr !== null) {
            const { age, sexcodeId, ethniccodeId } = curSubjectItem[0];
            /*
              case when b.SEXCodeId = 44 then CR / 0.9 else CR / 0.7 end as scrOverK
              ,case when b.SEXCodeId = 44 then -0.411 else -0.329 end as alpha
              ,case when b.SEXCodeId = 44 then 1.0 else 1.018 end as constant
              ,case when b.ETHNICCodeId = 511 then 1.159 else  1 end as race -- black 1.159
            */
            const scrOverK = maxCr / (sexcodeId === code.MALE ? 0.9 : 0.7);
            const alpha = sexcodeId === code.MALE ? -0.411 : -0.329;
            const constant = sexcodeId === code.MALE ? 1.0 : 1.018;
            const race = ethniccodeId === code.Black ? 1.159 : 1;

            /*
            ROUND(CASE WHEN scrOverK > 1 THEN 141 * POWER(1, alpha) * POWER(scrOverK,-1.209) * POWER(0.993,age) * constant * race
                ELSE 141 * POWER(scrOverK, alpha) * POWER(1,-1.209) * POWER(0.993,age) * constant * race END
             ,1) As egfr
           */
            let egrf = null;
            if (scrOverK > 1) {
              egrf =
                141 *
                Math.pow(1, alpha) *
                Math.pow(scrOverK, -1.209) *
                Math.pow(0.993, age) *
                constant *
                race;
            } else {
              egrf =
                141 *
                Math.pow(scrOverK, alpha) *
                Math.pow(1, -1.209) *
                Math.pow(0.993, age) *
                constant *
                race;
            }

            if (egrf >= standard[0]) {
              normalCnt++;
            } else if (egrf >= standard[1] && egrf < standard[0]) {
              mildCnt++;
            } else if (egrf >= standard[2] && egrf < standard[1]) {
              moderateCnt++;
            } else if (egrf >= standard[3] && egrf < standard[2]) {
              severeCnt++;
            } else if (egrf < standard[3]) {
              esrd++;
            }
          }
        }
      }

      data = [
        {
          name: 'Control(normal) GFR(>=90mL/min/1.73m2)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Mild decrease in GFR(60-89mL/min/1.73m2)',
          itemCount: mildCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Moderate decrease in GFR(30-59mL/min/1.73m2)',
          itemCount: moderateCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Severe decrease in GFR(15-29 mL/min/1.73m2)',
          itemCount: severeCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'ESRD: <15 not on dialysis 또는 투석중인 환자',
          itemCount: esrd,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useEgfr;
