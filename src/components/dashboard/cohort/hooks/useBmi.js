import api from '../api';
import helpers from '../../../../helpers';
import { useCodes, useFormatMessage } from '../../../../hooks';
import { codeGroup } from '../../../../constants';

const useBmi = projectId => {
  const t = useFormatMessage();
  const odata = helpers.oDataBuilder;
  const util = helpers.util;
  const [sexCodeItems] = useCodes({ codeGroupId: codeGroup.SexCode });
  const bmiRange = [
    [0, 18.5, `${t('underweight')}( ~ 18.5 kg/m2)`],
    [18.6, 23, `${t('healthy')}(18.6 ~ 23 kg/m2)`],
    [23.1, 25, `${t('overweight')}(23.1 ~ 25 kg/m2)`],
    [25.1, 30, `${t('obese')}(25.1 ~ 30 kg/m2)`],
    [30.1, 999, `${t('extremely.obese')}(30.1 ~ kg/m2)`]
  ];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const data = [];

    for (let i = 0; i < bmiRange.length; i++) {
      const [min, max, name] = bmiRange[i];

      const res = await api.fetchStatisticsSubjectsBody({
        projectId,
        projectSiteId,
        parentItemCount,
        odataQuery: `&$select=subjectid&$filter=${odata.between(
          'Bmi',
          min,
          max
        )}`
      });

      if (res && res.data) {
        data.push({
          name,
          itemCount: res.data.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(res.data.length, parentItemCount),
          hasChildren: false
        });
      }
    }

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    const sexCodeIds = [44, 45];
    const data = [];

    for (let i = 0; i < bmiRange.length; i++) {
      const [min, max, name] = bmiRange[i];
      const genderData = [];

      for (let n = 0; n < sexCodeIds.length; n++) {
        const sexCodeId = sexCodeIds[n];
        const res = await api.fetchStatisticsSubjectsBodyForChart({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=subjectid&$filter=${odata.between(
            'Bmi',
            min,
            max
          )} and ${odata.equal('SexcodeId', sexCodeId)}`
        });

        if (res && res.data) {
          genderData.push({
            label: name,
            itemCount: res.data.length,
            targetCount: parentItemCount,
            rate: util.calcPercentage(res.data.length, parentItemCount),
            hasChildren: false,
            name: _.find(sexCodeItems, { key: sexCodeIds[n] }).text
          });
        }
      }
      data.push(genderData);
    }

    return data;
  };

  return { build, buildForChart };
};

export default useBmi;
