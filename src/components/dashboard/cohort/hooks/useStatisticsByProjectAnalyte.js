import helpers from '../../../../helpers';
import api from '../api';

const useStatisticsByProjectAnalyte = () => {
  const util = helpers.util;

  const generateData = async ({
    fetch,
    projectId,
    projectSiteId,
    parentItemCount
  }) => {
    const data = [];

    const res = await fetch({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      const analytes = new Set();
      const totalByAnalytes = {};
      for (let i = 0; i < res.data.length; i++) {
        const { aliquotAnalyte, itemCount } = res.data[i];

        if (!analytes.has(aliquotAnalyte)) {
          analytes.add(aliquotAnalyte);
          totalByAnalytes[aliquotAnalyte] = 0;
        }

        totalByAnalytes[aliquotAnalyte] += itemCount;
      }

      for (let key of analytes) {
        const itemCount = totalByAnalytes[key];

        data.push({
          name: key,
          itemCount,
          targetCount: parentItemCount,
          rate: util.calcPercentage(itemCount, parentItemCount),
          hasChildren: false
        });
      }
    }

    return data;
  };

  const build = async ({
    metaData,
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    const data = await generateData({
      fetch: api.fetchStatisticsBySampleAliquotAnalyte,
      projectId: metaData.projectId,
      projectSiteId,
      parentItemCount
    });

    return data;
  };

  const buildForChart = async ({
    metaData,
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    const data = await generateData({
      fetch: api.fetchStatisticsBySampleAliquotAnalyte,
      projectId: metaData.projectId,
      projectSiteId,
      parentItemCount
    });

    return data;
  };

  return { build, buildForChart };
};

export default useStatisticsByProjectAnalyte;
