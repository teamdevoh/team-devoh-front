import api from '../api';
import helpers from '../../../../helpers';

const useWBC = projectId => {
  const standard = [4, 10];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];

    let decreasedCnt = 0;
    let normalCnt = 0;
    let leukocytosisCnt = 0;

    const res = await api.fetchStatisticsSubjectsLbhem({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,wbc&$filter=${odata.notEqual(
        'wbc',
        null
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let minWbc = null;

          curSubjectItem.forEach(lbhem => {
            const { wbc = null } = lbhem;
            if (wbc !== null && wbc.trim() !== '' && !isNaN(wbc)) {
              if (minWbc === null || minWbc > Number(wbc)) {
                minWbc = Number(wbc);
              }
            }
          });

          if (minWbc !== null) {
            if (minWbc < standard[0]) {
              decreasedCnt++;
            } else if (minWbc >= standard[0] && minWbc <= standard[1]) {
              normalCnt++;
            } else if (minWbc > standard[1]) {
              leukocytosisCnt++;
            }
          }
        }
      }

      data = [
        {
          name: 'WBC decreased(<4x10^9/L)',
          itemCount: decreasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Normal(>=4, <=10x10^9/L)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Leukocytosis(>10 x10^9/L)',
          itemCount: leukocytosisCnt,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useWBC;
