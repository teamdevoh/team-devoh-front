import moment from 'moment';
import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';
import { format } from '../../../../constants';

const useMonthly = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const fetchMonth = async props => {
    const { projectSiteId, parentItemCount, startDate, endDate } = props;

    const res = await api.fetchStatisticsInterview({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=SubjectId&$filter=${odata.between(
        'Consdtc',
        startDate,
        endDate
      )}`
    });

    return res.data;
  };

  const fetchMonthForChart = async props => {
    const { projectSiteId, parentItemCount, startDate, endDate } = props;

    const res = await api.fetchStatisticsInterviewForChart({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=SubjectId&$filter=${odata.between(
        'Consdtc',
        startDate,
        endDate
      )}`
    });

    return res.data;
  };

  const getStartEndDate = targetDate => {
    const startDate = moment(targetDate)
      .startOf('month')
      .format(format.YYYY_MM_DD);
    const endDate = moment(targetDate)
      .endOf('month')
      .format(format.YYYY_MM_DD);

    return { startDate, endDate };
  };

  const generateData = async ({
    projectSiteId,
    parentItemCount,
    parentPeriod,
    fetch
  }) => {
    let data = [];
    const date = parentPeriod.split(' ~ ');
    const minDate = date[0];
    const maxDate = date[1];
    const s = new Date(moment(minDate).startOf('month'));
    const e = new Date(moment(maxDate).endOf('month'));

    const diffMonth = Math.ceil(moment(e).diff(s, 'months', true));

    let targetDate = moment(maxDate);

    for (let i = 0; i < diffMonth; i++) {
      let { startDate, endDate } = getStartEndDate(targetDate);

      if (targetDate.month() === s.getMonth()) {
        startDate = minDate > startDate ? minDate : startDate;
      } else if (targetDate.month() === e.getMonth()) {
        endDate = maxDate < endDate ? maxDate : endDate;
      }

      const items = await fetch({
        projectSiteId,
        parentItemCount,
        startDate,
        endDate
      });

      if (items) {
        const standardDate = moment(startDate);
        const monthName = standardDate.format('MMM');

        data.push({
          name: `${monthName} ${standardDate.year()}`,
          itemCount: items.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(items.length, parentItemCount),
          hasChildren: true,
          itemType: itemType.MtSite,
          period: `${startDate} ~ ${endDate}`
        });
      }

      targetDate = moment(targetDate).add(-1, 'month');
    }

    return data;
  };

  const build = async ({ projectSiteId, parentItemCount, parentPeriod }) => {
    return generateData({
      projectSiteId,
      parentItemCount,
      parentPeriod,
      fetch: fetchMonth
    });
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    parentPeriod
  }) => {
    return generateData({
      projectSiteId,
      parentItemCount,
      parentPeriod,
      fetch: fetchMonthForChart
    });
  };

  return { build, buildForChart };
};

export default useMonthly;
