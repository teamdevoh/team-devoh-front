import api from '../api';
import helpers from '../../../../helpers';

const useRegimen = projectId => {
  const util = helpers.util;

  const generateDate = ({ data, parentItemCount }) => {
    const result = [];
    const g = _.groupBy(data, 'regimen');

    for (let regimen in g) {
      if (g.hasOwnProperty(regimen)) {
        const curRegimen = g[regimen];

        result.push({
          name: regimen,
          itemCount: curRegimen.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(curRegimen.length, parentItemCount),
          hasChildren: false
        });
      }
    }

    return result;
  };

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    let data = [];

    const res = await api.fetchStatisticsSubjectsRegimen({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      data = generateDate({ data: res.data, parentItemCount });
    }

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    let data = [];

    const res = await api.fetchStatisticsSubjectsRegimenForChart({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      data = generateDate({ data: res.data, parentItemCount });
    }

    return data;
  };

  return { build, buildForChart };
};

export default useRegimen;
