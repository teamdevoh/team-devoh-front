import api from '../api';
import helpers from '../../../../helpers';
import { useCodes } from '../../../../hooks';
import { codeGroup } from '../../../../constants';

const useGender = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;
  const [items] = useCodes({ codeGroupId: codeGroup.SexCode });
  const sexCodeIds = [44, 45];

  const generateData = async ({ promises, parentItemCount }) => {
    const resAll = await Promise.all(promises);
    const data = [];

    resAll.forEach((res, index) => {
      if (res && res.data) {
        data.push({
          name: _.find(items, { key: sexCodeIds[index] }).text,
          itemCount: res.data.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(res.data.length, parentItemCount),
          hasChildren: false
        });
      }
    });

    return data;
  };

  const build = ({ projectSiteId, parentItemCount, query = '' }) => {
    let promises = [];

    sexCodeIds.forEach(sexCodeId => {
      promises.push(
        api.fetchStatisticsInterview({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=SexcodeId&$filter=${odata.equal(
            'SexcodeId',
            sexCodeId
          )} ${query}`
        })
      );
    });

    return generateData({ promises, parentItemCount });
  };

  const buildForChart = ({ projectSiteId, parentItemCount, query = '' }) => {
    let promises = [];

    sexCodeIds.forEach(sexCodeId => {
      promises.push(
        api.fetchStatisticsInterviewForChart({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=SexcodeId&$filter=${odata.equal(
            'SexcodeId',
            sexCodeId
          )} ${query}`
        })
      );
    });

    return generateData({ promises, parentItemCount });
  };

  return { build, buildForChart };
};

export default useGender;
