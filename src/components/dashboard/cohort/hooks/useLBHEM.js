import api from '../api';
import itemType from '../itemType';

const useLBHEM = projectId => {
  const menuItems = [
    {
      name: 'WBC',
      key: 'wbc',
      count: 0,
      itemType: itemType.WBC
    },
    {
      name: 'Hemoglobin',
      key: 'hgb',
      count: 0,
      itemType: itemType.Hemoglobin
    },
    {
      name: 'Platelet',
      key: 'plt',
      count: null,
      itemType: itemType.Platelet
    },
    {
      name: 'Eosinophil(WBC x eosinophil x 10)',
      key: 'eos',
      count: 0,
      itemType: itemType.Eosinophil
    },
    {
      name: 'Absolute neutrophil count (ANC)',
      key: 'anc',
      count: 0,
      itemType: itemType.ANC
    }
  ];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const data = [];

    const res = await api.fetchStatisticsSubjectsLbhem({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];

          menuItems.forEach(item => {
            const { key } = item;
            let checkWbc = false;
            if (key === 'eos') {
              checkWbc = true;
            }

            for (let i = 0; i < curSubjectItem.length; i++) {
              const curLbhem = curSubjectItem[i][key] || null;

              if (checkWbc) {
                // Eosinophil(WBC x eosinophil x 10) 는 eos 값, wbc 값 모두 숫자여야 계산 가능
                const curWbc = curSubjectItem[i].wbc || null;
                const wbcisNum =
                  curWbc !== null && curWbc.trim() !== '' && !isNaN(curWbc);

                if (!wbcisNum) {
                  continue;
                }
              }

              if (
                curLbhem !== null &&
                curLbhem.trim() !== '' &&
                !isNaN(curLbhem)
              ) {
                item.count++;
                break;
              }
            }
          });
        }
      }

      menuItems.forEach(item => {
        data.push({
          name: item.name,
          itemCount: item.count,
          targetCount: Object.keys(g).length,
          hasChildren: true,
          itemType: item.itemType
        });
      });
    }

    return data;
  };

  return {
    build
  };
};

export default useLBHEM;
