import { useState } from 'react';
import itemType from '../itemType';
import api from '../api';
import {
  useSubjectStatus,
  useSubjectStatusSite,
  useTreatmentOutcome,
  useTreatmentOutcomeSite,
  useGender,
  useSmoke,
  useAge,
  useEthn,
  useMonthly,
  useMonthlySite,
  useWeeklySite,
  useBmi,
  useInitialDiagnosis,
  useFinalDiagnosis,
  useTBLocation,
  useInitiOrReTreatment,
  useComorbidity,
  useTdmTbDrug,
  useRegimen,
  useGenotyping,
  useGenotypingField,
  useTBDrugSample,
  useTBDrugSampleAliquot,
  useADR,
  useADRSoc,
  useADRPt,
  useADRName,
  useMICResult,
  useLBCH,
  useAlbumin,
  useCreatinine,
  useEgfr,
  useTotalBilirubin,
  useAST,
  useALT,
  useLBHEM,
  useWBC,
  useHemoglobin,
  usePlatelet,
  useEosinophil,
  useANC,
  useStatisticsByProjectAnalyte
} from './';
import helpers from '../../../../helpers';

const useStatistics = projectId => {
  const [loading, setLoading] = useState(false);
  const subjectStatus = useSubjectStatus(projectId);
  const subjectStatusSite = useSubjectStatusSite(projectId);
  const treatmentOutcome = useTreatmentOutcome(projectId);
  const treatmentOutcomeSite = useTreatmentOutcomeSite(projectId);
  const gender = useGender(projectId);
  const smoke = useSmoke(projectId);
  const age = useAge(projectId);
  const ethn = useEthn(projectId);
  const monthly = useMonthly(projectId);
  const monthlySite = useMonthlySite(projectId);
  const weeklySite = useWeeklySite(projectId);
  const bmi = useBmi(projectId);
  const initialDiagnosis = useInitialDiagnosis(projectId);
  const finalDiagnosis = useFinalDiagnosis(projectId);
  const tbLocation = useTBLocation(projectId);
  const initiOrReTreatment = useInitiOrReTreatment(projectId);
  const comorbidity = useComorbidity(projectId);
  const tdmTbDrug = useTdmTbDrug(projectId);
  const regimen = useRegimen(projectId);
  const genotyping = useGenotyping(projectId);
  const genotypingField = useGenotypingField(projectId);
  const analyte = useStatisticsByProjectAnalyte();
  const tbDrugSample = useTBDrugSample(projectId);
  const tbDrugSampleAliquot = useTBDrugSampleAliquot(projectId);
  const adr = useADR(projectId);
  const adrSoc = useADRSoc(projectId);
  const adrPt = useADRPt(projectId);
  const adrName = useADRName(projectId);
  const micResult = useMICResult(projectId);
  const lbch = useLBCH(projectId);
  const albumin = useAlbumin(projectId);
  const creatinine = useCreatinine(projectId);
  const egfr = useEgfr(projectId);
  const totalBilirubin = useTotalBilirubin(projectId);
  const ast = useAST(projectId);
  const alt = useALT(projectId);
  const lbhem = useLBHEM(projectId);
  const wbc = useWBC(projectId);
  const hemoglobin = useHemoglobin(projectId);
  const platelet = usePlatelet(projectId);
  const eosinophil = useEosinophil(projectId);
  const anc = useANC(projectId);

  const odata = helpers.oDataBuilder;

  const fetchItems = async props => {
    const { key, type, projectSiteId, parentItemCount, parentPeriod } = props;

    if (type === itemType.Site) {
      setLoading(true);
      const res = await api.fetchStatisticsBySites({
        projectId,
        projectSiteId,
        parentItemCount
      });
      setLoading(false);
      if (res && res.data) {
        return res.data;
      }
    }

    if (type === itemType.SubjectStatus) {
      setLoading(true);
      const data = await subjectStatus.build({
        ...props
      });
      setLoading(false);

      return data;
    }

    if (type === itemType.SubjectStatusSite) {
      setLoading(true);
      const data = await subjectStatusSite.build({
        ...props
      });
      setLoading(false);

      return data;
    }

    if (type === itemType.TreatmentOutcome) {
      setLoading(true);
      const data = await treatmentOutcome.build({
        ...props
      });
      setLoading(false);

      return data;
    }

    if (type === itemType.TreatmentOutcomeSite) {
      setLoading(true);
      const data = await treatmentOutcomeSite.build({
        ...props
      });
      setLoading(false);

      return data;
    }

    if (type === itemType.Inve) {
      setLoading(true);
      const res = await api.fetchInvestigatorsBySite({
        projectId,
        projectSiteId: key,
        parentItemCount
      });
      setLoading(false);
      if (res && res.data) {
        return res.data;
      }
    }
    if (type === itemType.Gend) {
      setLoading(true);

      const data = await gender.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Age) {
      setLoading(true);
      const data = await age.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Sex) {
      setLoading(true);
      const minAge = key;
      const maxAge = key + 9;
      const data = await gender.build({
        ...props,
        query: `and ${odata.between('age', minAge, maxAge)}`
      });
      setLoading(false);

      return data;
    }

    if (type === itemType.Ethn) {
      setLoading(true);
      const data = await ethn.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Smok) {
      setLoading(true);
      const data = await smoke.build(props);
      setLoading(false);

      return data;
    }
    if (type === itemType.Week) {
      setLoading(true);
      const res = await api.fetchStatisticsWeeks({
        ...props,
        targetDate: parentPeriod.split(' ~ ')[1]
      });
      setLoading(false);

      return res.data;
    }

    if (type === itemType.WkSite) {
      setLoading(true);
      const data = await weeklySite.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Monthly) {
      setLoading(true);
      const data = await monthly.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.MtSite) {
      setLoading(true);
      const data = await monthlySite.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Bmi) {
      setLoading(true);
      const data = await bmi.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.InitialDiagnosis) {
      setLoading(true);
      const data = await initialDiagnosis.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.FinalDiagnosis) {
      setLoading(true);
      const data = await finalDiagnosis.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.TBLocation) {
      setLoading(true);
      const data = await tbLocation.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.InitiOrReTreatment) {
      setLoading(true);
      const data = await initiOrReTreatment.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Comorbidity) {
      setLoading(true);
      const data = await comorbidity.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.TdmTbDrug) {
      setLoading(true);
      const data = await tdmTbDrug.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Regimen) {
      setLoading(true);
      const data = await regimen.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Genotyping) {
      setLoading(true);
      const data = await genotyping.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.GenotypingField) {
      setLoading(true);
      const data = await genotypingField.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.SampleAnalyte) {
      setLoading(true);
      const data = await analyte.build({ ...props, metaData: { projectId } });
      setLoading(false);

      return data;
    }

    if (type === itemType.TBDrugSample) {
      setLoading(true);
      const data = await tbDrugSample.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.TBDrugSampleAliquot) {
      setLoading(true);
      const data = await tbDrugSampleAliquot.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Adr) {
      setLoading(true);
      const data = await adr.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.AdrSoc) {
      setLoading(true);
      const data = await adrSoc.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.AdrPt) {
      setLoading(true);
      const data = await adrPt.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.AdrName) {
      setLoading(true);
      const data = await adrName.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.MICResult) {
      setLoading(true);
      const data = await micResult.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.LBCH) {
      setLoading(true);
      const data = await lbch.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Albumin) {
      setLoading(true);
      const data = await albumin.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Creatinine) {
      setLoading(true);
      const data = await creatinine.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.eGFR) {
      setLoading(true);
      const data = await egfr.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.TotalBilirubin) {
      setLoading(true);
      const data = await totalBilirubin.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.AST) {
      setLoading(true);
      const data = await ast.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.ALT) {
      setLoading(true);
      const data = await alt.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.LBHEM) {
      setLoading(true);
      const data = await lbhem.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.WBC) {
      setLoading(true);
      const data = await wbc.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Hemoglobin) {
      setLoading(true);
      const data = await hemoglobin.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Platelet) {
      setLoading(true);
      const data = await platelet.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Eosinophil) {
      setLoading(true);
      const data = await eosinophil.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.ANC) {
      setLoading(true);
      const data = await anc.build(props);
      setLoading(false);

      return data;
    }

    setLoading(false);
    return [];
  };

  return { fetchItems, loading };
};

export default useStatistics;
