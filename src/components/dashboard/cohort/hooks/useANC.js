import api from '../api';
import helpers from '../../../../helpers';

const useANC = projectId => {
  const standard = [500, 1000, 1500];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];

    let severe = 0;
    let moderate = 0;
    let mild = 0;
    let normal = 0;

    const res = await api.fetchStatisticsSubjectsLbhem({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,anc&$filter=${odata.notEqual(
        'anc',
        null
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let minAnc = null;

          curSubjectItem.forEach(lbhem => {
            const { anc = null } = lbhem;
            if (anc !== null && anc.trim() !== '' && !isNaN(anc)) {
              if (minAnc === null || minAnc > Number(anc)) {
                minAnc = Number(anc);
              }
            }
          });

          if (minAnc !== null) {
            // ANC 값이 20보다 작으면 ANC * 1000 해야 됨
            if (minAnc < 20) {
              minAnc *= 1000;
            }
            if (minAnc < standard[0]) {
              severe++;
            } else if (minAnc >= standard[0] && minAnc < standard[1]) {
              moderate++;
            } else if (minAnc >= standard[1] && minAnc < standard[2]) {
              mild++;
            } else if (minAnc >= standard[2]) {
              normal++;
            }
          }
        }
      }

      data = [
        {
          name: 'Severe neutropenia(<500)',
          itemCount: severe,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Moderate neutropenia(>=500, <1000)',
          itemCount: moderate,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Mild neutropenia(>=1000, <1500)',
          itemCount: mild,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Normal (>=1500)',
          itemCount: normal,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useANC;
