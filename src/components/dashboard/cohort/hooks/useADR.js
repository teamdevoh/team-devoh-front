import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';

export const adrGroup = [
  { name: 'SOC', type: itemType.AdrSoc },
  { name: 'PT', type: itemType.AdrPt },
  { name: 'ADR', type: itemType.AdrName }
];

const useADR = projectId => {
  const util = helpers.util;

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const data = [];

    const res = await api.fetchStatisticsSubjectsADR({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      const itemCount = res.data.length;
      const rate = util.calcPercentage(itemCount, parentItemCount);

      for (let i = 0; i < adrGroup.length; i++) {
        const { name, type } = adrGroup[i];

        data.push({
          name,
          itemCount,
          targetCount: parentItemCount,
          rate,
          itemType: type,
          hasChildren: true
        });
      }
    }

    return data;
  };

  return { build };
};

export default useADR;
