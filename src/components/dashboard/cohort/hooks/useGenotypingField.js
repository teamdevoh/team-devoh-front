import api from '../api';
import helpers from '../../../../helpers';
import { useCodes } from '../../../../hooks';
import { useBuild } from '../../../activity/hooks';
import { codeGroup } from '../../../../constants';
import { useEffect, useMemo, useState } from 'react';

const useGenotypingField = projectId => {
  const util = helpers.util;
  const { displayName } = useBuild();
  const [natGenCodes] = useCodes({ codeGroupId: codeGroup.NatGenCode });
  const [nat2PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Nat2PhenResCode
  });
  const [slco1b1GenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1GenResCode
  });
  const [slco1b1PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1PhenResCode
  });

  const filedInfo = {
    [codeGroup.NatGenCode]: { name: 'nat2GenResCodeId', source: natGenCodes },
    [codeGroup.Nat2PhenResCode]: {
      name: 'nat2PhenResCodeId',
      source: nat2PhenResCodes
    },
    [codeGroup.Slco1b1GenResCode]: {
      name: 'slco1b1GenResCodeId',
      source: slco1b1GenResCodes
    },
    [codeGroup.Slco1b1PhenResCode]: {
      name: 'slco1b1PhenResCodeId',
      source: slco1b1PhenResCodes
    }
  };

  const generateDate = ({ data, parentItemCount, codeGroupId }) => {
    const result = [];
    const { name: fieldName, source } = filedInfo[codeGroupId];
    const g = _.groupBy(data, fieldName);

    for (let codeId in g) {
      if (g.hasOwnProperty(codeId)) {
        const curCodeItems = g[codeId];
        const name = displayName({
          source,
          value: Number(codeId)
        });

        result.push({
          name,
          itemCount: curCodeItems.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(curCodeItems.length, parentItemCount),
          hasChildren: false
        });
      }
    }

    return result;
  };

  const build = async ({
    projectSiteId,
    parentItemCount,
    codeGroupId,
    query = ''
  }) => {
    const curFieldName = filedInfo[codeGroupId].name;
    let data = [];

    const res = await api.fetchStatisticsSubjectsGenotyping({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=${curFieldName}&$filter=${curFieldName} ne null and ${curFieldName} ne 0`
    });

    if (res && res.data) {
      data = generateDate({ data: res.data, parentItemCount, codeGroupId });
    }

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  return { build };
};

export const useGenotypingFieldForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  codeGroupId,
  query
}) => {
  const odata = helpers.oDataBuilder;
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [sexCodeItems] = useCodes({ codeGroupId: codeGroup.SexCode });
  const [natGenCodes] = useCodes({ codeGroupId: codeGroup.NatGenCode });
  const [nat2PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Nat2PhenResCode
  });
  const [slco1b1GenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1GenResCode
  });
  const [slco1b1PhenResCodes] = useCodes({
    codeGroupId: codeGroup.Slco1b1PhenResCode
  });

  const filedInfo = useMemo(
    () => ({
      [codeGroup.NatGenCode]: { name: 'nat2GenResCodeId', source: natGenCodes },
      [codeGroup.Nat2PhenResCode]: {
        name: 'nat2PhenResCodeId',
        source: nat2PhenResCodes
      },
      [codeGroup.Slco1b1GenResCode]: {
        name: 'slco1b1GenResCodeId',
        source: slco1b1GenResCodes
      },
      [codeGroup.Slco1b1PhenResCode]: {
        name: 'slco1b1PhenResCodeId',
        source: slco1b1PhenResCodes
      }
    }),
    [natGenCodes, nat2PhenResCodes, slco1b1GenResCodes, slco1b1PhenResCodes]
  );

  useEffect(
    () => {
      const buildForChart = async () => {
        setLoading(true);
        const { name: fieldName, source } = filedInfo[codeGroupId];
        const sexCodeIds = [44, 45];
        const data = [];

        for (let i = 0; i < source.length; i++) {
          const { value, text } = source[i];
          const genderData = [];
          let count = 0;

          for (let n = 0; n < sexCodeIds.length; n++) {
            const sexCodeId = sexCodeIds[n];

            const res = await api.fetchStatisticsSubjectsGenotypingForChart({
              projectId,
              projectSiteId,
              parentItemCount,
              odataQuery: `&$select=${fieldName}&$filter=${odata.equal(
                fieldName,
                value
              )} and ${odata.equal('SexcodeId', sexCodeId)}`
            });

            if (res && res.data) {
              count += res.data.length;
              genderData.push({
                label: text,
                itemCount: res.data.length,
                targetCount: parentItemCount,
                hasChildren: false,
                name: _.find(sexCodeItems, { key: sexCodeId }).text
              });
            }
          }

          if (count > 0) {
            data.push(genderData);
          }
        }

        setData(data);
        setLoading(false);
      };

      buildForChart();
    },
    [projectId, projectSiteId, parentItemCount, codeGroupId, query, filedInfo]
  );

  return { data, loading };
};

export default useGenotypingField;
