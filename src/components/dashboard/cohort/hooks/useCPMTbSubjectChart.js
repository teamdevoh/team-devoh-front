import { useState } from 'react';
import { generateChartData } from '../ChartContainer';

const useCPMTbSubjectChart = ({ fetchItems }) => {
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);

  const fetch = async ({
    id,
    targetCount,
    projectSiteId,
    period,
    itemType
  }) => {
    setLoading(true);
    const [start, end] = period.split(' ~ ');
    const param = {
      type: itemType,
      projectSiteId: projectSiteId,
      key: id,
      parentItemCount: targetCount,
      parentPeriod: period,
      start,
      end
    };

    const newData = await fetchItems(param);

    const { data } = generateChartData({ items: newData, doSolrt: true });

    setData(data);
    setLoading(false);
  };

  return {
    data,
    loading,
    fetch
  };
};

export default useCPMTbSubjectChart;
