import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';
import { codeGroup } from '../../../../constants';

export const genotypingFields = [
  codeGroup.NatGenCode,
  codeGroup.Nat2PhenResCode,
  codeGroup.Slco1b1GenResCode,
  codeGroup.Slco1b1PhenResCode
];

export const genotypingFiledInfo = {
  [codeGroup.NatGenCode]: { name: 'NAT2 Genotype', count: 0 },
  [codeGroup.Nat2PhenResCode]: { name: 'NAT2 Phenotype', count: 0 },
  [codeGroup.Slco1b1GenResCode]: { name: 'SLCO1B1 Genotype', count: 0 },
  [codeGroup.Slco1b1PhenResCode]: { name: 'SLCO1B1 Phenotype', count: 0 }
};

const useGenotyping = projectId => {
  const util = helpers.util;

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    let data = [];

    const res = await api.fetchStatisticsSubjectsGenotyping({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      for (let i = 0; i < res.data.length; i++) {
        const item = res.data[i];
        const {
          nat2GenResCodeId,
          nat2PhenResCodeId,
          slco1b1GenResCodeId,
          slco1b1PhenResCodeId
        } = item;

        if (!!nat2GenResCodeId) {
          genotypingFiledInfo[codeGroup.NatGenCode].count += 1;
        }
        if (!!nat2PhenResCodeId) {
          genotypingFiledInfo[codeGroup.Nat2PhenResCode].count += 1;
        }
        if (!!slco1b1GenResCodeId) {
          genotypingFiledInfo[codeGroup.Slco1b1GenResCode].count += 1;
        }
        if (!!slco1b1PhenResCodeId) {
          genotypingFiledInfo[codeGroup.Slco1b1PhenResCode].count += 1;
        }
      }

      for (let i = 0; i < genotypingFields.length; i++) {
        const curCodeGroupId = genotypingFields[i];
        const { name, count } = genotypingFiledInfo[curCodeGroupId];

        data.push({
          name,
          itemCount: count,
          targetCount: parentItemCount,
          rate: util.calcPercentage(count, parentItemCount),
          itemType: itemType.GenotypingField,
          codeGroupId: curCodeGroupId,
          hasChildren: true
        });
      }
    }

    return data;
  };

  return { build };
};

export default useGenotyping;
