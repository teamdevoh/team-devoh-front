import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useADRList = ({ projectId, projectSiteId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);

    const res = await api.fetchStatisticsSubjectsADR({
      projectId,
      projectSiteId
    });

    if (res && res.data) {
      setItems(res.data);
    }

    setLoading(false);
  });

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, projectSiteId]
  );

  return [{ items, loading }];
};

export default useADRList;
