import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';

const useADRName = projectId => {
  const util = helpers.util;

  const generateData = async ({
    projectSiteId,
    parentItemCount,
    aesocKey,
    aedecodKey,
    fetch,
    query = ''
  }) => {
    const data = [];

    const res = await fetch({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      let list = res.data;

      if (!!aesocKey) {
        list = list.filter(({ aesoc }) => {
          aesoc =
            aesoc === null || aesoc.trim() === ''
              ? 'no name'
              : aesoc.trim().toLocaleLowerCase();

          return aesoc === aesocKey;
        });
      }

      if (!!aedecodKey) {
        list = list.filter(({ aedecod }) => {
          aedecod =
            aedecod === null || aedecod.trim() === ''
              ? 'no name'
              : aedecod.trim().toLocaleLowerCase();

          return aedecod === aedecodKey;
        });
      }

      const itemGroup = _.groupBy(list, args => {
        return args['adrName'] === null || args['adrName'].trim() === ''
          ? 'no name'
          : args['adrName'].trim().toLocaleLowerCase();
      });

      for (let key in itemGroup) {
        if (itemGroup.hasOwnProperty(key)) {
          const curItems = itemGroup[key];
          const name = key === 'no name' ? 'no name' : curItems[0]['adrName'];

          data.push({
            name,
            itemCount: curItems.length,
            targetCount: parentItemCount,
            rate: util.calcPercentage(curItems.length, parentItemCount),
            itemType: itemType.AdrName,
            hasChildren: false
          });
        }
      }
    }

    return data;
  };

  const build = async ({
    projectSiteId,
    parentItemCount,
    aesocKey,
    aedecodKey,
    query = ''
  }) => {
    const data = await generateData({
      projectSiteId,
      parentItemCount,
      aesocKey,
      aedecodKey,
      fetch: api.fetchStatisticsSubjectsADR
    });

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    aesocKey,
    aedecodKey,
    query = ''
  }) => {
    return await generateData({
      projectSiteId,
      parentItemCount,
      aesocKey,
      aedecodKey,
      fetch: api.fetchStatisticsSubjectsADRForChart
    });
  };

  return { build, buildForChart };
};

export default useADRName;
