import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';

const useADRSoc = projectId => {
  const util = helpers.util;

  const generateData = async ({
    projectSiteId,
    parentItemCount,
    fetch,
    query = ''
  }) => {
    const data = [];

    const res = await fetch({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      const itemGroup = _.groupBy(res.data, args => {
        return args['aesoc'] === null || args['aesoc'].trim() === ''
          ? 'no name'
          : args['aesoc'].trim().toLocaleLowerCase();
      });

      for (let key in itemGroup) {
        if (itemGroup.hasOwnProperty(key)) {
          const curItems = itemGroup[key];
          const name = key === 'no name' ? 'no name' : curItems[0]['aesoc'];

          data.push({
            name,
            itemCount: curItems.length,
            targetCount: parentItemCount,
            rate: util.calcPercentage(curItems.length, parentItemCount),
            itemType: itemType.AdrPt,
            aesocKey: key,
            hasChildren: true
          });
        }
      }
    }

    return data;
  };

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const data = await generateData({
      projectSiteId,
      parentItemCount,
      fetch: api.fetchStatisticsSubjectsADR
    });

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    return await generateData({
      projectSiteId,
      parentItemCount,
      fetch: api.fetchStatisticsSubjectsADRForChart
    });
  };

  return { build, buildForChart };
};

export default useADRSoc;
