import { useState, useEffect } from 'react';
import api from '../api';
import helpers from '../../../../helpers';
import { useCodes } from '../../../../hooks';
import { codeGroup } from '../../../../constants';

const useTBDrugSampleAliquot = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const generateDate = ({ data, parentItemCount }) => {
    const result = [];
    const g = _.groupBy(data, 'shortName');

    for (let shortName in g) {
      if (g.hasOwnProperty(shortName)) {
        const curItems = g[shortName];

        result.push({
          name: shortName,
          itemCount: curItems.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(curItems.length, parentItemCount),
          hasChildren: false
        });
      }
    }

    return result;
  };

  const build = async ({
    projectSiteId,
    parentItemCount,
    projectSampleAliquotId,
    query = ''
  }) => {
    let data = [];

    const res = await api.fetchStatisticsSubjectsTBDrugSample({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$filter=${odata.equal(
        'projectSampleAliquotId',
        projectSampleAliquotId
      )} `
    });

    if (res && res.data) {
      data = generateDate({ data: res.data, parentItemCount });
    }

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  return { build };
};

export const useTBDrugSampleAliquotForChart = ({
  projectId,
  projectSiteId,
  parentItemCount,
  projectSampleAliquotId,
  query = ''
}) => {
  const odata = helpers.oDataBuilder;
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [sexCodeItems] = useCodes({ codeGroupId: codeGroup.SexCode });

  useEffect(
    () => {
      const buildForChart = async () => {
        setLoading(true);
        const sexCodeIds = [44, 45];
        const data = [];

        const res = await api.fetchStatisticsSubjectsTBDrugSampleForChart({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=shortName,sexCodeId&$filter=${odata.equal(
            'projectSampleAliquotId',
            projectSampleAliquotId
          )}`
        });

        if (res && res.data) {
          const shortNameGroup = _.groupBy(
            res.data,
            ({ shortName }) =>
              shortName === null ? 'null' : shortName.trim().toLocaleLowerCase()
          );

          for (let shortName in shortNameGroup) {
            if (shortNameGroup.hasOwnProperty(shortName)) {
              const curGroupItems = shortNameGroup[shortName];
              const genderData = [];

              const genderGroup = _.groupBy(curGroupItems, 'sexcodeId');
              for (let i = 0; i < sexCodeIds.length; i++) {
                const sexCodeId = sexCodeIds[i];
                const curItems = genderGroup[sexCodeId] || [];

                genderData.push({
                  label: shortName,
                  itemCount: curItems.length,
                  targetCount: parentItemCount,
                  hasChildren: false,
                  name: _.find(sexCodeItems, { key: sexCodeId }).text
                });
              }

              data.push(genderData);
            }
          }
        }

        setData(data);
        setLoading(false);
      };

      buildForChart();
    },
    [projectId, projectSiteId, parentItemCount, projectSampleAliquotId, query]
  );

  return { data, loading };
};

export default useTBDrugSampleAliquot;
