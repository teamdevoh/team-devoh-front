import api from '../api';
import helpers from '../../../../helpers';
import { useCodes } from '../../../../hooks';
import { code, codeGroup } from '../../../../constants';
import type from '../itemType';

const useTreatmentOutcome = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;
  const [items] = useCodes({ codeGroupId: codeGroup.CaseconCode });
  const caseconCodes = [
    code.Cured,
    code.TreatmentComplete,
    code.TreatmentFailed,
    code.Death,
    code.LostToFollowUp,
    code.NotEvaluated,
    code.Transfer,
    code.ChangeInDiagnosis,
    code.TBRelatedDeaths,
    code.NonTBRelatedDeaths
  ];

  const generateData = async ({ promises, parentItemCount }) => {
    const resAll = await Promise.all(promises);
    const data = [];

    resAll.forEach((res, index) => {
      if (res && res.data) {
        data.push({
          name: _.find(items, { key: caseconCodes[index] }).text,
          itemCount: res.data.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(res.data.length, parentItemCount),
          hasChildren: true,
          itemType: type.TreatmentOutcomeSite,
          metaData: { caseconCode: caseconCodes[index] }
        });
      }
    });

    return data;
  };

  const build = ({ projectSiteId, parentItemCount, query = '' }) => {
    const promises = [];

    caseconCodes.forEach(caseconCode => {
      promises.push(
        api.fetchStatisticsTreatmentOutcome({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=CaseconCodeId&$filter=${odata.equal(
            'CaseconCodeId',
            caseconCode
          )} ${query}`
        })
      );
    });

    return generateData({ promises, parentItemCount });
  };

  return { caseconCodesItems: items, build };
};

export default useTreatmentOutcome;
