import api from '../api';
import helpers from '../../../../helpers';
import { useCodes } from '../../../../hooks';
import { codeGroup } from '../../../../constants';

const useEthn = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;
  const [items] = useCodes({ codeGroupId: codeGroup.EthnicCode });
  const [sexCodeItems] = useCodes({ codeGroupId: codeGroup.SexCode });
  const ethnIds = [46, 47, 48, 49, 50, 51];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    let promises = [];
    let data = [];

    ethnIds.forEach(ethnId => {
      promises.push(
        api.fetchStatisticsInterview({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=EthniccodeId&$filter=${odata.equal(
            'EthniccodeId',
            ethnId
          )}`
        })
      );
    });

    const resAll = await Promise.all(promises);

    resAll.forEach((res, index) => {
      if (res && res.data) {
        data.push({
          name: _.find(items, { key: ethnIds[index] }).text,
          itemCount: res.data.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(res.data.length, parentItemCount),
          hasChildren: false
        });
      }
    });

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    const sexCodeIds = [44, 45];
    const data = [];

    for (let i = 0; i < ethnIds.length; i++) {
      const ethnId = ethnIds[i];
      const genderData = [];

      for (let n = 0; n < sexCodeIds.length; n++) {
        const sexCodeId = sexCodeIds[n];
        const res = await api.fetchStatisticsInterviewForChart({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=EthniccodeId&$filter=${odata.equal(
            'EthniccodeId',
            ethnId
          )} and ${odata.equal('SexcodeId', sexCodeId)}`
        });

        if (res && res.data) {
          genderData.push({
            label: _.find(items, { key: ethnId }).text,
            itemCount: res.data.length,
            targetCount: parentItemCount,
            rate: util.calcPercentage(res.data.length, parentItemCount),
            hasChildren: false,
            name: _.find(sexCodeItems, { key: sexCodeIds[n] }).text
          });
        }
      }
      data.push(genderData);
    }
    return data;
  };

  return { build, buildForChart };
};

export default useEthn;
