import api from '../api';
import helpers from '../../../../helpers';

const useAST = projectId => {
  const standard = [13, 33, 99, 165];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];
    let decreasedCnt = 0;
    let normalCnt = 0;
    let increasedCnt = 0;
    let ulnx3Cnt = 0;
    let ulnx5Cnt = 0;

    const res = await api.fetchStatisticsSubjectsLbch({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,ast&$filter=${odata.notEqual(
        'ast',
        null
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let maxAst = null;

          curSubjectItem.forEach(lbch => {
            const { ast = null } = lbch;
            if (ast !== null && ast.trim() !== '' && !isNaN(ast)) {
              if (maxAst === null || maxAst < Number(ast)) {
                maxAst = Number(ast);
              }
            }
          });

          if (maxAst !== null) {
            if (maxAst < standard[0]) {
              decreasedCnt++;
            } else if (maxAst >= standard[0] && maxAst <= standard[1]) {
              normalCnt++;
            } else if (maxAst > standard[1] && maxAst < standard[2]) {
              increasedCnt++;
            } else if (maxAst >= standard[2] && maxAst < standard[3]) {
              ulnx3Cnt++;
            } else if (maxAst >= standard[3]) {
              ulnx5Cnt++;
            }
          }
        }
      }

      data = [
        {
          name: 'AST decreased(<13)',
          itemCount: decreasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Normal(=>13, =<33U/L)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'AST increased(>33U/L)',
          itemCount: increasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'AST>ULNx3 (>= 99U/L)',
          itemCount: ulnx3Cnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'AST>ULNx5 (>= 165U/L)',
          itemCount: ulnx5Cnt,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useAST;
