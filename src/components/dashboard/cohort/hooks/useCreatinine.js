import api from '../api';
import helpers from '../../../../helpers';

const useCreatinine = projectId => {
  const standard = [0.8, 1.2];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];
    let decreasedCnt = 0;
    let normalCnt = 0;
    let increasedCnt = 0;

    const res = await api.fetchStatisticsSubjectsLbch({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,cr&$filter=${odata.notEqual('cr', null)}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let maxCr = null;

          curSubjectItem.forEach(lbch => {
            const { cr = null } = lbch;
            if (cr !== null && cr.trim() !== '' && !isNaN(cr)) {
              if (maxCr === null || maxCr < Number(cr)) {
                maxCr = Number(cr);
              }
            }
          });

          if (maxCr !== null) {
            if (maxCr < standard[0]) {
              decreasedCnt++;
            } else if (maxCr >= standard[0] && maxCr <= standard[1]) {
              normalCnt++;
            } else if (maxCr > standard[1]) {
              increasedCnt++;
            }
          }
        }
      }

      data = [
        {
          name: 'Creatinine decreased(<0.8mg/dl)',
          itemCount: decreasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Normal(0.8<=,<=1.2mg/dl)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Creatinine increased(1.2mg/dl<)',
          itemCount: increasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useCreatinine;
