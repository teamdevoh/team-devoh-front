import api from '../api';
import helpers from '../../../../helpers';
import { code } from '../../../../constants';

const useTdmTbDrug = projectId => {
  const util = helpers.util;

  const generateData = ({
    name,
    itemCount,
    targetCount,
    hasChildren,
    ...rest
  }) => {
    return {
      name,
      itemCount,
      targetCount,
      rate: util.calcPercentage(itemCount, targetCount),
      hasChildren,
      ...rest
    };
  };

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const data = [];

    const res = await api.fetchStatisticsSubjectsTdmTbDrug({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'exdrugId');

      for (let exdrugId in g) {
        if (g.hasOwnProperty(exdrugId)) {
          const curDrugs = g[exdrugId];

          if (Number(exdrugId) === Number(code.TBDrugOther)) {
            const otherGroup = _.groupBy(
              curDrugs,
              ({ exdrugoth }) =>
                exdrugoth === null
                  ? 'null'
                  : exdrugoth.trim().toLocaleLowerCase()
            );

            for (let exdrugoth in otherGroup) {
              if (otherGroup.hasOwnProperty(exdrugoth)) {
                data.push(
                  generateData({
                    name: otherGroup[exdrugoth][0].exdrugoth,
                    itemCount: otherGroup[exdrugoth].length,
                    targetCount: parentItemCount,
                    hasChildren: false
                  })
                );
              }
            }
          } else {
            const { shortName, metabolite, isExdrug } = curDrugs[0];
            const metaboliteStr = isExdrug ? '' : ` - ${metabolite}`;
            data.push(
              generateData({
                name: shortName + metaboliteStr,
                itemCount: curDrugs.length,
                targetCount: parentItemCount,
                hasChildren: false
              })
            );
          }
        }
      }
    }

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    const data = [];

    const res = await api.fetchStatisticsSubjectsTdmTbDrugForChart({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'exdrugId');

      for (let exdrugId in g) {
        if (g.hasOwnProperty(exdrugId)) {
          const curDrugs = g[exdrugId];

          if (Number(exdrugId) === Number(code.TBDrugOther)) {
            const otherGroup = _.groupBy(
              curDrugs,
              ({ exdrugoth }) =>
                exdrugoth === null
                  ? 'null'
                  : exdrugoth.trim().toLocaleLowerCase()
            );
            for (let exdrugoth in otherGroup) {
              if (otherGroup.hasOwnProperty(exdrugoth)) {
                data.push(
                  generateData({
                    name: otherGroup[exdrugoth][0].exdrugoth,
                    itemCount: otherGroup[exdrugoth].length,
                    targetCount: parentItemCount,
                    hasChildren: false
                  })
                );
              }
            }
          } else {
            const { metabolite, isExdrug } = curDrugs[0];
            const metaboliteStr = isExdrug ? '' : ` - ${metabolite}`;

            data.push(
              generateData({
                name: curDrugs[0].shortName + metaboliteStr,
                itemCount: curDrugs.length,
                targetCount: parentItemCount,
                hasChildren: false
              })
            );
          }
        }
      }
    }

    return data;
  };

  return { build, buildForChart };
};

export default useTdmTbDrug;
