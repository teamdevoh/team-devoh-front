import api from '../api';
import helpers from '../../../../helpers';
import { useCodes } from '../../../../hooks';
import { codeGroup } from '../../../../constants';

const useTBLocation = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;
  const [items] = useCodes({ codeGroupId: codeGroup.TBDiagnosisCode });
  const tbDiagnosisCodes = items.concat([
    { text: 'No Data', value: [null, 0] }
  ]);
  const [sexCodeItems] = useCodes({ codeGroupId: codeGroup.SexCode });

  const generateData = ({
    name,
    itemCount,
    targetCount,
    hasChildren,
    ...rest
  }) => {
    return {
      name,
      itemCount,
      targetCount,
      rate: util.calcPercentage(itemCount, targetCount),
      hasChildren,
      ...rest
    };
  };

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const data = [];

    for (let i = 0; i < tbDiagnosisCodes.length; i++) {
      const { value, text } = tbDiagnosisCodes[i];

      const res = await api.fetchStatisticsSubjectsDiagnosis({
        projectId,
        projectSiteId,
        parentItemCount,
        odataQuery: `&$select=subjectid,tbOtherSite&$filter=${odata.equalOrequal(
          'TbDiagnosiscodeId',
          value
        )}`
      });

      if (res && res.data) {
        data.push(
          generateData({
            name: text,
            itemCount: res.data.length,
            targetCount: parentItemCount,
            hasChildren: false
          })
        );
      }
    }

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    const sexCodeIds = [44, 45];
    const data = [];

    for (let i = 0; i < tbDiagnosisCodes.length; i++) {
      const { value, text } = tbDiagnosisCodes[i];
      const genderData = [];

      for (let n = 0; n < sexCodeIds.length; n++) {
        const sexCodeId = sexCodeIds[n];
        const res = await api.fetchStatisticsSubjectsDiagnosisForChart({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=subjectid,tbOtherSite&$filter=${odata.equalOrequal(
            'TbDiagnosiscodeId',
            value
          )} and ${odata.equal('SexcodeId', sexCodeId)}`
        });

        if (res && res.data) {
          genderData.push(
            generateData({
              label: text,
              itemCount: res.data.length,
              targetCount: parentItemCount,
              hasChildren: false,
              name: _.find(sexCodeItems, { key: sexCodeId }).text
            })
          );
        }
      }

      data.push(genderData);
    }

    return data;
  };

  return { build, buildForChart };
};

export default useTBLocation;
