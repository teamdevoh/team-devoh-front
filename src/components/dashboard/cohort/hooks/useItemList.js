import { useState, useEffect } from 'react';
import api from '../api';

const visibleItemList = {
  'Total Subject': false,
  '12-023': false
};

const filterItemsByProject = {
  76: {
    ...visibleItemList,
    'Subject Status': true,
    'Treatment Outcome': true
  }
};

const useItemList = ({ projectId, projectSiteId, odataQuery }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchItems = async () => {
    const res = await api.fetchCohortStatistics({ projectSiteId, odataQuery });
    if (res && res.data) {
      var newItems = res.data;

      const filterInfo =
        filterItemsByProject[Number(projectId)] || visibleItemList;
      if (typeof filterInfo !== 'undefined') {
        newItems = newItems.filter(item => {
          const { name } = item;

          if (typeof filterInfo[name] !== 'undefined') {
            return filterInfo[name];
          } else {
            return true;
          }
        });
      } else {
      }

      setItems(newItems);
    }
    setLoading(false);
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectSiteId, odataQuery]
  );

  return { items, loading };
};

export default useItemList;
