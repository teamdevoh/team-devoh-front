import { useState } from 'react';
import itemType from '../itemType';
import api from '../api';
import {
  useGender,
  useSmoke,
  useAge,
  useEthn,
  useMonthly,
  useMonthlySite,
  useWeeklySite,
  useBmi,
  useInitialDiagnosis,
  useFinalDiagnosis,
  useTBLocation,
  useInitiOrReTreatment,
  useComorbidity,
  useTdmTbDrug,
  useRegimen,
  useTBDrugSampleAliquot,
  useADRSoc,
  useADRPt,
  useADRName,
  useMICResult,
  useStatisticsByProjectAnalyte
} from './';
import helpers from '../../../../helpers';

const useStatisticsForChart = projectId => {
  const [loading, setLoading] = useState(false);
  const gender = useGender(projectId);
  const smoke = useSmoke(projectId);
  const age = useAge(projectId);
  const ethn = useEthn(projectId);
  const monthly = useMonthly(projectId);
  const monthlySite = useMonthlySite(projectId);
  const weeklySite = useWeeklySite(projectId);
  const bmi = useBmi(projectId);
  const initialDiagnosis = useInitialDiagnosis(projectId);
  const finalDiagnosis = useFinalDiagnosis(projectId);
  const tbLocation = useTBLocation(projectId);
  const initiOrReTreatment = useInitiOrReTreatment(projectId);
  const comorbidity = useComorbidity(projectId);
  const tdmTbDrug = useTdmTbDrug(projectId);
  const regimen = useRegimen(projectId);
  const analyte = useStatisticsByProjectAnalyte();
  const tbDrugSample = useTBDrugSampleAliquot(projectId);
  const adrSoc = useADRSoc(projectId);
  const adrPt = useADRPt(projectId);
  const adrName = useADRName(projectId);
  const micResult = useMICResult(projectId);

  const odata = helpers.oDataBuilder;

  const fetchItems = async props => {
    const { key, type, projectSiteId, parentItemCount } = props;

    if (type === itemType.Site) {
      setLoading(true);
      const res = await api.fetchSiteStatisticsForChart({
        projectId,
        projectSiteId,
        parentItemCount,
        ...props
      });
      setLoading(false);
      if (res && res.data) {
        return res.data;
      }
    }
    if (type === itemType.Inve) {
      setLoading(true);
      const res = await api.fetchSiteInvestigatorsStatisticsForChart({
        projectId,
        projectSiteId,
        parentItemCount,
        ...props
      });
      setLoading(false);
      if (res && res.data) {
        return res.data;
      }
    }
    if (type === itemType.Gend) {
      setLoading(true);

      const data = await gender.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Age) {
      setLoading(true);
      const data = await age.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Sex) {
      setLoading(true);
      const minAge = key;
      const maxAge = key + 9;
      const data = await gender.buildForChart({
        ...props,
        query: `and ${odata.between('age', minAge, maxAge)}`
      });
      setLoading(false);

      return data;
    }

    if (type === itemType.Ethn) {
      setLoading(true);
      const data = await ethn.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Smok) {
      setLoading(true);
      const data = await smoke.buildForChart(props);
      setLoading(false);

      return data;
    }
    if (type === itemType.Week) {
      setLoading(true);
      const res = await api.fetchStatisticsWeeksForChart({
        ...props
      });
      setLoading(false);

      return res.data;
    }

    if (type === itemType.WkSite) {
      setLoading(true);
      const data = await weeklySite.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Monthly) {
      setLoading(true);
      const data = await monthly.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.MtSite) {
      setLoading(true);
      const data = await monthlySite.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Bmi) {
      setLoading(true);
      const data = await bmi.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.InitialDiagnosis) {
      setLoading(true);
      const data = await initialDiagnosis.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.FinalDiagnosis) {
      setLoading(true);
      const data = await finalDiagnosis.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.TBLocation) {
      setLoading(true);
      const data = await tbLocation.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.InitiOrReTreatment) {
      setLoading(true);
      const data = await initiOrReTreatment.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Comorbidity) {
      setLoading(true);
      const data = await comorbidity.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.TdmTbDrug) {
      setLoading(true);
      const data = await tdmTbDrug.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Regimen) {
      setLoading(true);
      const data = await regimen.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.SampleAnalyte) {
      setLoading(true);
      const data = await analyte.buildForChart({
        ...props,
        metaData: { projectId }
      });
      setLoading(false);

      return data;
    }

    if (type === itemType.TBDrugSample) {
      setLoading(true);
      const data = await tbDrugSample.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Adr || type === itemType.AdrSoc) {
      setLoading(true);
      const data = await adrSoc.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.AdrPt) {
      setLoading(true);
      const data = await adrPt.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.AdrName) {
      setLoading(true);
      const data = await adrName.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.MICResult) {
      setLoading(true);
      const data = await micResult.buildForChart(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.LBCH) {
      setLoading(true);
      const data = await albumin.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.Creatinine) {
      setLoading(true);
      const data = await creatinine.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.eGFR) {
      setLoading(true);
      const data = await egfr.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.TotalBilirubin) {
      setLoading(true);
      const data = await totalBilirubin.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.AST) {
      setLoading(true);
      const data = await ast.build(props);
      setLoading(false);

      return data;
    }

    if (type === itemType.ALT) {
      setLoading(true);
      const data = await alt.build(props);
      setLoading(false);

      return data;
    }

    setLoading(false);
    return [];
  };

  return { fetchItems, loading };
};

export default useStatisticsForChart;
