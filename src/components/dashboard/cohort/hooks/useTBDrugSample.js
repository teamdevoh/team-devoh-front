import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';
import useProjectSampleAliquotList from '../../../sample-setting/hooks/useProjectSampleAliquotList';

const useTBDrugSample = projectId => {
  const util = helpers.util;
  const [{ items: sampleAliquotItems }] = useProjectSampleAliquotList({
    projectId
  });

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    let data = [];

    const res = await api.fetchStatisticsSubjectsTBDrugSample({
      projectId,
      projectSiteId,
      parentItemCount
    });

    const items = {};

    for (let i = 0; i < sampleAliquotItems.length; i++) {
      const { aliquotAnalyte, projectSampleAliquotId } = sampleAliquotItems[i];

      items[projectSampleAliquotId] = { name: aliquotAnalyte, count: 0 };
    }

    if (res && res.data) {
      for (let i = 0; i < res.data.length; i++) {
        const item = res.data[i];
        const { projectSampleAliquotId } = item;

        items[projectSampleAliquotId].count += 1;
      }

      for (let i = 0; i < sampleAliquotItems.length; i++) {
        const curProjectAliquotId =
          sampleAliquotItems[i].projectSampleAliquotId;
        const { name, count } = items[curProjectAliquotId];

        data.push({
          name,
          itemCount: count,
          targetCount: parentItemCount,
          rate: util.calcPercentage(count, parentItemCount),
          itemType: itemType.TBDrugSampleAliquot,
          projectSampleAliquotId: curProjectAliquotId,
          hasChildren: true
        });
      }
    }

    return data;
  };

  return { build };
};

export default useTBDrugSample;
