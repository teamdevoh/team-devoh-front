import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';
import { useCodes } from '../../../../hooks';
import { codeGroup } from '../../../../constants';

const useAge = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;
  const [items] = useCodes({ codeGroupId: codeGroup.SexCode });

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const ages = [1, 11, 21, 31, 41, 51, 61, 71, 81, 91];
    const promises = [];

    ages.forEach(age => {
      const minAge = age;
      const maxAge = age === 91 ? 999 : age + 9;
      promises.push(
        api.fetchStatisticsInterview({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=subjectId&$filter=${odata.between(
            'age',
            minAge,
            maxAge
          )}`
        })
      );
    });

    const resAll = await Promise.all(promises);
    const data = [];

    resAll.forEach((res, index) => {
      if (res && res.data) {
        const minAge = ages[index];
        const maxAge = ages[index] + 9;
        data.push({
          id: minAge,
          name: `${minAge} ~ ${minAge === 91 ? '' : maxAge}`,
          itemCount: res.data.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(res.data.length, parentItemCount),
          hasChildren: true,
          itemType: itemType.Sex
        });
      }
    });

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    query = ''
  }) => {
    const ages = [[1, 13], [14, 18], [19, 63], [64, 999]];
    const sexCodeIds = [44, 45];
    const data = [];

    for (let i = 0; i < ages.length; i++) {
      const [minAge, maxAge] = ages[i];
      const genderData = [];

      for (let n = 0; n < sexCodeIds.length; n++) {
        const sexCodeId = sexCodeIds[n];
        const res = await api.fetchStatisticsInterviewForChart({
          projectId,
          projectSiteId,
          parentItemCount,
          odataQuery: `&$select=subjectId&$filter=${odata.between(
            'age',
            minAge,
            maxAge
          )} and ${odata.equal('SexcodeId', sexCodeId)}`
        });

        if (res && res.data) {
          genderData.push({
            id: minAge,
            label: `${minAge === 1 ? '' : minAge} ~ ${
              maxAge === 999 ? '' : maxAge
            }`,
            itemCount: res.data.length,
            targetCount: parentItemCount,
            rate: util.calcPercentage(res.data.length, parentItemCount),
            hasChildren: false,
            itemType: itemType.Sex,
            name: _.find(items, { key: sexCodeIds[n] }).text
          });
        }
      }

      data.push(genderData);
    }

    return data;
  };

  return { build, buildForChart };
};

export default useAge;
