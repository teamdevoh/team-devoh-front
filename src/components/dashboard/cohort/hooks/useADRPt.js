import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';

const useADRPt = projectId => {
  const util = helpers.util;

  const generateData = async ({
    projectSiteId,
    parentItemCount,
    aesocKey,
    fetch,
    query = ''
  }) => {
    const data = [];

    const res = await fetch({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      let list = res.data;

      if (!!aesocKey) {
        list = list.filter(({ aesoc }) => {
          aesoc =
            aesoc === null || aesoc.trim() === ''
              ? 'no name'
              : aesoc.trim().toLocaleLowerCase();

          return aesoc === aesocKey;
        });
      }

      const itemGroup = _.groupBy(list, args => {
        return args['aedecod'] === null || args['aedecod'].trim() === ''
          ? 'no name'
          : args['aedecod'].trim().toLocaleLowerCase();
      });

      for (let key in itemGroup) {
        if (itemGroup.hasOwnProperty(key)) {
          const curItems = itemGroup[key];
          const name = key === 'no name' ? 'no name' : curItems[0]['aedecod'];

          data.push({
            name,
            itemCount: curItems.length,
            targetCount: parentItemCount,
            rate: util.calcPercentage(curItems.length, parentItemCount),
            itemType: itemType.AdrName,
            aesocKey,
            aedecodKey: key,
            hasChildren: true
          });
        }
      }
    }

    return data;
  };

  const build = async ({
    projectSiteId,
    parentItemCount,
    aesocKey,
    query = ''
  }) => {
    const data = await generateData({
      projectSiteId,
      parentItemCount,
      aesocKey,
      fetch: api.fetchStatisticsSubjectsADR
    });

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({
    projectSiteId,
    parentItemCount,
    aesocKey,
    query = ''
  }) => {
    return await generateData({
      projectSiteId,
      parentItemCount,
      aesocKey,
      fetch: api.fetchStatisticsSubjectsADRForChart
    });
  };

  return { build, buildForChart };
};

export default useADRPt;
