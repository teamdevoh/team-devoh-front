import api from '../api';
import helpers from '../../../../helpers';

const useEosinophil = projectId => {
  const standard = 500;

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];

    let normal = 0;
    let eosinophilia = 0;

    const res = await api.fetchStatisticsSubjectsLbhem({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,eos,wbc&$filter=${odata.notEqual(
        'eos',
        null
      )} and ${odata.notEqual('wbc', null)}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let minEos = null;
          let wbcVal = null;

          curSubjectItem.forEach(lbhem => {
            const { eos = null, wbc = null } = lbhem;
            if (
              eos !== null &&
              eos.trim() !== '' &&
              !isNaN(eos) &&
              wbc !== null &&
              wbc.trim() !== '' &&
              !isNaN(wbc)
            ) {
              if (minEos === null || minEos > Number(eos)) {
                minEos = Number(eos);
                wbcVal = Number(wbc);
              }
            }
          });

          if (minEos !== null) {
            const eosin = wbcVal * minEos * 10;

            if (eosin < standard) {
              normal++;
            } else if (eosin >= standard) {
              eosinophilia++;
            }
          }
        }
      }

      data = [
        {
          name: 'Normal(<500)',
          itemCount: normal,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Eosinophilia (>=500)',
          itemCount: eosinophilia,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useEosinophil;
