import api from '../api';
import helpers from '../../../../helpers';

const useALT = projectId => {
  const standard = [6, 27, 81, 135];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];
    let decreasedCnt = 0;
    let normalCnt = 0;
    let increasedCnt = 0;
    let ulnx3Cnt = 0;
    let ulnx5Cnt = 0;

    const res = await api.fetchStatisticsSubjectsLbch({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,alt&$filter=${odata.notEqual(
        'alt',
        null
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let maxAlt = null;

          curSubjectItem.forEach(lbch => {
            const { alt = null } = lbch;
            if (alt !== null && alt.trim() !== '' && !isNaN(alt)) {
              if (maxAlt === null || maxAlt < Number(alt)) {
                maxAlt = Number(alt);
              }
            }
          });

          if (maxAlt !== null) {
            if (maxAlt < standard[0]) {
              decreasedCnt++;
            } else if (maxAlt >= standard[0] && maxAlt <= standard[1]) {
              normalCnt++;
            } else if (maxAlt > standard[1] && maxAlt < standard[2]) {
              increasedCnt++;
            } else if (maxAlt >= standard[2] && maxAlt < standard[3]) {
              ulnx3Cnt++;
            } else if (maxAlt >= standard[3]) {
              ulnx5Cnt++;
            }
          }
        }
      }

      data = [
        {
          name: 'ALT decreased(<6)',
          itemCount: decreasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Normal(=>6, =<27U/L)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'ALT increased(>27U/L)',
          itemCount: increasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'ALT>ULNx3 (>= 81U/L)',
          itemCount: ulnx3Cnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'ALT>ULNx5 (>= 135U/L)',
          itemCount: ulnx5Cnt,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useALT;
