import api from '../api';
import helpers from '../../../../helpers';

const useSubjectStatusSite = projectId => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const generateData = ({ data, parentItemCount }) => {
    const g = _.groupBy(data, 'siteName');
    const newData = [];
    Object.keys(g).forEach(key => {
      newData.push({
        name: key,
        itemCount: g[key].length,
        targetCount: parentItemCount,
        rate: util.calcPercentage(g[key].length, parentItemCount),
        hasChildren: false
      });
    });

    return newData;
  };

  const build = async ({ projectSiteId, parentItemCount, metaData }) => {
    const res = await api.fetchStatisticsSubjectStatus({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=SiteName&$filter=${odata.equal(
        'SubjectStatusCode',
        metaData.statusCode
      )}`
    });

    if (res && res.data) {
      return generateData({ data: res.data, parentItemCount });
    }
  };

  return { build };
};

export default useSubjectStatusSite;
