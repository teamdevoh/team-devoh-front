import api from '../api';
import type from '../itemType';

const useLBCH = projectId => {
  const menuItems = [
    {
      name: 'Albumin',
      key: 'alb',
      count: 0,
      itemType: type.Albumin
    },
    {
      name: 'Creatinine',
      key: 'cr',
      count: 0,
      itemType: type.Creatinine
    },
    {
      name: 'eGFR (CKD-EPI)',
      key: 'calculator',
      count: null,
      itemType: type.eGFR
    },
    {
      name: 'Total Bilirubin',
      key: 'tbil',
      count: 0,
      itemType: type.TotalBilirubin
    },
    {
      name: 'AST (SGOT)',
      key: 'ast',
      count: 0,
      itemType: type.AST
    },
    {
      name: 'ALT (SGPT)',
      key: 'alt',
      count: 0,
      itemType: type.ALT
    }
  ];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const data = [];

    const res = await api.fetchStatisticsSubjectsLbch({
      projectId,
      projectSiteId,
      parentItemCount
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];

          menuItems.forEach(item => {
            const { key, itemType } = item;

            for (let i = 0; i < curSubjectItem.length; i++) {
              const curLbch = curSubjectItem[i][key] || null;

              if (itemType === type.eGFR) {
                // eGfr 계산을 위한 cr(숫자 아닌경우도), 나이, 성별, 인종이 null 이면 제외
                const { cr, age, sexcodeId, ethniccodeId } = curSubjectItem[i];
                if (
                  age !== null &&
                  sexcodeId !== null &&
                  ethniccodeId !== null &&
                  cr !== null &&
                  cr.trim() !== '' &&
                  !isNaN(cr)
                ) {
                  item.count++;
                  break;
                }
              } else {
                if (
                  curLbch !== null &&
                  curLbch.trim() !== '' &&
                  !isNaN(curLbch)
                ) {
                  item.count++;
                  break;
                }
              }
            }
          });
        }
      }

      menuItems.forEach(item => {
        data.push({
          name: item.name,
          itemCount: item.count,
          targetCount: Object.keys(g).length,
          hasChildren: true,
          itemType: item.itemType
        });
      });
    }

    return data;
  };

  return {
    build
  };
};

export default useLBCH;
