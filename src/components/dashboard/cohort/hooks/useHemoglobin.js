import api from '../api';
import helpers from '../../../../helpers';

const useHemoglobin = projectId => {
  const standard = [14, 18];

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];

    let decreasedCnt = 0;
    let normalCnt = 0;
    let increasedCnt = 0;

    const res = await api.fetchStatisticsSubjectsLbhem({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,hgb&$filter=${odata.notEqual(
        'hgb',
        null
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let minHgb = null;

          curSubjectItem.forEach(lbhem => {
            const { hgb = null } = lbhem;
            if (hgb !== null && hgb.trim() !== '' && !isNaN(hgb)) {
              if (minHgb === null || minHgb > Number(hgb)) {
                minHgb = Number(hgb);
              }
            }
          });

          if (minHgb !== null) {
            if (minHgb <= standard[0]) {
              decreasedCnt++;
            } else if (minHgb > standard[0] && minHgb <= standard[1]) {
              normalCnt++;
            } else if (minHgb > standard[1]) {
              increasedCnt++;
            }
          }
        }
      }

      data = [
        {
          name: 'Hemoglobin decreased(<= 14g/㎗)',
          itemCount: decreasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Normal(>14, <=18g/㎗)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Hemoglobin increased(>18g/㎗)',
          itemCount: increasedCnt,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useHemoglobin;
