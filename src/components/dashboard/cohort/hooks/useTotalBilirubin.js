import api from '../api';
import helpers from '../../../../helpers';

const useTotalBilirubin = projectId => {
  const standard = 1.2;

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];
    let normalCnt = 0;
    let hypoalbuminemiaCnt = 0;

    const res = await api.fetchStatisticsSubjectsLbch({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,tbil&$filter=${odata.notEqual(
        'tbil',
        null
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let maxTbil = null;

          curSubjectItem.forEach(lbch => {
            const { tbil = null } = lbch;
            if (tbil !== null && tbil.trim() !== '' && !isNaN(tbil)) {
              if (maxTbil === null || maxTbil < Number(tbil)) {
                maxTbil = Number(tbil);
              }
            }
          });

          if (maxTbil !== null) {
            if (maxTbil <= standard) {
              normalCnt++;
            } else if (maxTbil > standard) {
              hypoalbuminemiaCnt++;
            }
          }
        }
      }

      data = [
        {
          name: 'Normal(<=1.2mg/dL)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Hyperbilirubinemia(>1.2mg/dL)',
          itemCount: hypoalbuminemiaCnt,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useTotalBilirubin;
