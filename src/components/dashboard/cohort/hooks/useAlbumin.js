import api from '../api';
import helpers from '../../../../helpers';

const useAlbumin = projectId => {
  const standard = 3.8;

  const build = async ({ projectSiteId, parentItemCount, query = '' }) => {
    const odata = helpers.oDataBuilder;
    let data = [];
    let hypoalbuminemiaCnt = 0;
    let normalCnt = 0;

    const res = await api.fetchStatisticsSubjectsLbch({
      projectId,
      projectSiteId,
      parentItemCount,
      odataQuery: `&$select=subjectId,alb&$filter=${odata.notEqual(
        'alb',
        null
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'subjectId');

      for (let subjectId in g) {
        if (g.hasOwnProperty(subjectId)) {
          const curSubjectItem = g[subjectId];
          let maxAlb = null;

          curSubjectItem.forEach(lbch => {
            const { alb = null } = lbch;
            if (alb !== null && alb.trim() !== '' && !isNaN(alb)) {
              if (maxAlb === null || maxAlb < Number(alb)) {
                maxAlb = Number(alb);
              }
            }
          });

          if (maxAlb !== null) {
            if (maxAlb < standard) {
              hypoalbuminemiaCnt++;
            } else if (maxAlb >= standard) {
              normalCnt++;
            }
          }
        }
      }

      data = [
        {
          name: 'Hypoalbuminemia(<3.8g/dL)',
          itemCount: hypoalbuminemiaCnt,
          targetCount: parentItemCount,
          hasChildren: false
        },
        {
          name: 'Normal(>=3.8g/dL)',
          itemCount: normalCnt,
          targetCount: parentItemCount,
          hasChildren: false
        }
      ];
    }

    return data;
  };

  return {
    build
  };
};

export default useAlbumin;
