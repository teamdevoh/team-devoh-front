import React from 'react';
import { Tab } from 'semantic-ui-react';
import type from './itemType';
import { useFormatMessage } from '../../../hooks';

import CPMTbSiteChart from './CPMTbSiteChart';
import CPMTbInveChart from './CPMTbInveChart';

const CPMTbSubjectChart = props => {
  const { projectId, id, targetCount, chartOptions } = props;
  const t = useFormatMessage();

  const panes = [
    {
      menuItem: t('project.site'),
      type: type.Site,
      render: () => (
        <Tab.Pane>
          <CPMTbSiteChart
            projectId={projectId}
            targetCount={targetCount}
            chartOptions={chartOptions}
            id={id}
            {...props}
          />
        </Tab.Pane>
      )
    },
    {
      menuItem: t('investigator'),
      type: type.Inve,
      render: () => (
        <Tab.Pane>
          <CPMTbInveChart
            projectId={projectId}
            targetCount={targetCount}
            chartOptions={chartOptions}
            id={id}
            {...props}
          />
        </Tab.Pane>
      )
    }
  ];

  return <Tab panes={panes} />;
};

export default CPMTbSubjectChart;
