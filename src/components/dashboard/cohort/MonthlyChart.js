import React, { Fragment, useState, useEffect } from 'react';
import { Grid, Dimmer, Loader, Responsive } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import { ProjectSiteForChart } from '../../collection/Selector';
import { generateChartData } from './ChartContainer';
import useStatisticsForChart from './hooks/useStatisticsForChart';
import type from './itemType';
import ChartDateFilter from './ChartDateFilter';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const MonthlyChart = props => {
  const { projectId, id, targetCount, projectSiteId, period } = props;
  const [data, setData] = useState();
  const [totalItemCount, setTotalItemCount] = useState(targetCount);
  const [filter, setFilter] = useState({
    projectSiteId,
    period
  });
  const [loading, setLoading] = useState(false);
  const { fetchItems } = useStatisticsForChart(projectId);

  const fetch = async () => {
    setLoading(true);
    const param = {
      type: type.Monthly,
      projectSiteId: filter.projectSiteId,
      key: id,
      parentItemCount: totalItemCount,
      parentPeriod: filter.period
    };

    const newData = await fetchItems(param);

    const { data, totalItemCount: newTotalItemCount } = generateChartData({
      items: newData,
      doSort: false
    });

    setData(data);
    setTotalItemCount(newTotalItemCount);
    setLoading(false);
  };

  useEffect(
    () => {
      fetch();
    },
    [filter]
  );

  const handleDateFilterChange = period => {
    setFilter({
      ...filter,
      period
    });
  };

  const handleProejctSiteChange = (e, { name, value }) => {
    setFilter({
      ...filter,
      projectSiteId: value
    });
  };

  useEffect(
    () => {
      setFilter({
        ...filter,
        projectSiteId: props.projectSiteId
      });
    },
    [props.projectSiteId]
  );

  return (
    <Fragment>
      <div style={{ marginBottom: '20px' }}>
        <Grid columns={isMobile ? null : 'equal'}>
          <Grid.Column width={isMobile ? '16' : '8'}>
            <ProjectSiteForChart
              value={filter.projectSiteId}
              onChange={handleProejctSiteChange}
              projectId={projectId}
            />
          </Grid.Column>
          <Grid.Column>
            <ChartDateFilter
              period={period}
              onChange={handleDateFilterChange}
            />
          </Grid.Column>
        </Grid>
      </div>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <CohortChart data={data} title={filter.period} />
    </Fragment>
  );
};

export default MonthlyChart;
