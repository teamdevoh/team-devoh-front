import React, { Fragment, useCallback, useEffect, useState } from 'react';

import { Table, Loader, Button } from 'semantic-ui-react';
import RowWrapper from './RowWrapper';
import Name from './Name';
import type from './itemType';

const hasChart = {
  'cPMTb Subject': true,
  //'Subject Status': true,
  'Treatment Outcome': true,
  Weekly: true,
  Monthly: true,
  Gender: true,
  Age: true,
  Ethnicity: true,
  Smoke: true,
  BMI: true,
  'Initial Diagnosis': true,
  'Final Diagnosis': true,
  'TB Location': true,
  'Initial or Re-treatment': true,
  Comorbidity: true,
  'TDM TB Drug': true,
  Regimen: true,
  'MIC Result': true,
  Genotyping: true,
  Samples: true,
  'TB Drug Sample': true,
  ADR: true,
  LBCH: true,
  LBHEM: true
};

const Row = props => {
  const {
    indent,
    statistics,
    projectSiteId,
    id,
    index,
    name,
    period,
    itemCount,
    targetCount,
    hasChildren,
    parentName,
    itemType,
    activeRowKey,
    onRowClick,
    onChartClick
  } = props;
  const key = `${index}${parentName}${name}`;
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [items, setItems] = useState(null);

  const handleClick = useCallback(
    async () => {
      if (hasChildren) {
        setVisible(!visible);
        setLoading(true);
        if (items === null) {
          const data = await statistics.fetchItems({
            ...props,
            type: itemType,
            projectSiteId: projectSiteId,
            key: id,
            parentItemCount:
              itemType === type.Week || itemType === type.Monthly
                ? targetCount
                : itemCount,
            parentPeriod: period
          });
          setItems(data);
        }
        setLoading(false);
      }
      onRowClick(key);
    },
    [
      visible,
      items,
      id,
      itemType,
      projectSiteId,
      itemCount,
      period,
      activeRowKey,
      statistics.fetchItems
    ]
  );

  useEffect(
    () => {
      setVisible(false);
      setItems(null);
    },
    [projectSiteId]
  );

  const breadcrumbs = (parentName ? `${parentName} > ` : '') + name;

  const getHasChart = useCallback(
    () => {
      let has = hasChart[name];

      if (name === 'ADR' && id !== 22) {
        has = false;
      }
      return has;
    },
    [name, id]
  );

  return (
    <Fragment>
      <Table.Row active={activeRowKey === key ? true : false}>
        <Table.Cell>
          <Name
            onClick={handleClick}
            hasChildren={hasChildren}
            showChildren={visible}
            indent={indent}
            name={name}
            parentName={parentName}
          />
          <Loader inline="centered" active={loading} />
        </Table.Cell>
        <Table.Cell textAlign="center">{period}</Table.Cell>
        <Table.Cell textAlign="center" style={{ color: 'red' }}>
          {itemCount}
        </Table.Cell>
        <Table.Cell textAlign="center">
          {getHasChart() && <Button icon="chart line" onClick={onChartClick} />}
        </Table.Cell>
      </Table.Row>
      {visible && (
        <RowWrapper
          indent={indent + 1}
          projectSiteId={projectSiteId}
          parentName={breadcrumbs}
          items={items}
          activeRowKey={activeRowKey}
          onRowClick={onRowClick}
        />
      )}
    </Fragment>
  );
};

export default Row;
