import React, { Fragment, useEffect, useState } from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import type from './itemType';
import { generateChartData } from './ChartContainer';
import { ProjectSiteForChart } from '../../collection/Selector';
import { useTreatmentOutcome } from './hooks';

const TreatmentOutcomeChart = props => {
  const { projectId, id, targetCount, chartOptions } = props;
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const { caseconCodesItems, build } = useTreatmentOutcome(projectId);

  const [projectSiteId, setProjectSiteId] = useState(props.projectSiteId);

  const handleProejctSiteChange = (e, { name, value }) => {
    setProjectSiteId(value);
  };

  const fetch = async ({ id, targetCount, projectSiteId }) => {
    setLoading(true);
    const param = {
      projectSiteId: projectSiteId,
      parentItemCount: targetCount
    };

    const newData = await build(param);

    const { data } = generateChartData({ items: newData });

    setData(data);
    setLoading(false);
  };

  useEffect(
    () => {
      if (caseconCodesItems.length > 0) {
        fetch({
          id,
          targetCount,
          projectSiteId
        });
      }
    },
    [id, targetCount, projectSiteId, caseconCodesItems]
  );

  useEffect(
    () => {
      setProjectSiteId(props.projectSiteId);
    },
    [props.projectSiteId]
  );

  return (
    <Fragment>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <div style={{ marginBottom: '20px' }}>
        <ProjectSiteForChart
          value={projectSiteId}
          onChange={handleProejctSiteChange}
          projectId={projectId}
        />
      </div>
      <CohortChart data={data} options={chartOptions[type.TreatmentOutcome]} />
    </Fragment>
  );
};

export default TreatmentOutcomeChart;
