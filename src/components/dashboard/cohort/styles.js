import styled from 'styled-components';

const StyledBreadcrumbs = styled.div`
  margin-left: 1.5em;
  font-size: 0.8em;
  font-style: oblique;
`;

const StyledName = styled.div`
  cursor: pointer;
  word-break: break-all;
  ${props => {
    return {
      'margin-left': props.indent + 'em'
    };
  }};
`;

export { StyledBreadcrumbs, StyledName };
