import React, { Fragment, useEffect, useState } from 'react';
import { Bar } from 'react-chartjs-2';
import helpers from '../../../helpers';

const getBasicOption = ({ isMobile, legend = false }) => ({
  legend: { display: legend },
  scales: {
    xAxes: [
      {
        ticks: {
          autoSkip: false
        }
      }
    ],
    yAxes: [
      {
        // ticks: {
        //   stepSize: isMobile ? null : 10
        // }
      }
    ]
  }
});

const ChartContainer = props => {
  const { data, options: initOption, title, redraw = false } = props;
  const isMobile = helpers.util.isMobile();
  const basicOption = getBasicOption({ isMobile, ...props });
  const [options, setOptions] = useState({
    ...basicOption
  });

  useEffect(
    () => {
      let newOptions = {
        ...basicOption
      };
      if (!!title) {
        newOptions = {
          ...newOptions,
          title: {
            display: true,
            fontSize: 14,
            fontStyle: 'bold',
            text: title
          }
        };
      }
      if (initOption) {
        newOptions = {
          ...newOptions,
          ...initOption
        };
      }

      setOptions({
        ...newOptions
      });
    },
    [title, initOption]
  );

  return (
    <Fragment>
      {data && (
        <Bar
          data={data}
          width={100}
          height={50}
          options={options}
          redraw={redraw}
          // redraw={true}
        />
      )}
    </Fragment>
  );
};

export default ChartContainer;
