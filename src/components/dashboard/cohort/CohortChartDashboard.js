import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { chartOptionByItemType } from './RowWrapper';
import { useItemList } from './hooks';
import type from './itemType';
import WeeklyChart from './WeeklyChart';
import MonthlyChart from './MonthlyChart';
import CPMTbSiteChart from './CPMTbSiteChart';
import CPMTbInveChart from './CPMTbInveChart';
import TreatmentOutcomeChart from './TreatmentOutcomeChart';
import AdrChart from './AdrChart';
import { ProjectSite } from '../../collection/Selector';
import { Dimmer, Loader } from 'semantic-ui-react';
import ActivityStep from '../../collection/ActivityStep';
import collectionApi from '../../collection/api';

const Root = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
`;

const ChartWrap = styled.div`
  display: flex;

  @media screen and (max-width: 768px) {
    & {
      flex-direction: column;
    }

    & > div {
      border: none !important;
    }
  }

  & > div {
    flex: 1;
    padding: 24px;
    position: relative;
    border-color: #d4d4d5 !important;
    border-bottom: 1px solid;
    border-right: 1px solid;

    & :last-child {
      border-right: none;
    }
  }

  &.last-chart-wrap > div {
    border-bottom: none;
  }
`;

const SubTitle = styled.label`
  display: inline-block;
  font-weight: 800;
  margin-bottom: 20px;
`;

const CohortChartDashboard = () => {
  const { projectId } = useParams();
  const [projectSiteId, setProjectSiteId] = useState(0);
  const { items: statistics, loading } = useItemList({
    projectId,
    projectSiteId,
    odataQuery: `&$filter=name eq 'Weekly' or name eq 'Monthly'`
  });
  const [weeklyPeriod, setWeeklyPeriod] = useState('');
  const [monthlyPeriod, setMonthlyPeriod] = useState('');
  const [statusCount, setStatusCount] = useState({
    complete: 0,
    dropout: 0,
    ongoing: 0,
    register: 0
  });

  const handleProejctSiteChange = (e, { name, value }) => {
    setProjectSiteId(value);
  };

  useEffect(
    () => {
      if (statistics.length > 0 && projectSiteId === 0) {
        for (let i = 0; i < statistics.length; i++) {
          const { name, period } = statistics[i];
          if (name === 'Weekly') {
            setWeeklyPeriod(period);
          } else if (name === 'Monthly') {
            setMonthlyPeriod(period);
          }
        }
      }
    },
    [statistics, projectSiteId]
  );

  useEffect(
    () => {
      const fetch = async () => {
        const res = await collectionApi.fetchSubjectActivitiesBySort({
          projectId,
          projectSiteId,
          subjectStatusCode: 0,
          search: null,
          order: 'asc',
          ongoingMonths: null,
          caseConMonths: null
        });

        if (res.data.countOfSubjectStatus) {
          setStatusCount(res.data.countOfSubjectStatus);
        }
      };
      fetch();
    },
    [projectId, projectSiteId]
  );

  return (
    <Root>
      <ActivityStep statusCount={statusCount} />
      <ProjectSite
        value={projectSiteId}
        onChange={handleProejctSiteChange}
        projectId={projectId}
        style={{
          zIndex: 1100
        }}
      />

      <ChartWrap>
        <div>
          <SubTitle>Weekly</SubTitle>
          {weeklyPeriod ? (
            <WeeklyChart
              projectId={projectId}
              projectSiteId={projectSiteId}
              chartOptions={chartOptionByItemType}
              period={weeklyPeriod}
            />
          ) : (
            <Dimmer active={loading} inverted>
              <Loader size="big">Loading</Loader>
            </Dimmer>
          )}
        </div>
        <div>
          <SubTitle>Monthly</SubTitle>
          {monthlyPeriod ? (
            <MonthlyChart
              projectId={projectId}
              projectSiteId={projectSiteId}
              chartOptions={chartOptionByItemType}
              period={monthlyPeriod}
            />
          ) : (
            <Dimmer active={loading} inverted>
              <Loader size="big">Loading</Loader>
            </Dimmer>
          )}
        </div>
      </ChartWrap>
      <ChartWrap>
        <div>
          <SubTitle>Site</SubTitle>
          {monthlyPeriod ? (
            <CPMTbSiteChart
              projectId={projectId}
              targetCount={1}
              projectSiteId={projectSiteId}
              period={monthlyPeriod}
              chartOptions={chartOptionByItemType}
            />
          ) : (
            <Dimmer active={loading} inverted>
              <Loader size="big">Loading</Loader>
            </Dimmer>
          )}
        </div>
        <div>
          <SubTitle>Investigator</SubTitle>
          {monthlyPeriod ? (
            <CPMTbInveChart
              projectId={projectId}
              targetCount={1}
              projectSiteId={projectSiteId}
              period={monthlyPeriod}
              chartOptions={chartOptionByItemType}
            />
          ) : (
            <Dimmer active={loading} inverted>
              <Loader size="big">Loading</Loader>
            </Dimmer>
          )}
        </div>
      </ChartWrap>
      <ChartWrap className="last-chart-wrap">
        <div>
          <SubTitle>Treatment Outcome</SubTitle>
          <TreatmentOutcomeChart
            projectId={projectId}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
          />
        </div>
        <div>
          <SubTitle>ADR</SubTitle>
          <AdrChart
            projectId={projectId}
            targetCount={1}
            projectSiteId={projectSiteId}
            chartOptions={chartOptionByItemType}
            itemType={type.Adr}
          />
        </div>
      </ChartWrap>
    </Root>
  );
};

export default CohortChartDashboard;
