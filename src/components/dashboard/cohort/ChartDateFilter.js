import React, { useState, useEffect } from 'react';
import { Dropdown, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { useFormatMessage } from '../../../hooks';

const ChartDateFilter = ({ period, onChange }) => {
  const t = useFormatMessage();
  const [options, setOptions] = useState([]);
  const [initStartendDate, setinitStartendDate] = useState({
    start: '',
    end: ''
  });
  const [filter, setFilter] = useState({
    period
  });
  const [error, setError] = useState(false);

  useEffect(
    () => {
      // generate options
      const date = period.split(' ~ ');
      const minDate = date[0];
      const maxDate = date[1];
      const start = new Date(moment(minDate).startOf('month'));
      const end = new Date(moment(maxDate).endOf('month'));

      const diffMonth = Math.ceil(moment(end).diff(start, 'months', true));

      const generateOptionsText = date => {
        const yyyymm = moment(date)
          .format('YYYY-MM-DD')
          .substring(0, 7);
        return { text: yyyymm, value: yyyymm };
      };

      let curDate = start;
      const options = [{ text: '-', value: 0 }, generateOptionsText(curDate)];

      for (let i = 0; i < diffMonth; i++) {
        curDate = new Date(curDate.setMonth(curDate.getMonth() + 1));

        if (curDate > end) {
          break;
        }
        options.push(generateOptionsText(curDate));
      }

      setinitStartendDate({
        ...initStartendDate,
        start: date[0],
        end: date[1]
      });
      setOptions(options);
    },
    [period]
  );

  const handleDateFilterChange = (e, { name, value }) => {
    setError(false);
    let { period } = filter;
    const date = period.split(' ~ ');
    let start = date[0];
    let end = date[1];

    const selectedDate = `${value}-01`;

    if (name === 'startDate') {
      start =
        initStartendDate.start > selectedDate
          ? initStartendDate.start
          : selectedDate;
    } else if (name === 'endDate') {
      end =
        value === 0
          ? initStartendDate.end
          : moment(new Date(`${value}-01`))
              .endOf('month')
              .format('YYYY-MM-DD');
      end = initStartendDate.end < end ? initStartendDate.end : end;
    }

    if (start > end) {
      setError(true);
    } else {
      period = `${start} ~ ${end}`;
      setFilter({
        ...filter,
        period
      });
      onChange(period);
    }
  };

  return (
    <Popup
      position="top center"
      open={error}
      content={t('check.start.end.date')}
      trigger={
        <div
          style={{
            display: 'flex',
            flexWrap: 'wrap',
            alignItems: 'center'
          }}
        >
          <Dropdown
            name="startDate"
            style={{
              minWidth: '140px'
            }}
            placeholder={initStartendDate.start.substr(0, 7)}
            search
            selection
            options={options}
            onChange={handleDateFilterChange}
            error={error}
          />
          <span style={{ padding: '0 8px' }}>~</span>
          <Dropdown
            name="endDate"
            style={{
              minWidth: '140px'
            }}
            placeholder={initStartendDate.end.substr(0, 7)}
            search
            selection
            options={options}
            onChange={handleDateFilterChange}
            error={error}
          />
        </div>
      }
    />
  );
};

export default ChartDateFilter;
