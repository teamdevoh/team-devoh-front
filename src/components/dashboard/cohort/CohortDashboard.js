import React, { useCallback, useState } from 'react';
import { Table, Grid } from 'semantic-ui-react';
import RowWrapper from './RowWrapper';
import { ProjectSite } from '../../collection/Selector';
import { useItemList } from './hooks';
import { useParams } from 'react-router-dom';
import { useFormatMessage } from '../../../hooks';

const CohortDashboard = () => {
  const [projectSiteId, setProjectSiteId] = useState(0);
  const { projectId } = useParams();
  const { items } = useItemList({ projectId, projectSiteId });
  const [activeRowKey, setActiveRowKey] = useState('');
  const t = useFormatMessage();

  const handleSiteChange = useCallback(
    (event, data) => {
      setProjectSiteId(data.value);
    },
    [projectSiteId]
  );

  const handleRowClick = useCallback(key => {
    setActiveRowKey(key);
  }, []);

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column mobile="sixteen" largeScreen="five">
          <ProjectSite
            value={projectSiteId}
            onChange={handleSiteChange}
            projectId={projectId}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Table celled selectable compact="very" size="small">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell textAlign="center">
                  {t('item')}
                </Table.HeaderCell>
                <Table.HeaderCell textAlign="center" width="three">
                  {t('cohort.dashboard.period')}
                </Table.HeaderCell>
                <Table.HeaderCell textAlign="center" width="one">
                  {t('cohort.dashboard.subjects')}
                </Table.HeaderCell>
                <Table.HeaderCell textAlign="center" width="one">
                  Chart
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <RowWrapper
                indent={0}
                projectSiteId={projectSiteId}
                items={items}
                activeRowKey={activeRowKey}
                onRowClick={handleRowClick}
              />
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default CohortDashboard;
