import React, { Fragment, useState, useEffect } from 'react';
import { Dimmer, Loader, Tab } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import { generateChartData } from './ChartContainer';
import type from './itemType';
import {
  useAlbumin,
  useCreatinine,
  useEgfr,
  useTotalBilirubin,
  useAST,
  useALT
} from './hooks';

const LbchChart = props => {
  const { projectId, id, targetCount, projectSiteId, chartOptions } = props;
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [itemType, setItemType] = useState(type.Albumin);
  const albumin = useAlbumin(projectId);
  const creatinine = useCreatinine(projectId);
  const egfr = useEgfr(projectId);
  const totalBilirubin = useTotalBilirubin(projectId);
  const ast = useAST(projectId);
  const alt = useALT(projectId);

  const fetch = async () => {
    setLoading(true);
    let newData = [];
    const param = {
      type: itemType,
      projectSiteId,
      key: id,
      parentItemCount: targetCount
    };

    if (itemType === type.Albumin) {
      newData = await albumin.build(param);
    }

    if (itemType === type.Creatinine) {
      newData = await creatinine.build(param);
    }

    if (itemType === type.eGFR) {
      newData = await egfr.build(param);
    }

    if (itemType === type.TotalBilirubin) {
      newData = await totalBilirubin.build(param);
    }

    if (itemType === type.AST) {
      newData = await ast.build(param);
    }

    if (itemType === type.ALT) {
      newData = await alt.build(param);
    }

    const { data } = generateChartData({ items: newData });
    setData(data);
    setLoading(false);
  };

  useEffect(
    () => {
      fetch();
    },
    [projectSiteId, itemType]
  );

  const handleTabChange = (e, { activeIndex, panes }) => {
    setItemType(panes[activeIndex].type);
  };

  const panes = [
    {
      menuItem: 'Albumin',
      type: type.Albumin,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.Albumin]} />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'Creatinine',
      type: type.Creatinine,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.Creatinine]} />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'eGFR (CKD-EPI)',
      type: type.eGFR,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.eGFR]} />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'Total Bilirubin',
      type: type.TotalBilirubin,
      render: () => (
        <Tab.Pane>
          <CohortChart
            data={data}
            options={chartOptions[type.TotalBilirubin]}
          />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'AST (SGOT)',
      type: type.AST,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.AST]} />
        </Tab.Pane>
      )
    },
    {
      menuItem: 'ALT (SGPT)',
      type: type.ALT,
      render: () => (
        <Tab.Pane>
          <CohortChart data={data} options={chartOptions[type.ALT]} />
        </Tab.Pane>
      )
    }
  ];

  return (
    <Fragment>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <Tab panes={panes} onTabChange={handleTabChange} />
    </Fragment>
  );
};

export default LbchChart;
