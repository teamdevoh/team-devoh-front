import React from 'react';
import { Container, Grid, Label } from 'semantic-ui-react';
import helpers from '../../../helpers';
import { useFormatMessage } from '../../../hooks';
import { StyledB } from '../../activity/styles';
import { DataGrid } from '../../data-grid';
import { StyledCSVExport } from '../../export-tdm/styles';
import useADRList from './hooks/useADRList';

const AdrList = ({ projectId, projectSiteId }) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useADRList({ projectId, projectSiteId });

  const columns = [
    {
      key: 'siteName',
      name: t('project.site')
    },
    {
      key: 'subjectNo',
      name: 'Subject',
      width: 85,
      formatter: ({ value, row }) => `${row.subjectPrefix || ''}${value}`
    },
    { key: 'aesoc', name: 'SOC' },
    {
      key: 'aedecod',
      name: 'PT'
    },
    {
      key: 'adrName',
      name: 'ADR'
    },
    {
      key: 'adrOnsetDat',
      name: 'Date of Onset',
      width: 105
    },
    {
      key: 'adrOffsetDat',
      name: 'Date of Resolution',
      width: 105
    },
    {
      key: 'adrSeverity',
      name: t('activity.adr.adrSeverityCodeId')
    },
    {
      key: 'adrdrug',
      name: t('activity.adr.adrdrug')
    },
    {
      key: 'conCode',
      name: 'Response'
    }
  ];

  const handleCSVExportClick = () => {
    const fieldNames = columns.map(item => item.name);
    const fields = columns.map(item => {
      return {
        ...item,
        // label: item.name,
        value: (dataElement, field, data) => {
          return dataElement[item.key]
            ? String(dataElement[item.key]).replaceAll(',', ' ')
            : '';
        }
      };
    });

    helpers.util.exportCSV({
      data: items,
      fileName: 'ADR_List.csv',
      fields: fields,
      fieldNames: fieldNames
    });
  };

  return (
    <Grid style={{ padding: ' 1rem 0' }}>
      <Grid.Row style={{ padding: 0 }}>
        <Grid.Column>
          <div style={{ display: 'flex' }}>
            <Label size="big">
              Results
              <StyledB style={{ marginLeft: '8px' }}>{items.length}</StyledB>
            </Label>
            <StyledCSVExport
              positive
              icon="download"
              content="CSV Export"
              onClick={handleCSVExportClick}
            />
          </div>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ padding: 0 }}>
        <Grid.Column>
          <Container>
            <DataGrid
              rowNumber={{ show: true, key: 'no', name: 'No' }}
              columns={columns}
              rows={items}
              minHeight={500}
              rowKey="subjectAdrid"
              loading={loading}
            />
          </Container>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default AdrList;
