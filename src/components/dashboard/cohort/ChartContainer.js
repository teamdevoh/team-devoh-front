import React, { useEffect, useState, Fragment } from 'react';
import { Loader, Dimmer } from 'semantic-ui-react';
import type from './itemType';
import CohortChart from './CohortChart';
import { useFormatMessage } from '../../../hooks';

export const chartColor = [
  '#008299',
  '#FF6B5E',
  '#92C4FF',
  '#BCCC60',
  '#62CCAE',
  '#A693FF',
  '#FFA661'
];

const generateChartWrapper = ({ chartOptions, itemType, data }) => {
  const options = chartOptions[itemType] || false;
  let ChartComponent = null;

  switch (itemType) {
    case type.Gend:
    case type.TdmTbDrug:
    case type.Regimen:
    case type.MICResult:
    case type.SampleAnalyte:
      ChartComponent = (
        <Fragment>
          <CohortChart data={data} options={options} />
        </Fragment>
      );
      break;
    case type.Age:
    case type.Ethn:
    case type.Smok:
    case type.Bmi:
    case type.InitialDiagnosis:
    case type.FinalDiagnosis:
    case type.TBLocation:
    case type.InitiOrReTreatment:
    case type.Comorbidity:
      ChartComponent = (
        <Fragment>
          <CohortChart data={data} legend options={options} />
        </Fragment>
      );
      break;
    default:
      ChartComponent = [];
      break;
  }

  return ChartComponent;
};

export const generateChartData = ({ items, doSort = false }) => {
  let newItemCount = 0;
  if (doSort) {
    // 시간순이 아닌경우 count 순으로 정렬
    items = items.sort((a, b) => {
      // itemCount 비교 값
      return b.itemCount - a.itemCount;
    });
  }

  const labels = [];
  const data = [];
  const backgroundColor = [];
  for (let i = 0; i < items.length; i++) {
    const { name, itemCount } = items[i];
    newItemCount += itemCount;

    labels.push(!!name ? name : 'No Name');
    data.push(itemCount);
    backgroundColor.push(chartColor[i % chartColor.length]);
  }

  return {
    data: {
      labels,
      datasets: [
        {
          data,
          backgroundColor,
          metaData: items
        }
      ]
    },
    totalItemCount: newItemCount
  };
};

export const generateChartDataForGroupByGender = ({
  items,
  doSort = false
}) => {
  let data = {};
  const labels = [];
  const male = {
    label: 'MALE',
    backgroundColor: chartColor[0],
    data: []
  };
  const female = {
    label: 'FEMALE',
    backgroundColor: chartColor[1],
    data: []
  };
  const total = {
    label: 'Total',
    backgroundColor: chartColor[2],
    data: []
  };

  for (let i = 0; i < items.length; i++) {
    const [m, f] = items[i];
    const t = Number(m.itemCount) + Number(f.itemCount);

    labels.push(m.label);
    male.data.push(m.itemCount);
    female.data.push(f.itemCount);
    total.data.push(t);
  }

  if (doSort) {
    // count 높은 순으로 정렬하기
    const sortedLabels = [];
    const sortedMaleData = {
      label: 'MALE',
      backgroundColor: chartColor[0],
      data: []
    };
    const sortedFemale = {
      label: 'FEMALE',
      backgroundColor: chartColor[1],
      data: []
    };
    const sortedTotal = {
      label: 'Total',
      backgroundColor: chartColor[2],
      data: []
    };

    const mappedTotal = total.data.map((count, index) => {
      return { index, count };
    });

    mappedTotal.sort((a, b) => b.count - a.count);

    for (let i = 0; i < mappedTotal.length; i++) {
      const { index } = mappedTotal[i];

      sortedLabels.push(labels[index]);
      sortedMaleData.data.push(male.data[index]);
      sortedFemale.data.push(female.data[index]);
      sortedTotal.data.push(total.data[index]);
    }

    data = {
      labels: sortedLabels,
      datasets: [sortedMaleData, sortedFemale, sortedTotal]
    };
  } else {
    data = {
      labels,
      datasets: [male, female, total]
    };
  }

  return data;
};

const ChartContainer = props => {
  const {
    projectSiteId,
    id,
    targetCount,
    itemType,
    period,
    fetchItems,
    chartOptions
  } = props;
  const t = useFormatMessage();
  const [data, setData] = useState();
  const [totalItemCount, setTotalItemCount] = useState(props.itemCount);
  const [loading, setLoading] = useState(false);

  const fetch = async () => {
    setLoading(true);
    const param = {
      type: itemType,
      projectSiteId,
      key: id,
      parentItemCount:
        itemType === type.Week || itemType === type.Monthly
          ? targetCount
          : totalItemCount,
      parentPeriod: period
    };

    const data = await fetchItems(param);

    if (
      itemType === type.Age ||
      itemType === type.Ethn ||
      itemType === type.Smok ||
      itemType === type.Bmi ||
      itemType === type.InitiOrReTreatment
    ) {
      setData(generateChartDataForGroupByGender({ items: data }));
    } else if (
      itemType === type.InitialDiagnosis ||
      itemType === type.FinalDiagnosis ||
      itemType === type.TBLocation ||
      itemType === type.Comorbidity
    ) {
      setData(generateChartDataForGroupByGender({ items: data, doSort: true }));
    } else if (itemType === type.Regimen) {
      generateChartDataForRegimen(data);
    } else {
      const doSort = itemType !== type.Week && itemType !== type.Monthly;
      const chartData = generateChartData({ items: data, doSort });

      setData(chartData.data);
      setTotalItemCount(chartData.totalItemCount);
    }

    setLoading(false);
  };

  useEffect(
    () => {
      fetch();
    },
    [itemType, projectSiteId]
  );

  const generateChartDataForRegimen = items => {
    let newItemCount = 0;
    items = items.sort((a, b) => {
      // itemCount 비교 값
      return b.itemCount - a.itemCount;
    });

    const labels = [];
    const data = [];
    const backgroundColor = [];

    let fewDataCount = 0;

    for (let i = 0; i < items.length; i++) {
      const { name, itemCount } = items[i];
      newItemCount += itemCount;

      if (itemCount < 5) {
        fewDataCount += itemCount;
      } else {
        labels.push(!!name ? name : 'No Name');
        data.push(itemCount);
        backgroundColor.push(chartColor[i % chartColor.length]);
      }
    }

    labels.push('Etc');
    data.push(fewDataCount);
    backgroundColor.push(chartColor[items.length % chartColor.length]);

    setData({
      labels,
      datasets: [
        {
          data,
          backgroundColor,
          metaData: items
        }
      ]
    });

    setTotalItemCount(newItemCount);
  };

  return (
    <Fragment>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      {data &&
        generateChartWrapper({
          chartOptions,
          itemType,
          data,
          t
        })}
    </Fragment>
  );
};

export default ChartContainer;
