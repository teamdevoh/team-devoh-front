import React, { Fragment, useEffect, useState } from 'react';
import { Dimmer, Loader, Responsive, Grid } from 'semantic-ui-react';
import CohortChart from './CohortChart';
import useStatisticsForChart from './hooks/useStatisticsForChart';
import type from './itemType';
import { useCPMTbSubjectChart } from './hooks';
import { ProjectSiteForChart } from '../../collection/Selector';
import ChartDateFilter from './ChartDateFilter';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const CPMTbInveChart = props => {
  const {
    projectId,
    id,
    targetCount,
    projectSiteId,
    period,
    chartOptions
  } = props;
  const { fetchItems } = useStatisticsForChart(projectId);
  const { data, loading, fetch } = useCPMTbSubjectChart({ fetchItems });
  const [filter, setFilter] = useState({
    projectSiteId,
    period
  });

  const handleDateFilterChange = period => {
    setFilter({
      ...filter,
      period
    });
  };

  const handleProejctSiteChange = (e, { name, value }) => {
    setFilter({
      ...filter,
      projectSiteId: value
    });
  };

  useEffect(
    () => {
      fetch({
        id,
        targetCount,
        projectSiteId: filter.projectSiteId,
        period: filter.period,
        itemType: type.Inve
      });
    },
    [id, targetCount, filter]
  );

  useEffect(
    () => {
      setFilter({
        ...filter,
        projectSiteId: props.projectSiteId
      });
    },
    [props.projectSiteId]
  );

  return (
    <Fragment>
      <div style={{ marginBottom: '20px' }}>
        <Grid columns={isMobile ? null : 'equal'}>
          <Grid.Column width={isMobile ? '16' : '8'}>
            <ProjectSiteForChart
              value={filter.projectSiteId}
              onChange={handleProejctSiteChange}
              projectId={projectId}
            />
          </Grid.Column>
          <Grid.Column>
            <ChartDateFilter
              period={period}
              onChange={handleDateFilterChange}
            />
          </Grid.Column>
        </Grid>
      </div>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <CohortChart data={data} options={chartOptions[type.Inve]} />
    </Fragment>
  );
};

export default CPMTbInveChart;
