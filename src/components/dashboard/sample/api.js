import helpers from '../../../helpers';

const fetchSampleStatistics = ({ projectId }) => {
  const params = helpers.qs.stringify({ projectId });
  return helpers.Service.get(`api/sample/statistics?${params}`);
};

const fetchStatisticsBySampleStatus = ({
  projectId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/sample/statistics/sample?${params}${odataQuery}`
  );
};

const fetchStatisticsBySampleStatusForChart = ({
  projectId,
  parentItemCount,
  odataQuery
}) => {
  const params = helpers.qs.stringify({
    projectId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/sample/statistics/sample/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsWeeks = ({
  projectId,
  parentItemCount,
  odataQuery = '',
  targetDate
}) => {
  const params = helpers.qs.stringify({
    projectId,
    parentItemCount,
    targetDate
  });
  return helpers.Service.get(
    `api/sample/statistics/weeks?${params}${odataQuery}`
  );
};

const fetchStatisticsWeeksForChart = ({
  projectId,
  parentItemCount,
  odataQuery = '',
  targetDate
}) => {
  const params = helpers.qs.stringify({
    projectId,
    parentItemCount,
    targetDate
  });
  return helpers.Service.get(
    `api/sample/statistics/weeks/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsByRetentionPeriod = ({
  projectId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/sample/statistics/retention.period?${params}${odataQuery}`
  );
};

const fetchStatisticsByRetentionPeriodForChart = ({
  projectId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/sample/statistics/retention.period/chart?${params}${odataQuery}`
  );
};

const fetchStatisticsBySampleAliquotAnalyte = ({
  projectId,
  parentItemCount,
  odataQuery = ''
}) => {
  const params = helpers.qs.stringify({
    projectId,
    parentItemCount
  });
  return helpers.Service.get(
    `api/sample/statistics/sample/aliquot/analyte?${params}${odataQuery}`
  );
};

export default {
  fetchSampleStatistics,
  fetchStatisticsBySampleStatus,
  fetchStatisticsBySampleStatusForChart,
  fetchStatisticsWeeks,
  fetchStatisticsWeeksForChart,
  fetchStatisticsByRetentionPeriod,
  fetchStatisticsByRetentionPeriodForChart,
  fetchStatisticsBySampleAliquotAnalyte
};
