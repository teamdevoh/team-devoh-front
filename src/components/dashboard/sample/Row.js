import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Table, Loader, Button } from 'semantic-ui-react';
import RowWrapper from './RowWrapper';
import Name from '../cohort/Name';
import type from './itemType';

const hasChart = {
  'Total Sample': true,
  Weekly: true,
  Monthly: true,
  'Retention period': true
};

const Row = props => {
  const {
    indent,
    statistics,
    projectId,
    id,
    index,
    name,
    period,
    itemCount,
    targetCount,
    hasChildren,
    parentName,
    itemType,
    activeRowKey,
    onRowClick,
    onChartClick,
    onSampleExcelDownClick
  } = props;
  const key = `${index}${parentName}${name}`;
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [items, setItems] = useState(null);

  const handleClick = useCallback(
    async () => {
      if (hasChildren) {
        setVisible(!visible);
        setLoading(true);
        if (items === null) {
          const data = await statistics.fetchItems({
            ...props,
            type: itemType,
            projectId,
            key: id,
            parentItemCount:
              itemType === type.Week || itemType === type.Monthly
                ? targetCount
                : itemCount,
            parentPeriod: period
          });

          setItems(data);
        }
        setLoading(false);
      }
      onRowClick(key);
    },
    [
      visible,
      items,
      id,
      itemType,
      projectId,
      itemCount,
      period,
      activeRowKey,
      statistics.fetchItems
    ]
  );

  useEffect(
    () => {
      setVisible(false);
      setItems(null);
    },
    [projectId]
  );

  const breadcrumbs = (parentName ? `${parentName} > ` : '') + name;

  const getHasChart = useCallback(
    () => {
      const has =
        hasChart[name] || (props.metaData && !!props.metaData.hasChart);

      return has;
    },
    [name, props]
  );

  return (
    <Fragment>
      <Table.Row active={activeRowKey === key ? true : false}>
        <Table.Cell>
          <Name
            onClick={handleClick}
            hasChildren={hasChildren}
            showChildren={visible}
            indent={indent}
            name={name}
            parentName={parentName}
          />
          <Loader inline="centered" active={loading} />
        </Table.Cell>
        <Table.Cell textAlign="center">{period}</Table.Cell>
        <Table.Cell textAlign="center" style={{ color: 'red' }}>
          {itemCount}
        </Table.Cell>
        <Table.Cell textAlign="center">
          {getHasChart() && <Button icon="chart line" onClick={onChartClick} />}
          {!!props.metaData &&
            !!props.metaData.hasExcel && (
              <Button
                icon="file excel outline"
                onClick={onSampleExcelDownClick}
              />
            )}
        </Table.Cell>
      </Table.Row>
      {visible && (
        <RowWrapper
          indent={indent + 1}
          projectId={projectId}
          parentName={breadcrumbs}
          items={items}
          activeRowKey={activeRowKey}
          onRowClick={onRowClick}
        />
      )}
    </Fragment>
  );
};

export default Row;
