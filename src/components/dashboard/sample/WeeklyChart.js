import React, { Fragment, useState } from 'react';
import { ProjectsOfSamples } from '../../sample-status/Selector';
import DefaultChart from './DefaultChart';

const WeeklyChart = props => {
  const [projectId, setProjectId] = useState(props.projectId);

  const handleFilterChange = (e, { value }) => {
    setProjectId(value);
  };

  return (
    <Fragment>
      <div style={{ marginBottom: '20px' }}>
        <ProjectsOfSamples
          name="projectId"
          value={projectId}
          onChange={handleFilterChange}
          hasAll
          fluid
        />
      </div>
      <DefaultChart {...props} title={props.period} projectId={projectId} />
    </Fragment>
  );
};

export default WeeklyChart;
