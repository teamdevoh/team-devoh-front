import api from '../api';
import helpers from '../../../../helpers';
import { useCodes } from '../../../../hooks';
import { codeGroup } from '../../../../constants';

const useSampleStatus = () => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;
  const [items] = useCodes({ codeGroupId: codeGroup.SampleAliquotStatusCode });
  const sampleAliquotStatusCodes = items.concat([
    { text: 'No Data', value: [null, 0] }
  ]);

  const generateData = async ({ fetch, projectId, parentItemCount }) => {
    const data = [];

    for (let i = 0; i < sampleAliquotStatusCodes.length; i++) {
      const { value, text } = sampleAliquotStatusCodes[i];

      const res = await fetch({
        projectId,
        parentItemCount,
        odataQuery: `&$filter=${odata.equalOrequal(
          'SampleAliquotStatusCodeId',
          value
        )}`
      });

      if (res && res.data) {
        data.push({
          name: text,
          itemCount: res.data.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(res.data.length, parentItemCount),
          hasChildren: false
        });
      }
    }

    return data;
  };

  const build = async ({ projectId, parentItemCount, query = '' }) => {
    const data = await generateData({
      fetch: api.fetchStatisticsBySampleStatus,
      projectId,
      parentItemCount
    });

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({ projectId, parentItemCount, query = '' }) => {
    const data = await generateData({
      fetch: api.fetchStatisticsBySampleStatusForChart,
      projectId,
      parentItemCount
    });

    return data;
  };

  return { build, buildForChart };
};

export default useSampleStatus;
