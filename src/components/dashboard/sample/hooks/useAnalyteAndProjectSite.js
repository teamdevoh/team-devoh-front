import itemType from '../itemType';
import helpers from '../../../../helpers';

const useAnalyteAndProjectSite = () => {
  const util = helpers.util;

  const build = async ({ metaData, parentItemCount, query = '' }) => {
    const data = [
      {
        name: 'Analyte',
        itemCount: parentItemCount,
        targetCount: parentItemCount,
        rate: util.calcPercentage(parentItemCount, parentItemCount),
        hasChildren: true,
        itemType: itemType.ProjectAnalyte,
        metaData: {
          ...metaData,
          hasChart: false
        }
      },
      {
        name: 'Site',
        itemCount: parentItemCount,
        targetCount: parentItemCount,
        rate: util.calcPercentage(parentItemCount, parentItemCount),
        hasChildren: true,
        itemType: itemType.ProjectSite,
        metaData: {
          ...metaData,
          hasChart: false
        }
      }
    ];

    return data;
  };

  return { build };
};

export default useAnalyteAndProjectSite;
