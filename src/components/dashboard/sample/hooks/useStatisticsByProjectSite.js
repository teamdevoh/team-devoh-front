import { api as psApi } from '../../../../hooks';
import sampleApi from '../../../sample-setting/api';
import helpers from '../../../../helpers';
import api from '../api';
import itemType from '../itemType';
import { chartColor } from '../../cohort/ChartContainer';

const useStatisticsByProjectSite = () => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const generateData = async ({ fetch, projectId, parentItemCount }) => {
    const data = [];
    const psListRes = await psApi.fetchProjectSite({
      projectId,
      name: ''
    });

    const projectSiteList = psListRes.data.items;

    if (projectSiteList.length === 0) {
      return data;
    }

    const res = await fetch({
      projectId,
      parentItemCount
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, 'projectSiteId');

      for (let i = 0; i < projectSiteList.length; i++) {
        const { siteName, projectSiteId } = projectSiteList[i];

        const itemCount = !!g[projectSiteId]
          ? g[projectSiteId].reduce(
              (accumulator, item) => accumulator + item.itemCount,
              0
            )
          : 0;

        data.push({
          name: siteName,
          itemCount,
          targetCount: parentItemCount,
          rate: util.calcPercentage(itemCount, parentItemCount),
          hasChildren: true,
          itemType: itemType.SiteAliquotAnalyte,
          metaData: {
            projectId,
            projectSiteId
          }
        });
      }
    }

    return data;
  };

  const build = async ({ metaData, parentItemCount, query = '' }) => {
    const data = await generateData({
      fetch: api.fetchStatisticsBySampleAliquotAnalyte,
      projectId: metaData.projectId,
      parentItemCount
    });

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({ metaData, parentItemCount, query = '' }) => {
    const labels = [];
    const analyteDataSets = [];

    const psListRes = await psApi.fetchProjectSite({
      projectId: metaData.projectId,
      name: ''
    });

    const aliquotRes = await sampleApi.fetchProjectSampleAliquotList({
      projectId: metaData.projectId,
      projectSampleId: 0
    });

    const projectSiteList = psListRes.data.items;
    const analyteList = aliquotRes.data;

    for (let n = 0; n < analyteList.length; n++) {
      const { aliquotAnalyte } = analyteList[n];
      analyteDataSets[n] = {
        label: aliquotAnalyte,
        backgroundColor: chartColor[n],
        data: []
      };
    }

    const sortedLabels = [];
    const sortedAnalyteDataSets = JSON.parse(JSON.stringify(analyteDataSets));

    const total = {
      label: 'Total',
      backgroundColor: chartColor[(analyteList.length - 1) % chartColor.length],
      data: []
    };
    const sortedTotal = {
      label: 'Total',
      backgroundColor: chartColor[analyteList.length % chartColor.length],
      data: []
    };

    for (let i = 0; i < projectSiteList.length; i++) {
      const { siteName, projectSiteId } = projectSiteList[i];
      labels.push(siteName);

      const res = await api.fetchStatisticsBySampleAliquotAnalyte({
        projectId: metaData.projectId,
        parentItemCount,
        odataQuery: `&$filter=${odata.equal('projectSiteId', projectSiteId)}`
      });

      if (res && res.data) {
        let totalCount = 0;
        for (let n = 0; n < analyteList.length; n++) {
          const { projectSampleAliquotId } = analyteList[n];
          const curAliquot =
            res.data.find(
              item => item.projectSampleAliquotId === projectSampleAliquotId
            ) || {};

          totalCount += curAliquot.itemCount;
          analyteDataSets[n].data.push(curAliquot.itemCount);
        }
        total.data.push(totalCount);
      }
    }

    // dosort
    const mappedTotal = total.data.map((count, index) => {
      return { index, count };
    });

    mappedTotal.sort((a, b) => b.count - a.count);

    for (let i = 0; i < mappedTotal.length; i++) {
      const { index } = mappedTotal[i];

      sortedLabels.push(labels[index]);
      sortedTotal.data.push(total.data[index]);
      for (let n = 0; n < analyteDataSets.length; n++) {
        sortedAnalyteDataSets[n].data.push(analyteDataSets[n].data[index]);
      }
    }

    return {
      labels: sortedLabels,
      datasets: [...sortedAnalyteDataSets, sortedTotal]
    };
  };

  return { build, buildForChart };
};

export default useStatisticsByProjectSite;
