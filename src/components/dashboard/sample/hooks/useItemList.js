import { useState, useEffect } from 'react';
import api from '../api';

const useItemList = ({ projectId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchItems = async () => {
    const res = await api.fetchSampleStatistics({ projectId });
    if (res && res.data) {
      var newItems = res.data;

      setItems(newItems);
    }
    setLoading(false);
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId]
  );

  return { items, loading };
};

export default useItemList;
