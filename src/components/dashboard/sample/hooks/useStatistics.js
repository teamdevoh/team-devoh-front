import { useState } from 'react';
import itemType from '../itemType';
import {
  useSampleStatus,
  useWeekly,
  useWeeklyProject,
  useMonthly,
  useMonthlyProject,
  useRetentionPeriod,
  useRetentionPeriodMonthly,
  useStatisticsByProjectSite,
  useSiteAliquotAnalyte,
  useAnalyteAndProjectSite,
  useStatisticsByProjectAnalyte
} from './';

const useStatistics = () => {
  const [loading, setLoading] = useState(false);
  const sampleStatus = useSampleStatus();
  const weekly = useWeekly();
  const weeklyProject = useWeeklyProject();
  const monthly = useMonthly();
  const monthlyProject = useMonthlyProject();
  const retentionPeriod = useRetentionPeriod();
  const retentionPeriodMonthly = useRetentionPeriodMonthly();
  const statisticsByProjectSite = useStatisticsByProjectSite();
  const siteAliquotAnalyte = useSiteAliquotAnalyte();
  const analyteAndProjectSite = useAnalyteAndProjectSite();
  const projectAnalyte = useStatisticsByProjectAnalyte();

  const fetchItems = async props => {
    const { projectId, type, parentItemCount, parentPeriod } = props;

    if (type === itemType.SampleStatus) {
      setLoading(true);
      const data = await sampleStatus.build({
        projectId,
        parentItemCount
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.Weekly) {
      setLoading(true);
      const data = await weekly.build({
        projectId,
        parentItemCount,
        parentPeriod
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.WeeklyProject) {
      setLoading(true);
      const data = await weeklyProject.build({
        projectId,
        parentItemCount,
        parentPeriod
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.Monthly) {
      setLoading(true);
      const data = await monthly.build({
        projectId,
        parentItemCount,
        parentPeriod
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.MonthlyProject) {
      setLoading(true);
      const data = await monthlyProject.build({
        projectId,
        parentItemCount,
        parentPeriod
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.RetentionPeriod) {
      setLoading(true);
      const data = await retentionPeriod.build({
        projectId,
        parentItemCount
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.RetentionPeriodMonthly) {
      setLoading(true);
      const data = await retentionPeriodMonthly.build({
        ...props
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.AnalyteAndProjectSite) {
      setLoading(true);
      const data = await analyteAndProjectSite.build({
        ...props
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.ProjectAnalyte) {
      setLoading(true);
      const data = await projectAnalyte.build({
        ...props
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.ProjectSite) {
      setLoading(true);
      const data = await statisticsByProjectSite.build({
        ...props
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.SiteAliquotAnalyte) {
      setLoading(true);
      const data = await siteAliquotAnalyte.build({
        ...props
      });
      setLoading(false);
      return data;
    }

    setLoading(false);
    return [];
  };

  return { fetchItems, loading };
};

export default useStatistics;
