import api from '../api';
import helpers from '../../../../helpers';
import moment from 'moment';

const useRetentionPeriodMonthly = () => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const build = async ({ projectId, parentItemCount, name: year }) => {
    const data = [];

    if (isNaN(year)) {
      return data;
    }

    const res = await api.fetchStatisticsByRetentionPeriod({
      projectId,
      parentItemCount,
      odataQuery: `&$filter=${odata.between(
        'preserve',
        `'${year}-01-01'`,
        `'${year}-12-31'`
      )}`
    });

    if (res && res.data) {
      const g = _.groupBy(res.data, item => {
        return item.preserve.substring(0, 7);
      });

      Object.keys(g).forEach(key => {
        const standardDate = moment(`${key}-01`);
        const monthName = standardDate.format('MMM');

        data.push({
          name: monthName,
          itemCount: g[key].length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(g[key].length, parentItemCount),
          hasChildren: false,
          standardDate,
          metaData: { hasExcel: true }
        });
      });
      data.sort((a, b) => new Date(b.standardDate) - new Date(a.standardDate));

      return data;
    }
  };

  return { build };
};

export default useRetentionPeriodMonthly;
