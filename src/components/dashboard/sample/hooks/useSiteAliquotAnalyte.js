import helpers from '../../../../helpers';
import api from '../api';

const useSiteAliquotAnalyte = () => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const generateData = async ({
    fetch,
    projectId,
    parentItemCount,
    odataQuery
  }) => {
    const data = [];

    const res = await fetch({
      projectId,
      parentItemCount,
      odataQuery
    });

    if (res && res.data) {
      for (let i = 0; i < res.data.length; i++) {
        const { aliquotAnalyte, itemCount } = res.data[i];

        data.push({
          name: aliquotAnalyte,
          itemCount,
          targetCount: parentItemCount,
          rate: util.calcPercentage(itemCount, parentItemCount),
          hasChildren: false
        });
      }
    }

    return data;
  };

  const build = async ({ metaData, parentItemCount, query = '' }) => {
    const data = await generateData({
      fetch: api.fetchStatisticsBySampleAliquotAnalyte,
      projectId: metaData.projectId,
      parentItemCount,
      odataQuery: `&$filter=${odata.equal(
        'projectSiteId',
        metaData.projectSiteId
      )}`
    });

    data.sort((a, b) => b.itemCount - a.itemCount);

    return data;
  };

  const buildForChart = async ({ projectId, parentItemCount, query = '' }) => {
    const data = await generateData({
      fetch: api.fetchStatisticsBySampleStatusForChart,
      projectId,
      parentItemCount
    });

    return data;
  };

  return { build, buildForChart };
};

export default useSiteAliquotAnalyte;
