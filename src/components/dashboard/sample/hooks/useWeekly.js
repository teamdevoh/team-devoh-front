import api from '../api';

const useWeekly = () => {
  const build = async ({
    projectId,
    parentItemCount,
    parentPeriod,
    query = ''
  }) => {
    const res = await api.fetchStatisticsWeeks({
      projectId,
      parentItemCount,
      targetDate: parentPeriod.split(' ~ ')[1]
    });

    return res.data;
  };

  const buildForChart = async ({
    projectId,
    parentItemCount,
    parentPeriod,
    query = ''
  }) => {
    const res = await api.fetchStatisticsWeeksForChart({
      projectId,
      parentItemCount,
      targetDate: parentPeriod.split(' ~ ')[1]
    });

    return res.data;
  };

  return { build, buildForChart };
};

export default useWeekly;
