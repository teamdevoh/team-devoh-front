import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';

const useRetentionPeriod = () => {
  const util = helpers.util;

  const generateData = ({ data, parentItemCount }) => {
    const g = _.groupBy(data, item => {
      return item.preserve !== 'Permanent'
        ? item.preserve.substring(0, 4)
        : item.preserve;
    });
    const newData = [];
    Object.keys(g).forEach(key => {
      newData.push({
        name: key || 'No Data',
        itemCount: g[key].length,
        targetCount: parentItemCount,
        rate: util.calcPercentage(g[key].length, parentItemCount),
        hasChildren: key !== 'Permanent' && true,
        itemType: itemType.RetentionPeriodMonthly
      });
    });

    newData.sort((a, b) => (a.name === 'Permanent' ? -1 : b.name - a.name));

    return newData;
  };

  const build = async ({ projectId, parentItemCount }) => {
    const res = await api.fetchStatisticsByRetentionPeriod({
      projectId,
      parentItemCount
    });

    if (res && res.data) {
      return generateData({ data: res.data, parentItemCount });
    }
  };

  const buildForChart = async ({ projectId, parentItemCount }) => {
    const res = await api.fetchStatisticsByRetentionPeriodForChart({
      projectId,
      parentItemCount
    });

    if (res && res.data) {
      return generateData({ data: res.data, parentItemCount });
    }
  };

  return { build, buildForChart };
};

export default useRetentionPeriod;
