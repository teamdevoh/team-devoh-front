import api from '../api';
import helpers from '../../../../helpers';

const useWeeklyProject = () => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const generateData = ({ data, parentItemCount }) => {
    const g = _.groupBy(data, 'projectTitle');
    const newData = [];

    Object.keys(g).forEach(key => {
      newData.push({
        name: key,
        itemCount: g[key].length,
        targetCount: parentItemCount,
        rate: util.calcPercentage(g[key].length, parentItemCount),
        hasChildren: false
      });
    });

    return newData;
  };

  const build = async ({ projectId, parentItemCount, parentPeriod }) => {
    const date = parentPeriod.split(' ~ ');
    const res = await api.fetchStatisticsBySampleStatus({
      projectId,
      parentItemCount,
      odataQuery: `&$select=projectTitle&$filter=${odata.between(
        'sampleTime',
        `'${date[0]}'`,
        `'${date[1]}'`
      )}`
    });

    if (res && res.data) {
      return generateData({ data: res.data, parentItemCount });
    }
  };

  const buildForChart = async ({
    projectId,
    parentItemCount,
    parentPeriod
  }) => {
    const date = parentPeriod.split(' ~ ');
    const res = await api.fetchStatisticsBySampleStatus({
      projectId,
      parentItemCount,
      odataQuery: `&$select=projectTitle&$filter=${odata.between(
        'sampleTime',
        `'${date[0]}'`,
        `'${date[1]}'`
      )}`
    });

    if (res && res.data) {
      return generateData({ data: res.data, parentItemCount });
    }
  };

  return { build, buildForChart };
};

export default useWeeklyProject;
