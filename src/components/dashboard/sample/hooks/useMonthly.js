import moment from 'moment';
import api from '../api';
import helpers from '../../../../helpers';
import itemType from '../itemType';
import { format } from '../../../../constants';

const useMonthly = () => {
  const odata = helpers.oDataBuilder;
  const util = helpers.util;

  const fetchMonth = async props => {
    const { projectId, parentItemCount, startDate, endDate } = props;

    const res = await api.fetchStatisticsBySampleStatus({
      projectId,
      parentItemCount,
      odataQuery: `&$select=projectTitle,sampleTime&$filter=${odata.between(
        'sampleTime',
        `'${startDate}'`,
        `'${endDate}'`
      )}`
    });

    return res.data;
  };

  const fetchMonthForChart = async props => {
    const { projectId, parentItemCount, startDate, endDate } = props;

    const res = await api.fetchStatisticsBySampleStatusForChart({
      projectId,
      parentItemCount,
      odataQuery: `&$select=projectTitle&$filter=${odata.between(
        'sampleTime',
        `'${startDate}'`,
        `'${endDate}'`
      )}`
    });

    return res.data;
  };

  const getStartEndDate = targetDate => {
    const startDate = moment(targetDate)
      .startOf('month')
      .format(format.YYYY_MM_DD);
    const endDate = moment(targetDate)
      .endOf('month')
      .format(format.YYYY_MM_DD);

    return { startDate, endDate };
  };

  const generateData = async ({
    projectId,
    parentItemCount,
    parentPeriod,
    fetch
  }) => {
    let data = [];
    const date = parentPeriod.split(' ~ ');
    const minDate = date[0];
    const maxDate = date[1];
    const s = new Date(moment(minDate).startOf('month'));
    const e = new Date(moment(maxDate).endOf('month'));

    const diffMonth = Math.ceil(moment(e).diff(s, 'months', true));

    let targetDate = moment(maxDate);

    for (let i = 0; i < diffMonth; i++) {
      let { startDate, endDate } = getStartEndDate(targetDate);

      if (targetDate.month() === s.getMonth()) {
        startDate = minDate > startDate ? minDate : startDate;
      } else if (targetDate.month() === e.getMonth()) {
        endDate = maxDate < endDate ? maxDate : endDate;
      }

      const items = await fetch({
        projectId,
        parentItemCount,
        startDate,
        endDate
      });

      if (items) {
        const standardDate = moment(startDate);
        const monthName = standardDate.format('MMM');

        data.push({
          name: `${monthName} ${standardDate.year()}`,
          itemCount: items.length,
          targetCount: parentItemCount,
          rate: util.calcPercentage(items.length, parentItemCount),
          hasChildren: true,
          itemType: itemType.MonthlyProject,
          period: `${startDate} ~ ${endDate}`
        });
      }

      targetDate = moment(targetDate).add(-1, 'month');
    }

    return data;
  };

  const build = async ({ projectId, parentItemCount, parentPeriod }) => {
    return generateData({
      projectId,
      parentItemCount,
      parentPeriod,
      fetch: fetchMonth
    });
  };

  const buildForChart = async ({
    projectId,
    parentItemCount,
    parentPeriod
  }) => {
    return generateData({
      projectId,
      parentItemCount,
      parentPeriod,
      fetch: fetchMonthForChart
    });
  };

  return { build, buildForChart };
};

export default useMonthly;
