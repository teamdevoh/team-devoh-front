import { useState } from 'react';
import itemType from '../itemType';
import {
  useSampleStatus,
  useWeekly,
  useMonthly,
  useRetentionPeriod,
  useStatisticsByProjectSite
} from './';

const useStatisticsForChart = () => {
  const [loading, setLoading] = useState(false);
  const sampleStatus = useSampleStatus();
  const weekly = useWeekly();
  const monthyly = useMonthly();
  const retentionPeriod = useRetentionPeriod();
  const statisticsByProjectSite = useStatisticsByProjectSite();

  const fetchItems = async props => {
    const { projectId, type, parentItemCount, parentPeriod } = props;

    if (type === itemType.SampleStatus) {
      setLoading(true);
      const data = await sampleStatus.buildForChart({
        projectId,
        parentItemCount
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.Weekly) {
      setLoading(true);
      const data = await weekly.buildForChart({
        projectId,
        parentItemCount,
        parentPeriod
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.Monthly) {
      setLoading(true);
      const data = await monthyly.buildForChart({
        projectId,
        parentItemCount,
        parentPeriod
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.RetentionPeriod) {
      setLoading(true);
      const data = await retentionPeriod.buildForChart({
        projectId,
        parentItemCount
      });
      setLoading(false);
      return data;
    }

    if (type === itemType.AnalyteAndProjectSite) {
      setLoading(true);
      const data = await statisticsByProjectSite.buildForChart({
        ...props
      });
      setLoading(false);
      return data;
    }

    setLoading(false);
    return [];
  };

  return { fetchItems, loading };
};

export default useStatisticsForChart;
