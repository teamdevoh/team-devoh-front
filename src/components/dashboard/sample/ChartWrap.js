import React, { Fragment } from 'react';
import DefaultChart from './DefaultChart';
import type from './itemType';
import WeeklyChart from './WeeklyChart';

const chartOptions = {
  [type.Weekly]: {
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          const { index, yLabel } = tooltipItem;
          const { metaData } = data.datasets[0];
          let label = `${data.labels[index]}(${metaData[index].period})` || '';

          if (label) {
            label += ': ';
          }
          label += yLabel;
          return label;
        }
      }
    }
  }
};

const ChartWrap = props => {
  const { itemType } = props;

  const getChartComponent = () => {
    let result;

    switch (itemType) {
      case type.Weekly:
        result = <WeeklyChart chartOptions={chartOptions} {...props} />;
        break;
      case type.Monthly:
        result = (
          <DefaultChart
            chartOptions={chartOptions}
            title={props.period}
            {...props}
          />
        );
        break;
      case type.AnalyteAndProjectSite:
        result = (
          <DefaultChart
            chartOptions={chartOptions}
            title={props.period}
            legend={true}
            {...props}
          />
        );
        break;
      default:
        result = <DefaultChart chartOptions={chartOptions} {...props} />;
        break;
    }

    return result;
  };

  return <Fragment>{getChartComponent()}</Fragment>;
};

export default ChartWrap;
