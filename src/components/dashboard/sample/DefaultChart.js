import React, { Fragment, useState, useEffect } from 'react';
import { Loader, Dimmer } from 'semantic-ui-react';
import CohortChart from '../cohort/CohortChart';
import type from './itemType';
import { generateChartData } from '../cohort/ChartContainer';

const DefaultChart = props => {
  const {
    fetchItems,
    itemType,
    id,
    targetCount,
    itemCount,
    period,
    chartOptions,
    projectId,
    title,
    legend
  } = props;
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [totalItemCount, setTotalItemCount] = useState(itemCount);

  const fetch = async () => {
    setLoading(true);

    const param = {
      ...props,
      type: itemType,
      key: id,
      parentItemCount:
        itemType === type.Week || itemType === type.Monthly
          ? targetCount
          : totalItemCount,
      parentPeriod: period,
      projectId
    };

    const data = await fetchItems(param);

    if (itemType === type.AnalyteAndProjectSite) {
      setData(data);
    } else {
      const chartData = generateChartData({
        items: data,
        doSort:
          itemType !== type.Weekly &&
          itemType !== type.Monthly &&
          itemType !== type.RetentionPeriod
      });

      setData(chartData.data);
      setTotalItemCount(chartData.totalItemCount);
    }
    setLoading(false);
  };

  useEffect(
    () => {
      fetch();
    },
    [itemType, projectId]
  );

  return (
    <Fragment>
      <Dimmer active={loading} inverted>
        <Loader size="big">Loading</Loader>
      </Dimmer>
      <Fragment>
        <CohortChart
          data={data}
          options={chartOptions[itemType]}
          title={title}
          legend={legend}
        />
      </Fragment>
    </Fragment>
  );
};

export default DefaultChart;
