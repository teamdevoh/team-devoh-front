import React, { Fragment, useState } from 'react';
import Row from './Row';
import { useBoolean } from '../../../hooks';
import { Modal } from '../../modal';
import useStatisticsForChart from './hooks/useStatisticsForChart';
import useStatistics from './hooks/useStatistics';
import ChartWrap from './ChartWrap';
import sampleApi from '../../sample-status/api';
import { useParams } from 'react-router-dom';
import helpers from '../../../helpers';
import { fields, fieldNames } from '../../sample-status/DashBoardTool';

const RowWrapper = ({
  indent,
  projectId,
  parentName,
  items,
  activeRowKey,
  onRowClick
}) => {
  const odata = helpers.oDataBuilder;
  const { projectId: curProjectId } = useParams();
  const showChartModal = useBoolean(false);
  const [rowData, setRowData] = useState({});
  const { fetchItems } = useStatisticsForChart();
  const statistics = useStatistics(projectId);

  const handleOpenChartModal = rowData => () => {
    setRowData(rowData);
    showChartModal.setTrue();
  };

  const handleSampleExcelDownClick = rowData => async () => {
    const { standardDate } = rowData;
    const yyyymm = helpers.util
      .dateformat(standardDate, 'YYYY-MM-DD')
      .substring(0, 7);

    const res = await sampleApi.fetchSampleCollection({
      curProjectId,
      projectId,
      projectSiteId: 0,
      subjectIds: '',
      sampleStatusCodeId: 0,
      freezerId: 0,
      barcode: '',
      odataQuery: `&$filter=${odata.between(
        'preserve',
        `'${yyyymm}-01'`,
        `'${yyyymm}-31'`
      )}`
    });

    if (res && res.data) {
      helpers.util.exportCSV({
        fileName: `Retention_period_until_${yyyymm}.csv`,
        fields,
        fieldNames,
        data: res.data
      });
    }
  };

  if (items != null && items.length > 0) {
    return (
      <Fragment>
        {items.map((item, index) => {
          return (
            <Row
              key={index + item.name}
              index={index}
              id={item.id}
              projectId={projectId}
              parentName={parentName}
              indent={indent}
              statistics={statistics}
              activeRowKey={activeRowKey}
              onRowClick={onRowClick}
              onChartClick={handleOpenChartModal({
                ...item
              })}
              onSampleExcelDownClick={handleSampleExcelDownClick({
                ...item
              })}
              {...item}
            />
          );
        })}
        <Modal
          size="large"
          open={showChartModal.value}
          onOK={showChartModal.setFalse}
          title={Object.keys(rowData).length !== 0 ? rowData.name : ''}
          content={
            <ChartWrap
              projectId={projectId}
              fetchItems={fetchItems}
              {...rowData}
            />
          }
        />
      </Fragment>
    );
  }

  return <Fragment />;
};

export default RowWrapper;
