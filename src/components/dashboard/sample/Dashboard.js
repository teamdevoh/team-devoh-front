import React, { useCallback, useState } from 'react';
import { Table, Grid, Header } from 'semantic-ui-react';
import RowWrapper from './RowWrapper';
import { useItemList } from './hooks';
import { useFormatMessage } from '../../../hooks';
import CopyRedirectUrl from '../../modules/CopyRedirectUrl';
import { ProjectsOfSamples } from '../../sample-status/Selector';

const Dashboard = () => {
  const [projectId, setProjectId] = useState(0);
  const { items } = useItemList({ projectId });
  const [activeRowKey, setActiveRowKey] = useState('');
  const t = useFormatMessage();

  const handleFilterChange = (event, { value }) => {
    setProjectId(value);
  };

  const handleRowClick = useCallback(key => {
    setActiveRowKey(key);
  }, []);

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column>
          <Header as="h3" dividing>
            Dashboard
            <Header.Subheader>Sample</Header.Subheader>
            <CopyRedirectUrl />
          </Header>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column mobile="sixteen" largeScreen="five">
          <ProjectsOfSamples
            name="projectId"
            value={projectId}
            onChange={handleFilterChange}
            hasAll
            fluid
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Table celled selectable compact="very" size="small">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell textAlign="center">
                  {t('item')}
                </Table.HeaderCell>
                <Table.HeaderCell textAlign="center" width="three">
                  {t('cohort.dashboard.period')}
                </Table.HeaderCell>
                <Table.HeaderCell textAlign="center" width="one">
                  Count
                </Table.HeaderCell>
                <Table.HeaderCell textAlign="center" width="one">
                  Chart
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <RowWrapper
                indent={0}
                projectId={projectId}
                items={items}
                activeRowKey={activeRowKey}
                onRowClick={handleRowClick}
              />
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default Dashboard;
