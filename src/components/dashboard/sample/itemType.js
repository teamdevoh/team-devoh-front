const itemType = {
  SampleStatus: 'SampleStatus',
  Weekly: 'Weekly',
  Monthly: 'Monthly',
  RetentionPeriod: 'RetentionPeriod',
  WeeklyProject: 'WeeklyProject',
  MonthlyProject: 'MonthlyProject',
  RetentionPeriodMonthly: 'RetentionPeriodMonthly',
  AnalyteAndProjectSite: 'AnalyteAndProjectSite',
  ProjectSite: 'ProjectSite',
  SiteAliquotAnalyte: 'SiteAliquotAnalyte',
  ProjectAnalyte: 'ProjectAnalyte'
};

export default itemType;
