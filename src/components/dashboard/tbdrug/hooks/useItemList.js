import { useState, useEffect } from 'react';
import helpers from '../../../../helpers';
import api from '../api';

const useItemList = ({ projectId, searchDate, projectMemberId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchItems = async () => {
    setLoading(true);
    const res = await api.fetchTbdrugStatistics({
      projectId,
      searchDate: helpers.util.dateformat(searchDate, 'YYYY-MM-DD'),
      projectMemberId
    });
    if (res && res.data) {
      var newItems = res.data;

      setItems(newItems);
    }
    setLoading(false);
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, searchDate, projectMemberId]
  );

  return { projectId, items, loading };
};

export default useItemList;
