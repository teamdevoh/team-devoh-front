import helpers from '../../../helpers';

const fetchTbdrugStatistics = ({ projectId, searchDate, projectMemberId }) => {
  const params = helpers.qs.stringify({
    projectId,
    searchDate,
    projectMemberId
  });
  return helpers.Service.get(`api/activities/tbdrug/statistics?${params}`);
};

export default {
  fetchTbdrugStatistics
};
