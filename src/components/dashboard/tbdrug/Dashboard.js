import React, { useState } from 'react';
import { Grid, Header, Button, Icon, Popup } from 'semantic-ui-react';
import { useItemList } from './hooks';
import CopyRedirectUrl from '../../modules/CopyRedirectUrl';
import { MemberList } from '../../collection-tbdrug/Selector';
import { useParams } from 'react-router-dom';
import DashboardTable from './DashboardTable';
import helpers from '../../../helpers';
import { Container } from 'semantic-ui-react';
import { useFormatMessage } from '../../../hooks';
import styled from 'styled-components';

const StyledContainer = styled(Container)`
  &&& {
    display: flex;
    flex-wrap: wrap;
  }
`;

const SearchDateWrap = styled.div`
  margin: auto;
`;

const Dashboard = () => {
  const t = useFormatMessage();
  const { projectId } = useParams();
  const [searchDate, setSearchDate] = useState(new Date());
  const [projectMemberId, setProjectMemberId] = useState(null);
  const { items, loading } = useItemList({
    projectId,
    searchDate,
    projectMemberId
  });

  const handleMemberChange = (event, { name, value }) => {
    setProjectMemberId(value);
  };

  const handleChangeDateButton = (e, { name, value }) => {
    const newDate = new Date(searchDate);
    newDate.setDate(newDate.getDate() + value);

    setSearchDate(newDate);
  };

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column>
          <Header as="h3" dividing>
            Dashboard
            <Header.Subheader>TB Drugs</Header.Subheader>
            <CopyRedirectUrl />
          </Header>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column mobile="sixteen" widescreen="five">
          <StyledContainer>
            <SearchDateWrap>
              <Popup
                trigger={
                  <Button
                    icon
                    style={{ marginRight: '8px' }}
                    value={-7}
                    onClick={handleChangeDateButton}
                  >
                    <Icon name="angle double left" />
                  </Button>
                }
                content={t('a.week.ago')}
                inverted
              />
              {helpers.util.dateformat(searchDate, 'YYYY-MM-DD')}
              <Popup
                trigger={
                  <Button
                    icon
                    style={{ marginLeft: '8px' }}
                    value={7}
                    onClick={handleChangeDateButton}
                    disabled={
                      helpers.util.dateformat(searchDate, 'YYYY-MM-DD') ===
                      helpers.util.dateformat(new Date(), 'YYYY-MM-DD')
                    }
                  >
                    <Icon name="angle double right" />
                  </Button>
                }
                content={t('a.week.later')}
                inverted
              />
            </SearchDateWrap>
          </StyledContainer>
        </Grid.Column>
        <Grid.Column mobile="sixteen" widescreen="five">
          <MemberList
            projectId={projectId}
            value={projectMemberId}
            onChange={handleMemberChange}
            fluid={true}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <DashboardTable
            projectId={projectId}
            items={items}
            loading={loading}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default Dashboard;
