import React, { Fragment } from 'react';
import { DataGrid } from '../../data-grid';
import { Divider } from 'semantic-ui-react';
import styled from 'styled-components';
import helpers from '../../../helpers';
import {
  BasicPhysico,
  AdmeBasic,
  Inhibition,
  MeTransporter,
  PkData,
  FactorsAffecting,
  PgxInteractions,
  PdData,
  DrugInteractions,
  ToxicityDataSheet
} from '../../../constants/activity';
import api from '../../collection/api';

const WMTWrap = styled.div`
  display: flex;
  justify-content: space-around;

  & > :not(:last-child) {
    border-right: 1px solid #efefef;
  }

  & > * {
    flex: 1;
    text-align: center;
  }
`;

const ContentWrap = styled.div`
  min-height: 35px;
  display: inline-flex;
  justify-content: center;
  align-items: center;
`;

const Link = styled.a`
  cursor: pointer;

  ${props =>
    `color: ${
      props.color === 'red'
        ? '#db2828'
        : props.color === 'blue'
          ? '#1a69a4'
          : 'rgba(0, 0, 0, 0.87)'
    };`};
`;

const WMT = () => {
  return (
    <Fragment>
      <Divider style={{ margin: 0 }} />
      <WMTWrap>
        <span>Week</span>
        <span>Mon</span>
        <span>Tot</span>
      </WMTWrap>
    </Fragment>
  );
};

const WMTContentWrap = ({ projectId, keyId, row, activityId }) => {
  const handleGoActivityForm = async () => {
    const { tbdrugId } = row;
    const res = await api.fetchActivitiesOfGroup({
      activityGroupId: 0,
      subjectId: tbdrugId
    });

    if (res && res.data) {
      const paId = res.data.filter(item => item.activityId === activityId);

      helpers.history.push(
        `/project/${projectId}/collection/tbdrug/${tbdrugId}/activitygroup/0?projectActivityId=${
          paId[0].projectActivityId
        }&activityId=${activityId}`
      );
    }
  };

  return (
    <WMTWrap>
      <ContentWrap style={{ color: '#db2828' }}>
        <Link color="red" onClick={handleGoActivityForm}>
          {row[`${keyId}_W`]}
        </Link>
      </ContentWrap>
      <ContentWrap style={{ color: '#1a69a4' }}>
        <Link color="blue" onClick={handleGoActivityForm}>
          {row[`${keyId}_M`]}
        </Link>
      </ContentWrap>
      <ContentWrap>
        <Link onClick={handleGoActivityForm}>{row[`${keyId}_T`]}</Link>
      </ContentWrap>
    </WMTWrap>
  );
};

const activityIds = {
  'Basic Physico': BasicPhysico,
  'ADME Basic': AdmeBasic,
  'Inhibition or Induction': Inhibition,
  'ME & Transporter': MeTransporter,
  'PK Data': PkData,
  'Factors affecting': FactorsAffecting,
  'PGx interactions': PgxInteractions,
  'PD Data': PdData,
  'Drug-drug Interactions': DrugInteractions,
  'Toxicity Data Sheet': ToxicityDataSheet
};

const DashboardTable = ({ projectId, items, loading }) => {
  const columns = [
    {
      key: 'tbdrug',
      width: 150,
      name: 'Drug',
      frozen: true,
      formatter: ({ value, isScrolling, row }) => {
        return (
          <a
            style={{ cursor: 'pointer' }}
            onClick={() => {
              helpers.history.push(
                `/project/${projectId}/collection/tbdrug/${row.tbdrugId}`
              );
            }}
          >
            {value}
          </a>
        );
      }
    },
    {
      key: 'charge',
      width: 110,
      name: 'Charge',
      frozen: true
    },
    {
      key: 'phy_W',
      width: 150,
      headerRenderer: header => {
        return (
          <div>
            Basic Physico
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="phy"
            projectId={projectId}
            activityId={activityIds['Basic Physico']}
          />
        );
      }
    },
    {
      key: 'adme_W',
      width: 150,
      headerRenderer: header => {
        return (
          <div>
            ADME Basic
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="adme"
            projectId={projectId}
            activityId={activityIds['ADME Basic']}
          />
        );
      }
    },
    {
      key: 'inh_W',
      width: 160,
      headerRenderer: header => {
        return (
          <div>
            Inhibition or Induction
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="inh"
            projectId={projectId}
            activityId={activityIds['Inhibition or Induction']}
          />
        );
      }
    },
    {
      key: 'met_W',
      width: 150,
      headerRenderer: header => {
        return (
          <div>
            ME & Transporter
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="met"
            projectId={projectId}
            activityId={activityIds['ME & Transporter']}
          />
        );
      }
    },
    {
      key: 'pkd_W',
      width: 150,
      headerRenderer: header => {
        return (
          <div>
            PK Data
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="pkd"
            projectId={projectId}
            activityId={activityIds['PK Data']}
          />
        );
      }
    },
    {
      key: 'fac_W',
      width: 150,
      headerRenderer: header => {
        return (
          <div>
            Factors affecting
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="fac"
            projectId={projectId}
            activityId={activityIds['Factors affecting']}
          />
        );
      }
    },
    {
      key: 'pgx_W',
      width: 150,
      headerRenderer: header => {
        return (
          <div>
            PGx interactions
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="pgx"
            projectId={projectId}
            activityId={activityIds['PGx interactions']}
          />
        );
      }
    },
    {
      key: 'pdd_W',
      width: 150,
      headerRenderer: header => {
        return (
          <div>
            PD Data
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="pdd"
            projectId={projectId}
            activityId={activityIds['PD Data']}
          />
        );
      }
    },
    {
      key: 'ddi_W',
      width: 170,
      headerRenderer: header => {
        return (
          <div>
            Drug-drug Interactions
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="ddi"
            projectId={projectId}
            activityId={activityIds['Drug-drug Interactions']}
          />
        );
      }
    },
    {
      key: 'tox_W',
      width: 150,
      headerRenderer: header => {
        return (
          <div>
            Toxicity Data Sheet
            <WMT />
          </div>
        );
      },
      formatter: ({ value, isScrolling, row }) => {
        return (
          <WMTContentWrap
            row={row}
            keyId="tox"
            projectId={projectId}
            activityId={activityIds['Toxicity Data Sheet']}
          />
        );
      }
    }
  ];

  return (
    <DataGrid
      columns={columns}
      rows={items}
      rowKey="tbdrugId"
      minHeight={680}
      headerRowHeight={50}
      loading={loading}
    />
  );
};

export default DashboardTable;
