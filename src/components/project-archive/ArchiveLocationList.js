import React, { memo } from 'react';
import { Breadcrumb, Header, Icon } from 'semantic-ui-react';
import { ArchiveLocationListItem } from './';
import styled from 'styled-components';
import map from 'lodash/map';

const ArchiveHeader = ({ name, description }) => {
  const StyledHeader = styled(Header.Content)`
    word-break: break-word;
  `;

  return (
    <Header as="h2" size="small">
      <Icon name="folder open" />
      <StyledHeader>
        {name}
        <Header.Subheader>{description}</Header.Subheader>
      </StyledHeader>
    </Header>
  );
};

const ArchiveLocationList = ({ fields, items, onClick }) => {
  let name = '/';
  let description = '';
  if (items.length > 0) {
    const item = items[items.length - 1];
    name = item[fields.name];
    description = item[fields.description];
  }

  return (
    <div>
      <Breadcrumb size="small">
        {map(items, (item, index) => {
          return (
            <ArchiveLocationListItem
              key={`${item[fields.parentId]}_${item[fields.id]}`}
              id={item[fields.id]}
              parentId={item[fields.parentId]}
              name={item[fields.name]}
              isParent={items.length - 1 > index}
              onClick={onClick}
            />
          );
        })}
      </Breadcrumb>
      <ArchiveHeader name={name} description={description} />
    </div>
  );
};

export default memo(ArchiveLocationList);
