import React, { useCallback, useState, useEffect, memo } from 'react';
import { UploadLayer, FileList, FakeFileList } from '../modules';
import { Confirm, Modal, notification } from '../modal';
import { api } from './';
import { ChoiceOverwrite, ProjectArchiveMove } from './';
import { Loader, Dimmer } from 'semantic-ui-react';
import { useUpload, useFormatMessage } from '../../hooks';
import find from 'lodash/find';
import map from 'lodash/map';
import filter from 'lodash/filter';
import findIndex from 'lodash/findIndex';
import helpers from '../../helpers';
import { useArchiveDownload } from './hooks';
import RequestPanel from './RequestPanel';
import { role } from '../../constants';
import MultiDownloadButton from './/MultiDownloadButton';
import FileViewerWrapper from '../viewer/FileViewerWrapper';

const ProjectArchiveUpload = ({
  history,
  open,
  onClose,
  projectId,
  projectArchiveId
}) => {
  const t = useFormatMessage();
  const file = useUpload();
  const [isLoading, setIsLoading] = useState(false);
  const [archive, setRequest] = useArchiveDownload({
    projectId,
    projectArchiveId
  });
  const [attachedFiles, setAttachedFiles] = useState([]);
  const [already, setAlready] = useState({
    open: false,
    cb: () => {},
    filesObject: [],
    files: []
  });
  const [search, setSearch] = useState({
    open: false,
    tag: '',
    location: ''
  });
  const [move, setMove] = useState({
    open: false,
    selecteds: []
  });

  const [isViewReq, setIsViewReq] = useState(false);
  const [fileInfo, setFileInfo] = useState();
  const [viewerOpen, setViewerOpen] = useState(false);

  useEffect(() => {
    getItems();
    return () => {};
  }, []);

  const getItems = useCallback(async () => {
    setIsLoading(true);
    const res = await api.fetchArchiveAttachs({
      projectId,
      projectArchiveId: projectArchiveId
    });

    let items = [];
    await res.data.forEach(async item => {
      const fileObj = await helpers.file.setFileInfo({
        ...item,
        id: item.projectAttachmentId,
        thumbnailApiUrl: `api/projects/${projectId}/archives/${projectArchiveId}/attach.thumbnail/${
          item.projectAttachmentId
        }`
      });
      items.push(fileObj);
    });

    setIsLoading(false);
    setAttachedFiles(items);
  }, []);

  const handleUpload = useCallback(async (ids, alreadyFiles) => {
    if (ids.length > 0) {
      const res = await api.addArchiveAttach({
        projectId,
        projectArchiveId,
        model: { fileStorageIds: ids, createHistoriesTarget: alreadyFiles }
      });

      if (res.data) {
        setAlready({ ...already, filesObject: [], files: [] });
        getItems();
      }
    }
  }, []);

  const handleDownload = useCallback(async id => {
    await archive.onDownload({ id, projectArchiveId });
  }, []);

  const handleViewerClick = useCallback(
    async ({ fileInfo = {}, id }) => {
      if (fileInfo.name === null) {
        fileInfo = attachedFiles.find(file => file.key === id);
      }

      let isAllow = false;
      if (helpers.Identity.hasRole(role.ProjectArchive_Download)) {
        isAllow = true;
      } else {
        const allowRes = await api.isAllowReqAttach({
          projectId,
          projectArchiveId,
          projectAttachmentId: id
        });

        if (allowRes.data === null) {
          notification.confirm({
            title: t('View.files'),
            content: t('View.files.info.message'),
            onOK: async () => {
              setRequest({ isRequire: true, id });
              setIsViewReq(true);
            }
          });
        } else {
          isAllow = allowRes.data.allow;

          if (isAllow === false) {
            notification.warning({
              title: t('denied')
            });
          }
          if (isAllow === null) {
            notification.warning({
              title: t('common.waiting')
            });
          }
        }
      }

      if (isAllow === true) {
        setFileInfo(fileInfo);
        setViewerOpen(true);
      }
    },
    [attachedFiles]
  );

  const handleReqOKClick = useCallback(
    async data => {
      if (isViewReq) {
        const res = await api.addReqArchiveAttach({
          projectId,
          projectArchiveId,
          model: { projectAttachmentId: data.id, reason: data.value }
        });

        if (res && res.data) {
          setRequest({ isRequire: false, id: 0 });
          setIsViewReq(false);
          handleViewerClick({ id: data.id });
        }
      } else {
        await archive.onAddReq({
          id: data.id,
          projectArchiveId,
          reason: data.value
        });
      }
    },
    [isViewReq]
  );

  const handleRemoveAttach = useCallback(
    async id => {
      const res = await api.removeProjectAttach({
        projectId,
        projectArchiveId,
        projectAttachmentId: id
      });

      if (res.data) {
        setAttachedFiles(
          attachedFiles.filter(file => {
            return file.key !== id;
          })
        );
      }
    },
    [attachedFiles]
  );

  const handleUpdateTag = useCallback(
    async (id, tags) => {
      const joinedTags = tags.join('');
      const res = await api.updateAttachTag({
        projectId,
        projectArchiveId,
        projectAttachmentId: id,
        model: { tag: joinedTags }
      });
      if (res.data) {
        setAttachedFiles(
          attachedFiles.map((file, index) => {
            if (file.key === id) {
              file.tags = joinedTags;
            }
            return file;
          })
        );
      }
    },
    [attachedFiles]
  );

  const getHasFiles = useCallback(({ projectId, projectArchiveId }) => {
    return api.fetchAttachKeyValuePairs({ projectId, projectArchiveId });
  }, []);

  const handleMove = useCallback(
    parentId => {
      if (parentId.toString() !== projectArchiveId.toString()) {
        getHasFiles({ projectId, projectArchiveId: parentId }).then(res => {
          const files = [];
          for (let i = 0; i < move.selecteds.length; i++) {
            const item = move.selecteds[i];
            const foundFile = find(res.data, { name: item.name });
            if (foundFile) {
              files.push(foundFile);
            }
          }

          if (files.length > 0) {
            setAlready({
              ...already,
              open: true,
              files,
              filesObject: move.selecteds,
              cb: moveFiles => {
                changeLocation(parentId, moveFiles, files);
              }
            });
          } else {
            changeLocation(parentId, move.selecteds, []);
          }
        });
      }
    },
    [move]
  );

  const changeLocation = useCallback(
    (projectArchiveId, targetFiles, alreadyFiles) => {
      let keySet = [];
      map(targetFiles, file => {
        if (alreadyFiles.length > 0) {
          const found = find(alreadyFiles, { name: file.name });
          if (found) {
            keySet.push({ prevId: found.key, nextId: file.key });
          } else {
            keySet.push({ prevId: 0, nextId: file.key });
          }
        } else {
          keySet.push({ prevId: 0, nextId: file.key });
        }
      });

      if (keySet.length > 0) {
        api
          .changeAttachLocation({
            projectId,
            projectArchiveId,
            model: {
              listPrevWithNextId: keySet
            }
          })
          .then(res => {
            setAlready({ ...already, filesObject: [], files: [] });
            setAttachedFiles(
              filter(attachedFiles, file => {
                return findIndex(targetFiles, { key: file.key }) === -1;
              })
            );
            setMove({ ...move, open: false, selecteds: [] });
          });
      }
    }
  );

  const handleTagClick = useCallback(tag => {
    setSearch({
      open: true,
      tag: tag,
      location: `/project/${projectId}/search?keyword=${tag}`
    });
  }, []);

  const handleDrop = useCallback((acceptedFiles, alreadyFiles) => {
    onClose();
    file.upload(acceptedFiles, ids => {
      handleUpload(ids, alreadyFiles);
    });
  }, []);

  const handleGoHistories = useCallback(({ id, name }) => {
    history.push(
      `/project/${projectId}/archive/${projectArchiveId}/attach/${id}/histories`
    );
  }, []);

  const handleMoveContextClick = useCallback(({ id, name }) => {
    setMove({ ...move, open: true, selecteds: [{ key: id, name }] });
  }, []);

  const handleMoveClose = useCallback(() => {
    setMove({
      ...move,
      open: false,
      selecteds: []
    });
  }, []);

  const handleVerifyDrop = useCallback(files => {
    getHasFiles({ projectId, projectArchiveId }).then(res => {
      let alreadyFiles = [];
      for (let i = 0; i < files.length; i++) {
        const file = files[i];

        const foundFile = find(res.data, { name: file.name });
        if (foundFile) {
          alreadyFiles.push(foundFile);
        }
      }
      if (alreadyFiles.length > 0) {
        setAlready({
          ...already,
          open: true,
          files: alreadyFiles,
          filesObject: files,
          cb: handleDrop
        });
      } else {
        handleDrop(files, []);
      }
    });
  }, []);

  const handleSkip = useCallback(() => {
    let takeFiles = [];

    for (let i = 0; i < already.filesObject.length; i++) {
      const file = already.filesObject[i];
      const attachedIndex = findIndex(already.files, {
        name: file.name
      });
      if (attachedIndex === -1) {
        takeFiles.push(file);
      }
    }

    setAlready({ ...already, files: [], open: false });
    already.cb(takeFiles, []);
  }, []);

  const handleOverwrite = useCallback(
    () => {
      let files = [];
      for (let i = 0; i < already.files.length; i++) {
        const file = already.files[i];
        files.push(file.key);
      }
      setAlready({ ...already, files: files, open: false });
      already.cb(already.filesObject, files);
    },
    [already]
  );

  const handleExistInfoClose = useCallback(() => {
    setAlready({ ...already, open: false, files: [], filesObject: [] });
  }, []);

  const hasEditRole = helpers.Identity.hasRole(role.ProjectArchive_Edit);

  let userContext = [
    {
      onClick: handleGoHistories,
      icon: { name: 'history', color: 'green' },
      name: t('common.histories')
    }
  ];

  if (hasEditRole) {
    userContext.push({
      onClick: handleMoveContextClick,
      icon: { name: 'caret square right outline' },
      name: t('folder.move')
    });
  }

  const fetchSignedUrl = useCallback(
    fileInfo => {
      return api.fetchArchivePresignedurl({
        projectId,
        projectArchiveId,
        projectAttachmentId: fileInfo.key
      });
    },
    [projectId, projectArchiveId]
  );

  const handleViewerClose = useCallback(() => {
    setViewerOpen(false);
  }, []);

  const getDownloadUrl = useCallback(
    key => {
      if (!helpers.Identity.hasRole(role.ProjectArchive_Download)) {
        return `api/projects/${projectId}/archives/${projectArchiveId}/attach/${key}/req.download`;
      } else {
        return `api/projects/${projectId}/archives/${projectArchiveId}/attach/${key}`;
      }
    },
    [projectId, projectArchiveId]
  );

  return (
    <div>
      <Confirm
        title={t('common.search')}
        content={t('project.search.is', { name: search.tag })}
        open={search.open}
        onOK={() => {
          history.push(search.location);
        }}
        onCancel={() => setSearch({ ...search, open: false })}
      />
      <Modal
        open={already.open}
        onOK={handleExistInfoClose}
        title={'Exists file'}
        content={
          <ChoiceOverwrite
            files={already.files}
            onSkip={handleSkip}
            onOverwrite={handleOverwrite}
          />
        }
      />
      <FileViewerWrapper
        fileInfo={fileInfo}
        fetchSignedUrl={fetchSignedUrl}
        open={viewerOpen}
        onClose={handleViewerClose}
        getDownloadUrl={getDownloadUrl}
      />
      <UploadLayer
        open={open}
        onClose={onClose}
        title={t('common.attachment.upload')}
        onDrop={handleVerifyDrop}
      />
      <FileList
        files={attachedFiles}
        onDownloadClick={handleDownload}
        onDeleteClick={hasEditRole ? handleRemoveAttach : null}
        onUpdateTag={hasEditRole ? handleUpdateTag : null}
        onTagClick={handleTagClick}
        userContextMenus={userContext}
        MultiDownloadButton={MultiDownloadButton({
          projectId,
          projectArchiveId
        })}
        onViewerClick={handleViewerClick}
      />
      <FakeFileList items={file.fakes} />
      <Dimmer active={isLoading} inverted>
        <Loader size="small" />
      </Dimmer>
      <Modal
        open={move.open}
        onOK={handleMoveClose}
        title={'Move'}
        content={
          <ProjectArchiveMove
            projectId={projectId}
            projectArchiveId={projectArchiveId}
            onClick={handleMove}
          />
        }
      />
      <RequestPanel
        id={archive.request.id}
        show={archive.request.isRequire}
        onOKClick={handleReqOKClick}
        onCancelClick={archive.onUndoReq}
        loading={archive.loading}
      />
    </div>
  );
};

export default memo(ProjectArchiveUpload);
