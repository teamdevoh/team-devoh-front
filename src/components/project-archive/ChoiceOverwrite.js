import React, { memo } from 'react';
import { Button, Message, List } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import map from 'lodash/map';

const MainTitle = () => {
  const t = useFormatMessage();
  return (
    <Message.Header>{t('project.archive.file.exists.title')}</Message.Header>
  );
};

const ListWrapper = memo(({ items }) => {
  return (
    <List bulleted>
      {map(items, file => {
        return <Item key={file.key} name={file.name} />;
      })}
    </List>
  );
});

const Item = ({ name }) => {
  return <List.Item>{name}</List.Item>;
};

const SkipButton = memo(({ onClick }) => {
  const t = useFormatMessage();
  return (
    <Button negative onClick={onClick}>
      {t('project.archive.file.skip')}
    </Button>
  );
});

const OverwriteButton = memo(({ onClick }) => {
  const t = useFormatMessage();
  return (
    <Button
      positive
      icon="checkmark"
      labelPosition="right"
      content={t('project.archive.file.overwrite')}
      onClick={onClick}
    />
  );
});

const ChoiceOverwrite = ({ files, onSkip, onOverwrite }) => {
  return (
    <div>
      <Message info>
        <MainTitle />
        <ListWrapper items={files} />
      </Message>
      <SkipButton onClick={onSkip} />
      <OverwriteButton onClick={onOverwrite} />
    </div>
  );
};

export default ChoiceOverwrite;
