import React, { memo } from 'react';
import { Header } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';

const ProjectArchiveHeader = memo(() => {
  const t = useFormatMessage();

  return (
    <Header as="h3" dividing>
      {t('project.archive.title')}
      <Header.Subheader>
        {t('project.archive.title.description')}
      </Header.Subheader>
      <CopyRedirectUrl />
    </Header>
  );
});

export default ProjectArchiveHeader;
