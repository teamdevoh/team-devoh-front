import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { Form, TextArea, Message, Icon } from 'semantic-ui-react';
import { Confirm } from '../modal';
import { useFormatMessage } from '../../hooks';

const RequestPanel = ({ id, loading, show, onOKClick, onCancelClick }) => {
  const t = useFormatMessage();
  const [reason, setReason] = useState('');

  useEffect(
    () => {
      setReason('');
    },
    [show]
  );

  const handleChange = useCallback(
    (event, { name, value }) => {
      setReason(value);
    },
    [reason]
  );

  const handleOKClick = useCallback(
    () => {
      if (reason.trim().length > 0) {
        onOKClick({ id, value: reason });
      }
    },
    [reason]
  );

  const handleCancelClick = useCallback(() => {
    setReason('');
    onCancelClick();
  }, []);

  return (
    <Confirm
      title={t('project.archive.req.permission')}
      content={
        <Fragment>
          <Message negative>
            <Message.Header>
              {t('project.archive.req.permission.info.header')}
            </Message.Header>
            <p>
              {t('project.archive.req.permission.info')}
              <Icon name="phone" />
              <a href="tel:0518908669">(051-890-8669)</a>
            </p>
          </Message>
          <Form>
            <Form.Field required>
              <label>{t('reason')}</label>
              <TextArea value={reason} onChange={handleChange} />
            </Form.Field>
          </Form>
        </Fragment>
      }
      open={show}
      loading={loading}
      onOK={handleOKClick}
      onCancel={handleCancelClick}
    />
  );
};

export default RequestPanel;
