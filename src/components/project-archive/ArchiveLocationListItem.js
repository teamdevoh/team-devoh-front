import React, { useCallback } from 'react';
import { Breadcrumb } from 'semantic-ui-react';
import { ArchiveBreadcrumb } from './styles';

const ArchiveLocationListItem = ({ name, isParent, id, parentId, onClick }) => {
  const handleClick = useCallback(() => {
    onClick({ id, parentId });
  }, []);

  return (
    <React.Fragment>
      <ArchiveBreadcrumb
        {...(isParent ? { link: true } : { active: true })}
        onClick={handleClick}
      >
        {name}
      </ArchiveBreadcrumb>
      {isParent && <Breadcrumb.Divider />}
    </React.Fragment>
  );
};

export default ArchiveLocationListItem;
