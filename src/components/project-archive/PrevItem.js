import React from 'react';
import { onlyUpdateForKeys } from 'recompose';
import { List } from 'semantic-ui-react';

const PrevItem = onlyUpdateForKeys([])(({ onClick }) => {
  return (
    <List>
      <List.Item>
        <List.Icon
          name="arrow left"
          size="big"
          style={{ cursor: 'pointer' }}
          onClick={onClick}
        />
        <List.Content>
          <List.Header as="a" onClick={onClick}>
            ......
          </List.Header>
        </List.Content>
      </List.Item>
    </List>
  );
});

export default PrevItem;
