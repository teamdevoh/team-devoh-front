import helpers from '../../helpers';

const fetchArchiveLocation = ({ projectId, parentId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/archives/${parentId}/location`
  );
};

const fetchTaskLocation = ({ projectId, parentTaskItemId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/tasks.structure/${parentTaskItemId}/location`
  );
};

const fetchArchives = ({ projectId, parentId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    parentId,
    search: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(`api/projects/${projectId}/archives?${params}`);
};

const fetchArchivesByTask = ({
  projectId,
  parentTaskItemId,
  search,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    parentTaskItemId,
    search: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projects/${projectId}/tasks.structure?${params}`
  );
};

const fetchAttachKeyValuePairs = ({ projectId, projectArchiveId }) => {
  const params = helpers.qs.stringify({});
  return helpers.Service.get(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach.hasnames?${params}`
  );
};

const fetchArchiveAttachs = ({
  projectId,
  projectArchiveId,
  search,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectArchiveId,
    search: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach?${params}`
  );
};

const fetchAttachHistories = ({
  projectId,
  projectArchiveId,
  projectAttachmentId
}) => {
  return helpers.Service.get(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach/${projectAttachmentId}/histories`
  );
};

const fetchArchive = ({ projectId, projectArchiveId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/archives/${projectArchiveId}`
  );
};

const fetchThumbnail = ({
  projectId,
  projectArchiveId,
  projectAttachmentId
}) => {
  return helpers.util.previewImage(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach.thumbnail/${projectAttachmentId}`,
    projectAttachmentId
  );
};

const fetchTaskFilesAll = ({ projectId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    name: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projects/${projectId}/tasks.attachs.all?${params}`
  );
};

const fetchArchiveFilesAll = ({ projectId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    name: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projects/${projectId}/archives.attachs.all?${params}`
  );
};

const download = ({
  projectId,
  projectArchiveId,
  projectAttachmentId,
  progress,
  resolve,
  reject
}) => {
  return helpers.util.download(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach/${projectAttachmentId}`,
    progress,
    resolve,
    reject
  );
};

const downloadByReq = ({
  projectId,
  projectArchiveId,
  projectAttachmentId,
  progress,
  resolve,
  reject
}) => {
  return helpers.util.download(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach/${projectAttachmentId}/req.download`,
    progress,
    resolve,
    reject
  );
};

const fetchArchivePresignedurl = ({
  projectId,
  projectArchiveId,
  projectAttachmentId
}) => {
  return helpers.Service.get(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach/${projectAttachmentId}/req.presignedurl`
  );
};

const addArchive = ({ projectId, model }) => {
  return helpers.Service.post(`api/projects/${projectId}/archives`, model);
};

const addArchiveAttach = ({ projectId, projectArchiveId, model }) => {
  return helpers.Service.post(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach`,
    model
  );
};

const updateArchive = ({ projectId, projectArchiveId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/archives/${projectArchiveId}`,
    model
  );
};

const updateAttachTag = ({
  projectId,
  projectArchiveId,
  projectAttachmentId,
  model
}) => {
  return helpers.Service.put(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach/${projectAttachmentId}`,
    model
  );
};

const removeArchive = ({ projectId, projectArchiveId }) => {
  return helpers.Service.delete(
    `api/projects/${projectId}/archives/${projectArchiveId}`
  );
};

const removeProjectAttach = ({
  projectId,
  projectArchiveId,
  projectAttachmentId
}) => {
  return helpers.Service.delete(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach/${projectAttachmentId}`
  );
};

const changeArchiveLocation = ({ projectId, projectArchiveId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/archives/${projectArchiveId}/location`,
    model
  );
};

const changeAttachLocation = ({ projectId, projectArchiveId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach/location`,
    model
  );
};

const addReqArchiveAttach = ({ projectId, projectArchiveId, model }) => {
  return helpers.Service.post(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach.req`,
    model
  );
};

const isAllowReqAttach = ({
  projectId,
  projectArchiveId,
  projectAttachmentId
}) => {
  return helpers.Service.get(
    `api/projects/${projectId}/archives/${projectArchiveId}/attach/${projectAttachmentId}/req.allow`
  );
};

const fetchAttachReqs = ({ projectId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    search: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(`api/projects/${projectId}/attach.req?${params}`);
};

const updateReqAllow = ({ projectId, id, projectArchiveId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/attach.req/${id}/allow?archiveId=${projectArchiveId}`,
    model
  );
};

export default {
  fetchArchiveLocation,
  fetchTaskLocation,
  fetchArchives,
  fetchArchivesByTask,
  fetchAttachKeyValuePairs,
  fetchArchiveAttachs,
  fetchAttachHistories,
  fetchArchive,
  fetchThumbnail,
  fetchTaskFilesAll,
  fetchArchiveFilesAll,
  fetchAttachReqs,
  isAllowReqAttach,
  download,
  downloadByReq,
  fetchArchivePresignedurl,
  addArchive,
  addArchiveAttach,
  addReqArchiveAttach,
  updateArchive,
  updateAttachTag,
  updateReqAllow,
  changeArchiveLocation,
  changeAttachLocation,
  removeArchive,
  removeProjectAttach
};
