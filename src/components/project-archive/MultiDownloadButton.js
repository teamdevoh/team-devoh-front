import React, { useState, useEffect } from 'react';
import { Button, List, Icon } from 'semantic-ui-react';
import { role } from '../../constants';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import api from './api';
import { notification } from '../modal';
import RequestPanel from './RequestPanel';
import { Modal } from '../modal';
import styled from 'styled-components';
import { StyledCheckbox } from '../modules/FileList';

const LListItemLable = styled.div`
  font-weight: bold;
`;

export const FileDownloadResult = ({
  fileInfo,
  succeedIds = [],
  failedIds = [],
  deniedIds = [],
  waitingIds = []
}) => {
  const t = useFormatMessage();

  return (
    <List>
      {succeedIds.length > 0 && (
        <List.Item value="*">
          <LListItemLable>{t('download.success')}</LListItemLable>
          <List.Item as="ol">
            {succeedIds.map(id => {
              const { name } = fileInfo[id];
              return (
                <List.Item as="li" key={id}>
                  {name}
                </List.Item>
              );
            })}
          </List.Item>
        </List.Item>
      )}
      {failedIds.length > 0 && (
        <List.Item value="*">
          <LListItemLable>{t('download.failed')}</LListItemLable>
          <List.Item as="ol">
            {failedIds.map(id => {
              const { name } = fileInfo[id];
              return (
                <List.Item as="li" key={id}>
                  {name}
                </List.Item>
              );
            })}
          </List.Item>
        </List.Item>
      )}
      {deniedIds.length > 0 && (
        <List.Item value="*">
          <LListItemLable>{t('download.rejected')}</LListItemLable>
          <List.Item as="ol">
            {deniedIds.map(id => {
              const { name } = fileInfo[id];
              return (
                <List.Item as="li" key={id}>
                  {name}
                </List.Item>
              );
            })}
          </List.Item>
        </List.Item>
      )}
      {waitingIds.length > 0 && (
        <List.Item value="*">
          <LListItemLable>{t('common.waiting')}</LListItemLable>
          <List.Item as="ol">
            {waitingIds.map(id => {
              const { name } = fileInfo[id];
              return (
                <List.Item as="li" key={id}>
                  {name}
                </List.Item>
              );
            })}
          </List.Item>
        </List.Item>
      )}
    </List>
  );
};

const MultiDownloadButton = ({
  projectId,
  projectArchiveId: propProjectArchiveId,
  getProjectArchiveId
}) => ({ selectedList, totalCnt, onCheckAllClick }) => {
  const t = useFormatMessage();
  const [loading, setLoading] = useState(false);
  const [downloadReq, setDownloadReq] = useState({
    show: false,
    ids: [],
    reason: null
  });
  const [downloadIds, setDownloadIds] = useState([]);
  const [result, setResult] = useState({
    succeedIds: [],
    failedIds: [],
    deniedIds: [],
    waitingIds: []
  });
  const [showResult, setShowResult] = useState(false);

  const handleDownloadByReq = ({ downloadIds }) => {
    setLoading(true);

    let downloadApi = null;
    if (!helpers.Identity.hasRole(role.ProjectArchive_Download)) {
      downloadApi = api.downloadByReq;
    } else {
      downloadApi = api.download;
    }

    for (let i = 0; i < downloadIds.length; i++) {
      const id = downloadIds[i];

      const projectArchiveId = propProjectArchiveId || getProjectArchiveId(id);

      downloadApi({
        projectId,
        projectArchiveId,
        projectAttachmentId: id,
        resolve: res => {
          if (res) {
            const newResult = {
              ...result
            };

            newResult.succeedIds.push(id);

            setResult(newResult);
          }
        },
        reject: err => {
          const newResult = {
            ...result
          };

          newResult.failedIds.push(id);

          setResult(newResult);
        }
      });
    }
  };

  const handleDownloadClick = async () => {
    if (Object.keys(selectedList).length > 0) {
      const downloadIds = [];

      setLoading(true);

      if (!helpers.Identity.hasRole(role.ProjectArchive_Download)) {
        const reqIds = [];
        const deniedIds = [];
        const waitingIds = [];

        for (let itemKey in selectedList) {
          if (selectedList.hasOwnProperty(itemKey)) {
            const { key: id } = selectedList[itemKey];
            const projectArchiveId =
              propProjectArchiveId || getProjectArchiveId(id);

            const res = await api.isAllowReqAttach({
              projectId,
              projectArchiveId,
              projectAttachmentId: id
            });

            if (res.data === null) {
              reqIds.push(id);
              downloadIds.push(id);
            } else {
              const isAllow = res.data.allow;

              if (isAllow === false) {
                deniedIds.push(id);
              }
              if (isAllow === null) {
                waitingIds.push(id);
              }

              if (isAllow) {
                downloadIds.push(id);
              }
            }
          }
        }

        const newResult = {
          ...result
        };

        newResult.deniedIds = deniedIds;
        newResult.waitingIds = waitingIds;

        setResult(newResult);

        if (reqIds.length > 0) {
          setDownloadIds([...downloadIds]);
          setDownloadReq({
            show: true,
            ids: [...reqIds],
            reason: null
          });
        } else {
          setDownloadIds([...downloadIds]);
          handleDownloadByReq({ downloadIds });
        }

        setLoading(false);
      } else {
        for (let itemKey in selectedList) {
          if (selectedList.hasOwnProperty(itemKey)) {
            const { key: id } = selectedList[itemKey];

            downloadIds.push(id);
          }
        }

        setDownloadIds([...downloadIds]);

        handleDownloadByReq({ downloadIds });
        setLoading(false);
      }
    }
  };

  const handleReqOKClick = async ({ id: ids, value: reason }) => {
    const succeedIds = [];
    setLoading(true);

    for (let i = 0; i < ids.length; i++) {
      const id = ids[i];
      const projectArchiveId = propProjectArchiveId || getProjectArchiveId(id);

      const res = await api.addReqArchiveAttach({
        projectId,
        projectArchiveId,
        model: { projectAttachmentId: id, reason }
      });

      if (res && res.data) {
        succeedIds.push(id);
      }
    }

    if (succeedIds.length > 0) {
      setDownloadReq({
        show: false,
        ids: [],
        reason: null
      });

      const fileNames = succeedIds.map(fileKey => {
        const { name } = selectedList[fileKey];

        return `<li>${name}</li>`;
      });

      const style = `
          text-align: left;
      `;

      notification.info({
        title: t('download.req.ok'),
        text: `<ol style="${style}">${fileNames.join('')}</ol>`,
        confirmButtonName: t('continue.download'),
        onClose: () => {
          handleDownloadByReq({ downloadIds });
        }
      });
    }
    setLoading(false);
  };

  const handleCancelClick = () => {
    setDownloadReq({
      show: false,
      ids: [],
      reason: null
    });
  };

  const handleResultOkClick = () => {
    setShowResult(false);
  };

  useEffect(
    () => {
      const { failedIds, succeedIds } = result;

      if (
        downloadIds.length > 0 &&
        downloadIds.length === failedIds.length + succeedIds.length
      ) {
        setShowResult(true);
      }
    },
    [result, downloadIds]
  );

  useEffect(
    () => {
      if (showResult === false) {
        setDownloadReq({
          show: false,
          ids: [],
          reason: null
        });
        setResult({
          succeedIds: [],
          failedIds: [],
          deniedIds: [],
          waitingIds: []
        });
      }
    },
    [showResult]
  );

  useEffect(
    () => {
      if (showResult) {
        setLoading(false);
      }
    },
    [showResult]
  );

  return (
    totalCnt > 0 && (
      <div>
        <StyledCheckbox
          indeterminate={
            Object.keys(selectedList).length > 0 &&
            totalCnt > Object.keys(selectedList).length
          }
          checked={
            Object.keys(selectedList).length > 0 &&
            totalCnt === Object.keys(selectedList).length
          }
          onChange={onCheckAllClick}
        />
        <Button
          disabled={Object.keys(selectedList).length === 0}
          onClick={handleDownloadClick}
          loading={loading}
          type="button"
        >
          <Icon name="attach" />
          {t('common.download')}
        </Button>
        <RequestPanel
          id={downloadReq.ids}
          show={downloadReq.show}
          onOKClick={handleReqOKClick}
          onCancelClick={handleCancelClick}
          loading={loading}
        />
        <Modal
          open={showResult}
          onOK={handleResultOkClick}
          title={t('common.result')}
          content={
            <FileDownloadResult
              fileInfo={selectedList}
              succeedIds={result.succeedIds}
              failedIds={result.failedIds}
              deniedIds={result.deniedIds}
              waitingIds={result.waitingIds}
            />
          }
        />
      </div>
    )
  );
};

export default MultiDownloadButton;
