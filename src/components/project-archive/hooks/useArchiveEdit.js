import { useState, useCallback, useEffect } from 'react';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { useFormatMessage } from '../../../hooks';
import { notification } from '../../modal';
import api from '../api';

const useArchiveEdit = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const t = useFormatMessage();
  const [isLoading, setIsLoading] = useState(false);
  const { projectId, parentId, projectArchiveId } = match.params;
  const [model, setModel] = useState({
    projectArchiveId: projectArchiveId,
    projectId: projectId,
    parentId: parentId,
    name: '',
    description: ''
  });

  let isSubscribed = false;
  useEffect(
    () => {
      isSubscribed = true;
      if (projectArchiveId > 0) {
        getItem();
      }
      return () => {
        isSubscribed = false;
      };
    },
    [projectArchiveId]
  );

  const getItem = useCallback(async () => {
    const res = await api.fetchArchive({ projectId, projectArchiveId });
    if (isSubscribed) {
      setModel(res.data);
    }
  }, []);

  const handleChange = useCallback(
    (event, { name, value }) => {
      if (model.hasOwnProperty(name)) {
        setModel({ ...model, [name]: value });
      }
    },
    [model]
  );

  const handleSubmit = useCallback(
    async () => {
      setIsLoading(true);
      const res = await api.updateArchive({
        projectId: model.projectId,
        projectArchiveId: model.projectArchiveId,
        model
      });

      if (res.data) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: () => {
            history.goBack();
          }
        });
      }
      setIsLoading(false);
    },
    [model]
  );

  return [
    {
      model,
      loading: isLoading,
      onChange: handleChange,
      onSubmit: handleSubmit
    },
    setModel
  ];
};

export default useArchiveEdit;
