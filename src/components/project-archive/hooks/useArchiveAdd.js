import { useState, useCallback } from 'react';
import api from '../api';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { useFormatMessage } from '../../../hooks';
import { notification } from '../../modal';

const useArchiveAdd = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const t = useFormatMessage();
  const [isLoading, setIsLoading] = useState(false);
  const { projectId, parentId } = match.params;
  const [model, setModel] = useState({
    projectArchiveId: 0,
    projectId: projectId,
    parentId: parentId,
    name: '',
    description: ''
  });

  const handleChange = useCallback(
    (event, { name, value }) => {
      if (model.hasOwnProperty(name)) {
        setModel({ ...model, [name]: value });
      }
    },
    [model]
  );

  const handleSubmit = useCallback(
    async () => {
      setIsLoading(true);
      const res = await api.addArchive({ projectId: model.projectId, model });
      if (res.data) {
        notification.success({
          title: t('common.alert.added'),
          onClose: () => {
            history.goBack();
          }
        });
      }
      setIsLoading(false);
    },
    [model]
  );

  return [
    {
      model,
      loading: isLoading,
      onChange: handleChange,
      onSubmit: handleSubmit
    },
    setModel
  ];
};

export default useArchiveAdd;
