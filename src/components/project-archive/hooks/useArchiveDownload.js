import { useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';
import { notification } from '../../modal';
import { useFormatMessage } from '../../../hooks';
import { role } from '../../../constants';

const useArchiveDownload = ({ projectId }) => {
  const [loading, setLoading] = useState(false);
  const [request, setRequest] = useState({ isRequire: false, id: 0 });
  const t = useFormatMessage();

  const download = async ({ id, projectArchiveId }) => {
    setLoading(true);
    let isAllow = false;
    if (!helpers.Identity.hasRole(role.ProjectArchive_Download)) {
      const res = await api.isAllowReqAttach({
        projectId,
        projectArchiveId,
        projectAttachmentId: id
      });

      if (res.data === null) {
        setRequest({ isRequire: true, id });
      } else {
        isAllow = res.data.allow;

        if (isAllow === false) {
          notification.warning({
            title: t('denied')
          });
        }
        if (isAllow === null) {
          notification.warning({
            title: t('common.waiting')
          });
        }

        if (isAllow === true) {
          api.downloadByReq({
            projectId,
            projectArchiveId,
            projectAttachmentId: id,
            progress: percent => {
              if (percent === 100) {
                setLoading(false);
              }
            }
          });
        }
      }
    } else {
      api.download({
        projectId,
        projectArchiveId,
        projectAttachmentId: id,
        progress: percent => {
          if (percent === 100) {
            setLoading(false);
          }
        }
      });
    }

    setLoading(false);
  };

  const addReq = async ({ id, projectArchiveId, reason }) => {
    setLoading(true);
    const res = await api.addReqArchiveAttach({
      projectId,
      projectArchiveId,
      model: { projectAttachmentId: id, reason }
    });

    if (res && res.data) {
      setRequest({ isRequire: false, id: 0 });
      download({ id, projectArchiveId });
    }
    setLoading(false);
  };

  const undoReq = async () => {
    setRequest({ isRequire: false, id: 0 });
  };

  return [
    {
      request,
      loading,
      onDownload: download,
      onAddReq: addReq,
      onUndoReq: undoReq
    },
    setRequest
  ];
};

export default useArchiveDownload;
