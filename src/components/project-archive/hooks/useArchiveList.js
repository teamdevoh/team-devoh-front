import { useState, useEffect, useCallback } from 'react';
import helpers from '../../../helpers';
import api from '../api';

const useArchive = ({ projectId, parentId, search, offset, limit }) => {
  let isSubscribed = false;
  const [isLoading, setIsLoading] = useState(true);
  const [items, setItems] = useState([]);
  const [paging, setPaging] = useState({
    offset: null,
    limit: null,
    hasMore: false
  });

  useEffect(() => {
    isSubscribed = true;
    getItems();
    return () => {
      isSubscribed = false;
    };
  }, []);

  const filtering = targetId => {
    setItems(
      items.filter(item => {
        return item.projectArchiveId !== targetId;
      })
    );
  };

  const getItems = useCallback(async () => {
    const res = await api.fetchArchives({
      projectId: projectId,
      parentId: parentId,
      offset,
      limit
    });
    if (isSubscribed && res.data) {
      const merged = items.concat(res.data.items);
      const ordered = helpers.util.sort(merged, 'name', 'asc');

      setItems(ordered);
      setPaging({
        ...paging,
        offset: merged.length,
        hasMore: merged.length < res.data.paging.total
      });
      setIsLoading(false);
    }
  }, []);

  const onRemove = async ({ selectedId }) => {
    setIsLoading(true);
    const res = await api.removeArchive({
      projectId,
      projectArchiveId: selectedId
    });

    if (res.data) {
      filtering(selectedId);
    }
    setIsLoading(false);

    return res.data;
  };

  const onMove = async ({ toParentId, selectedId }) => {
    if (toParentId.toString() !== parentId.toString()) {
      const res = await api.changeArchiveLocation({
        projectId,
        projectArchiveId: selectedId,
        model: { parentId: toParentId }
      });

      if (res && res.data) {
        filtering(selectedId);
        return true;
      } else {
        return false;
      }
    }
  };

  return [{ items, isLoading, onRemove, onMove }, setItems];
};

export default useArchive;
