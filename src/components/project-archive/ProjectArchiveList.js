import React, { useState, useEffect, useCallback } from 'react';
import { Grid, Container, List, Loader } from 'semantic-ui-react';
import { LinkButton } from '../button';
import { Confirm, notification, Modal } from '../modal';
import {
  api,
  ProjectArchiveHeader,
  ProjectArchiveUpload,
  ProjectArchiveMove,
  ArchiveLocationList,
  ArchiveList,
  PrevItem
} from './';
import { SearchInput } from '../input';
import { RoleAware } from '../auth';
import { useFormatMessage } from '../../hooks';
import { useArchiveList } from './hooks';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { role } from '../../constants';

const Buttons = ({ visibleAddFile, onAddFolderClick, onAddFileClick }) => {
  const t = useFormatMessage();
  return (
    <RoleAware allowedRole={role.ProjectArchive_Edit}>
      <Grid.Row>
        <Grid.Column>
          <LinkButton
            icon="plus"
            name={t('folder')}
            onClick={onAddFolderClick}
          />
          {visibleAddFile && (
            <LinkButton icon="plus" name={t('file')} onClick={onAddFileClick} />
          )}
        </Grid.Column>
      </Grid.Row>
    </RoleAware>
  );
};

const ProjectArchiveList = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const t = useFormatMessage();
  const [selectedId, setSelectedId] = useState(0);
  const [isOpenConfirm, setIsOpenConfirm] = useState(false);
  const [open, setOpen] = useState(false);

  const { projectId, parentId } = match.params;
  const [archive] = useArchiveList({
    projectId: projectId,
    parentId: parentId
  });

  const [move, setMove] = useState({
    open: false
  });

  const [locations, setLocations] = useState([
    {
      projectArchiveId: 0,
      parentId: 0,
      name: 'Home',
      description: ''
    }
  ]);

  let _prevId = 0;
  const _fields = {
    id: 'projectArchiveId',
    parentId: 'parentId',
    name: 'name',
    description: 'description'
  };

  let isSubscribed = true;

  useEffect(() => {
    fetchLocation();
    return () => (isSubscribed = false);
  }, []);

  const fetchLocation = useCallback(async () => {
    if (parentId > 0) {
      const res = await api.fetchArchiveLocation({ projectId, parentId });
      const merged = locations.concat(res.data);
      if (isSubscribed) {
        setLocations(merged);

        _prevId =
          res.data.length > 1
            ? res.data[res.data.length - 2].projectArchiveId
            : 0;
      }
    }
  }, []);

  const handleRemove = useCallback(
    async () => {
      const is = await archive.onRemove({ selectedId: selectedId });
      if (is) {
        notification.success({
          title: t('common.alert.deleted'),
          onClose: () => {
            setIsOpenConfirm(false);
          }
        });
      }
    },
    [selectedId]
  );

  const handleMove = useCallback(
    async toParentId => {
      const is = await archive.onMove({ toParentId, selectedId });
      if (is) {
        setSelectedId(0);
        setMove({
          ...move,
          open: false
        });
      }
    },
    [selectedId]
  );

  const handleLocationClick = useCallback(({ id, parentId }) => {
    history.push(`/project/${projectId}/archive/${id}`);
  }, []);

  const handleNextClick = useCallback(id => {
    history.push(`/project/${projectId}/archive/${id}`);
  }, []);

  const handleGoTaskArchive = useCallback(() => {
    history.push(`/project/${projectId}/archivetask/${0}`);
  }, []);

  const handlePrevClick = useCallback(() => {
    history.push(`/project/${projectId}/archive/${_prevId}`);
  }, []);

  const handleAddFolderClick = useCallback(() => {
    history.push(`/project/${projectId}/archive/${parentId}/new`);
  }, []);

  const handleAddFileClick = useCallback(() => {
    setOpen(true);
  }, []);

  const handleUploadClose = useCallback(() => {
    setOpen(false);
  }, []);

  const handleRemoveConfirmClose = useCallback(() => {
    setIsOpenConfirm(false);
  }, []);

  const handleEditClick = useCallback(id => {
    history.push(`/project/${projectId}/archive/${parentId}/edit/${id}`);
  }, []);

  const handleMoveClick = useCallback(id => {
    setSelectedId(id);
    setMove({ ...move, open: true });
  }, []);

  const handleDeleteClick = useCallback(id => {
    setSelectedId(id);
    setIsOpenConfirm(true);
  }, []);

  const handleGoSearch = useCallback(value => {
    if (value && value.length > 0) {
      history.push(`/project/${projectId}/archive-search?value=${value}`);
    }
  }, []);

  return (
    <Container className="margin-0">
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <ProjectArchiveHeader />
              <SearchInput onClick={handleGoSearch} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <ArchiveLocationList
                fields={_fields}
                items={locations}
                onClick={handleLocationClick}
              />
            </Grid.Column>
          </Grid.Row>
          <Buttons
            visibleAddFile={parentId > 0 ? true : false}
            onAddFolderClick={handleAddFolderClick}
            onAddFileClick={handleAddFileClick}
          />

          <Grid.Row style={{ minHeight: '30em' }}>
            <Grid.Column>
              {parentId.toString() === '0' && (
                <List>
                  <List.Item>
                    <List.Icon
                      name="folder"
                      size="big"
                      style={{ cursor: 'pointer' }}
                      onClick={handleGoTaskArchive}
                    />
                    <List.Content>
                      <List.Header as="a" onClick={handleGoTaskArchive}>
                        {t('task')}
                      </List.Header>
                    </List.Content>
                  </List.Item>
                </List>
              )}

              {parentId > 0 && <PrevItem onClick={handlePrevClick} />}
              <ArchiveList
                fields={_fields}
                parentId={parentId}
                items={archive.items}
                onNextClick={handleNextClick}
                onEditClick={handleEditClick}
                onMoveClick={handleMoveClick}
                onDeleteClick={handleDeleteClick}
              />

              {archive.isLoading && <Loader active inline="centered" />}

              {parentId > 0 && (
                <ProjectArchiveUpload
                  open={open}
                  onClose={handleUploadClose}
                  projectId={projectId}
                  projectArchiveId={parentId}
                  history={history}
                />
              )}
              <Confirm
                title={t('folder.remove.title')}
                content={t('folder.remove.description')}
                open={isOpenConfirm}
                onOK={handleRemove}
                loading={archive.isLoading}
                onCancel={handleRemoveConfirmClose}
              />
              <Modal
                open={move.open}
                onOK={() => {
                  setSelectedId(0);
                  setMove({ ...move, open: false });
                }}
                title={'Move'}
                content={
                  <ProjectArchiveMove
                    projectId={projectId}
                    projectArchiveId={selectedId}
                    onClick={handleMove}
                  />
                }
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </Container>
  );
};

export default ProjectArchiveList;
