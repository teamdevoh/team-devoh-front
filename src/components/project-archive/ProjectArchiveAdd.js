import React from 'react';
import { Grid, Container } from 'semantic-ui-react';
import { ProjectArchiveHeader, ProjectArchiveForm } from './';
import { BackButton, SaveButton } from '../button';
import { useFormatMessage } from '../../hooks';
import { useArchiveAdd } from './hooks';
import { role } from '../../constants';

const ProjectArchiveAdd = ({ match, history }) => {
  const t = useFormatMessage();
  const [archive] = useArchiveAdd();

  return (
    <Container>
      <Grid>
        <Grid.Column width="16">
          <ProjectArchiveHeader />
        </Grid.Column>
        <Grid.Column width="16">
          <ProjectArchiveForm
            model={archive.model}
            onChange={archive.onChange}
            onSubmit={archive.onSubmit}
          >
            <BackButton name={t('common.cancel')} />
            <SaveButton
              allowedRole={role.ProjectArchive_Edit}
              name={t('common.save')}
              loading={archive.loading}
            />
          </ProjectArchiveForm>
        </Grid.Column>
      </Grid>
    </Container>
  );
};

export default ProjectArchiveAdd;
