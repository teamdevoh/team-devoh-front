import React, { useCallback, useState, useEffect, memo } from 'react';
import history from '../../helpers/history';
import api from './api';
import taskApi from '../task-manage/api';
import { Confirm } from '../modal';
import helpers from '../../helpers';
import { FileList } from '../modules';
import { Loader } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import find from 'lodash/find';
import { useInView } from 'react-intersection-observer';
import MultiDownloadButton from '../modules/MultiDownloadButton';
import FileViewerWrapper from '../viewer/FileViewerWrapper';

const TaskFileListContainer = ({ projectId, keyword }) => {
  const [ref, inView, entry] = useInView({
    root: null,
    threshold: 0
  });
  const t = useFormatMessage();
  const [search, setSearch] = useState({
    open: false,
    tag: '',
    location: ''
  });
  const [files, setFiles] = useState([]);
  const [total, setTotal] = useState(0);
  const [offset, setOffset] = useState(0);
  const _limit = 15;

  const [fileInfo, setFileInfo] = useState();
  const [viewerOpen, setViewerOpen] = useState(false);

  useEffect(() => {
    fetchItems({ offset: 0 });

    return () => {};
  }, []);

  const fetchItems = useCallback(
    async ({ offset }) => {
      const res = await api.fetchTaskFilesAll({
        projectId: projectId,
        search: keyword,
        offset: offset,
        limit: _limit
      });

      let items = [];
      await res.data.items.forEach(async item => {
        const fileObj = await helpers.file.setFileInfo({
          ...item,
          id: item.taskAttachmentId,
          thumbnailApiUrl: `api/tasks/${item.taskItemId}/attach.thumbnail/${
            item.taskAttachmentId
          }`
        });
        items.push({
          ...fileObj,
          taskItemId: item.taskItemId
        });
      });

      const merged = files.concat(items);
      setFiles(merged);
      setTotal(res.data.paging.total);
      setOffset(offset + _limit);
    },
    [files]
  );

  const foundItem = useCallback(
    key => {
      const item = find(files, { key });

      if (item === void 0) {
        throw new Error('please check key data');
      }

      return item;
    },
    [files]
  );

  const handleDownload = useCallback(
    id => {
      const item = foundItem(id);
      // setIsLoading(true);
      helpers.util
        .download(`api/tasks/${item.taskItemId}/attach/${id}`, percent => {
          if (percent === 100) {
            // setIsLoading(false);
          }
        })
        .catch(err => {
          // setIsLoading(false);
        });
    },
    [files]
  );

  const handleUpdateTag = useCallback(
    async (id, tags) => {
      const joinedTags = tags.join('');
      const item = foundItem(id);
      await taskApi.updateAttachTag({
        taskItemId: item.taskItemId,
        taskAttachmentId: id,
        model: { tag: joinedTags }
      });
      setFiles(
        files.map((file, index) => {
          if (file.key === id) {
            file.tags = joinedTags;
          }
          return file;
        })
      );
    },
    [files]
  );

  const handleRemoveAttach = useCallback(
    id => {
      const item = foundItem(id);
      taskApi
        .removeTaskAttach({ taskItemId: item.taskItemId, taskAttachmentId: id })
        .then(res => {
          if (res.data) {
            setFiles(
              files.filter(file => {
                return file.key !== id;
              })
            );
          }
        });
    },
    [files]
  );

  const handleTagClick = useCallback(tag => {
    setSearch({
      open: true,
      tag: tag,
      location: `/project/${projectId}/search?keyword=${tag}`
    });
  }, []);

  const handleShowFileLocation = useCallback(
    ({ id, name }) => {
      const item = foundItem(id);
      history.push(`/project/${projectId}/archivetask/${item.taskItemId}`);
    },
    [files]
  );

  useEffect(
    () => {
      if (inView && files.length < total && entry.isIntersecting) {
        fetchItems({ offset });
      }
    },
    [inView]
  );

  const getDownloadApi = useCallback(
    id => {
      const item = foundItem(id);
      return `api/tasks/${item.taskItemId}/attach/${id}`;
    },
    [files]
  );

  const handleViewerClick = useCallback(
    async ({ fileInfo = viewer, id }) => {
      setFileInfo(fileInfo);
      setViewerOpen(true);
    },
    [files]
  );

  const fetchSignedUrl = fileInfo => {
    const item = foundItem(fileInfo.key);

    return taskApi.fetchArchivePresignedurl({
      taskItemId: item.taskItemId,
      taskAttachmentId: fileInfo.key
    });
  };

  const handleViewerClose = () => {
    setViewerOpen(false);
  };

  const getDownloadUrl = useCallback(
    key => {
      const item = foundItem(key);
      return `api/tasks/${item.taskItemId}/attach/${key}`;
    },
    [files]
  );

  return (
    <div>
      <FileViewerWrapper
        fileInfo={fileInfo}
        fetchSignedUrl={fetchSignedUrl}
        open={viewerOpen}
        onClose={handleViewerClose}
        getDownloadUrl={getDownloadUrl}
      />
      <FileList
        files={files}
        onDownloadClick={handleDownload}
        onDeleteClick={handleRemoveAttach}
        onUpdateTag={handleUpdateTag}
        onTagClick={handleTagClick}
        userContextMenus={[
          {
            onClick: handleShowFileLocation,
            icon: { name: 'folder outline', color: 'black' },
            name: t('common.show.file.location')
          }
        ]}
        MultiDownloadButton={MultiDownloadButton({ getDownloadApi })}
        onViewerClick={handleViewerClick}
      />
      <div ref={ref}>
        <Loader
          active={files.length > 0 && files.length < total}
          inline="centered"
          size="mini"
        />
      </div>
      <Confirm
        title={t('common.search')}
        content={t('project.search.is', { name: search.tag })}
        open={search.open}
        onOK={() => {
          history.push(search.location);
        }}
        onCancel={() => setSearch({ ...search, open: false })}
      />
    </div>
  );
};

export default memo(TaskFileListContainer);
