import React, { useState, useCallback, useEffect } from 'react';
import { Container, Grid } from 'semantic-ui-react';
import {
  ProjectArchiveHeader,
  ArchiveList,
  PrevItem,
  ArchiveLocationList,
  api
} from './';
import { RoleAware } from '../auth';
import TaskUpload from '../task-manage/TaskUpload';
import { LinkButton } from '../button';
import { SearchInput } from '../input';
import helpers from '../../helpers';
import { role } from '../../constants';

const AddFileButton = ({ onClick }) => {
  return (
    <Grid.Row>
      <Grid.Column>
        <LinkButton
          color="blue"
          icon="plus"
          className="basic"
          name={'File'}
          onClick={onClick}
        />
      </Grid.Column>
    </Grid.Row>
  );
};

const ProjectArchiveListByTask = ({ match, history }) => {
  const _fields = {
    id: 'taskItemId',
    parentId: 'parentTaskItemId',
    name: 'title',
    description: 'note'
  };

  let _prevId = 0;

  const [isOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [locations, setLocations] = useState([
    {
      taskItemId: 0,
      parentTaskItemId: null,
      title: 'Home',
      note: '',
      isTask: false
    },
    {
      taskItemId: 0,
      parentTaskItemId: 0,
      title: 'Task',
      note: '',
      isTask: false
    }
  ]);
  const { projectId, parentId } = match.params;

  useEffect(() => {
    fetchLocation();
    fetchItems();
    return () => {};
  }, []);

  const fetchLocation = useCallback(async () => {
    const res = await api.fetchTaskLocation({
      projectId,
      parentTaskItemId: parentId
    });
    const merged = locations.concat(res.data);
    setLocations(merged);
    _prevId =
      res.data.length > 1 ? res.data[res.data.length - 2].taskItemId : 0;
  }, []);

  const fetchItems = useCallback(async () => {
    setIsLoading(true);
    const res = await api.fetchArchivesByTask({
      projectId,
      parentTaskItemId: parentId
    });
    if (res.data) {
      const ordered = helpers.util.sort(res.data.items, 'title', 'asc');
      setItems(ordered);
    }
    setIsLoading(false);
  }, []);

  const handleLocationClick = useCallback(({ id, parentId }) => {
    if (parentId === null) {
      history.push(`/project/${projectId}/archive/${0}`);
    } else {
      history.push(`/project/${projectId}/archivetask/${id}`);
    }
  }, []);

  const handleNextClick = useCallback(id => {
    history.push(`/project/${projectId}/archivetask/${id}`);
  }, []);

  const handlePrevClick = useCallback(() => {
    if (parentId.toString() === '0') {
      history.push(`/project/${projectId}/archive/${0}`);
    } else {
      history.push(`/project/${projectId}/archivetask/${_prevId}`);
    }
  }, []);

  const handleAddFileClick = useCallback(() => {
    setIsOpen(true);
  }, []);

  const handleUploadClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  const handleGoSearch = useCallback(value => {
    if (value && value.length > 0) {
      history.push(`/project/${projectId}/archive-search?value=${value}`);
    }
  }, []);

  return (
    <Container>
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <ProjectArchiveHeader />
              <SearchInput onClick={handleGoSearch} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <ArchiveLocationList
                fields={_fields}
                items={locations}
                onClick={handleLocationClick}
              />
            </Grid.Column>
          </Grid.Row>
          {items.length === 0 &&
            !isLoading && <AddFileButton onClick={handleAddFileClick} />}
          <RoleAware allowedRole={role.Task_Read}>
            <Grid.Row style={{ minHeight: '30em' }}>
              <Grid.Column>
                <PrevItem onClick={handlePrevClick} />
                <ArchiveList
                  fields={_fields}
                  parentId={parentId}
                  items={items}
                  onNextClick={handleNextClick}
                />
                {locations[locations.length - 1].isTask && (
                  <TaskUpload
                    taskItemId={parentId}
                    projectId={projectId}
                    isUpload={isOpen}
                    onUploadClose={handleUploadClose}
                  />
                )}
              </Grid.Column>
            </Grid.Row>
          </RoleAware>
        </Grid>
      </div>
    </Container>
  );
};

export default ProjectArchiveListByTask;
