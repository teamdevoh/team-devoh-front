import React from 'react';
import { Divider, Form, Input, TextArea } from 'semantic-ui-react';
import { useFormatMessage, useSubmit } from '../../hooks';

const ProjectArchiveForm = ({
  children,
  model = { name: '' },
  onChange,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  return (
    <div>
      <Form onSubmit={onValidate}>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('folder.name')}</label>
            <Input name="name" value={model.name} onChange={onChange} />
            {validator.message(t('folder.name'), model.name, 'required')}
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label>{t('folder.description')}</label>
            <TextArea
              name="description"
              value={model.description}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Divider />
        {children}
      </Form>
    </div>
  );
};

export default ProjectArchiveForm;
