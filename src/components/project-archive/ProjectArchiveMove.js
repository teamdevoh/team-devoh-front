import React, { useCallback, useState, useEffect } from 'react';
import { Grid, List, Container, Loader } from 'semantic-ui-react';
import { api } from './';
import { RoleAware } from '../auth';
import { SaveButton } from '../button';
import { useFormatMessage } from '../../hooks';
import map from 'lodash/map';
import { role } from '../../constants';

const ProjectArchiveMove = ({ projectId, onClick }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [parentId, setParentId] = useState(0);
  const [parentHistories, setParentHistories] = useState([]);

  useEffect(
    () => {
      getProjectArchives();
      return () => {};
    },
    [parentId]
  );

  const getProjectArchives = useCallback(() => {
    setIsLoading(true);
    api
      .fetchArchives({
        projectId: projectId,
        parentId: parentId
      })
      .then(res => {
        setIsLoading(false);
        setItems(res.data.items);
      });
  });

  const addParentHistory = useCallback(
    id => {
      const data = parentHistories;
      setParentHistories(data.concat(id));
    },
    [parentHistories]
  );

  const getParent = useCallback(
    () => {
      let data = parentHistories;
      const parentId = data[data.length - 1];
      data.splice(data.length - 1, 1);
      setParentHistories(data);
      return parentId;
    },
    [parentHistories]
  );

  const handleClick = useCallback(
    () => {
      onClick(parentId);
    },
    [parentId]
  );

  return (
    <Container>
      <div>
        <Grid>
          <RoleAware allowedRole={role.ProjectArchive_Edit}>
            <Grid.Row>
              <Grid.Column>
                <MoveButton onClick={handleClick} />
              </Grid.Column>
            </Grid.Row>
          </RoleAware>
          <RoleAware allowedRole={role.ProjectArchive_Edit}>
            <Grid.Row style={{ minHeight: '18em' }}>
              <Grid.Column>
                <List relaxed>
                  {parentHistories.length > 0 && (
                    <Top
                      onClick={() => {
                        setParentId(getParent());
                      }}
                    />
                  )}
                  {map(items, item => {
                    return (
                      <Item
                        key={item.projectArchiveId}
                        id={item.projectArchiveId}
                        name={item.name}
                        description={item.description}
                        onClick={id => {
                          addParentHistory(parentId);
                          setParentId(id);
                        }}
                      />
                    );
                  })}
                </List>
                {isLoading && <Loader active inline="centered" />}
              </Grid.Column>
            </Grid.Row>
          </RoleAware>
        </Grid>
      </div>
    </Container>
  );
};

const MoveButton = ({ onClick }) => {
  const t = useFormatMessage();
  return (
    <SaveButton
      allowedRole={role.ProjectArchive_Edit}
      name={t('folder.move.here')}
      loading={false}
      onClick={onClick}
    />
  );
};

const Top = ({ onClick }) => {
  return (
    <List.Item>
      <List.Icon name="arrow left" size="big" />
      <List.Content>
        <List.Header as="a" onClick={onClick}>
          .....
        </List.Header>
      </List.Content>
    </List.Item>
  );
};

const Item = ({ id, name, description, onClick }) => {
  const handleClick = useCallback(() => {
    onClick(id);
  }, []);

  return (
    <List.Item>
      <List.Icon name="folder" size="big" />
      <List.Content>
        <List.Header
          as="a"
          onClick={handleClick}
          style={{ wordBreak: 'break-word' }}
        >
          {name}
        </List.Header>
        <List.Description style={{ minHeight: '16px' }}>
          {description}
        </List.Description>
      </List.Content>
    </List.Item>
  );
};

export default ProjectArchiveMove;
