import React, { useCallback } from 'react';
import { List } from 'semantic-ui-react';
import { ContextMenuList } from '../modules';
import { ListItemHeaderWrapper, ListItemDescriptionWrapper } from './styles';

const ArchiveListItem = ({ name, description, contextItems, id, onClick }) => {
  const handleClick = useCallback(() => {
    onClick(id);
  }, []);

  return (
    <List.Item>
      <List.Content floated="right">
        <ContextMenuList items={contextItems} />
      </List.Content>
      <List.Icon name="folder" size="big" />
      <List.Content>
        <ListItemHeaderWrapper onClick={handleClick}>
          {name}
        </ListItemHeaderWrapper>
        <ListItemDescriptionWrapper>{description}</ListItemDescriptionWrapper>
      </List.Content>
    </List.Item>
  );
};

export default ArchiveListItem;
