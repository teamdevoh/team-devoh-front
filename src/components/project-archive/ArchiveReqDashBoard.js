import React, { useCallback, useEffect, useState, Fragment } from 'react';
import {
  List,
  Button,
  Container,
  Icon,
  Header,
  Segment,
  Loader
} from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import helpers from '../../helpers';
import api from './api';
import { notification } from '../modal';
import { useFormatMessage, useFace } from '../../hooks';
import styled from 'styled-components';
import orderBy from 'lodash/orderBy';
import moment from 'moment';
import { useInView } from 'react-intersection-observer';
import { LazyImage } from '../modules';
import ProjectArchiveHeader from './ProjectArchiveHeader';
import { format } from '../../constants';

const ItemWrapper = styled(List.Item)`
  &&& {
    margin-bottom: 1em;
    border-bottom: solid 1px #999;
  }
`;

const ReasonWrapper = styled.div`
  margin-top: 1em;
  margin-bottom: 1em;
`;
const ReasonBody = styled(Segment)`
  &&& {
    background: #e5e5e5;
    color: #232323;
  }
`;

const MarginBottomWrapper = styled.div`
  margin-bottom: 0.5em;
`;

const FilePanel = ({ name, onClick }) => {
  return (
    <Header as="h3">
      <a onClick={onClick}>
        <Icon name="linkify" size="small" />
      </a>
      <Header.Content>{name}</Header.Content>
    </Header>
  );
};

const ReasonPanel = ({ text }) => {
  return (
    <ReasonWrapper>
      <ReasonBody>
        {text.split('\n').map((singleText, index) => {
          return (
            <span key={index}>
              {singleText}
              <br />
            </span>
          );
        })}
      </ReasonBody>
    </ReasonWrapper>
  );
};

const RequestPanel = ({ name, faceId, date, onClick }) => {
  const t = useFormatMessage();
  const [faceUrl] = useFace(faceId);

  return (
    <MarginBottomWrapper>
      <LazyImage src={faceUrl} avatar={true} />
      <a onClick={onClick}>
        {t('project.archive.req.log', {
          user: name,
          date: moment(date, format.YYYYMMDDHHMMSS).fromNow()
        })}
      </a>
    </MarginBottomWrapper>
  );
};

const ResponsePanel = ({ name, faceId, isAllow, date, onClick }) => {
  const t = useFormatMessage();
  const [faceUrl] = useFace(faceId);

  return (
    <MarginBottomWrapper>
      {isAllow !== null && (
        <Fragment>
          <LazyImage src={faceUrl} avatar={true} />
          <a onClick={onClick}>
            {t('project.archive.res.log', {
              user: name,
              date: moment(date, format.YYYYMMDDHHMMSS).fromNow(),
              respond: isAllow ? t('allowed') : t('denied')
            })}
          </a>
        </Fragment>
      )}
    </MarginBottomWrapper>
  );
};

const Item = ({
  id,
  allower,
  fileName,
  requester,
  requesterMemberId,
  requesterFaceId,
  reqDate,
  resDate,
  isAllow,
  projectArchiveId,
  allowMemberId,
  allowerFaceId,
  reason,
  onAllowClick,
  onDenyClick
}) => {
  const t = useFormatMessage();
  const params = useParams();

  const handleFileNameClick = useCallback(() => {
    helpers.history.push(
      `/project/${params.projectId}/archive/${projectArchiveId}`
    );
  }, []);

  const handleReqMemberClick = useCallback(() => {
    helpers.history.push(
      `/project/${params.projectId}/member/edit/${requesterMemberId}`
    );
  }, []);

  const handleResMemberClick = useCallback(() => {
    helpers.history.push(
      `/project/${params.projectId}/member/edit/${allowMemberId}`
    );
  }, []);

  const handleAllowClick = useCallback(() => {
    onAllowClick(id, projectArchiveId);
  }, []);

  const handleDenyClick = useCallback(() => {
    onDenyClick(id, projectArchiveId);
  }, []);

  return (
    <ItemWrapper>
      <List.Content floated="right">
        <Button positive size="small" onClick={handleAllowClick}>
          {t('allow')}
        </Button>
        <Button negative size="small" onClick={handleDenyClick}>
          {t('deny')}
        </Button>
      </List.Content>
      <List.Content>
        <FilePanel name={fileName} onClick={handleFileNameClick} />
        <Container textAlign="justified">
          <ReasonPanel text={reason} />
          <RequestPanel
            name={requester}
            faceId={requesterFaceId}
            date={reqDate}
            onClick={handleReqMemberClick}
          />
          <ResponsePanel
            name={allower}
            faceId={allowerFaceId}
            date={resDate}
            isAllow={isAllow}
            onClick={handleResMemberClick}
          />
        </Container>
      </List.Content>
    </ItemWrapper>
  );
};

const ArchiveReqDashBoard = () => {
  const params = useParams();
  const t = useFormatMessage();
  const limit = 5;
  const [items, setItems] = useState([]);
  const [total, setTotal] = useState(0);
  const [start, setStart] = useState(0);
  const [ref, inView, entry] = useInView({
    root: null,
    threshold: 0
  });

  useEffect(() => {
    getItems({ offset: 0 });
  }, []);

  useEffect(
    () => {
      if (inView && items.length < total && entry.isIntersecting) {
        getItems({ offset: start });
      }
    },
    [inView]
  );

  const getItems = async ({ offset }) => {
    const res = await api.fetchAttachReqs({
      projectId: params.projectId,
      offset,
      limit
    });
    if (res && res.data) {
      const ordered = orderBy(res.data.items, ['reqDate'], ['desc']);
      const merged = items.concat(ordered);
      setItems(merged);
      setTotal(res.data.paging.total);
      setStart(offset + limit);
    }
  };

  const handleAllowClick = useCallback(
    async (id, projectArchiveId) => {
      const res = await api.updateReqAllow({
        projectId: params.projectId,
        id,
        projectArchiveId,
        model: { isAllow: true }
      });

      if (res && res.data) {
        showSaved(id, true);
      }
    },
    [items]
  );

  const handleDenyClick = useCallback(
    async (id, projectArchiveId) => {
      const res = await api.updateReqAllow({
        projectId: params.projectId,
        id,
        projectArchiveId,
        model: { isAllow: false }
      });

      if (res && res.data) {
        showSaved(id, false);
      }
    },
    [items]
  );

  const showSaved = (id, isAllow) => {
    notification.success({
      title: t('common.alert.changed'),
      onClose: () => {
        const data = items.map(item => {
          if (item.id === id) {
            item.isAllow = isAllow;
          }
          return item;
        });
        setItems(data);
      }
    });
  };

  return (
    <Container className="margin-0">
      <ProjectArchiveHeader />
      <List verticalAlign="middle">
        {items.map(item => {
          return (
            <Item
              key={item.id}
              onAllowClick={handleAllowClick}
              onDenyClick={handleDenyClick}
              {...item}
            />
          );
        })}
      </List>
      <div ref={ref}>
        <Loader
          active={items.length > 0 && items.length < total}
          inline="centered"
          size="mini"
        />
      </div>
    </Container>
  );
};

export default ArchiveReqDashBoard;
