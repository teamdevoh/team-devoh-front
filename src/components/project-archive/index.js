import { asyncComponent } from '../modules';

const ProjectArchiveHeader = asyncComponent(() =>
  import('./ProjectArchiveHeader')
);
const ProjectArchiveAdd = asyncComponent(() =>
  import('./ProjectArchiveAdd')
);
const ProjectArchiveList = asyncComponent(() =>
  import('./ProjectArchiveList')
);
const ProjectArchiveForm = asyncComponent(() =>
  import('./ProjectArchiveForm')
);
const ProjectArchiveUpload = asyncComponent(() =>
  import('./ProjectArchiveUpload')
);
const ProjectAttachHistoryList = asyncComponent(() =>
  import('./ProjectAttachHistoryList')
);
const ProjectArchiveMove = asyncComponent(() =>
  import('./ProjectArchiveMove')
);
const ChoiceOverwrite = asyncComponent(() =>
  import('./ChoiceOverwrite')
);
const ArchiveListItem = asyncComponent(() =>
  import('./ArchiveListItem')
);
const ArchiveLocationList = asyncComponent(() =>
  import('./ArchiveLocationList')
);
const ArchiveLocationListItem = asyncComponent(() =>
  import('./ArchiveLocationListItem')
);
const ArchiveList = asyncComponent(() =>
  import('./ArchiveList')
);
const PrevItem = asyncComponent(() =>
  import('./PrevItem')
);
const TaskFileListContainer = asyncComponent(() =>
  import('./TaskFileListContainer')
);
const ArchiveFileListContainer = asyncComponent(() =>
  import('./ArchiveFileListContainer')
);

export {
  ProjectArchiveHeader,
  ProjectArchiveAdd,
  ProjectArchiveList,
  ProjectArchiveForm,
  ProjectArchiveUpload,
  ProjectArchiveMove,
  ChoiceOverwrite,
  ArchiveListItem,
  ArchiveLocationList,
  ArchiveLocationListItem,
  ArchiveList,
  PrevItem,
  TaskFileListContainer,
  ArchiveFileListContainer
};
export { default as api } from './api';
