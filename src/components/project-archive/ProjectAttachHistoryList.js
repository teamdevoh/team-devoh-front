import React, { useCallback, useState, useEffect } from 'react';
import { FileList } from '../modules';
import { api, ProjectArchiveHeader } from './';
import { Container, Grid, Header, Loader } from 'semantic-ui-react';
import { Confirm } from '../modal';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import AnchorBack from '../button/AnchorBack';
import map from 'lodash/map';
import { useRouteMatch } from 'react-router-dom';
import { useArchiveDownload } from './hooks';
import RequestPanel from './RequestPanel';

const HistoryHeader = () => {
  const t = useFormatMessage();

  return (
    <React.Fragment>
      <Header as="h2">{t('history')}</Header>
      <AnchorBack />
    </React.Fragment>
  );
};

const ProjectAttachHistoryList = ({ history }) => {
  const match = useRouteMatch();
  const { projectId, parentId, projectAttachmentId } = match.params;
  const t = useFormatMessage();
  const [attachedFiles, setAttachedFiles] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [search, setSearch] = useState({
    tag: '',
    location: ''
  });
  const [modalOpen, setModalOpen] = useState(false);
  const [archive] = useArchiveDownload({ projectId: projectId });

  useEffect(() => {
    getItems();
    return () => {};
  }, []);

  const getItems = useCallback(async () => {
    setIsLoading(true);
    const res = await api.fetchAttachHistories({
      projectId,
      projectArchiveId: parentId,
      projectAttachmentId: projectAttachmentId
    });

    const files = [];
    await map(res.data, async item => {
      const fileObj = await helpers.file.setFileInfo({
        ...item,
        id: item.projectAttachmentId,
        thumbnailApiUrl: `api/projects/${projectId}/archives/${parentId}/attach.thumbnail/${
          item.projectAttachmentId
        }`
      });

      files.push(fileObj);
    });

    setIsLoading(false);
    setAttachedFiles(files);
  }, []);

  const handleDownload = useCallback(async id => {
    await archive.onDownload({ id, projectArchiveId: parentId });
  }, []);

  const handleTagClick = useCallback(tag => {
    setSearch({
      tag: tag,
      location: `/project/${projectId}/search?keyword=${tag}`
    });
    setModalOpen(true);
  }, []);

  const handleOK = useCallback(
    () => {
      history.push(search.location);
    },
    [search.location]
  );

  const handleCancel = useCallback(() => {
    setModalOpen(false);
  }, []);

  const handleReqOKClick = useCallback(async data => {
    await archive.onAddReq({
      id: data.id,
      projectArchiveId: parentId,
      reason: data.value
    });
  }, []);

  return (
    <Container>
      <RequestPanel
        id={archive.request.id}
        show={archive.request.isRequire}
        onOKClick={handleReqOKClick}
        onCancelClick={archive.onUndoReq}
        loading={archive.loading}
      />
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <ProjectArchiveHeader />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <HistoryHeader />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={{ minHeight: '18em' }}>
            <Grid.Column>
              <FileList
                files={attachedFiles}
                onDownloadClick={handleDownload}
                onTagClick={handleTagClick}
              />
              {isLoading && <Loader active inline="centered" />}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
      <Confirm
        title={t('common.search')}
        content={t('project.search.is', { name: search.tag })}
        open={modalOpen}
        onOK={handleOK}
        onCancel={handleCancel}
      />
    </Container>
  );
};

export default ProjectAttachHistoryList;
