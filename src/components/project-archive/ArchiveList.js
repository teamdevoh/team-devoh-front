import React, { useCallback } from 'react';
import { List } from 'semantic-ui-react';
import { ArchiveListItem } from './';
import { useFormatMessage } from '../../hooks';
import map from 'lodash/map';

const ArchiveList = ({
  fields,
  items,
  onEditClick,
  onMoveClick,
  onDeleteClick,
  onNextClick
}) => {
  const t = useFormatMessage();
  const handleNextClick = useCallback(id => {
    onNextClick(id);
  }, []);

  return (
    <List relaxed>
      {map(items, item => {
        const id = item[fields.id];
        const name = item[fields.name];
        const description = item[fields.description];

        let contextItems = [];
        if (onEditClick) {
          contextItems.push({
            id: id,
            name: t('folder.edit'),
            icon: 'edit',
            onClick: onEditClick
          });
        }

        if (onMoveClick) {
          contextItems.push({
            id: id,
            name: t('folder.move'),
            icon: 'caret square right outline',
            onClick: onMoveClick
          });
        }

        if (onDeleteClick) {
          contextItems.push({
            id: id,
            name: t('common.delete'),
            icon: 'remove',
            iconColor: 'red',
            onClick: onDeleteClick
          });
        }

        return (
          <ArchiveListItem
            key={id}
            id={id}
            name={name}
            description={description}
            onClick={handleNextClick}
            contextItems={contextItems}
          />
        );
      })}
    </List>
  );
};

export default ArchiveList;
