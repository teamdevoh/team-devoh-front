import React, { useCallback, useState, useEffect, memo } from 'react';
import history from '../../helpers/history';
import api from './api';
import { Confirm, notification } from '../modal';
import { FileList } from '../modules';
import { Loader } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import find from 'lodash/find';
import { useArchiveDownload } from './hooks';
import RequestPanel from './RequestPanel';
import helpers from '../../helpers';
import { useInView } from 'react-intersection-observer';
import { role } from '../../constants';
import MultiDownloadButton from './/MultiDownloadButton';
import FileViewerWrapper from '../viewer/FileViewerWrapper';

const ArchiveFileListContainer = ({ projectId, keyword }) => {
  const [ref, inView, entry] = useInView({
    root: null,
    threshold: 0
  });

  const t = useFormatMessage();
  const [search, setSearch] = useState({
    open: false,
    tag: '',
    location: ''
  });
  const [files, setFiles] = useState([]);
  const [total, setTotal] = useState(0);
  const [offset, setOffset] = useState(0);
  const [archive, setRequest] = useArchiveDownload({ projectId: projectId });
  const [fileInfo, setFileInfo] = useState();
  const [viewerOpen, setViewerOpen] = useState(false);
  const [isViewReq, setIsViewReq] = useState(false);

  const _limit = 20;

  useEffect(() => {
    fetchItems({ offset: 0 });

    return () => {};
  }, []);

  const fetchItems = useCallback(
    async ({ offset }) => {
      const res = await api.fetchArchiveFilesAll({
        projectId: projectId,
        search: keyword,
        offset: offset,
        limit: _limit
      });

      let items = [];
      await res.data.items.forEach(async item => {
        const fileObj = await helpers.file.setFileInfo({
          ...item,
          id: item.projectAttachmentId,
          thumbnailApiUrl: `api/projects/${projectId}/archives/${
            item.projectArchiveId
          }/attach.thumbnail/${item.projectAttachmentId}`
        });
        items.push({ ...fileObj, projectArchiveId: item.projectArchiveId });
      });

      const merged = files.concat(items);
      setFiles(merged);
      setTotal(res.data.paging.total);
      setOffset(offset + _limit);
    },
    [files, offset]
  );

  const foundItem = useCallback(
    key => {
      const item = find(files, { key });

      if (item === void 0) {
        throw new Error('please check key data');
      }

      return item;
    },
    [files]
  );

  const handleDownload = useCallback(
    async id => {
      const item = foundItem(id);
      await archive.onDownload({ id, projectArchiveId: item.projectArchiveId });
    },
    [files]
  );

  const handleUpdateTag = useCallback(
    async (id, tags) => {
      const joinedTags = tags.join('');
      const item = foundItem(id);
      const res = await api.updateAttachTag({
        projectId,
        projectArchiveId: item.projectArchiveId,
        projectAttachmentId: id,
        model: { tag: joinedTags }
      });
      if (res && res.data) {
        setFiles(
          files.map((file, index) => {
            if (file.key === id) {
              file.tags = joinedTags;
            }
            return file;
          })
        );
      }
    },
    [files]
  );

  const handleRemoveAttach = useCallback(
    async id => {
      const item = foundItem(id);
      const res = await api.removeProjectAttach({
        projectId,
        projectArchiveId: item.projectArchiveId,
        projectAttachmentId: id
      });
      if (res.data) {
        setFiles(
          files.filter(file => {
            return file.key !== id;
          })
        );
      }
    },
    [files]
  );

  const handleTagClick = useCallback(tag => {
    setSearch({
      open: true,
      tag: tag,
      location: `/project/${projectId}/search?keyword=${tag}`
    });
  }, []);

  useEffect(
    () => {
      if (inView && files.length < total && entry.isIntersecting) {
        fetchItems({ offset });
      }
    },
    [inView]
  );

  const handleGoHistories = useCallback(
    ({ id, name }) => {
      const item = foundItem(id);
      history.push(
        `/project/${projectId}/archive/${
          item.projectArchiveId
        }/attach/${id}/histories`
      );
    },
    [files]
  );

  const handleShowFileLocation = useCallback(
    ({ id, name }) => {
      const item = foundItem(id);
      history.push(`/project/${projectId}/archive/${item.projectArchiveId}`);
    },
    [files]
  );

  const getProjectArchiveId = useCallback(
    id => {
      return foundItem(id).projectArchiveId;
    },
    [files]
  );

  const handleViewerClick = useCallback(
    async ({ fileInfo = {}, id }) => {
      const item = foundItem(id);

      if (fileInfo.name === null) {
        fileInfo = files.find(file => file.key === id);
      }

      let isAllow = false;
      if (helpers.Identity.hasRole(role.ProjectArchive_Download)) {
        isAllow = true;
      } else {
        const allowRes = await api.isAllowReqAttach({
          projectId,
          projectArchiveId: item.projectArchiveId,
          projectAttachmentId: id
        });

        if (
          !helpers.Identity.hasRole(role.ProjectArchive_Download) &&
          allowRes.data === null
        ) {
          notification.confirm({
            title: t('View.files'),
            content: t('View.files.info.message'),
            onOK: async () => {
              setRequest({ isRequire: true, id });
              setIsViewReq(true);
            }
          });
        } else {
          isAllow = allowRes.data.allow;

          if (isAllow === false) {
            notification.warning({
              title: t('denied')
            });
          }
          if (isAllow === null) {
            notification.warning({
              title: t('common.waiting')
            });
          }
        }
      }

      if (isAllow === true) {
        setFileInfo(fileInfo);
        setViewerOpen(true);
      }
    },
    [files]
  );

  const handleReqOKClick = useCallback(
    async data => {
      const item = foundItem(data.id);

      if (isViewReq) {
        const res = await api.addReqArchiveAttach({
          projectId,
          projectArchiveId: item.projectArchiveId,
          model: { projectAttachmentId: data.id, reason: data.value }
        });

        if (res && res.data) {
          setRequest({ isRequire: false, id: 0 });
          setIsViewReq(false);
          handleViewerClick({ id: data.id });
        }
      } else {
        await archive.onAddReq({
          id: data.id,
          projectArchiveId: item.projectArchiveId,
          reason: data.value
        });
      }
    },
    [isViewReq]
  );

  const fetchSignedUrl = fileInfo => {
    const item = foundItem(fileInfo.key);

    return api.fetchArchivePresignedurl({
      projectId,
      projectArchiveId: item.projectArchiveId,
      projectAttachmentId: fileInfo.key
    });
  };

  const handleViewerClose = () => {
    setViewerOpen(false);
  };

  const getDownloadUrl = useCallback(
    key => {
      const item = foundItem(key);

      if (!helpers.Identity.hasRole(role.ProjectArchive_Download)) {
        return `api/projects/${projectId}/archives/${
          item.projectArchiveId
        }/attach/${key}/req.download`;
      } else {
        return `api/projects/${projectId}/archives/${
          item.projectArchiveId
        }/attach/${key}`;
      }
    },
    [projectId, files]
  );

  const hasEditRole = helpers.Identity.hasRole(role.ProjectArchive_Edit);

  return (
    <div>
      <FileViewerWrapper
        fileInfo={fileInfo}
        fetchSignedUrl={fetchSignedUrl}
        open={viewerOpen}
        onClose={handleViewerClose}
        getDownloadUrl={getDownloadUrl}
      />
      <RequestPanel
        id={archive.request.id}
        show={archive.request.isRequire}
        onOKClick={handleReqOKClick}
        onCancelClick={archive.onUndoReq}
        loading={archive.loading}
      />
      <FileList
        files={files}
        onDownloadClick={handleDownload}
        onDeleteClick={hasEditRole ? handleRemoveAttach : null}
        onUpdateTag={hasEditRole ? handleUpdateTag : null}
        onTagClick={handleTagClick}
        userContextMenus={[
          {
            onClick: handleGoHistories,
            icon: { name: 'history', color: 'green' },
            name: t('common.histories')
          },
          {
            onClick: handleShowFileLocation,
            icon: { name: 'folder outline', color: 'black' },
            name: t('common.show.file.location')
          }
        ]}
        MultiDownloadButton={MultiDownloadButton({
          projectId,
          getProjectArchiveId
        })}
        onViewerClick={handleViewerClick}
      />
      <div ref={ref}>
        <Loader
          active={files.length > 0 && files.length < total}
          inline="centered"
          size="mini"
        />
      </div>
      <Confirm
        title={t('common.search')}
        content={t('project.search.is', { name: search.tag })}
        open={search.open}
        onOK={() => {
          history.push(search.location);
        }}
        onCancel={() => setSearch({ ...search, open: false })}
      />
    </div>
  );
};

export default memo(ArchiveFileListContainer);
