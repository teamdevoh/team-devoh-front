import React, { useCallback, useState, memo } from 'react';
import {
  Accordion,
  Container,
  Icon,
  Grid,
  Label,
  Loader
} from 'semantic-ui-react';
import {
  ProjectArchiveHeader,
  TaskFileListContainer,
  ArchiveFileListContainer
} from './';
import { SearchInput } from '../input';
import helpers from '../../helpers';
import { useFormatMessage, useFetch } from '../../hooks';
import AnchorBack from '../button/AnchorBack';

const ArchiveSearch = ({ match, history, location }) => {
  const [activeIndex, setActiveIndex] = useState(-1);
  const t = useFormatMessage();
  const { projectId } = match.params;

  const params = helpers.qs.parse(location.search);

  const url = `api/projects/${projectId}/archives.attachs.result`;
  const query = helpers.qs.stringify({
    name: params.value === void 0 ? '' : params.value
  });

  const [result] = useFetch(`${url}?${query}`, {
    archive: 0,
    task: 0
  });

  const handleClick = useCallback((e, data) => {
    const newIndex = data.active === true ? -1 : data.index;
    setActiveIndex(newIndex);
  }, []);

  const handleGoSearch = useCallback(value => {
    if (value && value.length > 0) {
      history.push(`/project/${projectId}/archive-search?value=${value}`);
    }
  }, []);

  return (
    <Container>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <ProjectArchiveHeader />
            <SearchInput onClick={handleGoSearch} value={params.value} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <AnchorBack />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Accordion>
              <Accordion.Title
                disabled={true}
                active={activeIndex === 0}
                index={0}
                onClick={handleClick}
              >
                <Icon name="dropdown" />
                {!result.loading && (
                  <Label color="red" horizontal>
                    {result.data.task}
                  </Label>
                )}
                {result.loading && (
                  <Loader size="tiny" active inline="centered" />
                )}
                {t('task')}
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 0}>
                {result.data.task > 0 &&
                  activeIndex === 0 && (
                    <TaskFileListContainer
                      projectId={projectId}
                      keyword={params.value}
                    />
                  )}
              </Accordion.Content>
              <Accordion.Title
                active={activeIndex === 1}
                index={1}
                onClick={handleClick}
              >
                <Icon name="dropdown" />
                {!result.loading && (
                  <Label color="red" horizontal>
                    {result.data.archive}
                  </Label>
                )}
                {result.loading && (
                  <Loader size="tiny" active inline="centered" />
                )}
                {t('project.archive')}
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 1}>
                {result.data.archive > 0 &&
                  activeIndex === 1 && (
                    <ArchiveFileListContainer
                      projectId={projectId}
                      keyword={params.value}
                    />
                  )}
              </Accordion.Content>
            </Accordion>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
};

export default memo(ArchiveSearch);
