import styled from 'styled-components';
import { Breadcrumb, List } from 'semantic-ui-react';

export const ListItemHeaderWrapper = styled(List.Header).attrs({ as: 'a' })`
  word-break: break-word;
`;

export const ListItemDescriptionWrapper = styled(List.Description)`
  min-height: 16px;
`;

export const ArchiveBreadcrumb = styled(Breadcrumb.Section)`
  word-break: break-word;
`;
