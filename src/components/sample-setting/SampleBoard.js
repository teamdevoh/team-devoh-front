import React from 'react';
import { Button, Container } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';
import { useFormatMessage } from '../../hooks';
import { DataGrid } from '../data-grid';

const SampleBoard = ({
  curProjectId,
  items,
  loading,
  onRowClick,
  onRemove,
  selected
}) => {
  const t = useFormatMessage();
  const history = useHistory();

  const columns = [
    {
      key: 'projectTitle',
      name: 'Project'
    },
    {
      key: 'sampleName',
      name: 'Sample Name',
      formatter: ({ value, row }) => (
        <a
          style={{ cursor: 'pointer' }}
          onClick={() => {
            history.push(
              `/project/${curProjectId}/sample/settings/${row.projectSampleId}`
            );
          }}
        >
          {value}
        </a>
      )
    },
    { key: 'sampleMaterial', name: 'Material' },
    {
      key: 'sampleCount',
      name: 'Count'
    },
    {
      key: 'remove',
      name: 'Remove',
      width: 110,
      formatter: ({ value, row }) => {
        return (
          <Button
            size="tiny"
            onClick={onRemove({
              projectId: row.projectId,
              projectSampleId: row.projectSampleId
            })}
          >
            {t('folder.remove')}
          </Button>
        );
      }
    }
  ];

  return (
    <Container>
      <DataGrid
        columns={columns}
        rows={items}
        rowKey="projectSampleId"
        minHeight={640}
        loading={loading}
        onRowClick={onRowClick}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
      />
    </Container>
  );
};

export default SampleBoard;
