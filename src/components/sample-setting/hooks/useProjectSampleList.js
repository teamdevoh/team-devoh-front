import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useProjectSampleList = ({ projectId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    const res = await api.fetchProjectSamples({
      projectId: !!projectId ? projectId : 0
    });

    if (res && res.data) {
      setItems(res.data);
    }
    setLoading(false);
  });

  useEffect(
    () => {
      fetchItems();
    },
    [projectId]
  );

  return [{ items, loading }];
};

export default useProjectSampleList;
