import { useState, useEffect } from 'react';
import { useFormFields } from '../../../hooks';
import api from '../api';
import unitIds from '../../unit/unit';

const initial = {
  projectSampleAliquotId: null,
  projectId: null,
  projectSampleId: null,
  aliquotAnalyte: null,
  aliquotVolume: 0,
  aliquotUnitId: unitIds.Milliliter,
  aliquotCount: 0
};

const useProjectSampleAliquot = ({
  curProjectId,
  projectSampleId,
  projectSampleAliquotId,
  projectId
}) => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields(initial);

  const fetchItem = async () => {
    setLoading(true);
    const res = await api.fetchProjectSampleAliquot({
      curProjectId,
      projectSampleId,
      projectSampleAliquotId
    });
    setLoading(false);
    if (res && res.data) {
      setModel(res.data);
    }
  };

  const add = async () => {
    setLoading(true);
    try {
      const res = await api.addProjectSampleAliquot({
        projectId,
        projectSampleId,
        projectSampleAliquotId,
        model
      });
      return res.data;
    } finally {
      setLoading(false);
    }
  };

  const edit = async () => {
    setLoading(true);
    try {
      const res = await api.updateProjectSampleAliquot({
        projectId,
        projectSampleId,
        projectSampleAliquotId: model.projectSampleAliquotId,
        model
      });
      if (res && res.data) {
        return true;
      }
      return false;
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      if (projectSampleAliquotId > 0) {
        fetchItem();
      } else {
        setModel(initial);
      }
    },
    [projectSampleAliquotId]
  );

  useEffect(
    () => {
      setModel({
        ...model,
        projectId,
        projectSampleId
      });
    },
    [projectId, projectSampleId]
  );

  return [{ model, loading, onChange, onAdd: add, onEdit: edit }];
};

export default useProjectSampleAliquot;
