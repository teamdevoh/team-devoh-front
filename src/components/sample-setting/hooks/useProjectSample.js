import { useState, useEffect } from 'react';
import { useFormFields } from '../../../hooks';
import api from '../api';

const initial = {
  projectSampleId: null,
  projectId: null,
  sampleMaterialCodeId: null,
  sampleName: null,
  sampleCount: null,
  totalAliquotCount: null
};

const useProjectSample = ({ curProjectId, projectSampleId }) => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields(initial);

  const fetchItem = async () => {
    setLoading(true);
    const res = await api.fetchProjectSample({ curProjectId, projectSampleId });
    setLoading(false);
    if (res && res.data) {
      setModel(res.data);
    }
  };

  const add = async () => {
    setLoading(true);
    const res = await api.addProjectSample({
      projectId: model.projectId,
      model
    });
    setLoading(false);
    return res.data;
  };

  const edit = async () => {
    setLoading(true);
    const res = await api.updateProjectSample({
      projectId: model.projectId,
      projectSampleId: model.projectSampleId,
      model
    });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  useEffect(
    () => {
      if (projectSampleId > 0) {
        fetchItem();
      } else {
        setModel(initial);
      }
    },
    [projectSampleId]
  );

  return [{ model, loading, onChange, onAdd: add, onEdit: edit }];
};

export default useProjectSample;
