import { useCallback, useEffect, useState } from 'react';
import api from '../api';

const useProjectSampleAliquotList = ({ projectId, projectSampleId = 0 }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    if (projectSampleId >= 0) {
      const res = await api.fetchProjectSampleAliquotList({
        projectId,
        projectSampleId
      });

      if (res && res.data) {
        setItems(res.data);
      }
    }
    setLoading(false);
  });

  useEffect(
    () => {
      if (projectId !== null) {
        fetchItems();
      }
    },
    [projectId, projectSampleId]
  );

  return [{ items, loading }, { setItems }];
};

export default useProjectSampleAliquotList;
