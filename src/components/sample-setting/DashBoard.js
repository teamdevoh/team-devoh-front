import React, { useState, useEffect } from 'react';
import SampleBoard from './SampleBoard';
import SampleAliquotBoard from './SampleAliquotBoard';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
// import Title from './Title';
import { Grid, Segment, Responsive } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useProjectSampleList, useProjectSampleAliquotList } from './hooks';
import api from './api';
import { notification } from '../modal';
import { useFormatMessage } from '../../hooks';
import { useHistory } from 'react-router-dom';

const isMobile =
  Responsive.onlyMobile.maxWidth >= window.innerWidth ? true : false;

const DashBoard = ({ filter, onFilterChange }) => {
  const t = useFormatMessage();
  const history = useHistory();
  const { projectId } = useParams();
  const [selected, setSelected] = useState(null);
  const [removeAliquotId, setREmoveAliquotId] = useState(null);

  const [{ items: samleItems, loading: sampeLoading }] = useProjectSampleList({
    ...filter
  });

  const [
    { items: aliquotItems, loading: aliquotLoading },
    { setItems: setAliquotList }
  ] = useProjectSampleAliquotList({
    projectSampleId: selected === null ? -1 : selected.projectSampleId,
    ...filter
  });

  useEffect(
    () => {
      if (removeAliquotId !== null) {
        const newAliquotList = aliquotItems.filter(
          item => item.projectSampleAliquotId !== removeAliquotId
        );

        setAliquotList(newAliquotList);
        setREmoveAliquotId(null);
      }
    },
    [removeAliquotId]
  );

  /**
   * grid row 클릭 이벤트
   * @param {int} index
   * @param {json} row
   */
  const handleSampleRowClick = (index, row, ...rest) => {
    if (selected !== null && row.projectSampleId === selected.projectSampleId) {
      return;
    } else {
      setSelected(row);
    }
  };

  const handleSampleAddClick = () => {
    history.push(`/project/${projectId}/sample/settings/0`);
  };

  const handleSampleAliquotAddClick = () => {
    history.push(
      `/project/${projectId}/sample/settings/${
        selected.projectSampleId
      }/aliquot/0`
    );
  };

  const handleProjectSampleRemove = ({ projectId, projectSampleId }) => () => {
    notification.confirm({
      title: t('delete.message'),
      content: t('project.sample.delete'),
      onOK: async () => {
        const res = await api.removeProjectSample({
          projectId,
          projectSampleId
        });

        if (res && res.data) {
          history.replace();
        }
      }
    });
  };

  const handleAliquotRemove = ({
    projectId,
    projectSampleAliquotId,
    projectSampleId
  }) => () => {
    notification.confirm({
      title: t('delete.message'),
      content: '',
      onOK: async () => {
        const res = await api.removeAliquot({
          projectId,
          projectSampleId,
          projectSampleAliquotId
        });

        if (res && res.data) {
          setREmoveAliquotId(projectSampleAliquotId);
        } else {
          history.replace();
        }
      }
    });
  };

  useEffect(
    () => {
      setSelected(null);
    },
    [filter]
  );

  return (
    <Segment basic>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onFilterChange={onFilterChange}
            filter={filter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid divided="vertically">
        <Grid.Row style={{ marginTop: '20px' }}>
          <Grid.Column width={isMobile ? 16 : 8}>
            <Grid>
              <Grid.Row>
                <Grid.Column>
                  <DashBoardTool
                    title="Project Sample List"
                    onAddClick={handleSampleAddClick}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <SampleBoard
                    items={samleItems}
                    loading={sampeLoading}
                    onRowClick={handleSampleRowClick}
                    onRemove={handleProjectSampleRemove}
                    selected={selected}
                    curProjectId={projectId}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
          <Grid.Column width={isMobile ? 16 : 8}>
            <Grid>
              <Grid.Row>
                <Grid.Column>
                  <DashBoardTool
                    title="Aliquot Setting from Sample"
                    onAddClick={handleSampleAliquotAddClick}
                    addButtonDisabled={selected === null}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <SampleAliquotBoard
                    items={selected === null ? [] : aliquotItems}
                    loading={aliquotLoading}
                    onRemove={handleAliquotRemove}
                    curProjectId={projectId}
                    selected={selected}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default DashBoard;
