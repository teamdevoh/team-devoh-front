import React from 'react';
import { Container, Header } from 'semantic-ui-react';
import ProjectSampleForm from './ProjectSampleForm';
import { useParams } from 'react-router-dom';

const ProjectSampleContainer = () => {
  const { projectId, projectSampleId } = useParams();

  return (
    <Container className="margin-0">
      <Header as="h3" dividing>
        Sample
      </Header>
      <ProjectSampleForm
        projectId={projectId}
        projectSampleId={projectSampleId}
      />
    </Container>
  );
};

export default ProjectSampleContainer;
