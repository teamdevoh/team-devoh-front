import React from 'react';
import styled from 'styled-components';
import { Header, Button } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';

const FlexDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const StyledHeader = styled(Header)`
  &&& {
    margin: 0;
  }
`;

const DashBoardTool = ({ title, onAddClick, addButtonDisabled = false }) => {
  const t = useFormatMessage();
  return (
    <FlexDiv>
      <StyledHeader as="h3">{title}</StyledHeader>
      <Button onClick={onAddClick} disabled={addButtonDisabled}>
        {t('activity.add')}
      </Button>
    </FlexDiv>
  );
};

export default DashBoardTool;
