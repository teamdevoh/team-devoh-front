import React, { useRef, useState } from 'react';
import { Divider, Form, Input, Container } from 'semantic-ui-react';
import { BackButton, SaveButton } from '../button';
import { useFormatMessage, useSubmit } from '../../hooks';
import { useHistory } from 'react-router-dom';
import { notification } from '../modal';
import { useProjectSampleAliquot } from './hooks';
import { Unit } from './Selector';
import { useCodes } from '../../hooks';
import { useBuild } from '../activity/hooks';
import { codeGroup, role } from '../../constants';

const ProjectSampleAliquotForm = ({
  projectId,
  projectSampleId,
  projectSampleAliquotId: propProjectSampleAliquotId,
  projectSampleModel
}) => {
  const t = useFormatMessage();
  const [projectSampleAliquotId, setProjectSampleAliquotId] = useState(
    propProjectSampleAliquotId
  );
  const history = useHistory();
  const build = useBuild();
  const [sampleMaterial] = useCodes({
    codeGroupId: codeGroup.SampleMaterialCode
  });
  const [{ model, loading, onChange, onAdd, onEdit }] = useProjectSampleAliquot(
    {
      curProjectId: projectId,
      projectSampleId,
      projectSampleAliquotId: Number(projectSampleAliquotId),
      projectId: projectSampleModel.projectId
    }
  );
  const saveAndMove = useRef(false);

  const setSaveAndMove = val => {
    saveAndMove.current = val;
  };

  const getSaveAndMove = () => {
    return saveAndMove.current;
  };

  const handleMovePage = projectSampleAliquotId => () => {
    const move = getSaveAndMove();
    if (move) {
      history.push(`/project/${projectId}/sample/settings`);
    }
  };

  const handleSubmit = async () => {
    if (Number(projectSampleAliquotId) > 0) {
      const is = await onEdit();
      if (is) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: handleMovePage(projectSampleAliquotId)
        });
      }
    } else {
      const newProjectSampleAliquotId = await onAdd();
      if (newProjectSampleAliquotId) {
        setProjectSampleAliquotId(newProjectSampleAliquotId);
        notification.success({
          title: t('common.alert.saved'),
          onClose: handleMovePage(newProjectSampleAliquotId)
        });
      }
    }
  };

  const handleSaveClick = (saveAndMove = false) => () => {
    setSaveAndMove(saveAndMove);
    onValidate();
  };

  const [{ validator, onValidate }] = useSubmit(handleSubmit);

  const sampleMaterialName = build.displayName({
    source: sampleMaterial,
    value: Number(projectSampleModel.sampleMaterialCodeId)
  });

  return (
    <Container>
      <Form>
        <Form.Field>
          <label>Sample</label>
          {sampleMaterialName}, {projectSampleModel.sampleName}, Sample Count :{' '}
          {projectSampleModel.sampleCount}, Total Aliquot Count:{' '}
          {projectSampleModel.totalAliquotCount}
        </Form.Field>
        <Divider />
        <Form.Field required>
          <label>Analyte</label>
          <Input
            name="aliquotAnalyte"
            value={model.aliquotAnalyte || ''}
            onChange={onChange}
          />
          {validator.message('Analyte', model.aliquotAnalyte, 'required')}
        </Form.Field>
        <Form.Group widths="equal">
          <Form.Field required>
            <label>Volume</label>
            <Input
              name="aliquotVolume"
              value={model.aliquotVolume}
              onChange={onChange}
              type="number"
            />
            {validator.message('Volume', model.aliquotVolume, 'required')}
          </Form.Field>
          <Form.Field required>
            <label>Unit</label>
            <Unit
              name="aliquotUnitId"
              value={model.aliquotUnitId}
              onChange={onChange}
            />
            {validator.message('Unit', model.aliquotUnitId, 'required')}
          </Form.Field>
        </Form.Group>
        <Form.Field required>
          <label>Aliquot Count</label>
          <Input
            name="aliquotCount"
            value={model.aliquotCount}
            onChange={onChange}
            type="number"
          />
          {validator.message('Aliquot Count', model.aliquotCount, 'required')}
        </Form.Field>

        <Divider />
        <BackButton name={t('common.back')} />
        <SaveButton
          allowedRole={role.Subject_Edit}
          name={t('common.save')}
          loading={loading}
          onClick={handleSaveClick(false)}
        />
        <SaveButton
          allowedRole={role.Subject_Edit}
          name={t('common.save.move')}
          loading={loading}
          onClick={handleSaveClick(true)}
        />
      </Form>
    </Container>
  );
};

export default ProjectSampleAliquotForm;
