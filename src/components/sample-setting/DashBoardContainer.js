import React, { useState } from 'react';
import SampleSettingRoutes from './SampleSettingRoutes';

const DashBoardContainer = () => {
  const [filter, setFilter] = useState({
    projectId: 0
  });

  const handleFilterChange = (event, { name, value }) => {
    setFilter({ ...filter, [name]: value });
  };

  return (
    <SampleSettingRoutes filter={filter} onFilterChange={handleFilterChange} />
  );
};

export default DashBoardContainer;
