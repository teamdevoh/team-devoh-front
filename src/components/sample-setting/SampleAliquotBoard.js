import React from 'react';
import { Button, Container } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';
import { useFormatMessage } from '../../hooks';
import { DataGrid } from '../data-grid';

const SampleAliquotBoard = ({ items, loading, onRemove, curProjectId }) => {
  const t = useFormatMessage();
  const history = useHistory();

  const columns = [
    {
      key: 'aliquotAnalyte',
      name: 'Analyte',
      formatter: ({ value, row }) => (
        <a
          style={{ cursor: 'pointer' }}
          onClick={() => {
            history.push(
              `/project/${curProjectId}/sample/settings/${
                row.projectSampleId
              }/aliquot/${row.projectSampleAliquotId}`
            );
          }}
        >
          {value}
        </a>
      )
    },
    {
      key: 'aliquotVolume',
      name: 'Volume'
    },
    {
      key: 'aliquotUnit',
      name: 'Unit'
    },
    {
      key: 'aliquotCount',
      name: 'Count'
    },
    {
      key: 'remove',
      name: 'Remove',
      width: 110,
      formatter: ({ value, row }) => {
        return (
          <Button
            onClick={onRemove({
              projectId: row.projectId,
              projectSampleAliquotId: row.projectSampleAliquotId,
              projectSampleId: row.projectSampleId
            })}
            size="tiny"
          >
            {t('folder.remove')}
          </Button>
        );
      }
    }
  ];

  return (
    <Container>
      <DataGrid
        columns={columns}
        rows={items}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        rowKey="projectSampleAliquotId"
        minHeight={640}
        loading={loading}
      />
    </Container>
  );
};

export default SampleAliquotBoard;
