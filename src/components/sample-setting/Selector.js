import React from 'react';
import { Select } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import { useCodes } from '../../hooks';
import { useUnitList } from '../unit/hooks';

export const SampleMaterial = ({ name, value, onChange, hasAll = false }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.SampleMaterialCode });
  const options = hasAll
    ? [
        {
          text: '-----------------------------------------',
          value: 0
        },
        ...items
      ]
    : items;

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={options}
    />
  );
};

export const Unit = ({ name, value, onChange }) => {
  const [{ items }] = useUnitList();

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={items}
    />
  );
};
