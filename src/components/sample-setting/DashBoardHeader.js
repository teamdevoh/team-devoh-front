import React, { Fragment } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { ProjectsOfSamples } from '../sample-status/Selector';
import { StyledForm } from '../sample-status/styles';

const DashBoardHeader = ({ projectId, filter, onFilterChange }) => {
  return (
    <Fragment>
      <Grid stackable>
        <Grid.Column>
          <StyledForm>
            <Form.Group>
              <Form.Field>
                <label>Project</label>
                <ProjectsOfSamples
                  name="projectId"
                  value={filter.projectId}
                  onChange={onFilterChange}
                  hasAll
                />
              </Form.Field>
            </Form.Group>
          </StyledForm>
        </Grid.Column>
      </Grid>
    </Fragment>
  );
};

export default DashBoardHeader;
