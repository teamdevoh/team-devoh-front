import React from 'react';
import { Container, Header } from 'semantic-ui-react';
import ProjectSampleAliquotForm from './ProjectSampleAliquotForm';
import { useParams } from 'react-router-dom';
import { useProjectSample } from './hooks';

const ProjectSampleAliquotContainer = () => {
  const { projectId, projectSampleId, projectSampleAliquotId } = useParams();

  const [{ model }] = useProjectSample({
    curProjectId: projectId,
    projectSampleId: Number(projectSampleId)
  });

  return (
    <Container className="margin-0">
      <Header as="h3" dividing>
        Aliquot
      </Header>
      <ProjectSampleAliquotForm
        projectId={projectId}
        projectSampleId={projectSampleId}
        projectSampleAliquotId={projectSampleAliquotId}
        projectSampleModel={model}
      />
    </Container>
  );
};

export default ProjectSampleAliquotContainer;
