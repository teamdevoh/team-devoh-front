import helpers from '../../helpers';

const fetchProjectSamples = ({ projectId }) => {
  return helpers.Service.get(`api/project/${projectId}/samples`);
};

const fetchProjectSampleAliquotList = ({ projectId, projectSampleId }) => {
  return helpers.Service.get(
    `api/project/${projectId}/samples/${projectSampleId}/aliquot`
  );
};

const fetchProjectSample = ({ curProjectId, projectSampleId }) => {
  return helpers.Service.get(
    `api/project/${curProjectId}/samples/${projectSampleId}`
  );
};

const addProjectSample = ({ projectId, model }) => {
  return helpers.Service.post(`api/project/${projectId}/samples`, model);
};

const updateProjectSample = ({ projectId, projectSampleId, model }) => {
  return helpers.Service.put(
    `api/project/${projectId}/samples/${projectSampleId}`,
    model
  );
};

const removeProjectSample = ({ projectId, projectSampleId }) => {
  return helpers.Service.delete(
    `api/project/${projectId}/samples/${projectSampleId}`
  );
};

const fetchProjectSampleAliquot = ({
  curProjectId,
  projectSampleId,
  projectSampleAliquotId
}) => {
  return helpers.Service.get(
    `api/project/${curProjectId}/samples/${projectSampleId}/aliquot/${projectSampleAliquotId}`
  );
};

const addProjectSampleAliquot = ({ projectId, projectSampleId, model }) => {
  return helpers.Service.post(
    `api/project/${projectId}/samples/${projectSampleId}/aliquot`,
    model
  );
};

const updateProjectSampleAliquot = ({
  projectId,
  projectSampleId,
  projectSampleAliquotId,
  model
}) => {
  return helpers.Service.put(
    `api/project/${projectId}/samples/${projectSampleId}/aliquot/${projectSampleAliquotId}`,
    model
  );
};

const removeAliquot = ({
  projectId,
  projectSampleId,
  projectSampleAliquotId
}) => {
  return helpers.Service.delete(
    `api/project/${projectId}/samples/${projectSampleId}/aliquot/${projectSampleAliquotId}`
  );
};

export default {
  fetchProjectSamples,
  fetchProjectSampleAliquotList,
  fetchProjectSample,
  addProjectSample,
  updateProjectSample,
  removeProjectSample,
  fetchProjectSampleAliquot,
  addProjectSampleAliquot,
  updateProjectSampleAliquot,
  removeAliquot
};
