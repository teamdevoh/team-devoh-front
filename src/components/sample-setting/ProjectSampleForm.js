import React, { useRef } from 'react';
import { Divider, Form, Input, Container } from 'semantic-ui-react';
import { BackButton, SaveButton } from '../button';
import { useFormatMessage, useSubmit } from '../../hooks';
import { useHistory } from 'react-router-dom';
import { notification } from '../modal';
import { useProjectSample } from './hooks';
import { ProjectsOfSamples } from '../sample-status/Selector';
import { SampleMaterial } from './Selector';
import { role } from '../../constants';

const ProjectSampleForm = ({ projectId, projectSampleId }) => {
  const t = useFormatMessage();
  const history = useHistory();
  const [{ model, loading, onChange, onAdd, onEdit }] = useProjectSample({
    curProjectId: projectId,
    projectSampleId: Number(projectSampleId)
  });
  const saveAndMove = useRef(false);

  const setSaveAndMove = val => {
    saveAndMove.current = val;
  };

  const getSaveAndMove = () => {
    return saveAndMove.current;
  };

  const handleMovePage = projectSampleId => () => {
    const move = getSaveAndMove();
    if (move) {
      history.push(`/project/${projectId}/sample/settings`);
    }
  };

  const handleSubmit = async () => {
    if (Number(projectSampleId) > 0) {
      const is = await onEdit();
      if (is) {
        notification.success({
          title: t('common.alert.changed'),
          onClose: handleMovePage(projectSampleId)
        });
      }
    } else {
      const newTbdrugId = await onAdd();
      if (newTbdrugId) {
        notification.success({
          title: t('common.alert.saved'),
          onClose: handleMovePage(newTbdrugId)
        });
      }
    }
  };

  const handleSaveClick = (saveAndMove = false) => () => {
    setSaveAndMove(saveAndMove);
    onValidate();
  };

  const [{ validator, onValidate }] = useSubmit(handleSubmit);

  return (
    <Container>
      <Form>
        <Form.Field required>
          <label>Project</label>
          <ProjectsOfSamples
            name="projectId"
            value={model.projectId}
            onChange={onChange}
          />
          {validator.message('Project', model.projectId, 'required')}
        </Form.Field>
        <Form.Field required>
          <label>Material</label>
          <SampleMaterial
            name="sampleMaterialCodeId"
            value={model.sampleMaterialCodeId}
            onChange={onChange}
          />
          {validator.message(
            'Material',
            model.sampleMaterialCodeId,
            'required'
          )}
        </Form.Field>
        <Form.Field required>
          <label>Sample Name</label>
          <Input
            name="sampleName"
            value={model.sampleName || ''}
            onChange={onChange}
          />
          {validator.message('Short Name', model.sampleName, 'required')}
        </Form.Field>
        <Form.Field required>
          <label>Sample Count</label>
          <Input
            name="sampleCount"
            value={model.sampleCount === null ? 0 : model.sampleCount}
            onChange={onChange}
            type="number"
          />
          {validator.message('Short Count', model.sampleCount, 'required')}
        </Form.Field>

        <Divider />
        <BackButton name={t('common.back')} />
        <SaveButton
          allowedRole={role.Subject_Edit}
          name={t('common.save')}
          loading={loading}
          onClick={handleSaveClick(false)}
        />
        <SaveButton
          allowedRole={role.Subject_Edit}
          name={t('common.save.move')}
          loading={loading}
          onClick={handleSaveClick(true)}
        />
      </Form>
    </Container>
  );
};

export default ProjectSampleForm;
