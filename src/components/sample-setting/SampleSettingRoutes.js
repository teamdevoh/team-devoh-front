import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { asyncComponent } from '../../components/modules';
import { Authorized } from '../../components/auth';
import { role } from '../../constants';

const SampleSettingDataDashBoard = asyncComponent(() => import('./DashBoard'));
const ProjectSampleContainer = asyncComponent(() =>
  import('./ProjectSampleContainer')
);
const ProjectSampleAliquotContainer = asyncComponent(() =>
  import('./ProjectSampleAliquotContainer')
);

const SampleSettingRoutes = ({ filter, onFilterChange }) => {
  return (
    <Switch>
      <Route
        exact
        path="/project/:projectId/sample/settings"
        render={props => {
          const Component = Authorized(
            SampleSettingDataDashBoard,
            role.Sample_Read
          );
          return (
            <Component
              {...props}
              filter={filter}
              onFilterChange={onFilterChange}
            />
          );
        }}
      />
      <Route
        exact
        path="/project/:projectId/sample/settings/:projectSampleId"
        render={props => {
          const Component = Authorized(
            ProjectSampleContainer,
            role.Sample_Edit
          );
          return <Component {...props} />;
        }}
      />
      <Route
        exact
        path="/project/:projectId/sample/settings/:projectSampleId/aliquot/:projectSampleAliquotId"
        render={props => {
          const Component = Authorized(
            ProjectSampleAliquotContainer,
            role.Sample_Edit
          );
          return <Component {...props} />;
        }}
      />
    </Switch>
  );
};

export default SampleSettingRoutes;
