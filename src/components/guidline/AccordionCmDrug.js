import React, { Fragment } from 'react';
import { Icon } from 'semantic-ui-react';
import take from 'lodash/take';
import { BoxCmDrug } from './';
import { NestedAccordionContent, NestedAccordionTitle } from './styles';
import { useFormatMessage } from '../../hooks';

const AccordionCmDrug = ({ activeIndex, index, items = [], onClick }) => {
  const t = useFormatMessage();
  let fitleredItems = [];
  _.forEach(items, item => {
    if (item.cmdrugs.length > 0) {
      item.cmdrugs.forEach(cmdrug => {
        cmdrug.cmtrt = cmdrug.cmtrt.trim();
      });

      fitleredItems = [...fitleredItems, ...item.cmdrugs];
    }
  });

  const groups = _.groupBy(fitleredItems, 'cmtrt');
  const orderedGroups = _.orderBy(groups, g => g.length, 'desc');
  const viewItems = take(orderedGroups, 30);
  const total_cnt = _.sum(viewItems.map(item => item.length));
  const active = activeIndex === index;
  return (
    <Fragment>
      <NestedAccordionTitle active={active} index={index} onClick={onClick}>
        <Icon name="dropdown" />
        {t('activity.cm.title')} [<Icon name="briefcase" color="blue" />
        {total_cnt}]
      </NestedAccordionTitle>
      {active &&
        viewItems.length > 0 && (
          <NestedAccordionContent active={active}>
            <BoxCmDrug items={viewItems} />
          </NestedAccordionContent>
        )}
    </Fragment>
  );
};

export default AccordionCmDrug;
