import React, { Fragment, useState } from 'react';
import { Item } from 'semantic-ui-react';
import drugItems from './korean-drugs.json';
import { useBoolean } from '../../hooks';
import {
  GuidelineKoreanDrugByHealthModal,
  GuidelineKoreanDrugModal,
  KoreanDrugListItem
} from './';

const KoreanDrugList = ({ items, skipNames = [], initials = [] }) => {
  const open = useBoolean(false);
  const drugDetailOpen = useBoolean(false);
  const [selectedItem, setSelectedItem] = useState({});
  const [drugDetailList, setDrugDetailList] = useState({ name: '', items: [] });

  const filteredItems = drugItems.filter(item => {
    return (
      skipNames.indexOf(item.name.toLowerCase()) === -1 &&
      initials.indexOf(item.initial) > -1
    );
  });

  return (
    <Fragment>
      <Item.Group divided>
        {filteredItems.map(item => {
          return (
            <KoreanDrugListItem
              key={item.name}
              {...item}
              onImgClick={data => {
                open.setTrue();
                setSelectedItem({ ...item, ...data });
              }}
              onHeaderClick={data => {
                setDrugDetailList(data);
                drugDetailOpen.setTrue();
              }}
            />
          );
        })}
      </Item.Group>
      <GuidelineKoreanDrugModal
        open={open.value}
        onClose={open.setFalse}
        name={selectedItem.name}
        imgUrl={`/images/${selectedItem.image}`}
        prescription={selectedItem.prescription}
        dosage={selectedItem.dosage}
        method={selectedItem.method}
        sideEffects={selectedItem.sideEffects}
      />

      <GuidelineKoreanDrugByHealthModal
        open={drugDetailOpen.value}
        onClose={drugDetailOpen.setFalse}
        name={drugDetailList.name}
        title={drugDetailList.title}
        totalCount={drugDetailList.totalCount}
        items={drugDetailList.items}
      />
    </Fragment>
  );
};

export default KoreanDrugList;
