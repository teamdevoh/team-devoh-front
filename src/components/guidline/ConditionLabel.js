import React, { Fragment, memo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Icon, Label, Message } from 'semantic-ui-react';
import orderBy from 'lodash/orderBy';

const ConditionLabel = memo(({ condition = {}, onDeleteTagClick }) => {
  let tags = [];

  Object.keys(condition).forEach(key => {
    if (condition[key].value > '') {
      tags.push({
        key: key,
        text: condition[key].text,
        index: condition[key].index
      });
    }
  });

  const handleDeleteClick = useCallback(
    event => {
      onDeleteTagClick(event.target.getAttribute('value'));
    },
    [condition]
  );

  return (
    <Fragment>
      {tags.length > 0 && (
        <Message>
          {orderBy(tags, ['index'], 'asc').map(tag => {
            return (
              <Label as="a" key={tag.key} style={{ marginBottom: '0.3em' }}>
                {tag.text}
                <Icon
                  name="delete"
                  value={tag.key}
                  onClick={handleDeleteClick}
                />
              </Label>
            );
          })}
        </Message>
      )}
    </Fragment>
  );
});

ConditionLabel.propTypes = {
  condition: PropTypes.object.isRequired,
  onDeleteTagClick: PropTypes.func.isRequired
};

export default ConditionLabel;
