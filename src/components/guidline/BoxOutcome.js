import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash/groupBy';
import orderBy from 'lodash/orderBy';
import find from 'lodash/find';
import round from 'lodash/round';
import { Header, Icon, Item, Popup } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import helpers from '../../helpers';
import { useCodes, useFormatMessage } from '../../hooks';

const TreatmentOutcomeBox = ({ items }) => {
  const t = useFormatMessage();
  const [caseConcodes] = useCodes({ codeGroupId: codeGroup.CaseconCode });
  const groups = groupBy(items, 'caseconCodeId');
  const orderedGroups = orderBy(groups, g => g.length, 'desc');

  if (caseConcodes.length > 0) {
    return (
      <Item.Group>
        {orderedGroups.map(subjects => {
          const code = find(caseConcodes, { key: subjects[0].caseconCodeId });

          let minMonth = 0;
          let maxMonth = 0;
          let totalMonths = 0;
          subjects.forEach(item => {
            const treatmentPeriod =
              helpers.util.diffMonths(item.treatmentStart, item.treatmentEnd) +
              1;

            if (minMonth === 0 || minMonth > treatmentPeriod) {
              minMonth = treatmentPeriod;
            }
            if (maxMonth === 0 || maxMonth < treatmentPeriod) {
              maxMonth = treatmentPeriod;
            }

            totalMonths += treatmentPeriod;
          });

          return (
            <Item key={code.text} style={{ marginBottom: '-1em' }}>
              <Item.Content>
                <Header as="h5">{code.text}</Header>
                <Item.Description>
                  <Popup
                    trigger={<Icon name="user" color="blue" className="link" />}
                    content={t('prescriptions.count')}
                    inverted
                  />
                  {subjects.length}
                  <Popup
                    trigger={
                      <Icon
                        name="calendar alternate outline"
                        color="blue"
                        className="link"
                        style={{ marginLeft: '4px' }}
                      />
                    }
                    content={t('treatment.period')}
                    inverted
                  />
                  {t('average')}: {round(totalMonths / subjects.length, 1)}
                  {t('months')}, {t('shortest')} {minMonth}
                  {t('months')}, {t('longest')} {maxMonth}
                  {t('months')}
                </Item.Description>
              </Item.Content>
            </Item>
          );
        })}
      </Item.Group>
    );
  } else {
    return <Fragment />;
  }
};

TreatmentOutcomeBox.propTypes = {
  items: PropTypes.array.isRequired
};

export default TreatmentOutcomeBox;
