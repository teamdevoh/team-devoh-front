import React, { Fragment } from 'react';
import { Icon } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { BoxComorbid } from './';
import { NestedAccordionContent, NestedAccordionTitle } from './styles';

const AccordionComorbid = ({ activeIndex, index, items = [], onClick }) => {
  const t = useFormatMessage();

  let fitleredItems = [];
  _.forEach(items, item => {
    if (item.comorbids.length > 0) {
      item.comorbids.forEach(comorbid => {
        if (comorbid.name.indexOf('-') > -1) {
          comorbid.name = comorbid.name.toLowerCase();
        }
        comorbid.name = comorbid.name.trim();
      });
      fitleredItems = [...fitleredItems, ...item.comorbids];
    }
  });

  const groups = _.groupBy(fitleredItems, 'name');
  const orderedGroups = _.orderBy(groups, g => g.length, 'desc');

  const active = activeIndex === index;
  return (
    <Fragment>
      <NestedAccordionTitle active={active} index={index} onClick={onClick}>
        <Icon name="dropdown" />
        {t('activity.comorbid.title')} [<Icon name="briefcase" color="blue" />
        {orderedGroups.length}]
      </NestedAccordionTitle>
      {active &&
        items.length > 0 && (
          <NestedAccordionContent active={active}>
            <BoxComorbid items={orderedGroups} />
          </NestedAccordionContent>
        )}
    </Fragment>
  );
};

export default AccordionComorbid;
