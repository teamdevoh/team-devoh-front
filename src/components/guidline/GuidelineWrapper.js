import React, { Fragment, useCallback, useState } from 'react';
import { useBoolean } from 'react-hanger';
import {
  Button,
  ButtonGroup,
  Grid,
  Header,
  Icon,
  Segment
} from 'semantic-ui-react';
import { ResponsiveLarge, ResponsiveSmall } from '../element/';
import { GuidelinePanel } from './';
import { GuidelinePanelSegment } from './styles';

const GuidelineWrapper = ({ panel1, panel2, panel3 }) => {
  const FULLWIDTH = 16;

  const hideCondition = useBoolean(false);
  const CONTAINER_WIDTH = {
    KOREAN: 8,
    cPMTb: 8
  };

  const defaultSize = [CONTAINER_WIDTH.KOREAN, CONTAINER_WIDTH.cPMTb];
  const [size, setSize] = useState(defaultSize);

  const handleExpandClick = useCallback(index => {
    let newSize = [];
    size.forEach((width, i) => {
      newSize.push(i === index ? FULLWIDTH : 0);
    });
    setSize(newSize);
  }, []);

  const handleCompressClick = useCallback(index => {
    setSize(defaultSize);
  }, []);

  const [target, setTarget] = useState('condition');

  const handleItemClick = useCallback(name => {
    setTarget(name);
  }, []);

  return (
    <Fragment>
      <ResponsiveLarge>
        <Grid stackable>
          <Grid.Column width={hideCondition.value === true ? 'one' : 'four'}>
            <Header as="h2" dividing color="red">
              Condition
            </Header>
            <Icon
              name="exchange"
              className="link"
              onClick={hideCondition.toggle}
            />
            <GuidelinePanelSegment>
              <div
                style={{
                  display: hideCondition.value === true ? 'none' : 'block'
                }}
              >
                {panel1}
              </div>
            </GuidelinePanelSegment>
          </Grid.Column>
          <Grid.Column
            width={hideCondition.value === true ? 'fifteen' : 'twelve'}
          >
            <Grid stackable>
              <Grid.Row>
                <GuidelinePanel
                  index={0}
                  title="Korean Guidelines 4th"
                  color="orange"
                  width={size[0]}
                  onExpandClick={handleExpandClick}
                  onCompressClick={handleCompressClick}
                >
                  {panel2}
                </GuidelinePanel>
                <GuidelinePanel
                  index={1}
                  title="cPMTb-001"
                  color="yellow"
                  width={size[1]}
                  onExpandClick={handleExpandClick}
                  onCompressClick={handleCompressClick}
                >
                  {panel3}
                </GuidelinePanel>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid>
      </ResponsiveLarge>
      {/* Small screen */}
      <ResponsiveSmall>
        <Fragment>
          <ButtonGroup>
            <Button
              icon
              color="red"
              onClick={() => {
                handleItemClick('condition');
              }}
            >
              <Icon name="align left" />
            </Button>
            <Button
              color="orange"
              onClick={() => {
                handleItemClick('korean');
              }}
            >
              Korean Guidelines 4th
            </Button>
            <Button
              color="yellow"
              onClick={() => {
                handleItemClick('cpmtb');
              }}
            >
              cPMTb-001
            </Button>
          </ButtonGroup>
          <Segment basic>
            {target === 'condition' && (
              <Fragment>
                <Header as="h3" dividing color="red">
                  Condition
                </Header>
                {panel1}
              </Fragment>
            )}
            {target === 'korean' && (
              <Fragment>
                <Header as="h3" dividing color="orange">
                  Korean Guidelines 4th
                </Header>
                {panel2}
              </Fragment>
            )}
            {target === 'cpmtb' && (
              <Fragment>
                <Header as="h3" dividing color="yellow">
                  cPMTb-001
                </Header>
                {panel3}
              </Fragment>
            )}
          </Segment>
        </Fragment>
      </ResponsiveSmall>
    </Fragment>
  );
};

export default GuidelineWrapper;
