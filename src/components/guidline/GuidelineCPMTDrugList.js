import React, { Fragment, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { NestedAccordion } from './styles';
import {
  AccordionAdr,
  AccordionCmDrug,
  AccordionComorbid,
  AccordionOutcome,
  AccordionRegimen
} from './';
import { Divider, Icon, Label } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';

const AnnotationWrapper = () => {
  const t = useFormatMessage();
  return (
    <Fragment>
      <Icon name="user" color="blue" />
      <Label as="a" basic pointing="left">
        {t('prescriptions.count')}
      </Label>
      <Icon style={{ marginLeft: '1em' }} name="briefcase" color="blue" />
      <Label as="a" basic pointing="left">
        Cases
      </Label>
    </Fragment>
  );
};

const GuidelineCPMTDrugList = ({ items = [] }) => {
  const [activeIndex, setActiveIndex] = useState(-1);

  const handleAccordionClick = useCallback(
    (event, data) => {
      const newIndex = activeIndex === data.index ? -1 : data.index;
      setActiveIndex(newIndex);
    },
    [activeIndex]
  );

  return (
    <Fragment>
      <AnnotationWrapper />
      <Divider />
      <NestedAccordion fluid>
        <AccordionRegimen
          items={items}
          index={0}
          activeIndex={activeIndex}
          onClick={handleAccordionClick}
        />
        <AccordionOutcome
          items={items}
          index={1}
          activeIndex={activeIndex}
          onClick={handleAccordionClick}
        />
        <AccordionAdr
          items={items}
          index={2}
          activeIndex={activeIndex}
          onClick={handleAccordionClick}
        />
        <AccordionComorbid
          items={items}
          index={3}
          activeIndex={activeIndex}
          onClick={handleAccordionClick}
        />
        <AccordionCmDrug
          items={items}
          index={4}
          activeIndex={activeIndex}
          onClick={handleAccordionClick}
        />
      </NestedAccordion>
    </Fragment>
  );
};

GuidelineCPMTDrugList.propTypes = {
  items: PropTypes.array.isRequired
};

export default GuidelineCPMTDrugList;
