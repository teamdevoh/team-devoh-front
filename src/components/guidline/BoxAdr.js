import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash/groupBy';
import orderBy from 'lodash/orderBy';
import filter from 'lodash/filter';
import { BoxAdrItem } from '.';

const codeLevel = [
  { code: 'aesoccd', name: 'aesoc' },
  { code: 'aeptcd', name: 'aedecod' },
  { code: 'adrName', name: 'adrName' }
];

const BoxAdr = ({ items = [], level = 0, code = '' }) => {
  let data = items;
  const currentAdr = codeLevel[level];
  if (code) {
    data = filter(items, { [codeLevel[level - 1].code]: code });
  }

  const groups = groupBy(data, currentAdr.code);
  const sortedGruops = orderBy(groups, g => g.length, 'desc');

  return (
    <Fragment>
      {Object.keys(sortedGruops).map(key => {
        const currentLevel = level + 1;
        const adr = sortedGruops[key];
        const hasChildren = codeLevel[currentLevel] ? true : false;

        return (
          <BoxAdrItem
            key={key}
            level={currentLevel}
            code={adr[0][currentAdr.code]}
            name={adr[0][currentAdr.name]}
            hasChildren={hasChildren}
            source={adr}
          />
        );
      })}
    </Fragment>
  );
};

BoxAdr.propTypes = {
  items: PropTypes.array.isRequired,
  level: PropTypes.number.isRequired,
  code: PropTypes.string
};

export default BoxAdr;
