import React, { Fragment, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash/groupBy';
import orderBy from 'lodash/orderBy';
import { Icon } from 'semantic-ui-react';
import { NestedAccordionTitle, NestedAccordionContent } from './styles';
import { useBoolean } from 'react-hanger';
import {
  AccordionAdr,
  AccordionCmDrug,
  AccordionComorbid,
  AccordionOutcome
} from './';

const RegimenItem = ({ name = '', items = [] }) => {
  const active = useBoolean(false);
  const [activeFeatureIndex, setActiveFeatureIndex] = useState(-1);

  const handleItemClick = useCallback(
    () => {
      active.toggle();
    },
    [active.value]
  );

  const handleFeatureClick = useCallback(
    (event, data) => {
      const newIndex = activeFeatureIndex === data.index ? -1 : data.index;
      setActiveFeatureIndex(newIndex);
    },
    [activeFeatureIndex]
  );

  return (
    <Fragment>
      <NestedAccordionTitle active={active.value} onClick={handleItemClick}>
        <Icon name="dropdown" />
        {name} <Icon name="user" color="blue" />
        {items.length}
      </NestedAccordionTitle>
      <NestedAccordionContent active={active.value}>
        <AccordionOutcome
          activeIndex={activeFeatureIndex}
          index={0}
          items={items}
          onClick={handleFeatureClick}
        />
        <AccordionAdr
          activeIndex={activeFeatureIndex}
          index={1}
          items={items}
          onClick={handleFeatureClick}
        />
        <AccordionComorbid
          activeIndex={activeFeatureIndex}
          index={2}
          items={items}
          onClick={handleFeatureClick}
        />
        <AccordionCmDrug
          activeIndex={activeFeatureIndex}
          index={3}
          items={items}
          onClick={handleFeatureClick}
        />
      </NestedAccordionContent>
    </Fragment>
  );
};

RegimenItem.propTypes = {
  name: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired
};

const RegimenBox = ({ items }) => {
  const groups = groupBy(items, 'regimen');
  const sortedGruops = orderBy(groups, g => g.length, 'desc');

  return (
    <Fragment>
      {sortedGruops.map((g, index) => {
        const key = g[0].regimen;
        const name = key.substr(0, key.length - 1).replaceAll('|', '/');
        return <RegimenItem key={key} name={name} items={g} />;
      })}
    </Fragment>
  );
};

RegimenBox.propTypes = {
  items: PropTypes.array.isRequired
};

export default RegimenBox;
