import helpers from '../../helpers';

const fetchRegimens = condition => {
  const query = condition ? `?$filter=${condition}` : '';
  return helpers.Service.get(`api/drugs/regimen${query}`);
};

export default {
  fetchRegimens
};
