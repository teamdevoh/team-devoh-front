import React, { Fragment, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Form, Label } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { DatePicker } from '../datepicker';
import useExTbDrug from '../distribution/hooks/useExTbDrug';
import helpers from '../../helpers';

const GuidelineConditionForm = ({ model = {}, onChange }) => {
  const t = useFormatMessage();
  const drug = useExTbDrug();

  const handleChange = useCallback(
    (event, data) => {
      const nextModel = { ...model, [data.name]: data.value };

      const drugs = _.filter(
        drug.items,
        option => nextModel.exdrugid.indexOf(option.key) > -1
      ).map(item => {
        if (item.text.toLowerCase().indexOf('/') > -1) {
          return item.text.toLowerCase().split('/')[1];
        }
        if (item.text.indexOf(' ') > -1) {
          return item.text.toLowerCase().split(' ')[0];
        }
        return item.text.toLowerCase();
      });

      onChange({ ...nextModel, drugNames: drugs });
    },
    [model]
  );

  return (
    <Fragment>
      <Form.Group>
        <Form.Field width="twelve">
          <label>{t('activity.diagnosis.title')}</label>
          <DatePicker
            name="dateDiagnosis"
            value={model.dateDiagnosis}
            onChange={handleChange}
          />
        </Form.Field>
        <Form.Field width="four">
          <label>{t('elapsed.days')}</label>
          <Label size="big">
            {model.dateDiagnosis
              ? helpers.util.diffDays(model.dateDiagnosis, new Date())
              : 0}
          </Label>
        </Form.Field>
      </Form.Group>
      <Form.Group inline>
        <label>{t('drug.resistance')}</label>
        <Form.Radio
          name="drugResistance"
          label="Yes"
          value="Y"
          checked={model.drugResistance === 'Y'}
          onChange={handleChange}
        />
        <Form.Radio
          name="drugResistance"
          label="No"
          value="N"
          checked={model.drugResistance === 'N'}
          onChange={handleChange}
        />
      </Form.Group>
      {model.drugResistance === 'Y' && (
        <Form.Field>
          <label>{t('activity.subtbdrug.exdrugid')}</label>
          <Dropdown
            name="exdrugid"
            value={model.exdrugid}
            fluid
            multiple
            search
            selection
            options={drug.items}
            onChange={handleChange}
          />
        </Form.Field>
      )}
      <Form.Group grouped>
        <label>{t('liver.dysfunction')}</label>
        <Form.Radio
          name="dysfunction"
          label="Yes"
          value="Y"
          checked={model.dysfunction === 'Y'}
          onChange={handleChange}
        />
        <Form.Radio
          name="dysfunction"
          label="No"
          value="N"
          checked={model.dysfunction === 'N'}
          onChange={handleChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('visual.abnorma')}</label>
        <Form.Radio
          name="visualAbnormality"
          label="Yes"
          value="Y"
          checked={model.visualAbnormality === 'Y'}
          onChange={handleChange}
        />
        <Form.Radio
          name="visualAbnormality"
          label="No"
          value="N"
          checked={model.visualAbnormality === 'N'}
          onChange={handleChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('hearing.ability')}</label>
        <Form.Radio
          name="hearingAbility"
          label="Yes"
          value="Y"
          checked={model.hearingAbility === 'Y'}
          onChange={handleChange}
        />
        <Form.Radio
          name="hearingAbility"
          label="No"
          value="N"
          checked={model.hearingAbility === 'N'}
          onChange={handleChange}
        />
      </Form.Group>
      <Form.Group grouped>
        <label>{t('hiv.infection')}</label>
        <Form.Radio
          name="hIVInfection"
          label="Yes"
          value="Y"
          checked={model.hIVInfection === 'Y'}
          onChange={handleChange}
        />
        <Form.Radio
          name="hIVInfection"
          label="No"
          value="N"
          checked={model.hIVInfection === 'N'}
          onChange={handleChange}
        />
      </Form.Group>
    </Fragment>
  );
};

GuidelineConditionForm.propTypes = {
  model: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired
};

export default GuidelineConditionForm;
