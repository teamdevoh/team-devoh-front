import { useCallback } from 'react';

const healthDomain = 'https://www.health.kr';

const useMovingDrugDetail = () => {
  const open = useCallback(drugCode => {
    window.open(
      `${healthDomain}/searchDrug/result_drug.asp?drug_cd=${drugCode}`,
      '_blank'
    );
  }, []);

  return { open };
};

export default useMovingDrugDetail;
