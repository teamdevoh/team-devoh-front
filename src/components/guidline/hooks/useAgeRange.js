const useAgeRange = () => {
  let age = { min: -1, max: -1 };
  let ageGroup = [];
  const ageRange = 10;
  for (let i = 0; i < 20; i++) {
    const min = i * ageRange;
    const max = min + ageRange - 1;
    ageGroup.push({ min: min, max: max });
  }

  const getRagne = value => {
    age = _.find(ageGroup, range => {
      return range.min <= value && range.max >= value;
    });

    if (age === void 0) {
      console.log('out of age range');
      return null;
    }

    return age;
  };

  return { getRagne };
};

export default useAgeRange;
