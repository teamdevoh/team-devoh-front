import { useCallback } from 'react';

const healthDomain = 'https://www.health.kr';

const useMoreDrugDetailList = () => {
  const open = useCallback(name => {
    window.open(
      `${healthDomain}/searchDrug/search_total_result.asp?search_word=${name}&search_flag=all`,
      '_blank'
    );
  }, []);

  return { open };
};

export default useMoreDrugDetailList;
