import { useCallback, useEffect, useState } from 'react';
import helpers from '../../../helpers';
import api from '../api';

const useBuildOdataQuery = () => {
  const [regimenItems, setRegimenItems] = useState([]);
  const odata = helpers.oDataBuilder;

  const [condition, setCondition] = useState({
    tbTreatmentcodeId: {
      text: '',
      value: null,
      index: null,
      filterValue: null,
      query: odata.equal
    },
    sexcodeId: {
      text: '',
      value: null,
      index: null,
      filterValue: null,
      query: odata.equal
    },
    age: {
      text: '',
      value: null,
      index: null,
      filterValue: [],
      query: odata.between
    },
    weight: {
      text: '',
      value: null,
      index: null,
      filterValue: [],
      query: odata.between
    },
    digFinCodeId: {
      text: '',
      value: null,
      index: null,
      filterValue: null,
      query: odata.equal
    },
    tbDiagnosiscodeId: {
      text: '',
      value: null,
      index: null,
      filterValue: null,
      query: odata.equal
    }
  });

  const handleDeleteCondtion = useCallback(
    targetKey => {
      Object.keys(condition).forEach(key => {
        if (key === targetKey) {
          condition[key].value = '';
          condition[key].index = null;
          condition[key].filterValue = null;
        }
      });

      setCondition({ ...condition });
    },
    [condition]
  );

  const update = useCallback(
    item => {
      setCondition({ ...condition, ...item });
    },
    [condition]
  );

  const fetchItems = async () => {
    let queries = [];
    const keys = Object.keys(condition);
    keys.forEach((key, index) => {
      const conditionItem = condition[key];
      if (
        conditionItem.filterValue > '' ||
        (Array.isArray(conditionItem.filterValue) &&
          conditionItem.filterValue.length > 0)
      ) {
        switch (condition[key].query.name) {
          case 'between':
            const values = conditionItem.filterValue;
            queries.push(`${conditionItem.query(key, values[0], values[1])}`);
            break;
          case 'equal':
            queries.push(
              `${conditionItem.query(key, conditionItem.filterValue)}`
            );
            break;
          default:
            break;
        }
      }
    });

    const query =
      queries.length === 0
        ? ''
        : queries.length > 1
          ? queries.join(' and ')
          : queries[0];

    const res = await api.fetchRegimens(query);
    if (res) {
      setRegimenItems(res.data);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [condition]
  );

  return {
    item: condition,
    setItem: setCondition,
    updateItem: update,
    onDeleteCondition: handleDeleteCondtion,
    regimenItems: regimenItems
  };
};

export default useBuildOdataQuery;
