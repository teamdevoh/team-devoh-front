const useWeightRange = () => {
  let weight = { min: -1, max: -1 };
  let weightGroup = [];
  const weightRange = 10;
  for (let i = 0; i < 20; i++) {
    const min = i * weightRange;
    const max = min + weightRange - 1;
    weightGroup.push({ min: min, max: max });
  }

  const getRagne = value => {
    weight = _.find(weightGroup, range => {
      return range.min <= value && range.max >= value;
    });

    if (weight === void 0) {
      console.log('out of weight range');
      return null;
    }

    return weight;
  };

  return { getRagne };
};

export default useWeightRange;
