import React, { Fragment, useCallback, useState } from 'react';
import {
  ConditionLabel,
  GuidelineConditionFormCPMTb,
  GuidelineConditionForm,
  GuidelineKoreanIT,
  GuidelineKoreanITList,
  GuidelineWrapper,
  GuidelineCPMTDrugList
} from './';
import { useBuildOdataQuery } from './hooks';
import {
  Divider,
  Form,
  Reveal,
  Segment,
  Image,
  Message
} from 'semantic-ui-react';
import SelectLanguage from '../account-manage/SelectLanguage';
import { GuidelineLanguageBox } from './styles';
import { code } from '../../constants';
import FindOnPDFButton from './FindOnPDFButton';
import { ResponsiveLarge, ResponsiveSmall } from '../element';

const GuidelineContainer = () => {
  const [modelForKorean, setModelForKorean] = useState({
    dateDiagnosis: '',
    drugResistance: '',
    exdrugid: [],
    drugNames: [],
    dysfunction: '',
    visualAbnormality: '',
    hearingAbility: '',
    hIVInfection: ''
  });
  const buildQuery = useBuildOdataQuery();

  const handleFormChange = useCallback(data => {
    buildQuery.updateItem(data.condition);
  }, []);

  const handleFormChangeForKorean = useCallback(data => {
    setModelForKorean(data);
  }, []);

  const treatmentCodeId = buildQuery.item.tbTreatmentcodeId.value;
  const treatmentType =
    treatmentCodeId === code.InitialTreatment
      ? 'IT'
      : treatmentCodeId === code.ReTreatment
        ? 'RT'
        : 'N/A';

  return (
    <Segment basic style={{ height: '100vh' }}>
      <Segment basic style={{ marginTop: '-20px', marginBottom: '35px' }}>
        <GuidelineLanguageBox>
          <SelectLanguage />
        </GuidelineLanguageBox>
      </Segment>
      <GuidelineWrapper
        panel1={
          <Form>
            <GuidelineConditionFormCPMTb
              onChange={handleFormChange}
              model={buildQuery.item}
            />
            <GuidelineConditionForm
              onChange={handleFormChangeForKorean}
              model={modelForKorean}
            />
          </Form>
        }
        panel2={
          <Fragment>
            {treatmentType === 'N/A' && (
              <Message info>
                <Message.Header>
                  결핵치료 여부를 먼저 선택해 주세요.
                </Message.Header>
              </Message>
            )}
            <ResponsiveSmall>
              {treatmentType === 'N/A' && (
                <Image src="/images/KoreanGuideline4th.png" />
              )}
              {treatmentType === 'IT' && (
                <Fragment>
                  <FindOnPDFButton
                    style={{
                      float: 'right',
                      marginBottom: '0.5rem'
                    }}
                  />
                  <Divider clearing />
                  <GuidelineKoreanIT />
                  <GuidelineKoreanITList
                    drugs={modelForKorean.drugNames}
                    model={{ ...buildQuery.item }}
                  />
                </Fragment>
              )}
            </ResponsiveSmall>
            <ResponsiveLarge>
              <Reveal
                animated={'move up'}
                active={treatmentType === 'N/A' ? false : true}
                disabled={treatmentType === 'N/A' ? true : false}
              >
                <Reveal.Content visible>
                  <Image
                    src="/images/KoreanGuideline4th.png"
                    style={{ height: '100vh' }}
                  />
                </Reveal.Content>
                <Reveal.Content hidden>
                  {treatmentType === 'IT' && (
                    <Fragment>
                      <FindOnPDFButton
                        style={{
                          float: 'right',
                          marginBottom: '0.5rem'
                        }}
                      />
                      <Divider clearing />
                      <GuidelineKoreanIT />
                      <GuidelineKoreanITList
                        drugs={modelForKorean.drugNames}
                        model={{ ...buildQuery.item }}
                      />
                    </Fragment>
                  )}
                </Reveal.Content>
              </Reveal>
            </ResponsiveLarge>
          </Fragment>
        }
        panel3={
          <Fragment>
            <ConditionLabel
              condition={buildQuery.item}
              onDeleteTagClick={buildQuery.onDeleteCondition}
            />
            <GuidelineCPMTDrugList items={buildQuery.regimenItems} />
          </Fragment>
        }
      />
    </Segment>
  );
};

export default GuidelineContainer;
