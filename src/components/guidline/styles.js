import { Accordion, Button, Segment } from 'semantic-ui-react';
import styled from 'styled-components';

export const ButtonGroup = styled(Button.Group).attrs({
  fluid: true
})`
  &&& {
    -ms-overflow-style: none;
    scrollbar-width: none;
    ::-webkit-scrollbar {
      display: none;
    }
    overflow-x: auto;
  }
`;

export const GuidelinePanelSegment = styled(Segment)`
  &&& {
    height: 85vh;
    padding: 20px;
    border: solid 1px #d2d2d2;
    overflow-y: auto;
    color: #666;
  }
`;

export const NestedAccordion = styled(Accordion).attrs({
  styled: true
})`
  &&& {
    box-shadow: 0 0 0 0;
  }
`;

export const NestedAccordionTitle = styled(Accordion.Title)`
  &&& {
    border-top: 0px !important;
    padding-top: 0em !important;
    padding-bottom: 0.5em !important;
  }
`;

export const NestedAccordionContent = styled(Accordion.Content)`
  &&& {
    padding-bottom: 0.5em !important;
  }
`;
export const GuidelineLanguageBox = styled.div`
  position: absolute;
  right: 1em;
  padding: 10px;
  border: solid 1px #d2d2d2;
  z-index: 9;
`;

export const ImportantText = styled.span`
  color: red;
`;
