import React, { Fragment } from 'react';
import { Icon } from 'semantic-ui-react';
import { NestedAccordionContent, NestedAccordionTitle } from './styles';
import { BoxRegimen } from './';

const AccordionRegimen = ({ activeIndex, index, items = [], onClick }) => {
  const active = activeIndex === index;
  return (
    <Fragment>
      <NestedAccordionTitle active={active} index={index} onClick={onClick}>
        <Icon name="dropdown" />
        Regimen [<Icon name="user" color="blue" />
        {items.length}]
      </NestedAccordionTitle>
      {active &&
        items.length > 0 && (
          <NestedAccordionContent active={active}>
            <BoxRegimen items={items} />
          </NestedAccordionContent>
        )}
    </Fragment>
  );
};

export default AccordionRegimen;
