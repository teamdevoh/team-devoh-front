import React, { Fragment, useCallback } from 'react';
import { Item, Popup } from 'semantic-ui-react';
import { useBoolean } from '../../hooks';

const KoreanDrugListItem = ({
  image,
  name,
  title,
  prescription,
  dosage,
  method,
  sideEffects,
  onImgClick,
  onHeaderClick
}) => {
  const loading = useBoolean(false);

  const handleDrugHeaderClick = useCallback(() => {
    loading.setTrue();
    let url = `/searchDrug/ajax/ajax_commonSearch.asp?search_word=${name}&search_flag=all`;
    fetch(url)
      .then(res => res.json())
      .then(data => {
        const totalCount = data.length > 0 ? data[0].total_cnt : 0;
        onHeaderClick({ name, title, totalCount, items: data });
      })
      .finally(() => {
        loading.setFalse();
      });
  }, []);

  return (
    <Item>
      {Array.isArray(image) && (
        <div>
          {image.map(img => {
            return (
              <Popup
                trigger={
                  <Fragment>
                    <Item.Image
                      size="tiny"
                      as="a"
                      src={`/images/${img}`}
                      onClick={() => {
                        onImgClick({
                          image: img
                        });
                      }}
                    />
                    <br />
                  </Fragment>
                }
                content="이미지를 클릭하면 확대 이미지를 볼 수 있습니다."
                inverted
              />
            );
          })}
        </div>
      )}
      {Array.isArray(image) === false && (
        <Popup
          trigger={
            <Item.Image
              size="tiny"
              as="a"
              src={`/images/${image}`}
              onClick={onImgClick}
            />
          }
          content="이미지를 클릭하면 확대 이미지를 볼 수 있습니다."
          inverted
        />
      )}
      <Item.Content>
        <Item.Header
          as="a"
          onClick={() => {
            loading.value === false && handleDrugHeaderClick();
          }}
        >
          <span style={{ opacity: loading.value === true ? '0.3' : '1' }}>
            {title}
          </span>
        </Item.Header>
        <Item.Description>
          <p>처방 용량 : {prescription}</p>
          <p>용량(최대 용량) : {dosage}</p>
          <p>투여방법 : {method}</p>
          <p>주요 부작용 : {sideEffects}</p>
        </Item.Description>
      </Item.Content>
    </Item>
  );
};
export default KoreanDrugListItem;
