import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Header } from 'semantic-ui-react';
import { KoreanDrugList } from './';

const GuidelineKoreanITList = ({ drugs = [], model = {} }) => {
  return (
    <Fragment>
      <Header>초치료 항결핵제의 체중에 따른 용량과 투약방법</Header>
      <KoreanDrugList skipNames={drugs} initials={['H', 'R', 'E', 'Z']} />
    </Fragment>
  );
};

GuidelineKoreanITList.propTypes = {
  drugs: PropTypes.array.isRequired,
  model: PropTypes.object.isRequired
};

export default GuidelineKoreanITList;
