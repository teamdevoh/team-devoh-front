import React, { Fragment, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import { NestedAccordionContent, NestedAccordionTitle } from './styles';
import { BoxAdr } from './';

const BoxAdrItem = ({
  level,
  hasChildren = false,
  code,
  name,
  source = []
}) => {
  const [active, setActive] = useState(false);

  const handleAccordionClick = useCallback(
    (event, data) => {
      setActive(!data.active);
    },
    [active]
  );

  return (
    <Fragment>
      <NestedAccordionTitle
        active={hasChildren ? active : false}
        onClick={handleAccordionClick}
      >
        <Icon name={hasChildren ? 'dropdown' : 'circle'} />
        {name} <Icon name="briefcase" color="blue" /> {source.length}
      </NestedAccordionTitle>
      <NestedAccordionContent active={hasChildren ? active : false}>
        {active &&
          hasChildren && <BoxAdr items={source} level={level} filter={code} />}
      </NestedAccordionContent>
    </Fragment>
  );
};

BoxAdrItem.propTypes = {
  level: PropTypes.number.isRequired,
  hasChildren: PropTypes.bool.isRequired,
  code: PropTypes.string,
  name: PropTypes.string.isRequired,
  source: PropTypes.array.isRequired
};

export default BoxAdrItem;
