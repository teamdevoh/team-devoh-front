import React, { Fragment } from 'react';
import { Icon } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { NestedAccordionContent, NestedAccordionTitle } from './styles';
import { BoxOutcome } from './';

const AccordionOutcome = ({ activeIndex, index, items = [], onClick }) => {
  const t = useFormatMessage();
  const active = activeIndex === index;
  return (
    <Fragment>
      <NestedAccordionTitle active={active} index={index} onClick={onClick}>
        <Icon name="dropdown" />
        {t('activity.caseConclusion.caseconCodeId')} [
        <Icon name="user" color="blue" />
        {items.length}]
      </NestedAccordionTitle>
      {active &&
        items.length > 0 && (
          <NestedAccordionContent active={active}>
            <BoxOutcome items={items} />
          </NestedAccordionContent>
        )}
    </Fragment>
  );
};

export default AccordionOutcome;
