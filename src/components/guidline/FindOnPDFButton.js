import React, { useState, Fragment } from 'react';
import { Icon } from 'semantic-ui-react';
import PdfViewer from '../viewer';
//임시
import TreatmentGuidelinesPdf from '../viewer/결핵진료지침(4판)-(Web용)_최종.pdf';

const FindOnPDFButton = ({ style }) => {
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  return (
    <Fragment>
      <Icon
        name="file pdf outline"
        style={{ ...style, cursor: 'pointer' }}
        size="big"
        color="red"
        onClick={handleClick}
      />
      <PdfViewer
        open={open}
        onOK={() => setOpen(false)}
        src={TreatmentGuidelinesPdf}
        pageNumber={56}
        name="결핵진료지침(4판)-(Web용)_최종"
        type="application/pdf"
      />
    </Fragment>
  );
};

export default FindOnPDFButton;
