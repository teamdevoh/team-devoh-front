import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import { BoxAdr } from '.';
import { NestedAccordionContent, NestedAccordionTitle } from './styles';

const AccordionAdr = ({ activeIndex = -1, index = 0, items = [], onClick }) => {
  const adrItems = _.filter(items, item => item.adr.length > 0);
  let adrs = [];
  adrItems.forEach(item => {
    const filtered = _.filter(item.adr, item => item.aesoccd > '');
    filtered.forEach(adr => {
      adr.adrName = adr.adrName.toLowerCase().trim();
    });

    adrs = [...adrs, ...filtered];
  });

  const active = activeIndex === index;

  return (
    <Fragment>
      <NestedAccordionTitle active={active} index={index} onClick={onClick}>
        <Icon name="dropdown" />
        ADR [<Icon name="briefcase" color="blue" />
        {adrs.length}]
      </NestedAccordionTitle>
      {active &&
        adrs.length > 0 && (
          <NestedAccordionContent active={active}>
            <BoxAdr level={0} items={adrs} code={''} />
          </NestedAccordionContent>
        )}
    </Fragment>
  );
};

AccordionAdr.propTypes = {
  activeIndex: PropTypes.number,
  index: PropTypes.number,
  items: PropTypes.array,
  onClick: PropTypes.func
};

export default AccordionAdr;
