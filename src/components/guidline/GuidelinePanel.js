import React, { useCallback } from 'react';
import { useBoolean } from 'react-hanger';
import { Grid, Header, Icon } from 'semantic-ui-react';
import { GuidelinePanelSegment } from './styles';

const GuidelinePanel = ({
  children,
  color,
  index,
  width,
  onExpandClick,
  onCompressClick,
  title
}) => {
  const isPanelExpand = useBoolean(false);

  const handleExpandClick = useCallback(
    () => {
      isPanelExpand.setTrue();
      onExpandClick(index);
    },
    [onExpandClick]
  );

  const handleCompressClick = useCallback(
    () => {
      isPanelExpand.setFalse();
      onCompressClick(index);
    },
    [onCompressClick]
  );

  return (
    <Grid.Column
      width={width}
      style={{ display: width === 0 ? 'none' : 'block' }}
    >
      <Header as="h2" dividing color={color}>
        {title}
      </Header>
      {isPanelExpand.value === false && (
        <Icon name="expand" className="link" onClick={handleExpandClick} />
      )}
      {isPanelExpand.value === true && (
        <Icon name="compress" className="link" onClick={handleCompressClick} />
      )}
      <GuidelinePanelSegment>{children}</GuidelinePanelSegment>
    </Grid.Column>
  );
};

export default GuidelinePanel;
