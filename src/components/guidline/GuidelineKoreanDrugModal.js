import React from 'react';
import { Button, Form, Image, Modal } from 'semantic-ui-react';

const GuidelineKoreanDrugModal = ({
  open,
  onClose,
  name,
  imgUrl,
  prescription,
  dosage,
  method,
  sideEffects
}) => {
  return (
    <Modal onClose={onClose} open={open}>
      <Modal.Header>{name}</Modal.Header>
      <Modal.Content image>
        <Image size="medium" src={imgUrl} wrapped />
        <Modal.Description>
          <Form>
            <Form.Field>
              <label>처방 용량</label>
              {prescription}
            </Form.Field>
            <Form.Field>
              <label>용량(최대 용량)</label>
              {dosage}
            </Form.Field>
            <Form.Field>
              <label>투여방법</label>
              {method}
            </Form.Field>
            <Form.Field>
              <label>주요 부작용</label>
              {sideEffects}
            </Form.Field>
          </Form>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={onClose} positive>
          Close
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default GuidelineKoreanDrugModal;
