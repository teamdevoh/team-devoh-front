import React, { Fragment, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Form, Icon, Popup, Radio } from 'semantic-ui-react';
import { code, codeGroup } from '../../constants';
import { useCodes, useFormatMessage } from '../../hooks';
import { SusceptPopup } from '../activity/diagnosis';
import { DigFin, TBDiagnosis } from '../activity/Selector';
import { IntegerInput, DecimalInput } from '../input';
import { useAgeRange, useWeightRange } from './hooks';

let lastIndex = 0;

const GuidelineConditionFormCPMTb = ({ model, onChange }) => {
  const [sexCodes] = useCodes({ codeGroupId: codeGroup.SexCode });
  const ageRange = useAgeRange();
  const weightRange = useWeightRange();

  const t = useFormatMessage();

  const handleChange = useCallback(
    (event, data) => {
      const condition = {
        ...model,
        [data.name]: {
          ...model[data.name],
          filterValue: data.filterValue,
          text: data.label,
          value: data.value
        }
      };

      Object.keys(condition).forEach((key, index) => {
        const conditionItem = condition[key];
        if (conditionItem.value !== null && conditionItem.value !== '') {
          conditionItem.index =
            conditionItem.index === null ? lastIndex : conditionItem.index;
          lastIndex++;
        }
      });

      onChange({ condition: condition });
    },
    [model]
  );

  return (
    <Fragment>
      <Form.Group grouped>
        <Form.Field>
          <label>{t('activity.diagnosis.tbTreatmentcodeId')}</label>
          <Radio
            name="tbTreatmentcodeId"
            label="초치료 (Initial treatment)"
            value={code.InitialTreatment}
            checked={model.tbTreatmentcodeId.value === code.InitialTreatment}
            onChange={(event, data) => {
              handleChange(event, {
                ...data,
                filterValue: data.value
              });
            }}
          />
          <Popup
            wide="very"
            inverted
            trigger={<Icon name="question circle" className="link" />}
          >
            <Popup.Header>초치료 Initial treatment</Popup.Header>
            <Popup.Content>
              <div>
                이전에 결핵치료를 받은 적이 없거나, 1개월 미만의 결핵치료를 받은
                환자.
                <br />
                Subjets who have never received TB treatment before, or who have
                received TB treatment before for less than 1 month.
              </div>
            </Popup.Content>
          </Popup>
        </Form.Field>
        <Form.Field>
          <Radio
            name="tbTreatmentcodeId"
            label="재치료 (Re-treatment)"
            value={code.ReTreatment}
            checked={model.tbTreatmentcodeId.value === code.ReTreatment}
            onChange={(event, data) => {
              handleChange(event, {
                ...data,
                filterValue: data.value
              });
            }}
          />
          <Popup
            wide="very"
            inverted
            trigger={<Icon name="question circle" className="link" />}
          >
            <Popup.Header>재치료 Re-treatment</Popup.Header>
            <Popup.Content>
              <div>
                이전에 결핵 치료에 실패하였거나, 완치 후 재발되어 다시 치료하는
                경우, 최소 2개월간 치료를 중단하였다가 다시 치료하는 경우.
                <br />
                If subjects failed to respond to their previous treatment,
                suffer relapse after completion of previous treatment, or are
                re-treated after stopping previous treatment for at least 2
                months.
              </div>
            </Popup.Content>
          </Popup>
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <label>{t('activity.interview.sexcodeId')}</label>
        {sexCodes.map(code => {
          return (
            <Form.Radio
              key={code.value}
              name="sexcodeId"
              label={`${code.note}(${code.text})`}
              value={code.value}
              checked={model.sexcodeId.value === code.value}
              onChange={(event, data) => {
                handleChange(event, {
                  ...data,
                  filterValue: data.value
                });
              }}
            />
          );
        })}
      </Form.Group>
      <Form.Field>
        <label>{t('common.age')}</label>
        <IntegerInput
          name="age"
          value={model.age.value}
          onChange={(event, data) => {
            const age = ageRange.getRagne(data.value);
            handleChange(event, {
              ...data,
              value: age ? data.value : '',
              filterValue: age ? [age.min, age.max] : [],
              label: age ? `${t('common.age')} ${age.min} ~ ${age.max}` : ''
            });
          }}
        />
      </Form.Field>
      <Form.Field required>
        <label>{t('body.weight')}</label>
        <DecimalInput
          name="weight"
          options={{ suffix: ' kg' }}
          value={model.weight.value}
          hideLabel={true}
          onChange={(event, data) => {
            const weight = weightRange.getRagne(data.value);
            handleChange(event, {
              ...data,
              value: weight ? data.value : '',
              filterValue: weight ? [weight.min, weight.max] : [],
              label: weight
                ? `${t('body.weight')} ${weight.min}kg ~ ${weight.max}kg`
                : ''
            });
          }}
        />
      </Form.Field>
      <Form.Field>
        <label>
          {t('activity.diagnosis.tbSusceptcodeId')}
          <SusceptPopup />
        </label>
        <DigFin
          name="digFinCodeId"
          value={model.digFinCodeId.value}
          onChange={(event, data) => {
            const foundOption = _.find(data.options, { key: data.value });
            const label = foundOption ? foundOption.text : '';
            handleChange(event, {
              ...data,
              value: data.value,
              filterValue: data.value,
              label: label
            });
          }}
        />
      </Form.Field>
      <Form.Group grouped>
        <label>{t('activity.diagnosis.tbDiagnosiscodeId')}</label>
        <TBDiagnosis
          name="tbDiagnosiscodeId"
          value={model.tbDiagnosiscodeId.value}
          onChange={(event, data) => {
            const label = _.find(data.options, { key: data.value }).text;
            handleChange(event, {
              ...data,
              filterValue: data.value,
              label: label
            });
          }}
        />
      </Form.Group>
    </Fragment>
  );
};

GuidelineConditionFormCPMTb.propTypes = {
  model: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired
};

export default GuidelineConditionFormCPMTb;
