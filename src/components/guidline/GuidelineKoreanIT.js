import React from 'react';
import { Header, List } from 'semantic-ui-react';
import { ImportantText } from './styles';

const GuidelineKoreanIT = () => {
  return (
    <List as="ol">
      <List.Item as="li">
        <Header>피라진마이드(Rifabutin) 사용이 가능한 경우</Header>
        <List.Item as="ol">
          <List.Item as="li" value="-">
            2개월 초기 집중치료기 처방 : <ImportantText>HREZ</ImportantText>
          </List.Item>
          <List.Item as="li" value="-">
            <ImportantText>
              3개월 유지기 연장 여부 : 객담배양검사 양성 <br />( 개별 환자의
              상황을 감안하여 신중히 결정 권고 )
            </ImportantText>
          </List.Item>
          <List.Item as="li" value="-">
            4개월 후기 유지치료기
            <List.Item as="ol">
              <List.Item as="li" value="*">
                이소니아지드 및 리팜핀 감수성 :{' '}
                <ImportantText>HR</ImportantText>
              </List.Item>
              <List.Item as="li" value="*">
                약제감수성 검사 결과 확인되지 않았거나, 균음성 결핵인 경우 :{' '}
                <ImportantText>HRE</ImportantText>
              </List.Item>
            </List.Item>
          </List.Item>
          <List.Item as="li" value="-">
            약제내성 결핵 환자로부터 전염되었을 것으로 추정되는 초치료 환자의
            경우에 전염원이 되는 환자의 약제감수성 결과에 근거하여 개별적으로
            처방하고, 약제감수성검사 결과에 따라 처방을 수정한다.
          </List.Item>
        </List.Item>
      </List.Item>
      <List.Item as="li">
        <Header>피라진마이드(Rifabutin) 사용이 불가한 경우</Header>
        <List.Item as="ol">
          <List.Item as="li" value="-">
            9개월간 지속 처방 : <ImportantText>HRE</ImportantText>
          </List.Item>
        </List.Item>
      </List.Item>
    </List>
  );
};

export default GuidelineKoreanIT;
