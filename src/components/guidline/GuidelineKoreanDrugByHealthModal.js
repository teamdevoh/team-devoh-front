import React, { useCallback } from 'react';
import { Button, Icon, Item, Modal, Popup } from 'semantic-ui-react';
import { useMoreDrugDetailList, useMovingDrugDetail } from './hooks';

const LinkDrugImgButton = ({ drugCode, src }) => {
  const { open } = useMovingDrugDetail();
  const handleItemClick = useCallback(() => {
    open(drugCode);
  }, []);

  return <Item.Image size="small" as="a" src={src} onClick={handleItemClick} />;
};

const LinkMoreButton = ({ name }) => {
  const { open } = useMoreDrugDetailList();
  const handleMoreClick = useCallback(() => {
    open(name);
  }, []);

  return (
    <a style={{ float: 'right' }} className="link" onClick={handleMoreClick}>
      검색결과 더보기 <Icon name="plus" />
    </a>
  );
};

const HealthHeader = ({ title = '', totalCount = 0 }) => (
  <div>
    {title} 약학정보원 의약품검색 결과(
    {totalCount})
  </div>
);

const HealthItemImage = ({ drug_code = '', drug_pic = '', pack_img = '' }) => (
  <Popup
    trigger={
      <LinkDrugImgButton
        src={
          drug_pic ? drug_pic : pack_img ? pack_img : 'images/image-square.png'
        }
        drugCode={drug_code}
      />
    }
    content="이미지를 클릭하면 의약품 상세정보를 볼 수 있습니다."
    inverted
  />
);

const HealthItemTitle = ({ text, onClick }) => (
  <Item.Header as="a" onClick={onClick}>
    {text}
  </Item.Header>
);

const DesIngredient = ({ listSunbName, sunbCount, ingrMg }) => (
  <p>
    성분/함량 : {listSunbName}
    {sunbCount > 1 && (
      <Popup
        trigger={
          <span>
            외 {sunbCount - 1}
            <Icon name="eye" className="link" />
          </span>
        }
        content={
          <div
            dangerouslySetInnerHTML={{
              __html: `<p>${ingrMg
                .replaceAll('@', '')
                .replaceAll('|', '<br />')}</p>`
            }}
          />
        }
        inverted
      />
    )}
  </p>
);

const DesEffect = ({ text }) => (
  <p>
    효능 :
    <Popup
      trigger={<Icon name="eye" className="link" />}
      content={
        <div
          dangerouslySetInnerHTML={{
            __html: `<p>효능 / 효과</p><p>${text.replaceAll(
              'br',
              '<br />'
            )}</p>`
          }}
        />
      }
      inverted
    />
  </p>
);

const DesKFDAName = ({ text }) => <p>회사명 : {text}</p>;

const DesDrugForm = ({ text }) => <p>제형 : {text}</p>;

const DesDrugClass = ({ text }) => <p>구분 : {text}</p>;

const DesBohPrice = ({ text }) => <p>약가 : {text}</p>;

const GuidelineKoreanDrugByHealthModal = ({
  name,
  open,
  title,
  totalCount,
  items,
  onClose
}) => {
  const drugDetail = useMovingDrugDetail();

  return (
    <Modal open={open} onClose={onClose}>
      <Modal.Header>
        <HealthHeader />
      </Modal.Header>
      <Modal.Content scrolling>
        <Item.Group>
          {items.map(item => {
            return (
              <Item key={item.drug_code}>
                <HealthItemImage {...item} />
                <Item.Content>
                  <HealthItemTitle
                    text={item.drug_name}
                    onClick={() => {
                      drugDetail.open(item.drug_code);
                    }}
                  />
                  <Item.Description>
                    <DesIngredient
                      listSunbName={item.list_sunb_name}
                      sunbCount={item.sunb_count}
                      ingrMg={item.ingr_mg}
                    />
                    <DesEffect text={item.effect} />
                    <DesKFDAName text={item.upso_name_kfda} />
                    <DesDrugForm text={item.drug_form} />
                    <DesDrugClass text={item.drug_class} />
                    <DesBohPrice text={item.boh_price} />
                  </Item.Description>
                </Item.Content>
              </Item>
            );
          })}
        </Item.Group>
        {totalCount > 10 && <LinkMoreButton name={name} />}
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={onClose} positive>
          Close
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default GuidelineKoreanDrugByHealthModal;
