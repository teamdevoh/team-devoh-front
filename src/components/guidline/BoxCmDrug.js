import React from 'react';
import PropTypes from 'prop-types';
import { Header, Icon, Item } from 'semantic-ui-react';

const CmDrugBox = ({ items = [] }) => {
  return (
    <Item.Group>
      {items.map(item => {
        const name = item[0].cmtrt;
        return (
          <Item key={name} style={{ marginBottom: '-2em' }}>
            <Item.Content>
              <Header as="h5">{name}</Header>
              <Item.Description>
                <Icon name="briefcase" color="blue" />
                {item.length}
              </Item.Description>
            </Item.Content>
          </Item>
        );
      })}
    </Item.Group>
  );
};

CmDrugBox.propTypes = {
  items: PropTypes.array.isRequired
};

export default CmDrugBox;
