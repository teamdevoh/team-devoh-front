import React from 'react';
import { Button } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import useUser from '../account-manage/hooks/useUser';
import { DataGrid } from '../data-grid';
import { format } from '../../constants';

const BoxBoard = ({ items, loading, onRemove, rowRenderer }) => {
  const t = useFormatMessage();
  const [{ profile }] = useUser();

  const columns = [
    {
      key: 'boxBarcode',
      name: 'Box ID',
      width: 140
    },
    {
      key: 'project',
      name: t('project'),
      width: 140
    },
    {
      key: 'site',
      name: t('project.site')
    },
    {
      key: 'visit',
      name: 'Visit'
    },
    {
      key: 'sampleMaterial',
      name: 'Material'
    },
    {
      key: 'aliquotAnalyte',
      name: 'Analyte'
    },
    {
      key: 'aliquotCnt',
      name: 'Count'
    },
    {
      key: 'boxLocation',
      name: 'Freezer'
    },
    {
      key: 'currentDateTime',
      name: 'Freezer Out',
      formatter: ({ value, row }) => {
        return helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM);
      }
    },
    {
      key: 'aliquotMemberName',
      name: 'User',
      width: 150,
      formatter: ({ value, row }) => {
        return profile.name;
      }
    },
    {
      key: 'remove',
      name: 'Remove',
      width: 110,
      formatter: ({ value, row }) => {
        const { sampleBoxId } = row;
        return (
          <Button
            onClick={onRemove({
              sampleBoxId
            })}
            size="tiny"
          >
            {t('folder.remove')}
          </Button>
        );
      }
    }
  ];

  return (
    <div>
      <DataGrid
        columns={columns}
        rows={items}
        minHeight={600}
        loading={loading}
        rowNumber={{ show: true, key: 'no', name: 'No' }}
        rowKey="boxBarcode"
        rowRenderer={rowRenderer}
      />
    </div>
  );
};

export default BoxBoard;
