import React, { useState, useEffect, useRef } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import BoxBoard from './BoxBoard';
import DashBoardTool from './DashBoardTool';
import DashBoardHeader from './DashBoardHeader';
import notification from '../modal/notification';
import { useFormatMessage, useSubmit } from '../../hooks';
import api from './api';

const DashBoard = () => {
  const t = useFormatMessage();
  const boxIdEl = useRef(null);
  const [sampleBoxInfo, setSampleBoxInfo] = useState({});
  const [boxItems, setBoxItems] = useState([]);
  const [removeBoxId, setRemoveBoxId] = useState();
  const [loading, setLoading] = useState(false);
  const [freezerOutCodeId, setFreezerOutCodeId] = useState();

  useEffect(
    () => {
      if (sampleBoxInfo.boxBarcode) {
        const addedBox = boxItems.filter(
          item => item.sampleBoxId === sampleBoxInfo.boxBarcode
        );

        if (addedBox.length === 0) {
          setBoxItems(boxItems.concat([{ ...sampleBoxInfo }]));
        }
      } else {
        setBoxItems([]);
      }
    },
    [sampleBoxInfo.boxBarcode]
  );

  useEffect(
    () => {
      if (removeBoxId) {
        const newItems = boxItems.filter(
          item => removeBoxId !== item.sampleBoxId
        );

        setBoxItems(newItems);
        setSampleBoxInfo({});
        setRemoveBoxId();
        boxIdEl.current.focus();
      }
    },
    [removeBoxId]
  );

  const handleBoxRemove = ({ sampleBoxId }) => () => {
    setRemoveBoxId(sampleBoxId);
  };

  const handleSaveAliquotSubmit = async () => {
    boxIdEl.current.focus();
    setLoading(true);
    const model = boxItems.map(item => {
      const { sampleBoxId, currentDateTime } = item;
      return {
        sampleBoxId,
        boxStatusDateTime: currentDateTime,
        freezerOutCodeId
      };
    });
    try {
      const res = await api.updateSampleBoxesFreezerOut({
        model
      });

      if (res && res.data) {
        notification.success({
          title: t('common.ok'),
          onClose: () => {}
        });
        setSampleBoxInfo({});
      } else {
        notification.error({});
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const handleFreezerOutChange = (e, { name, value }) => {
    setFreezerOutCodeId(value);
  };

  const [{ validator, onValidate }] = useSubmit(handleSaveAliquotSubmit);

  return (
    <Segment basic>
      <Grid.Row>
        <Grid.Column width={16}>
          <DashBoardHeader
            sampleBoxInfo={sampleBoxInfo}
            setSampleBoxInfo={setSampleBoxInfo}
            freezerOutCodeId={freezerOutCodeId}
            setFreezerOutCodeId={setFreezerOutCodeId}
            onFreezerOutChange={handleFreezerOutChange}
            boxIdEl={boxIdEl}
            validator={validator}
            boxItems={boxItems}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool
            items={boxItems}
            onValidate={onValidate}
            loading={loading}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <BoxBoard items={boxItems} onRemove={handleBoxRemove} />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
