import React from 'react';
import { Radio } from 'semantic-ui-react';
import { useCodes } from '../../hooks';
import { RadioWrap } from './styles';
import { codeGroup } from '../../constants';

export const FreezerOutReason = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.FreezerOutCode });
  return (
    <RadioWrap>
      {items.map(item => (
        <Radio
          key={item.value}
          name={name}
          label={item.text}
          value={item.value}
          checked={item.value === value}
          onChange={onChange}
        />
      ))}
    </RadioWrap>
  );
};
