import React, { useState, useEffect } from 'react';
import { Form } from 'semantic-ui-react';
import { StyledForm } from '../sample-status/styles';
import styled from 'styled-components';
import notification from '../modal/notification';
import { useFormatMessage } from '../../hooks';
import { useSampleBoxInfoToBoxingByBarcode } from '../sample-boxing/hooks';
import { StyledInput } from '../sample-aliquot/styles';
import { FreezerOutReason } from './Selector';
import { code } from '../../constants';

const StyledDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const DashBoardHeader = ({
  sampleBoxInfo,
  setSampleBoxInfo,
  freezerOutCodeId,
  onFreezerOutChange,
  boxIdEl,
  validator,
  boxItems
}) => {
  const t = useFormatMessage();
  const [sampleBoxBarcode, setSampleBoxBarcode] = useState('');

  const [
    { loading: sampleInfoLoading, fetch: fetchSampleBoxInfo }
  ] = useSampleBoxInfoToBoxingByBarcode();

  useEffect(() => {
    boxIdEl.current.focus();
  }, []);

  useEffect(
    () => {
      if (!sampleBoxInfo.sampleBoxId) {
        setSampleBoxBarcode('');
        boxIdEl.current.focus();
      }
    },
    [sampleBoxInfo.sampleBoxId]
  );

  const handleBoxIdKeyUp = async e => {
    const value = e.target.value.trim();

    if (e.keyCode === 13 || e.key.toLowerCase() === 'enter') {
      if (value !== '') {
        const addedBox = boxItems.filter(item => item.boxBarcode === value);

        if (addedBox.length > 0) {
          notification.warning({
            title: t('added.item', { name: 'Box' })
          });
          setSampleBoxBarcode('');
          boxIdEl.current.focus();
        } else {
          const boxInfo = await fetchSampleBoxInfo(value);
          if (boxInfo) {
            if (boxInfo.sampleBoxId > 0) {
              if (
                boxInfo.boxStatusCodeId === code.OutOfStock ||
                boxInfo.boxStatusCodeId == null
              ) {
                notification.warning({
                  title: t('disabled.freezer.out')
                });
                setSampleBoxBarcode('');
                boxIdEl.current.focus();
              } else if (boxInfo.boxStatusCodeId === code.Warehousing) {
                setSampleBoxInfo(boxInfo);
                setSampleBoxBarcode('');
              }
            } else {
              // 존재하지 않는 box id
              setSampleBoxInfo({});
              boxIdEl.current.focus();
              setSampleBoxBarcode('');
              notification.warning({
                title: t('does.not.exist', { name: 'Box ID' })
              });
            }
          }
        }
      } else {
        notification.warning({
          title: t('empty.info', { name: 'Box ID' })
        });
      }
    }
  };

  const handleBarcodeChange = (event, { value, name }) => {
    setSampleBoxBarcode(value);
  };

  return (
    <StyledDiv>
      <StyledForm>
        <Form.Field required inline>
          <label>Reason</label>
          <FreezerOutReason
            name="freezerOutCodeId"
            value={freezerOutCodeId}
            onChange={onFreezerOutChange}
          />
          {validator.message('Reason', freezerOutCodeId, 'required')}
        </Form.Field>
        <Form.Field required>
          <label>Box ID</label>
          <StyledInput
            size="massive"
            name="boxId"
            ref={boxIdEl}
            onKeyUp={handleBoxIdKeyUp}
            onChange={handleBarcodeChange}
            value={sampleBoxBarcode}
            loading={sampleInfoLoading}
          />
        </Form.Field>
      </StyledForm>
    </StyledDiv>
  );
};

export default DashBoardHeader;
