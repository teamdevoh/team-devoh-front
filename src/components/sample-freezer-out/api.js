import helpers from '../../helpers';

const updateSampleBoxesFreezerOut = ({ model }) => {
  return helpers.Service.put(`api/samples/boxes.out`, model);
};

export default {
  updateSampleBoxesFreezerOut
};
