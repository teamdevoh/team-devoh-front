import styled from 'styled-components';

export const RadioWrap = styled.div`
  & .ui.radio.checkbox:not(:last-child) {
    margin-right: 10px;
  }
`;
