import React from 'react';
import { List } from 'semantic-ui-react';
import ActivityItem from './ActivityItem';

const ActivityList = ({ items, selecteds, onItemClick }) => {
  return (
    <List relaxed divided>
      {items.map((item, index) => {
        return (
          <ActivityItem
            key={`${item.activityId}_${index}`}
            id={item.activityId}
            {...item}
            selecteds={selecteds}
            onClick={onItemClick}
          />
        );
      })}
    </List>
  );
};

export default ActivityList;
