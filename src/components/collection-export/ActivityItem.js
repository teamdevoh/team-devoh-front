import React from 'react';
import { useBoolean } from 'react-hanger';
import { List, Checkbox, Grid, Icon } from 'semantic-ui-react';
import { CohortPreviewModal } from '../export-data-manage';

const ActivityItem = ({ id, name, note, selecteds, onClick }) => {
  const showModal = useBoolean(false);

  const handleChecked = (event, data) => {
    onClick(id, data.checked);
  };

  return (
    <List.Item>
      <List.Description>
        <Grid stackable>
          <Grid.Column width="eight">
            <Checkbox
              label={name}
              checked={selecteds[id]}
              onChange={handleChecked}
            />
            <Icon
              name="search"
              style={{ marginLeft: '0.5em', cursor: 'pointer' }}
              onClick={showModal.setTrue}
            />
            <CohortPreviewModal
              name={id}
              show={showModal.value}
              onClose={showModal.setFalse}
            />
          </Grid.Column>
          <Grid.Column textAlign="left" width="eight">
            {note}
          </Grid.Column>
        </Grid>
      </List.Description>
    </List.Item>
  );
};

export default ActivityItem;
