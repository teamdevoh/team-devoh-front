import React, { useCallback, useState, Fragment } from 'react';
import {
  Button,
  Progress,
  Accordion,
  Icon,
  Divider,
  Checkbox,
  Input,
  Grid,
  Header,
  TransitionablePortal,
  Sidebar,
  Message
} from 'semantic-ui-react';
import { useActivities, useExport, useProgressingName } from './hooks';
import ActivityList from './ActivityList';
import pickBy from 'lodash/pickBy';
import ConditionForm from './ConditionForm';
import { useFormFields, useFormatMessage } from '../../hooks';
import { useParams } from 'react-router-dom';
import Run from '../animation/Run';
import Title from './Title';
import { role, activity } from '../../constants';
import { RoleAware } from '../auth';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';

const ExportContainer = () => {
  const t = useFormatMessage();
  const params = useParams();

  const [projectSiteItems] = useMyProjectSiteList({
    projectId: params.projectId
  });

  const searchOption = {
    sexCodeId: 0,
    ageFrom: null,
    ageTo: null,
    bmiFrom: null,
    bmiTo: null,
    smokeCodeId: 0,
    projectSiteId: 0,
    docName: '-',
    tbSusceptcodeIds: [],
    tbDiagnosiscodeIds: [],
    exdrugids: [],
    hasADR: 0,
    aDRKeyword: '',
    caseconCodeId: 0
  };

  const av = activity;
  const relatedGroupItems = [
    {
      name: 'Basic Patient Information',
      values: [
        av.INTERVIEW,
        av.VS,
        av.CONSENT,
        av.TB_DIAGNOSIS,
        av.COMORBID,
        av.DIALYSIS,
        av.ICRE
      ]
    },
    { name: 'Medication History', values: [av.MEDICATION, av.COMED] },
    {
      name: 'Microbiological Exam',
      values: [av.AFB, av.XPERT, av.LPARESISTANT, av.IGRA, av.PCR, av.DST]
    },
    {
      name: 'Lab & Radiologic Findings',
      values: [av.LBCH, av.LBHEM, av.ELECTROLYTE, av.XRAY, av.CT, av.Other_exam]
    },
    {
      name: 'TDM',
      values: [
        av.TDMAnti,
        av.SAMPLING,
        av.TB_PK,
        av.PP,
        av.GENOTYPE,
        av.TDMREP,
        av.MIC_TRNS,
        av.MIC_RESULT,
        av.MICReport,
        av.RESISTANT_GENE
      ]
    },
    { name: 'ADR', values: [av.ADR, av.ADR_AGEP, av.ADR_DRESS, av.ADR_SJSTEN] },
    { name: 'Monitoring & Evaluation', values: [av.CASE_CON, av.Relapse] }
  ];

  const [{ model, onChange }] = useFormFields(searchOption);
  const [progress, setProgress] = useState(0);
  const [visibleList, setVisibleList] = useState(true);
  const [selecteds, setSelecteds] = useState({});
  const [activitySearch, setActivitySearch] = useState('');
  const { items } = useActivities({ projectId: params.projectId });

  const cohortExport = useExport({
    cb: percent => {
      setProgress(percent);
    }
  });

  const progressingName = useProgressingName({
    activities: items,
    downloadingFileIds: cohortExport.downloadingFileIds,
    selectedKeys: pickBy(selecteds, (value, key) => value === true)
  });

  const handleClick = useCallback(
    isFromRedis => id => {
      const selectedItems = pickBy(selecteds, (value, key) => value === true);

      setProgress(0);

      const ids = Object.keys(selectedItems);
      if (ids.length > 0) {
        let orderedKeys = [];
        relatedGroupItems.forEach(group => {
          group.values.forEach(key => {
            if (ids.indexOf(key.toString()) > -1) {
              orderedKeys.push(key.toString());
            }
          });
        });

        const searchOptions = { ...model };

        if (!!!searchOptions.projectSiteId && projectSiteItems.length === 1) {
          searchOptions.projectSiteId = projectSiteItems[0].value;
        }

        cohortExport.start(orderedKeys, searchOptions, isFromRedis);
      }
    },
    [progress, selecteds, model, projectSiteItems]
  );

  const handleBlankCRFClick = useCallback(
    id => {
      const selectedItems = pickBy(selecteds, (value, key) => value === true);

      setProgress(0);

      const ids = Object.keys(selectedItems);
      if (ids.length > 0) {
        let orderedKeys = [];
        relatedGroupItems.forEach(group => {
          group.values.forEach(key => {
            if (ids.indexOf(key.toString()) > -1) {
              orderedKeys.push(key.toString());
            }
          });
        });
        cohortExport.startBlankCRF(orderedKeys);
      }
    },
    [progress, selecteds, model]
  );

  const handleItemClick = (id, checked) => {
    selecteds[id] = checked;
    setSelecteds({ ...selecteds });
  };

  const handleActivityTitleClick = () => {
    setVisibleList(!visibleList);
  };

  const handleSelectedAll = useCallback(
    (event, data) => {
      for (let index = 0; index < items.length; index++) {
        const item = items[index];
        selecteds[item.activityId] = data.checked;
      }
      setSelecteds({ ...selecteds });
    },
    [items, selecteds]
  );

  const getItemsByGroup = useCallback(
    relatedKeys => {
      if (items.length > 0) {
        let groupItems = [];

        relatedKeys.forEach(relatedKey => {
          groupItems.push(_.find(items, { activityId: relatedKey }));
        });
        return groupItems;
      }
      return [];
    },
    [items]
  );

  const filterByKeyword = (source, keyword) => {
    return _.filter(source, item => {
      return (
        keyword === '' ||
        ((item.name &&
          item.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1) ||
          (item.note &&
            item.note.toLowerCase().indexOf(keyword.toLowerCase()) > -1))
      );
    });
  };

  return (
    <Fragment>
      <Grid style={{ paddingBottom: '125px' }}>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Fragment>
              <ConditionForm
                projectId={params.projectId}
                model={model}
                onChange={onChange}
                projectSiteItems={projectSiteItems}
              />
              <Divider />

              <Accordion>
                <Accordion.Title
                  active={visibleList}
                  index={0}
                  onClick={handleActivityTitleClick}
                >
                  <Icon name="dropdown" />
                  Activity
                </Accordion.Title>
                <Accordion.Content active={visibleList}>
                  <Grid>
                    <Grid.Column width="sixteen">
                      <Input
                        action={t('common.search')}
                        placeholder=""
                        value={activitySearch}
                        onChange={(event, data) => {
                          setActivitySearch(data.value);
                        }}
                      />
                    </Grid.Column>
                    <Grid.Column width="sixteen">
                      <Checkbox label={'All'} onChange={handleSelectedAll} />
                      <p>
                        Selected :
                        {
                          _.filter(
                            Object.keys(selecteds),
                            key => selecteds[key] === true
                          ).length
                        }
                      </p>
                      <p>
                        5개씩 하나의 파일로 생성됩니다.
                        <br />
                        It is created as one file of five.
                      </p>
                    </Grid.Column>

                    <Grid.Column width="sixteen">
                      {relatedGroupItems.map((group, index) => {
                        return (
                          <Fragment key={group.name}>
                            <Header>{group.name}</Header>
                            <ActivityList
                              items={filterByKeyword(
                                getItemsByGroup(group.values),
                                activitySearch
                              )}
                              selecteds={selecteds}
                              onItemClick={handleItemClick}
                            />
                          </Fragment>
                        );
                      })}
                    </Grid.Column>
                  </Grid>
                </Accordion.Content>
              </Accordion>
            </Fragment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <RoleAware allowedRole={role.Activity_Export_Excel_Read}>
        <TransitionablePortal
          open={true}
          transition={{ animation: 'swing down', duration: 1000 }}
          closeOnDocumentClick={false}
        >
          <Sidebar
            style={{
              position: 'fixed',
              bottom: 0
            }}
            direction="bottom"
            animation="push"
          >
            <Message info style={{ height: '161px', bottom: 0 }}>
              <Grid stackable>
                <Grid.Column width="two">
                  {/* <Button
                    color="blue"
                    size="tiny"
                    disabled={cohortExport.loading}
                    onClick={handleClick(false)}
                    loading={cohortExport.loading}
                  >
                    {t('common.export')}
                  </Button> */}
                  <Button
                    color="blue"
                    size="tiny"
                    disabled={cohortExport.loading}
                    onClick={handleClick(true)}
                    loading={cohortExport.loading}
                  >
                    {t('common.export')}
                  </Button>
                  <Button
                    color="blue"
                    size="tiny"
                    disabled={cohortExport.loading}
                    onClick={handleBlankCRFClick}
                    loading={cohortExport.loading}
                  >
                    Blank CRF Form
                  </Button>
                </Grid.Column>
                <Grid.Column width="fourteen">
                  <Progress percent={_.round(progress, 0)} indicating progress>
                    {progressingName}
                    {cohortExport.loading && <Run />}
                  </Progress>
                </Grid.Column>
              </Grid>
            </Message>
          </Sidebar>
        </TransitionablePortal>
      </RoleAware>
    </Fragment>
  );
};

export default ExportContainer;
