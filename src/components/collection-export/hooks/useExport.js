import { useState, useCallback } from 'react';
import { notification } from '../../modal';
import { useFormatMessage } from '../../../hooks';
import api from '../api';

const reqMax = 5;

const useExport = ({ sexCodeId = '', cb }) => {
  const t = useFormatMessage();
  const [loading, setLoading] = useState(false);
  const [downloadingFileIds, setDownloadingFileIds] = useState({
    source: [],
    currentIndex: 0
  });

  let progressEachValues = {};
  let fileCount = 0;

  const handleCb = (percent, index) => {
    progressEachValues[index] = percent;

    let sumProgress = 0;
    Object.keys(progressEachValues).forEach(key => {
      sumProgress += progressEachValues[key];
    });

    const average = sumProgress / fileCount;
    cb(average);
  };

  const start = useCallback(
    async (ids, searchOptions, isFromRedis) => {
      try {
        const chunkLength = Math.ceil(ids.length / reqMax);
        fileCount = chunkLength;

        const reqGroups = [];

        for (let i = 1; i < chunkLength + 1; i++) {
          var sliced = ids.slice((i - 1) * reqMax, i * reqMax);
          reqGroups.push(sliced.join(','));
        }

        progressEachValues = {};

        setLoading(true);

        exportExcel(reqGroups, 0, searchOptions, isFromRedis);
      } catch (error) {
        console.log(error);
        setLoading(false);
      }
    },
    [loading]
  );

  const startBlankCRF = useCallback(
    async ids => {
      try {
        const chunkLength = Math.ceil(ids.length / reqMax);
        fileCount = chunkLength;

        const reqGroups = [];

        for (let i = 1; i < chunkLength + 1; i++) {
          var sliced = ids.slice((i - 1) * reqMax, i * reqMax);
          reqGroups.push(sliced.join(','));
        }
        progressEachValues = {};

        setLoading(true);

        for (let i = 0; i < reqGroups.length; i++) {
          const reqGroup = reqGroups[i];
          await api.exportBlankCRFForm({ activityId: reqGroup });
        }
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    },
    [loading]
  );

  const exportExcel = async (ids, index, searchOptions, isFromRedis) => {
    setDownloadingFileIds({ source: ids, currentIndex: index });

    if (isFromRedis) {
      return await api
        .exportActivityFromRedis({
          ...searchOptions,
          activityId: ids[index],
          cb: percent => {
            handleCb(percent, index);
          }
        })
        .then(() => {
          if (ids.length > index + 1) {
            return exportExcel(ids, index + 1, searchOptions, isFromRedis);
          } else {
            setLoading(false);
          }
        })
        .catch(err => {
          console.error('Failed export');
          setLoading(false);
        });
    } else {
      if (!!searchOptions.projectSiteId) {
        return await api
          .exportActivity({
            ...searchOptions,
            activityId: ids[index],
            cb: percent => {
              handleCb(percent, index);
            }
          })
          .then(() => {
            if (ids.length > index + 1) {
              return exportExcel(ids, index + 1, searchOptions, isFromRedis);
            } else {
              setLoading(false);
            }
          })
          .catch(err => {
            console.error('Failed export');
            setLoading(false);
          });
      } else {
        try {
          const res = await api.sendEmailAboutActivity({
            ...searchOptions,
            activityId: ids.join(',')
          });

          if (res && res.data) {
            notification.info({
              title: t('excel.file.request.complete'),
              confirmButtonName: t('common.ok'),
              text: t('excel.file.send.email')
            });
          }
        } catch (err) {}
        setLoading(false);
      }
    }
  };

  return {
    loading,
    start,
    startBlankCRF,
    downloadingFileIds: downloadingFileIds
  };
};

export default useExport;
