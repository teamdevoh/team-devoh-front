import { useEffect, useState } from 'react';

const useProgressingName = ({
  activities,
  downloadingFileIds,
  selectedKeys
}) => {
  const [name, setName] = useState('');

  const buildName = items => {
    let name = '';
    items.forEach(item => {
      name += item.name + ',';
    });
    return name;
  };

  useEffect(
    () => {
      if (downloadingFileIds.source.length > 0) {
        const filteredItems = _.filter(activities, item => {
          const reqIds = downloadingFileIds.source[
            downloadingFileIds.currentIndex
          ].split(',');
          return reqIds.indexOf(item.activityId.toString()) > -1;
        });

        if (filteredItems.length > 0) {
          const name = buildName(filteredItems);
          setName(
            `${downloadingFileIds.currentIndex + 1} / ${
              downloadingFileIds.source.length
            } ${name}`
          );
        }
      }
    },
    [downloadingFileIds]
  );

  return name;
};

export default useProgressingName;
