import { useEffect, useState } from 'react';
import api from '../api';

const useActivities = ({ projectId }) => {
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchActivities({ projectId });
    if (res && res.data) {
      const orderedItems = _.orderBy(_.uniqBy(res.data, 'activityId'), [
        'activityId'
      ]);
      setItems(orderedItems);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId]
  );

  return { items };
};

export default useActivities;
