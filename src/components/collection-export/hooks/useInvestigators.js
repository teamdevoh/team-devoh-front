import api from '../api';
import { useState } from 'react';
import { useEffect } from 'react';

const useInvestigators = ({ projectId, projectSiteId }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = async () => {
    setLoading(true);
    const res = await api.fetchInvestigators({ projectId, projectSiteId });
    if (res && res.data) {
      let options = [];
      res.data.forEach(item => {
        options.push({ key: item, text: item, value: item });
      });
      setItems(options);
    }
    setLoading(false);
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, projectSiteId]
  );

  return { loading, items };
};

export default useInvestigators;
