import helpers from '../../helpers';

const exportActivity = ({
  sexCodeId,
  ageFrom,
  ageTo,
  bmiFrom,
  bmiTo,
  smokeCodeId,
  projectSiteId,
  docName,
  tbSusceptcodeIds,
  tbDiagnosiscodeIds,
  exdrugids,
  hasADR,
  aDRKeyword,
  caseconCodeId,
  activityId,
  cb
}) => {
  const params = helpers.qs.stringify({
    sexCodeId,
    ageFrom,
    ageTo,
    bmiFrom,
    bmiTo,
    smokeCodeId,
    projectSiteId,
    docName,
    activities: 'S,' + activityId,
    tbSusceptcodeIds,
    tbDiagnosiscodeIds,
    exdrugids,
    hasADR: hasADR === 0 ? null : hasADR,
    aDRKeyword,
    caseconCodeId
  });

  return helpers.util
    .download(`api/subjects/activities.export?${params}`, cb)
    .catch(err => {
      return Promise.reject(err);
    });
};

const exportActivityFromRedis = ({
  sexCodeId,
  ageFrom,
  ageTo,
  bmiFrom,
  bmiTo,
  smokeCodeId,
  projectSiteId,
  docName,
  tbSusceptcodeIds,
  tbDiagnosiscodeIds,
  exdrugids,
  hasADR,
  aDRKeyword,
  caseconCodeId,
  activityId,
  cb
}) => {
  const params = helpers.qs.stringify({
    sexCodeId,
    ageFrom,
    ageTo,
    bmiFrom,
    bmiTo,
    smokeCodeId,
    projectSiteId,
    docName,
    activities: 'S,' + activityId,
    tbSusceptcodeIds,
    tbDiagnosiscodeIds,
    exdrugids,
    hasADR: hasADR === 0 ? null : hasADR,
    aDRKeyword,
    caseconCodeId
  });

  return helpers.util
    .download(`api/subjects/activities.export.redis?${params}`, cb)
    .catch(err => {
      return Promise.reject(err);
    });
};

const sendEmailAboutActivity = ({
  sexCodeId,
  ageFrom,
  ageTo,
  bmiFrom,
  bmiTo,
  smokeCodeId,
  projectSiteId,
  docName,
  tbSusceptcodeIds,
  tbDiagnosiscodeIds,
  exdrugids,
  hasADR,
  aDRKeyword,
  caseconCodeId,
  activityId
}) => {
  const params = helpers.qs.stringify({
    sexCodeId,
    ageFrom,
    ageTo,
    bmiFrom,
    bmiTo,
    smokeCodeId,
    projectSiteId,
    docName,
    activities: activityId,
    tbSusceptcodeIds,
    tbDiagnosiscodeIds,
    exdrugids,
    hasADR: hasADR === 0 ? null : hasADR,
    aDRKeyword,
    caseconCodeId
  });

  return helpers.Service.get(`api/subjects/activities.export.all?${params}`);
};

const exportBlankCRFForm = ({ activityId }) => {
  const params = helpers.qs.stringify({
    activities: 'S,' + activityId
  });

  return helpers.util
    .download(`api/subjects/activities.export.blankcrf?${params}`, () => {})
    .catch(err => {
      return Promise.reject(err);
    });
};

const fetchActivities = ({ projectId }) => {
  return helpers.Service.get(`api/projects/${projectId}/activities`);
};

const fetchInvestigators = ({ projectId, projectSiteId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/sites/${projectSiteId}/investigators`
  );
};

export default {
  exportActivity,
  exportActivityFromRedis,
  sendEmailAboutActivity,
  exportBlankCRFForm,
  fetchActivities,
  fetchInvestigators
};
