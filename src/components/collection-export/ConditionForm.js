import React, { useEffect } from 'react';
import { Form, Input, Select, Dropdown } from 'semantic-ui-react';
import {
  Sex,
  SmokeStatus,
  TBSuscept,
  TBDiagnosis,
  Casecon
} from '../activity/Selector';
import { useFormatMessage } from '../../hooks';
import { SliderInput } from '../input';
import { useInvestigators } from './hooks';
import { SusceptPopup } from '../activity/diagnosis';
import { TbdrugList } from '../collection-tbdrug/Selector';

const ConditionForm = ({ projectId, model, onChange, projectSiteItems }) => {
  const t = useFormatMessage();
  const investigator = useInvestigators({
    projectId,
    projectSiteId: model.projectSiteId
  });

  useEffect(
    () => {
      onChange(null, { name: 'docName', value: '-' });
    },
    [model.projectSiteId]
  );

  return (
    <Form size="small">
      <Form.Group>
        <Form.Field width="four">
          <label>{t('site')}</label>
          <Dropdown
            fluid
            selection
            name="projectSiteId"
            defaultValue={model.projectSiteId}
            onChange={onChange}
            options={[
              {
                text: t('common.all'),
                value: 0,
                subjectprefix: ''
              }
            ].concat(projectSiteItems)}
          />
        </Form.Field>
        <Form.Field width="six">
          <label>{t('activity.interview.docName')}</label>
          <Select
            name="docName"
            item
            loading={investigator.loading}
            value={model.docName}
            onChange={onChange}
            options={[
              { key: '-', text: '-', value: '-' },
              ...investigator.items
            ]}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field width="four">
          <label>{t('activity.interview.sexcodeId')}</label>
          <Sex name="sexCodeId" value={model.sexCodeId} onChange={onChange} />
        </Form.Field>
        <Form.Field width="six">
          <label>
            {t('activity.interview.age')}
            (From)
          </label>
          <SliderInput
            label={t('activity.interview.age')}
            name="ageFrom"
            type="number"
            value={model.ageFrom}
            onChange={onChange}
            marks={{
              0: <p style={{ color: 'green' }}>0</p>,
              30: <p style={{ color: 'green' }}>30</p>,
              50: <p style={{ color: 'green' }}>50</p>,
              70: <p style={{ color: 'green' }}>70</p>,
              100: <p style={{ color: 'green' }}>100</p>
            }}
          />
        </Form.Field>
        <Form.Field width="six">
          <label>
            {t('activity.interview.age')}
            (To)
          </label>
          <SliderInput
            label={t('activity.interview.age')}
            name="ageTo"
            type="number"
            value={model.ageTo}
            onChange={onChange}
            marks={{
              0: <p style={{ color: 'green' }}>0</p>,
              30: <p style={{ color: 'green' }}>30</p>,
              50: <p style={{ color: 'green' }}>50</p>,
              70: <p style={{ color: 'green' }}>70</p>,
              100: <p style={{ color: 'green' }}>100</p>
            }}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field width="four">
          <label>{t('activity.interview.smokecodeId')}</label>
          <SmokeStatus
            name="smokeCodeId"
            value={model.smokeCodeId}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field width="six">
          <label>
            {t('activity.body.bmi')}
            (From)
          </label>
          <SliderInput
            label={'BMI'}
            name="bmiFrom"
            type="decimal"
            value={model.bmiFrom}
            onChange={onChange}
            marks={{
              0: <p style={{ color: 'green' }}>0</p>,
              10: <p style={{ color: 'green' }}>10</p>,
              20: <p style={{ color: 'green' }}>20</p>,
              30: <p style={{ color: 'green' }}>30</p>,
              40: <p style={{ color: 'green' }}>40</p>,
              50: <p style={{ color: 'green' }}>50</p>,
              70: <p style={{ color: 'green' }}>70</p>,
              100: <p style={{ color: 'green' }}>100</p>
            }}
          />
        </Form.Field>
        <Form.Field width="six">
          <label>
            {t('activity.body.bmi')}
            (To)
          </label>
          <SliderInput
            label={'BMI'}
            name="bmiTo"
            type="decimal"
            value={model.bmiTo}
            onChange={onChange}
            marks={{
              0: <p style={{ color: 'green' }}>0</p>,
              10: <p style={{ color: 'green' }}>10</p>,
              20: <p style={{ color: 'green' }}>20</p>,
              30: <p style={{ color: 'green' }}>30</p>,
              40: <p style={{ color: 'green' }}>40</p>,
              50: <p style={{ color: 'green' }}>50</p>,
              70: <p style={{ color: 'green' }}>70</p>,
              100: <p style={{ color: 'green' }}>100</p>
            }}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field width="four">
          <label>
            {t('activity.diagnosis.tbSusceptcodeId')}
            <SusceptPopup />
          </label>
          <TBSuscept
            name="tbSusceptcodeIds"
            value={model.tbSusceptcodeIds}
            onChange={onChange}
            multiple={true}
          />
        </Form.Field>
        <Form.Field width="six">
          <label>{t('activity.diagnosis.tbDiagnosiscodeId')}</label>
          <TBDiagnosis
            name="tbDiagnosiscodeIds"
            value={model.tbDiagnosiscodeIds}
            onChange={onChange}
            multiple={true}
          />
        </Form.Field>
        <Form.Field width="six">
          <label>{t('activity.subtbdrug.exdrugid')}</label>
          <TbdrugList
            name="exdrugids"
            value={model.exdrugids}
            onChange={onChange}
            multiple={true}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field width="four">
          <label>ADR</label>
          <Select
            name="hasADR"
            item
            value={model.hasADR}
            onChange={onChange}
            options={[
              { key: 0, text: '-', value: 0 },
              { key: true, text: 'Yes', value: true },
              { key: false, text: 'No', value: false }
            ]}
          />
        </Form.Field>
        <Form.Field width="six">
          <label>ADR, SOC, PT</label>
          <Input
            name="aDRKeyword"
            value={model.aDRKeyword}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field width="six">
          <label>{t('activity.caseConclusion.caseconCodeId')}</label>
          <Casecon
            name="caseconCodeId"
            defaultOption={{ key: 0, text: '-', value: 0 }}
            value={model.caseconCodeId}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
    </Form>
  );
};

export default ConditionForm;
