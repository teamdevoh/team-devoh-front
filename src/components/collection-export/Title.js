import React from 'react';
import { Header } from 'semantic-ui-react';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';

const Title = () => {
  return (
    <Header as="h3" dividing>
      Export
      <Header.Subheader>Export Cohort data</Header.Subheader>
      <CopyRedirectUrl />
    </Header>
  );
};

export default Title;
