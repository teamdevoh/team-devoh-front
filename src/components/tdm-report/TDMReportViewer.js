import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Table } from 'semantic-ui-react';
import crApi from '../case-review/api';
import afApi from '../activity-file/api';
import FileViewerWrapper from '../viewer/FileViewerWrapper';
import helpers from '../../helpers';
import { YYYY_MM_DD_HHMM } from '../../constants/format';
import TDMReportRemoveButton from './TDMReportRemoveButton';

const TDMReportViewer = ({
  subjectId,
  subjectVisitId,
  refresh,
  onRefreshed,
  onRenderFiles
}) => {
  const [files, setFiles] = useState([]);
  const [fileMeta, setFileMeta] = useState({
    url: '',
    key: '',
    name: '',
    type: ''
  });

  const getSignedUrl = async () => {
    const res = await crApi.fetchReportsForTDM(subjectId);
    if (res && res.data) {
      setFiles(_.filter(res.data, { subjectVisitId }));
    }
  };

  const setFileInfo = ({
    activityKeyId,
    activityAttachmentId,
    originalName
  }) => {
    setFileMeta({
      type: 'application/pdf',
      name: originalName,
      key: activityAttachmentId,
      activityKeyId,
      activityAttachmentId
    });
  };

  const handleFileClick = useCallback(async file => {
    const { activityKeyId, activityAttachmentId, originalName } = file;
    setFileInfo({ activityKeyId, activityAttachmentId, originalName });
  }, []);

  useEffect(
    () => {
      getSignedUrl();
      onRefreshed();
    },
    [subjectVisitId, refresh]
  );

  useEffect(
    () => {
      if (files.length === 1) {
        const { activityKeyId, activityAttachmentId, originalName } = files[0];
        setFileInfo({ activityKeyId, activityAttachmentId, originalName });
      }
      onRenderFiles(files);
    },
    [files]
  );

  useEffect(
    () => {
      if (files.length > 0) {
        const file = files[0];

        if (
          fileMeta.key !== file.activityAttachmentId &&
          fileMeta.name !== file.originalName
        ) {
          handleFileClick({
            activityKeyId: file.activityKeyId,
            activityAttachmentId: file.activityAttachmentId,
            originalName: file.originalName
          });
        }
      }
    },
    [files, fileMeta]
  );

  return (
    <Fragment>
      <Table compact="very">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>TDM Report File</Table.HeaderCell>
            <Table.HeaderCell>Date Time</Table.HeaderCell>
            <Table.HeaderCell>User</Table.HeaderCell>
            <Table.HeaderCell>Remove</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {files.map(file => {
            return (
              <Table.Row key={file.activityAttachmentId}>
                <Table.Cell>
                  <a
                    className="link"
                    onClick={() => {
                      handleFileClick({
                        activityKeyId: file.activityKeyId,
                        activityAttachmentId: file.activityAttachmentId,
                        originalName: file.originalName
                      });
                    }}
                  >
                    {file.originalName}
                  </a>
                </Table.Cell>
                <Table.Cell>
                  {helpers.util.dateformat(file.created, YYYY_MM_DD_HHMM)}
                </Table.Cell>
                <Table.Cell>{file.creator}</Table.Cell>
                <Table.Cell>
                  <TDMReportRemoveButton
                    subjectId={subjectId}
                    activityKeyId={file.activityKeyId}
                    activityAttachmentId={file.activityAttachmentId}
                    onRemoved={key => {
                      const newFiles = _.filter(files, file => {
                        return key !== file.activityAttachmentId;
                      });
                      setFiles(newFiles);
                      setFileMeta({
                        url: '',
                        key: '',
                        name: '',
                        type: ''
                      });
                    }}
                    fileName={file.originalName}
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>

      {fileMeta.name.length > 0 && (
        <FileViewerWrapper
          fileInfo={fileMeta}
          fetchSignedUrl={fileInfo => {
            const index = _.findIndex(files, {
              activityAttachmentId: fileInfo.activityAttachmentId
            });
            if (index > -1) {
              return afApi.fetchArchivePresignedurl({
                subjectId,
                activityKeyId: fileInfo.activityKeyId,
                activityAttachmentId: fileInfo.key
              });
            }
          }}
          open={true}
          onClose={() => {}}
          getDownloadUrl={() => {}}
          useModalType={false}
        />
      )}
    </Fragment>
  );
};

export default TDMReportViewer;
