import React from 'react';
import { Tab } from 'semantic-ui-react';

const ScriptBuildTab = ({ panes, onTabChange }) => {
  return (
    <Tab
      onTabChange={onTabChange}
      panes={[
        {
          menuItem: 'General',
          render: () => <Tab.Pane>{panes[0]}</Tab.Pane>
        },
        {
          menuItem: 'Custom',
          render: () => <Tab.Pane>{panes[1]}</Tab.Pane>
        }
      ]}
    />
  );
};

export default ScriptBuildTab;
