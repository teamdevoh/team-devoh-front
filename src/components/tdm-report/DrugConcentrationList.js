import React from 'react';
import { Table } from 'semantic-ui-react';

const DrugConcentrationList = ({ data }) => {
  return (
    <Table compact="very" celled fixed singleLine>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell title="Dose">Dose</Table.HeaderCell>
          <Table.HeaderCell title="Number">Number</Table.HeaderCell>
          <Table.HeaderCell title="Formulation">Formulation</Table.HeaderCell>
          <Table.HeaderCell
            style={{ width: '140px' }}
            title="Time of administration"
          >
            Time of administration
          </Table.HeaderCell>
          <Table.HeaderCell style={{ width: '140px' }} title="Sampling">
            Sampling
          </Table.HeaderCell>
          <Table.HeaderCell title="TAD">TAD</Table.HeaderCell>
          <Table.HeaderCell title="Concentration">
            Concentration
          </Table.HeaderCell>
          <Table.HeaderCell title="AUC">AUC</Table.HeaderCell>
          <Table.HeaderCell title="Cmax">Cmax</Table.HeaderCell>
          <Table.HeaderCell title="Date">Date</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {data.map(item => {
          const {
            sampleTime,
            exdose,
            exdosfrqName,
            exdosfrmName,
            medDatetime,
            tad,
            drugConc,
            drugConcUnit,
            ppAuc,
            ppCmax,
            resultDTC
          } = item;

          return (
            <Table.Row key={sampleTime}>
              <Table.Cell title={exdose}>{`${exdose} mg`}</Table.Cell>
              <Table.Cell title={exdosfrqName}>{exdosfrqName}</Table.Cell>
              <Table.Cell title={exdosfrmName}>{exdosfrmName}</Table.Cell>
              <Table.Cell>{medDatetime}</Table.Cell>
              <Table.Cell title={sampleTime}>{sampleTime}</Table.Cell>
              <Table.Cell title={`${tad} hr`}>{`${tad} hr`}</Table.Cell>
              <Table.Cell>{`${drugConc ? drugConc : ''} ${
                drugConcUnit ? drugConcUnit : ''
              }`}</Table.Cell>
              <Table.Cell title={ppAuc}>{ppAuc}</Table.Cell>
              <Table.Cell title={ppCmax}>{ppCmax}</Table.Cell>
              <Table.Cell title={resultDTC}>{resultDTC}</Table.Cell>
            </Table.Row>
          );
        })}
      </Table.Body>
    </Table>
  );
};

export default DrugConcentrationList;
