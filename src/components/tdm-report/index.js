import { asyncComponent } from '../modules';

const SubjectList = asyncComponent(() =>import('./SubjectList'));
const SubjectInfomation = asyncComponent(() => import('./SubjectInfomation'));
const SubjectTDMVisit = asyncComponent(() => import('./SubjectTDMVisit'));
const DrugConcentration = asyncComponent(() => import('./DrugConcentration'));
const TimeData = asyncComponent(() => import('./TimeData'));
const TDMReportViewer = asyncComponent(() => import('./TDMReportViewer'));
const ScriptBuildTab = asyncComponent(() => import('./ScriptBuildTab'));
const ScriptBuildLayout = asyncComponent(() => import('./ScriptBuildLayout'));
const PreBuildRun = asyncComponent(() => import('./PreBuildRun'));
const BuildRun = asyncComponent(() => import('./BuildRun'));
const CustomBuildRun = asyncComponent(() => import('./CustomBuildRun'));
const ReportViewButton = asyncComponent(() => import('./ReportViewButton'));
const DrugConcentrationList = asyncComponent(() => import('./DrugConcentrationList'));

export {
  SubjectList,
  SubjectInfomation,
  SubjectTDMVisit,
  DrugConcentration,
  TimeData,
  TDMReportViewer,
  ScriptBuildTab,
  ScriptBuildLayout,
  PreBuildRun,
  BuildRun,
  CustomBuildRun,
  ReportViewButton,
  DrugConcentrationList
};
