import helpers from '../../helpers';

const fetchSubjectsForTDMReport = ({ projectId, projectSiteId, subjectId }) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    subjectId
  });
  return helpers.Service.get(`api/subjects.tdmreport?${params}`);
};

const runReport = (
  { files, subjectId, activityKeyId, projectActivityId },
  onUploadProgress = f => f
) => {
  files.key = 'files';
  files.files = files;
  const params = helpers.qs.stringify({
    subjectId,
    activityKeyId,
    projectActivityId
  });
  return helpers.Service.upload(
    `api/subjects.tdmreport.run?${params}`,
    files,
    percent => {
      onUploadProgress(percent);
    }
  );
};

const finishedTDMReport = ({
  subjectId,
  activityKeyId,
  projectActivityId,
  payload
}) => {
  const params = helpers.qs.stringify({
    subjectId,
    activityKeyId,
    projectActivityId
  });
  return helpers.Service.post(
    `api/subjects.tdmreport.finished?${params}`,
    payload
  );
};

export default {
  fetchSubjectsForTDMReport,
  runReport,
  finishedTDMReport
};
