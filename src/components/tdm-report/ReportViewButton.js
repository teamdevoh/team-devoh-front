import React, { Fragment } from 'react';
import { Icon } from 'semantic-ui-react';

const ReportViewButton = ({ name = '', url = '' }) => {
  return (
    <Fragment>
      <Icon name={`eye ${name.length === 0 ? 'slash' : ''}`} />
      {name.length > 0 && (
        <a href={url} rel="noopener noreferrer" target="_blank">
          View {name}
        </a>
      )}
    </Fragment>
  );
};

export default ReportViewButton;
