import React, { Fragment } from 'react';
import { Table } from 'semantic-ui-react';
import { useTbdrug } from '../collection-tbdrug/hooks';
import { CSVExport } from '../export';
import useCSVOpt from './hooks/useCSVOpt';

const TimeData = ({ data = {}, exDrugId }) => {
  const [drug] = useTbdrug(exDrugId);
  const csvOpt = useCSVOpt();

  const targetData = data[exDrugId];

  const opt = csvOpt.get({ name: drug.model.shortName });

  return (
    <Fragment>
      <CSVExport
        data={targetData}
        fileName={opt.fileName}
        fields={opt.fields}
        fieldNames={opt.fieldNames}
      />
      <div
        style={{
          height: '300px',
          padding: '20px',
          border: 'solid 1px #d2d2d2',
          overflowY: 'auto'
        }}
      >
        <Table compact="very">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>ID</Table.HeaderCell>
              <Table.HeaderCell>TIME</Table.HeaderCell>
              <Table.HeaderCell>AMT</Table.HeaderCell>
              <Table.HeaderCell>II</Table.HeaderCell>
              <Table.HeaderCell>SS</Table.HeaderCell>
              <Table.HeaderCell>DV</Table.HeaderCell>
              <Table.HeaderCell>MDV</Table.HeaderCell>
              <Table.HeaderCell>EVID</Table.HeaderCell>
              <Table.HeaderCell>AGE</Table.HeaderCell>
              <Table.HeaderCell>BWT</Table.HeaderCell>
              <Table.HeaderCell>HT</Table.HeaderCell>
              <Table.HeaderCell>GENDER</Table.HeaderCell>
              <Table.HeaderCell>CMT</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {targetData.map(item => {
              return (
                <Table.Row key={item.TIME}>
                  <Table.Cell>{item.ID}</Table.Cell>
                  <Table.Cell>{item.TIME}</Table.Cell>
                  <Table.Cell>{item.AMT}</Table.Cell>
                  <Table.Cell>{item.II}</Table.Cell>
                  <Table.Cell>{item.SS}</Table.Cell>
                  <Table.Cell>{item.DV}</Table.Cell>
                  <Table.Cell>{item.MDV}</Table.Cell>
                  <Table.Cell>{item.EVID}</Table.Cell>
                  <Table.Cell>{item.AGE}</Table.Cell>
                  <Table.Cell>{item.BWT}</Table.Cell>
                  <Table.Cell>{item.HT}</Table.Cell>
                  <Table.Cell>{item.GENDER}</Table.Cell>
                  <Table.Cell>{item.CMT}</Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </div>
    </Fragment>
  );
};

export default TimeData;
