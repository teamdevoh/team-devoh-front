import React, { Fragment, useEffect, useState } from 'react';
import { Table } from 'semantic-ui-react';
import {
  useADRForCR,
  useSubjectCR,
  useSummaryForCR
} from '../case-review/hooks';
import { useCodes } from '../../hooks';
import { activity, activityGroup, code, codeGroup } from '../../constants';
import { useGetProjectActivityId } from '../collection/hooks';
import { useTbdrugList } from '../collection-tbdrug/hooks';

const SubjectInfomation = ({ subjectId }) => {
  const subject = useSubjectCR(subjectId);
  const [subjectSummary] = useSummaryForCR(subjectId);
  const [summary, setSummary] = useState({
    height: 0,
    weight: 0,
    tbStart: '',
    antiTbDrug: [],
    comorbids: [],
    medications: [],
    diagnosises: []
  });
  const [dosfrqCodes] = useCodes({
    codeGroupId: codeGroup.DosfrqCode
  });
  const [routeCodes] = useCodes({
    codeGroupId: codeGroup.RouteCode
  });
  const [dosfrmCodes] = useCodes({
    codeGroupId: codeGroup.DosfrmCode
  });
  const [comorbidCodes] = useCodes({
    codeGroupId: codeGroup.ComorbidCode
  });
  const [{ items: tbdrugs }] = useTbdrugList();
  const [tbSusceptCodes] = useCodes({ codeGroupId: codeGroup.TBSusceptCode });
  const [tbDiagnosisCodes] = useCodes({
    codeGroupId: codeGroup.TBDiagnosisCode
  });

  const getCodeName = (codes = [], codeId, otherCodeId, other) => {
    let name = '';
    if (!!codeId) {
      if (codeId === otherCodeId) {
        name = other;
      } else {
        const foundCode = codes.filter(code => code.value === codeId)[0];
        name = foundCode && foundCode.text;
      }
    }

    return name;
  };

  const getDateRange = (start, end) => {
    if (!!start || !!end) {
      return `${start || ''} ~ ${end || ''}`;
    } else {
      return '';
    }
  };

  const {
    age,
    docName,
    initial,
    sexCodeName,
    subjectNo,
    nat2GenRes,
    nat2PhenRes,
    slco1B1GenRes,
    slco1B1PhenRes
  } = subject.item;

  useEffect(
    () => {
      if (subjectSummary.items.hasOwnProperty('body')) {
        const body =
          subjectSummary.items.body[subjectSummary.items.body.length - 1];

        const diagnosis = subjectSummary.items.diagnosis;
        const comorbids = subjectSummary.items.comorbid;
        const medications = subjectSummary.items.sTbd;

        const antiTbDrug = subjectSummary.items.cm;

        setSummary({
          ...summary,
          weight: body.weight,
          height: body.height,
          tbStart:
            diagnosis.length > 0 ? diagnosis[diagnosis.length - 1].tbStart : '',
          diagnosises: diagnosis,
          antiTbDrug: antiTbDrug,
          comorbids: comorbids,
          medications: medications
        });
      }
    },
    [subjectSummary.items]
  );

  const adr = useGetProjectActivityId({
    activityGroupId: activityGroup.ADR,
    subjectId,
    activityId: activity.ADR
  });

  const { items } = useADRForCR({
    subjectId,
    projectActivityId: adr.id
  });

  const adrs = _.orderBy(items, 'adrOnsetDat', 'asc');

  return (
    <Fragment>
      <Table compact="very">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Subject No</Table.HeaderCell>
            <Table.HeaderCell>Doctor</Table.HeaderCell>
            <Table.HeaderCell>Initial</Table.HeaderCell>
            <Table.HeaderCell>Age</Table.HeaderCell>
            <Table.HeaderCell>Gender</Table.HeaderCell>
            <Table.HeaderCell>Treatment Start</Table.HeaderCell>
            <Table.HeaderCell>Weight</Table.HeaderCell>
            <Table.HeaderCell>Height</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell>{subjectNo}</Table.Cell>
            <Table.Cell>{docName}</Table.Cell>
            <Table.Cell>{initial}</Table.Cell>
            <Table.Cell>{age}</Table.Cell>
            <Table.Cell>{sexCodeName}</Table.Cell>
            <Table.Cell>{summary.tbStart}</Table.Cell>
            <Table.Cell>{summary.weight}</Table.Cell>
            <Table.Cell>{summary.height}</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>

      <Table compact="very">
        <Table.Body>
          <Table.Row>
            <Table.Cell>NAT2</Table.Cell>
            <Table.Cell>
              {nat2GenRes} - {nat2PhenRes}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>SLCO1B1</Table.Cell>
            <Table.Cell>
              {slco1B1GenRes} - {slco1B1PhenRes}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>TB Diagnosis</Table.Cell>
            <Table.Cell>
              {summary.diagnosises.map((item, index) => {
                let tracks = [];

                const {
                  subjectTbDiagnosisId,
                  dateDiagnosis,
                  tbSusceptcodeId,
                  drugSuscepOth,
                  tbDiagnosiscodeId,
                  tbOtherSite
                } = item;

                tracks.push(
                  getCodeName(
                    tbSusceptCodes,
                    tbSusceptcodeId,
                    code.TBSusceptOther,
                    drugSuscepOth
                  )
                );
                tracks.push(
                  getCodeName(
                    tbDiagnosisCodes,
                    tbDiagnosiscodeId,
                    code.TBDiagnosisOther,
                    tbOtherSite
                  )
                );

                return (
                  <div key={subjectTbDiagnosisId}>
                    {`${dateDiagnosis} : ${_.reject(tracks, _.isEmpty)}`}
                  </div>
                );
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>Anti TB-Drug</Table.Cell>
            <Table.Cell>
              {summary.antiTbDrug.map((item, index) => {
                let tracks = [];

                tracks.push(item.codrname);
                tracks.push(`${item.cmdose} ${item.cmdosu}`);
                tracks.push(
                  getCodeName(
                    dosfrqCodes,
                    item.cmdosfrqcodeId,
                    code.DosfrqOther,
                    item.cmdosfrqoth
                  )
                );
                tracks.push(
                  getCodeName(
                    routeCodes,
                    item.cmroutecodeId,
                    code.RouteOther,
                    item.cmrouteoth
                  )
                );
                tracks.push(
                  getCodeName(
                    dosfrmCodes,
                    item.cmdosfrmcodeId,
                    code.DosfrmOther,
                    item.cmdosfrmoth
                  )
                );
                tracks.push(getDateRange(item.cmstdtc, item.cmendtc));

                return (
                  <div key={index}>
                    {`${item.cmtrt} : ${_.reject(tracks, _.isEmpty)}`}
                  </div>
                );
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>Concomitant Medication</Table.Cell>
            <Table.Cell>
              {summary.medications.map(item => {
                let tracks = [];

                tracks.push(item.extrt);
                tracks.push(item.exdose ? `${item.exdose} mg` : '');
                tracks.push(
                  getCodeName(
                    dosfrqCodes,
                    item.exdosfrqcodeId,
                    code.DosfrqOther,
                    item.exdosfrqoth
                  )
                );
                tracks.push(
                  getCodeName(
                    dosfrmCodes,
                    item.exdosfrmcodeId,
                    code.DosfrqOther,
                    item.exdosfrmoth
                  )
                );
                tracks.push(item.exrouthinf);
                tracks.push(
                  getCodeName(
                    routeCodes,
                    item.exroutecodeId,
                    code.RouteOther,
                    item.exrouthoth
                  )
                );
                tracks.push(getDateRange(item.exstdtc, item.exendtc));

                return (
                  <div key={item.subjectTbdrugId}>
                    {`${getCodeName(
                      tbdrugs,
                      item.exdrugid,
                      code.TBDrugOther,
                      item.exdrugoth
                    )} : ${_.reject(tracks, _.isEmpty)}`}
                  </div>
                );
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>Comorbidity</Table.Cell>
            <Table.Cell>
              {summary.comorbids.map(item => {
                return (
                  <div key={item.subjectComorbidId}>{`${
                    item.comorbidDate
                  } : ${getCodeName(
                    comorbidCodes,
                    item.comorbidcodeId,
                    code.ComorbidityOther,
                    item.comorbidoth
                  )}`}</div>
                );
              })}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>ADR</Table.Cell>
            <Table.Cell>
              {adrs.length > 0 && (
                <Fragment>
                  {adrs.map(item => {
                    return <div key={item.subjectAdrid}>{item.adrName}</div>;
                  })}
                </Fragment>
              )}
              {adrs.length === 0 && <Fragment>N/A</Fragment>}
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
    </Fragment>
  );
};

export default SubjectInfomation;
