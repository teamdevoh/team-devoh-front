import { useCallback, useEffect, useState } from 'react';
import { useBoolean } from 'react-hanger';
import api from '../api';

const useSubjectsForTDMReport = ({
  projectId,
  projectSiteId = 0,
  subjectId = 0
}) => {
  const [items, setItems] = useState([]);
  const loading = useBoolean(false);

  const fetchItems = useCallback(async ({ projectSiteId, subjectId }) => {
    try {
      loading.setTrue();
      const res = await api.fetchSubjectsForTDMReport({
        projectId,
        projectSiteId,
        subjectId
      });
      if (res && res.data) {
        setItems(res.data);
      }
    } catch (error) {
    } finally {
      loading.setFalse();
    }
  }, []);

  useEffect(
    () => {
      fetchItems({ projectSiteId, subjectId });
    },
    [projectSiteId, subjectId]
  );

  return { items, loading: loading.value };
};

export default useSubjectsForTDMReport;
