const useCSVOpt = () => {
  const get = ({ name }) => {
    return {
      fileName: `${name}_01.csv`,
      fields: [
        'ID',
        'TIME',
        'AMT',
        'II',
        'SS',
        'DV',
        'MDV',
        'EVID',
        'AGE',
        'BWT',
        'HT',
        'GENDER',
        'CMT'
      ],
      fieldNames: [
        'ID',
        'TIME',
        'AMT',
        'II',
        'SS',
        'DV',
        'MDV',
        'EVID',
        'AGE',
        'BWT',
        'HT',
        'GENDER',
        'CMT'
      ]
    };
  };

  return { get };
};

export default useCSVOpt;
