import { useCallback } from 'react';
import api from '../api';

const useFinishedReport = ({ subjectId, activityKeyId, projectActivityId }) => {
  const logging = useCallback(
    async storageMeta => {
      const res = await api.finishedTDMReport({
        subjectId,
        activityKeyId,
        projectActivityId,
        payload: storageMeta
      });
      if (res && res.data) {
        const fileStorageId = res.data;
        return fileStorageId;
      }

      return 0;
    },
    [subjectId, activityKeyId, projectActivityId]
  );

  return { logging };
};

export default useFinishedReport;
