import { useEffect, useState } from 'react';
import { code } from '../../../constants';
import { useSummaryForCR } from '../../case-review/hooks';

const useBuildRawData = ({ subjectId }) => {
  const [subjectSummary] = useSummaryForCR(subjectId);
  const [weight, setWeight] = useState(0);
  const [height, setHeight] = useState(0);

  const buildRawData = ({ age, dose, sexCodeId, tads }) => {
    let tempData = [];

    const rowLength = 24 * 6;
    _.times(rowLength + 1, row => {
      let AMT = '.';
      let II = '.';
      let SS = '.';
      let DV = '.';
      let EVID = '0';

      if (row === 0) {
        AMT = dose;
        II = '24';
        SS = '1';
        EVID = '1';
      }

      tempData.push({
        ID: '1',
        TIME: _.round(row / 6, 3),
        AMT: AMT,
        II,
        SS,
        DV: DV,
        MDV: '1',
        EVID,
        AGE: age,
        BWT: weight,
        HT: height,
        GENDER:
          sexCodeId === code.MALE ? '0' : sexCodeId === code.FEMALE ? '1' : '',
        CMT: 1
      });
    });

    tempData.forEach((item, index) => {
      _.forEach(tads, tad => {
        const next = tempData[index + 1];
        if (Number(item.TIME) === Number(tad.tad)) {
          item.DV = tad.drugConc;
          item.MDV = '0';
        }
        if (
          Number(item.TIME) < Number(tad.tad) &&
          (next === void 0 || Number(next.TIME) > Number(tad.tad))
        ) {
          tempData.splice(index + 1, 0, {
            ID: '1',
            TIME: tad.tad,
            AMT: '.',
            II: '.',
            SS: '.',
            DV: tad.drugConc,
            MDV: '0',
            EVID: '0',
            AGE: age,
            BWT: weight,
            HT: height,
            GENDER:
              sexCodeId === code.MALE
                ? '0'
                : sexCodeId === code.FEMALE
                  ? '1'
                  : '',
            CMT: 1
          });
        }
      });
    });

    return tempData;
  };

  const getRawData = ({ age, dose, sexCodeId, tads, exDrugId }) => {
    const rawData = buildRawData({ age, dose, sexCodeId, tads, exDrugId });
    return rawData;
  };

  useEffect(
    () => {
      if (subjectSummary.items.hasOwnProperty('body')) {
        const body =
          subjectSummary.items.body[subjectSummary.items.body.length - 1];
        setWeight(body.weight);
        setHeight(body.height);
      }
    },
    [subjectId, subjectSummary.items]
  );

  return { getData: getRawData };
};

export default useBuildRawData;
