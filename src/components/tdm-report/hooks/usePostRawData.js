import { useCallback } from 'react';
import { useBoolean } from 'react-hanger';
import api from '../api';
import tbDrugApi from '../../collection-tbdrug/api';
import json2csv from 'json2csv';
import useCSVOpt from './useCSVOpt';

const usePostRawData = ({ subjectId, activityKeyId, projectActivityId }) => {
  const loading = useBoolean(false);
  const csvOpt = useCSVOpt();

  const post = useCallback(async ({ rawDataSet, scriptFile, cb }) => {
    loading.setTrue();
    let files = [];

    files.push(scriptFile);

    const keys = Object.keys(rawDataSet);
    let blobCnt = 0;
    keys.forEach(async (exDrugId, index) => {
      const drug = await tbDrugApi.fetchTbdrug({ tbdrugId: exDrugId });
      const shortName = drug.data.shortName;

      const opt = csvOpt.get({ name: shortName });
      const opts = {
        data: rawDataSet[exDrugId],
        fields: opt.fields,
        fieldNames: opt.fieldNames,
        quotes: '',
        withBOM: true
      };

      const csv = json2csv(opts);

      const blob = new Blob([csv], {
        type: 'text/csv;charset=utf-8;'
      });

      const fileOfBlob = new File([blob], opt.fileName);
      files.push(fileOfBlob);

      blobCnt++;

      const isLast = keys.length === blobCnt;
      if (isLast) {
        const res = await api
          .runReport({
            files: files,
            subjectId,
            activityKeyId,
            projectActivityId
          })
          .catch(err => {
            loading.setFalse();
          })
          .finally(() => {
            loading.setFalse();
          });

        if (res && res.data) {
          const { storage, signedUrl } = res.data.value;

          cb({ storage, signedUrl });
        }
      }
    });
  });

  return { post, loading: loading.value };
};

export default usePostRawData;
