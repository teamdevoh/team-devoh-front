import React, { Fragment, useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import usePostRawData from './hooks/usePostRawData';

const BuildRun = ({
  subjectId,
  subjectNo,
  activityKeyId,
  projectActivityId,
  rawDataSet,
  discussion,
  cb
}) => {
  const postRawData = usePostRawData({
    subjectId,
    activityKeyId,
    projectActivityId
  });

  const handleButtonClick = useCallback(
    async () => {
      const scriptName = 'main.rnw';
      const text = await fetch(`./scripts/${scriptName}`).then(res =>
        res.text()
      );
      const blob = new Blob([text]);
      const scriptFile = new File([blob], `TDMReport_${subjectNo}.rnw`);

      await postRawData.post({
        rawDataSet,
        scriptFile,
        cb: cb
      });
    },
    [rawDataSet, discussion]
  );

  return (
    <Fragment>
      <a className="link" onClick={handleButtonClick}>
        Run
      </a>
      {postRawData.loading && <Icon loading name="spinner" />}
    </Fragment>
  );
};

export default BuildRun;
