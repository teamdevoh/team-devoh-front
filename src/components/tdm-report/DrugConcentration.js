import React, { Fragment, useState } from 'react';
import { useBoolean } from 'react-hanger';
import {
  Icon,
  Tab,
  Header,
  TextArea,
  Grid,
  Segment,
  Button
} from 'semantic-ui-react';
import { activity, activityGroup } from '../../constants';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { useMyDrugs, useTDMConcentrationForCR } from '../case-review/hooks';
import { useGetProjectActivityId } from '../collection/hooks';
import { notification } from '../modal';
import { UploadLayer } from '../modules';
import {
  TimeData,
  TDMReportViewer,
  ScriptBuildTab,
  ScriptBuildLayout,
  PreBuildRun,
  BuildRun,
  CustomBuildRun,
  ReportViewButton,
  DrugConcentrationList
} from './';
import useBuildRawData from './hooks/useBuildRawData';
import useFinishedReport from './hooks/useFinishedReport';

const DrugConcentration = ({
  age,
  sexCodeId,
  subjectId,
  subjectNo,
  visitOrder,
  resultDTC,
  sampleTimes = [],
  subjectTDMReportId
}) => {
  const t = useFormatMessage();
  const isUpload = useBoolean(false);
  const finishedRun = useBoolean(false);
  const [droppedFile, setDroppedFile] = useState({ name: '' });
  const myDrug = useMyDrugs(subjectId);

  const myDrugsByVisit = _.filter(myDrug.items, {
    visitOrder: Number(visitOrder)
  });
  const drugs = _.groupBy(myDrugsByVisit, 'exDrugId');
  const buildRawData = useBuildRawData({ subjectId });
  const [prePdfInfo, setPrePDFInfo] = useState({ name: '' });
  const [pdfInfo, setPDFInfo] = useState({ name: '' });
  const [customPdfInfo, setCustomPDFInfo] = useState({ name: '' });
  const [discussion, setDiscussion] = useState('');
  const [tabIndex, setTabIndex] = useState(0);
  const [reportFileCount, setReportFileCount] = useState(0);

  const tdmFirst = useGetProjectActivityId({
    activityGroupId: activityGroup.FirstTDM,
    subjectId,
    activityId: activity.TB_PK
  });
  const tdmFU = useGetProjectActivityId({
    activityGroupId: activityGroup.FUTDM,
    subjectId,
    activityId: activity.TB_PK
  });
  const tdmRep = useGetProjectActivityId({
    activityGroupId: activityGroup.FirstTDM,
    subjectId,
    activityId: activity.TDMREP
  });
  const tdm = useTDMConcentrationForCR(subjectId, tdmFirst.id, tdmFU.id);

  const finishedReport = useFinishedReport({
    subjectId,
    activityKeyId: subjectTDMReportId,
    projectActivityId: tdmRep.id
  });

  let panes = [];
  let rawDataSet = {};

  const exDrugIds = Object.keys(drugs);

  exDrugIds.forEach(exDrugId => {
    const currentDrug = drugs[exDrugId][0];
    const { exdose, exdosfrqName, exdosfrmName, genericName } = currentDrug;

    let data = [];

    sampleTimes.forEach(sampleTime => {
      const sample = _.find(tdm.items, {
        sampleTime: sampleTime,
        exdrugId: Number(exDrugId)
      });
      if (sample) {
        const minutesDiff = helpers.util.diffMinutes(
          sample.medDatetime,
          sampleTime
        );
        const tad = _.round(minutesDiff / 60, 3);

        data.push({
          subjectVisitId: sample.subjectVisitId,
          sampleTime,
          exdose,
          exdosfrqName,
          exdosfrmName,
          medDatetime: sample.medDatetime,
          tad: tad,
          drugConc: sample.drugConc,
          drugConcUnit: sample.drugConcUnit,
          resultDTC,
          ppAuc: sample.ppAuc,
          ppCmax: sample.ppCmax
        });
      }
    });

    const subjectVisitId = data.length > 0 ? data[0].subjectVisitId : 0;

    rawDataSet[exDrugId] = buildRawData.getData({
      age,
      dose: exdose,
      sexCodeId: Number(sexCodeId),
      tads: data,
      exDrugId
    });

    panes.push({
      menuItem: genericName,
      render: () => (
        <div
          id={`tdmTab_${exDrugId}`}
          style={{
            padding: '10px',
            border: 'solid 1px #d2d2d2',
            overflowY: 'hidden',
            overflowX: 'hidden'
          }}
        >
          <DrugConcentrationList data={data} />
          <Header as="h4">Raw Data</Header>
          <TimeData data={rawDataSet} exDrugId={exDrugId} />
          {subjectVisitId > 0 && (
            <Fragment>
              <Header as="h4">TDM Report</Header>
              <ScriptBuildTab
                onTabChange={(event, data) => {
                  setTabIndex(data.activeIndex);
                }}
                panes={[
                  <ScriptBuildLayout columns={3}>
                    <Fragment>
                      <Grid.Column>
                        <Segment basic style={{ textAlign: 'center' }}>
                          <Grid>
                            <Grid.Column width="sixteen">
                              <Icon name="building outline" size="huge" />
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <Header as="h4">
                                <Header.Content>
                                  Pre-Build{' '}
                                  <PreBuildRun
                                    subjectId={subjectId}
                                    activityKeyId={subjectTDMReportId}
                                    projectActivityId={tdmRep.id}
                                    rawDataSet={rawDataSet}
                                    cb={({ storage, signedUrl }) => {
                                      setPrePDFInfo({
                                        ...storage,
                                        signedUrl: signedUrl
                                      });
                                    }}
                                  />
                                </Header.Content>
                              </Header>
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <ReportViewButton
                                name={prePdfInfo.originalName}
                                url={prePdfInfo.signedUrl}
                              />
                            </Grid.Column>
                          </Grid>
                        </Segment>
                      </Grid.Column>
                      <Grid.Column>
                        <Segment basic style={{ textAlign: 'center' }}>
                          <Grid>
                            <Grid.Column width="sixteen">
                              <Icon name="sticky note outline" size="huge" />
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <Header as="h4">Findings and Discussion</Header>
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <TextArea
                                style={{ width: '100%' }}
                                onBlur={event => {
                                  setDiscussion(event.target.value);
                                }}
                              />
                            </Grid.Column>
                          </Grid>
                        </Segment>
                      </Grid.Column>
                      <Grid.Column>
                        <Segment basic style={{ textAlign: 'center' }}>
                          <Grid>
                            <Grid.Column width="sixteen">
                              <Icon name="building" size="huge" />
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <Header as="h4">
                                <Header.Content>
                                  Build
                                  <BuildRun
                                    subjectId={subjectId}
                                    subjectNo={subjectNo}
                                    activityKeyId={subjectTDMReportId}
                                    projectActivityId={tdmRep.id}
                                    rawDataSet={rawDataSet}
                                    discussion={discussion}
                                    cb={({ storage, signedUrl }) => {
                                      setPDFInfo({
                                        ...storage,
                                        signedUrl: signedUrl
                                      });
                                    }}
                                  />
                                </Header.Content>
                              </Header>
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <ReportViewButton
                                name={pdfInfo.originalName}
                                url={pdfInfo.signedUrl}
                              />
                            </Grid.Column>
                          </Grid>
                        </Segment>
                      </Grid.Column>
                    </Fragment>
                  </ScriptBuildLayout>,
                  <div>
                    <ScriptBuildLayout columns={2}>
                      <Grid.Column>
                        <Segment basic style={{ textAlign: 'center' }}>
                          <Grid>
                            <Grid.Column width="sixteen">
                              <Icon name="upload" size="huge" />
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <Header as="h4">
                                <Header.Content>
                                  Custom Script{' '}
                                  <a
                                    className="link"
                                    onClick={isUpload.setTrue}
                                  >
                                    Upload
                                  </a>
                                </Header.Content>
                              </Header>
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              {droppedFile.name &&
                                droppedFile.name.length > 0 && (
                                  <div>Dropped file {droppedFile.name}</div>
                                )}
                            </Grid.Column>
                          </Grid>
                        </Segment>
                      </Grid.Column>
                      <Grid.Column>
                        <Segment basic style={{ textAlign: 'center' }}>
                          <Grid>
                            <Grid.Column width="sixteen">
                              <Icon name="building outline" size="huge" />
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <Header as="h4">
                                <Header.Content>
                                  Build{' '}
                                  <CustomBuildRun
                                    subjectId={subjectId}
                                    activityKeyId={subjectTDMReportId}
                                    projectActivityId={tdmRep.id}
                                    rawDataSet={rawDataSet}
                                    scriptFile={droppedFile}
                                    cb={({ storage, signedUrl }) => {
                                      setCustomPDFInfo({
                                        ...storage,
                                        signedUrl: signedUrl
                                      });
                                    }}
                                  />
                                </Header.Content>
                              </Header>
                            </Grid.Column>
                            <Grid.Column width="sixteen">
                              <ReportViewButton
                                name={customPdfInfo.originalName}
                                url={customPdfInfo.signedUrl}
                              />
                            </Grid.Column>
                          </Grid>
                        </Segment>
                      </Grid.Column>
                    </ScriptBuildLayout>
                  </div>
                ]}
              />
              <br />
              {reportFileCount === 0 && (
                <Button
                  color="blue"
                  floated="right"
                  disabled={
                    (tabIndex === 0
                      ? pdfInfo.name.length
                      : customPdfInfo.name.length) === 0
                  }
                  onClick={async () => {
                    const key = await finishedReport.logging(
                      tabIndex === 0 ? pdfInfo : customPdfInfo
                    );
                    if (key > 0) {
                      notification.success({
                        title: t('common.alert.changed'),
                        onClose: () => {}
                      });
                    }
                    finishedRun.setTrue();
                  }}
                >
                  Save
                </Button>
              )}
              <br />
              <br />
              <TDMReportViewer
                subjectId={subjectId}
                subjectVisitId={subjectVisitId}
                refresh={finishedRun.value}
                onRefreshed={finishedRun.setFalse}
                onRenderFiles={files => {
                  setReportFileCount(files.length);
                }}
              />
            </Fragment>
          )}
        </div>
      )
    });
  });

  return (
    <Fragment>
      <UploadLayer
        open={isUpload.value}
        onClose={isUpload.setFalse}
        title={'Upload'}
        multiple={false}
        accept={'.rnw'}
        onDrop={acceptedFile => {
          isUpload.setFalse();
          setDroppedFile(acceptedFile[0]);
        }}
      />
      <Header as="h4">TB Drug Concentration</Header>
      <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
    </Fragment>
  );
};

export default DrugConcentration;
