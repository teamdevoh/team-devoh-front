import React from 'react';
import helpers from '../../helpers';
import { DataGrid } from '../data-grid';

const SubjectList = ({ items, loading, onRowClick }) => {
  const columns = [
    {
      key: 'subjectNo',
      name: 'Subject',
      width: 170,
      formatter: ({ value, row }) => `${value}/${row.age}/${row.sex}`
    },
    {
      key: 'visitOrder',
      name: 'Visit',
      width: 50,
      formatter: ({ value, row }) => helpers.util.getOrdinal(value)
    },
    {
      key: 'excombi',
      name: 'Regimen'
    },
    {
      key: 'tdM_REPDTC',
      name: 'Report',
      width: 110
    }
  ];

  return (
    <DataGrid
      rowNumber={{ show: true, key: 'no', name: 'No' }}
      columns={columns}
      rows={items}
      minHeight={700}
      rowKey="subjectId"
      loading={loading}
      onRowClick={onRowClick}
    />
  );
};
export default SubjectList;
