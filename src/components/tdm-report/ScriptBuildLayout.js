import React from 'react';
import { Grid } from 'semantic-ui-react';

const ScriptBuildLayout = ({ columns, children }) => {
  return (
    <Grid columns={columns} divided>
      <Grid.Row stretched>{children}</Grid.Row>
    </Grid>
  );
};

export default ScriptBuildLayout;
