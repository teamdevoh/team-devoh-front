import React, { useEffect, useState } from 'react';
import { Table } from 'semantic-ui-react';
import helpers from '../../helpers';

const SubjectTDMVisit = ({
  subjectId,
  items,
  onRowClick,
  initFirstSelected = false
}) => {
  const [selectedVisit, setSelectedVisit] = useState(0);

  useEffect(
    () => {
      setSelectedVisit(0);
    },
    [subjectId]
  );

  useEffect(
    () => {
      if (initFirstSelected && items.length > 0 && selectedVisit === 0) {
        const item = items[0];
        const sampleTimes = item.sampleTime.split(',');

        setSelectedVisit(item.visitOrder);
        onRowClick({ ...item, sampleTimes });
      }
    },
    [initFirstSelected, items, selectedVisit]
  );

  return (
    <Table compact="very" selectable>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Visit</Table.HeaderCell>
          <Table.HeaderCell>Visit DateTime</Table.HeaderCell>
          <Table.HeaderCell>Regimen</Table.HeaderCell>
          <Table.HeaderCell>Sampling</Table.HeaderCell>
          <Table.HeaderCell>TDM Date</Table.HeaderCell>
          <Table.HeaderCell>Reporting Date</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {items.map((item, index) => {
          const {
            excombi,
            visitOrder,
            visitDateTime,
            sampleTime,
            resultDTC,
            tdM_REPDTC
          } = item;
          const sampleTimes = sampleTime.split(',');
          return (
            <Table.Row
              key={index}
              style={{
                backgroundColor: selectedVisit === visitOrder ? 'yellow' : '',
                cursor: 'pointer'
              }}
              onClick={() => {
                setSelectedVisit(visitOrder);
                onRowClick({ ...item, sampleTimes });
              }}
            >
              <Table.Cell>{helpers.util.getOrdinal(visitOrder)}</Table.Cell>
              <Table.Cell>{visitDateTime}</Table.Cell>
              <Table.Cell>{excombi}</Table.Cell>
              <Table.Cell>
                {sampleTimes.map(value => {
                  return <div key={value}>{value}</div>;
                })}
              </Table.Cell>
              <Table.Cell>{resultDTC}</Table.Cell>
              <Table.Cell>{tdM_REPDTC}</Table.Cell>
            </Table.Row>
          );
        })}
      </Table.Body>
    </Table>
  );
};

export default SubjectTDMVisit;
