import React, { Fragment, useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import usePostRawData from './hooks/usePostRawData';

const CustomBuildRun = ({
  subjectId,
  activityKeyId,
  projectActivityId,
  rawDataSet,
  scriptFile,
  cb
}) => {
  const postRawData = usePostRawData({
    subjectId,
    activityKeyId,
    projectActivityId
  });

  const handleButtonClick = useCallback(
    async () => {
      if (scriptFile.size > 0) {
        await postRawData.post({
          rawDataSet,
          scriptFile,
          cb: cb
        });
      }
    },
    [rawDataSet, scriptFile]
  );

  return (
    <Fragment>
      <a className="link" onClick={handleButtonClick}>
        Run
      </a>
      {postRawData.loading && <Icon loading name="spinner" />}
    </Fragment>
  );
};

export default CustomBuildRun;
