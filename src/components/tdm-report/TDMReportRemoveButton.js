import React, { Fragment, useCallback } from 'react';
import { useBoolean } from 'react-hanger';
import { Button } from 'semantic-ui-react';
import { useAttachRemove } from '../activity-file/hooks';
import { Confirm } from '../modal';

const TDMReportRemoveButton = ({
  subjectId,
  activityKeyId,
  activityAttachmentId,
  fileName,
  onRemoved
}) => {
  const showRemoveConfirm = useBoolean(false);
  const attachRemove = useAttachRemove({ subjectId, activityKeyId });

  const handleRemove = useCallback(
    async () => {
      const result = await attachRemove.remove(activityAttachmentId);
      if (result) {
        onRemoved(activityAttachmentId);
      }
    },
    [subjectId, activityKeyId, activityAttachmentId, onRemoved]
  );

  return (
    <Fragment>
      <Button
        color="red"
        loading={attachRemove.loading}
        size="mini"
        onClick={showRemoveConfirm.setTrue}
      >
        Remove
      </Button>
      <Confirm
        title={'Remove TDM Report'}
        content={`Are you sure you want to delete ${fileName}?`}
        open={showRemoveConfirm.value}
        onOK={() => {
          handleRemove();
          showRemoveConfirm.setFalse();
        }}
        onCancel={showRemoveConfirm.setFalse}
      />
    </Fragment>
  );
};

export default TDMReportRemoveButton;
