import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {
  Checkbox,
  Container,
  Dropdown,
  Form,
  Grid,
  Header,
  Segment
} from 'semantic-ui-react';
import { ProjectSite } from '../collection/Selector';
import useSubjectsForTDMReport from './hooks/useSubjectsForTDMReport';
import {
  DrugConcentration,
  SubjectList,
  SubjectInfomation,
  SubjectTDMVisit
} from './';

const DashBoardContainer = () => {
  const { projectId } = useParams();
  const subject = useSubjectsForTDMReport({ projectId });
  const [selectedSubject, setSelectedSubject] = useState({
    subjectId: 0,
    subjectNo: ''
  });
  const [selectedVisit, setSelectedVisit] = useState({
    visitOrder: 0,
    resultDTC: '',
    sampleTimes: []
  });
  const [visitsOfSubject, setVisitsOfSubject] = useState([]);
  const [filter, setFilter] = useState({
    projectSiteId: 0,
    subjectId: 0,
    isRequireReporting: false,
    onlyIRPE: false
  });

  const handleFilterChange = useCallback(
    (event, data) => {
      setFilter({ ...filter, [data.name]: data.value });
    },
    [filter]
  );

  let options = [{ key: 0, value: 0, text: 'All' }];
  subject.items.forEach(subject => {
    if (
      (filter.projectSiteId === 0 ||
        subject.projectSiteId === filter.projectSiteId.toString()) &&
      _.findIndex(options, { key: subject.subjectId }) === -1
    ) {
      options.push({
        key: subject.subjectId,
        value: subject.subjectId,
        text: `${subject.subjectNo}/${subject.age}/${subject.sex}`
      });
    }
  });

  useEffect(
    () => {
      options = [];
      setFilter({ ...filter, subjectId: 0 });
    },
    [filter.projectSiteId]
  );

  const condition = ({ projectSiteId, subjectId, tdM_REPDTC, excombi }) => {
    return (
      (filter.projectSiteId === 0 ||
        projectSiteId === filter.projectSiteId.toString()) &&
      (filter.subjectId === 0 || subjectId === filter.subjectId) &&
      (filter.isRequireReporting === false || tdM_REPDTC === '') &&
      (filter.onlyIRPE === false || excombi === 'INH/RIF/PZA/EMB')
    );
  };

  const filteredSubjects = _.filter(subject.items, item => {
    return condition({ ...item });
  });

  const handleSubjectRowClick = useCallback(
    (idx, row) => {
      const foundItems = _.filter(subject.items, { subjectId: row.subjectId });

      setSelectedSubject({
        subjectId: row.subjectId,
        subjectNo: row.subjectNo
      });
      setVisitsOfSubject(foundItems);
      setSelectedVisit({
        visitOrder: 0,
        resultDTC: '',
        sampleTimes: []
      });
    },
    [subject.items]
  );

  const hasVisitSelected = selectedVisit.visitOrder > 0 ? true : false;

  return (
    <Grid>
      <Grid.Column width="six">
        <Container>
          <Header>Target Subjects</Header>
          <Form widths="equal">
            <Form.Group>
              <Form.Field>
                <label>Site</label>
                <ProjectSite
                  projectId={projectId}
                  name="projectSiteId"
                  value={filter.projectSiteId}
                  onChange={handleFilterChange}
                />
              </Form.Field>
              <Form.Field>
                <label>Subject</label>
                <Dropdown
                  search
                  selection
                  name="subjectId"
                  value={filter.subjectId}
                  options={options}
                  onChange={handleFilterChange}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group>
              <Form.Field>
                <Checkbox
                  name="isRequireReporting"
                  label="Require Reporting"
                  onChange={(event, data) => {
                    handleFilterChange(event, {
                      name: data.name,
                      value: data.checked
                    });
                  }}
                />
              </Form.Field>
              <Form.Field>
                <Checkbox
                  name="onlyIRPE"
                  label="INF/RIF/PZA/EMB"
                  onChange={(event, data) => {
                    handleFilterChange(event, {
                      name: data.name,
                      value: data.checked
                    });
                  }}
                />
              </Form.Field>
            </Form.Group>
          </Form>
          <SubjectList
            items={filteredSubjects}
            loading={subject.loading}
            onRowClick={handleSubjectRowClick}
          />
        </Container>
      </Grid.Column>
      <Grid.Column width="ten">
        <Header>TDM Report</Header>
        <Segment style={{ height: '800px', overflow: 'auto' }}>
          {selectedSubject.subjectId > 0 && (
            <Fragment>
              <Header as="h4">Subject Information</Header>
              <SubjectInfomation subjectId={selectedSubject.subjectId} />
              <Header as="h4">Select Of TDM Visit</Header>
              <SubjectTDMVisit
                subjectId={selectedSubject.subjectId}
                items={visitsOfSubject}
                onRowClick={item => {
                  setSelectedVisit({ ...item });
                }}
              />
              {hasVisitSelected && (
                <DrugConcentration
                  subjectId={selectedSubject.subjectId}
                  subjectNo={selectedSubject.subjectNo}
                  age={
                    visitsOfSubject.length > 0 ? visitsOfSubject[0].age : '0'
                  }
                  sexCodeId={
                    visitsOfSubject.length > 0
                      ? visitsOfSubject[0].sexCodeId
                      : ''
                  }
                  {...selectedVisit}
                />
              )}
            </Fragment>
          )}
        </Segment>
      </Grid.Column>
    </Grid>
  );
};

export default DashBoardContainer;
