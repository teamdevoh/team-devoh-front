import React, { useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import MaskedInput, { conformToMask } from 'react-text-mask';
import replace from 'lodash/replace';

const getObject = event => {
  return {
    name: event.target.name,
    value: replace(event.target.value, /\(/g, '')
      .replace(/\)/g, '')
      .replace(/\s/g, '')
      .replace(/-/g, '')
  };
};

const getMask = number => {
  if (number && number.substr(0, 2) === '02') {
    if (number.length > 9) {
      return [
        '(',
        /[0-9]/,
        /[0-9]/,
        ')',
        ' ',
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        '-',
        /\d/,
        /\d/,
        /\d/,
        /\d/
      ];
    } else {
      return [
        '(',
        /[0-9]/,
        /[0-9]/,
        ')',
        ' ',
        /\d/,
        /\d/,
        /\d/,
        '-',
        /\d/,
        /\d/,
        /\d/,
        /\d/
      ];
    }
  } else {
    if (number.length > 10) {
      return [
        '(',
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        ')',
        ' ',
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        '-',
        /\d/,
        /\d/,
        /\d/,
        /\d/
      ];
    } else {
      return [
        '(',
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        ')',
        ' ',
        /\d/,
        /\d/,
        /\d/,
        '-',
        /\d/,
        /\d/,
        /\d/,
        /\d/
      ];
    }
  }
};

const getNumber = value => {
  let number = '';
  if (value) {
    const numbers = value.match(/\d/g);
    if (numbers) {
      number = numbers.join('');
    }
  }
  return number;
};

const PhoneInput = ({ name, placeholder, value, onBlur, onChange }) => {
  let number = getNumber(value);
  const mask = getMask(number);
  const conformed = conformToMask(value, mask, { guide: false });

  const handleChange = useCallback(event => {
    event.preventDefault();
    onChange(event, getObject(event));
  });

  return (
    <div className="ui left icon input">
      <MaskedInput
        name={name}
        mask={value => {
          let number = getNumber(value);
          const mask = getMask(number);
          return mask;
        }}
        placeholder={placeholder}
        guide={false}
        onChange={handleChange}
        value={conformed.conformedValue}
      />
      <Icon name="phone" />
    </div>
  );
};

export default PhoneInput;
