import React, { useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import MaskedInput, { conformToMask } from 'react-text-mask';
import replace from 'lodash/replace';

const getObject = event => {
  return {
    name: event.target.name,
    value: replace(event.target.value, /\(/g, '')
      .replace(/\)/g, '')
      .replace(/\s/g, '')
      .replace(/-/g, '')
  };
};

const getMask = numberLength => {
  if (numberLength > 10) {
    return [
      '(',
      /[0]/,
      /[1]/,
      /[{0,1,6,7,8,9}]/,
      ')',
      ' ',
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      /\d/,
      /\d/
    ];
  } else {
    return [
      '(',
      /[0]/,
      /[1]/,
      /[{0,1,6,7,8,9}]/,
      ')',
      ' ',
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      /\d/,
      /\d/
    ];
  }
};

const getNumberLength = value => {
  let numberLength = 0;
  if (value) {
    const numbers = value.match(/\d/g);
    if (numbers) {
      numberLength = numbers.join('').length;
    }
  }
  return numberLength;
};

const MobileInput = ({ name, placeholder, value, onBlur, onChange }) => {
  let numberLength = getNumberLength(value || '');
  const mask = getMask(numberLength);
  const conformed = conformToMask(value || '', mask, { guide: false });

  const handleChange = useCallback(event => {
    event.preventDefault();
    onChange(event, getObject(event));
  });

  return (
    <div className="ui left icon input">
      <MaskedInput
        name={name}
        mask={value => {
          let numberLength = getNumberLength(value);
          const mask = getMask(numberLength);
          return mask;
        }}
        placeholder={placeholder}
        guide={false}
        onChange={handleChange}
        value={conformed.conformedValue}
      />
      <Icon name="mobile alternate" />
    </div>
  );
};

export default MobileInput;
