import React, { useCallback } from 'react';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

const getObject = event => {
  const numbers = event.target.value.match(/\d/g);
  let number = event.target.value;
  if (numbers) {
    number = numbers.join('');
  }
  return {
    name: event.target.name,
    value: number
  };
};

const numberMask = createNumberMask({
  prefix: '',
  suffix: '',
  integerLimit: 3
});

const IntegerInput = ({ name, placeholder, value, onChange, label }) => {
  const handleChange = useCallback(event => {
    event.preventDefault();
    onChange(event, getObject(event));
  });

  return (
    <div className="ui left labeled input">
      <div className="ui basic label">{label || '#'}</div>
      <MaskedInput
        name={name}
        mask={numberMask}
        data-input
        placeholder={placeholder}
        guide={false}
        onChange={handleChange}
        value={value}
      />
    </div>
  );
};

export default IntegerInput;
