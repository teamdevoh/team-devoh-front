import React, { useRef, useCallback, useEffect } from 'react';
import { Input } from 'semantic-ui-react';
import { activity } from '../../constants';
import { useVerifyRange } from '../../hooks';

const WarningMinMaxInput = ({ name, value, min, max, unit, onChange }) => {
  const inputRef = useRef();
  const [level] = useVerifyRange({ value, min, max });

  const handleChange = useCallback(
    (event, data) => {
      onChange(event, data);
    },
    [onChange]
  );

  useEffect(
    () => {
      if (level) {
        const input = inputRef.current.inputRef.current;
        input.style.backgroundColor = activity.LabRanges[level].color.hex;
      }
    },
    [level]
  );

  return (
    <Input
      name={name}
      ref={inputRef}
      label={{ basic: true, content: unit }}
      labelPosition="right"
      placeholder=""
      value={value === null ? '' : value}
      onChange={handleChange}
    />
  );
};

export default WarningMinMaxInput;
