import React, { useState } from 'react';
import DateInput from './DateInput';
import { Checkbox, Input, Form, Message, Dropdown } from 'semantic-ui-react';

const DateInputTestContainer = () => {
  const locale = window.sessionStorage.getItem('locale');

  const defalutValue =
    locale === 'en' ? '10/05/2019 23:20' : '2019-05-10 23:20';
  const defaultSeparate = locale === 'en' ? '/' : '-';
  const defalutMin = locale === 'en' ? '01/05/2019 13:50' : '2019-05-01 13:50';
  const defalutMax = locale === 'en' ? '01/09/2019 13:50' : '2019-09-01 13:50';
  const defalutDateFormat =
    locale === 'en' ? 'dd/MM/yyyy HH:mm' : 'yyyy-MM-dd HH:mm';

  const [value, setValue] = useState(defalutValue);
  const [separate] = useState(defaultSeparate);
  const [min, setMin] = useState(defalutMin);
  const [max, setMax] = useState(defalutMax);
  const [dateFormat, setDateFormat] = useState(defalutDateFormat);
  const [isFailed, setIsFailed] = useState(false);

  let options = [
    { text: 'yyyy-MM-dd HH:mm', value: 'yyyy-MM-dd HH:mm' },
    { text: 'yyyy-MM-dd HH:uk', value: 'yyyy-MM-dd HH:uk' },
    { text: 'yyyy-MM-dd uk:uk', value: 'yyyy-MM-dd uk:uk' },
    { text: 'yyyy-MM-uk uk:uk', value: 'yyyy-MM-uk uk:uk' },
    { text: 'yyyy-uk-uk uk:uk', value: 'yyyy-uk-uk uk:uk' },
    { text: 'ukuk-uk-uk uk:uk', value: 'ukuk-uk-uk uk:uk' },
    { text: 'yyyy-MM-dd', value: 'yyyy-MM-dd' },
    { text: 'yyyy-MM-uk', value: 'yyyy-MM-uk' },
    { text: 'yyyy-uk-uk', value: 'yyyy-uk-uk' },
    { text: 'ukuk-uk-uk', value: 'ukuk-uk-uk' },
    { text: 'yyyy-MM', value: 'yyyy-MM' },
    { text: 'yyyy-uk', value: 'yyyy-uk' },
    { text: 'ukuk-uk', value: 'ukuk-uk' },
    { text: 'HH:mm', value: 'HH:mm' },
    { text: 'HH:uk', value: 'HH:uk' },
    { text: 'uk:uk', value: 'uk:uk' }
  ];

  if (locale === 'en') {
    options = [
      { text: 'dd/MM/yyyy HH:mm', value: 'dd/MM/yyyy HH:mm' },
      { text: 'dd/MM/yyyy HH:uk', value: 'dd/MM/yyyy HH:uk' },
      { text: 'dd/MM/yyyy uk:uk', value: 'dd/MM/yyyy uk:uk' },
      { text: 'uk/MM/yyyy uk:uk', value: 'uk/MM/yyyy uk:uk' },
      { text: 'uk/uk/yyyy uk:uk', value: 'uk/uk/yyyy uk:uk' },
      { text: 'uk/uk/ukuk uk:uk', value: 'uk/uk/ukuk uk:uk' },
      { text: 'dd/MM/yyyy', value: 'dd/MM/yyyy' },
      { text: 'uk/MM/yyyy', value: 'uk/MM/yyyy' },
      { text: 'uk/uk/yyyy', value: 'uk/uk/yyyy' },
      { text: 'uk/uk/ukuk', value: 'uk/uk/ukuk' },
      { text: 'MM/yyyy', value: 'MM/yyyy' },
      { text: 'uk/yyyy', value: 'uk/yyyy' },
      { text: 'uk/ukuk', value: 'uk/ukuk' },
      { text: 'HH:mm', value: 'HH:mm' },
      { text: 'HH:uk', value: 'HH:uk' },
      { text: 'uk:uk', value: 'uk:uk' }
    ];
  }

  return (
    <Form>
      <Form.Group widths="equal">
        <Form.Field>
          <label>
            Format Current:
            {locale}
          </label>
          <Checkbox
            label="Is En?"
            checked={locale === 'en' ? true : false}
            onChange={(event, data) => {
              window.sessionStorage.setItem(
                'locale',
                data.checked ? 'en' : 'ko'
              );
              window.location.reload();
            }}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field>
          <label>Separate</label>
          <label>{separate}</label>
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>min</label>
          <Input
            name="min"
            value={min}
            onChange={event => {
              setMin(event.target.value);
            }}
          />
        </Form.Field>
        <Form.Field>
          <label>max</label>
          <Input
            name="max"
            value={max}
            onChange={event => {
              setMax(event.target.value);
            }}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field>
          <label>Date Format</label>
          <Dropdown
            options={options}
            value={dateFormat}
            onChange={(event, data) => {
              setDateFormat(data.value);
            }}
          />
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field>
          <DateInput
            name="dateinput"
            placeholder=""
            value={value}
            options={{
              min: min,
              max: max,
              dateFormat: dateFormat,
              separate: separate
            }}
            onChange={(event, data) => {
              setValue(data.value);
              setIsFailed(data.failed);
            }}
          />
        </Form.Field>
      </Form.Group>
      {isFailed && (
        <Message negative>
          <Message.Header>Invalid</Message.Header>
          <p>Min Max Error</p>
        </Message>
      )}
    </Form>
  );
};

export default DateInputTestContainer;
