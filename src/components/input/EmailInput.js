import React, { useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import MaskedInput from 'react-text-mask';
import emailMask from 'text-mask-addons/dist/emailMask';
import replace from 'lodash/replace';

const getObject = event => {
  return {
    name: event.target.name,
    value: replace(event.target.value, /\(/g, '')
      .replace(/\)/g, '')
      .replace(/\s/g, '')
  };
};
const EmailInput = ({ name, placeholder, value, onBlur, onChange }) => {
  const handleChange = useCallback(event => {
    event.preventDefault();
    onChange(event, getObject(event));
  });

  return (
    <div className="ui left icon input">
      <MaskedInput
        name={name}
        mask={emailMask}
        placeholder={placeholder}
        guide={false}
        // onBlur={event => {
        //   event.preventDefault();
        //   onChange(event, getObject(event));
        // }}
        onChange={handleChange}
        value={value}
      />
      <Icon name="mail" />
    </div>
  );
};

export default EmailInput;
