import React, { useState, useCallback, memo } from 'react';
import { Input } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';

const SearchInput = ({ onClick, value }) => {
  const t = useFormatMessage();
  const [inputValue, setInputValue] = useState(value || '');

  const handleChange = useCallback(
    (event, data) => {
      setInputValue(data.value);
    },
    [inputValue]
  );

  const handleClick = useCallback(
    () => {
      onClick(inputValue);
    },
    [inputValue]
  );

  const handleKeyPress = useCallback(e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      onClick(e.target.value);
    }
  }, []);

  return (
    <Input
      fluid
      placeholder={t('common.search')}
      value={inputValue}
      onChange={handleChange}
      onKeyPress={handleKeyPress}
      action={{
        content: t('common.search'),
        onClick: handleClick,
        disabled: inputValue && inputValue.length > 0 ? false : true
      }}
    />
  );
};

export default memo(SearchInput);
