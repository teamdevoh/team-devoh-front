import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Search } from 'semantic-ui-react';
import './SearchContainer.css';
import escapeRegExp from 'lodash/escapeRegExp';
import filter from 'lodash/filter';

const propTypes = {
  source: PropTypes.array.isRequired,
  onResultSelect: PropTypes.func
};

const defaultProps = {};

class SearchContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      results: [],
      value: ''
    };

    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  handleSearchChange(e, { value }) {
    this.setState({ isLoading: true, value });

    setTimeout(() => {
      const re = new RegExp(escapeRegExp(this.state.value), 'i');
      const isMatch = result => re.test(result.title);

      this.setState({
        isLoading: false,
        results: filter(this.props.source, isMatch)
      });
    }, 200);
  }

  render() {
    const { isLoading, value, results } = this.state;

    return (
      <Search
        fluid={true}
        loading={isLoading}
        onResultSelect={this.props.onResultSelect}
        onSearchChange={this.handleSearchChange}
        results={results}
        value={value}
      />
    );
  }
}

SearchContainer.propTypes = propTypes;
SearchContainer.defaultProps = defaultProps;

export default SearchContainer;
