import React, { useCallback } from 'react';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

const getObject = event => {
  return {
    name: event.target.name,
    value: event.target.value.replace(/[^\\.0-9]/g, '')
  };
};

const DecimalInput = ({
  name,
  placeholder,
  value,
  onChange,
  options,
  label,
  hideLabel,
  disabled = false
}) => {
  const maskOption = {
    prefix: '',
    suffix: '',
    decimalSymbol: '.',
    decimalLimit: 1,
    allowDecimal: true,
    integerLimit: 3,
    ...options
  };
  const numberMask = createNumberMask({ ...maskOption, suffix: '' });

  const handleChange = useCallback(event => {
    event.preventDefault();
    onChange(event, getObject(event));
  });

  return (
    <div className={`ui ${maskOption.suffix ? 'right' : 'left'} labeled input`}>
      {hideLabel !== true && (
        <div className="ui basic label">{label || '#'}</div>
      )}
      <MaskedInput
        name={name}
        mask={numberMask}
        data-input
        placeholder={placeholder}
        guide={false}
        onChange={handleChange}
        value={value}
        disabled={disabled}
      />
      {maskOption.suffix && (
        <div className="ui teal label">{maskOption.suffix}</div>
      )}
    </div>
  );
};

export default DecimalInput;
