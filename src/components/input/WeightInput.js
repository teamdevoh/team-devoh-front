import React, { useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import replace from 'lodash/replace';

const getObject = event => {
  return {
    name: event.target.name,
    value: replace(event.target.value, /\(/g, '')
      .replace(/\)/g, '')
      .replace(/\s/g, '')
      .replace(/-/g, '')
      .replace(/hr/g, '')
  };
};

const numberMask = createNumberMask({
  prefix: '',
  suffix: ' hr',
  includeThousandsSeparator: false,
  allowDecimal: true,
  decimalLimit: 1,
  decimalSymbol: '.'
});

const WeightInput = ({ name, placeholder, value, onBlur, onChange }) => {
  const handleChange = useCallback(event => {
    event.preventDefault();
    onChange(event, getObject(event));
  });

  return (
    <div className="ui left icon input">
      <MaskedInput
        name={name}
        mask={numberMask}
        placeholder={placeholder}
        guide={false}
        // onBlur={event => {
        //   event.preventDefault();
        //   onChange(event, getObject(event));
        // }}
        onChange={handleChange}
        value={value}
      />
      <Icon name="hourglass half" />
    </div>
  );
};

export default WeightInput;
