import React from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import IntegerInput from './IntegerInput';
import './styles.css';
import { useCallback } from 'react';
import DecimalInput from './DecimalInput';

const SliderInput = ({
  label,
  type,
  name,
  value,
  min,
  max,
  marks = {},
  onChange
}) => {
  const handleChange = useCallback(
    value => {
      onChange(null, { name, value });
    },
    [onChange]
  );

  const handleInputChange = useCallback(
    (event, data) => {
      onChange(null, { name: data.name, value: data.value });
    },
    [onChange]
  );

  const minValue = min || 0;
  const maxValue = max || 100;

  return (
    <div className="slider-input">
      <div className="slider">
        <Slider
          name={name}
          min={minValue}
          max={maxValue}
          value={value}
          onChange={handleChange}
          marks={marks}
        />
      </div>
      <div className="number-input">
        {type === 'decimal' && (
          <DecimalInput
            name={name}
            label={label}
            value={value}
            onChange={handleInputChange}
          />
        )}
        {type === 'number' && (
          <IntegerInput
            name={name}
            label={label}
            value={value}
            onChange={handleInputChange}
          />
        )}
      </div>
    </div>
  );
};

export default SliderInput;
