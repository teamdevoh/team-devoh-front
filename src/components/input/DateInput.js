import React, { useCallback } from 'react';
import { Icon, Input, Label } from 'semantic-ui-react';
import MaskedInput from 'react-text-mask';
import moment from 'moment';
import styled from 'styled-components';
import reverse from 'lodash/reverse';
import { format } from '../../constants';

const LabelButton = styled(Label)`
  cursor: pointer;
`;

const TodayButton = styled(Icon)`
  &&& {
    margin-right: 0em;
  }
`;

const DateInput = ({
  name,
  placeholder,
  value = '',
  onChange,
  options = {
    min: '',
    max: '',
    dateFormat: ''
  },
  rightLabel,
  onKeyUp = f => f
}) => {
  const isValid = useCallback(
    (value, minValue, maxValue, f) => {
      const dateObj = makeDateObj(value);
      const minDateObj = makeDateObj(minValue);
      const maxDateObj = makeDateObj(maxValue);

      if (dateObj.minute === 'uk' || !dateObj.minute) {
        dateObj.minute = '00';
        minDateObj.minute = '00';
        maxDateObj.minute = '59';
      }

      if (dateObj.hour === 'uk' || !dateObj.hour) {
        dateObj.hour = '00';
        minDateObj.hour = '00';
        maxDateObj.hour = '23';
      }

      if (dateObj.day === 'uk' || !dateObj.day) {
        dateObj.day = '01';
        minDateObj.day = '01';
        maxDateObj.day = '31';
      }

      if (dateObj.month === 'uk') {
        dateObj.month = '01';
        minDateObj.month = '01';
        maxDateObj.month = '12';
      }

      let firstYear = dateObj.year.substr(0, 2);
      let lastYear = dateObj.year.substr(2, 2);
      if (lastYear === 'uk') {
        lastYear = '00';
        minDateObj.year = '1900';
        maxDateObj.year = '9999';
      }

      if (firstYear === 'uk') {
        firstYear = '19';
        minDateObj.year = '1900';
        maxDateObj.year = '9999';
      }

      dateObj.year = firstYear.concat(lastYear);

      const dateFormat =
        f === 'yyyy-MM-dd'
          ? format.YYYY_MM_DD
          : f === format.HHMM
            ? format.HHMM
            : format.YYYY_MM_DD_HHMM;
      const targetValue = moment(
        `${dateObj.year}-${dateObj.month}-${dateObj.day} ${dateObj.hour}:${
          dateObj.minute
        }`,
        dateFormat
      );

      minDateObj.year = minDateObj.year ? minDateObj.year : '1900';
      minDateObj.month = minDateObj.month ? minDateObj.month : '01';
      minDateObj.day = minDateObj.day ? minDateObj.day : '01';
      minDateObj.hour = minDateObj.hour ? minDateObj.hour : '00';
      minDateObj.minute = minDateObj.minute ? minDateObj.minute : '00';
      const minDate = moment(
        `${minDateObj.year}-${minDateObj.month}-${minDateObj.day} ${
          minDateObj.hour
        }:${minDateObj.minute}`,
        dateFormat
      );

      maxDateObj.year = maxDateObj.year ? maxDateObj.year : '9999';
      maxDateObj.month = maxDateObj.month ? maxDateObj.month : '12';
      maxDateObj.day = maxDateObj.day
        ? maxDateObj.day
        : moment(maxDateObj.month).daysInMonth();
      maxDateObj.hour = maxDateObj.hour ? maxDateObj.hour : '23';
      maxDateObj.minute = maxDateObj.minute ? maxDateObj.minute : '59';

      const maxDate = moment(
        `${maxDateObj.year}-${maxDateObj.month}-${maxDateObj.day} ${
          maxDateObj.hour
        }:${maxDateObj.minute}`,
        dateFormat
      );

      if (targetValue.isValid()) {
        const target = Number(targetValue.format(format.YYYYMMDDHHMM));
        const min = Number(minDate.format(format.YYYYMMDDHHMM));
        const max = Number(maxDate.format(format.YYYYMMDDHHMM));
        return min <= target && target <= max;
      }

      return false;
    },
    [options]
  );

  const getMask = useCallback(
    () => {
      let mask = [];
      for (let i = 0; i < options.dateFormat.length; i++) {
        const char = options.dateFormat[i];
        if (char === options.separate || char === ':' || char === ' ') {
          mask.push(char);
        } else {
          const pattern = `[{0-9${
            char === 'u' || char === 'k' ? ',' + char : ''
          }}]`;
          mask.push(new RegExp(pattern));
        }
      }

      return mask;
    },
    [options.dateFormat, options.separate]
  );

  const splitFormat = useCallback(format => {
    let date = '';
    let time = '';
    format = format || '';

    if (format.indexOf(' ') > -1) {
      const dateTime = format.split(' ');
      date = dateTime[0];
      time = dateTime[1];
    } else {
      if (format.indexOf(options.separate) > -1) {
        date = format;
      }
      if (format.indexOf(':') > -1) {
        time = format;
      }
    }
    return {
      date: date,
      time: time
    };
  }, []);

  const makeDateObj = useCallback(value => {
    let year = '';
    let month = '';
    let day = '';
    let hour = '';
    let minute = '';

    const obj = splitFormat(value, options.separate);
    if (obj.date.indexOf(options.separate) > -1) {
      let dates = obj.date.split(options.separate);
      if (dates[0].length < 4) {
        dates = reverse(dates);
      }

      year = dates[0];
      month = dates[1];
      day = dates[2];
    }

    if (obj.time) {
      const times = obj.time.split(':');
      hour = times[0];
      minute = times[1];
    }

    return {
      year,
      month,
      day,
      hour,
      minute
    };
  }, []);

  const getValue = useCallback((value, dateFormat) => {
    return ConvertDate({
      dateFormat,
      ...makeDateObj(value)
    });
  }, []);

  const ConvertDate = useCallback(
    ({ dateFormat, year, month, day, hour, minute }) => {
      const obj = splitFormat(dateFormat, options.separate);

      let result = '';
      let isReversed = false;

      if (obj.date !== '') {
        let formates = obj.date.split(options.separate);
        if (formates[0].length < 4) {
          isReversed = true;
          formates = reverse(formates);
        }

        if (formates[0]) {
          formates[0] = year;
        }
        if (formates[1]) {
          formates[1] = month;
        }
        if (formates[2]) {
          formates[2] = day;
        }

        if (isReversed) {
          formates = reverse(formates);
        }

        for (let i = 0; i < formates.length; i++) {
          const format = formates[i];
          result += format + options.separate;
        }

        result = result.substr(0, result.length - 1);
      }

      if (obj.time !== '') {
        let times = obj.time.split(':');
        times[0] = times[0] ? hour : null;
        times[1] = times[1] ? minute : null;

        result += result.length > 0 ? ' ' : result;
        result += `${times[0]}:${times[1]}`;
      }

      return result;
    },
    []
  );

  const handleChange = useCallback(
    event => {
      event.preventDefault();
      const isBetween = isValid(
        event.target.value,
        options.min,
        options.max,
        options.dateFormat
      );
      onChange(event, {
        name: name,
        value: event.target.value,
        failed: !isBetween
      });
    },
    [onChange]
  );

  // useEffect(
  //   () => {
  //     const isBetween = isValid(
  //       value,
  //       options.min,
  //       options.max,
  //       options.dateFormat
  //     );
  //     onChange(
  //       {},
  //       {
  //         name: name,
  //         value: value,
  //         failed: !isBetween
  //       }
  //     );
  //   },
  //   [options.min, options.max, value]
  // );

  const handleTodayButtonClick = useCallback(
    event => {
      const today = moment(new Date()).format(
        options.separate === '/'
          ? format.DD_MM_YYYY_HHMM
          : format.YYYY_MM_DD_HHMM
      );
      const isBetween = isValid(
        today,
        options.min,
        options.max,
        options.dateFormat
      );
      onChange(event, {
        name: name,
        value: today.substr(0, options.dateFormat.length),
        failed: !isBetween
      });
    },
    [options]
  );

  const handleTimesClick = useCallback(
    event => {
      onChange(event, {
        name: name,
        value: '',
        failed: true
      });
    },
    [onChange]
  );

  return (
    <Input labelPosition="right">
      <LabelButton basic onClick={handleTodayButtonClick}>
        <TodayButton color="green" name="calendar check outline" />
      </LabelButton>
      <MaskedInput
        name={name}
        mask={getMask(options.dateFormat)}
        keepCharPositions={true}
        guide={true}
        placeholder={placeholder}
        onChange={handleChange}
        value={getValue(value, options.dateFormat)}
        onKeyUp={onKeyUp}
      />
      {!!rightLabel ? (
        rightLabel
      ) : (
        <LabelButton onClick={handleTimesClick}>X</LabelButton>
      )}
    </Input>
  );
};

export default DateInput;
