import React, { useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

const getObject = event => {
  const numbers = event.target.value.match(/\d/g);
  let number = event.target.value;
  if (numbers) {
    number = numbers.join('');
  }
  return {
    name: event.target.name,
    value: number
  };
};

const numberMask = createNumberMask({
  prefix: '',
  suffix: '',
  integerLimit: 3
});

const PercentageInput = ({ name, placeholder, value, onBlur, onChange }) => {
  const handleChange = useCallback(event => {
    event.preventDefault();
    onChange(event, getObject(event));
  });

  return (
    <div className="ui left icon input">
      <MaskedInput
        name={name}
        mask={numberMask}
        placeholder={placeholder}
        guide={false}
        onChange={handleChange}
        value={value}
      />
      <Icon name="percent" />
    </div>
  );
};

export default PercentageInput;
