import { asyncComponent } from '../modules';

const EmailInput = asyncComponent(() => import('./EmailInput'));
const MobileInput = asyncComponent(() => import('./MobileInput'));
const IntegerInput = asyncComponent(() => import('./IntegerInput'));
const PercentageInput = asyncComponent(() => import('./PercentageInput'));
const PhoneInput = asyncComponent(() => import('./PhoneInput'));
const SearchInput = asyncComponent(() => import('./SearchInput'));
const SearchContainer = asyncComponent(() => import('./SearchContainer'));
const WeightInput = asyncComponent(() => import('./WeightInput'));
const DateInput = asyncComponent(() => import('./DateInput'));
const DecimalInput = asyncComponent(() => import('./DecimalInput'));
const WarningMinMaxInput = asyncComponent(() => import('./WarningMinMaxInput'));
const SliderInput = asyncComponent(() => import('./SliderInput'));

export {
  EmailInput,
  MobileInput,
  IntegerInput,
  PercentageInput,
  PhoneInput,
  SearchInput,
  SearchContainer,
  WeightInput,
  DateInput,
  DecimalInput,
  WarningMinMaxInput,
  SliderInput
};
