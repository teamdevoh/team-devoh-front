import React from 'react';
import { useParams } from 'react-router-dom';
import { role } from '../../constants';
import helpers from '../../helpers';
import { ResListContainer, ReqListContainer } from './';

const ListWarpper = () => {
  const { projectId } = useParams();

  if (helpers.Identity.hasRole(role.DataRequest_Approver)) {
    return <ResListContainer projectId={projectId} />;
  } else {
    return <ReqListContainer projectId={projectId} />;
  }
};

export default ListWarpper;
