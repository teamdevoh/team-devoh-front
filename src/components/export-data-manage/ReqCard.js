import React, { Fragment, useCallback } from 'react';
import { Card, Dimmer, Header, Segment } from 'semantic-ui-react';
import { DownloadCohort, DownloadTDM } from '.';
import { format } from '../../constants';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { Run } from '../animation';
import { Color, ReqType } from './constant';
import useExportCohort from './hooks/useExportCohort';
import useExportTDM from './hooks/useExportTDM';

const ReqCard = ({
  projectId,
  approvedDateTime,
  approvedMemberName,
  exportDateTime,
  isApproved,
  rejectReason,
  reqCondition = [],
  reqDataType,
  reqDateTime,
  reqMemberId,
  reqMemberName,
  reqReason,
  originCondition,
  subjectDataRequestId,
  onHeaderClick
}) => {
  const t = useFormatMessage();
  const exportTDM = useExportTDM({ projectId });
  const exportCohort = useExportCohort({
    projectId
  });

  const handleCohortExportClick = useCallback(model => {
    exportCohort.start({
      ...model
    });
  }, []);

  const handleTDMExportClick = useCallback(model => {
    exportTDM.start({
      ...model
    });
  }, []);

  return (
    <Card
      color={
        isApproved === true
          ? Color.Approve
          : isApproved === false
            ? Color.Reject
            : Color.Pending
      }
    >
      <Card.Content>
        <Card.Header>
          <a
            onClick={() => {
              onHeaderClick({ type: reqDataType, key: subjectDataRequestId });
            }}
          >
            {helpers.util.dateformat(reqDateTime, format.YYYY_MM_DD_HHMM)}
          </a>
        </Card.Header>
        <Card.Meta>
          {ReqType.TDM === reqDataType ? 'TDM' : 'All Cohort Data'}
        </Card.Meta>
        <Card.Description>
          {isApproved === true ? (
            <div>
              <span style={{ color: Color.Approve }}>
                {t('common.approved')}
              </span>
              <span>
                ({approvedMemberName},
                {helpers.util.dateformat(
                  approvedDateTime,
                  format.YYYY_MM_DD_HHMM
                )}
                )
              </span>
            </div>
          ) : isApproved === false ? (
            <div>
              <span style={{ color: Color.Reject }}>
                {t('common.rejected')}
              </span>
              <span>
                ({approvedMemberName},
                {helpers.util.dateformat(
                  approvedDateTime,
                  format.YYYY_MM_DD_HHMM
                )}
                )
              </span>
            </div>
          ) : (
            <span style={{ color: Color.Pending }}>{t('common.pending')}</span>
          )}
        </Card.Description>
        <Card.Description>
          <div
            style={{
              height: '150px',
              padding: '20px',
              border: 'solid 1px #d2d2d2',
              overflowY: 'auto',
              color: '#666'
            }}
          >
            {reqReason && (
              <div style={{ marginTop: '1em' }}>
                <Header as="h4" dividing>
                  {t('request.reason')}
                </Header>
                {reqReason.split('\n').map((singleText, index) => {
                  return (
                    <span key={index}>
                      {singleText}
                      <br />
                    </span>
                  );
                })}
              </div>
            )}
            {reqCondition && (
              <div style={{ marginTop: '1em' }}>
                <Header as="h4" dividing>
                  {t('data.scope')}
                </Header>
                {reqCondition.map(item => {
                  return <div key={item}>{item}</div>;
                })}
              </div>
            )}
            {rejectReason && (
              <div style={{ marginTop: '1em' }}>
                <Header as="h4" dividing>
                  {t('reject.reason')}
                </Header>
                {rejectReason.split('\n').map((singleText, index) => {
                  return (
                    <span key={index}>
                      {singleText}
                      <br />
                    </span>
                  );
                })}
              </div>
            )}
          </div>
        </Card.Description>
      </Card.Content>
      <Card.Content style={{ height: '70px', textAlign: 'center' }}>
        <Dimmer.Dimmable
          as={Segment}
          basic
          blurring
          dimmed={exportCohort.loading || exportTDM.loading}
        >
          <Dimmer active={exportCohort.loading || exportTDM.loading} inverted>
            <Fragment>
              <Run style={{ height: '30px' }} />
              <div style={{ color: 'black' }}>Exporting...</div>
            </Fragment>
          </Dimmer>
          {ReqType.Cohort === reqDataType && (
            <DownloadCohort
              projectId={projectId}
              exportDateTime={exportDateTime}
              isApproved={isApproved}
              originCondition={originCondition}
              subjectDataRequestId={subjectDataRequestId}
              onClick={handleCohortExportClick}
              done={exportCohort.done}
            />
          )}
          {ReqType.TDM === reqDataType && (
            <DownloadTDM
              projectId={projectId}
              exportDateTime={exportDateTime}
              isApproved={isApproved}
              originCondition={originCondition}
              subjectDataRequestId={subjectDataRequestId}
              onClick={handleTDMExportClick}
              done={exportTDM.done}
            />
          )}
        </Dimmer.Dimmable>
      </Card.Content>
    </Card>
  );
};

export default ReqCard;
