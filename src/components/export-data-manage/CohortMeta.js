import React from 'react';
import { useBoolean } from 'react-hanger';
import { Divider, Header, Icon, Message } from 'semantic-ui-react';
import { CohortPreviewButton } from '.';
import { activity } from '../../constants';

const CohortMeta = () => {
  const visible = useBoolean(true);

  if (visible.value) {
    return (
      <Message onDismiss={visible.setFalse}>
        <Message.List>
          <Message.Item>
            <Header>All Cohort Data</Header>
          </Message.Item>
          <Divider />
          <Header as="h4">
            cPMTb-001 임상시험 대상자의 모든 임상데이터를 CSV(Excel) File로
            추출할 수 있으며, 연구자들은 추출된 데이터를 활용하여 다양한 연구에
            활용할 수 있도록 제공하고 있습니다.
          </Header>
          <div style={{ marginTop: '1em' }}>
            <Icon name="warning" color="teal" />
            <span style={{ color: 'red' }}>
              주의. 데이터양이 방대하여 다운로드 하는 일 기준으로 전일 데이터를
              제공합니다. 또한 데이터 입력자의 오기 수정과 데이터 리뷰 결과에
              따라 수정되는 경우 다운로드 일자에 따라 일부 데이터는 변경될 수
              있습니다.
            </span>
          </div>
          <Message.Item style={{ marginTop: '1em' }}>
            <Header>포함되는 데이터(Activity)</Header>
          </Message.Item>
          <Divider />
          <Header as="h4">Basic Patient Information</Header>
          <CohortPreviewButton
            text="Demography"
            name={activity.INTERVIEW}
            color="red"
          />
          <CohortPreviewButton
            text="Physical Measurement"
            name={activity.VS}
            color="orange"
          />
          <CohortPreviewButton
            text="Consent for Biological sample collection"
            name={activity.CONSENT}
            color="yellow"
          />
          <CohortPreviewButton
            text="Temporary Dx For TB"
            name={activity.TB_DIAGNOSIS}
            color="olive"
          />
          <CohortPreviewButton
            text="Comorbidity"
            name={activity.COMORBID}
            color="green"
          />
          <CohortPreviewButton
            text="Dialysis"
            name={activity.DIALYSIS}
            color="teal"
          />
          <CohortPreviewButton
            text="Re-consent of Subject"
            name={activity.ICRE}
            color="blue"
          />
          <Header as="h4" style={{ marginTop: '1em' }}>
            Medical History
          </Header>
          <CohortPreviewButton
            text="Anti-TB drugs"
            name={activity.MEDICATION}
            color="violet"
          />
          <CohortPreviewButton
            text="Concomitant Medication"
            name={activity.COMED}
            color="purple"
          />
          <Header as="h4" style={{ marginTop: '1em' }}>
            Microbiological Exam
          </Header>
          <CohortPreviewButton
            text="AFB Test"
            name={activity.AFB}
            color="pink"
          />
          <CohortPreviewButton
            text="Xpert MTB/RIF"
            name={activity.XPERT}
            color="brown"
          />
          <CohortPreviewButton
            text="Rapid drug susceptibility testing"
            name={activity.LPARESISTANT}
            color="grey"
          />
          <CohortPreviewButton
            text="Interferon-gamma releasing assay"
            name={activity.IGRA}
            color="black"
          />
          <CohortPreviewButton text="TB PCR" name={activity.PCR} color="red" />
          <CohortPreviewButton
            text="Drug Susceptibility Test"
            name={activity.DST}
            color="orange"
          />
          <Header as="h4" style={{ marginTop: '1em' }}>
            Lab & Radiologic Findings
          </Header>
          <CohortPreviewButton
            text="LBCH(Biochemistry)"
            name={activity.LBCH}
            color="yellow"
          />
          <CohortPreviewButton
            text="LBHEM(Hematology)"
            name={activity.LBHEM}
            color="olive"
          />
          <CohortPreviewButton
            text="Electrolyte"
            name={activity.ELECTROLYTE}
            color="green"
          />
          <CohortPreviewButton
            text="Chest X-ray"
            name={activity.XRAY}
            color="teal"
          />
          <CohortPreviewButton
            text="CT(Computed Tomography)"
            name={activity.CT}
            color="blue"
          />
          <CohortPreviewButton
            text="Other relevant test"
            name={activity.Other_exam}
            color="violet"
          />
          <Header as="h4" style={{ marginTop: '1em' }}>
            TDM
          </Header>
          <CohortPreviewButton
            text="TDM anti-TB Drug"
            name={activity.TDMAnti}
            color="purple"
          />
          <CohortPreviewButton
            text="TDM sampling"
            name={activity.SAMPLING}
            color="pink"
          />
          <CohortPreviewButton
            text="TB Drug concentration"
            name={activity.TB_PK}
            color="brown"
          />
          <CohortPreviewButton
            text="PK parameters"
            name={activity.PP}
            color="grey"
          />
          <CohortPreviewButton
            text="PGx result"
            name={activity.GENOTYPE}
            color="black"
          />
          <CohortPreviewButton
            text="Therapeutic Drug Monitoring Report"
            name={activity.TDMREP}
            color="red"
          />
          <CohortPreviewButton
            text="Mtb isolate transfer"
            name={activity.MIC_TRNS}
            color="orange"
          />
          <CohortPreviewButton
            text="MIC results"
            name={activity.MIC_RESULT}
            color="yellow"
          />
          <CohortPreviewButton
            text="MIC Report"
            name={activity.MICReport}
            color="olive"
          />
          <CohortPreviewButton
            text="Resistant gene sequencing"
            name={activity.RESISTANT_GENE}
            color="green"
          />
          <Header as="h4" style={{ marginTop: '1em' }}>
            ADR
          </Header>
          <CohortPreviewButton
            text="Adverse Drug Reaction"
            name={activity.ADR}
            color="teal"
          />
          <CohortPreviewButton
            text="AGEP (EuroSCAR)"
            name={activity.ADR_AGEP}
            color="blue"
          />
          <CohortPreviewButton
            text="DRESS (RegiSCAR scoring)"
            name={activity.ADR_DRESS}
            color="violet"
          />
          <CohortPreviewButton
            text="SJS/TEN (SCORTEN score)"
            name={activity.ADR_SJSTEN}
            color="purple"
          />
          <Header as="h4" style={{ marginTop: '1em' }}>
            Monitoring & Evaluation
          </Header>
          <CohortPreviewButton
            text="Case Conclusion"
            name={activity.CASE_CON}
            color="pink"
          />
          <CohortPreviewButton
            text="Relapse after the end of treatment"
            name={activity.Relapse}
            color="brown"
          />
        </Message.List>
      </Message>
    );
  }

  return (
    <Message>
      <div
        style={{ color: 'blue', cursor: 'pointer' }}
        onClick={visible.setTrue}
      >
        Cohort Data
        <Icon name="help" />
      </div>
    </Message>
  );
};

export default CohortMeta;
