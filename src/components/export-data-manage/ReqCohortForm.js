import React, { Fragment } from 'react';
import { Dropdown, Form, Header, Icon, TextArea } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import IntegerInput from '../input/IntegerInput';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';
import { CohortMeta, GenderSelector, SmokeSelector } from './';

const ReqCohortForm = ({ children, projectId, model, onChange }) => {
  const t = useFormatMessage();

  const [projectSiteItems] = useMyProjectSiteList({
    projectId: projectId
  });

  return (
    <Fragment>
      <Header as="h4" dividing>
        <Icon name="group" />
        <Header.Content>Cohort Data</Header.Content>
      </Header>
      <CohortMeta />
      <Form>
        <Form.Field>
          <label>{t('site')}</label>
          <Dropdown
            fluid
            selection
            name="projectSiteId"
            value={Number(model.projectSiteId)}
            onChange={onChange}
            options={[
              {
                text: t('common.all'),
                value: 0,
                subjectprefix: ''
              }
            ].concat(projectSiteItems)}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.sexcodeId')}</label>
          <GenderSelector
            name="sexCodeId"
            value={model.sexCodeId}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('activity.interview.smokecodeId')}</label>
          <SmokeSelector
            name="smokeCodeId"
            value={model.smokeCodeId}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Group>
          <Form.Field>
            <label>Age From</label>
            <IntegerInput
              name="ageFrom"
              label={t('common.age')}
              value={model.ageFrom}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <label>Age To</label>
            <IntegerInput
              name="ageTo"
              label={t('common.age')}
              value={model.ageTo}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group>
          <Form.Field>
            <label>BMI From</label>
            <IntegerInput
              name="bmiFrom"
              label="BMI"
              value={model.bmiFrom}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field>
            <label>BMI To</label>
            <IntegerInput
              name="bmiTo"
              label="BMI"
              value={model.bmiTo}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Field>
          <label>{t('request.reason')}</label>
          <TextArea
            name="reqReason"
            value={model.reqReason}
            onChange={onChange}
          />
        </Form.Field>
        {children}
      </Form>
    </Fragment>
  );
};

export default ReqCohortForm;
