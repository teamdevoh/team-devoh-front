import React, { Fragment } from 'react';
import { Form, Message, TextArea } from 'semantic-ui-react';
import { format } from '../../constants';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';

const RejectBox = ({ reqUserName, reqReason, reqDate, onChange }) => {
  const t = useFormatMessage();
  return (
    <Fragment>
      <Message attached>
        <Message.Header>{reqUserName}</Message.Header>
        <Message.Content>{reqReason}</Message.Content>
      </Message>
      <Message attached>
        {helpers.util.dateformat(reqDate, format.YYYY_MM_DD_HHMM)}
      </Message>
      <Form className="attached fluid segment">
        <Form.Field>
          <label>{t('reject.reason')}</label>
          <TextArea onChange={onChange} />
        </Form.Field>
      </Form>
    </Fragment>
  );
};

export default RejectBox;
