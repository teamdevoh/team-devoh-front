import React, { useCallback, useEffect, useState } from 'react';
import { Dropdown, Form, Grid, Header } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import { DatePicker } from '../datepicker/';
import { ReqType, ReqTypeName } from './constant';
import useSubjectDataResList from './hooks/useSubjectDataResList';
import ResList from './ResList';

const ResListContainer = ({ projectId }) => {
  const t = useFormatMessage();
  const [reqUsers, setReqUsers] = useState([]);
  const [selectedReqUserId, setSelectedReqUserId] = useState(0);

  const [model, setModel] = useState({
    reqDataType: '',
    fromDate: '',
    toDate: '',
    subjectDataReqStatus: '',
    reqMemberId: 0
  });
  const subjectDataResList = useSubjectDataResList({ projectId, ...model });

  const handleChange = useCallback(
    (event, data) => {
      setModel({ ...model, [data.name]: data.value });
    },
    [model]
  );

  useEffect(
    () => {
      if (subjectDataResList.items.length > 0) {
        let users = [{ key: 0, text: '-', value: 0 }];
        subjectDataResList.items.forEach(item => {
          if (_.find(users, { key: item.reqMemberId }) === void 0) {
            users.push({
              key: item.reqMemberId,
              text: item.reqMemberName,
              value: item.reqMemberId
            });
          }
        });
        setReqUsers(users);
      }
    },
    [subjectDataResList.items]
  );
  return (
    <div>
      <Header dividing>Review and approve a request</Header>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Form>
              <Form.Group>
                <Form.Field width="three">
                  <label>{t('classification')}</label>
                  <Dropdown
                    fluid
                    selection
                    name="reqDataType"
                    defaultValue=""
                    onChange={handleChange}
                    options={[
                      { key: '', text: '-', value: '' },
                      {
                        key: ReqType.TDM,
                        text: ReqTypeName[ReqType.TDM],
                        value: ReqType.TDM
                      },
                      {
                        key: ReqType.Cohort,
                        text: ReqTypeName[ReqType.Cohort],
                        value: ReqType.Cohort
                      }
                    ]}
                  />
                </Form.Field>
                <Form.Field>
                  <label>
                    {t('data.request.date')}
                    (From)
                  </label>
                  <DatePicker
                    name="fromDate"
                    value={model.fromDate}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <label>
                    {t('data.request.date')}
                    (To)
                  </label>
                  <DatePicker
                    name="toDate"
                    value={model.toDate}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Field width="three">
                  <label>{t('status')}</label>
                  <Dropdown
                    fluid
                    selection
                    name="subjectDataReqStatus"
                    defaultValue=""
                    onChange={handleChange}
                    options={[
                      { key: '', text: '-', value: '' },
                      {
                        key: '0',
                        text: 'Pending',
                        value: '0'
                      },
                      {
                        key: '1',
                        text: 'Approved',
                        value: '1'
                      },
                      {
                        key: '2',
                        text: 'Rejected',
                        value: '2'
                      }
                    ]}
                  />
                </Form.Field>
                <Form.Field width="three">
                  <label>{t('request.user')}</label>
                  <Dropdown
                    fluid
                    selection
                    defaultValue={0}
                    onChange={(event, data) => {
                      setSelectedReqUserId(data.value);
                    }}
                    options={reqUsers}
                  />
                </Form.Field>
              </Form.Group>
            </Form>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width="sixteen">
            <ResList
              projectId={projectId}
              loading={subjectDataResList.loading}
              refresh={subjectDataResList.refresh}
              items={subjectDataResList.items.filter(item => {
                return (
                  selectedReqUserId === 0 ||
                  item.reqMemberId === selectedReqUserId
                );
              })}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default ResListContainer;
