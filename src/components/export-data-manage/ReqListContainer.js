import React, { useEffect } from 'react';
import { Button, Grid, Header, Segment, Tab } from 'semantic-ui-react';
import { ReqCardGroup, ReqList } from '.';
import helpers from '../../helpers';
import useSubjectDataReqList from './hooks/useSubjectDataReqList';

const ReqListContainer = ({ projectId }) => {
  const subjectDataReqList = useSubjectDataReqList({ projectId });

  useEffect(
    () => {
      if (
        subjectDataReqList.rendered === true &&
        subjectDataReqList.items.length === 0
      ) {
        helpers.history.push(`/project/${projectId}/export/req/new`);
      }
    },
    [subjectDataReqList.rendered, subjectDataReqList.items]
  );

  return (
    <Segment basic>
      <Header dividing>Request List</Header>
      <Grid>
        <Grid.Row>
          <Grid.Column width="sixteen">
            <Button
              style={{ float: 'right' }}
              color="blue"
              onClick={() => {
                helpers.history.push(`/project/${projectId}/export/req/new`);
              }}
            >
              Add Request
            </Button>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width="sixteen">
            <Tab
              panes={[
                {
                  menuItem: {
                    key: 'grid',
                    icon: 'grid layout',
                    content: 'Grid'
                  },
                  render: () => (
                    <Tab.Pane>
                      <ReqCardGroup
                        projectId={projectId}
                        loading={subjectDataReqList.loading}
                        items={subjectDataReqList.items}
                      />
                    </Tab.Pane>
                  )
                },
                {
                  menuItem: { key: 'table', icon: 'table', content: 'Table' },
                  render: () => (
                    <Tab.Pane>
                      <ReqList
                        projectId={projectId}
                        loading={subjectDataReqList.loading}
                        items={subjectDataReqList.items}
                        refresh={subjectDataReqList.refresh}
                      />
                    </Tab.Pane>
                  )
                }
              ]}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ReqListContainer;
