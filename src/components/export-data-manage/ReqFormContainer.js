import React, { useCallback, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Card, Container, Icon } from 'semantic-ui-react';
import { CreateReqTDM, CreateReqCohort } from '.';
import helpers from '../../helpers';
import { ReqType } from './constant';
import { TDMCard, CohortCard } from './styles';

const ReqFormContainer = () => {
  const { projectId } = useParams();
  const [selectedReqType, setSelectedReqType] = useState(ReqType.TDM);

  const handleCardClick = useCallback((event, data) => {
    setSelectedReqType(data.name);
  }, []);

  const handleRequestListClick = useCallback(() => {
    helpers.history.push(`/project/${projectId}/export/req/list`);
  }, []);

  return (
    <Container textAlign="justified">
      <Card.Group stackable>
        <Card onClick={handleRequestListClick}>
          <Card.Content>
            <Card.Header>Request List</Card.Header>
            <Card.Description style={{ float: 'right' }}>
              <Icon name="arrow alternate left" size="big" />
            </Card.Description>
          </Card.Content>
        </Card>
        <TDMCard
          name={ReqType.TDM}
          selected={selectedReqType === ReqType.TDM}
          onClick={handleCardClick}
        >
          <Card.Content>
            <Card.Header>TDM Data</Card.Header>
            <Card.Description style={{ float: 'right' }}>
              <Icon name="pills" size="big" />
            </Card.Description>
          </Card.Content>
        </TDMCard>
        <CohortCard
          name={ReqType.Cohort}
          selected={selectedReqType === ReqType.Cohort}
          onClick={handleCardClick}
        >
          <Card.Content>
            <Card.Header>All Cohort Data</Card.Header>
            <Card.Description style={{ float: 'right' }}>
              <Icon name="group" size="big" />
            </Card.Description>
          </Card.Content>
        </CohortCard>
      </Card.Group>
      <Container style={{ marginTop: '3em' }}>
        {selectedReqType === ReqType.TDM && (
          <CreateReqTDM projectId={projectId} />
        )}
        {selectedReqType === ReqType.Cohort && (
          <CreateReqCohort projectId={projectId} />
        )}
      </Container>
    </Container>
  );
};

export default ReqFormContainer;
