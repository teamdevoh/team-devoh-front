import { useBoolean } from 'react-hanger';
import api from '../api';

const useSubjectDataRes = ({ projectId }) => {
  const loading = useBoolean(false);

  const approve = async ({ subjectDataRequestId }) => {
    try {
      loading.setTrue();
      const res = await api.editSubjectDataApprove({
        projectId,
        subjectDataRequestId
      });
      if (res && res.data) {
        return res.data;
      }
    } catch (error) {
    } finally {
      loading.setFalse();
    }
  };

  const reject = async ({ subjectDataRequestId, rejectReason }) => {
    try {
      loading.setTrue();
      const res = await api.editSubjectDataReject({
        projectId,
        subjectDataRequestId,
        rejectReason
      });
      if (res && res.data) {
        return res.data;
      }
    } catch (error) {
    } finally {
      loading.setFalse();
    }
  };

  return { approve, reject, loading: loading.value };
};

export default useSubjectDataRes;
