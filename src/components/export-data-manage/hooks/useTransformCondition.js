import { useCallback } from 'react';
import { Condition, ReqType } from '../constant';

const useTransformCondition = () => {
  const { Name, Symbol } = Condition;

  const nullToEmpty = value => {
    return value === 'undefined' || value === null ? '' : value;
  };

  const deserialize = useCallback(data => {
    let keyValues = [];

    const conditions = data.split(Symbol.Item);
    conditions.forEach(condition => {
      const keyValuePairs = condition.split(Symbol.KV);
      const key = keyValuePairs[0];
      const value = keyValuePairs[1];
      keyValues.push({ key, value });
    });

    return keyValues;
  }, []);

  const serialize = useCallback(
    ({
      reqType,
      ageFrom,
      ageTo,
      bmiFrom,
      bmiTo,
      projectSiteId,
      sexCodeId,
      smokeCodeId,
      subjectId,
      tbdrugId
    }) => {
      if (reqType === ReqType.Cohort) {
        return `${Name.ProjectSite}${Symbol.KV}${projectSiteId}${Symbol.Item}${
          Name.SexCode
        }${Symbol.KV}${sexCodeId}${Symbol.Item}${Name.SmokeCode}${
          Symbol.KV
        }${smokeCodeId}${Symbol.Item}${Name.Age}${Symbol.KV}${nullToEmpty(
          ageFrom
        )}${Symbol.FT}${nullToEmpty(ageTo)}${Symbol.Item}${Name.BMI}${
          Symbol.KV
        }${nullToEmpty(bmiFrom)}${Symbol.FT}${nullToEmpty(bmiTo)}`;
      } else {
        return `${Name.ProjectSite}${Symbol.KV}${projectSiteId}${Symbol.Item}${
          Name.Subject
        }${Symbol.KV}${subjectId}${Symbol.Item}${Name.TBDrug}${
          Symbol.KV
        }${tbdrugId}`;
      }
    },
    []
  );

  return { deserialize, serialize };
};

export default useTransformCondition;
