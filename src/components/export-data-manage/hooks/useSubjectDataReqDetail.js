import { useCallback, useEffect, useState } from 'react';
import { useTransformCondition } from '.';
import api from '../api';
import { Condition } from '../constant';

const useSubjectDataReqDetail = ({ projectId, subjectDataRequestId }) => {
  const [item, setItem] = useState({});
  const transformCondition = useTransformCondition();

  const fetchItem = useCallback(
    async () => {
      const res = await api.fetchItemForReqUser({
        projectId,
        subjectDataRequestId
      });
      if (res && res.data) {
        let model = {
          isApproved: res.data.isApproved,
          exportDateTime: res.data.exportDateTime,
          reqReason: res.data.reqReason,
          reqDataType: res.data.reqDataType
        };

        const conditions = transformCondition.deserialize(
          res.data.reqCondition
        );

        conditions.forEach(condition => {
          const { key, value } = condition;
          const { Name, Symbol } = Condition;

          if (key.indexOf(Name.Age) > -1) {
            model['ageFrom'] = value.split(Symbol.FT)[0];
            model['ageTo'] = value.split(Symbol.FT)[1];
          } else if (key.indexOf(Name.BMI) > -1) {
            model['bmiFrom'] = value.split(Symbol.FT)[0];
            model['bmiTo'] = value.split(Symbol.FT)[1];
          } else {
            model[key] = value;
          }
        });

        setItem(model);
      }
    },
    [projectId, subjectDataRequestId]
  );

  useEffect(
    () => {
      fetchItem();
    },
    [projectId, subjectDataRequestId]
  );

  return { item };
};

export default useSubjectDataReqDetail;
