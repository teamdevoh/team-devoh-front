import { useCallback, useEffect, useState } from 'react';
import { code, codeGroup } from '../../../constants';
import { useBoolean, useCodes, useFormatMessage } from '../../../hooks';
import useMyProjectSiteList from '../../project-manage/hooks/useMyProjectSiteList';
import api from '../api';
import collectionApi from '../../collection/api';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import { useTransformCondition } from './';
import { Condition } from '../constant';

const useSubjectDataReqList = ({ projectId }) => {
  const t = useFormatMessage();
  const [items, setItems] = useState([]);
  const loading = useBoolean(false);
  const rendered = useBoolean(false);
  const [projectSiteItems] = useMyProjectSiteList({
    projectId: projectId
  });
  const [drugs] = useTbdrugList();
  const [smokeCodes] = useCodes({ codeGroupId: codeGroup.SmokeCode });
  const { Symbol } = Condition;

  const transformCondition = useTransformCondition();

  const getConditionSummaries = async (key, value) => {
    value = value === 'undefined' ? '' : value;
    if (key === 'projectSiteId') {
      let item = {
        name: t('site'),
        text: 'All'
      };
      if (value.length > 0) {
        const foundItem = _.find(projectSiteItems, { value: Number(value) });
        if (foundItem) {
          item.text = foundItem.text;
        }
      }
      return item;
    }

    if (key === 'sexCodeId') {
      let item = {
        name: t('activity.interview.sexcodeId'),
        text: 'All'
      };

      if (value.length > 0 && value !== '0') {
        item.text =
          value === code.MALE.toString()
            ? t('common.male')
            : t('common.female');
      }
      return item;
    }

    if (key === 'smokeCodeId') {
      let item = {
        name: t('activity.interview.smokecodeId'),
        text: 'All'
      };
      if (value.length > 0 && value !== '0') {
        const foundItem = _.find(smokeCodes, { value: Number(value) });
        item.text = foundItem.text;
      }
      return item;
    }

    if (key === 'age') {
      let item = {
        name: t('common.age'),
        text: 'All'
      };

      const fromTo = value.split(Symbol.FT);

      if (!isNaN(fromTo[0]) && !isNaN(fromTo[1])) {
        item.text = `${fromTo[0]}${Symbol.FT}${fromTo[1]}`;
      }
      return item;
    }

    if (key === 'bmi') {
      let item = {
        name: 'BMI',
        text: 'All'
      };

      const fromTo = value.split(Symbol.FT);

      if (!isNaN(fromTo[0]) && !isNaN(fromTo[1])) {
        item.text = `${fromTo[0]}${Symbol.FT}${fromTo[1]}`;
      }
      return item;
    }

    if (key === 'subjectId') {
      let item = {
        name: t('subject'),
        text: 'All'
      };
      if (value.length > 0 && value !== '0') {
        const res = await collectionApi.fetchSubject({ subjectId: value });
        if (res && res.data) {
          const { subjectPrefix, subjectNo, initial } = res.data;
          item.text = `${subjectPrefix}${subjectNo}/${initial}`;
        }
        return item;
      }
    }

    if (key === 'tbdrugId') {
      let item = {
        name: t('tb.drug'),
        text: 'All'
      };
      if (value.length > 0 && value !== '0') {
        const foundItem = _.find(drugs.items, { value: Number(value) });
        if (foundItem) {
          item.text = foundItem.text;
        }
      }
      return item;
    }
  };

  const fetchItems = useCallback(
    async () => {
      loading.setTrue();
      try {
        const res = await api.fetchItemsForReqUser({ projectId });
        if (res && res.data) {
          if (res.data.length === 0) {
            rendered.setTrue();
          }

          res.data.forEach(async (item, index) => {
            let originConditionItems = [];
            let conditionItems = [];
            let promises = [];

            const conditions = transformCondition.deserialize(
              item.reqCondition
            );

            conditions.forEach(condition => {
              const { key, value } = condition;
              promises.push(getConditionSummaries(key, value));
              originConditionItems.push({ key, value });
            });

            const responses = await Promise.all(promises);
            responses.forEach(summary => {
              if (summary !== void 0) {
                conditionItems.push(`${summary.name} : ${summary.text}`);
              }
            });

            item.reqCondition = conditionItems;
            item.originCondition = originConditionItems;

            if (res.data.length - 1 === index) {
              setItems(res.data);
            }
          });
        }
      } catch (error) {
      } finally {
        loading.setFalse();
      }
    },
    [projectSiteItems, smokeCodes, drugs.items]
  );

  useEffect(
    () => {
      if (projectSiteItems.length > 0 && smokeCodes.length > 0) {
        fetchItems();
      }
    },
    [projectId, projectSiteItems, smokeCodes, drugs.items]
  );

  return {
    items,
    loading: loading.value,
    refresh: fetchItems,
    rendered: rendered.value
  };
};

export default useSubjectDataReqList;
