import { useCallback, useEffect, useState } from 'react';
import { code, codeGroup } from '../../../constants';
import { useBoolean, useCodes, useFormatMessage } from '../../../hooks';
import { useTbdrugList } from '../../collection-tbdrug/hooks';
import useMyProjectSiteList from '../../project-manage/hooks/useMyProjectSiteList';
import api from '../api';
import collectionApi from '../../collection/api';
import { useTransformCondition } from '.';
import { Condition } from '../constant';

const useSubjectDataResList = ({
  projectId,
  reqDataType,
  fromDate,
  toDate,
  subjectDataReqStatus,
  reqMemberId
}) => {
  const t = useFormatMessage();
  const [items, setItems] = useState([]);
  const loading = useBoolean(false);
  const [projectSiteItems] = useMyProjectSiteList({
    projectId: projectId
  });
  const [drugs] = useTbdrugList();
  const [smokeCodes] = useCodes({ codeGroupId: codeGroup.SmokeCode });
  const transformCondition = useTransformCondition();
  const { Symbol } = Condition;

  const getConditionSummaries = async (key, value) => {
    value = value === 'undefined' ? '' : value;
    if (key === 'projectSiteId') {
      let item = {
        name: t('site'),
        text: 'All'
      };
      if (value.length > 0) {
        const foundItem = _.find(projectSiteItems, { value: Number(value) });
        if (foundItem) {
          item.text = foundItem.text;
        }
      }
      return item;
    }

    if (key === 'sexCodeId') {
      let item = {
        name: t('activity.interview.sexcodeId'),
        text: 'All'
      };

      if (value.length > 0 && value !== '0') {
        item.text =
          value === code.MALE.toString()
            ? t('common.male')
            : t('common.female');
      }
      return item;
    }

    if (key === 'smokeCodeId') {
      let item = {
        name: t('activity.interview.smokecodeId'),
        text: 'All'
      };
      if (value.length > 0 && value !== '0') {
        const foundItem = _.find(smokeCodes, { value: Number(value) });
        item.text = foundItem.text;
      }
      return item;
    }

    if (key === 'age') {
      let item = {
        name: t('common.age'),
        text: 'All'
      };

      const fromTo = value.split(Symbol.FT);

      if (!isNaN(fromTo[0]) && !isNaN(fromTo[1])) {
        item.text = `${fromTo[0]}${Symbol.FT}${fromTo[1]}`;
      }
      return item;
    }

    if (key === 'bmi') {
      let item = {
        name: 'BMI',
        text: 'All'
      };

      const fromTo = value.split(Symbol.FT);

      if (!isNaN(fromTo[0]) && !isNaN(fromTo[1])) {
        item.text = `${fromTo[0]}${Symbol.FT}${fromTo[1]}`;
      }
      return item;
    }

    if (key === 'subjectId') {
      let item = {
        name: t('subject'),
        text: 'All'
      };
      if (value.length > 0 && value !== '0') {
        const res = await collectionApi.fetchSubject({ subjectId: value });
        if (res && res.data) {
          const { subjectPrefix, subjectNo, initial } = res.data;
          item.text = `${subjectPrefix}${subjectNo}/${initial}`;
        }
        return item;
      }
    }

    if (key === 'tbdrugId') {
      let item = {
        name: t('tb.drug'),
        text: 'All'
      };
      if (value.length > 0 && value !== '0') {
        const foundItem = _.find(drugs.items, { value: Number(value) });
        if (foundItem) {
          item.text = foundItem.text;
        }
      }
      return item;
    }
  };

  const fetchItems = useCallback(
    async () => {
      loading.setTrue();
      try {
        const res = await api.fetchItemsForApprover({
          projectId,
          reqDataType,
          fromDate,
          toDate,
          subjectDataReqStatus,
          reqMemberId
        });
        if (res && res.data) {
          res.data.forEach(item => {
            let conditionItems = [];

            const conditions = transformCondition.deserialize(
              item.reqCondition
            );

            conditions.forEach(async condition => {
              const { key, value } = condition;
              const summary = await getConditionSummaries(key, value);
              if (summary !== void 0) {
                conditionItems.push(`${summary.name} : ${summary.text}`);
              }
            });
            item.reqCondition = conditionItems;
          });
          setItems(res.data);
        }
      } catch (error) {
      } finally {
        loading.setFalse();
      }
    },
    [
      projectId,
      projectSiteItems,
      smokeCodes,
      reqDataType,
      fromDate,
      toDate,
      subjectDataReqStatus,
      reqMemberId
    ]
  );

  useEffect(
    () => {
      if (projectSiteItems.length > 0 && smokeCodes.length > 0) {
        fetchItems();
      }
    },
    [
      projectId,
      projectSiteItems,
      smokeCodes,
      reqDataType,
      fromDate,
      toDate,
      subjectDataReqStatus,
      reqMemberId
    ]
  );

  return { items, loading: loading.value, refresh: fetchItems };
};

export default useSubjectDataResList;
