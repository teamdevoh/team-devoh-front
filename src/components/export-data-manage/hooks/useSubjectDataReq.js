import { useCallback } from 'react';
import { useBoolean } from 'react-hanger';
import { useTransformCondition } from '.';
import api from '../api';

const useSubjectDataReq = ({ projectId, model }) => {
  const loading = useBoolean(false);
  const transformCondition = useTransformCondition();

  const add = async ({ reqType }) => {
    const condition = transformCondition.serialize({ ...model, reqType });

    const newModel = {
      reqDataType: reqType,
      reqCondition: condition,
      reqReason: model.reqReason
    };

    loading.setTrue();

    try {
      const res = await api.addSubjectDataRequest({
        projectId,
        model: newModel
      });
      if (res && res.data) {
        return res.data;
      }
    } catch (error) {
    } finally {
      loading.setFalse();
    }
  };

  const edit = useCallback(
    async subjectDataRequestId => {
      const condition = transformCondition.serialize({
        ...model,
        reqType: model.reqDataType
      });

      const newModel = {
        reqCondition: condition,
        reqReason: model.reqReason
      };

      loading.setTrue();

      try {
        const res = await api.editSubjectDataRequest({
          projectId,
          subjectDataRequestId: subjectDataRequestId,
          model: newModel
        });
        if (res && res.data) {
          return res.data;
        }
      } catch (error) {
      } finally {
        loading.setFalse();
      }
    },
    [projectId, model]
  );

  return { add, edit, loading: loading.value };
};

export default useSubjectDataReq;
