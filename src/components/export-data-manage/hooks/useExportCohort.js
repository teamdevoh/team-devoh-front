import { useBoolean } from 'react-hanger';
import { activity as av } from '../../../constants';
import api from '../api';

const useExportCohort = ({ projectId }) => {
  const done = useBoolean(false);
  const loading = useBoolean(false);

  const BasicPatientInformationGroup = [
    av.INTERVIEW,
    av.VS,
    av.CONSENT,
    av.TB_DIAGNOSIS,
    av.COMORBID,
    av.DIALYSIS,
    av.ICRE
  ];

  const MedicationHistoryGroup = [av.MEDICATION, av.COMED];

  const MicrobiologicalExamGroup = [
    av.AFB,
    av.XPERT,
    av.LPARESISTANT,
    av.IGRA,
    av.PCR,
    av.DST
  ];

  const LabnRadiologicFindingsGroup = [
    av.LBCH,
    av.LBHEM,
    av.ELECTROLYTE,
    av.XRAY,
    av.CT,
    av.Other_exam
  ];

  const TDMGroup = [
    av.TDMAnti,
    av.SAMPLING,
    av.TB_PK,
    av.PP,
    av.GENOTYPE,
    av.TDMREP,
    av.MIC_TRNS,
    av.MIC_RESULT,
    av.MICReport,
    av.RESISTANT_GENE
  ];
  const ADRGroup = [av.ADR, av.ADR_AGEP, av.ADR_DRESS, av.ADR_SJSTEN];

  const MonitoringnEvaluationGroup = [av.CASE_CON, av.Relapse];

  let groups = [
    BasicPatientInformationGroup,
    MedicationHistoryGroup,
    MicrobiologicalExamGroup,
    LabnRadiologicFindingsGroup,
    TDMGroup,
    ADRGroup,
    MonitoringnEvaluationGroup
  ];

  const start = async ({
    subjectDataRequestId,
    projectSiteId,
    sexCodeId,
    smokeCodeId,
    ageFrom,
    ageTo,
    bmiFrom,
    bmiTo,
    cb
  }) => {
    loading.setTrue();

    await exportData({
      groups,
      projectId,
      subjectDataRequestId,
      projectSiteId,
      sexCodeId,
      smokeCodeId,
      ageFrom,
      ageTo,
      bmiFrom,
      bmiTo,
      cb,
      activeIndex: 0
    });
  };

  const exportData = async data => {
    try {
      if (data.groups.length > data.activeIndex) {
        const ids = data.groups[data.activeIndex].join(',');

        await api.exportActivityForReqUser({
          ...data,
          activityId: ids,
          totalGroupLength: data.groups.length
        });

        exportData({ ...data, activeIndex: data.activeIndex + 1 });
      }
    } catch (error) {
    } finally {
      if (data.groups.length === data.activeIndex + 1) {
        done.setTrue();
        loading.setFalse();
      }
    }
  };

  return { start, loading: loading.value, done: done.value };
};

export default useExportCohort;
