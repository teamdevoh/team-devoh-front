import React from 'react';
import { Table } from 'semantic-ui-react';
import { Modal } from '../modal';
import data from './data.json';

const CohortPreviewModal = ({ name, show, onClose }) => {
  const item = data[name];
  const hasInitialNo = item.initial && item.initial.length > 0;
  return (
    <Modal
      size="fullscreen"
      open={show}
      onOK={onClose}
      title="Preview"
      content={
        <Table celled structured>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell
                style={{ backgroundColor: '#F0FFFF' }}
                colSpan={hasInitialNo ? '3' : '2'}
              >
                Subject
              </Table.HeaderCell>
              <Table.HeaderCell
                style={{ backgroundColor: '#F5F5DC' }}
                colSpan={item.columns.length}
              >
                {item.title}
              </Table.HeaderCell>
            </Table.Row>
            <Table.Row>
              <Table.HeaderCell style={{ backgroundColor: '#F0FFFF' }}>
                Site
              </Table.HeaderCell>
              <Table.HeaderCell style={{ backgroundColor: '#F0FFFF' }}>
                No
              </Table.HeaderCell>
              {hasInitialNo && (
                <Table.HeaderCell style={{ backgroundColor: '#F0FFFF' }}>
                  Initial
                </Table.HeaderCell>
              )}
              {item.columns.map((column, i) => {
                return (
                  <Table.HeaderCell
                    key={i}
                    style={{ backgroundColor: '#F5F5DC' }}
                  >
                    {column.split('\n').map((singleText, index) => {
                      return (
                        <span key={index}>
                          {singleText}
                          <br />
                        </span>
                      );
                    })}
                  </Table.HeaderCell>
                );
              })}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell>{item.site}</Table.Cell>
              <Table.Cell>{item.subjectNo}</Table.Cell>
              {hasInitialNo && <Table.Cell>{item.initial}</Table.Cell>}
              {item.data.map((d, i) => {
                return (
                  <Table.Cell key={i}>
                    {d.split('\n').map((singleText, index) => {
                      return (
                        <span key={index}>
                          {singleText}
                          <br />
                        </span>
                      );
                    })}
                  </Table.Cell>
                );
              })}
            </Table.Row>
          </Table.Body>
        </Table>
      }
    />
  );
};

export default CohortPreviewModal;
