import { asyncComponent } from '../modules';

const ReqButton = asyncComponent(() => import('./ReqButton'));
const ListWarpper = asyncComponent(() => import('./ListWarpper'));
const ReqListContainer = asyncComponent(() => import('./ReqListContainer'));
const ReqList = asyncComponent(() => import('./ReqList'));
const ResListContainer = asyncComponent(() => import('./ResListContainer'));
const ResList = asyncComponent(() => import('./ResList'));
const ReqFormContainer = asyncComponent(() => import('./ReqFormContainer'));
const ReqTDMForm = asyncComponent(() => import('./ReqTDMForm'));
const ReqCohortForm = asyncComponent(() => import('./ReqCohortForm'));
const GenderSelector = asyncComponent(() => import('./GenderSelector'));
const SmokeSelector = asyncComponent(() => import('./SmokeSelector'));
const CreateReqCohort = asyncComponent(() => import('./CreateReqCohort'));
const CreateReqTDM = asyncComponent(() => import('./CreateReqTDM'));
const EditReqCohort = asyncComponent(() => import('./EditReqCohort'));
const ApproveBox = asyncComponent(() => import('./ApproveBox'));
const RejectBox = asyncComponent(() => import('./RejectBox'));
const ReqCardGroup = asyncComponent(() => import('./ReqCardGroup'));
const ReqCard = asyncComponent(() => import('./ReqCard'));
const DownloadCohort = asyncComponent(() => import('./DownloadCohort'));
const DownloadTDM = asyncComponent(() => import('./DownloadTDM'));
const TDMMeta = asyncComponent(() => import('./TDMMeta'));
const CohortMeta = asyncComponent(() => import('./CohortMeta'));
const CohortPreviewButton = asyncComponent(() => import('./CohortPreviewButton'));
const CohortPreviewModal = asyncComponent(() => import('./CohortPreviewModal'));

export {
  ReqButton,
  ListWarpper,
  ReqListContainer,
  ReqList,
  ResListContainer,
  ReqFormContainer,
  ReqTDMForm,
  ReqCohortForm,
  GenderSelector,
  SmokeSelector,
  CreateReqCohort,
  CreateReqTDM,
  EditReqCohort,
  ApproveBox,
  RejectBox,
  ReqCardGroup,
  ReqCard,
  DownloadCohort,
  DownloadTDM,
  TDMMeta,
  CohortMeta,
  CohortPreviewButton,
  CohortPreviewModal
};
