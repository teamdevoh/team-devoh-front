import React from 'react';
import { useBoolean } from 'react-hanger';
import { Divider, Header, Icon, Message } from 'semantic-ui-react';
import { CohortPreviewButton } from '.';

const TDMMeta = () => {
  const visible = useBoolean(true);
  if (visible.value) {
    return (
      <Message onDismiss={visible.setFalse}>
        <Message.List>
          <Message.Item>
            <Header>TDM Data</Header>
          </Message.Item>
          <Divider />
          <Header as="h4">
            cPMTb-001 임상시험 대상자의 결핵약물 복용시간과 채혈시간 및
            약물농도분석결과 데이터를 CSV(Excel) File로 추출할 수 있으며, 추출된
            데이터를 활용하여 TDM 농도분석결과 보고서, TDM Scatter Plot등의 RAW
            데이터로 활용할 수 있습니다.
          </Header>
          <div>
            <Icon name="warning" color="teal" />
            <span style={{ color: 'red' }}>
              데이터 입력자의 오기 수정과 데이터 리뷰 결과에 따라 수정되는 경우
              다운로드 일자에 따라 일부 데이터는 변경될 수 있습니다.
            </span>
          </div>
          <Message.Item style={{ marginTop: '1em' }}>
            <Header>포함되는 데이터(Activity)</Header>
          </Message.Item>
          <Divider />
          <CohortPreviewButton
            text="Demography"
            name={'demography'}
            color="red"
          />
          <CohortPreviewButton
            text="Physical Measurement"
            name={'physical'}
            color="orange"
          />
          <CohortPreviewButton
            text="Temporary Dx For TB"
            name={'temporary'}
            color="yellow"
          />
          <CohortPreviewButton
            text="PGx Result(NAT2 & SLCO1B1)"
            name={'pGx'}
            color="olive"
          />
          <CohortPreviewButton
            text="TDM anti-TB Drug"
            name={'anti-TB'}
            color="green"
          />
          <CohortPreviewButton
            text="TDM Sampling"
            name={'sampling'}
            color="teal"
          />
          <CohortPreviewButton
            text="TB Drug Concentration"
            name={'concentration'}
            color="blue"
          />
          <CohortPreviewButton
            text="PK Parameters"
            name={'pk_parameters'}
            color="violet"
          />
          <CohortPreviewButton
            text="LBCH(Biochemistry)"
            name={'lbch'}
            color="purple"
          />
          <CohortPreviewButton
            text="LBHEM(Hematology)"
            name={'lbhem'}
            color="pink"
          />
          <CohortPreviewButton
            text="Comorbidity"
            name={'comorbidity'}
            color="brown"
          />
          <CohortPreviewButton
            text="Concomitant Medication"
            name={'concomitant'}
            color="grey"
          />
          <CohortPreviewButton
            text="Concomitant Medication"
            name={'concomitant'}
            color="grey"
          />
          <CohortPreviewButton
            text="Adverse Drug Reaction"
            name={'adr'}
            color="black"
          />
          <CohortPreviewButton
            text="Case Conclusion"
            name={'case'}
            color="orange"
          />
        </Message.List>
      </Message>
    );
  }

  return (
    <Message>
      <div
        style={{ color: 'blue', cursor: 'pointer' }}
        onClick={visible.setTrue}
      >
        TDM Data
        <Icon name="help" />
      </div>
    </Message>
  );
};

export default TDMMeta;
