import React from 'react';
import { Card, Icon, Grid } from 'semantic-ui-react';
import { code } from '../../constants';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { ItemCard } from './styles';

const GenderSelector = ({ name, value, onChange }) => {
  const t = useFormatMessage();
  const genderType = {
    All: 0,
    Male: code.MALE,
    Female: code.FEMALE
  };

  const selectedGender = value || '';

  const isMobile = helpers.util.isMobile();
  const iconSize = isMobile ? 'large' : 'big';

  return (
    <div style={{ marginTop: '1em' }}>
      <Card.Group itemsPerRow={isMobile ? 3 : 9}>
        <ItemCard
          selected={selectedGender.toString() === genderType.All.toString()}
        >
          <Card.Content
            textAlign="center"
            onClick={() => {
              onChange(null, { name, value: genderType.All.toString() });
            }}
          >
            <Grid>
              <Grid.Column width="eight" textAlign="center">
                <Icon name="male" size={iconSize} />
                <div className="name">{t('common.male')}</div>
              </Grid.Column>
              <Grid.Column width="eight" textAlign="center">
                <Icon name="female" size={iconSize} />
                <div className="name">{t('common.female')}</div>
              </Grid.Column>
            </Grid>
          </Card.Content>
        </ItemCard>
        <ItemCard
          selected={selectedGender.toString() === genderType.Male.toString()}
        >
          <Card.Content
            textAlign="center"
            onClick={() => {
              onChange(null, { name, value: genderType.Male.toString() });
            }}
          >
            <Icon name="male" size={iconSize} />
            <div className="name">{t('common.male')}</div>
          </Card.Content>
        </ItemCard>
        <ItemCard
          selected={selectedGender.toString() === genderType.Female.toString()}
        >
          <Card.Content
            textAlign="center"
            onClick={() => {
              onChange(null, { name, value: genderType.Female.toString() });
            }}
          >
            <Icon name="female" size={iconSize} />
            <div className="name">{t('common.female')}</div>
          </Card.Content>
        </ItemCard>
      </Card.Group>
    </div>
  );
};

export default GenderSelector;
