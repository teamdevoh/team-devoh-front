import React, { Fragment } from 'react';
import { Form, Message } from 'semantic-ui-react';
import { format } from '../../constants';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { ApprovalCheckbox } from './styles';

const ApproveBox = ({ reqUserName, reqReason, reqDate, checked, onChange }) => {
  const t = useFormatMessage();
  return (
    <Fragment>
      <Message attached>
        <Message.Header>{reqUserName}</Message.Header>
        <Message.Content>{reqReason}</Message.Content>
      </Message>
      <Message attached>
        {helpers.util.dateformat(reqDate, format.YYYY_MM_DD_HHMM)}
      </Message>
      <Form className="attached fluid segment">
        <ApprovalCheckbox
          label={t('agree.approval')}
          checked={checked}
          onChange={onChange}
        />
      </Form>
    </Fragment>
  );
};

export default ApproveBox;
