import React, { Fragment } from 'react';
import { useBoolean } from 'react-hanger';
import { Button } from 'semantic-ui-react';
import { CohortPreviewModal } from '.';

const CohortPreviewButton = ({ color, name, text }) => {
  const showModal = useBoolean(false);

  return (
    <Fragment>
      <Button basic color={color} onClick={showModal.setTrue}>
        {text}
      </Button>
      <CohortPreviewModal
        name={name}
        show={showModal.value}
        onClose={showModal.setFalse}
      />
    </Fragment>
  );
};

export default CohortPreviewButton;
