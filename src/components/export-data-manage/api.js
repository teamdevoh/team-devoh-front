import helpers from '../../helpers';

const addSubjectDataRequest = ({ projectId, model }) => {
  return helpers.Service.post(`api/projects/${projectId}/data.request`, model);
};

const editSubjectDataRequest = ({ projectId, subjectDataRequestId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/data.request/${subjectDataRequestId}`,
    model
  );
};

const fetchItemsForReqUser = ({ projectId }) => {
  const params = helpers.qs.stringify({});
  return helpers.Service.get(
    `api/projects/${projectId}/data.request.my?${params}`
  );
};

const fetchItemsForApprover = ({
  projectId,
  reqDataType,
  fromDate,
  toDate,
  subjectDataReqStatus,
  reqMemberId
}) => {
  const params = helpers.qs.stringify({
    reqDataType,
    fromDate,
    toDate,
    subjectDataReqStatus,
    reqMemberId
  });
  return helpers.Service.get(
    `api/projects/${projectId}/data.request.approver?${params}`
  );
};

const fetchItemForReqUser = ({ projectId, subjectDataRequestId }) => {
  return helpers.Service.get(
    `api/projects/${projectId}/data.request.my/${subjectDataRequestId}`
  );
};

const editSubjectDataApprove = ({ projectId, subjectDataRequestId }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/data.request/${subjectDataRequestId}/approve`,
    {}
  );
};

const editSubjectDataReject = ({
  projectId,
  subjectDataRequestId,
  rejectReason
}) => {
  return helpers.Service.put(
    `api/projects/${projectId}/data.request/${subjectDataRequestId}/reject`,
    { rejectReason }
  );
};

const exportActivityForReqUser = ({
  projectId,
  subjectDataRequestId,
  sexCodeId,
  ageFrom,
  ageTo,
  bmiFrom,
  bmiTo,
  smokeCodeId,
  projectSiteId,
  docName,
  tbSusceptcodeIds,
  tbDiagnosiscodeIds,
  exdrugids,
  hasADR,
  aDRKeyword,
  caseconCodeId,
  activityId,
  cb,
  activeIndex,
  totalGroupLength
}) => {
  const params = helpers.qs.stringify({
    sexCodeId,
    ageFrom,
    ageTo,
    bmiFrom,
    bmiTo,
    smokeCodeId,
    projectSiteId: projectSiteId === '' ? 0 : projectSiteId,
    docName,
    activities: 'S,' + activityId,
    tbSusceptcodeIds,
    tbDiagnosiscodeIds,
    exdrugids,
    hasADR: hasADR === 0 ? null : hasADR,
    aDRKeyword,
    caseconCodeId,
    activeIndex,
    totalGroupLength
  });

  return helpers.util
    .download(
      `api/projects/${projectId}/data.request.my/${subjectDataRequestId}/export.cohort?${params}`,
      cb
    )
    .catch(err => {
      return Promise.reject(err);
    });
};

const fetchExportTdmList = ({
  projectId,
  subjectDataRequestId,
  projectSiteId,
  subjectId,
  tbdrugId,
  offset,
  limit
}) => {
  const params = helpers.qs.stringify({
    projectSiteId,
    subjectId,
    tbdrugId,
    offset,
    limit
  });

  return helpers.Service.get(
    `api/projects/${projectId}/data.request.my/${subjectDataRequestId}/export.tdm?${params}`
  );
};

export default {
  addSubjectDataRequest,
  editSubjectDataRequest,
  fetchItemsForReqUser,
  fetchItemsForApprover,
  fetchItemForReqUser,
  editSubjectDataApprove,
  editSubjectDataReject,
  exportActivityForReqUser,
  fetchExportTdmList
};
