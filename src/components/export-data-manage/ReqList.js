import React, { Fragment, useEffect } from 'react';
import { Dimmer, Icon, Popup, Segment } from 'semantic-ui-react';
import { DownloadCohort, DownloadTDM } from '.';
import { format } from '../../constants';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import { Run } from '../animation';
import { DataGrid } from '../data-grid';
import { Color, ReqType } from './constant';
import useExportCohort from './hooks/useExportCohort';
import useExportTDM from './hooks/useExportTDM';

const ReqList = ({ projectId, loading, items, refresh }) => {
  const t = useFormatMessage();

  const exportTDM = useExportTDM({ projectId });
  const exportCohort = useExportCohort({
    projectId
  });

  const handleMoveToEdit = ({ type, key }) => {
    helpers.history.push(
      `/project/${projectId}/export/req/edit/${key}/${
        type === ReqType.Cohort ? 'cohort' : 'tdm'
      }`
    );
  };

  useEffect(
    () => {
      if (exportCohort.done || exportTDM.done) {
        refresh();
      }
    },
    [exportCohort.done, exportTDM.done]
  );

  const columns = [
    {
      key: 'reqDateTime',
      name: t('data.request.date'),
      formatter: ({ value, row }) => {
        return (
          <a
            className="link"
            onClick={() => {
              handleMoveToEdit({
                type: row.reqDataType,
                key: row.subjectDataRequestId
              });
            }}
          >
            {helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM)}
          </a>
        );
      }
    },
    {
      key: 'reqDataType',
      name: t('classification'),
      formatter: ({ value, row }) => (
        <div>{ReqType.TDM === value ? 'TDM' : 'All Cohort Data'}</div>
      )
    },
    {
      key: 'reqCondition',
      name: t('data.scope'),
      formatter: ({ value, row }) => (
        <Fragment>
          <Popup
            content={
              <div>
                {value.map(item => {
                  return <div key={item}>{item}</div>;
                })}
              </div>
            }
            trigger={
              <div>
                <Icon name="zoom-in" />
                {value[0]}
              </div>
            }
          />
        </Fragment>
      )
    },
    {
      key: 'reqReason',
      name: t('request.reason'),
      formatter: ({ value, row }) => (
        <Fragment>
          <Popup
            content={<div>{value}</div>}
            trigger={
              <div>
                <Icon name="zoom-in" />
                {value}
              </div>
            }
          />
        </Fragment>
      )
    },
    {
      key: 'isApproved',
      name: t('common.approve'),
      formatter: ({ value, row }) => {
        if (value === true) {
          return (
            <div style={{ color: Color.Approve }}>{t('common.approved')}</div>
          );
        } else if (value === false) {
          return (
            <Popup
              content={
                <Fragment>
                  <div style={{ color: Color.Reject }}>
                    {t('common.rejected')}
                  </div>
                  {row.rejectReason && (
                    <div>
                      {row.rejectReason.split('\n').map((singleText, index) => {
                        return (
                          <span key={index}>
                            {singleText}
                            <br />
                          </span>
                        );
                      })}
                    </div>
                  )}
                </Fragment>
              }
              trigger={
                <div>
                  <Icon name="zoom-in" />
                  <span style={{ color: 'red' }}>{t('common.rejected')}</span>
                </div>
              }
            />
          );
        } else {
          return (
            <div style={{ color: Color.Pending }}>{t('common.pending')}</div>
          );
        }
      }
    },
    {
      key: 'approvedDateTime',
      name: t('approve.Date'),
      formatter: ({ value, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM)}</div>
      )
    },
    {
      key: 'approvedMemberName',
      name: t('approver')
    },
    {
      key: 'exportDateTime',
      name: t('common.download'),
      formatter: ({ value, row }) => {
        if (row.reqDataType === ReqType.Cohort) {
          return (
            <DownloadCohort
              projectId={projectId}
              exportDateTime={value}
              {...row}
              onClick={model => {
                exportCohort.start({
                  ...model
                });
              }}
            />
          );
        } else {
          return (
            <DownloadTDM
              projectId={projectId}
              exportDateTime={value}
              {...row}
              onClick={model => {
                exportTDM.start({
                  ...model
                });
              }}
            />
          );
        }
      }
    }
  ];

  return (
    <Dimmer.Dimmable
      as={Segment}
      basic
      blurring
      dimmed={exportCohort.loading || exportTDM.loading}
    >
      <Dimmer inverted active={exportCohort.loading || exportTDM.loading}>
        <Fragment>
          <Run />
          <div style={{ color: 'black' }}>Exporting...</div>
        </Fragment>
      </Dimmer>

      <DataGrid
        columns={columns}
        rows={items}
        loading={loading}
        minHeight={600}
        rowKey="subjectDataRequestId"
        rowNumber={{ show: true, key: 'no', name: 'No' }}
      />
    </Dimmer.Dimmable>
  );
};

export default ReqList;
