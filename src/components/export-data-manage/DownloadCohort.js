import React, { Fragment, useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import { format } from '../../constants';
import helpers from '../../helpers';

const DownloadCohort = ({
  projectId,
  exportDateTime,
  isApproved,
  originCondition,
  subjectDataRequestId,
  onClick,
  done
}) => {
  const handleDownloadClick = useCallback(
    () => {
      let model = {};
      originCondition.forEach(condition => {
        let value = condition.value;
        if (condition.key === 'age' || condition.key === 'bmi') {
          const values = value.split('-');
          const from = values[0];
          const to = values[1];

          model[condition.key + 'From'] = from;
          model[condition.key + 'To'] = to;
        } else {
          model[condition.key] = value;
        }
      });

      onClick({ subjectDataRequestId, ...model });
    },
    [originCondition, onClick]
  );

  if (!done && exportDateTime === null) {
    return isApproved === true ? (
      <Icon name="download" className="link" onClick={handleDownloadClick} />
    ) : (
      <Fragment />
    );
  } else {
    return (
      <div>
        Exported{' '}
        {helpers.util.dateformat(
          done
            ? exportDateTime === null
              ? new Date()
              : exportDateTime
            : exportDateTime,
          format.YYYY_MM_DD_HHMM
        )}
      </div>
    );
  }
};
export default DownloadCohort;
