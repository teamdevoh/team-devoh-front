import React, { useCallback, useEffect, useState } from 'react';
import { Button, Container } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { useFormatMessage } from '../../hooks';
import useSubjectDataReq from './hooks/useSubjectDataReq';
import { ReqTDMForm } from './';
import helpers from '../../helpers';
import useSubjectDataReqDetail from './hooks/useSubjectDataReqDetail';
import { useParams } from 'react-router-dom';

const EditReqTDM = () => {
  const { projectId, subjectDataRequestId } = useParams();
  const t = useFormatMessage();

  const subjectDataReqDetail = useSubjectDataReqDetail({
    projectId,
    subjectDataRequestId
  });

  const [model, setModel] = useState({});

  const subjectDataReq = useSubjectDataReq({ projectId, model });

  useEffect(
    () => {
      setModel(subjectDataReqDetail.item);
    },
    [subjectDataReqDetail.item]
  );

  const handleChange = useCallback(
    (event, data) => {
      setModel({ ...model, [data.name]: data.value });
    },
    [model]
  );

  const handleSubmit = useCallback(
    () => {
      const isOK = subjectDataReq.edit(subjectDataRequestId);
      if (isOK) {
        toast(<div>{t('common.alert.changed')}</div>, {
          type: 'success',
          autoClose: 1000,
          onClose: () =>
            helpers.history.push(`/project/${projectId}/export/req/list`)
        });
      }
    },
    [model]
  );

  return (
    <Container>
      <ReqTDMForm projectId={projectId} model={model} onChange={handleChange}>
        {subjectDataReqDetail.item.isApproved === null && (
          <Button onClick={handleSubmit}>Edit</Button>
        )}
      </ReqTDMForm>
    </Container>
  );
};

export default EditReqTDM;
