import React, { useCallback, useState } from 'react';
import { toast } from 'react-toastify';
import { useBoolean, useFormatMessage } from '../../hooks';
import useSubjectDataReq from './hooks/useSubjectDataReq';
import { ReqButton, ReqCohortForm } from './';
import helpers from '../../helpers';
import { ReqType } from './constant';

const CreateReqCohort = ({ projectId }) => {
  const t = useFormatMessage();
  const saved = useBoolean(false);
  const [model, setModel] = useState({
    projectSiteId: '',
    sexCodeId: '0',
    smokeCodeId: '0',
    ageFrom: '',
    ageTo: '',
    bmiFrom: '',
    bmiTo: '',
    reqReason: ''
  });

  const subjectDataReq = useSubjectDataReq({ projectId, model });

  const handleChange = useCallback(
    (event, data) => {
      setModel({ ...model, [data.name]: data.value });
    },
    [model]
  );

  const handleSubmit = useCallback(
    () => {
      const isOK = subjectDataReq.add({ reqType: ReqType.Cohort });
      if (isOK) {
        saved.setValue(isOK);
        toast(<div>{t('common.alert.requested')}</div>, {
          type: 'success',
          autoClose: 1000,
          onClose: () =>
            helpers.history.push(`/project/${projectId}/export/req/list`)
        });
      }
    },
    [model]
  );

  return (
    <ReqCohortForm projectId={projectId} model={model} onChange={handleChange}>
      <ReqButton onClick={handleSubmit} succeded={saved.value} />
    </ReqCohortForm>
  );
};

export default CreateReqCohort;
