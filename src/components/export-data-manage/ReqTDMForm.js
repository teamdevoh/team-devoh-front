import React, { Fragment } from 'react';
import { Dropdown, Form, Header, Icon, TextArea } from 'semantic-ui-react';
import { TDMMeta } from '.';
import { useFormatMessage } from '../../hooks';
import { TbdrugList } from '../collection-tbdrug/Selector';
import { Subjects } from '../collection/Selector';
import useMyProjectSiteList from '../project-manage/hooks/useMyProjectSiteList';

const ReqTDMForm = ({ children, projectId, model, onChange }) => {
  const t = useFormatMessage();

  const [projectSiteItems] = useMyProjectSiteList({
    projectId: projectId
  });

  return (
    <Fragment>
      <Header as="h4" dividing>
        <Icon name="pills" />
        <Header.Content>TDM Data</Header.Content>
      </Header>
      <TDMMeta />
      <Form>
        <Form.Field>
          <label>{t('site')}</label>
          <Dropdown
            fluid
            selection
            name="projectSiteId"
            value={Number(model.projectSiteId)}
            onChange={onChange}
            options={[
              {
                text: t('common.all'),
                value: 0,
                subjectprefix: ''
              }
            ].concat(projectSiteItems)}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('subject')}</label>
          <Subjects
            projectSiteId={model.projectSiteId}
            name="subjectId"
            value={Number(model.subjectId)}
            onChange={onChange}
            reset={model.projectSiteId === 0}
            hasAll
            disabled={model.projectSiteId === '' || model.projectSiteId === 0}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('tb.drug')}</label>
          <TbdrugList
            defaultOption={[
              {
                text: '-----------------------------------------',
                value: 0
              }
            ]}
            fluid
            name="tbdrugId"
            value={Number(model.tbdrugId)}
            onChange={onChange}
          />
        </Form.Field>
        <Form.Field>
          <label>{t('request.reason')}</label>
          <TextArea
            name="reqReason"
            value={model.reqReason}
            onChange={onChange}
          />
        </Form.Field>
        {children}
      </Form>
    </Fragment>
  );
};

export default ReqTDMForm;
