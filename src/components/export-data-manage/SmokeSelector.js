import React from 'react';
import { Card } from 'semantic-ui-react';
import { codeGroup } from '../../constants';
import helpers from '../../helpers';
import { useCodes } from '../../hooks';
import { ItemCard } from './styles';

const SmokeSelector = ({ name, value, onChange }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.SmokeCode });

  const isMobile = helpers.util.isMobile();

  return (
    <div style={{ marginTop: '1em' }}>
      <Card.Group itemsPerRow={isMobile ? 3 : 9}>
        {[{ key: 0, text: 'All', value: 0 }, ...items].map(item => {
          return (
            <ItemCard
              key={item.value}
              selected={
                item.value.toString() ===
                (value === void 0 ? '' : value).toString()
              }
            >
              <Card.Content
                textAlign="center"
                onClick={() => {
                  onChange(null, { name, value: item.value });
                }}
              >
                <div className="name">{item.text}</div>
              </Card.Content>
            </ItemCard>
          );
        })}
      </Card.Group>
    </div>
  );
};

export default SmokeSelector;
