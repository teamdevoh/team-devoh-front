import React, { Fragment, useCallback } from 'react';
import { Icon } from 'semantic-ui-react';
import { format } from '../../constants';
import helpers from '../../helpers';

const DownloadTDM = ({
  projectId,
  exportDateTime,
  isApproved,
  originCondition,
  subjectDataRequestId,
  onClick,
  done
}) => {
  const handleDownloadClick = useCallback(
    () => {
      let model = {};
      originCondition.forEach(condition => {
        let value = condition.value;
        model[condition.key] = value;
      });

      onClick({
        subjectDataRequestId,
        ...model
      });
    },
    [originCondition, onClick]
  );

  if (!done && exportDateTime === null) {
    return isApproved === true ? (
      <Icon name="download" className="link" onClick={handleDownloadClick} />
    ) : (
      <Fragment />
    );
  } else {
    return (
      <div>
        Exported{' '}
        {helpers.util.dateformat(
          exportDateTime || new Date(),
          format.YYYY_MM_DD_HHMM
        )}
      </div>
    );
  }
};

export default DownloadTDM;
