import React, { Fragment, useCallback, useState } from 'react';
import { Button, Icon, Popup } from 'semantic-ui-react';
import { format } from '../../constants';
import helpers from '../../helpers';
import { useBoolean, useFormatMessage } from '../../hooks';
import { DataGrid } from '../data-grid';
import { Confirm, notification } from '../modal';
import { ReqType } from './constant';
import useSubjectDataRes from './hooks/useSubjectDataRes';
import { ApproveBox, RejectBox } from './';

const ResList = ({ projectId, loading, items, refresh }) => {
  const t = useFormatMessage();
  const defaultSelectedItem = {
    key: 0,
    reqUserName: '',
    reqReason: '',
    reqDate: ''
  };
  const [selectedItem, setSelectedItem] = useState(defaultSelectedItem);
  const [approvalAgree, setApprovalAgree] = useState(false);
  const [rejectReason, setRejectReason] = useState('');
  const showApproveConfirm = useBoolean(false);
  const showRejectConfirm = useBoolean(false);

  const subjectDataRes = useSubjectDataRes({ projectId });

  const handleApproveClick = useCallback(currentRow => {
    const {
      subjectDataRequestId,
      reqMemberName,
      reqReason,
      reqDateTime
    } = currentRow;
    setSelectedItem({
      key: subjectDataRequestId,
      reqUserName: reqMemberName,
      reqReason,
      reqDate: reqDateTime
    });

    showApproveConfirm.setTrue();
  }, []);

  const handleRejectClick = useCallback(currentRow => {
    const {
      subjectDataRequestId,
      reqMemberName,
      reqReason,
      reqDateTime
    } = currentRow;
    setSelectedItem({
      key: subjectDataRequestId,
      reqUserName: reqMemberName,
      reqReason,
      reqDate: reqDateTime
    });
    showRejectConfirm.setTrue();
  }, []);

  const editApprove = useCallback(
    async () => {
      if (approvalAgree) {
        const isOK = await subjectDataRes.approve({
          subjectDataRequestId: selectedItem.key
        });
        if (isOK) {
          setApprovalAgree(false);
          setSelectedItem(defaultSelectedItem);
          notification.success({
            title: 'Approved',
            onClose: () => {
              refresh();
            }
          });
        }
        showApproveConfirm.setFalse();
      }
    },
    [selectedItem, approvalAgree]
  );

  const editReject = useCallback(
    async () => {
      const isOK = await subjectDataRes.reject({
        subjectDataRequestId: selectedItem.key,
        rejectReason: rejectReason
      });
      if (isOK) {
        notification.success({
          title: 'Rejected',
          onClose: () => {
            refresh();
          }
        });
      }
      showRejectConfirm.setFalse();
    },
    [selectedItem, rejectReason]
  );

  const columns = [
    {
      key: 'reqDateTime',
      name: t('data.request.date'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM)}</div>
      )
    },
    {
      key: 'exportDateTime',
      name: t('common.download'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM)}</div>
      )
    },
    {
      key: 'reqMemberName',
      name: t('request.user')
    },
    {
      key: 'reqDataType',
      name: t('classification'),
      formatter: ({ value, isScrolling, row }) => (
        <div>{ReqType.TDM === value ? 'TDM' : 'All Cohort Data'}</div>
      )
    },
    {
      key: 'reqCondition',
      name: t('data.scope'),
      formatter: ({ value, isScrolling, row }) => (
        <Fragment>
          <Popup
            content={
              <div>
                {value.map(item => {
                  return <div key={item}>{item}</div>;
                })}
              </div>
            }
            trigger={
              <div>
                <Icon name="zoom-in" />
                {value[0]}
              </div>
            }
          />
        </Fragment>
      )
    },
    {
      key: 'reqReason',
      name: t('request.reason'),
      formatter: ({ value, isScrolling, row }) => (
        <Fragment>
          <Popup
            content={<div>{value}</div>}
            trigger={
              <div>
                <Icon name="zoom-in" />
                {value}
              </div>
            }
          />
        </Fragment>
      )
    },
    {
      key: 'isApproved',
      name: t('common.approve'),
      formatter: ({ value, isScrolling, row }) => {
        if (value === true) {
          return <div style={{ color: 'green' }}>{t('common.approved')}</div>;
        } else if (value === false) {
          return (
            <Popup
              content={
                <Fragment>
                  <div style={{ color: 'red' }}>{t('common.rejected')}</div>
                  {row.rejectReason && (
                    <div>
                      {row.rejectReason.split('\n').map((singleText, index) => {
                        return (
                          <span key={index}>
                            {singleText}
                            <br />
                          </span>
                        );
                      })}
                    </div>
                  )}
                </Fragment>
              }
              trigger={
                <div>
                  <Icon name="zoom-in" />
                  <span style={{ color: 'red' }}>{t('common.rejected')}</span>
                </div>
              }
            />
          );
        } else {
          return <Fragment />;
        }
      }
    },
    {
      key: 'approvedDateTime',
      name: t('approve.Date'),
      formatter: ({ value, isScrolling, row }) => {
        if (row.isApproved === null) {
          return (
            <Button.Group size="mini">
              <Button
                positive
                onClick={() => {
                  handleApproveClick(row);
                }}
              >
                {t('common.approve')}
              </Button>
              <Button.Or />
              <Button
                negative
                onClick={() => {
                  handleRejectClick(row);
                }}
              >
                {t('common.reject')}
              </Button>
            </Button.Group>
          );
        } else {
          return (
            <div>{helpers.util.dateformat(value, format.YYYY_MM_DD_HHMM)}</div>
          );
        }
      }
    },
    {
      key: 'approvedMemberName',
      name: t('approver')
    }
  ];

  return (
    <Fragment>
      <DataGrid
        columns={columns}
        rows={items}
        loading={loading}
        minHeight={600}
        rowKey="subjectDataRequestId"
        rowNumber={{ show: true, key: 'no', name: 'No' }}
      />
      <Confirm
        title={t('common.approve')}
        content={
          <ApproveBox
            {...selectedItem}
            checked={approvalAgree}
            onChange={(event, data) => {
              setApprovalAgree(data.checked);
            }}
          />
        }
        open={showApproveConfirm.value}
        disabledOK={approvalAgree === false}
        onOK={editApprove}
        loading={subjectDataRes.loading}
        onCancel={showApproveConfirm.setFalse}
      />
      <Confirm
        title={t('common.reject')}
        content={
          <RejectBox
            {...selectedItem}
            onChange={(event, data) => {
              setRejectReason(data.value);
            }}
          />
        }
        open={showRejectConfirm.value}
        disabledOK={rejectReason.length === 0}
        onOK={editReject}
        loading={subjectDataRes.loading}
        onCancel={showRejectConfirm.setFalse}
      />
    </Fragment>
  );
};

export default ResList;
