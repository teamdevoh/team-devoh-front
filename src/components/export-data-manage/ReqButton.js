import React from 'react';
import { Button } from 'semantic-ui-react';
import { useFormatMessage } from '../../hooks';
import SendingSuccess from '../animation/SendingSuccess';

const ReqButton = ({ onClick, succeded = false }) => {
  const t = useFormatMessage();
  return (
    <Button color="black" onClick={onClick}>
      <span style={{ float: 'left' }}>{t('request')}</span>
      <span
        style={{
          float: 'right',
          marginLeft: '0.5em',
          height: '1.2em',
          width: '1.2em'
        }}
      >
        {succeded && <SendingSuccess play={succeded} />}
      </span>
    </Button>
  );
};

export default ReqButton;
