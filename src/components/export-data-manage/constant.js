export const ReqType = {
  TDM: 'T',
  Cohort: 'C'
};

export const ReqTypeName = {
  T: 'TDM',
  C: 'Cohort'
};

export const Condition = {
  Name: {
    ProjectSite: 'projectSiteId',
    SexCode: 'sexCodeId',
    SmokeCode: 'smokeCodeId',
    Age: 'age',
    BMI: 'bmi',
    Subject: 'subjectId',
    TBDrug: 'tbdrugId'
  },
  Symbol: {
    Item: '|',
    KV: ':',
    FT: '-'
  }
};

export const Color = {
  Pending: 'blue',
  Approve: 'green',
  Reject: 'red'
};
