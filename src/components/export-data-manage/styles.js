import { Card, Checkbox } from 'semantic-ui-react';
import styled from 'styled-components';
import helpers from '../../helpers';

export const TDMCard = styled(Card).attrs({ link: false, raised: false })`
  .header,
  .icon {
    ${props => {
      if (props.selected) {
        return 'color: white !important';
      }
    }};
  }
  ${props => {
    if (props.selected) {
      return `background: linear-gradient(90deg, rgba(231,89,21,1) 0%, rgba(201,129,58,1) 90%) !important`;
    }
    return '';
  }};
  &:hover {
    .header,
    .icon {
      ${props =>
        props.selected === false ? 'color: rgba(231,89,21,1) !important' : ''};
    }
  }
`;

export const CohortCard = styled(Card)`
  .header,
  .icon {
    ${props => {
      if (props.selected) {
        return 'color: white !important';
      }
    }};
  }
  ${props => {
    if (props.selected) {
      return `background: linear-gradient(90deg, rgba(21,90,231,1) 0%, rgba(58,151,201,1) 90%) !important`;
    }
    return '';
  }};
  &:hover {
    .header,
    .icon {
      ${props =>
        props.selected === false ? 'color: rgba(21,90,231,1) !important' : ''};
    }
  }
`;

export const ItemCard = styled(Card)`
  .name {
    font-size: ${helpers.util.isMobile() ? '8px' : '10px'};
    text-align: center;
  }
  &&& {
    cursor: pointer;
  }
  .name,
  .icon {
    ${props => {
      if (props.selected) {
        return 'color: white !important';
      }
    }};
  }
  ${props => {
    if (props.selected) {
      return `background: linear-gradient(90deg, rgba(21,90,231,1) 0%, rgba(58,151,201,1) 90%) !important`;
    }
    return '';
  }};
  &:hover {
    .name,
    .icon {
      ${props =>
        props.selected === false ? 'color: rgba(21,90,231,1) !important' : ''};
    }
  }
`;

export const ApprovalCheckbox = styled(Checkbox)`
  label {
    ${props => {
      return `color: ${props.checked ? 'green !important' : 'red !important'}`;
    }};
  }
`;
