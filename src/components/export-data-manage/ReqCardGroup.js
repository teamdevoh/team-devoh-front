import React from 'react';
import { Card } from 'semantic-ui-react';
import { ReqCard } from '.';
import helpers from '../../helpers';
import { ReqType } from './constant';

const ReqCardGroup = ({ projectId, loading, items }) => {
  const handleMoveToEdit = ({ type, key }) => {
    helpers.history.push(
      `/project/${projectId}/export/req/edit/${key}/${
        type === ReqType.Cohort ? 'cohort' : 'tdm'
      }`
    );
  };

  return (
    <Card.Group>
      {items.map(item => {
        return (
          <ReqCard
            key={item.subjectDataRequestId}
            projectId={projectId}
            {...item}
            onHeaderClick={handleMoveToEdit}
          />
        );
      })}
    </Card.Group>
  );
};

export default ReqCardGroup;
