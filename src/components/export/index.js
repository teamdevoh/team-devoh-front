import { asyncComponent } from '../modules';

const CSVExport = asyncComponent(() =>
  import('./CSVExport')
);

export {
  CSVExport
}