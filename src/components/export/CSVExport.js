import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import helpers from '../../helpers';

const propTypes = {
  /**
   * CSV file name
   */
  fileName: PropTypes.string,
  /**
   * Array of JSON objects.
   */
  data: PropTypes.array.isRequired,
  /**
   * Currently only grid columns are available.
   * Defaults to toplevel JSON attributes.
   */
  fields: PropTypes.array,
  /**
   * Array of Strings, names for the fields at the same indexes.
   * Must be the same length as fields array.
   * (Optional. Maintained for backwards compatibility.
   * Use fields config object for more features)
   */
  fieldNames: PropTypes.arrayOf(PropTypes.string)
};

const defaultProps = {
  fileName: 'data.csv'
};

class CSVExport extends Component {
  constructor(props) {
    super(props);

    this.export = this.export.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.data !== this.props.data;
  }

  export() {
    const { fileName, fields, fieldNames, data } = this.props;

    helpers.util.exportCSV({
      fileName,
      fields,
      fieldNames,
      data
    });
  }

  render() {
    return (
      <Button
        positive
        icon="download"
        content="CSV Export"
        onClick={this.export}
        className={this.props.className}
      />
    );
  }
}

CSVExport.propTypes = propTypes;
CSVExport.defaultProps = defaultProps;

export default CSVExport;
