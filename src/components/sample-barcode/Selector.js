import React from 'react';
import { Select } from 'semantic-ui-react';
import { useProjectSampleList } from '../sample-setting/hooks';
import { useFormatMessage } from '../../hooks';

export const TdmVisitOrder = ({ name, value, onChange, ...rest }) => {
  const t = useFormatMessage();
  const options = [
    {
      text: t('all'),
      value: 0
    },
    {
      text: '1st TDM',
      value: 1
    },
    {
      text: '2nd TDM',
      value: 2
    },
    {
      text: '3rd TDM',
      value: 3
    },
    {
      text: '4th TDM',
      value: 4
    }
  ];

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={options}
      {...rest}
    />
  );
};

export const TdmVisitOrderHasNotSelected = ({
  name,
  subjectId,
  value,
  onChange,
  ...rest
}) => {
  const t = useFormatMessage();
  const options = [
    {
      text: t('common.notSelected'),
      value: null
    },
    {
      text: '1st TDM',
      value: 1
    },
    {
      text: '2nd TDM',
      value: 2
    },
    {
      text: '3rd TDM',
      value: 3
    },
    {
      text: '4th TDM',
      value: 4
    }
  ];

  return (
    <Select
      name={name}
      item
      value={value}
      onChange={onChange}
      options={options}
      {...rest}
    />
  );
};

export const ProjectSamples = ({ name, projectId, value, onChange }) => {
  const t = useFormatMessage();
  const [{ items, loading }] = useProjectSampleList({
    projectId
  });

  const options = items.map(item => {
    const { sampleName, projectSampleId } = item;

    return {
      text: sampleName,
      value: projectSampleId
    };
  });

  return (
    <Select
      name={name}
      item
      loading={loading}
      value={value}
      onChange={onChange}
      options={[
        {
          text: t('all'),
          value: 0
        },
        ...options
      ]}
    />
  );
};
