import { Label } from 'semantic-ui-react';
import styled from 'styled-components';

export const FlexDiv = styled.div`
  display: flex;
`;

export const RightButtonWrap = styled.div`
  margin-left: auto;
  display: flex;
  & button:last-child {
    margin-right: 0;
  }
`;

export const StyledDetail = styled(Label.Detail)`
  &&& {
    color: #db2828;
  }
`;
