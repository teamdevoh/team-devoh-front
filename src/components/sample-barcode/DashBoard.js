import React, { useState, useCallback } from 'react';
import Board from './Board';
import DashBoardHeader from './DashBoardHeader';
import DashBoardTool from './DashBoardTool';
import Title from './Title';
import { Grid, Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';
import { useSampleBarcodeList } from './hooks';

const DashBoard = () => {
  const { projectId } = useParams();
  const [filter, setFilter] = useState({
    projectId: null,
    siteId: 0,
    projectSiteId: 0,
    subjectIds: null,
    visitOrder: 0,
    projectSampleId: 0,
    barcode: ''
  });

  const [
    { items, loading, columns, exportCSV },
    { setItems }
  ] = useSampleBarcodeList({
    curProjectId: projectId,
    ...filter
  });

  const [selectedIndexes, setSelectedIndexes] = useState([]);

  const handleFilterChange = useCallback(
    (event, { name, value }) => {
      let newFilter = { ...filter, [name]: value };

      if (name === 'projectId') {
        newFilter = {
          ...newFilter,
          projectSiteId: 0,
          subjectIds: null,
          projectSampleId: 0
        };
      }

      if (name === 'projectSiteId') {
        newFilter = {
          ...newFilter,
          subjectIds: null
        };
      }

      setFilter({ ...newFilter });
    },
    [filter]
  );

  const handleRowsSelected = rows => {
    setSelectedIndexes(selectedIndexes.concat(rows.map(r => r.rowIdx)));
  };

  const handleRowsDeselected = rows => {
    let rowIndexes = rows.map(r => r.rowIdx);
    setSelectedIndexes(
      selectedIndexes.filter(i => rowIndexes.indexOf(i) === -1)
    );
  };

  return (
    <Segment basic>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Title />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid.Row>
        <Grid.Column>
          <DashBoardHeader
            projectId={projectId}
            onFilterChange={handleFilterChange}
            filter={filter}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row style={{ marginTop: '20px' }}>
        <Grid.Column width="16">
          <DashBoardTool
            projectId={projectId}
            items={items}
            setItems={setItems}
            loading={loading}
            selectedIndexes={selectedIndexes}
            exportCSV={exportCSV}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Board
            columns={columns}
            items={items}
            loading={loading}
            onRowsSelected={handleRowsSelected}
            onRowsDeselected={handleRowsDeselected}
            selectedIndexes={selectedIndexes}
          />
        </Grid.Column>
      </Grid.Row>
    </Segment>
  );
};

export default DashBoard;
