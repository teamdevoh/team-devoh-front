import React from 'react';
import { Header, Grid, Container } from 'semantic-ui-react';
import styled from 'styled-components';
import { Modal } from '../modal';
import { useFormatMessage } from '../../hooks';
import CopyToClipboard from '../modules/CopyToClipboard';

const StyledContents = styled.div`
  margin-top: 10px;
`;

const StyledColumn = styled(Grid.Column)`
  &&& {
    & > span {
      font-size: 1em !important;
    }
  }
`;

const BarcodePrintInfoModal = ({
  open,
  onOK,
  title,
  printableCount,
  targetUrl
}) => {
  const t = useFormatMessage();

  return (
    <Modal
      size="large"
      open={open}
      onOK={onOK}
      title={title}
      content={
        <Container>
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Header as="h3">
                  {!!printableCount &&
                    t('sample.barcode.print.info', {
                      printCount: printableCount
                    })}{' '}
                  {t('sample.barcode.print.address.info')}
                </Header>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <StyledContents>{targetUrl}</StyledContents>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <StyledColumn>
                <CopyToClipboard
                  title={t('common.copy.that.url.clipboard')}
                  text={targetUrl}
                />
              </StyledColumn>
            </Grid.Row>
          </Grid>
        </Container>
      }
    />
  );
};

export default BarcodePrintInfoModal;
