import React, { Fragment, useState } from 'react';
import { Label, Button } from 'semantic-ui-react';
import { notification } from '../modal';
import { useFormatMessage, useBoolean } from '../../hooks';
import api from './api';
import helpers from '../../helpers';
import { useBarcodePrint } from './hooks';
import BarcodePrintInfoModal from './BarcodePrintInfoModal';
import { FlexDiv, RightButtonWrap, StyledDetail } from './styles';
import { format } from '../../constants';
import { StyledCSVExport } from '../export-tdm/styles';

const DashBoardTool = ({
  projectId,
  items,
  setItems,
  selectedIndexes,
  exportCSV
}) => {
  const t = useFormatMessage();
  const showBarcodePrintinfo = useBoolean(false);
  const [printableCount, setPrintableCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [{ loading: printLoading, onPrintBarcode }] = useBarcodePrint({
    projectId
  });
  const [csvLoading, setCsvLoading] = useState(false);

  const handleCreateSampleIdClick = async () => {
    setLoading(true);
    const creatableItems = items.filter((item, index) => {
      const { barcode } = item;
      return selectedIndexes.includes(index) && !barcode;
    });

    if (creatableItems.length === 0) {
      notification.info({
        title: t('sample.barcode.already.created'),
        confirmButtonName: t('common.ok'),
        onClose: () => {
          setLoading(false);
        }
      });
    } else {
      const model = creatableItems.map(item => {
        const {
          projectId,
          subjectId,
          subjectVisitId,
          subjectTDMSamplingId
        } = item;

        return {
          projectId,
          subjectId,
          subjectVisitId,
          subjectTDMSamplingId
        };
      });

      const res = await api.addSampleBarcodes({ projectId, model });

      if (res && res.data) {
        notification.success({
          title: t('project.task.completed'),
          onClose: () => {}
        });

        const newItems = items.map(item => {
          const { subjectTDMSamplingId } = item;
          const newItem = {
            ...item
          };

          if (typeof res.data[subjectTDMSamplingId] !== 'undefined') {
            newItem['sampleId'] = res.data[subjectTDMSamplingId].sampleId;
            newItem['barcode'] = res.data[subjectTDMSamplingId].barcode;
            newItem['memberName'] = null;
          }

          return {
            ...newItem
          };
        });
        setItems(newItems);
      }
      setLoading(false);
    }
  };

  const handlePrintBarcodeClick = async () => {
    const printBarcodes = items.filter((item, index) => {
      const { barcode } = item;
      return selectedIndexes.includes(index) && barcode;
    });

    if (printBarcodes.length === 0) {
      notification.info({
        title: t('sample.barcode.none'),
        confirmButtonName: t('common.ok'),
        onClose: () => {}
      });
    } else {
      const model = printBarcodes.map(item => {
        const {
          // irbNo,
          site,
          subjectNo,
          initial,
          visit,
          material,
          sample,
          scheduledDateTime,
          scheduledDate,
          scheduledTime,
          samplingDateTime,
          samplingDate,
          samplingTime,
          barcode,
          brthdtc
        } = item;

        return {
          irbNo: site,
          subjectNo,
          initial,
          visitName: visit,
          analyte: '',
          material,
          barcodeName: sample,
          scheduledDateTime: samplingDateTime || scheduledDateTime,
          scheduledDate: samplingDate || scheduledDate,
          scheduledTime: samplingTime || scheduledTime,
          actualDateTime: samplingDateTime,
          actualDate: samplingDate,
          actualTime: samplingTime,
          barcode,
          dob: helpers.util.dateformat(brthdtc, format.YYYY_MM_DD)
        };
      });

      const data = await onPrintBarcode(model);

      if (data > 0) {
        showBarcodePrintinfo.setTrue();
        setPrintableCount(data);
      }
    }
  };

  const handleRemoveClick = () => {
    const removeBarcodes = items.filter((item, index) => {
      const { barcode } = item;
      return selectedIndexes.includes(index) && barcode;
    });

    if (removeBarcodes.length === 0) {
      notification.info({
        title: t('sample.barcode.none'),
        confirmButtonName: t('common.ok'),
        onClose: () => {
          setLoading(false);
        }
      });
    } else {
      notification.confirm({
        title: t('delete.message'),
        content: `${t('sample.barcode.remove.disabled.info')}`,
        onOK: async () => {
          const model = removeBarcodes.map(item => {
            const { sampleId, subjectTDMSamplingId } = item;

            return {
              sampleId,
              subjectTDMSamplingId
            };
          });

          const res = await api.removeSampleBarcodes({
            projectId,
            model
          });

          if (res && res.data) {
            if (res.data.length === 0) {
              notification.info({
                title: t('sample.barcode.remove.rejected.info'),
                confirmButtonName: t('common.ok'),
                onClose: () => {
                  setLoading(false);
                }
              });
            } else {
              notification.success({
                title: t('project.task.completed'),
                onClose: () => {
                  setLoading(false);
                }
              });

              const newItems = items.map(item => {
                const { sampleId } = item;
                const newItem = {
                  ...item
                };

                if (res.data.includes(sampleId)) {
                  newItem['barcode'] = null;
                }

                return {
                  ...newItem
                };
              });
              setItems(newItems);
            }
          }
        }
      });
    }
  };

  const handleExport = async () => {
    setCsvLoading(true);
    try {
      await exportCSV('Sample List');
    } catch (err) {
      console.log(err);
    }
    setCsvLoading(false);
  };

  return (
    <Fragment>
      <FlexDiv>
        <Label size="big">
          Sample Count
          <StyledDetail>{items.length}</StyledDetail>
        </Label>
        <RightButtonWrap>
          <Button
            disabled={selectedIndexes.length === 0}
            onClick={handleCreateSampleIdClick}
            loading={loading || printLoading}
          >
            Create Sample ID
          </Button>
          <Button
            disabled={selectedIndexes.length === 0}
            onClick={handlePrintBarcodeClick}
            loading={loading || printLoading}
          >
            Print Barcode
          </Button>
          <Button
            disabled={selectedIndexes.length === 0}
            onClick={handleRemoveClick}
            loading={loading || printLoading}
          >
            Remove Sample ID
          </Button>
          <StyledCSVExport
            positive
            icon="download"
            loading={csvLoading}
            onClick={handleExport}
            content="CSV Export"
          />
        </RightButtonWrap>
      </FlexDiv>
      <BarcodePrintInfoModal
        open={showBarcodePrintinfo.value}
        onOK={showBarcodePrintinfo.setFalse}
        title={'Print Sample Barcode'}
        printableCount={printableCount}
        targetUrl={`http://oz.econsert.com/ozrviewer/ozreport_c.html?api=SampleManager&action=GetStudyLabelPrintByLocalIP&doc=SampleManagement/SampleLabel&param=studyID:0`}
      />
    </Fragment>
  );
};

export default DashBoardTool;
