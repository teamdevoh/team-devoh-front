import { useCallback, useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';

const useSampleBarcodeList = ({
  curProjectId,
  projectId,
  projectSiteId,
  subjectIds,
  visitOrder,
  projectSampleId,
  barcode
}) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchItems = useCallback(async () => {
    setLoading(true);
    const res = await api.fetchSampleBarcodes({
      curProjectId,
      projectId,
      projectSiteId,
      subjectIds: subjectIds && subjectIds.join(','),
      visitOrder,
      projectSampleId,
      barcode
    });

    setLoading(false);
    if (res && res.data) {
      return res.data;
    } else {
      return [];
    }
  });

  useEffect(
    () => {
      if (!!projectId) {
        // project 기본 선택되어 있어야함

        const fetch = async () => {
          const item = await fetchItems();

          setItems(item);
        };
        fetch();
      }
    },
    [
      curProjectId,
      projectId,
      projectSiteId,
      subjectIds,
      visitOrder,
      projectSampleId,
      barcode
    ]
  );

  const columns = [
    {
      key: 'site',
      name: 'Site'
    },
    {
      key: 'subjectNo',
      name: 'Subject',
      width: 130,
      formatter: ({ value, row }) => {
        const { initial } = row;
        return value + (!!initial ? `/${initial}` : '');
      }
    },
    {
      key: 'visit',
      name: 'Visit',
      width: 130
    },
    {
      key: 'scheduledDateTime',
      name: 'Scheduled Date',
      width: 140
    },
    {
      key: 'sample',
      name: 'Sample',
      width: 140
    },
    {
      key: 'barcode',
      name: 'Sample ID'
    },
    {
      key: 'originBarcode',
      name: 'Origin Barcode'
    },
    {
      key: 'samplingDateTime',
      name: 'Sampling'
    },
    {
      key: 'memberName',
      name: 'User',
      width: 150
    }
  ];

  const exportCSV = useCallback(
    async fileName => {
      const items = await fetchItems();
      if (items) {
        items.forEach(item => {
          const { subjectNo, initial, memberName } = item;
          item.subjectNo = subjectNo + (!!initial ? `/${initial}` : '');
          item.memberName = !!memberName ? memberName.replaceAll(',', ' ') : '';
        });

        const fields = columns.map(item => ({ key: item.key }));
        const fieldNames = columns.map(item => item.name);

        helpers.util.exportCSV({
          fileName: `${fileName}.csv`,
          data: items,
          fields,
          fieldNames
        });

        return items;
      }
      return [];
    },
    [
      curProjectId,
      projectId,
      projectSiteId,
      subjectIds,
      visitOrder,
      projectSampleId,
      barcode
    ]
  );

  return [{ items, loading, columns, exportCSV }, { setItems }];
};

export default useSampleBarcodeList;
