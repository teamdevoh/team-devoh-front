import helpers from '../../helpers';

const fetchSampleBarcodes = ({
  curProjectId,
  projectId,
  projectSiteId,
  subjectIds,
  visitOrder,
  projectSampleId,
  barcode
}) => {
  const params = helpers.qs.stringify({
    projectId,
    projectSiteId,
    subjectIds,
    visitOrder,
    projectSampleId,
    barcode
  });

  return helpers.Service.get(
    `api/projects/${curProjectId}/samples/barcodes?${params}`
  );
};

const addSampleBarcodes = ({ projectId, model }) => {
  return helpers.Service.post(
    `api/project/${projectId}/samples/barcodes`,
    model
  );
};

const addSampleBarcodesPrint = ({ projectId, model }) => {
  return helpers.Service.post(
    `api/project/${projectId}/samples/barcodes/print`,
    model
  );
};

const removeSampleBarcodes = ({ projectId, model }) => {
  return helpers.Service.delete(`api/project/${projectId}/samples/barcodes`, {
    data: model
  });
};

export default {
  fetchSampleBarcodes,
  addSampleBarcodes,
  addSampleBarcodesPrint,
  removeSampleBarcodes
};
