import React from 'react';
import { Header } from 'semantic-ui-react';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';

const Title = () => {
  return (
    <Header as="h3" dividing>
      Sample Barcode List
      <Header.Subheader>Collection Description</Header.Subheader>
      <CopyRedirectUrl />
    </Header>
  );
};

export default Title;
