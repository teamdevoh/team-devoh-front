import React from 'react';
import { DataGrid } from '../data-grid';

const Board = ({
  columns,
  items,
  loading,
  selectedIndexes,
  onRowsSelected,
  onRowsDeselected
}) => {
  return (
    <DataGrid
      columns={columns}
      rows={items}
      rowNumber={{ show: true, key: 'no', name: 'No' }}
      rowKey="subjectTDMSamplingId"
      minHeight={550}
      loading={loading}
      rowSelection={{
        showCheckbox: true,
        enableShiftSelect: true,
        onRowsSelected,
        onRowsDeselected,
        selectBy: {
          indexes: selectedIndexes
        }
      }}
    />
  );
};

export default Board;
