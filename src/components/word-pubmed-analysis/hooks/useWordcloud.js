import { useCallback, useEffect, useState } from 'react';

const useWordcloud = () => {
  const [items, setItems] = useState([]);

  const getItems = useCallback(async () => {
    const res = await fetch(`https://wordcloud.cpmtb.kr/wordcloud`);
    const data = await res.json();

    let newItems = [];
    data.forEach(word => {
      if (word.key !== 'tuberculosis' && word.value >= 10) {
        newItems.push({ text: word.key, value: word.value });
      }
    });
    setItems(newItems);
  });

  useEffect(() => {
    getItems();
  }, []);

  return { items };
};

export default useWordcloud;
