import React, { Fragment, useCallback, useState } from 'react';
import { Header, Form } from 'semantic-ui-react';
import { IntegerInput } from '../input';

const WordNetwork = () => {
  const [model, setModel] = useState({
    nodeSize: 10,
    weight: 10
  });

  const onChange = useCallback(
    (event, data) => {
      setModel({ ...model, [data.name]: data.value });
    },
    [model]
  );

  return (
    <Fragment>
      <Header dividing>
        National Library of Medicine, Tuberculosis Keyword, Period 1 year, Word
        Network
      </Header>
      <Form>
        <Form.Group>
          <Form.Field width="three">
            <label>Node Size</label>
            <IntegerInput
              name="nodeSize"
              placeholder={''}
              value={model.nodeSize}
              onChange={onChange}
            />
          </Form.Field>
          <Form.Field width="three">
            <label>Weight</label>
            <IntegerInput
              name="weight"
              placeholder={''}
              value={model.weight}
              onChange={onChange}
            />
          </Form.Field>
        </Form.Group>
      </Form>
      <div>
        <iframe
          title="wordnet"
          src={`https://wordcloud.cpmtb.kr/wordnet.html?nodesize=${
            model.nodeSize ? model.nodeSize : 10
          }&weight=${(model.weight ? model.weight : 10) / 10000}`}
          style={{ display: 'block', width: '100vw', height: '100vh' }}
        />
      </div>
    </Fragment>
  );
};

export default WordNetwork;
