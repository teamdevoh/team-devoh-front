import React from 'react';
import ReactWordcloud from 'react-wordcloud';
import useWordcloud from './hooks/useWordcloud';
import { select } from 'd3-selection';
import { Fragment } from 'react';
import { Header } from 'semantic-ui-react';

/** Helper Page
 * https://react-wordcloud.netlify.app
 */
const WordCloud = () => {
  const wordcloud = useWordcloud();

  const getCallback = callback => {
    return function(word, event) {
      const element = event.target;
      const text = select(element);
      const mainKeyword = 'tuberculosis';
      text.on('click', () => {
        window.open(
          `https://pubmed.ncbi.nlm.nih.gov/?term=(${mainKeyword})+AND+(${
            word.text
          })&sort=date&filter=datesearch.y_1&filter=datesearch.y_1`,
          '_blank'
        );
      });
    };
  };

  const callbacks = {
    getWordColor: word => 'white',
    onWordClick: getCallback('onWordClick'),
    onWordMouseOut: getCallback('onWordMouseOut'),
    onWordMouseOver: getCallback('onWordMouseOver')
  };

  const options = {
    deterministic: false,
    enableTooltip: false,
    rotations: 0,
    rotationAngles: [-350, -300],
    fontSizes: [9, 95],
    transitionDuration: 1000
  };

  return (
    <Fragment>
      <Header dividing>
        National Library of Medicine, Tuberculosis Keyword, Period 1 year, Word
        Cloud
      </Header>
      <div style={{ height: '100vh', width: '100%', backgroundColor: 'black' }}>
        <ReactWordcloud
          maxWords={wordcloud.items.length}
          callbacks={callbacks}
          options={options}
          words={wordcloud.items}
        />
      </div>
    </Fragment>
  );
};

export default WordCloud;
