import { asyncComponent } from '../modules';

const Pagination = asyncComponent(() =>
  import('./Pagination')
);

export { Pagination }