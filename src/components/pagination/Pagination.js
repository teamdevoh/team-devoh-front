import React, { Component } from 'react';
import { Responsive, Pagination as SMPagination } from 'semantic-ui-react';
import parseInt from 'lodash/parseInt';

class Pagination extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activePage: 1
    };

    this.getTotalPages = this.getTotalPages.bind(this);
    this.handlePaginationChange = this.handlePaginationChange.bind(this);
  }

  handlePaginationChange(e, { activePage }) {
    this.setState({ activePage });

    const offset = (activePage - 1) * this.props.rowsPerPage;
    const limit = this.props.rowsPerPage;

    this.props.onPageChange({ offset, limit });
  }

  getTotalPages() {
    const { totalRows, rowsPerPage } = this.props;

    return parseInt(
      totalRows / rowsPerPage + (totalRows % rowsPerPage === 0 ? 0 : 1)
    );
  }

  render() {
    const { activePage } = this.state;
    const totalPages = this.getTotalPages();

    return (
      <div>
        <Responsive minWidth={415}>
          <SMPagination
            activePage={activePage}
            onPageChange={this.handlePaginationChange}
            totalPages={totalPages}
            ellipsisItem={false}
          />
        </Responsive>
        <Responsive maxWidth={414}>
          <SMPagination
            activePage={activePage}
            onPageChange={this.handlePaginationChange}
            boundaryRange={0}
            siblingRange={0}
            totalPages={totalPages}
            ellipsisItem={false}
            firstItem={null}
            lastItem={null}
          />
        </Responsive>
      </div>
    );
  }
}

export default Pagination;
