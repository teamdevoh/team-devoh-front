import React from 'react';
import { Placeholder } from 'semantic-ui-react';
import { asyncComponent } from '../modules';

const CommentItem = asyncComponent(() =>
  import('./CommentItem')
);
const TaskNotifyContainer = asyncComponent(() =>
  import('./TaskNotifyContainer')
);
const TaskTransition = asyncComponent(() =>
  import('./TaskTransition')
);
const TaskTimelineList = asyncComponent(() =>
  import('./TaskTimelineList')
);
const TaskListContainer = asyncComponent(() =>
  import('./TaskListContainer')
);
const TaskConversation = asyncComponent(() =>
  import('./TaskConversation')
);
const TaskAudit = asyncComponent(() =>
  import('./TaskAudit')
);
const AssignedList = asyncComponent(() =>
  import('./AssignedList'), <div className="ui input" />
);
const TaskTagList = asyncComponent(() =>
  import('./TaskTagList')
);
const Reply = asyncComponent(() =>
  import('./Reply')
);
const TemplateTaskList = asyncComponent(() =>
  import('./TemplateTaskList')
);
const TaskSortable = asyncComponent(() =>
  import('./TaskSortable')
);
const TaskTodoList = asyncComponent(() =>
  import('./TaskTodoList')
);
const TaskTodoListItem = asyncComponent(() =>
  import('./TaskTodoListItem')
);

export {
  CommentItem,
  TaskAudit,
  AssignedList,
  TaskNotifyContainer,
  TaskTimelineList,
  TaskListContainer,
  TaskConversation,
  TaskTagList,
  Reply,
  TemplateTaskList,
  TaskSortable,
  TaskTransition,
  TaskTodoList,
  TaskTodoListItem
};
export { default as api } from './api';

export { default as TaskAdd } from './TaskAdd';
export { default as TaskEdit } from './TaskEdit';
export { default as TaskForm } from './TaskForm';
export { default as TaskTree } from './TaskTree';
export { default as TaskCheckList } from './TaskCheckList';
export { default as TaskList } from './TaskList';
export { default as TaskEditLayer } from './TaskEditLayer';

