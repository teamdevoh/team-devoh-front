import React, { useState, useCallback, useEffect, Fragment } from 'react';
import api from './api';
import update from 'react-addons-update';
import notification from '../modal/notification';
import TaskCheckListItem from './TaskCheckListItem';
import { Accordion, Icon, Form, Input } from 'semantic-ui-react';
import { useFormatMessage, useBoolean } from '../../hooks';
import { RoleAware } from '../auth';
import filter from 'lodash/filter';
import findIndex from 'lodash/findIndex';
import map from 'lodash/map';
import { role } from '../../constants';

const TaskCheckList = ({ name, taskItemId, onChange }) => {
  const t = useFormatMessage();
  const [items, setItems] = useState([]);
  const [isRemoving, setIsRemoving] = useState(false);

  let isSubscribed = false;
  useEffect(() => {
    isSubscribed = true;
    fetchItems();
    return () => {
      isSubscribed = false;
    };
  }, []);

  const fetchItems = useCallback(async () => {
    const res = await api.fetchTaskCheckItems({ taskItemId });
    if (isSubscribed && res.data) {
      setItems(res.data);
    }
  }, []);

  const handleDeleteClick = useCallback(async taskCheckItemId => {
    setIsRemoving(true);
    const res = await api.removeTaskCheckItem({
      taskItemId,
      taskCheckItemId
    });
    if (res.data) {
      notification.success({
        title: t('common.alert.deleted'),
        onClose: () => {
          setItems(
            filter(items, item => {
              return item.taskCheckItemId !== taskCheckItemId;
            })
          );
        }
      });
    }
    setIsRemoving(false);
  });

  const handleCheckItem = useCallback((event, data) => {
    const foundIndex = findIndex(items, { taskCheckItemId: data.value });

    const updated = update(items, {
      [foundIndex]: {
        isComplete: { $set: data.checked }
      }
    });
    setItems(updated);
    onChange(event, { name: data.name, value: updated });
  });

  const appendItem = ({ id, title }) => {
    const merged = items.concat({
      taskCheckItemId: id,
      taskItemId: taskItemId,
      title: title,
      isComplete: false
    });
    setItems(merged);
  };

  return (
    <Fragment>
      {map(items, (item, index) => {
        return (
          <TaskCheckListItem
            key={item.taskCheckItemId}
            id={item.taskCheckItemId}
            name={name}
            title={item.title}
            onChange={handleCheckItem}
            checked={item.isComplete}
            buttonName={t('task.delete.task.checkitem', { name: item.title })}
            isRemoving={isRemoving}
            onDeleteClick={handleDeleteClick}
          />
        );
      })}
      <RoleAware allowedRole={`${role.Task_Edit},${role.ConsoleTask_Edit}`}>
        <AddCheckItem taskItemId={taskItemId} onSaved={appendItem} />
      </RoleAware>
    </Fragment>
  );
};

const AddCheckItem = ({ taskItemId, onSaved }) => {
  const t = useFormatMessage();
  const visible = useBoolean(false);
  const [title, setTitle] = useState('');
  const [loading, setLoading] = useState(false);

  const handleChange = useCallback((event, { name, value }) => {
    setTitle(value);
  });

  const addCheckItem = useCallback(
    async () => {
      if (title.trim()) {
        setLoading(true);
        setTitle('');
        const res = await api.addTaskCheckItem({ taskItemId, title });
        if (res && res.data > 0) {
          onSaved({ id: res.data, title });
        }
        setLoading(false);
      }
    },
    [title]
  );

  const handleKeyDown = useCallback(
    event => {
      if (event.keyCode === 13) {
        event.preventDefault();
        addCheckItem();
        return false;
      } else {
        return true;
      }
    },
    [title]
  );

  return (
    <Accordion>
      <Accordion.Title
        active={visible.value}
        index={0}
        onClick={visible.toggle}
      >
        <Icon name="dropdown" />
        {t('task.checklist.add')}
      </Accordion.Title>
      <Accordion.Content active={visible.value}>
        <Form.Group>
          <Form.Field width="sixteen">
            <Input
              fluid
              name="title"
              loading={loading}
              disabled={loading}
              placeholder={t('comment.enterkey')}
              value={title}
              onChange={handleChange}
              onKeyDown={handleKeyDown}
            />
          </Form.Field>
        </Form.Group>
      </Accordion.Content>
    </Accordion>
  );
};

export default TaskCheckList;
