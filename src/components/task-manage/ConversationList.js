import React, { useCallback, memo } from 'react';
import { Comment, Header } from 'semantic-ui-react';
import { Reply } from './';
import ConversationListItem from './ConversationListItem';
import { useFormatMessage } from '../../hooks';
import map from 'lodash/map';

const ConversationList = ({
  items,
  onReplyAdd,
  onReplyUpdate,
  onReplyRemove
}) => {
  const t = useFormatMessage();

  const handleReplyAdd = useCallback((value, parentId) => {
    onReplyAdd(value, parentId);
  });

  const handleParentCommentAdd = useCallback(value => {
    onReplyAdd(value, 0);
  });

  const handleReplyUpdate = useCallback((value, id) => {
    onReplyUpdate(value, id);
  });

  return (
    <div>
      <Comment.Group threaded>
        <Header as="h3" dividing>
          {t('comment.title')}
        </Header>
        <Reply onClick={handleParentCommentAdd} />

        {map(items, item => {
          return (
            <ConversationListItem
              key={item.id}
              id={item.id}
              memberName={item.memberName}
              memberId={item.memberId}
              fileStorageId={item.fileStorageId}
              date={item.date}
              comment={item.comment}
              children={item.children}
              onReplyRemove={onReplyRemove}
              onReplyUpdate={handleReplyUpdate}
              onReplyAdd={handleReplyAdd}
            />
          );
        })}
      </Comment.Group>
    </div>
  );
};

export default memo(ConversationList);
