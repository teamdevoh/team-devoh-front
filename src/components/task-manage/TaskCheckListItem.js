import React from 'react';
import { Checkbox, Form, Popup, Icon } from 'semantic-ui-react';
import { RemoveButton } from '../button';
import { role } from '../../constants';

const TaskCheckListItem = ({
  id,
  name,
  buttonName,
  title,
  onChange,
  checked,
  isRemoving,
  onDeleteClick
}) => {
  const handleDeleteClick = event => {
    onDeleteClick(id);
  };

  return (
    <Form.Field>
      <Checkbox
        label={title}
        name={name}
        value={id}
        onChange={onChange}
        checked={checked}
      />
      <Popup
        size="huge"
        trigger={<Icon name="close" />}
        disabled={isRemoving}
        className="link"
        content={
          <RemoveButton
            allowedRole={`${role.Task_Edit},${role.ConsoleTask_Edit}`}
            name={buttonName}
            onClick={handleDeleteClick}
            loading={isRemoving}
          />
        }
        on="click"
        position="top right"
      />
    </Form.Field>
  );
};

export default TaskCheckListItem;
