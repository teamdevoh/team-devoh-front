import React, { memo, useCallback } from 'react';
import { List, Message } from 'semantic-ui-react';
import { TaskTodoListItem } from './';
import { Anchor } from '../button';
import { useFormatMessage } from '../../hooks';
import helpers from '../../helpers';
import map from 'lodash/map';

const TaskEmpty = ({ projectId }) => {
  const t = useFormatMessage();

  const handleClick = useCallback(() => {
    helpers.history.push(`/project/${projectId}/task/portal/all`);
  }, []);

  return (
    <Message info>
      <Message.Header>{t('project.my.tasks.empty')}</Message.Header>
      <Anchor onClick={handleClick} text={t('project.my.tasks.empty.link')} />
    </Message>
  );
};

const TaskTodoList = ({ projectId, tasks, onItemClick, children }) => {
  return (
    <List celled>
      {map(tasks, task => {
        return (
          <TaskTodoListItem
            key={task.taskItemId}
            id={task.taskItemId}
            title={task.title}
            status={task.status}
            scheduleFromDate={task.scheduleFromDate}
            scheduleToDate={task.scheduleToDate}
            rate={task.rate}
            onItemClick={onItemClick}
          />
        );
      })}
      {tasks.length === 0 && <TaskEmpty projectId={projectId} />}
    </List>
  );
};

export default memo(TaskTodoList);
