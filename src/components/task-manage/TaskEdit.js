import React, { useCallback, memo, Fragment } from 'react';
import { RemoveButton, SaveButton, LinkButton } from '../button';
import { Confirm } from '../modal';
import { Checkbox, Grid, Form, Header, Icon } from 'semantic-ui-react';
import { RoleAware } from '../auth';
import { TaskForm, TaskConversation, TaskAudit } from './';
import TaskUpload from './TaskUpload';
import { useFormatMessage, useBoolean } from '../../hooks';
import { useTaskEdit } from './hooks';
import { CompleteSegment, Holder } from './styles';
import { role } from '../../constants';

const CompleteMark = memo(({ isComplete }) => {
  const t = useFormatMessage();

  return (
    <Fragment>
      {isComplete && (
        <CompleteSegment raised>
          <Header as="h2">
            <Icon.Group size="big">
              <Icon name="check circle" color="green" />
            </Icon.Group>
            {t('project.task.completed')}
          </Header>
        </CompleteSegment>
      )}
    </Fragment>
  );
});

const UploadWrapper = memo(({ projectId, taskItemId }) => {
  const t = useFormatMessage();
  const isOpen = useBoolean(false);

  return (
    <RoleAware allowedRole={`${role.TaskAttach_Read},${role.ConsoleTask_Read}`}>
      <Grid.Row>
        <Grid.Column>
          <Fragment>
            <LinkButton
              color="twitter"
              icon="cloud upload"
              labelPosition="right"
              name={t('common.add.attachment')}
              onClick={isOpen.setTrue}
            />
            <TaskUpload
              isUpload={isOpen.value}
              onUploadClose={isOpen.setFalse}
              projectId={projectId}
              taskItemId={taskItemId}
            />
          </Fragment>
        </Grid.Column>
      </Grid.Row>
    </RoleAware>
  );
});

const CommentOrAudit = memo(({ taskItemId }) => {
  const t = useFormatMessage();
  const isAudit = useBoolean(false);
  return (
    <Fragment>
      <Grid.Row>
        <Grid.Column>
          <Form>
            <Form.Group>
              <Form.Field>
                <Checkbox
                  radio
                  label={t('comment.title')}
                  name="checkboxRadioGroup"
                  checked={!isAudit.value}
                  onChange={isAudit.setFalse}
                />
              </Form.Field>
              <Form.Field>
                <Checkbox
                  radio
                  label={t('task.audit')}
                  name="checkboxRadioGroup"
                  checked={isAudit.value}
                  onChange={isAudit.setTrue}
                />
              </Form.Field>
            </Form.Group>
          </Form>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <div>
            {!isAudit.value && (
              <RoleAware
                allowedRole={`${role.TaskConversation_Read},${
                  role.ConsoleTask_Read
                }`}
              >
                <TaskConversation taskItemId={taskItemId} />
              </RoleAware>
            )}
            {isAudit.value && <TaskAudit taskItemId={taskItemId} />}
          </div>
        </Grid.Column>
      </Grid.Row>
    </Fragment>
  );
});

const TaskEdit = ({ projectId, taskItemId, onSaved, onRemoved }) => {
  const t = useFormatMessage();
  const [task] = useTaskEdit(projectId, taskItemId);
  const isRemove = useBoolean(false);

  const handleSubmit = async () => {
    const is = await task.onEdit();
    if (is) {
      onSaved(task.model);
    }
  };

  const handleRemove = useCallback(async () => {
    const is = await task.onRemove();
    if (is) {
      onRemoved();
    }
  }, []);

  return (
    <Grid stackable>
      <Grid.Row>
        <Grid.Column width={16}>
          <CompleteMark isComplete={task.model.sectionCodeId === 3} />
          {task.isFetch && (
            <TaskForm
              projectId={projectId}
              taskItemId={taskItemId}
              loaded={task.loading}
              model={task.model}
              onChange={task.onChange}
              onSubmit={handleSubmit}
            >
              <SaveButton
                allowedRole={`${role.Task_Edit},${role.ConsoleTask_Edit}`}
                name={t('common.save')}
                loading={task.loading}
              />
              <RemoveButton
                allowedRole={`${role.Task_Edit},${role.ConsoleTask_Edit}`}
                name={t('common.remove')}
                onClick={isRemove.setTrue}
                disabled={!taskItemId}
              />
              <Confirm
                title={t('task.delete.title')}
                content={t('task.delete')}
                open={isRemove.value}
                onOK={handleRemove}
                onCancel={isRemove.setFalse}
                loading={task.loading}
              />
            </TaskForm>
          )}
          {!task.isFetch && <Holder fluid />}
        </Grid.Column>
      </Grid.Row>
      <UploadWrapper projectId={projectId} taskItemId={taskItemId} />
      <CommentOrAudit taskItemId={taskItemId} />
    </Grid>
  );
};

export default memo(TaskEdit);
