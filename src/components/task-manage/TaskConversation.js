import React, { Fragment, useState, memo, useCallback } from 'react';
import { Confirm, notification } from '../modal';
import ConversationList from './ConversationList';
import { Loader, Dimmer } from 'semantic-ui-react';
import { useTaskConversationList, useTaskConversation } from './hooks/';
import { useFormatMessage, useBoolean } from '../../hooks';

const TaskConversation = memo(({ taskItemId }) => {
  const t = useFormatMessage();
  const isRemove = useBoolean(false);
  const [id, setId] = useState(0);
  const [conversation] = useTaskConversationList(taskItemId);
  const [conversationEdit] = useTaskConversation(taskItemId);

  const handleAdd = useCallback(async (value, parentId) => {
    const is = await conversationEdit.onAdd(parentId, value);
    if (is) {
      notification.success({
        title: t('common.alert.saved'),
        onClose: () => {
          conversation.refresh();
        }
      });
    }
  }, []);

  const handleUpdate = useCallback(async (value, id) => {
    const is = await conversationEdit.onEdit(value, id);
    if (is) {
      notification.success({
        title: t('common.alert.saved'),
        onClose: () => {
          conversation.refresh();
        }
      });
    }
  }, []);

  const handleRemove = useCallback(
    async () => {
      const is = await conversationEdit.onRemove(id);
      if (is) {
        notification.success({
          title: t('common.alert.deleted'),
          onClose: () => {
            conversation.refresh();
            isRemove.setFalse();
            setId(0);
          }
        });
      }
    },
    [id]
  );

  const showConfirm = id => {
    setId(id);
    isRemove.setTrue();
  };

  return (
    <Fragment>
      <ConversationList
        items={conversation.items}
        isLoading={conversation.loading}
        onReplyAdd={handleAdd}
        onReplyUpdate={handleUpdate}
        onReplyRemove={showConfirm}
      />
      <Dimmer active={conversation.loading} inverted>
        <Loader size="small">Loading</Loader>
      </Dimmer>
      <Confirm
        title={t('comment.delete.title')}
        content={t('comment.delete')}
        open={isRemove.value}
        onOK={handleRemove}
        onCancel={isRemove.setFalse}
        loading={conversationEdit.loading}
      />
    </Fragment>
  );
});

export default TaskConversation;
