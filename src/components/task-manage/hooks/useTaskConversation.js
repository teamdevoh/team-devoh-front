import api from '../api';
import { useState } from 'react';

const useTaskConversation = taskItemId => {
  const [loading, setLoading] = useState(false);
  const add = async (parentId, value) => {
    const model = {
      parentTaskConversationId: parentId,
      comment: value
    };

    setLoading(true);
    const res = await api.addConversation({ taskItemId, model });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  const edit = async (value, id) => {
    const model = {
      comment: value
    };
    setLoading(true);
    const res = await api.updateConversation({
      taskItemId,
      taskConversationId: id,
      model
    });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  const remove = async removeId => {
    setLoading(true);
    const res = await api.removeConversation({
      taskItemId,
      taskConversationId: removeId
    });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  return [{ loading, onAdd: add, onEdit: edit, onRemove: remove }];
};
export default useTaskConversation;
