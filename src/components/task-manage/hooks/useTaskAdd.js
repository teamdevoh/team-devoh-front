import { useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';
import { useFormFields } from '../../../hooks';
import { code, format } from '../../../constants';

const initialState = {
  taskItemId: 0,
  isTask: false,
  isRegular: false,
  sectionCodeId: code.Open,
  parentTaskItemId: 0,
  orderNo: 0,
  title: '',
  rate: 0,
  weight: 0,
  scheduleDate: [],
  note: '',
  projectMemberIds: [],
  tagCodeIds: []
};

const useTaskAdd = projectId => {
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields(initialState);

  const handleAdd = async () => {
    setLoading(true);
    const dateformat = helpers.util.dateformat;
    if (model.scheduleDate) {
      if (model.scheduleDate.length > 0 && model.scheduleDate[0]) {
        model.scheduleFromDate = dateformat(
          model.scheduleDate[0],
          format.YYYY_MM_DD
        );
      }
      if (model.scheduleDate.length > 1 && model.scheduleDate[1]) {
        model.scheduleToDate = dateformat(
          model.scheduleDate[1],
          format.YYYY_MM_DD
        );
      }
    }

    const rate = Number(model.rate);
    model.sectionCodeId =
      rate > 0 && rate < 100
        ? code.Ongoing
        : rate === 100
          ? code.Completed
          : code.Open;

    const res = await api.addTask({ projectId, model });
    setLoading(false);
    if (res && res.data) {
      setModel({
        ...initialState,
        parentTaskItemId: model.parentTaskItemId
      });
      return res.data;
    }

    return false;
  };

  return [{ model, loading, onChange, onAdd: handleAdd }, setModel];
};

export default useTaskAdd;
