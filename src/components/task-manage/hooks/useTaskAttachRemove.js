import { useState } from 'react';
import api from '../api';

const useTaskAttachRemove = taskItemId => {
  const [loading, setLoading] = useState(false);

  const remove = async taskAttachmentId => {
    setLoading(true);
    const res = await api.removeTaskAttach({ taskItemId, taskAttachmentId });
    setLoading(true);
    if (res && res.data) {
      return true;
    }
    return false;
  };

  return { remove, loading };
};

export default useTaskAttachRemove;
