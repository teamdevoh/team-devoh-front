import { useState, useCallback } from 'react';
import helpers from '../../../helpers';

const useTaskAttachDownload = taskItemId => {
  const [loading, setLoading] = useState(false);

  const download = useCallback(async taskAttachmentId => {
    setLoading(true);
    helpers.util
      .download(
        `api/tasks/${taskItemId}/attach/${taskAttachmentId}`,
        percent => {
          if (percent === 100) {
            setLoading(false);
          }
        }
      )
      .catch(err => {
        setLoading(false);
      });
  }, []);

  return { download, loading };
};

export default useTaskAttachDownload;
