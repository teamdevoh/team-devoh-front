import { useEffect, useState } from 'react';
import api from '../api';

const useTodos = ({ projectId, name, offset, limit }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchTaskTodos({ projectId, name, offset, limit });
      if (res && res.data) {
        setItems(res.data.items);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId, name, offset, limit]
  );

  return { items, loading };
};

export default useTodos;
