import { useCallback, useEffect, useState } from 'react';
import { useFormFields } from '../../../hooks';
import api from '../api';
import pmApi from '../../project-manage/api';
import cmApi from '../../code-manage/api';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import helpers from '../../../helpers';
import { code, codeGroup, format } from '../../../constants';

const useTaskEdit = (projectId, taskItemId) => {
  const [isFetch, setIsFetch] = useState(false);
  const [loading, setLoading] = useState(false);
  const [{ model, onChange }, setModel] = useFormFields({
    isTask: false,
    isRegular: false,
    sectionCodeId: code.Open,
    parentTaskItemId: 0,
    orderNo: 0,
    title: '',
    rate: 0,
    weight: 0,
    scheduleDate: [],
    checkitems: [],
    note: '',
    projectMemberIds: [],
    tagCodeIds: [],
    scheduleFromDate: null,
    scheduleToDate: null
  });

  const fetchItem = useCallback(
    async () => {
      try {
        const res = await api.fetchTask({
          projectId: projectId.toString(),
          taskItemId
        });
        if (res && res.data) {
          const dates = [];
          dates.push(res.data.scheduleFromDate);
          dates.push(res.data.scheduleToDate);
          res.data.scheduleDate = dates;
          res.data.checkitems = [];

          setModel(res.data);
        }
      } catch (error) {
      } finally {
        setIsFetch(true);
      }
    },
    [projectId, taskItemId]
  );

  const getMemberNames = useCallback(
    async values => {
      let names = [];
      const res = await pmApi.fetchProjectMembers({ projectId });
      if (res.data) {
        res.data.items.forEach(item => {
          if (values.indexOf(item.projectMemberId) > -1) {
            names.push({ name: item.memberName });
          }
        });
      }
      return names;
    },
    [projectId]
  );

  const getTagNames = useCallback(async values => {
    let names = [];
    const res = await cmApi.fetchCodes(codeGroup.Tag);
    if (res && res.data) {
      res.data.forEach(item => {
        if (values.indexOf(item.codeId) > -1) {
          names.push({ name: item.name, color: item.tag1.toLowerCase() });
        }
      });
    }
    return names;
  }, []);

  const edit = useCallback(
    async () => {
      const dateformat = helpers.util.dateformat;
      model.scheduleFromDate = null;
      model.scheduleToDate = null;
      if (model.scheduleDate) {
        if (model.scheduleDate.length > 0 && model.scheduleDate[0]) {
          dateformat(model.scheduleDate[0], format.YYYY_MM_DD);
          model.scheduleFromDate = dateformat(
            model.scheduleDate[0],
            format.YYYY_MM_DD
          );
        }
        if (model.scheduleDate.length > 1 && model.scheduleDate[1]) {
          model.scheduleToDate = dateformat(
            model.scheduleDate[1],
            format.YYYY_MM_DD
          );
        }
      }

      const rate = Number(model.rate);
      model.sectionCodeId =
        rate > 0 && rate < 100
          ? code.Ongoing
          : rate === 100
            ? code.Completed
            : code.Open;

      model.taskCheckItems = mapValues(
        keyBy(model.checkitems, 'taskCheckItemId'),
        'isComplete'
      );

      const members = await getMemberNames(model.projectMemberIds);
      model.members = members;
      const tags = await getTagNames(model.tagCodeIds);
      model.tags = tags;

      setLoading(true);
      const res = await api.updateTask({ projectId, taskItemId, model });
      setLoading(false);
      if (res && res.data) {
        return true;
      }
      return false;
    },
    [model]
  );

  useEffect(
    () => {
      fetchItem();
      return () => {};
    },
    [taskItemId]
  );

  const remove = useCallback(async () => {
    setLoading(true);
    const res = await api.removeTask({ projectId, taskItemId });
    setLoading(false);
    if (res && res.data) {
      return true;
    }
    return false;
  }, []);

  return [
    {
      model,
      loading,
      isFetch,
      onChange,
      onEdit: edit,
      onRemove: remove
    },
    setModel
  ];
};

export default useTaskEdit;
