import { useEffect, useState } from 'react';
import api from '../api';
import helpers from '../../../helpers';

const useTaskAttachmentList = taskItemId => {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const res = await api.fetchTaskAttachments({ taskItemId });
    let items = [];
    await res.data.forEach(async item => {
      const fileObj = await helpers.file.setFileInfo({
        ...item,
        id: item.taskAttachmentId,
        thumbnailApiUrl: `api/tasks/${taskItemId}/attach.thumbnail/${
          item.taskAttachmentId
        }`
      });
      items.push(fileObj);
    });

    setItems(items);
    setLoading(false);
  };

  useEffect(
    () => {
      fetchItems();
    },
    [taskItemId]
  );

  return [{ items, loading }, setItems];
};

export default useTaskAttachmentList;
