import { useCallback, useEffect, useState } from 'react';
import defaults from 'lodash/defaults';
import api from '../api';

const useTaskConversationList = taskItemId => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = useCallback(
    async () => {
      try {
        setLoading(true);
        const res = await api.fetchConversation({ taskItemId });
        if (res && res.data) {
          let items = [];
          for (let i = 0; i < res.data.items.length; i++) {
            const item = res.data.items[i];
            items.push({
              id: item.taskConversationId,
              parentId: item.parentTaskConversationId,
              memberName: item.memberName,
              memberId: item.creator,
              fileStorageId: item.myFaceFileStorageId,
              date: item.created,
              comment: item.comment,
              children: item.children
            });
          }

          const data = transformToTree(items);

          setItems(data);
        }
      } catch (error) {
      } finally {
        setLoading(false);
      }
    },
    [taskItemId]
  );

  const transformToTree = arr => {
    var nodes = {};
    return arr.filter(function(obj) {
      var id = obj['id'],
        parentId = obj['parentId'];

      nodes[id] = defaults(obj, nodes[id], { children: [] });
      parentId &&
        (nodes[parentId] = nodes[parentId] || { children: [] })[
          'children'
        ].push(obj);

      return !parentId;
    });
  };

  useEffect(
    () => {
      fetchItems();
    },
    [taskItemId]
  );

  return [{ items, loading, refresh: fetchItems }, setItems];
};

export default useTaskConversationList;
