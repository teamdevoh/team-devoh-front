import { useEffect, useState } from 'react';
import api from '../api';

const useTaskList = ({ projectId }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    try {
      setLoading(true);
      const res = await api.fetchTasks({ projectId });
      if (res && res.data) {
        setItems(res.data.items);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const refresh = () => {
    fetchItems();
  };

  useEffect(
    () => {
      fetchItems();
    },
    [projectId]
  );

  return { items, loading, refresh };
};

export default useTaskList;
