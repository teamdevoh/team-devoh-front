import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import JqxTreeGrid from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxtreegrid';
import { SearchInput } from '../input';
import { injectIntl } from 'react-intl';
import helpers from '../../helpers';
import { Form } from 'semantic-ui-react';
import { Confirm } from '../modal';
import { Copy } from '../animation';
import defaults from 'lodash/defaults';

const gridName = 'templateGrid';

class TemplateTaskList extends Component {
  constructor(props) {
    super(props);

    const intl = props.intl;
    const self = this;
    const data = {
      dataType: 'json',
      dataFields: [
        { name: 'templateTaskId', type: 'number' },
        { name: 'parentTemplateTaskId', type: 'number' },
        { name: 'title', type: 'string' },
        { name: 'note', type: 'string' },
        { name: 'orderNo', type: 'number' },
        { name: 'workDay', type: 'number' },
        { name: 'checkItemCount', type: 'number' },
        { name: 'children', type: 'array' },
        { name: 'expanded', type: 'bool' }
      ],
      hierarchy: {
        root: 'children'
      },
      id: 'templateTaskId',
      url: `${__API_URL__}api/tasks/templates`,
      beforeLoadComplete: function(records) {
        const data = self.transformToTree(records);
        return data;
      }
    };

    this.state = {
      modalOpen: false,
      templateTaskId: 0,
      isSaving: false,
      source: new $.jqx.dataAdapter(data, {
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', helpers.Auth.jwt);
        }
      }),
      columns: [
        {
          text: intl.formatMessage({ id: 'common.id' }),
          dataField: 'templateTaskId',
          hidden: true
        },
        {
          text: intl.formatMessage({ id: 'task' }),
          dataField: 'title',
          cellsRenderer: (row, column, value, model) => {
            if (model.parentTemplateTaskId === 0) {
              return `<span data-row=${row}
              class=${this.refs[gridName]._id}_linkbutton
              style="margin-right : 5px; margin-top: 8px; cursor: pointer;">${value}</span>`;
            } else {
              return value;
            }
          }
        },
        {
          text: intl.formatMessage({ id: 'task.note' }),
          dataField: 'note',
          width: '20%'
        },
        {
          text: intl.formatMessage({ id: 'task.days' }),
          dataField: 'workDay',
          width: '10%'
        },
        {
          text: intl.formatMessage({ id: 'task.checklist' }),
          dataField: 'checkItemCount',
          width: '15%'
        }
      ]
    };

    this.filterRows = this.filterRows.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  applyFilter = (dataField, search) => {
    this.refs[gridName].clearFilters();
    let filtertype = 'stringfilter';
    // create a new group of filters.
    let filtergroup = new $.jqx.filter();
    let filter = filtergroup.createfilter(filtertype, search, 'CONTAINS');
    filtergroup.addfilter(1, filter);
    // add the filters.
    this.refs[gridName].addFilter(dataField, filtergroup);
    // apply the filters.
    this.refs[gridName].applyFilters();
  };

  refreshData() {
    this.refs[gridName].updateBoundData('cells');
  }

  filterRows(value) {
    this.applyFilter('title', value);
  }

  transformToTree(arr) {
    var nodes = {};
    return arr.filter(function(obj) {
      var id = obj['templateTaskId'],
        parentId = obj['parentTemplateTaskId'];

      nodes[id] = defaults(obj, nodes[id], { children: [] });
      parentId &&
        (nodes[parentId] = nodes[parentId] || { children: [] })[
          'children'
        ].push(obj);

      return !parentId;
    });
  }

  render() {
    const { filterRows } = this;
    const { height, intl, onSelect } = this.props;
    const { isSaving } = this.state;

    const gridProps = height
      ? { autoheight: false, height: height }
      : { autoheight: false };

    return (
      <Form>
        <Form.Field>
          <SearchInput
            onClick={value => {
              filterRows(value);
            }}
          />
        </Form.Field>
        <JqxTreeGrid
          ref={gridName}
          width={'100%'}
          columnsResize={true}
          {...gridProps}
          sortable={true}
          autoRowHeight={false}
          source={this.state.source}
          columns={this.state.columns}
          rendered={() => {
            let linkButtonsContainers = document.getElementsByClassName(
              `${this.refs[gridName]._id}_linkbutton`
            );
            for (let i = 0; i < linkButtonsContainers.length; i++) {
              const element = linkButtonsContainers[i];
              ReactDOM.render(
                <a
                  onClick={() => {
                    this.setState({
                      modalOpen: true,
                      templateTaskId: element.dataset.row
                    });
                  }}
                >
                  {element.innerText}
                </a>,
                element
              );
            }
          }}
        />
        <Confirm
          title={intl.formatMessage({
            id: 'task.template.copy'
          })}
          content={
            isSaving ? (
              <Copy />
            ) : (
              intl.formatMessage({ id: 'task.template.copy.confirm' })
            )
          }
          open={this.state.modalOpen}
          onOK={() => {
            this.setState({ isSaving: true });
            onSelect(this.state.templateTaskId);
          }}
          onCancel={() => this.setState({ modalOpen: false })}
          loading={isSaving}
        />
      </Form>
    );
  }
}

export default injectIntl(TemplateTaskList, { withRef: true });
