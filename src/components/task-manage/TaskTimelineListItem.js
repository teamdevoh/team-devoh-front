import React, { Fragment, useCallback } from 'react';
import { Label, List, Header, Segment, Icon } from 'semantic-ui-react';
import { TransitNumber } from '../modules';
import TaskTimelineMemberItem from './TaskTimelineMemberItem';
import LazyLoad from 'react-lazyload';
import map from 'lodash/map';
import { TimeLineItemWrapper } from './styles';

const TaskStatus = ({ color, name }) => {
  return (
    <Label size="mini" color={color.toLowerCase()}>
      {name}
    </Label>
  );
};

const HeadLine = ({ title, note }) => {
  return (
    <Fragment>
      <Header as="h4" dividing>
        {title}
      </Header>
      <p>{note}</p>
    </Fragment>
  );
};

const Progress = ({ value }) => {
  return (
    <Header>
      <LazyLoad>
        <Icon.Group>
          <div>
            <TransitNumber value={value} />
            <Icon corner="bottom right" name="percent" />
          </div>
        </Icon.Group>
      </LazyLoad>
    </Header>
  );
};

const TaskTimelineListItem = ({
  date,
  title,
  rate,
  note,
  tags,
  taskMembers
}) => {
  const handleMemberClick = useCallback(id => {
    // TODO: 사용자 클릭시 액션처리 필요.
  }, []);

  return (
    <TimeLineItemWrapper dateText={date}>
      <Progress value={rate} />
      <Segment>
        <HeadLine title={title} note={note} />
        {map(tags, tag => {
          return (
            <TaskStatus key={tag.tagCodeId} name={tag.name} color={tag.color} />
          );
        })}
        <br />
        <List horizontal>
          {map(taskMembers, taskMember => {
            return (
              <TaskTimelineMemberItem
                key={taskMember.memberId}
                id={taskMember.memberId}
                src={taskMember.face}
                name={taskMember.name}
                onClick={handleMemberClick}
              />
            );
          })}
        </List>
      </Segment>
    </TimeLineItemWrapper>
  );
};

export default TaskTimelineListItem;
