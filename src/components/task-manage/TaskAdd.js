import React, { useRef, useState } from 'react';
import { CancelButton, SaveButton } from '../button';
import { Grid, Message, Header, Divider, Icon } from 'semantic-ui-react';
import helpers from '../../helpers';
import { notification } from '../modal';
import { TaskForm, TaskTree } from './';
import { useFormatMessage, useBoolean } from '../../hooks';
import { useTaskAdd } from './hooks';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { TreeWrapper } from './styles';
import { format, role } from '../../constants';

const TaskHeader = ({
  children,
  isSelected,
  title,
  isTask,
  fromDate,
  toDate
}) => {
  const t = useFormatMessage();

  return (
    <Header as="h3" dividing color={isSelected ? 'green' : 'red'}>
      {isSelected
        ? `${title} ${
            isTask === true ? `(${t('task')})` : `(${t('category')})`
          }`
        : t('task.add.parent.notselected')}

      <Header.Subheader>
        {isSelected && <Icon name="calendar alternate outline" />}
        {isSelected &&
          helpers.util.dateRange(fromDate, toDate, format.YYYY_MM_DD)}
        {!isSelected && t('task.add.parent.selectinfo')}
      </Header.Subheader>
    </Header>
  );
};

const NotCategory = ({ title }) => {
  const t = useFormatMessage();
  return (
    <Message
      info
      header={t('task.undercreate', { title: title })}
      content={
        <div>
          {`[${title}]`}
          {t('task.istask')}
          <Icon name="check square outline" /> <Icon name="arrow right" /> X
        </div>
      }
    />
  );
};

const TaskAdd = () => {
  const taskTree = useRef();
  const match = useRouteMatch();
  const { projectId } = match.params;
  const history = useHistory();
  const t = useFormatMessage();
  const [selectedRow, setSelectedRow] = useState({
    taskItemId: -1
  });
  const stay = useBoolean(false);
  const [task, setModel] = useTaskAdd(projectId);

  const handleRowSelect = event => {
    setSelectedRow(event.args.row);
    setModel({ ...task.model, parentTaskItemId: event.args.row.taskItemId });
  };

  const handleAdd = async () => {
    const is = await task.onAdd();
    if (is) {
      notification.success({
        title: t('common.alert.added'),
        onClose: () => {
          if (!stay.value) {
            history.push(`/project/${projectId}/task/portal/all`);
          } else {
            const tree = taskTree.current.refs.wrappedInstance;
            tree.refreshData();
            tree.setSelectedRowId(selectedRow.taskItemId);
          }
        }
      });
    }
  };

  return (
    <Grid stackable>
      <TreeWrapper width={6}>
        <TaskTree
          ref={taskTree}
          projectId={match.params.projectId}
          onRowSelect={handleRowSelect}
        />
      </TreeWrapper>
      <Grid.Column width={10}>
        <TaskHeader
          isSelected={selectedRow.taskItemId >= 0}
          title={selectedRow.title}
          isTask={selectedRow.isTask}
          fromDate={selectedRow.scheduleFromDate}
          toDate={selectedRow.scheduleToDate}
        />
        <Divider horizontal />
        {!selectedRow.isTask && (
          <TaskForm
            loaded={true}
            projectId={match.params.projectId}
            model={task.model}
            onChange={task.onChange}
            onSubmit={handleAdd}
          >
            <CancelButton
              to={`/project/${projectId}/task/portal/all`}
              name={t('common.cancel')}
            />
            <SaveButton
              allowedRole={`${role.Task_Edit},${role.ConsoleTask_Edit}`}
              disabled={selectedRow.taskItemId < 0}
              onClick={stay.setFalse}
              name={t('common.save')}
              loading={task.loading}
            />
            <SaveButton
              allowedRole={`${role.Task_Edit},${role.ConsoleTask_Edit}`}
              disabled={selectedRow.taskItemId < 0}
              onClick={stay.setTrue}
              name={t('common.savencontinue')}
              loading={task.loading}
            />
          </TaskForm>
        )}
        {selectedRow.isTask && (
          <div>
            <NotCategory title={selectedRow.title} />
            <CancelButton
              to={`/project/${projectId}/task/portal/all`}
              name={t('common.cancel')}
            />
          </div>
        )}
      </Grid.Column>
    </Grid>
  );
};

export default TaskAdd;
