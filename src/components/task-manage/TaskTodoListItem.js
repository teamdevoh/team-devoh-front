import React, { useCallback } from 'react';
import { List } from 'semantic-ui-react';
import { ProgressBar } from '../modules';
import helpers from '../../helpers';
import Anchor from '../button/Anchor';
import { StatusLabel, DateRangeSpan, ProgressBarSpan } from './styles';
import { format } from '../../constants';

const ItemHeader = ({ onClick, status, title }) => {
  const renderOverOrBy = useCallback(() => {
    if (status === 'OVER' || status === 'BY') {
      return (
        <StatusLabel
          circular
          empty
          color={status === 'OVER' ? 'red' : 'blue'}
        />
      );
    }
  }, []);

  return (
    <List.Header>
      <Anchor text={title} onClick={onClick} />
      {renderOverOrBy()}
    </List.Header>
  );
};

const Schedule = ({ from, to }) => {
  return (
    <DateRangeSpan>
      {helpers.util.dateformat(from, format.YYYY_MM_DD)}
      {' ~ '}
      {helpers.util.dateformat(to, format.YYYY_MM_DD)}
    </DateRangeSpan>
  );
};

const Percentage = ({ value }) => {
  return (
    <ProgressBarSpan>
      <ProgressBar percent={value} />
    </ProgressBarSpan>
  );
};

const TaskTodoListItem = ({
  title,
  scheduleFromDate,
  scheduleToDate,
  rate,
  status,
  id,
  onItemClick
}) => {
  const handleClick = useCallback(() => {
    onItemClick({ id });
  }, []);

  return (
    <List.Item>
      <List.Content>
        <ItemHeader onClick={handleClick} status={status} title={title} />
      </List.Content>
      <List.Content>
        <Schedule from={scheduleFromDate} to={scheduleToDate} />
        <Percentage value={rate} />
      </List.Content>
    </List.Item>
  );
};

export default TaskTodoListItem;
