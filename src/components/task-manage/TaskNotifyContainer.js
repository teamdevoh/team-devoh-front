import React, { useCallback, useEffect, useState, memo } from 'react';
import {
  Card,
  Dropdown,
  Loader,
  Icon,
  Grid,
  Segment,
  Visibility
} from 'semantic-ui-react';
import api from './api';
import { notification } from '../modal';
import TaskEditLayer from './TaskEditLayer';
import helpers from '../../helpers';
import { useFormatMessage } from '../../hooks';
import moment from 'moment';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { project } from '../../states';
import map from 'lodash/map';
import filter from 'lodash/filter';
import find from 'lodash/find';
import { code, format } from '../../constants';

const StyledContent = styled(Card.Content)`
  cursor: pointer;
`;

const SelectNotification = memo(({ onChange }) => {
  const t = useFormatMessage();
  const options = [
    {
      key: 1,
      text: t('project.notification.all'),
      value: 1
    },
    {
      key: 2,
      text: t('project.notification.unread'),
      value: 2
    }
  ];

  return <Dropdown defaultValue={2} onChange={onChange} options={options} />;
});

const TaskNotificationListItem = memo(
  ({ onClick, id, description, isComplete, meta, title }) => {
    const handleClick = useCallback(() => {
      onClick(id);
    });

    return (
      <Card link onClick={handleClick}>
        <Card.Content>
          {isComplete && (
            <Icon name="check circle" size="large" color="green" />
          )}
          {title}
        </Card.Content>
        <StyledContent>
          <Card.Meta>{meta}</Card.Meta>
          <Card.Description>{description}</Card.Description>
        </StyledContent>
      </Card>
    );
  }
);

const TaskNotificationList = memo(({ isLoading, items, onClick }) => {
  const t = useFormatMessage();
  return (
    <Card.Group itemsPerRow={1}>
      {map(items, item => {
        return (
          <TaskNotificationListItem
            key={item.auditEntryId}
            id={item.auditEntryId}
            title={item.title}
            isComplete={item.sectionCodeId === code.Completed}
            meta={t('common.changedby', {
              created: moment(item.createdDate, format.YYYYMMDDHHMM).fromNow(),
              creator: item.memberName
            })}
            description={item.sectionCodeName}
            onClick={onClick}
          />
        );
      })}
      {isLoading && <Loader active inline="centered" />}
    </Card.Group>
  );
});

const NotifyContainer = memo(({ onlyUnread, onClick }) => {
  const [items, setItems] = useState([]);
  const [offset, setOffset] = useState(0);
  const [hasMore, setHasMore] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const selectedProject = useSelector(project.selectors.getSelectedItem);

  const _limit = 10;
  let isSubscribed = true;

  useEffect(
    () => {
      fetchItems(0, []);
      return () => (isSubscribed = false);
    },
    [onlyUnread]
  );

  const fetchItems = useCallback(
    async (start, data) => {
      setIsLoading(true);
      try {
        const res = await api.fetchNotifications({
          projectId: selectedProject.key,
          onlyUnread: onlyUnread,
          offset: start,
          limit: _limit
        });

        const merged = data.concat(res.data.items);
        if (isSubscribed) {
          setOffset(merged.length);
          setHasMore(merged.length < res.data.paging.total);
          setIsLoading(false);
          setItems(merged);
        }
      } catch (e) {
        if (isSubscribed) {
          setIsLoading(false);
        }
      }
    },
    [items, offset, onlyUnread]
  );

  const handleVisibleBottom = useCallback(
    () => {
      if (!isLoading && hasMore) {
        fetchItems(offset, items);
      }
    },
    [items]
  );

  const handleClick = useCallback(
    id => {
      const targetItem = find(items, { auditEntryId: id });
      if (targetItem.isRead === false) {
        const filtered = filter(items, item => {
          return item.auditEntryId !== id;
        });
        api
          .addTaskReadHistory({
            taskItemId: targetItem.taskItemId,
            model: { AuditEntryId: id }
          })
          .then(res => {
            setOffset(filtered.length);
            setItems(filtered);
          });
      }

      onClick(targetItem.taskItemId);
    },
    [items]
  );

  return (
    <Visibility continuous={hasMore} onBottomVisible={handleVisibleBottom}>
      <TaskNotificationList
        items={items}
        onClick={handleClick}
        isLoading={isLoading}
      />
    </Visibility>
  );
});

const TaskNotifyContainer = () => {
  const t = useFormatMessage();
  const [onlyUnread, setOnlyUnread] = useState(true);
  const [show, setShow] = useState(false);
  const [selectedTaskItemId, setSelectedTaskItemId] = useState(0);
  const selectedProject = useSelector(project.selectors.getSelectedItem);

  const handleClick = useCallback(taskItemId => {
    setSelectedTaskItemId(taskItemId);
    setShow(true);
  }, []);

  const handleChangeNotifi = useCallback((event, data) => {
    setOnlyUnread(data.value === 2 ? true : false);
  }, []);

  const handleTaskEditClose = useCallback(() => {
    setShow(false);
  }, []);

  const handleTaskSaved = useCallback(model => {
    setShow(false);
    notification.success({
      title: t('common.alert.changed'),
      onClose: () => {}
    });
  }, []);

  const handleTaskRemove = useCallback(() => {
    setShow(false);
    notification.success({
      title: t('common.alert.deleted'),
      onClose: () => {}
    });
  }, []);

  return (
    <Segment
      style={
        !helpers.util.isMobile()
          ? {
              ...{
                width: '100%',
                position: 'fixed',
                top: '0px',
                bottom: '0px',
                right: '0',
                overflowX: 'hidden',
                maxWidth: '300px',
                background: '#e1e5ed'
              }
            }
          : {}
      }
    >
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <SelectNotification onChange={handleChangeNotifi} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row style={{ minHeight: '5em' }}>
          <Grid.Column>
            <NotifyContainer onlyUnread={onlyUnread} onClick={handleClick} />
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <TaskEditLayer
        open={show}
        onClose={handleTaskEditClose}
        title={t('task.title.edit')}
        projectId={selectedProject.key}
        taskItemId={selectedTaskItemId}
        onSaved={handleTaskSaved}
        onRemoved={handleTaskRemove}
      />
    </Segment>
  );
};

export default memo(TaskNotifyContainer);
