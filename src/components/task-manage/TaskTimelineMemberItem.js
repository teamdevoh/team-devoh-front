import React from 'react';
import { List } from 'semantic-ui-react';
import { LazyImage } from '../modules';
import { TimeLineMemberWrapper } from './styles';

const TaskTimelineMemberItem = ({ id, name, onClick, src }) => {
  const handleClick = () => {
    onClick(id);
  };

  return (
    <TimeLineMemberWrapper>
      <LazyImage src={src} avatar={true} />
      <List.Content>
        <List.Header onClick={handleClick}>{name}</List.Header>
      </List.Content>
    </TimeLineMemberWrapper>
  );
};

export default TaskTimelineMemberItem;
