import React, { useCallback, memo } from 'react';
import { Form } from 'semantic-ui-react';
import { RoleAware } from '../auth';
import { useFormatMessage } from '../../hooks';
import { role } from '../../constants';

const styles = {
  height: '50px'
};

const Reply = memo(({ onChange, value, onClick }) => {
  const t = useFormatMessage();

  const handleKeyPress = useCallback(event => {
    if (event.key === 'Enter' && event.target.value.trim().length > 0) {
      event.preventDefault();
      onClick(event.target.value);
      event.target.value = '';
    }
  }, []);

  const handleSubmit = useCallback(event => {
    onClick(event.target['comment'].value);
    event.target['comment'].value = '';
  }, []);

  return (
    <RoleAware
      allowedRole={`${role.TaskConversation_Edit},${role.ConsoleTask_Edit}`}
    >
      <Form reply onSubmit={handleSubmit}>
        <Form.TextArea
          name="comment"
          style={styles}
          value={value}
          placeholder={t('comment.enterkey')}
          onChange={onChange}
          onKeyPress={handleKeyPress}
        />
      </Form>
    </RoleAware>
  );
});

export default Reply;
