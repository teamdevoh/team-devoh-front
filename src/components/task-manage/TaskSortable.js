import React, { Component } from 'react';
import SortableTree from 'react-sortable-tree';
import 'react-sortable-tree/style.css';
import { Input, Icon } from 'semantic-ui-react';
import api from './api';

class TaskSortable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isChanged: false,
      data: props.data,
      search: ''
    };

    this.handleModeNode = this.handleModeNode.bind(this);
  }

  handleModeNode(data) {
    const { onChange } = this.props;
    let ordered = {};
    for (let i = 0; i < data.nextParentNode.children.length; i++) {
      const child = data.nextParentNode.children[i];
      ordered[child.taskItemId] = i;
    }

    if (data.nextParentNode && data.nextParentNode.isTask !== true) {
      data.node.parentTaskItemId = data.nextParentNode.taskItemId;

      api
        .changedOrderParent({
          taskItemId: data.node.taskItemId,
          model: {
            parentTaskItemId: data.nextParentNode.taskItemId,
            orderedTasks: ordered
          }
        })
        .then(res => {
          onChange(res.data);
        });
    }
  }

  render() {
    const { handleModeNode } = this;
    const { data, search } = this.state;

    return (
      <div style={{ height: '700px' }}>
        <Input
          fluid
          icon="search"
          onChange={(event, data) => {
            this.setState({ search: data.value });
          }}
        />
        <SortableTree
          treeData={data}
          rowHeight={50}
          searchQuery={search}
          onlyExpandSearchedNodes={search ? true : false}
          canDrop={data => {
            if (
              data.nextParent &&
              !data.nextParent.isTask &&
              data.nextParent.taskItemId > -1
            ) {
              return true;
            } else {
              return false;
            }
          }}
          onMoveNode={handleModeNode}
          onChange={treeData => {
            this.setState({ data: treeData, isChanged: true });
          }}
          generateNodeProps={rowInfo => {
            if (rowInfo.node.isTask && rowInfo.node.isTask === true) {
              return {
                buttons: [<Icon name="tasks" />]
              };
            }
          }}
        />
      </div>
    );
  }
}

export default TaskSortable;
