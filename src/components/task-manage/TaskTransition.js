import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';

class TaskTransition extends Component {
  render() {
    const { children, timeout } = this.props;
    return (
      <CSSTransition
        in={true}
        timeout={timeout || 300}
        classNames="fade"
        unmountOnExit
        appear
      >
        {children}
      </CSSTransition>
    );
  }
}

export default TaskTransition;
