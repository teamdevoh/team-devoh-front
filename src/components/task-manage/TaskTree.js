import React, { Component } from 'react';
import JqxTreeGrid from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxtreegrid';
import { SearchInput } from '../input';
import { injectIntl } from 'react-intl';
import helpers from '../../helpers';
import { Form } from 'semantic-ui-react';
import defaults from 'lodash/defaults';
import { format } from '../../constants';

class TaskTree extends Component {
  constructor(props) {
    super(props);

    const intl = props.intl;

    const self = this;
    const data = {
      dataType: 'json',
      dataFields: [
        { name: 'taskItemId', type: 'number' },
        { name: 'parentTaskItemId', type: 'number' },
        { name: 'title', type: 'string' },
        { name: 'scheduleFromDate', type: 'date' },
        { name: 'scheduleToDate', type: 'date' },
        { name: 'note', type: 'string' },
        { name: 'isTask', type: 'bool' },
        { name: 'children', type: 'array' },
        { name: 'expanded', type: 'bool' }
      ],
      hierarchy: {
        root: 'children'
      },
      id: 'taskItemId',
      url: `${__API_URL__}api/projects/${props.projectId}/tasks`,
      beforeLoadComplete: function(records) {
        const data = self.transformToTree(records, self.state.selectRowId);

        const root = [
          {
            taskItemId: 0,
            parentTaskItemId: 0,
            title: 'Root',
            scheduleFromDate: '',
            scheduleToDate: '',
            note: '',
            expanded: true,
            children: []
          }
        ];
        root[0].children = data;
        return root;
      },
      loadComplete: () => {
        if (self.refs.tGrid !== void 0) {
          self.refs.tGrid.selectRow(self.state.selectRowId);
        }
      }
    };

    this.state = {
      selectRowId: 0,
      source: new $.jqx.dataAdapter(data, {
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', helpers.Auth.jwt);
        }
      }),
      columns: [
        {
          text: intl.formatMessage({ id: 'common.id' }),
          dataField: 'taskItemId',
          hidden: true
        },
        {
          text: intl.formatMessage({ id: 'task' }),
          dataField: 'title',
          width: '250'
        },
        {
          text: intl.formatMessage({ id: 'task.schedule' }),
          dataField: 'schedule'
        }
      ]
    };

    this.filterRows = this.filterRows.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.handleRowClick = this.handleRowClick.bind(this);
    this.setSelectedRowId = this.setSelectedRowId.bind(this);
  }

  handleRowClick(event) {
    this.props.onRowSelect(event);
  }

  applyFilter = (dataField, search) => {
    this.refs.tGrid.clearFilters();
    let filtertype = 'stringfilter';
    // create a new group of filters.
    let filtergroup = new $.jqx.filter();
    let filter = filtergroup.createfilter(filtertype, search, 'CONTAINS');
    filtergroup.addfilter(1, filter);
    // add the filters.
    this.refs.tGrid.addFilter(dataField, filtergroup);
    // apply the filters.
    this.refs.tGrid.applyFilters();
  };

  setSelectedRowId(key) {
    this.setState({ selectRowId: key });
  }

  refreshData() {
    this.refs.tGrid.updateBoundData('cells');
  }

  filterRows(value) {
    this.applyFilter('title', value);
  }

  transformToTree(arr, selectedRowId) {
    var nodes = {};
    return arr.filter(function(obj) {
      var id = obj['taskItemId'],
        parentId = obj['parentTaskItemId'];

      if (selectedRowId > 0 && id === selectedRowId) {
        obj['expanded'] = true;
      }

      obj['schedule'] = helpers.util.dateRange(
        obj['scheduleFromDate'],
        obj['scheduleToDate'],
        format.YYYY_MM_DD
      );

      nodes[id] = defaults(obj, nodes[id], { children: [] });
      parentId &&
        (nodes[parentId] = nodes[parentId] || { children: [] })[
          'children'
        ].push(obj);

      return !parentId;
    });
  }

  render() {
    const { filterRows, handleRowClick } = this;
    const { height } = this.props;

    const gridProps = height
      ? { autoheight: false, height: height }
      : { autoheight: true };

    return (
      <div>
        <Form>
          <Form.Field>
            <SearchInput
              onClick={value => {
                filterRows(value);
              }}
            />
          </Form.Field>
          <JqxTreeGrid
            ref="tGrid"
            width={'100%'}
            columnsResize={true}
            {...gridProps}
            sortable={true}
            source={this.state.source}
            columns={this.state.columns}
            onRowClick={handleRowClick}
          />
        </Form>
      </div>
    );
  }
}

export default injectIntl(TaskTree, { withRef: true });
