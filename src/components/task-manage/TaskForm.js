import React, { useCallback, memo, Fragment } from 'react';
import { Divider, Form, Input, TextArea, Checkbox } from 'semantic-ui-react';
import { PercentageInput, WeightInput } from '../input';
import { AssignedList, TaskTagList, TaskCheckList } from './';
import { DateRangePicker } from '../datepicker';
import { useFormatMessage, useSubmit } from '../../hooks';

const TaskForm = ({
  children,
  model,
  onChange,
  projectId,
  taskItemId,
  loaded,
  onSubmit
}) => {
  const t = useFormatMessage();
  const [{ validator, onValidate }] = useSubmit(onSubmit);

  const handleCheckItemChange = useCallback(
    (event, data) => {
      onChange(event, {
        name: data.name,
        value: data.value
      });
    },
    [model]
  );

  return (
    <Form onSubmit={onValidate}>
      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('task')}</label>
          <Input name="title" value={model.title} onChange={onChange} />
          {validator.message(t('task'), model.title, 'required')}
        </Form.Field>
      </Form.Group>
      {!model.hasChildren && (
        <Form.Group>
          <Form.Field>
            <Checkbox
              label={t('task.istask')}
              name="isTask"
              checked={model.isTask === true}
              onChange={(event, data) => {
                onChange(event, { name: data.name, value: data.checked });
              }}
            />
            {validator.message(t('task.istask'), model.isTask, 'required')}
          </Form.Field>
          {model.isTask && (
            <Form.Field>
              <Checkbox
                label={t('task.isregular')}
                name="isRegular"
                checked={model.isRegular === true}
                onChange={(event, data) => {
                  onChange(event, { name: data.name, value: data.checked });
                }}
              />
            </Form.Field>
          )}
        </Form.Group>
      )}

      {model.isTask && (
        <Fragment>
          <Form.Group widths="equal">
            <Form.Field required>
              <label>{t('task.schedule')}</label>
              <DateRangePicker
                name="scheduleDate"
                value={model.scheduleDate}
                onChange={onChange}
              />
              {validator.message(
                t('task.schedule'),
                model.scheduleDate.length > 0 ? '1' : '',
                'required'
              )}
            </Form.Field>
            <Form.Field>
              <label>{t('task.weight')}</label>
              <WeightInput
                name="weight"
                placeholder={t('task.weight')}
                value={model.weight}
                onChange={onChange}
              />
            </Form.Field>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field>
              <label>{t('task.assigned')}</label>
              <AssignedList
                name="projectMemberIds"
                projectId={projectId}
                placeholder={t('task.assigned')}
                value={model.projectMemberIds}
                onChange={onChange}
              />
            </Form.Field>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field>
              <label>{t('task.rate')}</label>
              <PercentageInput
                name="rate"
                placeholder={t('task.rate')}
                value={model.rate}
                onChange={onChange}
              />
            </Form.Field>
            <Form.Field>
              <label>{t('task.tag')}</label>
              <TaskTagList
                name="tagCodeIds"
                placeholder={t('task.tag')}
                value={model.tagCodeIds}
                onChange={onChange}
              />
            </Form.Field>
          </Form.Group>

          {taskItemId && (
            <Form.Group grouped>
              <label>{t('task.checklist')}</label>
              <TaskCheckList
                name="checkitems"
                taskItemId={taskItemId}
                onChange={handleCheckItemChange}
                value={model.checkitems}
              />
            </Form.Group>
          )}
        </Fragment>
      )}

      <Form.Group widths="equal">
        <Form.Field>
          <label>{t('task.note')}</label>
          <TextArea
            name="note"
            value={model.note === null ? '' : model.note}
            onChange={onChange}
          />
        </Form.Field>
      </Form.Group>
      <Divider />
      {children}
    </Form>
  );
};

export default memo(TaskForm);
