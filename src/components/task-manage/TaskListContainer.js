import React, { useCallback, useState, useRef } from 'react';
import { AddLinkButton, LinkButton } from '../button';
import { TaskList } from './';
import { Grid } from 'semantic-ui-react';
import { Modal, notification } from '../modal';
import { TemplateTaskList, TaskEditLayer } from './';
import api from './api';
import { useFormatMessage, useBoolean } from '../../hooks';

const TaskListContainer = ({ projectId }) => {
  const t = useFormatMessage();
  const isShowTemplateTask = useBoolean(false);
  const isModalOpen = useBoolean(false);
  const [selectedTaskItemId, setSelectedTaskItemId] = useState(0);
  const taskListRef = useRef();

  const handleCopyTask = useCallback(
    async templateTaskId => {
      try {
        const res = await api.addCopyTaskFromTemplate(
          projectId,
          templateTaskId
        );
        if (res && res.data) {
          notification.success({
            title: t('common.alert.added'),
            onClose: () => {
              isShowTemplateTask.setFalse();
              taskListRef.current.refs.wrappedInstance.refreshData();
            }
          });
        }
      } catch (error) {}
    },
    [projectId]
  );

  const handleSaveTask = useCallback(
    model => {
      taskListRef.current.refs.wrappedInstance.updateRow(
        selectedTaskItemId,
        model
      );
      isModalOpen.setFalse();
      notification.success({
        title: t('common.alert.changed'),
        onClose: () => {}
      });
    },
    [selectedTaskItemId]
  );

  const handleRemoveTask = useCallback(
    () => {
      taskListRef.current.refs.wrappedInstance.deleteRow(selectedTaskItemId);
      isModalOpen.setFalse();
      notification.success({
        title: t('common.alert.deleted'),
        onClose: () => {}
      });
    },
    [selectedTaskItemId]
  );

  const handleTaskItemClick = useCallback(selectedId => {
    isModalOpen.setTrue();
    setSelectedTaskItemId(selectedId);
  }, []);

  return (
    <div>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <AddLinkButton
              name={t('task.add')}
              to={`/project/${projectId}/task/new`}
            />
            <LinkButton
              color="red"
              icon="copy outline"
              labelPosition="right"
              name={t('task.template.copy')}
              onClick={isShowTemplateTask.setTrue}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <TaskList
              ref={taskListRef}
              projectId={projectId}
              onItemClick={handleTaskItemClick}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Modal
        open={isShowTemplateTask.value}
        onOK={isShowTemplateTask.setFalse}
        title={t('task.template.copy.title')}
        content={<TemplateTaskList onSelect={handleCopyTask} />}
      />

      <TaskEditLayer
        open={isModalOpen.value}
        onClose={() => {
          isModalOpen.setFalse();
        }}
        title={t('task.title.edit')}
        projectId={projectId}
        taskItemId={selectedTaskItemId}
        onSaved={handleSaveTask}
        onRemoved={handleRemoveTask}
      />
    </div>
  );
};

export default TaskListContainer;
