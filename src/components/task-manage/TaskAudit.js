import React, { Component } from 'react';
import { Item, Icon, Visibility } from 'semantic-ui-react';
import { injectIntl } from 'react-intl';
import moment from 'moment';
import api from './api';
import { LoadingBall } from '../animation';
import helpers from '../../helpers';
import map from 'lodash/map';
import { format } from '../../constants';

class TaskAudit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      offset: 0,
      limit: 10,
      hasMore: false,
      isLoading: false
    };

    this.getItems = this.getItems.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getItems();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getItems() {
    const { taskItemId } = this.props;
    const { offset, limit } = this.state;
    this.setState({ isLoading: true });
    api
      .fetchTaskAudits({ taskItemId, offset, limit })
      .then(res => {
        const merged = this.state.items.concat(res.data.items);
        if (this._isMounted) {
          this.setState({
            items: merged,
            offset: offset + limit,
            hasMore: merged.length < res.data.paging.total,
            isLoading: false
          });
        }
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }

  getItem(item, icon, meta, extra) {
    return (
      <Item key={`${item.auditEntryId}_${item.propertyName}`}>
        <Item.Content>
          <Item.Header as="h4">
            <span style={{ fontSize: '12px' }}>
              <Icon
                name={icon.name}
                color={icon.color ? icon.color.toLowerCase() : 'grey'}
                size="big"
              />
              {item.memberName}(
              {moment(item.createdDate, format.YYYYMMDDHHMM).fromNow()})
            </span>
          </Item.Header>
          <Item.Meta>{meta}</Item.Meta>
          <Item.Extra
            style={{
              textDecoration: item.isRemove === 'True' ? 'line-through' : ''
            }}
          >
            {extra}
          </Item.Extra>
        </Item.Content>
      </Item>
    );
  }

  render() {
    const { intl } = this.props;
    const { hasMore, items, isLoading } = this.state;

    return (
      <div>
        <Visibility
          continuous={hasMore}
          onBottomVisible={() => {
            if (!isLoading && hasMore) {
              this.getItems();
            }
          }}
        >
          <Item.Group>
            {map(items, item => {
              if (item.entityTypeName === 'TaskConversation') {
                const meta =
                  item.state === 0
                    ? intl.formatMessage({ id: 'task.audit.add.comment' })
                    : item.isRemove === 'True'
                      ? intl.formatMessage({ id: 'task.audit.delete.comment' })
                      : intl.formatMessage({ id: 'task.audit.change.comment' });

                const extra =
                  item.state === 0
                    ? `${item.newValue}`
                    : item.isRemove === 'True'
                      ? `${item.newValue}`
                      : `${item.oldValue} -> ${item.newValue}`;
                const icon = {
                  name: 'comment alternate outline',
                  color: 'orange'
                };

                return this.getItem(item, icon, meta, extra);
              } else if (item.entityTypeName === 'TaskAttachment') {
                const meta =
                  item.state === 0
                    ? intl.formatMessage({ id: 'task.audit.add.attach' })
                    : item.isRemove === 'True'
                      ? intl.formatMessage({ id: 'task.audit.delete.attach' })
                      : '';

                const extra =
                  item.state === 0
                    ? `${item.newValue}`
                    : item.isRemove === 'True'
                      ? item.newValue
                      : `${item.oldValue} -> ${item.newValue}`;

                const icon = {
                  name: 'file alternate outline',
                  color: ''
                };
                return this.getItem(item, icon, meta, extra);
              } else if (item.entityTypeName === 'TaskMember') {
                const meta =
                  item.state === 0
                    ? intl.formatMessage({ id: 'task.audit.add.member' })
                    : item.isRemove === 'True'
                      ? intl.formatMessage({ id: 'task.audit.delete.member' })
                      : '';

                const extra =
                  item.state === 0
                    ? `${item.newValue}`
                    : item.isRemove === 'True'
                      ? item.newValue
                      : `${item.oldValue} -> ${item.newValue}`;
                const icon = {
                  name: 'user outline',
                  color: ''
                };
                return this.getItem(item, icon, meta, extra);
              } else if (item.entityTypeName === 'TaskTag') {
                const meta =
                  item.state === 0
                    ? intl.formatMessage({ id: 'task.audit.add.tag' })
                    : item.isRemove === 'True'
                      ? intl.formatMessage({ id: 'task.audit.delete.tag' })
                      : '';

                const extra =
                  item.state === 0
                    ? `${item.newValue}`
                    : item.isRemove === 'True'
                      ? item.newValue
                      : `${item.oldValue} -> ${item.newValue}`;

                const icon = {
                  name: 'tags',
                  color: item.value1
                };
                return this.getItem(item, icon, meta, extra);
              } else if (item.entityTypeName === 'TaskCheckItem') {
                const meta =
                  item.state === 0
                    ? intl.formatMessage({ id: 'task.audit.add.checkitem' })
                    : item.isRemove === 'True'
                      ? intl.formatMessage({
                          id: 'task.audit.delete.checkitem'
                        })
                      : intl.formatMessage({
                          id: 'task.audit.change.checkitem'
                        });

                const extra =
                  item.state === 0
                    ? `${item.newValue}`
                    : item.isRemove === 'True'
                      ? `${item.newValue}`
                      : `${item.value1} ${item.oldValue} -> ${item.newValue}`;
                const icon = {
                  name: 'check',
                  color: 'green'
                };

                return this.getItem(item, icon, meta, extra);
              } else {
                let meta = '';
                let extra = '';
                let icon = {
                  name: 'edit outline',
                  color: ''
                };

                if (item.state === 0) {
                  meta = intl.formatMessage({ id: 'task.audit.created' });
                  icon.name = 'plus';
                  icon.color = 'black';
                } else {
                  switch (item.propertyName) {
                    case 'Title':
                      meta = intl.formatMessage({ id: 'task.audit.title' });
                      icon.name = 'header';
                      icon.color = 'black';
                      break;
                    case 'ScheduleFromDate':
                    case 'ScheduleToDate':
                      meta = intl.formatMessage({ id: 'task.audit.schedule' });
                      icon.name = 'calendar alternate outline';
                      icon.color = 'yellow';
                      break;
                    case 'Weight':
                      meta = intl.formatMessage({ id: 'task.audit.weight' });
                      icon.name = 'hourglass half';
                      icon.color = 'brown';
                      break;
                    case 'Rate':
                      meta = intl.formatMessage({ id: 'task.audit.rate' });
                      icon.name = 'battery empty';
                      icon.color = 'red';
                      extra = 'Open';
                      const newValue = Number(item.newValue);
                      if (newValue > 0 && newValue < 100) {
                        icon.name = 'battery half';
                        icon.color = 'blue';
                        extra = 'Ongoing';
                      }
                      if (newValue === 100) {
                        icon.name = 'battery full';
                        icon.color = 'green';
                        extra = 'Completed';
                      }
                      break;
                    case 'OrderNo':
                    case 'ParentTaskItemId':
                      meta = intl.formatMessage({ id: 'task.audit.order' });
                      icon.name = 'sort numeric down';
                      icon.color = 'black';
                      break;
                    case 'Note':
                      meta = intl.formatMessage({ id: 'task.audit.note' });
                      break;
                    default:
                      break;
                  }

                  extra = `${item.oldValue} -> ${item.newValue}`;

                  if (item.propertyName === 'ScheduleToDate') {
                    const oldValue = item.value1.split(',');
                    const oldDate = helpers.util.dateRange(
                      oldValue[0],
                      oldValue[1],
                      format.YYYY_MM_DD
                    );
                    const newValue = item.value2.split(',');
                    const newDate = helpers.util.dateRange(
                      newValue[0],
                      newValue[1],
                      format.YYYY_MM_DD
                    );

                    extra = `${oldDate} -> ${newDate}`;
                  }

                  if (
                    item.propertyName === 'OrderNo' ||
                    item.propertyName === 'ParentTaskItemId'
                  ) {
                    extra = `${item.oldValue} -> ${item.newValue}`;
                  }
                  // extra =
                  //   item.propertyName === 'OrderNo' ||
                  //   item.propertyName === 'ParentTaskItemId'
                  //     ? ``
                  //     : `${item.oldValue} -> ${item.newValue}`;
                }

                if (meta) {
                  return this.getItem(item, icon, meta, extra);
                }
              }
            })}
          </Item.Group>
        </Visibility>
        {hasMore && (
          <div style={{ margin: 'auto' }}>
            <LoadingBall />
          </div>
        )}
      </div>
    );
  }
}

export default injectIntl(TaskAudit);
