import helpers from '../../helpers';

const fetchCodeGroupById = codeGroupId => {
  return helpers.Service.get(`api/codegroups/${codeGroupId}`);
};

const fetchTasks = ({ projectId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    title: search === void 0 ? '' : search,
    offset,
    limit
  });
  return helpers.Service.get(`api/projects/${projectId}/tasks?${params}`);
};

const fetchConversation = ({ taskItemId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    comment: search === void 0 ? '' : search,
    taskItemId: taskItemId,
    offset,
    limit
  });
  return helpers.Service.get(`api/tasks/${taskItemId}/conversation?${params}`);
};

const fetchTaskAttachments = ({ taskItemId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    taskItemId: taskItemId,
    offset,
    limit
  });
  return helpers.Service.get(`api/tasks/${taskItemId}/attach?${params}`);
};

const fetchTaskCheckItems = ({ taskItemId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    taskItemId: taskItemId,
    offset,
    limit
  });
  return helpers.Service.get(`api/tasks/${taskItemId}/checkitems?${params}`);
};

const fetchTaskAudits = ({ taskItemId, search, offset, limit }) => {
  const params = helpers.qs.stringify({
    offset,
    limit
  });
  return helpers.Service.get(`api/tasks/${taskItemId}/audittrails?${params}`);
};

const fetchTask = ({ projectId, taskItemId }) => {
  return helpers.Service.get(`api/projects/${projectId}/tasks/${taskItemId}`);
};

const fetchTaskTodos = ({ projectId, name, offset, limit }) => {
  const params = helpers.qs.stringify({
    name,
    offset,
    limit
  });
  return helpers.Service.get(`api/projects/${projectId}/todos?${params}`);
};

const fetchNotifications = ({ projectId, onlyUnread, offset, limit }) => {
  const params = helpers.qs.stringify({
    onlyUnread,
    offset,
    limit
  });
  return helpers.Service.get(
    `api/projects/${projectId}/tasks.notification?${params}`
  );
};

const addTask = ({ projectId, model }) => {
  return helpers.Service.post(`api/projects/${projectId}/tasks`, model);
};

const addConversation = ({ taskItemId, model }) => {
  return helpers.Service.post(`api/tasks/${taskItemId}/conversation`, model);
};

const addTaskAttach = ({ taskItemId, model }) => {
  return helpers.Service.post(`api/tasks/${taskItemId}/attach`, model);
};

const addTaskCheckItem = ({ taskItemId, title }) => {
  return helpers.Service.post(`api/tasks/${taskItemId}/checkitems/${title}`);
};

const addCopyTaskFromTemplate = (projectId, templateTaskId) => {
  return helpers.Service.post(`api/tasks/${templateTaskId}/copy-tasks`, {
    projectId: projectId,
    templateTaskId: templateTaskId
  });
};

const addTaskReadHistory = ({ taskItemId, model }) => {
  return helpers.Service.post(`api/tasks/${taskItemId}/audit.read`, model);
};

const updateTask = ({ projectId, taskItemId, model }) => {
  return helpers.Service.put(
    `api/projects/${projectId}/tasks/${taskItemId}`,
    model
  );
};

const updateAttachTag = ({ taskItemId, taskAttachmentId, model }) => {
  return helpers.Service.put(
    `api/tasks/${taskItemId}/tags/${taskAttachmentId}`,
    model
  );
};

const changedOrderParent = ({ taskItemId, model }) => {
  return helpers.Service.put(`api/tasks/${taskItemId}/position`, model);
};

const updateConversation = ({ taskItemId, taskConversationId, model }) => {
  return helpers.Service.put(
    `api/tasks/${taskItemId}/conversation/${taskConversationId}`,
    model
  );
};

const removeTask = ({ projectId, taskItemId }) => {
  return helpers.Service.delete(
    `api/projects/${projectId}/tasks/${taskItemId}`
  );
};

const removeConversation = ({ taskItemId, taskConversationId }) => {
  return helpers.Service.delete(
    `api/tasks/${taskItemId}/conversation/${taskConversationId}`
  );
};

const fetchArchivePresignedurl = ({ taskItemId, taskAttachmentId }) => {
  return helpers.Service.get(
    `api/tasks/${taskItemId}/attach/${taskAttachmentId}/req.presignedurl/`
  );
};

const removeTaskAttach = ({ taskItemId, taskAttachmentId }) => {
  return helpers.Service.delete(
    `api/tasks/${taskItemId}/attach/${taskAttachmentId}`
  );
};

const removeTaskCheckItem = ({ taskItemId, taskCheckItemId }) => {
  return helpers.Service.delete(
    `api/tasks/${taskItemId}/checkitems/${taskCheckItemId}`
  );
};

export default {
  addTask,
  addConversation,
  addTaskAttach,
  addTaskCheckItem,
  addCopyTaskFromTemplate,
  addTaskReadHistory,
  updateTask,
  updateAttachTag,
  changedOrderParent,
  updateConversation,
  removeTask,
  removeConversation,
  removeTaskAttach,
  removeTaskCheckItem,
  fetchCodeGroupById,
  fetchTasks,
  fetchConversation,
  fetchTask,
  fetchTaskTodos,
  fetchTaskAttachments,
  fetchTaskCheckItems,
  fetchArchivePresignedurl,
  fetchTaskAudits,
  fetchNotifications
};
