import React, { useState, useCallback, Fragment } from 'react';
import { Header, Grid, Tab, Loader } from 'semantic-ui-react';
import { TaskListContainer, TaskTodoList, TaskTimelineList } from './';
import { notification } from '../modal';
import TaskEditLayer from './TaskEditLayer';
import { useRouteMatch } from 'react-router-dom';
import { useFetch, ListModel, useFormatMessage } from '../../hooks';
import CopyRedirectUrl from '../modules/CopyRedirectUrl';

const TaskDashBoard = ({ history }) => {
  const match = useRouteMatch();
  const t = useFormatMessage();
  const { active, projectId } = match.params;
  const activeIndex = active === 'my' ? 0 : active === 'all' ? 1 : 2;

  const [task, setTask] = useState({ show: false, taskItemId: 0 });

  const url = `api/projects/${projectId}/todos`;
  const [todo, doFetch] = useFetch(`${url}`, new ListModel());

  const getActiveName = activeIndex => {
    switch (activeIndex) {
      case 0:
        return 'my';
      case 1:
        return 'all';
      default:
        return 'timeline';
    }
  };

  const handleClickTab = useCallback((event, data, active) => {
    const activeName = getActiveName(data.activeIndex);
    history.push(`/project/${projectId}/task/portal/${activeName}`);
  }, []);

  const handleClickTodo = useCallback(({ id }) => {
    setTask({ show: true, taskItemId: id });
  }, []);

  const handleCloseTask = useCallback(() => {
    setTask({ show: false });
  }, []);

  const handleSaveTask = useCallback(model => {
    setTask({ show: false });
    notification.success({
      title: t('common.alert.changed'),
      onClose: () => {
        doFetch(`${url}?bust=${Date.now()}`);
      }
    });
  }, []);

  const handleRemoveTask = useCallback(() => {
    setTask({ show: false });
    notification.success({
      title: t('common.alert.deleted'),
      onClose: () => {
        doFetch(`${url}?bust=${Date.now()}`);
      }
    });
  }, []);

  return (
    <Fragment>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as="h3" dividing>
              {t('task.title')}
              <Header.Subheader>{t('task.title.description')}</Header.Subheader>
              <CopyRedirectUrl />
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Tab
              menu={{ secondary: true, pointing: true, borderless: true }}
              activeIndex={activeIndex}
              onTabChange={handleClickTab}
              panes={[
                {
                  menuItem: t('project.my.tasks'),
                  render: () => (
                    <Tab.Pane attached={false} basic>
                      <TaskTodoList
                        projectId={projectId}
                        tasks={todo.data.items}
                        onItemClick={handleClickTodo}
                      />
                      {todo.loading && <Loader active inline="centered" />}
                    </Tab.Pane>
                  )
                },
                {
                  menuItem: t('task'),
                  render: () => (
                    <Tab.Pane attached={false} basic>
                      <TaskListContainer projectId={projectId} />
                    </Tab.Pane>
                  )
                },
                {
                  menuItem: t('task.timeline'),
                  render: () => (
                    <Tab.Pane attached={false} basic>
                      <TaskTimelineList projectId={projectId} />
                    </Tab.Pane>
                  )
                }
              ]}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16} />
        </Grid.Row>
      </Grid>

      <TaskEditLayer
        open={task.show}
        onClose={handleCloseTask}
        title={t('task.title.edit')}
        projectId={projectId}
        taskItemId={task.taskItemId}
        onSaved={handleSaveTask}
        onRemoved={handleRemoveTask}
      />
    </Fragment>
  );
};

export default TaskDashBoard;
