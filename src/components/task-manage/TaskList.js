import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import JqxTreeGrid from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxtreegrid';
import { injectIntl } from 'react-intl';
import helpers from '../../helpers';
import { Grid, Icon } from 'semantic-ui-react';
import { Modal, notification } from '../modal';
import { ProgressBar } from '../modules';
import { TaskSortable } from './';
import defaults from 'lodash/defaults';
import orderBy from 'lodash/orderBy';
import filter from 'lodash/filter';
import { format } from '../../constants';

const gridName = 'taskGrid';

class TaskList extends Component {
  constructor(props) {
    super(props);

    const intl = props.intl;
    const self = this;

    const data = {
      dataType: 'json',
      cache: true,
      dataFields: [
        { name: 'taskItemId', type: 'number' },
        { name: 'parentTaskItemId', type: 'number' },
        { name: 'orderNo', type: 'number' },
        { name: 'isTask', type: 'bool' },
        { name: 'actualRate', type: 'number' },
        { name: 'expectedRate', type: 'number' },
        { name: 'title', type: 'string' },
        { name: 'scheduleFromDate', type: 'date' },
        { name: 'scheduleToDate', type: 'date' },
        { name: 'note', type: 'string' },
        { name: 'tags', type: 'string' },
        { name: 'taskMembers', type: 'string' },
        { name: 'weight', type: 'number' },
        { name: 'rate', type: 'number' },
        { name: 'taskCheckItemSummary', type: 'string' },
        { name: 'children', type: 'array' },
        { name: 'expanded', type: 'bool' }
      ],
      hierarchy: {
        root: 'children'
        // keyDataField: { name: 'taskItemId' },
        // parentDataField: { name: 'parentTaskItemId' }
      },
      id: 'taskItemId',
      url: `${__API_URL__}api/projects/${props.projectId}/tasks`,
      beforeLoadComplete: function(records) {
        records = orderBy(records, ['orderNo'], ['asc']);
        let actualRate = 0;
        let expectedRate = 0;
        const parents = filter(records, { parentTaskItemId: 0 });
        for (let i = 0; i < parents.length; i++) {
          const parent = parents[i];
          actualRate += parent.actualRate;
          expectedRate += parent.expectedRate;
        }

        const data = self.transformToTree(records);

        const root = [
          {
            taskItemId: 0,
            parentTaskItemId: -1,
            title: 'Root',
            scheduleFromDate: '',
            scheduleToDate: '',
            rate:
              expectedRate > 0
                ? Math.round((actualRate / expectedRate) * 100)
                : 0,
            note: '',
            expanded: true,
            children: []
          }
        ];
        root[0].children = data;
        return root;
      }
    };

    this.state = {
      isChangedOrder: false,
      rows: [],
      showOrder: false,
      taskItemId: 0,
      search: '',
      isExpand: true,
      isFetching: true,
      source: new jqx.dataAdapter(data, {
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', helpers.Auth.jwt);
        },
        formatData: data => {
          return {
            title: this.state.search
          };
        },
        loadError: error => {
          notification.error({});
        },
        loadComplete: data => {
          if (this._isMounted) {
            self.setState({ isFetching: false });
          }
        }
      }),
      columns: [
        {
          text: intl.formatMessage({ id: 'common.id' }),
          dataField: 'taskItemId',
          hidden: true
        },
        {
          text: intl.formatMessage({ id: 'task' }),
          dataField: 'title',
          width: '250',
          cellsRenderer: (row, column, value) => {
            return `<span data-row=${row}
            class=taskGrid_linkbutton
            style="margin-right : 5px; margin-top: 8px; cursor: pointer;">${value}</span>`;
          }
        },
        {
          text: intl.formatMessage({ id: 'task.istask' }),
          dataField: 'isTask',
          width: '50',
          cellsRenderer: (row, column, value) => {
            return value === true
              ? '<i aria-hidden="true" class="check icon" />'
              : '';
          }
        },
        {
          text: intl.formatMessage({ id: 'task.days' }),
          dataField: 'days',
          width: '40'
        },
        {
          text: intl.formatMessage({ id: 'task.schedule' }),
          dataField: 'schedule',
          width: '170'
        },
        {
          text: intl.formatMessage({ id: 'task.assigned' }),
          dataField: 'taskMembers',
          width: '150'
        },
        {
          text: intl.formatMessage({ id: 'task.tag' }),
          dataField: 'tags',
          width: '200'
        },
        {
          text: intl.formatMessage({ id: 'task.weight' }),
          dataField: 'weight',
          width: '80'
        },
        {
          text: intl.formatMessage({ id: 'task.rate' }),
          dataField: 'rate',
          cellsRenderer: (row, column, value) => {
            return `<div data-row=${row} data-rate=${value}
            class='progress'></div>`;
          }
        },
        {
          text: intl.formatMessage({ id: 'task.checklist' }),
          dataField: 'taskCheckItemSummary',
          width: '80'
        }
      ]
    };

    this.refreshData = this.refreshData.bind(this);
    this.expandOrCollapseRows = this.expandOrCollapseRows.bind(this);
    this.transformToTree = this.transformToTree.bind(this);
    this.updateRow = this.updateRow.bind(this);
    this.deleteRow = this.deleteRow.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.projectId !== this.props.projectId ||
      nextState.taskItemId !== this.state.taskItemId ||
      nextState.isExpand !== this.state.isExpand ||
      nextState.rows !== this.state.rows ||
      nextState.showOrder !== this.state.showOrder ||
      nextState.isChangedOrder !== this.state.isChangedOrder ||
      nextState.isFetching !== this.state.isFetching
    );
  }

  refreshData() {
    if (this.state.isFetching === false) {
      this.setState({ isFetching: true }, () => {
        this.refs[gridName].updateBoundData('cells');
      });
    }
  }

  updateRow(key, model) {
    const grid = this.refs[gridName];
    grid.updateRow(key, {
      ...model,
      title: `${model.title}|^|`,
      days: helpers.util.diffDays(model.scheduleFromDate, model.scheduleToDate),
      schedule: helpers.util.dateRange(
        model.scheduleFromDate,
        model.scheduleToDate,
        format.YYYY_MM_DD
      ),
      taskMembers: this.memebersToString(model.members),
      weight: model.weight,
      rate: model.rate,
      tags: this.tagsToString(model.tags),
      taskCheckItemSummary: `${
        filter(model.checkitems, checkItem => {
          return checkItem.isComplete === true;
        }).length
      } / ${model.checkitems.length}`
    });
  }

  deleteRow(key) {
    const grid = this.refs[gridName];
    grid.deleteRow(key);
  }

  memebersToString(taskMembers) {
    let members = '';
    for (let j = 0; j < taskMembers.length; j++) {
      const taskMember = taskMembers[j];
      members += `${taskMember.name},`;
    }
    return members;
  }

  tagsToString(taskTags) {
    let tags = '';
    for (let i = 0; i < taskTags.length; i++) {
      const tag = taskTags[i];
      tags += `<a class="ui ${tag.color.toLowerCase()} label mini ">${
        tag.name
      }</a>`;
    }
    return tags;
  }

  transformToTree(arr) {
    let nodes = {};
    const self = this;
    return arr.filter(function(obj) {
      var id = obj['taskItemId'],
        parentId = obj['parentTaskItemId'];

      obj['schedule'] = helpers.util.dateRange(
        obj['scheduleFromDate'],
        obj['scheduleToDate'],
        format.YYYY_MM_DD
      );

      obj['days'] = helpers.util.diffDays(
        obj['scheduleFromDate'],
        obj['scheduleToDate']
      );
      obj['expanded'] = true;

      obj['taskMembers'] = self.memebersToString(obj['taskMembers']);
      obj['tags'] = self.tagsToString(obj['tags']);

      nodes[id] = defaults(obj, nodes[id], { children: [] });
      parentId &&
        (nodes[parentId] = nodes[parentId] || { children: [] })[
          'children'
        ].push(obj);

      return !parentId;
    });
  }

  expandOrCollapseRows() {
    const { isExpand } = this.state;
    if (isExpand) {
      this.refs[gridName].collapseAll();
    } else {
      this.refs[gridName].expandAll();
    }
    this.setState({ isExpand: !isExpand });
  }

  render() {
    const { refreshData } = this;
    const { intl } = this.props;
    const { isFetching } = this.state;
    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <Icon name="refresh" loading={isFetching} />
              <a style={{ cursor: 'pointer' }} onClick={refreshData}>
                {intl.formatMessage({ id: 'comment.refresh' })}
              </a>
              {' | '}
              {this.state.isExpand ? (
                <Icon name="caret right" />
              ) : (
                <Icon name="caret down" />
              )}
              <a
                style={{ cursor: 'pointer' }}
                onClick={this.expandOrCollapseRows}
              >
                {this.state.isExpand
                  ? intl.formatMessage({ id: 'common.collapse' })
                  : intl.formatMessage({ id: 'common.expand' })}
              </a>
              {' | '}
              <Icon name="sort content ascending" />
              <a
                style={{ cursor: 'pointer' }}
                onClick={() => {
                  this.setState({
                    showOrder: true,
                    rows: this.refs[gridName].getRows()
                  });
                }}
              >
                {intl.formatMessage({ id: 'common.reordering' })}
              </a>
            </Grid.Column>
            <Grid.Column width={16} style={{ marginBottom: '3em' }}>
              <JqxTreeGrid
                ref={gridName}
                width={'100%'}
                source={this.state.source}
                columns={this.state.columns}
                columnsResize={true}
                sortable={true}
                autoRowHeight={false}
                filterable={true}
                filterMode={'advanced'}
                rendered={() => {
                  let progressContainers = document.getElementsByClassName(
                    'progress'
                  );
                  for (let i = 0; i < progressContainers.length; i++) {
                    const element = progressContainers[i];

                    ReactDOM.render(
                      <ProgressBar percent={element.dataset.rate} />,
                      element
                    );
                  }

                  let linkButtonsContainers = document.getElementsByClassName(
                    `taskGrid_linkbutton`
                  );
                  for (let i = 0; i < linkButtonsContainers.length; i++) {
                    const element = linkButtonsContainers[i];

                    // if root is not
                    if (element.dataset.row > 0) {
                      let changed = false;
                      if (element.innerText.indexOf('|^|') > -1) {
                        changed = true;
                      }
                      ReactDOM.render(
                        <span>
                          <a
                            onClick={() => {
                              this.props.onItemClick(element.dataset.row);
                            }}
                          >
                            {changed
                              ? element.innerText.replace('|^|', ' ')
                              : element.innerText}
                          </a>
                          {changed ? <Icon color="red" name="sync" /> : null}
                        </span>,
                        element
                      );
                    }
                  }
                }}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Modal
          open={this.state.showOrder}
          onOK={() => {
            const isChanged = this.state.isChangedOrder;
            this.setState({ showOrder: false, isChangedOrder: false });
            if (isChanged) {
              refreshData();
            }
          }}
          title={intl.formatMessage({ id: 'common.reordering' })}
          content={
            <TaskSortable
              data={this.state.rows}
              onChange={isChanged => {
                this.setState({ isChangedOrder: isChanged });
              }}
            />
          }
        />
      </div>
    );
  }
}

export default injectIntl(TaskList, { withRef: true });
