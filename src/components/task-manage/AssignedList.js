import React, { useCallback, memo } from 'react';
import { Dropdown } from 'semantic-ui-react';
import { useProjectMemberKeyPair } from '../../hooks';

const AssignedList = memo(
  ({ name, onChange, placeholder, projectId, value }) => {
    const member = useProjectMemberKeyPair({
      projectId
    });

    const renderLabel = useCallback(label => {
      return {
        content: `${label.text}`,
        image: {
          avatar: label.image.avatar,
          src: label.image.src,
          size: 'mini'
        }
      };
    });

    return (
      <Dropdown
        placeholder={placeholder}
        name={name}
        fluid
        multiple
        selection
        options={member.items}
        value={value}
        onChange={onChange}
        renderLabel={renderLabel}
      />
    );
  }
);

export default AssignedList;
