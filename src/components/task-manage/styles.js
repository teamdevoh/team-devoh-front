import styled from 'styled-components';
import { Grid, Label, List, Placeholder, Segment } from 'semantic-ui-react';
import { TimelineItem } from 'vertical-timeline-component-for-react';

export const TreeWrapper = styled(Grid.Column)`
  margin-bottom: 6em;
`;

export const CompleteSegment = styled(Segment)`
  &&& {
    text-align: center;
    background-color: #e1e5ed;
    margin-bottom: 0.5em;
  }
`;

export const Holder = styled(Placeholder)`
  &&& {
    height: 500px;
  }
`;

export const TimeLineItemWrapper = styled(TimelineItem)`
  color: #e86971;
`;

export const TimeLineMemberWrapper = styled(List.Item)`
  &&& {
    text-align: center;
  }
`;

export const DateRangeSpan = styled.span`
  float: left;
  width: 60%;
`;

export const ProgressBarSpan = styled.span`
  float: right;
  width: 40%;
`;

export const StatusLabel = styled(Label)`
  float: right;
`;
