import React, { useCallback, useState, useEffect, memo } from 'react';
import { Comment, Dropdown, Image } from 'semantic-ui-react';
import { Reply } from './';
import moment from 'moment';
import helpers from '../../helpers';
import { useFormatMessage, useBoolean } from '../../hooks';
import { format } from '../../constants';

const UserFace = memo(({ imageSrc }) => {
  return (
    <div className="avatar">
      <Image src={imageSrc} rounded avatar={true} />
    </div>
  );
});

const Author = memo(({ author }) => {
  return <Comment.Author>{author}</Comment.Author>;
});

const WrittenDate = memo(({ date }) => {
  return (
    <Comment.Metadata style={{ marginLeft: '0' }}>
      {moment(date, format.YYYYMMDDHHMM).fromNow()}
    </Comment.Metadata>
  );
});

const ContextMenu = memo(
  ({ displayText, onEditClick, onRemoveClick, onCloseClick }) => {
    const t = useFormatMessage();
    return (
      <Dropdown text={displayText}>
        <Dropdown.Menu>
          <Dropdown.Item text="---------" onClick={onCloseClick} />
          <Dropdown.Item text={t('common.edit')} onClick={onEditClick} />
          <Dropdown.Item text={t('common.delete')} onClick={onRemoveClick} />
        </Dropdown.Menu>
      </Dropdown>
    );
  }
);

const ReplyButton = memo(({ onClick }) => {
  const t = useFormatMessage();
  return <div onClick={onClick}>{t('comment.reply')}</div>;
});

const CommentItem = ({
  id,
  author,
  faceId,
  date,
  comment,
  children,
  creator,
  onReplyRemove,
  onReplyAdd,
  onReplyUpdate
}) => {
  const isShowEdit = useBoolean(false);
  const isShowReply = useBoolean(false);
  const [face, setFace] = useState('');
  const [newComment, setNewComment] = useState(comment);

  useEffect(() => {
    helpers.util.getFace(faceId).then(imageUrl => {
      setFace(imageUrl);
    });

    return () => {};
  }, []);

  const handleEmptyClick = useCallback(() => {
    isShowEdit.setFalse();
  }, []);

  const handleEditClick = useCallback(() => {
    isShowEdit.setTrue();
  }, []);

  const handleReplyRemove = useCallback(() => {
    onReplyRemove(id);
  }, []);

  const handleReplyChange = useCallback((event, { name, value }) => {
    setNewComment(value);
  }, []);

  const handleReplyUpdate = useCallback(value => {
    onReplyUpdate(value, id);
    isShowEdit.setFalse();
  }, []);

  const handleReplyAdd = useCallback(value => {
    onReplyAdd(value, id);
    isShowReply.setFalse();
    setNewComment('');
  }, []);

  const handleClick = useCallback(event => {
    isShowReply.toggle();
  }, []);

  return (
    <Comment>
      <UserFace imageSrc={face} />
      <Comment.Content>
        <Author author={author} />
        <WrittenDate date={date} />
        <Comment.Text>
          {helpers.Identity.profile.id === creator && (
            <ContextMenu
              displayText={comment}
              onEditClick={handleEditClick}
              onRemoveClick={handleReplyRemove}
              onCloseClick={handleEmptyClick}
            />
          )}
          {helpers.Identity.profile.id !== creator && <div>{comment}</div>}
          {isShowEdit.value && (
            <Reply
              onClick={handleReplyUpdate}
              onChange={handleReplyChange}
              value={newComment}
            />
          )}
        </Comment.Text>
        <Comment.Actions>
          <Comment.Action>
            <ReplyButton onClick={handleClick} />
          </Comment.Action>
        </Comment.Actions>
        {isShowReply.value && <Reply onClick={handleReplyAdd} />}
      </Comment.Content>
      {children}
    </Comment>
  );
};

export default CommentItem;
