import React, { useCallback } from 'react';
import { Dropdown } from 'semantic-ui-react';
import { useCodes } from '../../hooks';
import { codeGroup } from '../../constants';

const TaskTagList = ({ name, onChange, placeholder, required, value }) => {
  const [items] = useCodes({ codeGroupId: codeGroup.Tag });

  const renderLabel = useCallback(label => {
    return {
      color: label.tag1.toLowerCase(),
      content: `${label.text}`,
      size: 'mini'
    };
  }, []);

  return (
    <Dropdown
      placeholder={placeholder}
      name={name}
      fluid
      required={required}
      multiple
      selection
      options={items}
      value={value}
      onChange={onChange}
      renderLabel={renderLabel}
    />
  );
};

export default TaskTagList;
