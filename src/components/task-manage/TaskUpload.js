import React, { useCallback, useState, memo } from 'react';
import history from '../../helpers/history';
import api from './api';
import { Confirm } from '../modal';
import helpers from '../../helpers';
import { UploadLayer, FileList, FakeFileList } from '../modules';
import { Dimmer, Loader, Segment } from 'semantic-ui-react';
import { useUpload, useFormatMessage } from '../../hooks';
import map from 'lodash/map';
import toArray from 'lodash/toArray';
import { useUser } from '../account-manage/hooks';
import {
  useTaskAttachmentList,
  useTaskAttachDownload,
  useTaskAttachRemove
} from './hooks';
import MultiDownloadButton from '../modules/MultiDownloadButton';
import FileViewerWrapper from '../viewer/FileViewerWrapper';

const TaskUpload = ({ projectId, taskItemId, isUpload, onUploadClose }) => {
  const t = useFormatMessage();
  const file = useUpload();
  const [search, setSearch] = useState({
    open: false,
    tag: '',
    location: ''
  });
  const [my] = useUser();
  const [attach, setFiles] = useTaskAttachmentList(taskItemId);
  const attachDown = useTaskAttachDownload(taskItemId);
  const attachRemove = useTaskAttachRemove(taskItemId);
  const [fileInfo, setFileInfo] = useState();
  const [viewerOpen, setViewerOpen] = useState(false);

  const handleUpdateTag = useCallback(
    async (taskAttachmentId, tags) => {
      const res = await api.updateAttachTag({
        taskItemId,
        taskAttachmentId,
        model: { tag: tags.join('') }
      });
      if (res && res.data) {
        const files = attach.items;
        map(files, file => {
          if (file.key === taskAttachmentId) {
            file.tags = tags.join('');
          }
        });

        setFiles({ ...files });
      }
    },
    [attach.items]
  );

  const handleRemoveAttach = useCallback(
    async taskAttachmentId => {
      const is = attachRemove.remove(taskAttachmentId);
      if (is) {
        setFiles(
          toArray(attach.items).filter(file => {
            return file.key !== taskAttachmentId;
          })
        );
      }
    },
    [attach.items]
  );

  const handleTagClick = useCallback(tag => {
    setSearch({
      open: true,
      tag: tag,
      location: `/project/${projectId}/search?keyword=${tag}`
    });
  }, []);

  const handleDrop = async acceptedFiles => {
    onUploadClose();

    file.upload(acceptedFiles, async ids => {
      const res = await api.addTaskAttach({
        taskItemId,
        model: { fileStorageIds: ids }
      });
      let newItems = [];
      await ids.forEach(async (id, index) => {
        const attached = _.find(res.data, { key: id });
        const meta = attached.value.split('|');
        const newTaskAttachmentId = meta[0];
        const created = meta[1];
        const acceptedFile = acceptedFiles[index];

        const fileObj = await helpers.file.setFileInfo({
          extension: acceptedFile.type,
          id: newTaskAttachmentId,
          originalName: acceptedFile.name,
          size: acceptedFile.size,
          created: created,
          creator: my.profile.name,
          thumbnailApiUrl: `api/tasks/${taskItemId}/attach.thumbnail/${newTaskAttachmentId}`,
          tag: ''
        });

        newItems.push(fileObj);
      });

      setFiles(toArray(attach.items).concat(newItems));
    });
  };

  const getDownloadApi = useCallback(
    id => {
      return `api/tasks/${taskItemId}/attach/${id}`;
    },
    [taskItemId]
  );

  const handleViewerClick = useCallback(
    async ({ fileInfo = viewer, id }) => {
      setFileInfo(fileInfo);
      setViewerOpen(true);
    },
    [attach.items]
  );

  const fetchSignedUrl = fileInfo => {
    return api.fetchArchivePresignedurl({
      taskItemId,
      taskAttachmentId: fileInfo.key
    });
  };

  const handleViewerClose = () => {
    setViewerOpen(false);
  };

  const getDownloadUrl = useCallback(
    key => {
      return `api/tasks/${taskItemId}/attach/${key}`;
    },
    [taskItemId]
  );

  return (
    <div>
      <FileViewerWrapper
        fileInfo={fileInfo}
        fetchSignedUrl={fetchSignedUrl}
        open={viewerOpen}
        onClose={handleViewerClose}
        getDownloadUrl={getDownloadUrl}
      />
      <UploadLayer
        open={isUpload}
        onClose={onUploadClose}
        title={t('common.attachment.upload')}
        onDrop={handleDrop}
      />
      <Segment>
        <Dimmer active={attachDown.loading} inverted>
          <Loader size="small">Loading</Loader>
        </Dimmer>

        <FileList
          files={attach.items}
          onDownloadClick={attachDown.download}
          onDeleteClick={handleRemoveAttach}
          onUpdateTag={handleUpdateTag}
          onTagClick={handleTagClick}
          MultiDownloadButton={MultiDownloadButton({ getDownloadApi })}
          onViewerClick={handleViewerClick}
        />
      </Segment>

      <FakeFileList items={file.fakes} />

      <Confirm
        title={t('common.search')}
        content={t('project.search.is', { name: search.tag })}
        open={search.open}
        onOK={() => {
          history.push(search.location);
        }}
        onCancel={() => setSearch({ ...search, open: false })}
      />
    </div>
  );
};

export default memo(TaskUpload);
