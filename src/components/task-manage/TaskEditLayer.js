import React from 'react';
import { TaskEdit } from '../task-manage';
import { Modal } from '../modal';

const TaskEditLayer = ({
  open,
  projectId,
  title,
  taskItemId,
  onClose,
  onSaved,
  onRemoved
}) => {
  return (
    <Modal
      open={open}
      onOK={onClose}
      title={title}
      content={
        <TaskEdit
          projectId={projectId}
          taskItemId={taskItemId}
          onSaved={onSaved}
          onRemoved={onRemoved}
        />
      }
    />
  );
};

export default TaskEditLayer;
