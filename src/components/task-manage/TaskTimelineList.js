import React, { useState, useCallback, useEffect } from 'react';
import { Timeline } from 'vertical-timeline-component-for-react';
import api from './api';
import './TaskTimeline.css';
import helpers from '../../helpers';
import TaskTimelineListItem from './TaskTimelineListItem';
import { useFormatMessage } from '../../hooks';
import filter from 'lodash/filter';
import map from 'lodash/map';
import { format } from '../../constants';

const TaskTimelineList = ({ projectId }) => {
  const t = useFormatMessage();
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch();
    return () => {};
  }, []);

  const fetch = async () => {
    const items = await getTasks();
    const results = await getFace(items);

    setItems(results);
  };

  const getTasks = useCallback(async () => {
    const res = await api.fetchTasks({ projectId });
    const parents = filter(res.data.items, { parentTaskItemId: 0 });

    return parents;
  }, []);

  const getFace = useCallback(async items => {
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      for (let j = 0; j < item.taskMembers.length; j++) {
        const taskMember = item.taskMembers[j];
        const url = await helpers.util.getFace(taskMember.myFaceFileStorageId);
        taskMember.face = url;
      }
    }

    return items;
  }, []);

  return (
    <Timeline lineColor={'#ddd'}>
      {map(items, item => {
        const diffDays = helpers.util.diffDays(
          item.scheduleFromDate,
          item.scheduleToDate
        );
        const date = helpers.util.dateRange(
          item.scheduleFromDate,
          item.scheduleToDate,
          format.YYYY_MM_DD
        );

        return (
          <TaskTimelineListItem
            key={item.taskItemId}
            date={date.length > 0 ? date : t('task.timeline.nodate')}
            title={`${item.title}, ${diffDays} Days`}
            rate={item.rate}
            note={item.note}
            tags={item.tags}
            taskMembers={item.taskMembers}
          />
        );
      })}
    </Timeline>
  );
};

export default TaskTimelineList;
