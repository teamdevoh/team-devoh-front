import React, { PureComponent } from 'react';
import { Comment } from 'semantic-ui-react';
import { CommentItem } from './';
import map from 'lodash/map';

class ConversationListItem extends PureComponent {
  getComment = ({
    id,
    memberName,
    memberId,
    fileStorageId,
    date,
    comment,
    children
  }) => {
    const { onReplyRemove, onReplyUpdate, onReplyAdd } = this.props;
    if (children.length > 0) {
      return (
        <CommentItem
          key={id}
          id={id}
          author={memberName}
          faceId={fileStorageId}
          date={date}
          comment={comment}
          creator={memberId}
          onReplyAdd={onReplyAdd}
          onReplyUpdate={onReplyUpdate}
          onReplyRemove={onReplyRemove}
        >
          <Comment.Group threaded collapsed={false}>
            {map(children, child => {
              return this.getComment({
                id: child.id,
                memberName: child.memberName,
                memberId: child.memberId,
                fileStorageId: child.fileStorageId,
                date: child.date,
                comment: child.comment,
                children: child.children
              });
            })}
          </Comment.Group>
        </CommentItem>
      );
    } else {
      return (
        <CommentItem
          key={id}
          id={id}
          author={memberName}
          faceId={fileStorageId}
          date={date}
          comment={comment}
          creator={memberId}
          onReplyAdd={onReplyAdd}
          onReplyUpdate={onReplyUpdate}
          onReplyRemove={onReplyRemove}
        />
      );
    }
  };

  render() {
    const {
      id,
      memberName,
      memberId,
      fileStorageId,
      date,
      comment,
      children
    } = this.props;

    return this.getComment({
      id,
      memberName,
      memberId,
      fileStorageId,
      date,
      comment,
      children
    });
  }
}

export default ConversationListItem;
