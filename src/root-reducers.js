import { combineReducers } from 'redux';
import account from './states/account';
import identity from './states/identity';
import shared from './states/shared';
import activity from './states/activity';
import subject from './states/subject';
import project from './states/project';
import poppk from './states/poppk';

export default combineReducers({
  [account.constants.NAME]: account.reducer,
  [identity.constants.NAME]: identity.reducer,
  [shared.constants.NAME]: shared.reducer,
  [activity.constants.NAME]: activity.reducer,
  [subject.constants.NAME]: subject.reducer,
  [project.constants.NAME]: project.reducer,
  [poppk.constants.NAME]: poppk.reducer
});
