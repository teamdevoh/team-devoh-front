module.exports = {
  memberList: [
    {
      projectMemberId: 1,
      memberId: 1,
      face: 'src/styleguide/tempface.jpg',
      memberName: '홍길동',
      teamName: '개발팀',
      rate: '30',
      scheduleFromDate: '2019-01-01',
      scheduleToDate: '2019-12-31',
      projectSites: ['부산백병원', '일산백병원']
    },
    {
      projectMemberId: 2,
      memberId: 2,
      face: 'src/styleguide/tempface.jpg',
      memberName: '이순신',
      teamName: '개발팀',
      rate: '50',
      scheduleFromDate: '2019-01-01',
      scheduleToDate: '2019-12-31',
      projectSites: ['일산백병원']
    }
  ]
};
