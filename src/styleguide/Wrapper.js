import 'babel-polyfill';
import React from 'react';
import { createStore, compose, applyMiddleware } from 'redux';
import { IntlProvider } from 'react-intl';
import { InjectIntlContext } from '@comparaonline/react-intl-hooks';
import 'semantic-ui-css/semantic.min.css';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';
import { logger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../root-reducers';
import reduxCatch from 'redux-catch';
// app messages
import local_en from '../locales/en.json';
import local_ko from '../locales/ko.json';
import moment from 'moment';
import helpers from '../helpers';

import { addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import ko from 'react-intl/locale-data/ko';
import 'moment/locale/ko';
import { useDispatch } from 'react-redux';
import { createHashHistory } from 'history';
import { identity } from '../states';
addLocaleData([...en, ...ko]);
export const history = createHashHistory();

const appMessages = {
  en: local_en,
  ko: local_ko
};

const userLanguage = window.localStorage.getItem(helpers.storagekeys.Locale);
const language = userLanguage || navigator.language.split(/[-_]/)[0];
// if (process.env.NODE_ENV !== 'production') {
//   System.import('react-addons-perf').then(Perf => (window.Perf = Perf));
// }
moment.locale(language);

function configureStore(initialState) {
  const middlewares = [
    reduxCatch(errorHandler),
    // thunk middleware can also accept an extra argument to be passed to each thunk action
    // https://github.com/gaearon/redux-thunk#injecting-a-custom-argument
    thunkMiddleware
  ];

  if (process.env.NODE_ENV === 'development') {
    middlewares.push(logger);
  }

  function errorHandler(error, getState, lastAction, dispatch) {
    console.log(error);
    //dispatch(action.errors(error));
  }

  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middlewares),
      window.devToolsExtension
        ? window.devToolsExtension()
        : f => {
            // add support for Redux dev tools
            return f;
          }
    )
  );

  // if (module.hot) {
  //   // Enable Webpack hot module replacement for reducers
  //   module.hot.accept(rootReducer, () => {
  //     const nextReducer = require(rootReducer).default; // eslint-disable-line global-require
  //     store.replaceReducer(nextReducer);
  //   });
  // }

  return store;
}

export const store = configureStore();

const AppProvider = ({ contexts, children }) =>
  contexts.reduce(
    (prev, context) =>
      React.createElement(context, {
        children: prev
      }),
    children
  );

const Wrapper = ({ children }) => {
  const dispatch = useDispatch();

  const profile = {
    memberId: 1,
    name: '오도원',
    loginName: 'sysadmin',
    avatar: null,
    myFaceFileStorageId: 633,
    roles: ['Project_Edit', 'CanRemove', 'CanSave']
  };

  helpers.Identity.setProfile({
    id: profile.memberId,
    name: profile.name,
    loginName: profile.loginName,
    faceId: profile.myFaceFileStorageId,
    roles: profile.roles
  });

  dispatch(identity.actions.setUser(profile));

  return (
    <IntlProvider locale={language} messages={appMessages[language]}>
      <InjectIntlContext>
        <AppProvider contexts={[]}>{children}</AppProvider>
      </InjectIntlContext>
    </IntlProvider>
  );
};

export default Wrapper;
