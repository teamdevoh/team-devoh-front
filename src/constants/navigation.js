import * as role from './role';

export const FirstWorkByVariable = {
  cohort: 'collection-board',
  tbdrug: 'collection-board',
  dilicohort: 'collection-board',
  poppk: 'collection-board'
};

export const WorkItemGroup = {
  Common: 'Common',
  Cohort: 'Cohort',
  Sample: 'Sample',
  TBDrug: 'TBDrug',
  DILI: 'DILICohort',
  Console: 'Console',
  PopPK: 'PopPK'
};

export const WorkItem = Object.freeze({
  Project: [
    {
      id: 'home',
      name: 'Home',
      route: '/project/{projectId}',
      icon: 'home',
      locale: 'common.home',
      dependencyProject: true,
      role: role.Project_Read,
      active: false,
      groups: [WorkItemGroup.Common]
    },
    // {
    //   id: 'notify',
    //   name: 'Notify',
    //   route: '/project/{projectId}/notify',
    //   icon: 'alarm',
    //   locale: 'project.notification.title',
    //   dependencyProject: true,
    //   role: role.role.Task_Read,
    //   active: false,
    //   groups: [WorkItemGroup.Common]
    // },
    {
      id: 'conversation',
      name: 'Conversation',
      route: '/project/{projectId}/conversation',
      icon: 'conversation',
      locale: 'post.title',
      dependencyProject: true,
      role: role.Post_Read,
      active: false,
      groups: [WorkItemGroup.Common]
    },
    {
      id: 'task',
      name: 'Task',
      route: '/project/{projectId}/task/portal/all',
      icon: 'tasks',
      locale: 'task',
      dependencyProject: true,
      role: role.Task_Read,
      active: false,
      groups: [WorkItemGroup.Common]
    },
    {
      id: 'collection-dashboard',
      name: 'Dashboard',
      route: '/project/{projectId}/collection/{variable}/dashboard',
      icon: 'dashboard',
      locale: 'dashboard',
      dependencyProject: true,
      role: role.Activity_Dashboard_Allow,
      active: false,
      groups: [WorkItemGroup.Cohort, WorkItemGroup.TBDrug]
    },
    {
      id: 'collection-distribution',
      name: 'Distribution',
      route: '/project/{projectId}/collection/{variable}/dist',
      icon: 'cubes',
      locale: 'distribution',
      dependencyProject: true,
      role: role.Distribution_Read,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'collection-TDMScatterPlot ',
      name: 'TDMScatterPlot',
      route: '/project/{projectId}/collection/{variable}/tdm',
      icon: 'cubes',
      locale: 'tdm.scatter.plot',
      dependencyProject: true,
      role: role.TDMScatterPlot_Allow,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'collection-MICdistribution',
      name: 'MICDistribution',
      route: '/project/{projectId}/collection/{variable}/dist-mic',
      icon: 'cubes',
      locale: 'distribution.mic',
      dependencyProject: true,
      role: role.MICDistribution_Read,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'collection-board',
      name: 'CollectionBoard',
      route: '/project/{projectId}/collection/{variable}',
      icon: 'bed',
      locale: 'collection',
      dependencyProject: true,
      role: role.Activity_Allow,
      active: false,
      groups: [
        WorkItemGroup.Cohort,
        WorkItemGroup.TBDrug,
        WorkItemGroup.DILI,
        WorkItemGroup.PopPK
      ]
    },
    {
      id: 'subject-log',
      name: 'SubjectLog',
      route: '/project/{projectId}/subject/log/0',
      icon: 'id badge',
      locale: 'subject.log',
      dependencyProject: true,
      role: role.SubjectLog_Read,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'tdm-status',
      name: 'TDMStatus',
      route: '/project/{projectId}/tdm/status',
      icon: 'eye dropper',
      locale: 'tdm.status',
      dependencyProject: true,
      role: role.TDMStatus_Read,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'mic-status',
      name: 'MICStatus',
      route: '/project/{projectId}/mic/status/0',
      icon: 'film',
      locale: 'mic.status',
      dependencyProject: true,
      role: role.MICStatus_Read,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'sample-dashboard',
      name: 'Dashboard',
      route: '/project/{projectId}/sample/dashboard',
      icon: 'dashboard',
      locale: 'dashboard',
      dependencyProject: true,
      role: role.Sample_Dashboard_Read,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sample',
      name: 'Sample',
      route: '/project/{projectId}/sample/status',
      icon: 'pallet',
      locale: 'common.sample.status',
      dependencyProject: true,
      role: role.Sample_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'collection-export',
      name: 'CollectionExport',
      route: '/project/{projectId}/collection/{variable}/export',
      icon: 'file excel',
      locale: 'common.export',
      dependencyProject: true,
      role: role.Activity_Export_Allow,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'collection-export-pk-data',
      name: 'PKDataExport',
      route: '/project/{projectId}/collection/{variable}/pkdata/export',
      icon: 'file excel',
      locale: 'common.pkdata.export',
      dependencyProject: true,
      role: role.PKData_Export_Allow,
      active: false,
      groups: [WorkItemGroup.TBDrug]
    },
    {
      id: 'collection-export-pbpk-model',
      name: 'PBPKModelExport',
      route: '/project/{projectId}/collection/{variable}/pbpkmodel/export',
      icon: 'file excel',
      locale: 'common.pbpkmodel.export',
      dependencyProject: true,
      role: role.PBPKModel_Export_Allow,
      active: false,
      groups: [WorkItemGroup.TBDrug]
    },
    {
      id: 'tdm-export',
      name: 'TdmExport',
      route: '/project/{projectId}/tdm/export',
      icon: 'file excel',
      locale: 'common.tdm.export',
      dependencyProject: true,
      role: role.Activity_Read,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'tdm-report',
      name: 'TdmReport',
      route: '/project/{projectId}/tdm/report',
      icon: 'file pdf',
      locale: 'report.tdm',
      dependencyProject: true,
      role: role.TDMReport_Build,
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'data-request',
      name: 'ExportDataManager',
      route: '/project/{projectId}/export/req/list',
      icon: 'paper plane outline',
      locale: 'common.data.request',
      dependencyProject: true,
      role: '',
      active: false,
      groups: [WorkItemGroup.Cohort]
    },
    {
      id: 'sampleSetting',
      name: 'SampleSetting',
      route: '/project/{projectId}/sample/settings',
      icon: 'cogs',
      locale: 'common.settings',
      dependencyProject: true,
      role: role.Sample_Setting_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sampleBarcode',
      name: 'SampleBarcode',
      route: '/project/{projectId}/sample/barcode',
      icon: 'barcode',
      locale: 'common.sample',
      dependencyProject: true,
      role: role.Sample_Barcode_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'aliquotBarcode',
      name: 'AliquotBarcode',
      route: '/project/{projectId}/sample/aliquot/barcode',
      icon: 'barcode',
      locale: 'common.aliquot',
      dependencyProject: true,
      role: role.Sample_Aliquot_Barcode_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sampleAliquot',
      name: 'SampleAliquot',
      route: '/project/{projectId}/sample/aliquot',
      icon: 'sitemap',
      locale: 'common.sample.aliquot',
      dependencyProject: true,
      role: role.Sample_Aliquot_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sampleBoxes',
      name: 'SampleBoxes',
      route: '/project/{projectId}/sample/boxes',
      icon: 'boxes',
      locale: 'common.boxes',
      dependencyProject: true,
      role: role.Sample_Box_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sampleBoxing',
      name: 'SampleBoxing',
      route: '/project/{projectId}/sample/boxing',
      icon: 'download',
      locale: 'common.boxing',
      dependencyProject: true,
      role: role.Sample_Boxing_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sampleFreezerIn',
      name: 'SampleFreezerIn',
      route: '/project/{projectId}/sample/freezer-in',
      icon: 'sign in',
      locale: 'common.freezer.in',
      dependencyProject: true,
      role: role.Sample_Freezer_In_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sampleFreezerOut',
      name: 'SampleFreezerOut',
      route: '/project/{projectId}/sample/freezer-out',
      icon: 'share square',
      locale: 'common.freezer.out',
      dependencyProject: true,
      role: role.Sample_Freezer_Out_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sampleOut',
      name: 'SampleOut',
      route: '/project/{projectId}/sample/out',
      icon: 'upload',
      locale: 'common.sample.out',
      dependencyProject: true,
      role: role.Sample_Out_Allow,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'sampleFreezing',
      name: 'SampleFreezing',
      route: '/project/{projectId}/sample/freezing',
      icon: 'server',
      locale: 'common.sample.freezing',
      dependencyProject: true,
      role: role.Sample_Freezing_Read,
      active: false,
      groups: [WorkItemGroup.Sample]
    },
    {
      id: 'archive',
      name: 'Archive',
      route: '/project/{projectId}/archive/0',
      icon: 'archive',
      locale: 'project.archive',
      dependencyProject: true,
      role: role.ProjectArchive_Read,
      active: false,
      groups: [WorkItemGroup.Common]
    },
    {
      id: 'archiveReq',
      name: 'Ticket',
      route: '/project/{projectId}/archive/req',
      icon: 'ticket alternate',
      locale: 'project.archive.req',
      dependencyProject: true,
      role: role.ProjectArchive_ReqAllow,
      active: false,
      groups: [WorkItemGroup.Common]
    },
    {
      id: 'search',
      name: 'Search',
      route: '/project/{projectId}/search',
      icon: 'search',
      locale: 'project.search',
      dependencyProject: true,
      role: role.Project_Read,
      active: false,
      groups: [WorkItemGroup.Common]
    }
  ],
  Console: [
    {
      id: 'console',
      name: 'Console',
      route: '/',
      originalRoute: '/',
      icon: 'gamepad',
      locale: 'console',
      dependencyProject: false,
      role: '',
      active: false,
      groups: [WorkItemGroup.Console]
    },
    {
      id: 'taskProgress',
      name: 'TaskProgress',
      route: '/console/task',
      originalRoute: '/console/task',
      icon: 'tasks',
      locale: 'task',
      dependencyProject: false,
      role: role.ConsoleTask_Read,
      active: false,
      groups: [WorkItemGroup.Console]
    },
    {
      id: 'taskScheduler',
      name: 'TaskScheduler',
      route: '/console/task/scheduler',
      originalRoute: '/console/task/scheduler',
      icon: 'time',
      locale: 'schedule',
      dependencyProject: false,
      role: role.ConsoleTask_Read,
      active: false,
      groups: [WorkItemGroup.Console]
    },
    {
      id: 'wordcloud',
      name: 'WordCloud',
      route: '/console/wordcloud',
      originalRoute: '/console/wordcloud',
      icon: 'cloud',
      locale: 'wordcloud',
      dependencyProject: false,
      role: '',
      active: false,
      groups: [WorkItemGroup.Console]
    },
    {
      id: 'wordnetwork',
      name: 'WordNetwork',
      route: '/console/wordnetwork',
      originalRoute: '/console/wordnetwork',
      icon: 'snowflake',
      locale: 'wordnetwork',
      dependencyProject: false,
      role: '',
      active: false,
      groups: [WorkItemGroup.Console]
    }
  ]
});
