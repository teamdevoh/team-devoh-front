export const ADR = 1;
export const ADR_AGEP = 2;
export const ADR_DRESS = 3;
export const ADR_SJSTEN = 4;
export const AFB = 5;
export const CASE_CON = 6;
export const COMED = 7;
export const COMORBID = 8;
export const CONSENT = 9;
export const CT = 10;
export const DIALYSIS = 11;
export const DST = 12;
export const ELECTROLYTE = 13;
export const GENOTYPE = 14;
export const ICRE = 15;
export const IGRA = 16;
export const INTERVIEW = 17;
export const LBCH = 18;
export const LBHEM = 19;
export const LPARESISTANT = 20;
export const MEDICATION = 21;
export const MIC_RESULT = 22;
export const MIC_SETUP = 23;
export const MIC_TRNS = 24;
export const Other_exam = 25;
export const PCR = 26;
export const PP = 27;
export const RESISTANT_GENE = 28;
export const SAMPLING = 29;
export const TB_DIAGNOSIS = 30;
export const TB_PK = 31;
export const TDMREP = 32;
export const VS = 33;
export const XPERT = 34;
export const XRAY = 35;
export const BasicPhysico = 36;
export const AdmeBasic = 37;
export const Inhibition = 38;
export const MeTransporter = 39;
export const PkData = 40;
export const FactorsAffecting = 41;
export const PgxInteractions = 42;
export const PdData = 43;
export const DrugInteractions = 44;
export const ToxicityDataSheet = 45;
export const TDMAnti = 47;
export const MICReport = 49;
export const Relapse = 51;
export const Modeling = 53;

export const LabType = Object.freeze({
  LBCH: 'LBCH',
  LBHEM: 'LBHEM'
});

export const LabRanges = Object.freeze({
  LOW: { name: 'LOW', color: { name: 'yellow', hex: '#fbbd08' } },
  HIGH: { name: 'HIGH', color: { name: 'red', hex: '#db2828' } },
  NORMAL: { name: 'NORMAL', color: { name: '', hex: '' } }
});

export const LabLBCHItems = Object.freeze({
  ALB: 'ALB',
  BUN: 'BUN',
  CR: 'CR',
  EGFR: 'EGFR',
  TBIL: 'TBIL',
  TPRO: 'TPRO',
  AST: 'AST',
  ALT: 'ALT',
  ALP: 'ALP',
  UA: 'UA'
});

export const LabLBHEMItems = Object.freeze({
  WBC: 'WBC',
  RBC: 'RBC',
  HGB: 'HGB',
  HCT: 'HCT',
  PLT: 'PLT',
  NEU: 'NEU',
  LYM: 'LYM',
  MON: 'MON',
  EOS: 'EOS',
  BAS: 'BAS',
  ANC: 'ANC'
});
