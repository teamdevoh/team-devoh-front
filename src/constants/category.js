export const Language = [
  { key: 'en', text: 'English', value: 'en' },
  { key: 'ko', text: '한국어', value: 'ko' }
];

export const PopPKFile = {
  Original: 'O',
  Nonmem: 'N',
  Model: 'M'
};
