

# Genotype Analysis

- [Prerequisites](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-prerequisites)
- [Editor](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-editor)
- [VS Tools](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-vs-tools)
- [Reference](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-reference)
- [Installation](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-installation)
- [Getting started](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-getting-started)
- [Folder Structure](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-folder-structure)
- [Available Scripts](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-available-scripts)
- [Style Guide](https://paikctcproject.visualstudio.com/spmed-genotype-analysis-front#user-content-style-guide)

## Prerequisites
- [Chocolatey](https://chocolatey.org/)
  ```
  [Git] C:\> choco install git
  [SourceTree] C:\> choco install sourcetree
  [NodeJS] C:\> choco install nodejs-lts
  ```

- chrome://extensions
  - [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

  - [Redux DevTools Extension](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd)

    ![Redux DevTools](https://camo.githubusercontent.com/a0d66cf145fe35cbe5fb341494b04f277d5d85dd/687474703a2f2f692e696d6775722e636f6d2f4a34476557304d2e676966)
    
  - [React Perf](https://chrome.google.com/webstore/detail/react-perf/hacmcodfllhbnekmghgdlplbdnahmhmm)

    ![React Perfv](https://raw.githubusercontent.com/crysislinux/chrome-react-perf/master/demo/v1.0.0.gif)


## Editor
- [Visual Studio Code](https://code.visualstudio.com/)


## Reference
- [ReactJS](https://facebook.github.io/react/)
- [Redux](https://deminoth.github.io/redux/)
- [WebPack](https://webpack.github.io/)
- [React Data Grid](http://adazzle.github.io/react-data-grid/)
- [Semantic UI React](https://react.semantic-ui.com/)
- [lodash](https://lodash.com/)
- [Axios](https://github.com/mzabriskie/axios)
- [Draft wysiwyg](https://jpuri.github.io/react-draft-wysiwyg)

## Installation
```
git clone .....[repository url]..... genotype-analysis-front
cd genotype-analysis-front
npm install
```

## Getting started

```
npm start
```


## Folder Structure

After creation, your project should look like this:

```
genotype-analysis/
  config/
  node_modules/
  public/
    index.html
    favicon.ico
  scripts/
  src/
  .eslintrc.json
  .gitignore
  package.json
  README.md
  styleguide.config.js
  Web.config
```

For the project to build, **these files must exist with exact filenames**:

* `public/index.html` is the page template;
* `src/index.js` is the JavaScript entry point.

You can delete or rename the other files.

You may create subdirectories inside `src`. For faster rebuilds, only files inside `src` are processed by Webpack.

You need to **put any JS and CSS files inside `src`**, or Webpack won't see them.

Only files inside `public` can be used from `public/index.html`.

Read instructions below for using assets from JavaScript and HTML.

You can, however, create more top-level directories.

They will not be included in the production build so you can use them for things like documentation.

## Available Scripts

In the project directory, you can run:

```
npm start
```

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

```
npm test
```


Launches the test runner in the interactive watch mode.

See the section about [running tests](#running-tests) for more information.

```
npm run build
```

Builds the app for production to the `build` folder.

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

Your app is ready to be deployed!


## Style Guide

```
npm run styleguide
```

Open [http://localhost:6060](http://localhost:6060) to view it in the browser.

The page will reload if you make edits.

```
npm run styleguide:build
```

Builds the app for production to the `styleguide` folder.

